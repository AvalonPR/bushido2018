var base_url = window.location.origin;
if (base_url == 'http://localhost') {
    base_url = 'http://localhost/Bushido2018'
    }

$(document).on('ready', function () {
    var path = window.location.pathname.split("/");
    var last_path = path.pop();
    var controller = path.pop();
    if (controller == 'admin') {
        last_path = controller + '/' + last_path;
    } else {
        if (last_path == 'viaje') {
            last_path = 'inicio/viaje';
        }
        if (last_path == 'contacto') {
            last_path = 'inicio/contacto';
        }
    }
    
    $(document).on('click', 'input[name="radio-viaje"]', function() {
        if($(this).val()=='1'){
            document.getElementById('yearSelect').style.display = "block";
        } else{
            document.getElementById('yearSelect').style.display = "none";
        }
    
    });
    
    
    var target = $('nav a[href="' + base_url + '/' + last_path + '"]');
    console.log(target);
    // Add active class to target link
    target.addClass('current');
    
    $('#newpass').click(function () {
        $('#newpass').removeAttr('placeholder');
    });
    $('#newpass2').click(function () {
        $('#newpass2').removeAttr('placeholder');
    });
    $('#changebutpw').click(function () {
        var path = window.location.pathname.split("/");
        var n = path.indexOf("profesionales");
        if(n === 1){
            var title = "Confirmar cambio";
            var mes = "Cambiar la contraseña lo hará también para https://profesionales.yokohamaiberia.es";
            var confirm = "Confirmar";
            var cancel = "Cancelar";
        } else{
            var title = "Confirmar alteração";
            var mes = "Altere a senha também para https://profissionais.yokohamaiberia.pt";
            var confirm = "Confirme";
            var cancel = "Cancelar";
        }
        bootbox.confirm({
            title: title,
            message: mes,
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> '+cancel
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> '+confirm
                }
            },
            callback: function (result) {
                if (result === true) {
                    $.ajax({
                        type: "POST",
                        url: base_url + "/inicio/changePw",
                        data: {
                            newpass: $('#newpass').val(),
                            newpass2: $('#newpass2').val()
                        },
                        success: function (data) {
                            var obj = JSON.parse(data);
                            if (obj.error == 1) {
                                toastr.success(obj.toast);
                            } else {
                                toastr.error(obj.toast);
                            }
                        }
                    });
                }
            }
        });
    });
    
 
    
    const changeText = function (el, text, color) {
        el.text(text).css('color', color);
    };
  
    $('.contrasena').keyup(function(){
        let len = this.value.length;
        const pbText = $('.registerform .progress-bar_text');
        if (len === 0) {
          $(this).css('border-color', '#2F96EF');
          changeText(pbText, 'Contraseña vacía', '#aaa');
        } else if (len > 0 && len <= 4) {
          $(this).css('border-color', '#FF4B47');
          changeText(pbText, 'Contraseña débil', '#FF4B47');
        } else if (len > 4 && len <= 8) {
          $(this).css('border-color', '#F9AE35');
          changeText(pbText, 'Contraseña media', '#F9AE35');
        } else {
          $(this).css('border-color', '#2DAF7D');
          changeText(pbText, 'Contraseña fuerte', '#2DAF7D');
        } 
    });
    
    $('#eye').mousedown(function(){
        $('#pass').removeAttr('type');
        $('#eye').html("visibility_off");
    });
    
    $('#eye').mouseup(function(){
        $('#pass').attr('type','password');
        $('#eye').html("visibility");
    });
    
    $('#eyeconfirm').mousedown(function(){
        $('#passconfirm').removeAttr('type');
        $('#eyeconfirm').html("visibility_off");
    });
    
    $('#eyeconfirm').mouseup(function(){
        $('#passconfirm').attr('type','password');
        $('#eyeconfirm').html("visibility");
    });
    
   
    $(".bt-switch").bootstrapSwitch();


    $('.quantity').on('click', '.plus', function (e) {
        $input = $(this).prev('input.qty');
        var val = parseInt($input.val());
        var step = $input.attr('step');
        step = 'undefined' !== typeof (step) ? parseInt(step) : 1;
        $input.val(val + step).change();
    });

    $('.quantity').on('click', '.minus',
            function (e) {
                $input = $(this).next('input.qty');
                var val = parseInt($input.val());
                var step = $input.attr('step');
                step = 'undefined' !== typeof (step) ? parseInt(step) : 1;
                if (val > 1) {
                    $input.val(val - step).change();
                }
            });

    $('.quantityCart').on('click', '.plus', function (e) {
        var key = $(this).prev('input.qty').attr("data-id");
        var total = document.getElementById('amount' + key).innerHTML;
        total = total.split(" ");
        total = total[0].replace('.', '');
        $input = $(this).prev('input.qty');
        var val = parseInt($input.val());
        total = total * (val + 1);
        $(document.getElementById('totalAmount' + key)).text(total + ' km');
        var step = $input.attr('step');
        step = 'undefined' !== typeof (step) ? parseInt(step) : 1;
        $input.val(val + step).change();
    });


    $('.quantityCart').on('click', '.minus',
            function (e) {
                var key = $(this).next('input.qty').attr("data-id");
                var total = document.getElementById('amount' + key).innerHTML;
                total = total.split(" ");
                total = total[0].replace('.', '');
                $input = $(this).next('input.qty');
                var val = parseInt($input.val());
                var step = $input.attr('step');
                step = 'undefined' !== typeof (step) ? parseInt(step) : 1;
                if (val > 1) {
                    total = total * (val - 1);
                    $(document.getElementById('totalAmount' + key)).text(total + ' Km');
                    $input.val(val - step).change();
                }
            });

    $('.quantity-size').on('click', '.plus', function (e) {
        var sum = 0;
        var id = parseInt($("#id")[0].value);
        var qty = $("#quantity" + id)[0].value;
        var unidades = parseInt($("#unidades")[0].value);
        qty = qty * unidades;
        var arr = $('input[name="qtysize[]"]').map(function () {
            if ($($(this)[0].parentNode).css('display') == 'block') {
                if (this.value == '0') {
                    sum = 0;
                    return 1;
                } else {
                    sum = 1;
                    return this.value;
                }
            }
        }).get();
        var max = parseInt(array_sum(arr)) + parseInt(sum);
        $input = $(this).prev('input.qty');
        var val = parseInt($input.val());
        var step = $input.attr('step');
        step = 'undefined' !== typeof (step) ? parseInt(step) : 1;
        if (max <= qty) {
            $input.val(val + step).change();
        }
    });

    $('.quantity-size').on('click', '.minus', function (e) {
        var sum = 0;
        var id = parseInt($("#id")[0].value);
        var qty = $("#quantity" + id)[0].value;
        var unidades = parseInt($("#unidades")[0].value);
        qty = qty * unidades;
        var arr = $('input[name="qtysize[]"]').map(function () {
            if ($($(this)[0].parentNode).css('display') == 'block') {
                if (this.value == '0' || this.value == '1') {
                    sum = 0;
                    return 1;
                } else {
                    sum = 1;
                    return this.value;
                }
            }
        }).get();
        var max = parseInt(array_sum(arr)) - parseInt(sum);
        $input = $(this).next('input.qty');
        var val = parseInt($input.val());
        var step = $input.attr('step');
        step = 'undefined' !== typeof (step) ? parseInt(step) : 1;
        if (val > 1 && max <= qty) {
            $input.val(val - step).change();
        }
    });

    $('#contacts').on('click', 'li', function () {
        $("#contacts li").removeClass("active"); //assuming that it has to be removed from other li's, else remove this line
        $(this).addClass("active");
    });

    $("input:checkbox").on('click', function () {
        // in the handler, 'this' refers to the box clicked on
        var $box = $(this);
        if ($box.is(":checked")) {
            // the name of the box is retrieved using the .attr() method
            // as it is assumed and expected to be immutable
            var group = "input:checkbox[name='" + $box.attr("name") + "']";
            // the checked state of the group/box on the other hand will change
            // and the current value is retrieved using .prop() method
            $(group).prop("checked", false);
            $box.prop("checked", true);
        } else {
            $box.prop("checked", false);
        }
    });

    if ($('.travel-date-group')) {
        $('.travel-date-group .input-daterange').datepicker({
            autoclose: true,
            locale: 'es',
            format: 'yyyy-mm-dd',
            todayHighlight: true
        });
    }

    if ($('#campas-activas')) {
        $('#campas-activas').dataTable({
            responsive: true,
            "order": [[0, "desc"]]
        });
    }
    if ($('#slidersTable')) {
        $('#slidersTable').dataTable({
            responsive: true,
            "order": [[2, "desc"]]
        });
    }
    if ($('#historico-campanas')) {
        $('#historico-campanas').dataTable({
            responsive: true,
            "order": [[0, "desc"]]
        });
    }
    if ($('#listado-usuarios')) {
        $('#listado-usuarios').dataTable({
            responsive: true,
            "order": [[0, "desc"]]
        });
    }
    if ($('#estadisticas_table')) {
        $('#estadisticas_table').dataTable({
            responsive: true,
            "order": [[3, "desc"]]
        });
    }

    if ($('#FormReal_table')) {
        $('#FormReal_table').dataTable({
            responsive: true,
            "order": [[0, "desc"]]
        });
    }
    
    if ($('#ProxFormTable')) {
        $('#ProxFormTable').dataTable({
            responsive: true,
            "order": [[0, "asc"]]
        });
    }
    
    if ($('#pedidos_table')) {
        $('#pedidos_table').dataTable({
            responsive: true,
            "order": [[1, "desc"]]
        });
    }
    
    if ($('#promociones_user')) {
        $('#promociones_user').dataTable({
            responsive: true,
            "order": [[0, "desc"]]
        });
    }
    
    if ($('#users_table')) {
        $('#users_table').dataTable({
            responsive: true,
        });
        $('#users_table').on('change', '.switch-details', function(){
            console.log($(this));
            if($(this)[0].checked){
                var id = ($(this).attr('data-id'));
                var estado = 1;
            }else{
                var id = ($(this).attr('data-id'));
                var estado = 0;
            }
            $.ajax({
                type: "POST",
                url: base_url + "/admin/ajax/modifyUser",
                data: {
                    id: id,
                    estado: estado
                },
                success: function (data) {
                    toastr.success("Usuario modificado");
                }
            });
        });
    }
    

    $('#mobileSelect').on('change', function () {
        var url = $(this).val();
        window.location.replace(url);
    });


    $("#input-1").fileinput({
        maxFileCount: 1
    });

    // Multiple Select
    $(".select-1").select2({
        placeholder: "Selecciona Múltiples variables"
    });

    // Loading array data
    var data = [{id: 0, text: 'enhancement'}, {id: 1, text: 'bug'}, {id: 2, text: 'duplicado'}, {id: 3, text: 'no válido'}, {id: 4, text: 'wontfix'}];
    $(".select-data-array").select2({
        data: data
    })
    $(".select-data-array-selected").select2({
        data: data
    });

    var grafica1 = 1;
    var grafica2 = 0;


    $(".grafica-compras").click(function (e) {
        e.preventDefault();

        if (grafica1 == 0) {

            $('.grafica-compras').addClass('active');
            $('.grafica-gasto').removeClass('active');
            $("#grafica1").css("opacity", "1");
            $("#grafica2").css("opacity", "0");
            grafica1 = 1;
            grafica2 = 0;

        }
    })

    $(".grafica-gasto").click(function (e) {
        e.preventDefault();

        if (grafica2 == 0) {

            $('.grafica-compras').removeClass('active');
            $('.grafica-gasto').addClass('active');
            $("#grafica2").css("opacity", "1");
            $("#grafica1").css("opacity", "0");
            grafica1 = 0;
            grafica2 = 1;

        }
    })

    // Enabled/Disabled
    $(".select-disabled").select2();
    $(".select-enable").on("click", function () {
        $(".select-disabled").prop("disabled", false);
        $(".select-disabled-multi").prop("disabled", false);
    });
    $(".select-disable").on("click", function () {
        $(".select-disabled").prop("disabled", true);
        $(".select-disabled-multi").prop("disabled", true);
    });

    // Without Search
    $(".select-hide").select2({
        minimumResultsForSearch: Infinity
    });

    // select Tags
    $(".select-tags").select2({
        tags: true
    });

    // Select Splitter
    $('.selectsplitter').selectsplitter();

    if ($('#envioBox').length) {
        document.getElementById('envioBox').onchange = function () {
            document.getElementById('shipping-form-companyname').readOnly = this.checked;
            document.getElementById('shipping-form-companyname').value = document.getElementById('billing-form-companyname').value;
            document.getElementById('shipping-form-address').readOnly = this.checked;
            document.getElementById('shipping-form-address').value = document.getElementById('billing-form-address').value;
            document.getElementById('shipping-form-city').readOnly = this.checked;
            document.getElementById('shipping-form-city').value = document.getElementById('billing-form-city').value;
        };
    }
});



function saveForm() {
    var nombre = document.getElementById('nombre').value;
    var fecha = document.getElementById('fecha').value;
    if (nombre == '' || fecha == '') {
        toastr.error('Hay campos vacíos');
    } else {
        $.ajax({
            type: "POST",
            url: base_url + "/admin/ajax/saveForm",
            data: {
                nombre: nombre,
                fecha: fecha
            },
            success: function (data) {
                var obj = JSON.parse(data);
                document.getElementById('proxFormsTable').innerHTML = obj.table;
                if ($.fn.dataTable.isDataTable('#ProxFormTable')) {
                    table = $('#ProxFormTable').DataTable();
                    table.destroy();
                }
                $('#ProxFormTable').DataTable({
                    responsive: true,
                    "order": [[0, "asc"]]
                });
                toastr.success(obj.toast);
            }
        });
    }

}


function addtoCart(id) {
    var arr = null;
    var color = null;
    var cartWrapper = $('#top-cart');
    var cartBody = cartWrapper.find('.top-cart-item');
    var cartList = cartBody.find('ul').eq(0);
    var cartCount = cartWrapper.children(0).find('.count');
    var cartTotal = cartWrapper.find('.top-cart-action').find('span');
    var unidades = parseInt(cartCount[0].innerText);
    var qty = 1;
    if ($("#quantity" + id).length > 0) {
        qty = $("#quantity" + id)[0].value;
    }
    if (unidades === 0) {
        $(cartWrapper[0].children[0]).removeAttr('style');
    }
    unidades = parseInt(unidades) + parseInt(qty);
    var prcacumulado = 0;
    var elemento = null;
    arr = $('input[name="qtysize[]"]').map(function () {
        if ($($(this)[0].parentNode).css('display') == 'block') {
            elemento = new Array();
            elemento = [$($(this)[0].parentNode).attr("data-size"), this.value];
            return JSON.stringify(elemento);
        }
    }).get();
    if ($("input:checkbox").length) {
        $('#color:checked').each(
                function () {
                    color = ($(this).attr('data-id'));
                }
        );
    }
    $.ajax({
        type: "POST",
        url: base_url + "/carrito/ajax/insert",
        data: {
            id: id,
            qty: qty,
            arr: arr,
            color: color
        },
        success: function (respuesta) {
            var obj = JSON.parse(respuesta);
            if (obj.error == '1') {
                toastr.error(obj.toast);
            } else {
                cartCount.text(unidades);
                $('#top-cart-ul > li').remove();
                var arrayCart = jQuery.parseJSON(obj.carrito);
                for (var key in arrayCart) {
                    if (arrayCart.hasOwnProperty(key)) {
                        var productAdded = $('<li class="top-cart-list"><div class="top-cart-item-image"><a href="' + base_url + '/productos/' + arrayCart[key]["options"]["url"] + '"><img src="' + arrayCart[key]["options"]["img"] + '" alt="' + arrayCart[key]["name"] + '" /></a></div><div class="top-cart-item-desc"><a href="' + base_url + '/productos/' + arrayCart[key]["options"]["url"] + '"><span class="fleft" style="width: 85%;">' + arrayCart[key]["name"] + '</span></a><span class="top-cart-item-quantity fright">x ' + arrayCart[key]["qty"] + '</span><div class="clear"></div><span class="top-cart-item-price fleft">' + Math.round(arrayCart[key]["price"]) + ' Km</span><a href="#" class="remove fright" onclick="removeCart(\'' + arrayCart[key]["rowid"] + '\')" title="Eliminar este item"><i class="icon-trash2"></i></a></div><div class="clear"></div></li>');
                        cartList.prepend(productAdded);
                        prcacumulado += Math.round(arrayCart[key]["price"] * arrayCart[key]["qty"]);
                    }
                }
                var finalprc = Math.round(prcacumulado);
                cartTotal.text((finalprc.toLocaleString('en').toString().replace(',', '.')) + ' Km');

                $('#top-cart-trigger').addClass('animated bounce');
                $('#top-cart').addClass('top-cart-open');
            }
        }
    });
}

function sendToProduct(url) {
    toastr.info("Este artículo necesita elección de talla y/o color.");
    setTimeout(function () {
        window.location.replace(base_url + "/productos/" + url[0] + '/' + url[1] + '/' + url[2] + '/' + url[3]);
    }, 2000);
}


function removedatatable(id, flag = false) {
    if (!flag) {
        removeCart(id);
    }
    $.ajax({
        type: "POST",
        url: base_url + "/carrito/ajax/remove",
        data: {
            id: id
        },
        success: function (respuesta) {
            $('#data-cart').children('tr').remove();
            $('#cart-total').children('tr').remove();
            document.getElementById('cart-data-context').innerHTML = respuesta;
            if ($.fn.dataTable.isDataTable('#ProxFormTable')) {
                table = $('#ProxFormTable').DataTable();
                table.destroy();
            }
            $('#ProxFormTable').DataTable({
                responsive: true,
                "order": [[0, "asc"]]
            });
        }
    });
}

function removeCart(id) {
    if (window.location == base_url + "/carrito") {
        removedatatable(id, true);
    } else if (window.location == base_url + "/carrito/pago") {
        removedatatablepago(id, true);
    }
    var cartWrapper = $('#top-cart');
    var cartBody = cartWrapper.find('.top-cart-item');
    var cartList = cartBody.find('ul').eq(0);
    var cartCount = cartWrapper.children(0).find('.count');
    var cartTotal = cartWrapper.find('.top-cart-action').find('span');
    var prcacumulado = 0;
    var unidades = 0;
    $.ajax({
        type: "POST",
        url: base_url + "/carrito/ajax/removeCart",
        data: {
            id: id
        },
        success: function (respuesta) {
            $('#top-cart-ul > li').remove();
            var arrayCart = jQuery.parseJSON(respuesta);
            for (var key in arrayCart) {
                if (arrayCart.hasOwnProperty(key)) {
                    var productAdded = $('<li class="top-cart-list"><div class="top-cart-item-image"><a href="' + base_url + '/productos/' + arrayCart[key]["options"]["url"] + '"><img src="' + arrayCart[key]["options"]["img"] + '" alt="' + arrayCart[key]["name"] + '" /></a></div><div class="top-cart-item-desc"><a href="' + base_url + '/productos/' + arrayCart[key]["options"]["url"] + '"><span class="fleft" style="width: 85%;">' + arrayCart[key]["name"] + '</span></a><span class="top-cart-item-quantity fright">x ' + arrayCart[key]["qty"] + '</span><div class="clear"></div><span class="top-cart-item-price fleft">' + Math.round(arrayCart[key]["price"]) + ' Km</span><a href="#" class="remove fright" onclick="removeCart(\'' + arrayCart[key]["rowid"] + '\')" title="Eliminar este item""><i class="icon-trash2"></i></a></div><div class="clear"></div></li>');

                    cartList.prepend(productAdded);
                    prcacumulado = prcacumulado + arrayCart[key]["subtotal"];
                    unidades = parseInt(unidades + arrayCart[key]["qty"]);
                }
            }
            if (unidades == 0) {
                $('.top-cart-content').hide();
                cartWrapper[0].children[0].setAttribute("style", "pointer-events:none; cursor:default;");
            }
            cartCount.text(unidades);
            cartTotal.text((prcacumulado.toLocaleString('en').toString().replace(',', '.')) + ' Km');
        }
    });
}


function aceptarLegales() {
    var com = document.getElementById('comunicaciones').value;
    document.getElementById('aceptar-condiciones').onsubmit = function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "/inicio/aceptarLegales",
            data: {
                bases: 'on',
                privacidad: 'on',
                comunicaciones: com
            },
        });
        toastr.success("Gracias por aceptar nuestras bases legales.");
        setTimeout(function () {
            window.location.replace(base_url + "/inicio");
        }, 2000);
    }
}


function aceptarViaje() {
    var viaje = document.querySelector('input[name="radio-viaje"]:checked').value;
    var ano = document.querySelector('input[name="radio-year"]:checked').value;
    document.getElementById('aceptar-viaje').onsubmit = function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "/inicio/aceptarViaje",
            data: {
                viaje: viaje,
                ano: ano
            }
        });
        toastr.success("Tu opción ha sido guardada.");
        setTimeout(function () {
            window.location.replace(base_url + "/inicio");
        }, 2000);
    }
}

function selectSize(talla, id) {
    var button = document.getElementById(talla);
    var div = 'div' + talla;
    var qty = document.getElementById(div);
    if ($(button).hasClass('activo')) {
        button.classList.remove("activo");
        qty.style.display = "none";
    } else {
        var cant = $("#quantity" + id)[0].value;
        var unidades = parseInt($("#unidades")[0].value);
        cant = cant * unidades;
        var arr = $('input[name="qtysize[]"]').map(function () {
            if ($($(this)[0].parentNode).css('display') == 'block') {
                return this.value;
            }
        }).get();
        var max = parseInt(array_sum(arr));
        if (max < cant) {
            button.className += " activo";
            qty.style.display = "block";
        }
    }
}

function selectColor(color, id) {
    var button = document.getElementById('color' + color);
    if ($(button).hasClass('color-checked')) {
        button.classList.remove("color-checked");
    } else {
        button.className += " color-checked";
    }
}

function array_sum(array) {
    var key
    var sum = 0

    // input sanitation
    if (typeof array !== 'object') {
        return null
    }

    for (key in array) {
        if (!isNaN(parseFloat(array[key]))) {
            sum += parseFloat(array[key])
        }
    }
    return sum
}


function popupViewed() {
    $.ajax({
        type: "POST",
        url: base_url + "/inicio/viewPopup",
        data: {
            view: true,
        },
    });
}

                