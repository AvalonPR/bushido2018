<!-- Page Title
============================================= -->
<section id="page-title" class="header programa_header">

    <div class="container clearfix">
        <h1>Condicones generales</h1>
        <span>&nbsp;</span>
    </div>

</section><!-- #page-title end -->

<!-- Content
        ============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix legal">
            
            <!-- Post Content
            ============================================= -->
            <div class="postcontent nobottommargin col_last clearfix">
                
                <h2>AVISO LEGAL</h2>
                <h3>Condiciones generales</h3>
                <p>El dominio www.bushido.yokohamaiberia.es, es propiedad de YOKOHAMA IBERIA S.A. aunque serán alojados y gestionados por la agencia Avalon Diseño, Comunicación y Marketing, S.L. La agencia pone a disposición de dichos dominios los espacios necesarios en sus servidores para alojar todos los archivos necesarios para el correcto funcionamiento del programa online. Avalon Diseño, Comunicación y Marketing, S.L. aplicará a Yokohama la misma garantía de servicio que le aplique a su vez su proveedor de servicios de alojamiento digital.</p>
                <p>Las operaciones de compra deben ser realizadas por el participante que haya cerrado efectivamente la compra con Yokohama, y no podrán ser cedidas a otros participantes. Este hecho se considerará como una infracción grave con la consiguiente baja en el programa de los participantes implicados.</p>
                <p>Para poder acceder a los premios será condición indispensable que el participante sea cliente activo en el momento del canje de los regalos y en la fecha de entrega de los premios o disfrute de los mismos.</p>
                <p>Los premios se entregarán en un máximo de 45 días hábiles, desde el momento en que se confirme el pedido. El plazo de entrega de los premios no se garantiza durante los periodos vacacionales: Semana Santa, julio, agosto, fiestas nacionales y fiestas navideñas.</p>
                <p>Los premios conseguidos no podrán ser canjeados por premios diferentes a los definidos ni por dinero. Las fotografías de los premios no son contractuales.</p>
                <p>Es responsabilidad del participante la comprobación de que el premio esté en buen estado. Es responsabilidad del participante el envío de la garantía para su sellado en caso de conformidad con el premio.</p>
                <p>Yokohama se reserva el derecho a modificar o sustituir los premios por otros de valor equivalente o eliminarlos definitivamente cuando, por causas ajenas a su voluntad, no sea posible entregar el solicitado. En este caso, enviará, a cambio, artículos de igual o superior valor, comunicándolo oportunamente al solicitante. Si el participante no está conforme con el cambio propuesto, se anulará el canje restableciéndose de nuevo los kilómetros y/o puntos utilizados en el mismo.</p>
                <p>Siempre que se produzca cualquier variación en la gerencia del taller participante, el propio taller deberá comunicarlo a su comercial de Yokohama y a la Secretaría de programa a través del e-mail: administracion.bushido@yokohamaiberia.com.</p>
                <p>Las omisiones o reclamaciones deberán ser presentadas por los participantes a la Secretaría de programa a través del email: administracion.bushido@yokohamaiberia.com. En caso de verificarse cualquier reclamación cuya importancia justifique una corrección o complemento del presente reglamento, Yokohama lo hará efectivo y lo comunicará a los participantes.</p>
                <p>La Secretaría de programa dispondrá asimismo del teléfono +34 915 631 011 de atención al participante de recepción de llamadas en horario de lunes a jueves de 10 -14h. y 15 - 18h. y viernes de 9 - 15h., donde se atenderán todas aquellas dudas que los participantes pudieran tener, si bien dicha comunicación no tendrá carácter contractual para la resolución de los problemas y/o reclamaciones.</p>
                <p>El programa está sujeto a la legislación fiscal vigente.</p>
                <p>El presente reglamento podrá ser modificado por Yokohama cuando se verifique la necesidad de adecuarlo al espíritu (de apoyo e incentivo a los participantes para alcanzar objetivos comerciales) con que fue creado. El cambio se comunicará a los participantes, con 10 días de antelación.</p>
                <p>Yokohama se reserva el derecho de cambiar tanto el sistema de adjudicación de puntos (valoración de cada tipo de Operación - Variables), como los premios (Catálogo de Regalos, Viajes y Servicios).</p>
                <p>El hecho de participar en este programa implica la aceptación de estas bases y de los posibles anexos que puedan ir añadiéndose durante el periodo de vigencia del mismo. Los Participantes en el Programa aceptan el criterio de Yokohama para dirimir cualquier clase de discrepancia que surja en la interpretación de estas bases o asuntos contemplados en las mismas.</p>
                <p>Yokohama evaluará y decidirá al respecto sobre todo aquello que no esté contemplado en las bases y normas del programa.</p>

                
            </div>
        <!-- POST CONTENT END
            ============================================= -->
        
        
        <!-- Sidebar
            ============================================= -->
            <div class="sidebar nobottommargin clearfix">
                <div class="sidebar-widgets-wrap">
                    <div class="widget widget_links clearfix">

                        <h3>El programa</h3>
                        
                        <ul>
                            <li><a href="{{baseurl}}inicio/bases"><div>Bases legales</div></a></li>
                            <li><a href="{{baseurl}}inicio/generales" class="active"><div>Condiciones generales</div></a></li>
                            <li><a href="{{baseurl}}inicio/privacidad"><div>Política de privacidad</div></a></li>
                            <li><a href="{{baseurl}}inicio/canjeo"><div>Canjeo</div></a></li>
                            <li><a href="{{baseurl}}inicio/cookies"><div>Cookies</div></a></li> 
                                
                        </ul>

                    </div>
                </div>
            </div>
            
            <!-- Sidebar END
            ============================================= -->
        
        
        </div>
            
            
    </div>

</section><!-- #content end -->
