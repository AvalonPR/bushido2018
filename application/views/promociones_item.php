<?php
include 'header.php';
?>
<!-- Page Title
============================================= -->
<section id="page-title">

    <div class="container clearfix">
        <h1>Promoción Decathlon</h1>
        <span>Activa desde 13/03/2018 hasta 13/04/2018</span>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">
            <h3>Usuarios adheridos a la promoción</h3>
            <div class="line-custom"></div>
            <div class="table-responsive">
                <table class="table table-striped activas">
                    <thead>
                        <tr>
                            <th class="col-md-2">Nombre</th>
                            <th class="col-md-2">Importe total</th>
                            <th class="col-md-2">Gastado</th>
                            <th class="col-md-1">Disponible</th>
                            <th class="col-md-1">Canjeos realizados</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="click"><a href="{{siteurl}}taller/usuarios/info">Carla Rodríguez</a></td>
                            <td>500 puntos</td>
                            <td>100 puntos</td>
                            <td>400 puntos</td>
                            <td class="click"><button class="button" data-toggle="modal" data-target="#ModalUserPromo"><i class="material-icons">remove_red_eye</i></button></td>
                        </tr>
                        <tr>
                            <td class="click"><a href="user_info">Carla Rodríguez</a></td>
                            <td>500 puntos</td>
                            <td>100 puntos</td>
                            <td>400 puntos</td>
                            <td class="click"><button class="button" data-toggle="modal" data-target="#ModalUserPromo"><i class="material-icons">remove_red_eye</i></button></td>
                        </tr>
                    </tbody>
                </table>
            </div>


        </div>

    </div>

</section><!-- #content end -->


<!--Modal promos activas-->
<div class="modal fade" id="ModalUserPromo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-body">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Canjeos realizados</h4>
                </div>
                <div class="modal-body ">
                    <table class="table table-striped activas">
                        <thead>
                            <tr>
                                <th class="col-md-2">Producto</th>
                                <th class="col-md-2">Nombre</th>
                                <th class="col-md-2">Puntos</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="click"><img alt="" src="{{imgurl}}shop/thumbs/small/dress-3.jpg"></td>
                                <td><a href="#"> Producto 1 </a></td>
                                <td>50 puntos</td>
                            </tr>
                            <tr>
                                <td class="click"><img alt="" src="{{imgurl}}shop/thumbs/small/shoes-2.jpg"></td>
                                <td>Producto 1</td>
                                <td>50 puntos</td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
<?php
include 'footer.php'
?>