<!-- Page Title
============================================= -->
<section id="page-title" class="header programa_header">

    <div class="container clearfix">
        <h1>Condiciones de canjeo</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
        ============================================= -->
<section id="content">
    <div class="content-wrap">
    
    

    

        <div class="container clearfix legal">
            <!-- Post Content
            ============================================= -->
            <div class="postcontent nobottommargin col_last clearfix">
                <p>Los participantes podrán optar a canjear los kilómetros acumulados por uno o varios regalos del catálogo del programa en función de los kilómetros obtenidos. El último día hábil para el canjeo de productos o servicios será el 31 de enero de 2020 a las 23:55h. Una vez pasada dicha fecha, los kilómetros obtenidos y no redimidos serán borrados de la cuenta del participante. Cuando el participante realice un canje se descontarán los kilómetros necesarios para alcanzar el total de puntuación del regalo. Dentro del catálogo de regalos podremos encontrar productos y servicios.</p>
                <ul>
                  <li>
                    <h5>PRODUCTOS</h5>
                    <p>Antes de realizar un pedido el Participante deberá asegurarse de las características del regalo solicitado (modelo, talla, color) ya que una vez cursado el pedido no se permitirá su anulación.</p>
                    <p>El plazo de entrega de los regalos es de 30/45 días una vez realizado el pedido. Al recibo del regalo, el Participante deberá tener en cuenta las siguientes consideraciones:</p>
                    <ul>
                      <li>Es responsabilidad del Participante la comprobación de que el regalo está en buen estado. El plazo para cursar una reclamación por rotura ó defecto es de 48 horas.</li>
                      <li>El tiempo máximo de recogida de un regalo en mal estado es de una semana, exceptuando Ceuta, Melilla, Baleares, Canarias y Portugal.</li>
                      <li>No se admitirán devoluciones de productos en buen estado.</li>
                      <li>En caso de problemas de funcionamiento o disconformidad con el regalo recibido, el Participante podrá acudir al Teléfono de Atención de Programa: +34 91 563 10 11.</li>
                    </ul>
                    <p>Los productos tienen una garantía de dos años desde la entrega; no es necesario sellar el documento de garantía que se incluye. El albarán de entrega constituye el documento hábil para acreditar la fecha de entrega del producto (art. 123RDL 1/2007).</p>
                    <p>Es <strong><u>obligatorio</u></strong> por tanto, guardar el albarán de entrega para acreditar la recepción del producto y hacer efectiva la mencionada garantía durante los 2 años siguientes a su recepción, en cualquier punto de venta con servicio técnico.</p>
                  </li>
                  <li>
                    <h5>SERVICIOS</h5>
                    <p>En este Catálogo podrán elegir entre diversos servicios como, entradas para eventos, Programación de planes de aventura, etc.</p>
                    <p>Todos los Servicios están sujetos a especificaciones concretas de temporada y normas de funcionamiento. Para solicitar un presupuesto es imprescindible rellenar todos los datos solicitados en la ficha de solicitud.</p>
                    <p>La petición de servicios se deberá realizar como mínimo 14 días antes de la fecha deseada.</p>
                    <p>Para realizar la petición es importante tener en cuenta que:</p>
                    <ul>
                      <li>La petición no se tramitará hasta no tener la conformidad por escrito del Participante.</li>
                      <li>Toda petición queda bajo disponibilidad, hasta el momento de confirmación del servicio en firme.</li>
                    </ul>
                    <blockquote>
                      <p>Gastos de cancelación: toda petición confirmada por el Participante y que fuese cancelada y en este proceso se generaran gastos de anulación por cualquier causa, el Participante deberá abonar los puntos o euros necesarios hasta cubrir dichos gastos de cancelación.</p>
                    </blockquote>
                    <p>El catálogo de premios publicado en la web irá variando tanto de productos como de precios según existencias en los proveedores, descatalogaciones o incorporaciones de nuevos productos.</p>
                  </li>
                </ul>
                <h4 id="siviaje">Viaje</h4>
                <div></div>
                <p>El premio final del Programa de Incentivos Yokohama Bushido es un viaje.</p>
                <p>Cualquier usuario que quiera disfrutar del viaje, debe cumplir el objetivo de facturación trimestral, lo que implica que acepta el bloqueo de su cuenta para el canjeo de regalos. Los kilómetros conseguidos durante este periodo se utilizarán para conseguir el viaje. Esta acción solo se podrá deshacer con el consentimiento explícito de Yokohama.</p>
                <p>Hasta el 31 de marzo de 2019, cada usuario tendrá la posibilidad de elegir una o dos plazas para el viaje. Hay 40 plazas disponibles para España y otras 40 para Portugal, que se concederán por orden de petición. Una vez que se seleccione el viaje, quedará bloqueado el canjeo para el resto del catálogo de regalos.</p>
                <p>El viaje podrá disfrutarse en 2020 o reservar una plaza para 2021.</p>
                <p>Condiciones:</p>
                <ul>
                  <li><strong>Facturación 2 plazas:</strong> 100.000€ de facturación donde el valor medio de compra del neumático debe ser superior a 55€. Facturación en la gama PCR (Turismo, furgoneta y 4x4).</li>
                  <br>
                  <li><strong>Facturación 1 plaza:</strong> 65.000€ de facturación donde el valor medio de compra del neumático debe ser superior a 55€.  Facturación en la gama PCR (Turismo, furgoneta y 4x4).</li>
                  <br>
                  <li>El cliente una vez seleccionado el viaje no accederá a puntos Bushido; seleccione 1 o 2 plazas.</li>
                  <br>
                  <br>
                  <li>Se realizará un corte el 1 de julio de 2019, fecha en la cual el cliente deberá haber conseguido al menos el 49% del objetivo (2 plazas = 49.000€ de facturación y 1 plaza = 31.850€ de facturación, ambos con una media mínima de 55€/neumático). En caso de no haber conseguido el objetivo marcado pasará al programa de kilómetros automáticamente sin consulta previa.</li>
                  <li>Podrá seleccionar plazas HASTA EL 31 DE MARZO 2019, a partir de entonces la opción ya no estará disponible y optará por el catálogo de forma automática.</li>
                </ul>
                <p>Si no se llegara a alcanzar el cupo destinado a cada viaje, Yokohama podrá invitar a los participantes que considere oportuno hasta cubrir las plazas libres. Las condiciones de cada viaje y los requisitos para ser invitado serán decididos por Yokohama.</p>
                       
        
                
            </div>
            
            
            <!-- Sidebar
            ============================================= -->
            <div class="sidebar nobottommargin clearfix">
                <div class="sidebar-widgets-wrap">
                    <div class="widget widget_links clearfix">

                        <h3>El programa</h3>
                        <ul>
                            <li><a href="{{baseurl}}inicio/bases"><div>Bases legales</div></a></li>
                            <li><a href="{{baseurl}}inicio/generales"><div>Condiciones generales</div></a></li>
                            <li><a href="{{baseurl}}inicio/privacidad"><div>Política de privacidad</div></a></li>
                            <li><a href="{{baseurl}}inicio/canjeo" class="active"><div>Canjeo</div></a></li>
                            <li><a href="{{baseurl}}inicio/cookies"><div>Cookies</div></a></li> 
                                
                        </ul>

                    </div>
                </div>
            </div>
            
            <!-- Sidebar END
            ============================================= -->
            
        </div>
    
    </div>

</section><!-- #content end -->
