<!-- Page Title
============================================= -->
<section id="page-title" class="header programa_header">

    <div class="container clearfix">
        <h1>Condições de troca</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
        ============================================= -->
<section id="content">
    <div class="content-wrap">
    
        <div class="container clearfix legal">
            <!-- Post Content
            ============================================= -->
            <div class="postcontent nobottommargin col_last clearfix">
                <p>Os participantes poderão optar pela troca dos quilómetros e pontos acumulados por um ou mais presentes do catálogo do programa em função dos quilómetros obtidos.<br/>
            Quando o participante realiza uma troca serão descontados os quilómetros necessários para alcançar o total de pontos do presente. Dentro do catálogo de presentes poderá encontrar produtos e serviços.</p>
                <ul>
                  <li>
                    <h5>PRODUTOS</h5>
                    <p>Antes de realizar um pedido o Participante deverá assegurar-se das caraterísticas do presente solicitado (modelo, tamanho, cor) já que uma vez finalizado o pedido não se permite a sua anulação.</p>
                    <p>O prazo de entrega dos presentes é de 30/45 dias depois de realizado o pedido. Ao receber o presente, o Participante deverá ter em conta as seguintes considerações:</p>
                    <ul>
                      <li>É da responsabilidade do Participante comprovar que o presente está em boas condições. O prazo para efetuar reclamação por avaria ou defeito é de 48 horas.</li>
                      <li>O tempo máximo de recolha de um presente em mau estado é de uma semana, excetuando Ceuta, Melilla, Baleares, Canárias e Portugal.</li>
                      <li>Não serão aceites devoluções de produtos em bom estado.</li>
                      <li>Em caso de problemas de funcionamento ou não conformidade com o presente recebido, o Participante poderá contactar o Telefone de Atendimento do Programa: +34 91 563 10 11.</li>
                    </ul>
                    <p>Os produtos têm garantia de dois anos desde a entrega; não é necessário carimbar o documento de garantia incluído. A guia de entrega constitui o documento legal para acreditar a data de entrega do produto (artigo 123RDL 1/2007).</p>
                    <p>É <strong><u>obrigatório</u></strong> guardar a guia de entrega para validar a receção do produto e tornar efetiva a garantia respetiva durante os 2 anos seguintes à sua receção, em qualquer ponto de venda com serviço técnico.</p>
                  </li>
                  <li>
                    <h5>SERVIÇOS</h5>
                    <p>Neste catálogo poderão ser elegíveis entre vários serviços, entradas para eventos, Programação de viagens de aventura, etc.</p>
                    <p>Todos os serviços estão sujeitos a especificações concretas de periodicidade e normas de funcionamento. Para solicitar um orçamento é imprescindível preencher todos os dados solicitados na ficha de solicitação.</p>
                    <p>O pedido de serviços deve ser realizado com um mínimo de 14 dias antes da data desejada.</p>
                    <p>Para realizar o pedido é importante ter em conta que:</p>
                    <ul>
                      <li>O pedido não será tratado até ser obtida e conformidade por escrito do Participante.</li>
                      <li>Todos os pedidos estão pendentes de confirmação, até ao momento da respetiva por assinatura.</li>
                    </ul>
                    <blockquote>
                      <p>Gastos de cancelamento: todos os pedidos confirmados pelo Participante e que sejam cancelados gerando custos de anulação por qualquer motivo, o Participante deverá reembolsar os pontos ou Euros necessários até serem cobertos os custos do cancelamento.</p>
                    </blockquote>
                    <p>O catálogo de prémios publicado na web irá variar a nível de produtos e preços, mediante existências nos fornecedores, descatalogados ou novos produtos.</p>
                  </li>
                </ul>
                <h4 id="siviaje">Viagens</h4>
                <div></div>
                <p>O prémio final do Programa de Incentivos Yokohama Bushido é uma viagem.</p>
                <p>Qualquer utilizador que queira disfrutar da viagem deverá cumprir o objetivo de faturação trimestral, o que implica que aceita o bloqueio da sua conta para a troca de presentes. Os quilómetros conseguidos durante este período serão utilizados para a viagem. Esta ação só poderá ser revertida com o consentimento explícito da Yokohama.</p>
                <p>Até 31 de março de 2020, cada utilizador terá a possibilidade de escolher um ou dois lugares para a viagem. Existem 40 lugares disponíveis para Espanha e 40 para Portugal, que serão atribuídos por ordem de pedido. Uma vez selecionada a viagem, a troca de KM ficará bloqueada no catálogo.</p>
                <p>El viaje podrá disfrutarse en 2020 o reservar una plaza para 2021.A viagem irá realizar-se me 2020 ou reservar lugares para 2021.</p>
                <p>Condições:</p>
                <ul>
                  <li><strong>Faturação 2 lugares:</strong> 100.000€ de faturação onde o valor médio de compra do pneu deverá ser superior a 50€. Faturação na gama PCR (Turismo, VAN e 4x4).</li>
                  <br>
                  <li><strong>Faturação 1 lugar:</strong> 65.000€ de faturação onde o valor médio de compra do pneu deverá ser superior a 50€. Faturação na gama PCR (Turismo, VAN e 4x4).</li>
                  <br>
                  <li>O cliente, uma vez selecionada a viagem deixará de aceder aos KM do Bushido; selecione 1 ou 2 lugares</li>
                  <br>
                  <br>
                  <li>Será efetuado uma contabilização a 1 de julho de 2019, data a qual o cliente deverá ter conseguido pelo menos 49% do objetivo ( 2 lugares = 49.000€ de faturação e 1 lugar = 31.850€ de faturação, ambos com um média mínima de 50€/pneu). No caso de não ter conseguido o objetivo definido passará automaticamente para o programa de KM sem aviso prévio.</li>
                  </ul>
                
        
                
            </div>
            
            
            <!-- Sidebar
            ============================================= -->
            <div class="sidebar nobottommargin clearfix">
                <div class="sidebar-widgets-wrap">
                    <div class="widget widget_links clearfix">

                        <h3>El programa</h3>
                        <ul>
                            <li><a href="{{baseurl}}inicio/bases"><div>Bases legais</div></a></li>
                            <li><a href="{{baseurl}}inicio/generales"><div>Condições Gerais</div></a></li>
                            <li><a href="{{baseurl}}inicio/privacidad"><div>Política de Privacidade</div></a></li>
                            <li><a href="{{baseurl}}inicio/canjeo" class="active"><div>Condições de troca</div></a></li>
                            <li><a href="{{baseurl}}inicio/cookies"><div>Política de cookies</div></a></li>   
                                
                        </ul>

                    </div>
                </div>
            </div>
            
            <!-- Sidebar END
            ============================================= -->
            
        </div>
    
    </div>

</section><!-- #content end -->
