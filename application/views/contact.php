
<!-- Page Title
============================================= -->
<section id="page-title" class="header contacto_header">

    <div class="container clearfix">
        <h1>{{textos.titulo}}</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content" class="bg-contacto">

    <div class="content-wrap ">

        <div class="container clearfix">
            
            

            <!-- Postcontent
            ============================================= -->
            <div class="postcontent nobottommargin">

                <h3>{{textos.entradilla}}</h3>

                <div class="contact-widget formulario_contacto">

                    <div class="contact-form-result"></div>
                <div class="contact-widget">
                    <div class="contact-form-result"></div>

                    <form class="nobottommargin" id="template-contactform" name="template-contactform" action="{{baseurl}}inicio/sendContact" method="post">

                        <div class="form-process"></div>

                        <div class="col_one_third">
                            <label for="template-contactform-name">{{textos.tabla.nombre}} <small>*</small></label>
                            <input type="text" id="template-contactform-name" name="name" value="" class="sm-form-control required" />
                        </div>

                        <div class="col_one_third">
                            <label for="template-contactform-email">{{textos.tabla.email}} <small>*</small></label>
                            <input type="email" id="template-contactform-email" name="email" value="{{user.email}}" class="required email sm-form-control" readonly="true"/>
                        </div>

                        <div class="col_one_third col_last">
                            <label for="template-contactform-phone">{{textos.tabla.tfno}}</label>
                            <input type="text" id="template-contactform-phone" name="phone" value="" class="sm-form-control" />
                        </div>

                        <div class="clear"></div>

                        <div class="col_full">
                            <label for="template-contactform-subject">{{textos.tabla.asunto}} <small>*</small></label>
                            <input type="text" id="template-contactform-subject" name="subject" value="" class="required sm-form-control" />
                        </div>

                        <div class="clear"></div>

                        <div class="col_full">
                            <label for="template-contactform-message">{{textos.tabla.msn}} <small>*</small></label>
                            <textarea class="required sm-form-control" id="template-contactform-message" name="message" rows="6" cols="30"></textarea>
                        </div>

                        <div class="col_full hidden">
                            <input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />
                        </div>

                        <div class="col_full">
                            <button class="button" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">{{textos.butt}}</button>
                        </div>

                    </form>
                </div>

                </div>
                <p>{{textos.claim|raw}}</p>

            </div><!-- .postcontent end -->
            
            
            <!-- Sidebar
            ============================================= -->
            <div class="sidebar nobottommargin col_last clearfix">
                    <div class="sidebar-widgets-wrap">

                        <div class="widget clearfix">
                            <h3><img src="{{imgurl}}logo-yokohama-dark.png" style=""></h3>
                            <br>
                            <div id="dir-sidebar">
                                
                                    <p>{{textos.dir}}<br>
                                    {{textos.complete}} <br>

                                    <p style="margin-bottom: 0;">{{textos.tfno}}<br>
                                        {{textos.email|raw}}</p>
                            </div>

                        </div>

                            <div class="widget clearfix">

				<div id="google-map1" style="height: 250px;" class="gmap"></div>				

                            </div>				

                    </div>
            </div>
            <!-- .sidebar end -->

        </div>

    </div>

</section>
