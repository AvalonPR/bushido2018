
<!-- Page Sub Menu
                ============================================= -->
<div id="page-menu" class="hidden-xs hidden-sm">

    <div id="page-menu-wrap">

        <div class="container clearfix">

            <nav id="primary-menu" class="catalogo-completo">

                <ul>
                    <li id="catalogo_menu" class="current mega-menu sub-menu"><a href="#" style="color: #fff;">{{textos.subtitulo}}<i class="icon-angle-down"></i></a>
                        <div id="catalogo_menu_in" class="mega-menu-content clearfix">
                            {%for key, j in jerarquia%}
                            <ul class="mega-menu-column col-5">
                                <li><a class="menu_white" href="{{baseurl}}productos/{{key}}">{{j.nombre}}</a></li>
                            </ul>
                            {%endfor%}
                        </div>
                    </li>
                </ul>
                        

            </nav><!-- #primary-menu end -->
        </div>
    </div>

</div><!-- #page-menu end -->

<!-- Page Title
============================================= -->
<section id="page-title" class="catalogo_header header">

    <div class="container clearfix">
        <h1>{{textos.titulo}}</h1>
    </div>

</section><!-- #page-title end -->



<!-- Content
============================================= -->
<section id="content" class="catalogo">
    <!--MIGA DE PAN-->
<div class="container">
    {%if breadcumb.categoria !='' %}
    <ul class="migapan">
            <li class="migapan-item"><a href="{{baseurl}}productos">&nbsp;{{textos.titulo}}&nbsp;</a></li>
            {% if breadcumb.categoria !='' %}
                {%if breadcumb|last == breadcumb.categoria %}
                <li  class="migapan-item active" aria-current="page" >&nbsp;{{breadcumb.categoria|capitalize}}&nbsp;</li>
                {%else%}
                <li class="migapan-item"><a href="{{baseurl}}productos/{{breadcumb.categoria}}">&nbsp;{{breadcumb.categoria|capitalize}}&nbsp;</a></li>
                {%endif%}
            {%endif%}
            {% if breadcumb.subcategoria !='' %}
                {%if breadcumb|last == breadcumb.subcategoria %}
                <li  class="migapan-item active" aria-current="page" >&nbsp;{{breadcumb.subcategoria|capitalize}}&nbsp;</li>
                {%else%}
                <li class="migapan-item"><a href="{{baseurl}}productos/{{breadcumb.categoria}}/{{breadcumb.subcategoria}}">&nbsp;{{breadcumb.subcategoria|capitalize}}&nbsp;</a></li>
                {%endif%}
            {%endif%}
            {% if breadcumb.familia !='' %}
                {%if breadcumb|last == breadcumb.familia %}
                <li  class="migapan-item active" aria-current="page" >&nbsp;{{breadcumb.familia|capitalize}}&nbsp;</li>
                {%else%}
                <li  class="migapan-item"><a href="{{baseurl}}productos/{{breadcumb.categoria}}/{{breadcumb.subcategoria}}/{{breadcumb.familia}}">&nbsp;{{breadcumb.familia|capitalize}}&nbsp;</a></li>
                {%endif%}
            {%endif%}
    </ul>
    {%endif%}
</div>

<!--FIN MIGA DE PAN-->

    <div class="content-wrap">

        <div class="container clearfix">

            <!--Busqueda catálogo mobile-->
            <form action="#" method="post" class="nobottommargin hidden-lg hidden-md">
                <div class="bottommargin-sm">
                    <label for="">{{textos.filtros}}</label>
                    <select class="select-1 form-control" style="width:100%;" id="mobileSelect">
                                {% for key, j  in jerarquia%}
                                {%set categoria = key %}
                                        {% for key, c in j.children%}
                                        {%set familia = key %}
                                        <optgroup label="{{j.nombre}}/{{c.nombre}}">
                                        <li><a href="#"></a>
                                            <ul>
                                                {% for key, g in c.children%}
                                                <option value="{{baseurl}}productos/{{categoria}}/{{familia}}/{{key}}">{{g.nombre}}</option>
                                                 {%endfor%}
                                        </optgroup>
                                            {%endfor%}
                                    {%endfor%}
                                </ul>
                    </select>
                </div>
            </form>
            <!-- Aviso no se pueden realizar pedidos
            ============================================= -->
            {% if user.tipo != 'C' %}
            <div class="alert style-msg errormsg center"><i class="material-icons">error</i><b>Comerciales y administradores no pueden canjear productos.</b></div>
            {%elseif user.viaje != 0 %}
            <div class="alert style-msg errormsg center"><i class="material-icons">error</i><b>{{textos.content.noviaje}}</b></div>            
            {%endif%}
            <!-- Post Content
            ============================================= -->



            <div class="postcontent nobottommargin col_last">

                <!-- Shop
                ============================================= -->
                
                <div id="bannerPromocion" class="col_full bottommargin-xs">
                    <a href="{{baseurl}}productos/promocion/especial/lanzamiento" target="_self">
                        <img src="{%if userData.nacionalidad!='PT' %}{{imgurl}}banners/banner-promocion-catalogo.jpg {%else%}{{imgurl}}banners/banner-promocion-catalogo-pt.jpg{%endif%}" alt="" class="img-responsive">
                    </a>
                </div>
                <div id="shop" class="shop product-3 clearfix" data-layout="fitRows">
                    {%for producto in productos%}
                    <div class="product clearfix producto">
                        <div class="product-image product-image-catalog">
                            {%set url= [producto.parent.categoria.slug, producto.parent.subcategoria.slug, producto.parent.familia.slug, producto.slug] %}
                            <a href="{{baseurl}}productos/{{producto.parent.categoria.slug}}/{{producto.parent.subcategoria.slug}}/{{producto.parent.familia.slug}}/{{producto.slug}}" class="pdt_image" style="background-image: url('https://apps.avalonprplus.com/uploads/{{producto.imagen}}')"></a>
                            {%if producto.precioDto is not null%}
                            <div class="sale-flash">{{textos.content.promo}}</div>
                            {%endif%}
                            <!--alt="{{producto.nombre}}"-->
                            <div class="product-overlay">
                                {%if producto.precioDto is not null%}
                                <a {%if user.viaje != 0 %}{%elseif user.puntos < producto.precioDto %}{%elseif user.puntos < (cart_total + producto.precioDto) %}{%else%}{%if producto.atributos is empty%}onclick="addtoCart({{producto.id}})"{%else%}onclick="sendToProduct({{url|json_encode}})"{%endif%}{%endif%} class="add-to-cart{%if user.viaje !=0 %}-disabled{%elseif user.puntos < producto.precioDto %}-disabled{%elseif user.puntos < (cart_total + producto.precioDto) %}-disabled{%endif%}"><i class="icon-shopping-cart"></i><span> {{textos.botones.addmore}}</span></a>
                                {%else%}
                                <a {%if user.viaje != 0 %}{%elseif user.puntos < producto.precio %}{%elseif user.puntos < (cart_total + producto.precio) %}{%else%}{%if producto.atributos is empty%}onclick="addtoCart({{producto.id}})"{%else%}onclick="sendToProduct({{url|json_encode}})"{%endif%}{%endif%} class="add-to-cart{%if user.viaje !=0 %}-disabled{%elseif user.puntos < producto.precio %}-disabled{%elseif user.puntos < (cart_total + producto.precio) %}-disabled{%endif%}"><i class="icon-shopping-cart"></i><span> {{textos.botones.addmore}}</span></a>
                                {%endif%}
                                <a class="under" href="#" data-toggle="modal" data-target="#{{producto.id}}"><i class="icon-zoom-in2"></i><span> {{textos.botones.vista}}</span></a>
                            </div>
                        </div>
                        <div class="product-data col-md-12 col-sm-12 col-xs-12 bottommargin-sm">
                        
                            <a href="{{baseurl}}productos/{{producto.parent.categoria.slug}}/{{producto.parent.subcategoria.slug}}/{{producto.parent.familia.slug}}/{{producto.slug}}"><h4>{{producto.nombre}}</h4></a>
                        
                            <p class="precio-home"><i class="fas fa-angle-double-right"></i>{%if producto.precioDto is not null%}<del>{{producto.precio|number_format(0, ',', '.')}} Km</del> <ins>{{producto.precioDto|number_format(0, ',', '.')}} Km</ins> {%else%} {{producto.precio|number_format(0, ',', '.')}} Km{% endif%}</p>

                        </div>
                    </div>
                    {%endfor%}

                </div><!-- #shop end -->

            </div><!-- .postcontent end -->


            <!--Sidebar
            ============================================= -->
            <div class="sidebar nobottommargin hidden-sm hidden-xs">
                <div class="sidebar-widgets-wrap">

                    <div id="catalogoMenuLat" class="widget widget_links clearfix">

                        <h4 class="notopmargin">{{textos.cats}}</h4>
                        <nav class="nav-tree nobottommargin nav-tabs">
                            <ul class="menu-jerarquia">
                                {% for key, j  in jerarquia%}
                                {%set categoria = key %}
                                <li{%if categoria == breadcumb.categoria%} class="active" {%endif%}><a href="#">{{j.nombre}}</a>
                                    <ul {%if categoria == breadcumb.categoria %}style="display: block;"{%endif%}>
                                        {% for key, c in j.children%}
                                        {%set familia = key %}
                                        <li{%if familia == breadcumb.subcategoria%} class="active" {%endif%}><a href="#">{{c.nombre}}</a>
                                            <ul {%if familia == breadcumb.subcategoria %}style="display: block;"{%endif%}>
                                                {% for key, g in c.children%}
                                                <li class="{%if key == breadcumb.familia%} active {%endif%} side-submenu-catalogo"><a href="{{baseurl}}productos/{{categoria}}/{{familia}}/{{key}}">{{g.nombre}}</a></li>
                                                 {%endfor%}
                                            </ul>
                                        </li>
                                        {%endfor%}
                                    </ul>
                                </li>
                                {%endfor%}
                            </ul>
                        </nav>

                        </div>

                    </div>
                </div> <!--.sidebar end -->

            </div>

        </div>

    </section><!-- #content end -->

    {%for producto in productos%}
    <!-- Modal -->
    <div class="modal fade" id="{{producto.id}}" tabindex="-1" role="dialog" aria-labelledby="Modalproduct-{{producto.id}}" aria-hidden="true">

        <div class="modal-dialog ">

            <div class="modal-body col-sm-12 col-xs-12 producto-modal">
                <button class="close" type="button" data-dismiss="modal" aria-hidden="true"><b>x</b></button>
                <div class="single-product clearfix">

                    <div class="product-modal-title">
                        <h2>{{producto.nombre}}</h2>
                    </div>

                    <div class="product modal-padding clearfix">

                        <div class="col_half nobottommargin">
                            <div class="product-image">
                                <a href="#" title="#"><img src="https://apps.avalonprplus.com/uploads/{{producto.imagen}}" alt="{{producto.nombre}}"></a></div>

                        </div>
                        <div class="col_half nobottommargin col_last product-desc">
                            <div class="product-price product-price-modal">{%if producto.precioDto is not null%}<del>{{producto.precio|number_format(0, ',', '.')}} </del> <ins>{{producto.precioDto|number_format(0, ',', '.')}} </ins> {%else%}<ins> {{producto.precio|number_format(0, ',', '.')}} </ins> {% endif%}</div>
                            <div class="clear"></div>
                            <div class="line"></div>
                            
                            <!-- Product Modal - Tallas y colores
                                ============================================= -->
                            
                            <input type="hidden" value="{{producto.unidades}}" id="unidades">
                   {%if productos.atributos is not empty%}
                            {%for k,atributo in productos.atributos%}
                                <!-- Product Single - Quantity & Cart Button
                                ============================================= -->
                                {%if k=='2'%}
                                <p class="">{{textos.item.talla}}</p>
                                <div class="col_full talla grid-container" data-layout="fitRows">
                                    
                                    {%for a in atributo%}
                                    <div class="size-item fleft">
                                        <input onclick="selectSize('size{{a}}')" data-id="" type="button" class="clearfix" value="{{a}}" id="size{{a}}">
                                        <div class="quantity-size" id="divsize{{a}}" style="display:none;">
                                            
                                                <input type="button" value="-" class="minus size-minus">
                                                <input type="text" step="1" min="1" value="1" title="Qty" class="qty" name="qtysize[]" size="4" />
                                                <input type="button" value="+" class="plus size-plus">
                                            
                                        </div>
                                    </div>
                                    {%endfor%}
                                </div>
                                <div class="line"></div>
                                {%endif%}
                                {%if k=='1'%}
                                <p class="">{{textos.item.color}}</p>
                                <div class="col_full color_ grid-container" data-layout="fitRows">
                                    {%for a in atributo%}
                                    <input type="button" class="color-item fleft" style="background-color: #{{a}};">
                                    {%endfor%}
                                </div>
                                {%endif%}
                            {%endfor%}
                        {%endif%}
                        
                        <!-- Product Modal - Fin Tallas y colores
                                ============================================= -->
                            

                            <!-- Product Single - Quantity & Cart Button
                            ============================================= -->
                                <div class="quantity">
                                    <input type="button" value="-" class="minus">
                                    <input type="text" step="1" min="1" name="quantity" value="1" title="Qty" class="qty" size="4" id="quantity{{producto.id}}"/>
                                    <input type="button" value="+" class="plus">
                                </div>   
                            {%set url= [producto.parent.categoria.slug, producto.parent.subcategoria.slug, producto.parent.familia.slug, producto.slug] %} 
                        {%if producto.precioDto is not null%}
                        <button onclick="addtoCart({{producto.id}})" class="single_product_add add-to-cart{%if user.viaje != 0 %}-disabled{%elseif user.puntos < producto.precioDto %}-disabled{%elseif user.puntos < (cart_total + producto.precioDto) %}-disabled{%endif%} button nomargin fright" {%if user.viaje != 0 %}disabled{%elseif user.puntos < producto.precioDto %}disabled{%elseif user.puntos < (cart_total + producto.precioDto) %}disabled{%endif%} >{{textos.botones.addmore}}</button>
                        {%else%}
                        <button onclick="addtoCart({{producto.id}})" class="single_product_add add-to-cart{%if user.viaje != 0 %}-disabled{%elseif user.puntos < producto.precio %}-disabled{%elseif user.puntos < (cart_total + producto.precio) %}-disabled{%endif%} button nomargin fright" {%if user.viaje != 0 %}disabled{%elseif user.puntos < producto.precio %}disabled{%elseif user.puntos < (cart_total + producto.precio) %}disabled{%endif%} >{{textos.botones.addmore}}</button>
                        {%endif%}

                            <!-- Product Single - Quantity & Cart Button End -->

                            <div class="clear"></div>
                            <div class="line"></div>
                            <p>{{producto.descripcion|raw}}</p>
                        </div>
                    </div>

                </div>

            </div>


            <!--  Fin modal -->
        </div>
    </div>

    {%endfor%}
