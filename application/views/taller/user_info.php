
<!-- Page Title
============================================= -->
<section id="page-title" class="usuarios_header">

    <div class="container clearfix">
        <h1>Carla Rodríguez</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>Marca de vehículo</th>
                    <td>BMW</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>Modelo de vehículo</th>
                    <td>Serie 1 cinco puertas</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>Visitas al taller</th>
                    <td><span class="contitrade" style="text-transform: uppercase;"><i class="material-icons" style="font-size: 16px;">av_timer</i> 23/05/2017</span> </td>
                    <td><b>Motivo:</b>  Loren Ipsum dolor sit amet, consectetur adipisicing elit.</td>
                    <td class="click"><i class="material-icons contitrade" style="font-size: 16px;">label</i> <a style="display: initial;" href="{{siteurl}}/taller/campanas/item"> Promoción Decathlon</a></td>
                    <td><b>Importe del regalo:</b> 50 puntos</td>
                </tr>
                <tr>
                    <th></th>
                    <td><span class="contitrade" style="text-transform: uppercase;"><i class="material-icons" style="font-size: 16px;">av_timer</i> 23/05/2017</span> </td>
                    <td><b>Motivo:</b>  Loren Ipsum dolor sit amet, consectetur adipisicing elit.</td>
                    <td class="click"><i class="material-icons contitrade" style="font-size: 16px;">label</i> <a style="display: initial;" href="{{siteurl}}/taller/campanas/item"> Promoción Decathlon</a></td>
                    <td><b>Importe del regalo:</b> 50 puntos</td>

                </tr>
                <tr>
                    <th></th>
                    <td><span class="contitrade" style="text-transform: uppercase;"><i class="material-icons" style="font-size: 16px;">av_timer</i> 23/05/2017</span> </td>
                    <td><b>Motivo:</b>  Loren Ipsum dolor sit amet, consectetur adipisicing elit.</td>
                    <td class="click"><i class="material-icons contitrade" style="font-size: 16px;">label</i> <a style="display: initial;" href="{{siteurl}}/taller/campanas/item"> Promoción Decathlon</a></td>
                    <td><b>Importe del regalo:</b> 50 puntos</td>
                </tr>

                <tr>
                    <th>Dirección usuario</th>
                    <td>Calle del Álamo 10, Madrid</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>Número de contacto</th>
                    <td>6885233695 / 915236985</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td>j.carlos@gmail.com</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

            </table>

        </div>

    </div>

</section><!-- #content end -->

<!--
                                                <td><div class="toggle">
                                                <div class="togglet"><i class="material-icons">add_circle</i><i class="material-icons">remove_circle</i>Más información</div>
                                                <div class="togglec">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
                                                </div>
                                          </td>-->

