<section id="slider" class="slider-parallax swiper_wrapper clearfix" data-autoplay="7000" data-speed="650" data-loop="true">

    <div class="slider-parallax-inner">

        <div class="swiper-container swiper-parent">
            <div class="swiper-wrapper">
                {%for slider in sliders%}
                <div class="swiper-slide dark" style="background-image: url('{{imgurl}}sliders/{{slider.id}}/{{slider.imagen}}');">
                    <div class="container clearfix">
                        <div class="slider-caption slider-caption-center">
                            <h2 data-caption-animate="fadeInUp">{{slider.titulo|raw}}</h2>
                            <p data-caption-animate="fadeInUp" data-caption-delay="200">{{slider.subtitulo|raw}}</p>
                            <a data-caption-animate="fadeInUp" href="{{slider.url}}" class="button">{{slider.boton}}</a>
                        </div>
                    </div>
                </div>
                {%endfor%}
            </div>
            <div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
            <div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
        </div>

    </div>

</section>



<!-- Content
============================================= -->
<section id="content">

    <div class="nobottompadding">
        
        {% if user.viaje != 1 and user.viaje !=2 %}
        <div class="fondo-productos">
        <div class="container clearfix">
            <div class="col-lg-5 col-md-6 hidden-sm hidden-xs" style="margin-top: -20px;">
                <img class="samurai-home" alt="samurai" src="{{imgurl}}img-catalogo-paint.png"/>
            </div>
            
            <div class="col-lg-7 col-md-6 col-sm-12 col-xs-12 buscador">
                <div class="clear bottommargin-lg hidden-xs"></div>
                <h2>{{textos.buscador.titulo}}</h2>
                <p>{{textos.buscador.subtitulo}}</p>
                <div class="line-custom bottommargin-sm"></div>
                <form>
                    <select>
                        <option value="" disabled selected hidden>{{textos.buscador.categoria.label}}</option>
                        {%for key, j in jerarquia%}
                        <ul class="mega-menu-column col-5">
                            <option value="{{j.nombre}}"><a class="menu_white" href="{{baseurl}}productos/{{key}}">{{j.nombre}}</a></option>
                        </ul>
                        {%endfor%}
                    </select>
                    <select>
                        <option value="" disabled selected hidden>{{textos.buscador.subcategoria.label}}</option>

                        {%for key, j in jerarquia%}
                                        {% for key, c in j.children%}
                        <option value="{{c.nombre}}">{{c.nombre}}</option>
                        {%endfor%}
                        {%endfor%}
                    </select>
                    <select style="margin-right: 0;">
                        <option value="" disabled selected hidden>{{textos.buscador.familia.label}}</option>
                            {%for key, j in jerarquia%}
                                        {% for key, c in j.children%}
                                                {% for key, g in c.children%}
                        <option value="{{g.nombre}}">{{g.nombre}}</option>
                        {%endfor%}
                            {%endfor%}
                        {%endfor%}
                    </select>
                    <input type="submit" value="BUSCAR"/>
                </form>
            </div>
            
        </div>
        
        
        
        <div class="container clearfix">


            <div class="row galeria">
                {%for producto in productos%}
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 producto">
                    <div class="product-image product-image-catalog">
                            {%set url= [producto.parent.categoria.slug, producto.parent.subcategoria.slug, producto.parent.familia.slug, producto.slug] %}
                            <a href="{{base_url()}}productos/{{producto.parent.categoria.slug}}/{{producto.parent.subcategoria.slug}}/{{producto.parent.familia.slug}}/{{producto.slug}}" class="pdt_image" style="background-image: url('https://apps.avalonprplus.com/uploads/{{producto.imagen}}')"></a>
                                                        
                    </div>
                    <div class="product-data col-md-12 col-sm-12 col-xs-12 bottommargin-sm">
                        
                        <a href="{{base_url()}}productos/{{producto.parent.categoria.slug}}/{{producto.parent.subcategoria.slug}}/{{producto.parent.familia.slug}}/{{producto.slug}}"><h4>{{producto.nombre}}</h4></a>
                        
                        <p class="precio-home"><i class="fas fa-angle-double-right"></i>{%if producto.precioDto is not null%}<del>{{producto.precio|number_format(0, ',', '.')}} Km</del> <ins>{{producto.precioDto|number_format(0, ',', '.')}} Km</ins> {%else%} {{producto.precio|number_format(0, ',', '.')}} Km{% endif%}</p>

                    </div>
                </div>
                {%endfor%}
            </div>
            

        </div>
        </div>
        {%endif%}
        <div class="promo promo-dark promo-full nobottommargin header-stick notopborder" style="overflow: hidden;">
            <div class="container clearfix homes" >
                <div class="promoText col-md-7 col-xs-12 topmargin-sm">
                    <h3>{{textos.viaje.titulo}}</h3>
                    <div class="line-custom"></div>
                    <span>
                        <p>{{textos.viaje.subtitulo}}</p>
                        <div class="clear"></div>
                        <a href="{{base_url()}}inicio/viaje" class="button">{{textos.viaje.boton}} <i class="material-icons">flight_takeoff</i></a>
                    </span>
                </div>
                <div class="promoImg col-md-5 col-xs-12">
                        <img src="{{imgurl}}barco-viaje-yokohama-2019.png" data-animate="fadeInUp" data-delay="100" alt="el gran viaje está a punto de empezar">
		
                </div>
            </div>
        </div>
          

        <div class="parallax light nobottommargin" id="testimonial-taller" style="padding: 100px 0px; background-position: 50% 216.6px;" data-stellar-background-ratio="0.4">

            <div class="fslider testimonial testimonial-full" data-animation="fade" data-arrows="false">
                <div class="flexslider" style="height: 227px;">
                    <div class="slider-wrap">
                        <div class="slide" data-thumb-alt="" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
                            <div class="testi-content">
                                <div class="testi-meta">
                                {{textos.secciones.entrada}}                           
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="trayectoria-home">
        <div class="container clearfix bg-trayectoria nobottommargin">
            <div>
                <div id="justicia" class="steps">
                    <span data-toggle="tooltip" data-placement="top" title="Justicia">Gi</span>
                    <span class="km">820 km</span>
                    <img src="{{imgurl}}trayectoria/steps-trayectoria-01.png" alt="Gi, Justicia">
                </div>
                <div id="valor" class="steps">
                    <span data-toggle="tooltip" data-placement="top" title="Valor">Yu</span>
                    <span class="km">1.640 km</span>
                    <img src="{{imgurl}}trayectoria/steps-trayectoria-02.png" alt="Yu, Valor">
                </div>
                <div id="compasion" class="steps">
                    <span data-toggle="tooltip" data-placement="top" title="Compasión">Jin</span>
                    <span class="km">2.460 km</span>
                    <img src="{{imgurl}}trayectoria/steps-trayectoria-03.png" alt="Gi, Justicia">
                </div>
                <div id="cortesia" class="steps">
                    <span data-toggle="tooltip" data-placement="top" title="Cortesía">Rei</span>
                    <span class="km">3.280 km</span>
                    <img src="{{imgurl}}trayectoria/steps-trayectoria-04.png" alt="Gi, Justicia">
                </div>
                <div id="sinceridad" class="steps">
                    <span data-toggle="tooltip" data-placement="top" title="Sinceridad">Makoto</span>
                    <span class="km">4.100 km</span>
                    <img src="{{imgurl}}trayectoria/steps-trayectoria-05.png" alt="Gi, Justicia">
                </div>
                <div id="honor" class="steps">
                    <span data-toggle="tooltip" data-placement="top" title="Honor">Meyo</span>
                    <span class="km">4.920 km</span>
                    <img src="{{imgurl}}trayectoria/steps-trayectoria-06.png" alt="Gi, Justicia">
                </div>
                <div id="lealtad" class="steps">
                    <span data-toggle="tooltip" data-placement="top" title="Lealtad">Chu</span>
                    <span class="km">5.900 km</span>
                    <img src="{{imgurl}}trayectoria/steps-trayectoria-07.png" alt="Gi, Justicia">
                </div>
            </div>
        </div>    
        </div>
        <div class="promo promo-dark promo-full nobottommargin">
            <div class="container clearfix center">
                <h3>{{textos.secciones.trayectoria.label}}</h3>
                
                <a class="btn-trayectoria" href="{{textos.secciones.trayectoria.url}}">{{textos.secciones.trayectoria.boton}}  </a>
            </div>
        </div>

        
    </div>

</section><!-- #content end -->

{%if popups is not null and user.popup == 0%}
{%for popup in popups%} 
<section id="content" onclick="popupViewed();">
    <div class="content-wrap" onclick="popupViewed();">

        <div class="container clearfix" onclick="popupViewed();">

            <div class="modal-on-load" data-target="#myModalHome" onclick="popupViewed();"></div>

            <!-- Modal -->
            <div class="modal1 mfp-hide" id="myModalHome">
                <div class="block divcenter" style="background-color: #FFF; max-width: 500px;">
                    <div class="center" style="padding: 2%;">
                        <a href="#" id="closemodalini" onClick="$.magnificPopup.close(); popupViewed(); return false;">&times;</a>
                        <h3>{{popup.titulo}}</h3> 
                        {%if popup.url != ''%}<a href="{{popup.url}}" {%if 'mailto' not in popup.url%}target="_blank"{%endif%}>{%endif%}
                                                 <img src="{{imgurl}}pop-up/{{popup.id}}/{{popup.imagen}}" alt="{{popup.descripcion}}">
                            {%if popup.url != ''%}</a>{%endif%}
                    </div>
                    <div class="section center nomargin" style="padding: 30px;">
                        <a href="#" class="button" onClick="$.magnificPopup.close(); popupViewed(); return false;">Ok</a>
                    </div>
                </div>
            </div>

        </div>

    </div>

</section>

{%endfor%}
{%endif%}