<!-- Page Title
        ============================================= -->
<section id="page-title" class="header trayectoria_header">

    <div class="container clearfix">
        <h1>{{textos.camino.tabtitulo}}</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
        ============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">
            <h3 style="margin: 0;" >{{textos.camino.tabtitulo}}</h3>
            <div style="margin: 1% 0 4% 0;" class="line-custom"></div>
            <div class="trayectoria">
                <div class="container clearfix bg-trayectoria nobottommargin">
                    
                    <div id="justicia" class="steps">
                        <span data-toggle="tooltip" data-placement="top" title="{{textos.camino.content.gi.traduccion}}">Gi</span>
                        <span class="km">820 km</span>
                        <img src="{{imgurl}}trayectoria/steps-trayectoria-01.png" alt="Gi, {{textos.camino.content.gi.traduccion}}">
                    </div>
                    <div id="valor" class="steps">
                        <span data-toggle="tooltip" data-placement="top" title="{{textos.camino.content.yu.traduccion}}">Yu</span>
                        <span class="km">1.640 km</span>
                        <img src="{{imgurl}}trayectoria/steps-trayectoria-02.png" alt="Yu, {{textos.camino.content.yu.traduccion}}">
                    </div>
                    <div id="compasion" class="steps">
                        <span data-toggle="tooltip" data-placement="top" title="{{textos.camino.content.jin.traduccion}}">Jin</span>
                        <span class="km">2.460 km</span>
                        <img src="{{imgurl}}trayectoria/steps-trayectoria-03.png" alt="Gi, {{textos.camino.content.jin.traduccion}}">
                    </div>
                    <div id="cortesia" class="steps">
                        <span data-toggle="tooltip" data-placement="top" title="{{textos.camino.content.rei.traduccion}}">Rei</span>
                        <span class="km">3.280 km</span>
                        <img src="{{imgurl}}trayectoria/steps-trayectoria-04.png" alt="Gi, {{textos.camino.content.rei.traduccion}}">
                    </div>
                    <div id="sinceridad" class="steps">
                        <span data-toggle="tooltip" data-placement="top" title="{{textos.camino.content.makoto.traduccion}}">Makoto</span>
                        <span class="km">4.100 km</span>
                        <img src="{{imgurl}}trayectoria/steps-trayectoria-05.png" alt="Gi, {{textos.camino.content.makoto.traduccion}}">
                    </div>
                    <div id="honor" class="steps">
                        <span data-toggle="tooltip" data-placement="top" title="{{textos.camino.content.meyo.traduccion}}">Meyo</span>
                        <span class="km">4.920 km</span>
                        <img src="{{imgurl}}trayectoria/steps-trayectoria-06.png" alt="Gi, {{textos.camino.content.meyo.traduccion}}">
                    </div>
                    <div id="lealtad" class="steps">
                        <span data-toggle="tooltip" data-placement="top" title="{{textos.camino.content.chu.traduccion}}">Chu</span>
                        <span class="km">5.900 km</span>
                        <img src="{{imgurl}}trayectoria/steps-trayectoria-07.png" alt="Gi, {{textos.camino.content.chu.traduccion}}">
                    </div>
                    
                    
                </div>     
            </div>
            
        </div>
            <div class="promo promo-dark promo-full bottommargin-sm">
            <div class="container clearfix center">
                <h3>{{textos.camino.titulo}}</h3>
            </div>
        </div>
        
        
            
        <div class="container clearfix">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="col-lg-6 col-sm-12">
                    <img src="{{imgurl}}trayectoria/bushido/virtudes-samurai-01.png">
                </div>
                <div class="col-lg-6 col-sm-12">
                    <h4><span class="virtudes">GI</span> / {{textos.camino.content.gi.titulo}}</h4>
                    <p>{{textos.camino.content.gi.texto}}</p>
                </div>
            </div>
            
            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="col-lg-6 col-sm-12">
                    <img src="{{imgurl}}trayectoria/bushido/virtudes-samurai-02.png">
                </div>
                <div class="col-lg-6 col-sm-12">
                    <h4><span class="virtudes">YU</span> / {{textos.camino.content.yu.titulo}}</h4>
                    <p>{{textos.camino.content.yu.texto}}</p>
                </div>
            </div>
            
            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="col-lg-6 col-sm-12">
                    <img src="{{imgurl}}trayectoria/bushido/virtudes-samurai-03.png">
                </div>
                <div class="col-lg-6 col-sm-12">
                    <h4><span class="virtudes">JIN</span> / {{textos.camino.content.jin.titulo}}</h4>                    
                    <p>{{textos.camino.content.jin.texto}}</p>
                </div>
            </div>
            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="col-lg-6 col-sm-12">
                    <img src="{{imgurl}}trayectoria/bushido/virtudes-samurai-04.png">
                </div>
                <div class="col-lg-6 col-sm-12">
                    <h4><span class="virtudes">REI</span> / {{textos.camino.content.rei.titulo}}</h4>
                    <p>{{textos.camino.content.rei.texto}}</p>
                </div>
            </div>
            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="col-lg-6 col-sm-12">
                    <img src="{{imgurl}}trayectoria/bushido/virtudes-samurai-05.png">
                </div>
                <div class="col-lg-6 col-sm-12">
                    <h4><span class="virtudes">MAKOTO</span> / {{textos.camino.content.makoto.titulo}}</h4>
                    <p>{{textos.camino.content.makoto.texto}}</p>

                </div>
            </div>
            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="col-lg-6 col-sm-12">
                    <img src="{{imgurl}}trayectoria/bushido/virtudes-samurai-06.png">
                </div>
                <div class="col-lg-6 col-sm-12">
                    <h4><span class="virtudes">MEYO</span> / {{textos.camino.content.meyo.titulo}}</h4>
                    <p>{{textos.camino.content.meyo.texto}}</p>
                </div>
            </div>
            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="col-lg-6 col-sm-12">
                    <img src="{{imgurl}}trayectoria/bushido/virtudes-samurai-07.png">
                </div>
                <div class="col-lg-6 col-sm-12">
                    <h4><span class="virtudes">CHU</span> / {{textos.camino.content.chu.titulo}}</h4>
                    <p>{{textos.camino.content.chu.texto}}</p>
                </div>
            </div>
        </div>
            

            
            
            
            
             
  
    </div>

</section><!-- #content end -->

