<?php
include 'header.php';
?>
<!-- Page Title
        ============================================= -->
<section id="page-title" class="formacion_header">

    <div class="container clearfix">
        <h1>Formación</h1>
        <span>Datos de tus cursos realizados</span>
    </div>

</section><!-- #page-title end -->

<!-- Content
        ============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">
            <h2>Mis formaciones</h2>
            <div class="line-custom"></div>
            {%if formaciones is not null%}
            <div class="table-responsive">
                <table id="estadisticas_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Nombre formación</th>
                        </tr>
                    </thead>
                    <tbody>
                        {%for formacion in formaciones%}
                        <tr>
                            <td>{{formacion.fecha}}</td>
                            <td>{{formacion.nombre}}</td>
                        </tr>
                        {%endfor%}
                    </tbody>
                </table>
            </div>
            {%else%}
            <div class="alert style-msg errormsg center"><p><i class="material-icons">error</i>No has acudido a ninguna formación todavía, acude a nuestras formaciones y gana puntos extra en tus compras.</p></div>
            {%endif%}
            
            <div class="line"></div>
            
            <h2>Próximas formaciones</h2>
            <div class="line-custom"></div>
            <div class="table-responsive">
                <table id="ProxFormTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Nombre formación</th>
                        </tr>
                    </thead>
                    <tbody>
                        {%for formacion in proxForms%}
                        <tr>
                            <td>{{formacion.fecha}}</td>
                            <td>{{formacion.nombre}}</td>
                        </tr>
                        {%endfor%}
                    </tbody>
                </table>
            </div>

            <!--Fin tabla datos-->
        </div>
    </div>

</section><!-- #content end -->
<?php
include 'footer.php'
?>