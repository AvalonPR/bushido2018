<!-- Page Title
============================================= -->
<section id="page-title" class="pedidos-taller">

    <div class="container clearfix">
        <h1>Pedidos anteriores</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">
            <div class="col_full clearfix">
            <a class="fright button button-rounded button-yellow" href="{{siteurl}}inicio/pedidos">Volver a pedidos</a>
            </div>

            <!--Tabla pedidos-->
            <div class="row">
                <div class="table-responsive">
                    <table id="pedidos_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Número</th>
                                <th>Fecha</th>
                                <th>Usuario</th>
                                <th>Dirección</th>
                                <th>Estado</th>
                                <th>Artículos</th>
                                <th>Km totales</th>
                            </tr>
                        </thead>
                        <tbody>
                            {% for pedido in pedidos%}
                            <tr>
                                <td class="click">{{ pedido.fecha|date("Y") }}-{{pedido.id}}</td>
                                <td>{{pedido.fecha}}</td>
                                <td class="click"><a href="{{siteurl}}inicio/pedidos" target="_blank">{{pedido.usuario}}</td>
                                <td>{{pedido.direccion}}, {{pedido.poblacion}}, {{pedido.provincia}} - {{pedido.cp}}</td>
                                <td>Entregado</td>
                                <td class="click center"><button class="button" data-toggle="modal" data-target="#{{pedido.id}}"><i class="material-icons">add_circle</i></button></td>
                                <td class="click center" >{{pedido.kmtotal}}</td>
                            </tr>
                            {%endfor%}
                        </tbody>
                    </table>
                </div>
            </div>
            <!--Fin tabla pedidos-->
            {%for pedido in pedidos%}
            <!--Modal pedidos-->
            <div class="modal fade" id="{{pedido.id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-body">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="myModalLabel">Artículos del pedido <span class="contitrade"> {{pedido.id}}</span></h4>
                            </div>
                            <div class="modal-body">
                                <table class="table table-striped activas table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="col-md-8">Artículo</th>
                                            <th class="col-md-1">Cantidad</th>
                                            <th class="col-md-2">Puntos</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {%for linea in pedido.pedido%}
                                        <tr>
                                            <td class="click">{{linea.nombre}}</td>
                                            <td>{{linea.uds}}</td>
                                            <td>{{linea.kmx}}</td>
                                        </tr>
                                        {%endfor%}
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Fin modal pedidos-->
            {%endfor%}
            
        </div>

    </div>

</section><!-- #content end -->
