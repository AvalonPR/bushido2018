<!--  ======================Page Title======================= -->
<section id="page-title" class="header perfil_header">

    <div class="container clearfix">
        <h1>{{textos.datos.titulo}}</h1>
    </div>

</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">


            <div class="row clearfix">


                <div class="col-sm-12">
                    <div class="col-md-3">

                        {% if user.avatar is not null %}

                        <div class="imgav" style="max-width: 225px;"><span width="220px" height="220px" id="avatarImg" style="background-image: url({{imgurl}}avatares/{{user.cod_user}}/{{user.avatar}});" class="alignleft img-circle img-thumbnail notopmargin nobottommargin" ></span></div>
                        {%else%}
                        <div class="imgav"><img src="{{imgurl}}avatares/avatar.jpg" class="alignleft img-circle img-thumbnail notopmargin nobottommargin" style="max-width: 225px;"></div>
                        {%endif%}

                        <div class="clear bottommargin-sm"></div>

                        <div class="contact-widget">
                            <div class="contact-form-result"></div>
                            <form id="avatarForm" name="frmava" class="nomargin" action="{{base_url()}}inicio/changeAva" method="post" enctype="multipart/form-data">
                                <div>
                                    <label>{{textos.datos.avatar.cambiar}}</label><br>
                                    <input id="input-1" type="file" class="file" accept="image" name="cambiarAvatar" required>
                                    {{textos.datos.avatar.imagen}}: 220px x 220px 
                                </div>  
                            </form>
                        </div>

                    </div>
                    <div class="col-md-9">
                        <div class="heading-block noborder bottommargin-sm">
                            <h3 id="user-name">{{user.nombre}}</h3>
                            <span>{{textos.datos.tittabla}}</span>
                        </div>

                        <div class="style-msg errormsg">
                            <div class="sb-msg"><i class="icon-remove-sign"></i>{{textos.datos.info}}</div>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        </div>
                        <div class="col_half bottommargin-sm">
                            <label for="billing-form-name">{{textos.datos.tabla.razon}}:</label>
                            <input type="text" id="billing-form-name" name="name" value="{{user.nombre|capitalize}}" class="sm-form-control" readonly/>
                        </div>

                        <div class="col_half col_last bottommargin-sm">
                            <label for="billing-form-lname">{{textos.datos.tabla.nif}}:</label>
                            <input type="text" id="billing-form-lname" name="cif" value="{{user.nif}}" class="sm-form-control" readonly/>
                        </div>
                        <div class="col_two_third bottommargin-sm">
                            <label for="billing-form-name">{{textos.datos.tabla.email}}:</label>
                            <input type="text" id="billing-form-name" name="name" value="{{user.email}}" class="sm-form-control" readonly/>
                        </div>

                        <div class="col_one_third col_last bottommargin-sm">
                            <label for="billing-form-lname">{{textos.datos.tabla.tfno}}:</label>
                            <input type="number" id="billing-form-email" name="phone" value="{{user.telefono}}" class="sm-form-control" readonly/>
                        </div>


                    </div>


                    <div class="clear bottommargin-sm"></div>

                    <div class="col-md-12 clearfix">
                        <div class="fancy-title title-dotted-border">
                            <h3 class="notopmargin">{{textos.datos.direcciones}}</h3>
                        </div>

                        <div class="col_half">
                            <h4 class="notopmargin"><i class="fas fa-angle-double-right bushido"></i>&nbsp;{{textos.datos.direccion}}</h4>

                            {% if direccionesEnvio is not null%}


                            <div class="accordion clearfix">

                                {% for dirEnvio in direccionesEnvio%}
                                <div class="acctitle"><i class="acc-closed fas fa-map-marker-alt bushido"></i><i class="acc-open icon-remove-circle bushido"></i>{{dirEnvio.direccion}}</div>
                                <div class="acc_content clearfix">{{dirEnvio.poblacion}}, {{dirEnvio.provincia}}, {{dirEnvio.cp}} TFNO:{{dirEnvio.tfno}}</div>

                                {% endfor %}
                            </div>

                            {% else %}
                            <div class="accordion clearfix">

                                <div class="acctitle"><i class="acc-closed fas fa-map-marker-alt bushido"></i><i class="acc-open icon-remove-circle bushido"></i>{{textos.datos.nodir}}</div>
                                <div class="acc_content clearfix">{{textos.datos.nodirdetails}}</div>

                            </div>
                            {% endif %}

                            <h4 class="notopmargin bottommargin-xs"><i class="fas fa-angle-double-right bushido"></i>Añadir nueva dirección de envío</h4>

                            <div class="contact-widget">
                                <div class="contact-form-result"></div>
                                <form id="billing-form" name="billing-form" class="nobottommargin" action="{{baseurl}}/inicio/saveDir" method="post">

                                    <div class="col_full bottommargin-xs">
                                        <label for="billing-form-companyname">{{textos.datos.tabla.dir}}</label>
                                        <input type="text" id="billing-form-companyname" name="direccion" class="sm-form-control" />
                                    </div>

                                    <div class="col_half bottommargin-xs">
                                        <label for="billing-form-address">{{textos.datos.tabla.pob}}:</label>
                                        <input type="text" id="billing-form-address" name="poblacion" class="sm-form-control" />
                                    </div>
                                    <div class="col_half bottommargin-xs col_last">
                                        <label for="billing-form-address">{{textos.datos.tabla.prov}}:</label>
                                        <input type="text" id="billing-form-address" name="provincia"  class="sm-form-control" />
                                    </div>
                                    <div class="col_half bottommargin-xs">
                                        <label for="billing-form-address">{{textos.datos.tabla.tfno}}:</label>
                                        <input type="text" id="billing-form-address" name="tfno" class="sm-form-control" />
                                    </div>
                                    <div class="col_half bottommargin-xs col_last">
                                        <label for="billing-form-address">{{textos.datos.tabla.cp}}:</label>
                                        <input type="text" id="billing-form-address" name="cp" class="sm-form-control" />
                                    </div>


                                    <div class="fright">
                                        <input type="submit" class="button button-rounded button-dark topmargin-sm fright" name="guardarDir" onclick='saveNewDir("{{user.iduser}}")' style="width: 100%;">
                                    </div>
                                </form>
                            </div>

                        </div>

                        <div class="col_half col_last">
                            <h4 class="notopmargin"><i class="fas fa-angle-double-right bushido"></i>&nbsp;Dirección de facturación</h4>

                            <div class="accordion clearfix" id="dirFacturacion">
                                {% for direccion in direcciones %}
                                <div class="acctitle"><i class="acc-closed fas fa-map-marker-alt bushido"></i><i class="acc-open icon-remove-circle bushido"></i>{{direccion.Calle}}</div>
                                <div class="acc_content clearfix">{{direccion.Ciudad}}, {{direccion.CP}}. {{direccion.Pais}}</div>
                                {% endfor %}
                            </div>
                        </div>
                        <div class="line"></div>
                        <div class="contact-widget">
                            <div class="contact-form-result"></div>
                            <h3 id="user-name">{{textos.datos.pass.cambiar}}</h3>

                            <div class="col_full">{{textos.datos.pass.entradilla}}</div>  
                            <div class="col_half">
                                <label for="billing-form-email">{{textos.datos.pass.new}}</label>
                                <input type="password" name="newpass" id="newpass" placeholder="******" class="sm-form-control"/>
                            </div>
                            <div class="col_half col_last">
                                <label for="billing-form-city">{{textos.datos.pass.rep}}</label>
                                <input type="password" name="newpass2" id="newpass2"  placeholder="****" class="sm-form-control"/>
                            </div>
                            <div class="col-md-12">
                                <button id="changebutpw" href="#" class="button button-3d fright">{{textos.datos.pass.cambiar}}</button>  
                            </div>
                        </div>
                    </div>

                    <div class="line visible-xs-block"></div>
                </div>

                <div class="line visible-xs-block"></div>

            </div>

        </div>

    </div>

</section><!-- #content end -->
