
<!-- Page Title
        ============================================= -->
<section id="page-title" class="header estadisticas_header">

    <div class="container clearfix">
        <h1>Estadísticas</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
        ============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">
            
            <!--Tartas-->
            <div class="col-md-4 col-sm-4 col-xs-12 bottommargin-sm" id="pieChart" style="opacity: 0;">
                <h3 class="center">Gasto en €</h3>
                <canvas id="pieChartCanvas" width="300" height="300"></canvas>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 bottommargin-sm" id="pieChart2" style="opacity: 0;">
                <h3 class="center">Tamaño de llanta de Neumáticos</h3>
                <canvas id="pieChartCanvas2" width="300" height="300"></canvas>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 bottommargin-sm" id="pieChart3" style="opacity: 0;">
                <h3 class="center">Marca de Neumáticos</h3>
                <canvas id="pieChartCanvas3" width="300" height="300"></canvas>
            </div>
            <!--Fin tartas-->
            <div class="line"></div>
            
            <!--Tabla datos-->
            <h3 style="margin: 0;" >Datos</h3>
            <div style="margin: 1% 0 4% 0;" class="line-custom"></div>
            <!--<div class="col_full"><a class="button button-border button-rounded button-fill fill-from-top button-amber" download="#" href="#"><span>Descargar datos en excel </span><i class="material-icons" style="vertical-align: middle;">file_download</i></a></div>-->
            <div class="table-responsive">
                <table id="estadisticas_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th class="hidden-xs">Tipo de producto</th>
                            <th>Nombre de cliente</th>
                            <th class="hidden-xs">Marca</th>
                            <th class="hidden-xs">Llanta</th>
                            <th class="hidden-xs">Unidades</th>
                            <th class="hidden-xs">Total (€)</th>
                            <th>Puntos Base</th>
                            <th class="hidden-xs">Formaciones</th>
                            <th class="hidden-xs">Recambios</th>

                        </tr>
                    </thead>
                    <tbody>
                     {%set user = '0' %}
                    {%for key, fact in facturacion%}
                                          {%set user = key %}
                        {%for p in fact.Facturacion %}
                            <tr>
                                <td class="click">{{p.fecha}}</td>
                                <td class="click">{{p.archivo}}</td>                                
                                <td class="click"><a href="{{siteurl}}inicio/perfil">{{p.nomCliente}}</a></td>
                                <td class="click">{{p.marca|capitalize}}</td>
                                <td class="click">{{p.llanta|capitalize}}</td>
                                <td class="click">{{p.unidades|number_format(0, '.', '')}}</td>
                                <td class="click">{{p.total|number_format(2, '.', '')}}</td>
                                <td class="click">{{p.kilometros|number_format(2, '.', '')}}</td>
                                
                                
                                <td class="click"><button class="button check_estads" data-toggle="modal" data-target="#Formacion{{user}}">{%if fact.Formacion is not null%}<i class="material-icons" style="color: green; cursor:pointer;">check_circle</i>{%else%}<i class="material-icons" style="color: red; cursor: pointer;">cancel</i>{%endif%}</button></td>
                               
                                <td class="click"><button class="button check_estads" data-toggle="modal" data-target="#Recambios{{user}}">{%if fact.Recambios is not null%}{% if fact.Recambios.sumPrimSem2018>60%}<i class="material-icons" style="color: green; cursor:pointer;">check_circle</i>{%else%}<i class="material-icons" style="color: red; cursor: pointer;">cancel</i>{%endif%}{%else%}<i class="material-icons" style="color: red; cursor: pointer;">cancel</i>{%endif%}</button></td>
                                
                          
                            </tr>
                    {%endfor%}
                    {%endfor%}
                    </tbody>
                </table>
            </div>

                                {%set user = '0'%}
                    {%for key, fact in facturacion%}
                                          {%set user = key %}
<!-- Modal formación -->
<div class="modal fade" id="Formacion{{user}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-body">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title modal-title-formacion" id="myModalLabel">Formaciones realizadas <span class="contitrade">{{factForAdmin.puntosTotales.razon_social}}</span></h4>
                </div>
                <div class="modal-body">
                    <div class="opening-table">
                        {%if fact.Formacion is not null%}
                        
                        <div class="time-table-wrap clearfix">
                        {%for fo in fact.Formacion%}
                            <div class="time-table clearfix">
                                <span class="col-md-9"><b>{{fo.nombre}}</b></span>
                                <span class="col-md-3">{{fo.fecha}}</span>
                            </div>
                        {%endfor%}
                          {%else%}
                            <div class="time-table clearfix">
                                <span class="col-md-9"><b>No ha realizado ninguna formación</b></span>
                            </div>
                           {%endif%}
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
<!--Fin modal formación-->
{%endfor%}


                                {%set user = '0'%}
                    {%for key, fact in facturacion%}
                                          {%set user = key %}
<!-- Modal recambios -->
<div class="modal fade" id="Recambios{{user}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-body">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title modal-title-formacion" id="myModalLabel">Recambios <span class="contitrade">{{factForAdmin.puntosTotales.razon_social}}</span></h4>
                </div>
                <div class="modal-body">

                     <div class="opening-table">
                        
                        <div class="time-table-wrap clearfix">
                            
                            {%if fact.Recambios is not null%}
                        <table class="time-table-wrap clearfix" width="100%">
                            
                            <tr class="time-table clearfix font-bold theader">
                                
                                <th class="col-md-4 "><b>Semestre</b></th>
                                <th class="col-md-4 center"><b>Puntos<br>acumulados</b></th>
                                <th class="col-md-4 center"><b>Compra restante</b></th>
                                
                            </tr>
                        {%for re in fact.Recambios%}
                            <tr class="time-table clearfix">
                                <td class="col-md-4"><b>{{loop.index}} Semestre</b></td>
                                <td class="col-md-4 center">{{re|number_format(2, ',', '.')}} puntos</td>
                                <td class="col-md-4 center">{%if re>=60%}0 {%else%}{{1200-(re|number_format(2, '.', ',')/0.05)|number_format(2, ',', '.')}}{%endif%} €</td>
                            </tr>
                        {%endfor%}    
                            
                            
                        </table>
                            {%else%}
                            <div class="time-table clearfix">
                                <span class="col-md-9"><b>No ha realizado ninguna compra de recambios</b></span>
                            </div>
                            
                            {%endif%}
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>

{%endfor%}  <!--Fin tabla datos-->
        </div>
    </div>

</section><!-- #content end -->

<script type="text/javascript">
$(window).load(function () {

    var pieChartData1 = [
        {%for k, f in firstPie%}
        {
            value: {{f.0.total|number_format(2, '.', '')}},
            label: "{{k}}",
            color: "{{color[loop.index0]}}"
        },
        {%endfor%}

    ];
    
    var pieChartData2 = [
        {%for k, f in secondPie%}
        {
            value: {{f.0.total|number_format(2, '.', '')}},
            label: "{{k}}”",
            color: "{{color[loop.index0]}}"
        },
        {%endfor%}

    ];
    
    var pieChartData3 = [
        {%for k, f in thirdPie%}
        {
            value: {{f.0.total|number_format(2, '.', '')}},
            label: "{{k}}",
            color: "{{color[loop.index0]}}"
        },
        {%endfor%}

    ];


    var globalGraphSettings = {animation: Modernizr.canvas};


    function showPieChart() {
        var ctx = document.getElementById("pieChartCanvas").getContext("2d");
        new Chart(ctx).Pie(pieChartData1, globalGraphSettings);

        var ctx2 = document.getElementById("pieChartCanvas2").getContext("2d");
        new Chart(ctx2).Pie(pieChartData2, globalGraphSettings);

        var ctx2 = document.getElementById("pieChartCanvas3").getContext("2d");
        new Chart(ctx2).Pie(pieChartData3, globalGraphSettings);
    }



    $('#pieChart').appear(function () {
        $(this).css({opacity: 1});
        setTimeout(showPieChart, 300);
    }, {accX: 0, accY: -155}, 'easeInCubic');

    $('#pieChart2').appear(function () {
        $(this).css({opacity: 1});
        setTimeout(showPieChart, 300);
    }, {accX: 0, accY: -155}, 'easeInCubic');

    $('#pieChart3').appear(function () {
        $(this).css({opacity: 1});
        setTimeout(showPieChart, 300);
    }, {accX: 0, accY: -155}, 'easeInCubic');


});
</script>
