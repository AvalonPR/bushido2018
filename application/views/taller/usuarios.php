<?php
include 'header.php';
?>
<!-- Page Title
============================================= -->
<section id="page-title" class="usuarios_header">

    <div class="container clearfix">
        <h1>USUARIOS</h1>
        <span>Todo lo que necesitas saber de tus usuarios</span>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix table-responsive">
            <table id="listado-usuarios" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="col-md-2">Nombre</th>
                        <th class="col-md-2">Promociones adheridas</th>
                        <th class="col-md-2">Contacto</th>
                        <th class="col-md-1">Más</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Juan Carlos Pérez</td>
                        <td>4</td>
                        <td>688593362 / <a href="mailto: j.perez@gmail.com" target="_blank">j.perez@gmail.com</a></td>
                        <td class="click"><a href="{{siteurl}}/taller/usuarios/info"><i class="material-icons">add_circle</i></a></td>
                    </tr>
                    <tr>
                        <td>Elena Sánchez</td>
                        <td>1</td>
                        <td >688593362 / <a href="mailto: elena@gmail.com" target="_blank">elena@gmail.com</a></td>
                        <td class="click"><a href=""{{siteurl}}/taller/usuarios/info"" target="_blank"><i class="material-icons">add_circle</i></a></td>
                    </tr>
                    <tr>
                        <td><a href="">Santos Castro</a></td>
                        <td><a href="">2</a></td>
                        <td>688593362 / <a href="mailto: santosc@gmail.com" target="_blank">santosc@gmail.com</a></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>



        </div>

    </div>

</section><!-- #content end -->

<?php
include 'footer.php'
?>