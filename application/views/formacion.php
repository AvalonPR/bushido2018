<!-- Page Title
        ============================================= -->
<section id="page-title" class="formacion_header">

    <div class="container clearfix">
        <h1>{{formacion.nombre}}</h1>
        <span>{{formacion.fecha}}</span>
    </div>

</section><!-- #page-title end -->

<!-- Content
        ============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">
            <h2>{{formacion.nombre}}</h2>
                {% if users is not empty %}
            <div class="table-responsive">
                <table id="ProxFormTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Nombre formación</th>
                            <th>Usuario</th>
                        </tr>
                    </thead>
                    <tbody>
                        {%for formacion in users%}
                        <tr>
                            <td>{{formacion.fecha}}</td>
                            <td>{{formacion.nombre}}</td>
                            <td><a href="{{siteurl}}{%if userData.tipo=='2'%}comercial{%else%}admin{%endif%}/estadisticas/{{formacion.cod_user}}">{{formacion.razon_social}}</a></td>
                        </tr>
                        {%endfor%}
                    </tbody>
                </table>
            </div>
            <!--Fin tabla datos-->
            {%else%}
            {%if userData.tipo=='2'%}
            <p>Parece que ninguno de tus clientes ha participado en esta formación.</p>
            {%else%}
            <p>No hay datos de está formación todavía, puedes subirlos <a href="{{baseurl}}admin/formacion/subir">aquí</a></p>
            {%endif%}
            {% endif %}
        </div>
    </div>

</section><!-- #content end -->
