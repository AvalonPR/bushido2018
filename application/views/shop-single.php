<!-- Page Sub Menu
                ============================================= -->
<div id="page-menu" class="hidden-xs hidden-sm">

    <div id="page-menu-wrap">

        <div class="container clearfix">
            <nav id="primary-menu" class="catalogo-completo">

                <ul>
                    <li id="catalogo_menu" class="current mega-menu sub-menu"><a href="#" style="color: #fff;">{{textos.subtitulo}} <i class="icon-angle-down"></i></a>
                        <div id="catalogo_menu_in" class="mega-menu-content clearfix">
                            {%for key, j in jerarquia%}
                            <ul class="mega-menu-column col-5">
                                <li><a class="menu_white" href="{{baseurl}}productos/{{key}}">{{j.nombre}}</a></li>
                            </ul>
                            {%endfor%}
                        </div>
                    </li>
                </ul>

            </nav><!-- #primary-menu end -->
            <!-- #primary-menu end -->
        </div>
    </div>

</div>
<!-- #page-menu end -->


<!--MIGA DE PAN-->
<div class="container">
    <ul class="migapan">
            <li class="migapan-item"><a href="{{baseurl}}productos">&nbsp;{{textos.titulo}}&nbsp;</a></li>
            <li class="migapan-item"><a href="{{baseurl}}productos/{{productos.parent.categoria.slug}}">&nbsp;{{productos.parent.categoria.slug|capitalize}}&nbsp;</a></li>
            <li class="migapan-item"><a href="{{baseurl}}productos/{{productos.parent.categoria.slug}}/{{productos.parent.subcategoria.slug}}">&nbsp;{{productos.parent.subcategoria.slug|capitalize}}&nbsp;</a></li>
            <li class="migapan-item"><a href="{{baseurl}}productos/{{productos.parent.categoria.slug}}/{{productos.parent.subcategoria.slug}}/{{productos.parent.familia.slug}}">&nbsp;{{productos.parent.familia.slug|capitalize}}&nbsp;</a></li>
            <li class="migapan-item active" aria-current="page">&nbsp;{{productos.nombre|capitalize}}</li>
    </ul>
</div>

<!--FIN MIGA DE PAN-->

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

            <div class="nobottommargin clearfix col_last">

                <div class="single-product">

                    <div class="product">


                        <div class="col-lg-5 col-md-5 col-sm-4 col-xs-12 center">
                            <img src="https://apps.avalonprplus.com/uploads/{{productos.imagen}}" alt="{{productos.nombre}}">
                        </div>

                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-8 col-xs-12 nobottommargin col_last product-desc">

                        <!-- Product Single - Name
                                                                    ============================================= -->
                        <div class="product-title product-title-single">
                            <h2>{{productos.nombre}}</h2>
                        </div>

                        <!-- Product Single - Price
                                                                    ============================================= -->
                        <div class="product-price product-price-single">{%if productos.precioDto is not null%}<del>{{productos.precio|number_format(0, ',', '.')}} </del> <ins>{{productos.precioDto|number_format(0, ',', '.')}} </ins> {%else%}<ins> {{productos.precio|number_format(0, ',', '.')}} </ins> {% endif%}</div>
                        <div class="clear"></div>
                        <div class="line"></div>
                        <input type="hidden" value="{{productos.unidades}}" id="unidades">
                        <input type="hidden" value="{{productos.id}}" id="id">
                        {%if productos.atributos is not empty%}
                            {%for k,atributo in productos.atributos%}
                                <!-- Product Single - Quantity & Cart Button
                                ============================================= -->
                                {%if k=='2'%}
                                <p class="">{{textos.item.talla}}</p>
                                <div class="col_full talla grid-container" data-layout="fitRows">
                                    
                                    {%for a in atributo%}
                                    <div class="size-item fleft">
                                        <input onclick="selectSize('size{{a}}', '{{productos.id}}')"  type="button" class="clearfix" value="{{a}}" id="size{{a}}">
                                        <div class="quantity-size" id="divsize{{a}}" data-size="{{a}}" style="display:none;">
                                                <input type="button" value="-" class="minus size-minus">
                                                <input type="text" step="1" min="1" value="1" title="Qty" class="qty" name="qtysize[]" size="4" />
                                                <input type="button" value="+" class="plus size-plus">
                                        </div>
                                    </div>
                                    {%endfor%}
                                </div>
                                <div class="line"></div>
                                {%endif%}
                                {%if k=='1'%}
                                <p class="">{{textos.item.color}}</p>
                                <div class="col_full color_ grid-container" data-layout="fitRows">
                                    {%for a in atributo%}
                                    <label class="color-item fleft"
                                           {%set color = a|split(',') %}>
                                        <input  type="checkbox" id="color" data-id="{{color.1}},{{color.0}}" name="color[1][]">
                                        <span class="checkmark" style="background-color: {{color.0}};"></span>
                                    </label>
                                    {%endfor%}
                                </div>
                                <div class="line"></div>
                                {%endif%}
                            {%endfor%}
                        {%endif%}
                        
                       

                        
                        <div class="quantity quantitySingle">
                            <input type="button" value="-" class="minus">
                            <input type="text" step="1" min="1" name="quantity" value="1" title="Qty" class="qty" size="4" id="quantity{{productos.id}}"/>
                            <input type="button" value="+" class="plus">
                        </div>
                        {%if productos.precioDto is not null%}
                        <button onclick="addtoCart({{productos.id}})" class="single_product_add add-to-cart{%if user.viaje != 0 %}-disabled{%elseif user.puntos < productos.precioDto %}-disabled{%elseif user.puntos < (cart_total + productos.precioDto) %}-disabled{%endif%} button nomargin fright" {%if user.viaje != 0 %}disabled{%elseif user.puntos < productos.precioDto %}disabled{%elseif user.puntos < (cart_total + productos.precioDto) %}disabled{%endif%} >{{textos.botones.addmore}}</button>
                        {%else%}
                        <button onclick="addtoCart({{productos.id}})" class="single_product_add add-to-cart{%if user.viaje != 0 %}-disabled{%elseif user.puntos < productos.precio %}-disabled{%elseif user.puntos < (cart_total + productos.precio) %}-disabled{%endif%} button nomargin fright" {%if user.viaje != 0 %}disabled{%elseif user.puntos < productos.precio %}disabled{%elseif user.puntos < (cart_total + productos.precio) %}disabled{%endif%} >{{textos.botones.addmore}}</button>
                        {%endif%}
                        <!-- Product Single - Quantity & Cart Button End -->

                        <div class="clear"></div>
                        
                        <div class="line"></div>

                        <p>{{productos.descripcion|raw}}</p>


                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</section>
