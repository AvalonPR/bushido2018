{%if user.logged_in%}
<!-- Footer
============================================= -->
<footer id="footer" class="dark">    
    
    <div class="imagen-footer">
    <div class="container ">

        <!-- Footer Widgets
        ============================================= -->
        <div class="footer-widgets-wrap clearfix">

            

                <div class="col_one_third">

                    <div class="widget clearfix">

                        <img src="{{imgurl}}logo-footer-white.png" alt="" class="footer-logo">

                    </div>
                </div>
                

                <div class="col_one_third">

                    <div class="widget widget_links clearfix">

                        <h4>Páginas</h4>

                        <ul>
                            {%if user.tipo == 'C' %}
                            <li><a href="{{base_url()}}inicio/viaje">{{menu.viaje|capitalize}}</a></li>
                            <li><a href="{{base_url()}}productos">{{menu.catalogo|capitalize}}</a></li>
                            <li><a href="{{base_url()}}inicio/trayectoria">{{menu.trayectoria|capitalize}}</a></li>
                            <li><a href="{{base_url()}}inicio/bases">{{menu.programa|capitalize}}</a></li>
                            {%endif%}
                            <!--<li><a href="{{base_url()}}{%if user.tipo== '1' %}inicio/{%elseif user.tipo == '2' %}comercial/{%elseif user.tipo == '3'%}admin/{%endif%}estadisticas">Mis Estadísticas</a></li>-->
                            <li><a href="{{base_url()}}{%if user.tipo == 'C' %}inicio/{%elseif user.tipo == 'S'%}comercial/{%elseif user.tipo == 'A'%}admin/{%endif%}perfil">{{menu.perfil|capitalize}}</a></li>
                        </ul>

                    </div>

                </div>

                

            

            <div class="col_one_third col_last">

                    <h5>Sede:</h5>
                    <p>{{footer.direccion}}<br>
                    {{footer.direccion2}} <br>
                    {{footer.direccion3}}</p>

                    <p>Teléfono: {{footer.tfno}}<br>
                    Email: <a href="mailto:info.bushido@yokohamaiberia.com" target="_blank">info.bushido@yokohamaiberia.com</a></p>

            </div>

        </div><!-- .footer-widgets-wrap end -->

    </div>
    </div>
    <!-- Copyrights
    ============================================= -->
    <div id="copyrights">

        

            <div class="center">
                &copy; Yokohama Iberia {{"now"|date("Y")}}<br>
                <div class="copyright-links"><a href="{{base_url()}}inicio/bases">{{footer.aviso}}</a> / <a href="{{base_url()}}inicio/privacidad">{{footer.privacidad}}</a> / <a href="{{base_url()}}inicio/cookies">{{footer.cookies}}</a></div>
            </div>

            

        

    </div><!-- #copyrights end -->
    
    

</footer><!-- #footer end -->
<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

{%else%}
{% if 'profissionais' in uri %}
    <div id="cookies-eu-banner" style="display: none;">
        <div class="divcenter">
            <span>By continuing to visit this site, you accept the use of cookies by Google Analytics for statistical purposes.</span>
            <a href="{{base_url()}}cliente/privacidad" id="cookies-eu-more">ler mais</a>
            <button id="cookies-eu-reject" class="button button-3d button-rounded button-black">Rejeitar</button>
            <button id="cookies-eu-accept" class="button button-3d button-rounded button-red">Aceitar</button>
        </div>
    </div> 
{%else%}
<div class="cookiesContainer" id="cookies-eu-banner" style="display: block;">
    <div class="cookiesContent block divcenter">
        <h3 class="notopmargin center">Valoramos su privacidad </h3>
        <div id="divCookieModal">
            <span>El sitio web https://www.yokohamaiberia.es/ utiliza cookies propias y de terceros para recopilar información que ayuda a optimizar su visita a sus páginas web. No se utilizarán las cookies para recoger información de carácter personal. Usted puede permitir su uso o rechazarlo, también puede cambiar su configuración siempre que lo desee. Encontrará más información en nuestra <a href="{{base_url()}}inicio/cookies" id="cookies-eu-more" target="_blank">Política de Cookies.</a></span>
            
        </div>

        <div id="divMoreInfo" style="display: none;">
            Puedes configurar tus preferencias y elegir como quieres que tus datos sean utilizados para los siguientes propósitos. Puedes elegir configurar tus preferencias solo con nosotros independientemente del resto de nuestros partners. Cada propósito tiene una descripción para que puedas saber como nosotros y nuestros partners utilizamos tus datos
            <div>
                <h4>Almacenamiento y acceso a la información</h4>
                <p>El almacenamiento de información, o el acceso a información que ya está almacenada en su dispositivo, como identificadores de publicidad, identificadores de dispositivos, cookies y tecnologías similares.</p>
            </div>
            <input id="infoCookie" class="bt-switch" type="checkbox" checked data-on-text="Activar" data-off-text="Desactivar">
            <div>
                <h4>Google</h4>
                <p>Permitir a Google y sus socios tecnológicos recopilar datos y emplear cookies para realizar mediciones y personalizar sus anuncios.</p>
            </div>
            <input id="googleCookie"  class="bt-switch" type="checkbox" checked data-on-text="Activar" data-off-text="Desactivar">

        </div>
            <div class="center topmargin-xs" >                     
                <button id="cookies-eu-reject" class="button button-3d button-rounded button-black">Rechazar</button>
                <button id="cookies-eu-accept" class="button button-3d button-rounded button-red">Aceptar</button>
            </div>
        <a id="showCookiesModal" style="display:none;">< Volver</a>
        <a id="showMoreCookieInfo">Más información</a>

    </div>
</div>
{%endif%}
{%endif%}
{% for script in footerscripts %}
{{script|raw}}
{% endfor %}    

</body>
</html>



