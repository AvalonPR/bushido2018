<!-- Page Title
                ============================================= -->
<section id="page-title" class="header viaje_header">

    <div class="container clearfix">
        <h1 style="">{{textos.titulo}}</h1>
    </div>

</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id="content">


    <div class="content-wrap" style="padding-bottom: 0;">

        <div class="container clearfix">

            <div class="row clearfix">

                <div class="col-sm-12">

                    <div class="clear"></div>

                    <div class="row clearfix">

                        <div class="condicionesViaje col-md-12 bottommargin-lg">
                            
                            

                            {% if userData.viaje is null%}
                            
                            <h3 style="text-align: center;">{{textos.subtitulo}}</h3>
                            
                            <p>{{textos.bienvenida}}</p>
                            
                            <p>{{textos.condiciones}}*</p>
                            
                            <div class="col-md-6 col-xs-12">
                                <img class="img-responsive" src="{{imgurl}}facturacion_01.jpg" alt="">
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <img class="img-responsive" src="{{imgurl}}facturacion_02.jpg" alt="">
                            </div>
                            
                            <ul>
                                <li>{{textos.dosplazas}}</li>
                                <li>{{textos.unaplaza}}</li>
                            </ul>
                            <div class="style-msg" style="background-color: #EEE;">
                                <div class="sb-msg"><b>{{textos.sinviaje}} </b></div>
                            </div>
                            
                            
                            <div class="center" style="margin-bottom: 20px;">
                                <a href="#myModal1" data-lightbox="inline" class="button button-large button-rounded">{{textos.boton}}</a>						
                            </div>
                            <small>*{{textos.corte}}</small>
                            <img src="{{imgurl}}viaje-bushido.jpg" alt="Viaje Yokohama Bushido 2020-2021">
                        
                            <!-- Modal -->
                            <div class="modal1 mfp-hide" id="myModal1">
                                <div class="block divcenter" style="background-color: #dc0026; max-width: 520px;">
                                    <button title="Close (Esc)" type="button" class="mfp-close">×</button>
                                    <div class="row nomargin clearfix">

                                        <div class="col-padding" data-height-xl="350" data-height-lg="400" data-height-md="456" data-height-sm="456" data-height-xs="456">

                                            <h3>{{textos.estimado}}</h3>
                                            
                                            <p>{{textos.eleccion}}</p>
                                            <form class="form-legales clearfix" id="aceptar-viaje" >
                                                <div>
                                                    <input id="radio-10" class="radio-style" name="radio-viaje" type="radio" value="1">
                                                    <label for="radio-10" class="radio-style-3-label">1 {{textos.plaza}}</label>
                                                </div>
                                                <div id="yearSelect" style='display: none;'>
                                                    <p>{{textos.eleccionyear}}</p>
                                                    <div>
                                                        <input id="radio-2020" class="radio-style"name="radio-year" type="radio" value="2020" checked>
                                                        <label for="radio-2020" class="radio-style-3-label radio-small">2020</label>
                                                    </div>
                                                    <div>
                                                        <input id="radio-2021" class="radio-style" name="radio-year" type="radio" value="2021">
                                                        <label for="radio-2021" class="radio-style-3-label radio-small">2021</label>
                                                    </div>
                                                </div>
                                                <div>
                                                    <input id="radio-11" class="radio-style"name="radio-viaje" type="radio" value="2">
                                                    <label for="radio-11" class="radio-style-3-label">2 {{textos.plazas}}</label>
                                                </div>
                                                <div>
                                                    <input id="radio-12" class="radio-style" name="radio-viaje" type="radio" value="0">
                                                    <label for="radio-12" class="radio-style-3-label">{{textos.participar}}</label>
                                                </div>
                                                <button class="button button-dark fright" onclick="aceptarViaje()">{{textos.aceptar}}</button></p>
                                                <div class="clear"></div>
                                            </form>
                                            

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Fin Modal bases legales-->    
                            {%else%}
                            <h2 class="center">{{textos.tituloviaje}}<span style="font-family: 'yozakura_jpregular';">{{textos.destinoviaje}}</span>!</h2>
                            <h3 class="center">{{textos.subtitulo}}</h3>
                            <p class="center">{{textos.descarga}}</p>
                            
                            <div class="divcenter" style="width: fit-content;">
                                {%if user.idioma =='PT'%}
                                <a href="{{base_url()}}uploads/programa_viajem_Bushido_2019_Oporto.pdf" class="button button-rounded button-reveal button-large button-red tright bottommargin-sm" target="_blank"><i class="far fa-file-pdf"></i><span>{{textos.botonprograma}}/OPORTO</span></a>
                                <a href="{{base_url()}}uploads/programa_viajem_Bushido_2019_Lisboa.pdf" class="button button-rounded button-reveal button-large button-red tright bottommargin-sm" target="_blank"><i class="far fa-file-pdf"></i><span>{{textos.botonprograma}}/LISBOA</span></a>              
                                {%else%}                 
                                <a href="{{base_url()}}uploads/programa_viaje_Bushido_2019.pdf" class="button button-rounded button-reveal button-large button-red tright bottommargin-sm" target="_blank"><i class="far fa-file-pdf"></i><span>{{textos.botonprograma}}</span></a>
                                {%endif%}
                            </div>

                            <img src="{{imgurl}}viaje-bushido-vietnam.jpg" alt="Viaje Yokohama Bushido 2020-2021">
                            
                            

                            
                            <h3>{{textos.condiciones}}</h3>
                            <div class="col-md-6 col-xs-12">
                                <img class="img-responsive" src="{{imgurl}}facturacion_01.jpg" alt="">
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <img class="img-responsive" src="{{imgurl}}facturacion_02.jpg" alt="">
                            </div>
                            <ul>
                                <li>{{textos.dosplazas}}</li>
                                <li>{{textos.unaplaza}}</li>
                            </ul>
                            <div class="style-msg" style="background-color: #EEE;">
                                <div class="sb-msg"><b>{{textos.sinviaje}} </b></div>
                            </div>
                            
                            <p>*{{textos.corte}}</p>
                            
                            
                            {%endif%}
                            <div class="divider"><i class="icon-circle"></i></div>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!--<div class="col_full viaje_call center">
        <h3 style="color:white;">¿Ya has hecho las maletas?</h3>
        <a href="{{base_url()}}uploads/agenda_CruceroBushido_2018.pdf" class="button button-xlarge tright button-dark" id="travel_button">Descarga aquí la agenda del viaje <i class="material-icons">cloud_download</i></a></br>
    <span>&nbsp;</span>
</div>-->
</section><!-- #content end -->
