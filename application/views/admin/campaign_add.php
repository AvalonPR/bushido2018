<!-- Page Title
============================================= -->
<section id="page-title" class="admin_header">

    <div class="container clearfix">
        <h1>Campaña nueva</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

            <!--Slider item-->
            <h3>Configuración campaña</h3>
            <div class="line-custom"></div>
            <div class="row">
                <div class="col_half bottommargin-sm">
                    <label for="slider-title">Destinatario final:</label>
                    <ul style="list-style: none;">
                        <li class="select_user">
                            <select class="selectpicker col-md-12 col-sm-12 col-xs-12">
                                <option>[Elige uno]</option>
                                <option>Usuario Taller</option>
                                <option>Usuario Final</option>
                            </select>
                        </li>
                    </ul>
                </div>
                <div class="col_half col_last bottommargin-sm file_ups">
                    <label>Imagen <span> Tamaño: 1665x500 pixels Extensión: .jpg, .png.</span></label><br>
                    <input id="input-1" type="file" class="file">
                </div>
                <div class="col_half">
                    <label for="slider-title">Título</label>
                    <input type="text" id="slider-title" name="slider-title" value="Promoción Mayo 2018" class="sm-form-control"/>
                </div>

                <div class="col_full">
                    <!--Url externa view-->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="url" class="radio-style" name="radio-group-2" type="radio">
                        <label for="radio-7" class="radio-style-2-label">Url externa</label>
                    </div>
                    <!--Página view-->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="pagina" class="radio-style" name="radio-group-2" type="radio">
                        <label for="radio-8" class="radio-style-2-label">Crear página</label>
                    </div>
                </div>
                <!--Div oculto url-->
                <div class="col_half" id="url_slider_show">
                    <input type="text" id="slider-url" name="slider-url" value="http://..." class="sm-form-control"/>
                </div>
                <!--Fin div oculto url-->
                <!--Div oculto página nueva-->
                <div id="new_page_slider_show">
                    <div class="col_full">
                        <h3>Elige el estilo de tu página</h3>
                        <ul style="list-style: none;">
                            <li class="col-md-4 col-sm-4 col-xs-12">
                                <select class="selectpicker">
                                    <option>[Style]</option>
                                    <option>Paragraph</option>
                                    <option>Heading 1</option>
                                    <option>Heading 2</option>
                                </select>
                            </li>
                            <li class="col-md-4 col-sm-4 col-xs-12">
                                <select class="selectpicker">
                                    <option>[Fuente]</option>
                                    <option>Arial</option>
                                    <option>Helvetica</option>
                                    <option>Continental Stag</option>
                                </select>
                            </li>
                            <li class="col-md-4 col-sm-4 col-xs-12">
                                <select class="selectpicker">
                                    <option>[Size]</option>
                                    <option>12</option>
                                    <option>14</option>
                                    <option>16</option>
                                </select>
                            </li>
                        </ul>
                    </div>
                    <div class="col_full" id="admin_edit">
                        <div class="col_full">
                            <ul style="margin-bottom: 0" class="col-md-12 col-sm-12 col-xs-12 admin_edit_mb">
                                <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/bold.png" alt="Negrita" title="Negrita"></li>
                                <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/italic.png" alt="Cursiva" title="Cursiva"></li>
                                <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/underline.png" alt="Subrayado" title="Subrayado"></li>
                                <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/left_just.png" alt="Alinear a la izquierda" title="Alinear a la izquierda"></li>
                                <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/centre.png" alt="Alinear al centro" title="Alinear al centro"></li>
                                <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/right_just.png" alt="Justificar" title="Justificar"></li>
                                <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/hr.png" alt="Línea horizontal" title="Línea horizontal"></li>
                                <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/numbered_list.png" alt="Lista numerada" title="Lista numerada"></li>
                                <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/list.png" alt="Lista" title="Lista"></li>
                                <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/outdent.png" alt="Quitar sangría" title="Quitar sangría"></li>
                                <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/indent.png" alt="Añadir sangría" title="Añadir sangria"></li>
                                <li class="col-md-1 col-sm-1 col-xs-2 center"><button  class="button_edit" data-toggle="modal" data-target="#ModalFuente"><img src="{{imgurl}}icons/textcolor.png" alt="Color del texto" title="Color del texto"></button></li>
                            </ul>
                            <ul class="col-md-12 col-sm-12 col-xs-12 admin_edit_mb">
                                <li class="col-md-1 col-sm-1 col-xs-2 center"><button  class="button_edit" data-toggle="modal" data-target="#ModalBg"><img src="{{imgurl}}icons/bgcolor.png" alt="Color del fondo" title="Color del fondo"></button></li>
                                <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/hyperlink.png" alt="Añadir link" title="Añadir link"></li>
                                <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/unlink.png" alt="Quitar link" title="Quitar link"></li>
                                <li class="col-md-1 col-sm-1 col-xs-2 center"><button  class="button_edit" data-toggle="modal" data-target="#ModalImg"><img src="{{imgurl}}icons/image.png" alt="Insertar imagen" title="Insertar imagen"></button></li>
                                <li class="col-md-1 col-sm-1 col-xs-2 center"><button  class="button_edit" data-toggle="modal" data-target="#ModalTable"><img src="{{imgurl}}icons/insert_table.png" alt="Insertar tabla" title="Insertar tabla"></button></li>
                                <li class="col-md-1 col-sm-1 col-xs-2 center"><button  class="button_edit" data-toggle="modal" data-target="#ModalVideo"> <img src="{{imgurl}}icons/video.png" alt="Insertar vídeo" title="Insertar vídeo"></button></li>
                                <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/undo.png" alt="Deshacer" title="Deshacer"></li>
                                <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/redo.png" alt="Rehacer" title="Rehacer"></li>
                                <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/removeformat.png" alt="Eliminar formato" title="Eliminar formato"></li>
                            </ul>
                        </div>
                    </div>
                    <iframe class="col_full" name="editable" src="" id="edit_content">

                    </iframe>
                </div>


                <!--Modal color fuente-->
                <div class="modal fade" id="ModalFuente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-body">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel">Color de la fuente</h4>
                                </div>
                                <div class="modal-body colors_admin_container">
                                    <div class="col-md-12">
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 255, 255);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 204, 204);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 204, 153);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 255, 153);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 255, 204);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(153, 255, 153);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(153, 255, 255);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(204, 255, 255);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(204, 204, 255);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 204, 255);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(204, 204, 204);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 102, 102);"></div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 153, 102);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 255, 102);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 255, 51);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(102, 255, 153);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 255, 255);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(102, 255, 255);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(153, 153, 255);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 153, 255);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(244, 67, 54);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 153, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 204, 102);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 255, 0);"></div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 255, 51);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(102, 204, 204);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 204, 255);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(102, 102, 204);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(204, 102, 204);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(153, 153, 153);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(204, 0, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 102, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 204, 51);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 204, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 204, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(0, 204, 204);"></div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 102, 255);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(102, 51, 255);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(204, 51, 204);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(102, 102, 102);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(153, 0, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(204, 102, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(204, 153, 51);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(153, 153, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(0, 153, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 153, 153);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 51, 255);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(102, 0, 204);"></div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="col-md-1 colors_admin" style="background: rgb(153, 51, 153);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 51, 51);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(102, 0, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(153, 51, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(153, 102, 51);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(102, 102, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(0, 102, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 102, 102);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(0, 0, 153);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 51, 153);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(102, 51, 102);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(0, 0, 0);"></div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 0, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(102, 51, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(102, 51, 51);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 51, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(0, 51, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(0, 51, 51);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(0, 0, 102);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 0, 153);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 0, 51);"></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Fin modal color fuente-->

                <!--Modal color fondo-->
                <div class="modal fade" id="ModalBg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-body">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel">Color del fondo</h4>
                                </div>
                                <div class="modal-body colors_admin_container">
                                    <div class="col-md-12">
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 255, 255);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 204, 204);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 204, 153);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 255, 153);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 255, 204);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(153, 255, 153);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(153, 255, 255);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(204, 255, 255);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(204, 204, 255);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 204, 255);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(204, 204, 204);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 102, 102);"></div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 153, 102);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 255, 102);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 255, 51);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(102, 255, 153);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 255, 255);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(102, 255, 255);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(153, 153, 255);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 153, 255);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(244, 67, 54);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 153, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 204, 102);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 255, 0);"></div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 255, 51);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(102, 204, 204);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 204, 255);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(102, 102, 204);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(204, 102, 204);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(153, 153, 153);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(204, 0, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 102, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 204, 51);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(255, 204, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 204, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(0, 204, 204);"></div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 102, 255);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(102, 51, 255);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(204, 51, 204);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(102, 102, 102);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(153, 0, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(204, 102, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(204, 153, 51);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(153, 153, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(0, 153, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 153, 153);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 51, 255);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(102, 0, 204);"></div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="col-md-1 colors_admin" style="background: rgb(153, 51, 153);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 51, 51);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(102, 0, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(153, 51, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(153, 102, 51);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(102, 102, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(0, 102, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 102, 102);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(0, 0, 153);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 51, 153);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(102, 51, 102);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(0, 0, 0);"></div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 0, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(102, 51, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(102, 51, 51);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 51, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(0, 51, 0);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(0, 51, 51);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(0, 0, 102);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 0, 153);"></div>
                                        <div class="col-md-1 colors_admin" style="background: rgb(51, 0, 51);"></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Fin modal color fondo-->

                <!--Modal insertar imagen-->
                <div class="modal fade" id="ModalImg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-body">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel">Insertar imagen</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="col_full">
                                        <label>Seleccione una o más imágenes</label><br>
                                        <input id="input-1" type="file" class="file">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                    <button type="button" class="btn btn-primary">Insertar imagen</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Fin modal insertar imagen-->

                <!--Modal insertar tabla-->
                <div class="modal fade" id="ModalTable" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-body">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel">Insertar tabla</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="col_full">
                                        <label for="#">Filas</label>
                                        <input class="col-md-12" type="text" name="#">
                                    </div>

                                    <div class="col_full">
                                        <label for="#">Columnas</label>
                                        <input class="col-md-12" type="text" name="#">
                                    </div>

                                    <div class="col_full">
                                        <label for="#">Ancho</label>
                                        <input class="col-md-12" type="text" name="#">
                                    </div>

                                    <div class="col_full">
                                        <label for="#">Ancho</label>
                                        <select class="selectpicker col-md-12">
                                            <option>Pixels</option>
                                            <option>Centímetros</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                    <button type="button" class="btn btn-primary">Insertar tabla</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Fin modal insertar tabla-->

                <!--Modal insertar vídeo-->
                <div class="modal fade" id="ModalVideo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-body">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel">Insertar vídeo</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="col_full">
                                        <label for="#">URL de vídeo</label>
                                        <input class="col-md-12" type="text" name="#">
                                    </div>

                                    <div class="col_full">
                                        <label for="#">Alto (px)</label>
                                        <input class="col-md-12" type="text" name="#">
                                    </div>

                                    <div class="col_full">
                                        <label for="#">Ancho (px)</label>
                                        <input class="col-md-12" type="text" name="#">
                                    </div>

                                    <div class="col_full">
                                        <label for="#">Alineado en pantalla</label>
                                        <select class="selectpicker col-md-12">
                                            <option>Derecha</option>
                                            <option>Izquierda</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                    <button type="button" class="btn btn-primary">Insertar vídeo</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Fin modal insertar vídeo-->

                <!--Fin div oculto página nueva-->

            </div>
            <!--Fin slider item-->
        </div>

    </div>

</section><!-- #content end -->