<!-- Page Title
============================================= -->
<section id="page-title" class="pedidos-taller">

    <div class="container clearfix">
        <h1>Pedidos</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">
            
            
            <div class="line"></div>
            <!--Tabla pedidos-->
            <div class="row">
                <div class="table-responsive">
                    <table id="pedidos_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Número</th>
                                <th>Fecha</th>
                                <th>Destinatario</th>
                                <th>Dirección</th>
                                <th>Estado</th>
                                <th>Artículos</th>
                                <th>Puntos totales</th>
                            </tr>
                        </thead>
                        <tbody>
                            {% for pedido in pedidos%}
                            <tr>
                                <td class="click">{{ pedido.fecha|date("Y") }}-{{pedido.idpedidos}}</td>
                                <td>{{pedido.fecha}}</td>
                                <td class="click"><a data-toggle="modal" href="#shipping{{pedido.idpedidos}}" >{{pedido.datosEnvio.shippingname}}</td>
                                <td>{{pedido.datosEnvio.shippingaddress}}</td>
                                <td class="center">
                                    <select class="selectpicker">
                                        <option{%if pedido.estado=='pendiente'%} selected{%endif%}>Pendiente</option>
                                        <option{%if pedido.estado=='enviado'%} selected{%endif%} value="enviado">Enviado</option>
                                        <option{%if pedido.estado=='entregado'%} selected{%endif%} value="entregado">Entregado</option>
                                        <option{%if pedido.estado=='devuelto'%} selected{%endif%} value="devuelto">Devuelto</option>
                                    </select>
                                </td>
                                <td class="click center"><button class="button plus-table" data-toggle="modal" data-target="#producto{{pedido.idpedidos}}"><i class="material-icons">add_circle</i></button></td>
                                <td class="click center" >{{pedido.kmtotal|number_format(0,',','.')}}</td>
                            </tr>
                            {%endfor%}
                        </tbody>
                    </table>
                </div>
            </div>
            <!--Fin tabla pedidos-->
            {%for pedido in pedidos%}
            <!--Modal pedidos-->
            <div class="modal fade" id="producto{{pedido.idpedidos}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-body">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="myModalLabel">Artículos del pedido <span class="contitrade"> {{pedido.idpedidos}}</span></h4>
                            </div>
                            <div class="modal-body">
                                <table class="table table-striped activas table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="col-md-8">Artículo</th>
                                            <th class="col-md-1">Cantidad</th>
                                            <th class="col-md-2">Puntos</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {%for linea in pedido.pedido%}
                                        <tr>
                                            <td class="click">{{linea.name}}
                                        {% if linea.options.size is not null%}
                                        {% for talla in linea.options.size%}
                                        </br><span>Talla: {{talla.0}} x {{talla.1}}uds</span>
                                        {%endfor%}</td>
                                        {%endif%}
                                            <td>{{linea.qty}}</td>
                                            <td>{{linea.price|number_format(0,',','.')}}</td>
                                        </tr>

                                        {%endfor%}
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Fin modal pedidos-->
            
            <!--Inicio Modal Datos de envío-->
             <div class="modal fade" id="shipping{{pedido.idpedidos}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-body">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h3 class="modal-title" id="myModalLabel">Datos de envío <span class="contitrade">pedido {{pedido.idpedidos}}</span></h3>
                            </div>
                            <div class="modal-body">
                                <table class="table table-striped activas table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="col-half crillee">Datos facturación</th>
                                            <th class="col-half crillee">Datos envío</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <tr>
                                            <td>{{pedido.datosEnvio.billingname}}</td>
                                            <td>{{pedido.datosEnvio.shippingname}}</td>
                                        </tr>

                                        <tr>
                                            <td>{{pedido.datosEnvio.billingcompanyname}}</td>
                                            <td>{{pedido.datosEnvio.shippingcompanyname}}</td>
                                        </tr>
                                        
                                        <tr>
                                            <td>{{pedido.datosEnvio.billingaddress}}</td>
                                            <td>{{pedido.datosEnvio.shippingaddress}}</td>
                                        </tr>
                                        
                                        <tr>
                                            <td>{{pedido.datosEnvio.billingcity}}</td>
                                            <td>{{pedido.datosEnvio.shippingcity}}</td>
                                        </tr>

                                        
                                    </tbody>
                                </table>
                                {%if pedido.datosEnvio.shippingmessage !=''%}
                                <table class="table table-striped activas table-bordered col-half">
                                    <thead>
                                        <th class="crillee">Observaciones</th>
                                    </thead>
                                    <tbody>
                                        <td>{{pedido.datosEnvio.shippingmessage}}</td>
                                    </tbody>
                                </table>
                                {%endif%}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Fin modal datos de envío-->
            {%endfor%}
            <div class="line"></div>
           
            <div class="">
                <a class="fright button button-rounded button-yellow" href="{{siteurl}}{%if userData.tipo=='2'%}comercial{%else%}admin{%endif%}/pedidos_2017">Ver pedidos 2017</a>
            </div>
        </div>

    </div>

</section><!-- #content end -->