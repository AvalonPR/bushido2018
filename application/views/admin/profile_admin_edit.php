

<!-- Page Title
                ============================================= -->
<section id="page-title" class="perfil_header">

    <div class="container clearfix">
        <h1>Mi perfil</h1>
    </div>

</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

            <div class="row clearfix">

                <div class="col-sm-12">

                    <!--<img src="images/icons/avatar.jpg" class="alignleft img-circle img-thumbnail notopmargin nobottommargin" alt="Avatar" style="max-width: 84px;">-->

                    <div class="heading-block noborder">
                        <h3 id="user-name">Datos de contacto</h3>
                        <span></span>
                        <div class="line-custom"></div>
                    </div>

                    <div class="clear"></div>
                    <div class="row clearfix">

                        <div class="col-md-12">

                <div class="contact-widget">
                    <div class="contact-form-result"></div>

                    <p class="">Si necesitas editar algún dato que no puedas desde aquí. Ponte en contacto con nosotros mandando un email a <a href="mailto:contacto@bestdrive.plus">contacto@bestdrive.plus</a></p>
                            <form id="billing-form" name="billing-form" class="nobottommargin" action="{{baseurl}}/inicio/upsertDatos" method="post">
                                <div class="col_full ">
                                    <label for="billing-form-city">Email:</label>
                                    <input type="text" id="billing-form-city" name="billing-form-city" value="{{user.email_gerente}}" class="sm-form-control" readonly/>
                                </div>
                                
                                <div class="col_half">
                                    <label for="billing-form-name">Nombre:</label>
                                    <input type="text" id="billing-form-name" name="billing-form-name" value="{{user.nombre_gerente}}" class="sm-form-control" readonly/>
                                </div>

                                <div class="col_half col_last">
                                    <label for="billing-form-lname">NIF / CIF:</label>
                                    <input type="text" id="billing-form-lname" name="billing-form-lname" value="{{user.cif}}" class="sm-form-control" readonly/>
                                </div>

                            </form>
                        </div>
                                <div class="line"></div>
                <div class="contact-widget">
                    <div class="contact-form-result"></div>
                                <form  action="{{baseurl}}/admin/cambiarPass" method="post">
                                <div class="col_full">Si quieres cambiar de contraseña puedes hacerlo a continuación</div>

                                <div class="col_half ">
                                    <label for="billing-form-city">Contraseña antigua:</label>
                                    <input type="password" id="pass" name="pass" placeholder="****" class="sm-form-control" />
                                </div>

                                <div class="col_half ">
                                    <label for="billing-form-email">Contraseña nueva:</label>
                                    <input type="password" id="new_pass" name="new_pass" placeholder="****" class="sm-form-control" />
                                </div>

                                <div class="col_half col_last">
                                    <label for="billing-form-city">Repetir contraseña nueva:</label>
                                    <input type="password" id="repeat" name="repeat" placeholder="****" class="sm-form-control" />
                                </div>


                                <div class="col-md-12">
                                    <button href="#" class="button button-3d fright">Cambiar contraseña</button>
                                </div>

                            </form>
                </div>
                        </div>

                    </div>

                </div>

                <div class="line visible-xs-block"></div>

            </div>

        </div>

    </div>

</section><!-- #content end -->
