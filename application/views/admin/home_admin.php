
<section id="slider" class="slider-parallax swiper_wrapper clearfix sliderAdmin">

    <div class="slider-parallax-inner">

        <div class="swiper-container swiper-parent">
            <div class="swiper-wrapper">
                <div class="swiper-slide dark" style="background-image: url('{{imgurl}}slider-home-admin.jpg');">
                    <div class="container clearfix">
                        <div class="slider-caption slider-caption-center">
                            <h2 data-caption-animate="fadeInUp">Bienvenido a Yokohama Bushido</h2>
                            <p data-caption-animate="fadeInUp" data-caption-delay="200">Edición 2018-2019</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</section>

