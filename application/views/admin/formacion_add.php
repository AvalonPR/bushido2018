<!-- Page Title
============================================= -->
<section id="page-title" class="admin_header">

    <div class="container clearfix">
        <h1>Añadir formación</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">
    <div class="contact-widget">

        <div class="contact-form-result"></div>

        <div class="content-wrap">

            <div class="container clearfix">

                <!--Facturación Item-->
                <h3>Nueva formación</h3>
                <div class="line-custom"></div>

                    <div class="col_half">
                        <label for="slider-title">Nombre de la formación</label>
                        <input type="text" id="nombre" name="nombre" value="" placeholder="Promoción Mayo 2018" class="sm-form-control" required/>
                    </div>
                    <div class="col_half col_last travel-date-group"> 
                        <label for="fechas">Fecha</label>
                        <div class="input-daterange input-group">
                            <span class="input-group-addon"></span>
                            <input type="text" class="sm-form-control tleft today datepicker travel-date-group " id="fecha" name="fecha" placeholder="MM/DD/YYYY">
                        </div>
                    </div>
                    <div class="col_full center">
                        <button class="button button-rounded button-reveal button-large button-yellow" onclick="saveForm()"><i class="material-icons">save</i><span>Guardar cambios</span></button>
                    </div>
                <div class="line"></div>
                <!--Fin facturación item-->

                <h2>Próximas formaciones</h2>
                <div id="proxFormsTable">
                    <div class="table-responsive">
                        <table id="ProxFormTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Nombre formación</th>
                                </tr>
                            </thead>
                            <tbody>
                                {%for formacion in proxForms%}
                                <tr>
                                    <td>{{formacion.fecha}}</td>
                                    <td>{{formacion.nombre}}</td>
                                </tr>
                                {%endfor%}
                            </tbody>
                        </table>
                    </div>
                </div>
                <!--Fin facturación item-->

                <div class="col-md-3 col-sm-3 col-xs-12 fleft back">
                    <a href="#" class="center col-md-12 col-xs-12 button button-rounded button-large button-black"><span>Volver</span></a>
                </div>

            </div>

        </div>
    </div>
</section><!-- #content end -->