<!-- Page Title
============================================= -->
<section id="page-title" class="usuarios_header">

    <div class="container clearfix">
        <h1>Usuarios</h1>
        <span></span>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">
                      
            
            <h3 style="margin: 0;" >Usuarios del programa</h3>
            <div style="margin: 1% 0 4% 0;" class="line-custom"></div>
            <div class="table-responsive">
                <table id="users_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="col-md-1" id="editar">&nbsp;</th>
                            <th class="col-md-3">Nombre</th>
                            <th class="col-md-2">CIF</th>
                            <th class="col-md-2"><span data-toggle="tooltip" data-placement="top" title="Tooltip on top">Payer</span></th>
                            <th class="col-md-2">Estado</th>
                            <th class="col-md-2">Viaje</th>
                        </tr>
                    </thead>
                    <tbody> 
                            {%for user in users%}
                            <tr>
                                <td class="click center">
                                    <a href="{{siteurl}}{%if userData.tipo=='2'%}comercial{%else%}admin{%endif%}/usuarios/edit/{{user.cod_user}}"><i class="icon-edit"></i></a>
                                </td>
                                <td class="click">{{user.razon_social}}</td>
                                <td class="click">{{user.cif}}</td>
                                <td class="click">{{user.cod_user}}</td>
                                <td class="click center">
                                    <p><label class="switch">
                                        <input class="switch-details" data-id="{{user.iduser}}" id="checkboxUser" type="checkbox"{%if user.estado=="1"%} checked{%endif%}>
                                        <span class="slider round"></span>
                                        </label>
                                    </p>
                                </td>
                                <td class="click center">{%if user.viaje==1%}<i class="material-icons">flight_takeoff</i> {%else%} <i class="material-icons">airplanemode_inactive</i>{%endif%}</td>
                                
                            </tr>
                            {%endfor%}
                        
                    </tbody>
                </table>
            </div>



        </div>

    </div>


</section><!-- #content end -->

<!--
<script type="text/javascript">
    $(document).ready( function () {
      $('#editar').dataTable( {
        "bSort": false
      } );
    } );

</script>-->