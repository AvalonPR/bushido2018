
<!-- Page Title
============================================= -->
<section id="page-title" class="admin_header">

    <div class="container clearfix">
        <h1>Campañas</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

            <div class="heading-block center">
                <h1>Camapañas recientes</h1>
                <span>Información de todas las campañas detalladas</span>
            </div>

            <!-- Posts
            ============================================= -->
            <div id="posts">

                <div class="entry clearfix">
                    <div class="entry-image">
                        <a href="" data-lightbox="image"><img class="image_fade" src="{{imgurl}}/portfolio/large/9.jpg" alt=""></a>
                    </div>
                    <div class="entry-title">
                        <h2><a href="blog-single.html">This is a Standard post with a Preview Image</a></h2>
                    </div>
                    <ul class="entry-meta clearfix">
                        <li><i class="icon-calendar3"></i> 23 Mayo 2018</li>
                        <li><a href="#"><i class="icon-user"></i> admin</a></li>
                        <li><i class="icon-folder-open"></i> <a href="#">General</a>, <a href="#">Media</a></li>
                    </ul>
                    <div class="entry-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, asperiores quod est tenetur in. Eligendi, deserunt, blanditiis est quisquam doloribus voluptate id aperiam ea ipsum magni aut perspiciatis rem voluptatibus officia eos rerum deleniti quae nihil facilis repellat atque vitae voluptatem libero at eveniet veritatis ab facere.</p>
                        <a href="{{siteurl}}/admin/campana/single"class="more-link">Leer más</a>
                    </div>
                </div>

            </div><!-- #posts end -->

            <!-- Posts
            ============================================= -->
            <div id="posts">

                <div class="entry clearfix">
                    <div class="entry-image">
                        <a href="" data-lightbox="image"><img class="image_fade" src="{{imgurl}}/portfolio/large/9.jpg" alt=""></a>
                    </div>
                    <div class="entry-title">
                        <h2><a href="blog-single.html">This is a Standard post with a Preview Image</a></h2>
                    </div>
                    <ul class="entry-meta clearfix">
                        <li><i class="icon-calendar3"></i> 23 Mayo 2018</li>
                        <li><a href="#"><i class="icon-user"></i> admin</a></li>
                        <li><i class="icon-folder-open"></i> <a href="#">General</a>, <a href="#">Media</a></li>
                    </ul>
                    <div class="entry-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, asperiores quod est tenetur in. Eligendi, deserunt, blanditiis est quisquam doloribus voluptate id aperiam ea ipsum magni aut perspiciatis rem voluptatibus officia eos rerum deleniti quae nihil facilis repellat atque vitae voluptatem libero at eveniet veritatis ab facere.</p>
                        <a href="{{siteurl}}/admin/campana/single"class="more-link">Leer más</a>
                    </div>
                </div>

            </div><!-- #posts end -->

            <!-- Posts
            ============================================= -->
            <div id="posts">

                <div class="entry clearfix">
                    <div class="entry-image">
                        <a href="" data-lightbox="image"><img class="image_fade" src="{{imgurl}}/portfolio/large/9.jpg" alt=""></a>
                    </div>
                    <div class="entry-title">
                        <h2><a href="blog-single.html">This is a Standard post with a Preview Image</a></h2>
                    </div>
                    <ul class="entry-meta clearfix">
                        <li><i class="icon-calendar3"></i> 23 Mayo 2018</li>
                        <li><a href="#"><i class="icon-user"></i> admin</a></li>
                        <li><i class="icon-folder-open"></i> <a href="#">General</a>, <a href="#">Media</a></li>
                    </ul>
                    <div class="entry-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, asperiores quod est tenetur in. Eligendi, deserunt, blanditiis est quisquam doloribus voluptate id aperiam ea ipsum magni aut perspiciatis rem voluptatibus officia eos rerum deleniti quae nihil facilis repellat atque vitae voluptatem libero at eveniet veritatis ab facere.</p>
                        <a href="{{siteurl}}/admin/campana/single"class="more-link">Leer más</a>
                    </div>
                </div>

            </div><!-- #posts end -->

            <!-- Pagination
            ============================================= -->
            <ul class="pager nomargin campanas_resume">
                <li class="previous"><a class="col-md-3 col-sm-4 col-xs-12" href="#">&larr; Más antiguas</a></li>
                <li class="next"><a class="col-md-3 col-sm-4 col-xs-12" href="#">Más nuevas &rarr;</a></li>
            </ul><!-- .pager end -->

        </div>

    </div>

</section><!-- #content end -->