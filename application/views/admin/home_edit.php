
<!-- Page Title
============================================= -->
<section id="page-title" class="admin_header">

    <div class="container clearfix">
        <h1>Textos Home</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">
        <div class="contact-widget">

            <div class="contact-form-result"></div>

    <div class="content-wrap">

            <form action="{{baseurl}}admin/saveTextos" method="post">
                <div class="container clearfix">
                    <!--Slider item-->
                    <h3>Footer slider: Usuario Taller </h3>
                    <div class="line-custom"></div>
                    <div class="row">
                        <div class="col_half">
                            <label for="slider-title">Título</label>
                            <input type="text" id="footer-title" name="textosTaller-titulo" value="{{textosTaller.titulo}}" class="sm-form-control"/>
                        </div>
                        <div                               class="col_half col_last">
                            <label for="slider-button">Texto</label>
                            <input maxlength="450" type="text" id="footer-text" name="textosTaller-texto" value="{{textosTaller.texto}}" class="sm-form-control"/>
                        </div>
                        <div class="col_half">
                            <label for="slider-button">Texto botón</label>
                            <input maxlength="40" type="text" id="footer-button-text" name="textosTaller-boton" value="{{textosTaller.boton}}" class="sm-form-control"/>
                        </div>
                        <div class="                    col_half col_last">
                            <label for="slider-subtitle">URL</label>
                            <input type="text" id="slider-url" name="textosTaller-url" value="{{textosTaller.url}}" class="sm-form-control"/>
                        </div>
                        <div class="col_full">
                            <label for="slider-on">Estado</label><br>
                            <input class="bt-switch" type="checkbox" name="textosTaller-estado" {%if textosTaller.estado=='1'%}checked{%endif%} data-on-color="themecolor">
                        </div>
                    </div>
                    <!--Fin slider item-->

                    <!--Slider item-->
                    <h3>Footer slider: Usuario Final </h3>
                    <div class="line-cus                tom"></div>
                    <div class="row">
                        <div class="col_half">
                            <label for="slider-title">Título</label>
                            <input type="text" id="footer-title" name="textosUfinal-titulo" value="{{textosUfinal.titulo}}" class="sm-form-control"/>
                        </div>
                        <div class="col_half col_last">
                            <label for="slider-button">Texto</label>
                            <input maxlength="450" type="text" id="footer-text" name="textosUfinal-texto" value="{{textosUfinal.texto}}" class="sm-form-control"/>
                        </div>
                        <div class="col_half">
                            <label for="slider-button">Texto botón</label>
                            <input maxlength="40" type="text" id="footer-button-text" name="textosUfinal-boton" value="{{textosUfinal.boton}}" class="sm-form-control"/>
                        </div>
                        <div class="col_half col_last">
                            <label for=    "slider-subtitle">URL</label>
                            <input type="text" id="slider-url" name="textosUfinal-url" value="{{textosUfinal.url}}" class="sm-form-control"/>
                        </div>
                        <div class="col_full">
                            <label for="slider-on">Estado</label><br>
                            <input class="bt-switch" type="checkbox" name="textosUfinal-estado" {%if textosUfinal.estado=='1'%}checked{%endif%} data-on-color="themecolor">
                        </div>
                    </div        >
                    <!--Fin slider item-->

                    <div class="line"></div>
                    <div class="fright">
                        <button class="button button-rounded button-reveal button-large button-yellow"><i class="material-icons">save</i><span>Guardar cambios</span></button>
                    </div>
                </div>  
            </form> 
        </div>
    </div>
</section><!-- #content end -->


