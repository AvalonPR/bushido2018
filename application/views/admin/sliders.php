
<!-- Page Title
============================================= -->
<section id="page-title"  class="admin_header">

    <div class="container clearfix">
        <h1>Mis Sliders</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">
    <div class="contact-widget">

        <div class="contact-form-result"></div>

        <div class="content-wrap">

            <div class="container clearfix">

                <!--Slider item-->
                <h3>Sliders</h3>
                <div class="line-custom"></div>            
                <div class="table-responsive">
                    <table id="slidersTable" class="table table-striped table-bordered center" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="col-md-1">Imagen</th>
                                <th class="col-md-2">Destinatario</th>
                                <th class="col-md-2">Desde - Hasta</th>
                                <th class="col-md-2">Título</th>
                                <th class="col-md-1">Estado</th>
                                <th class="col-md-1">Editar</th>
                                <th class="col-md-1">Eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                        {% for slider in sliders %}
                        <tr>
                            <td> <img src="{{imgurl}}sliders/{{slider.idsliders}}/{{slider.imagen}}"> </td>
                            <td class="click">{{slider.zona}}</td>
                            <td>15/02/2018 - 01/04/2018</td>
                            <td class="click"><a href="{{siteurl}}admin/slider/edit/{{slider.idsliders}}">{{slider.titulo|raw}}</a></td>
                            <td><input class="bt-switch" type="checkbox" id='estadoSlider' {%if slider.estado=='1'%}checked{%endif%} data-on-color="themecolor" data-id="{{slider.idsliders}}"></td>
                            <td><a href="{{siteurl}}admin/slider/edit/{{slider.idsliders}}"><i class="material-icons">edit</i></a></td>
                            <td><a href="{{siteurl}}admin/slider/edit/{{slider.idsliders}}"><i class="material-icons">delete_forever</i></a></td>
                        </tr>
                        {%endfor%}
            </tbody>
                </table>
            </div>


            <!--New slider-->
            <div class="fright">
                <a href="{{siteurl}}admin/slider/add"><i class="material-icons add_admin">add_circle</i></a>
            </div>

            <div class="line"></div>
        </div>

    </div>
</div>
</section><!-- #content end -->


