<!-- Page Title
============================================= -->
<section id="page-title" class="admin_header">

    <div class="container clearfix">
        <h1>Añadir formación</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">
    <div class="contact-widget">

        <div class="contact-form-result"></div>

        <div class="content-wrap">

            <div class="container clearfix">

                <!--Facturación Item-->
                <h3>Nueva formación</h3>
                <div class="line-custom"></div>
                <form method="post" action="{{baseurl}}admin/uploadForm" enctype="multipart/form-data">
                <div class="col_one_third">
                    <label for="slider-title">Seleccione la formación</label>
                    <select name="idformacion" data-live-search="true" class="selectpicker form-control bottommargin-sm">
                        <option value="" disabled selected>Formación</option>
                        {%for formacion in pastForms%}
                        <option value="{{formacion.idformacion}}">{{formacion.nombre}}</option>
                        {%endfor%}
                    </select>
                </div>
                <div class="col_two_third col_last"> 
                    <label for="excel">Excel</label>
                    <input id="input-1" name="formExcel" type="file" class="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                </div>
                <div class="col_full center">
                    <button class="button button-rounded button-reveal button-large button-yellow"><i class="material-icons">save</i><span>Guardar cambios</span></button>
                </div>
                </form>
                <div class="line"></div>
                <!--Fin facturación item-->


                <div class="col-md-3 col-sm-3 col-xs-12 fleft back">
                    <a href="#" class="center col-md-12 col-xs-12 button button-rounded button-large button-black"><span>Volver</span></a>
                </div>

            </div>

        </div>
    </div>
</section><!-- #content end -->