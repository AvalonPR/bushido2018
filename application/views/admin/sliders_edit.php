
<!-- Page Title
============================================= -->
<section id="page-title" class="admin_header">

    <div class="container clearfix">
                <h1>Editando slider: </h1><br>
                <h2>{{slider.titulo|raw}}</h2>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">
    <div class="contact-widget">

        <div class="contact-form-result"></div>

        <div class="content-wrap">
            <form action="{{baseurl}}admin/addSlider/{{slider.idsliders}}" method="post" enctype="multipart/form-data">

                <div class="container clearfix">
                    
                 
                    <!--Slider item-->
                    <h3>Imagen Actual</h3>
                    <div class="line-custom"></div>   
                    <div class="row">
                        <div class="col-md-12">
                          <div><img src="{{imgurl}}/sliders/{{slider.idsliders}}/{{slider.imagen}}"></div>
                        </div>
                    </div>

                    <!--Slider item-->
                    <h3>Configuración slider</h3>
                    <div class="line-custom"></div>
                    <div class="row">

                        <div class="col_half bottommargin-sm">
                            <label for="slider-title">Destinatario final:</label>
                            <ul style="list-style: none;">
                                <li class="select_user">
                                    <select name="zona" class="selectpicker col-md-12 col-sm-12 col-xs-12" required>
                                        <option>[Elige uno]</option>
                                        <option {%if slider.zona=='taller'%}selected{%endif%} value="taller">Usuario Taller</option>
                                        <option {%if slider.zona=='uFinal'%}selected{%endif%} value="uFinal">Usuario Final</option>
                                    </select>
                                </li>
                            </ul>
                        </div>
                        <div class="col_half col_last bottommargin-sm file_ups">
                            <label>Imagen <span style="font-weight: 500;"> Tamaño: 1665x500 pixels Extensión: .jpg, .png.</span></label><br>
                            <input name="imagen" id="input-1" type="file" class="file">
                        </div>
                        <div class="col_half">
                            <label for="slider-title">Título</label>
                            <input type="text" id="slider-title" name="titulo" value="{{slider.titulo}}" placeholder="Promoción Mayo 2018" class="sm-form-control" required/>
                        </div>
                        <div class="col_half col_last">
                            <label for="slider-subtitle">Subtítulo</label>
                            <input type="text" id="slider-subtitle" name="subtitulo" value="{{slider.subtitulo}}" placeholder="Oferta especial en neumáticos" class="sm-form-control"/>
                        </div>
                        <div class="col_full">
                            <div class="col-md-3 col-sm-3  col-xs-6 nobottommargin-xs travel-date-group">
                                <label for="slider-button">Texto del botón</label>
                                <input type="text" id="slider-button" name="boton" value="{{slider.boton}}" placeholder="Saber más" class="sm-form-control" required/>
                            </div> 
                            <div class="col-md-3 col-sm-3  col-xs-6 nobottommargin-xs travel-date-group">
                                <label for="slider-on">Estado</label><br>
                                <input class="bt-switch" {%if slider.estado=='1'%}checked{%endif%} type="checkbox" name="estado" checked data-on-color="themecolor">
                            </div>  
                            <div class="col-md-6 col-sm-6  col-xs-12 nobottommargin-xs travel-date-group">
                                <label for="fechas">Activo</label>
                                <div class="input-daterange input-group">
                                    <span class="input-group-addon">Desde</span>
                                    <input type="text" class="sm-form-control tleft today datepicker travel-date-group " name="fechaI" placeholder="MM/DD/YYYY" value="{{slider.fechaI}}">
                                           <span class="input-group-addon">Hasta</span>
                                    <input type="text" class="sm-form-control tleft today datepicker" name="fechaF" placeholder="MM/DD/YYYY" value="{{slider.fechaF}}">
                                </div>
                            </div>
                        </div>
                        <div class="col_full"><br/>
                            <!--Url externa view-->
                            <div class="col-md-6">
                                <input id="url" class="radio-style" name="url" checked name="tipo" type="radio" value="{{slider.url}}" placeholder="url">
                                <label for="url" class="radio-style-2-label">Url externa</label>
                            </div>
                            <!--Página view-->
                            <div class="col-md-6">
                                <input id="pagina" class="radio-style" name="tipo" type="radio" value="pagina">
                                <label for="pagina" class="radio-style-2-label">Cambiar página</label>
                            </div>
                        </div>
                        <!--Div oculto url-->
                        <div class="col_half" id="url_slider_show"><br/>
                            <input type="text" id="slider-url" name="url" value="" placeholder="http://..." class="sm-form-control"/>
                        </div>
                        <!--Fin div oculto url-->

                        <!--Div oculto página nueva-->
                        <div id="new_page_slider_show" style="display:none;">
                            <div class="col_full">
                                <br>
                                <h3>Esto creará una nueva página</h3>
                                <br>
                                <h3>Elige el estilo de tu página</h3>
                                <br>
                                <ul style="list-style: none;">
                                    <li class="col-md-4 col-sm-4">
                                        <select class="selectpicker">
                                            <option>[Style]</option>
                                            <option>Paragraph</option>
                                            <option>Heading 1</option>
                                            <option>Heading 2</option>
                                        </select>
                                    </li>
                                    <li class="col-md-4 col-sm-4">
                                        <select class="selectpicker">
                                            <option>[Fuente]</option>
                                            <option>Arial</option>
                                            <option>Helvetica</option>
                                            <option>Continental Stag</option>
                                        </select>
                                    </li>
                                    <li class="col-md-4 col-sm-4">
                                        <select class="selectpicker">
                                            <option>[Size]</option>
                                            <option>12</option>
                                            <option>14</option>
                                            <option>16</option>
                                        </select>
                                    </li>
                                </ul>
                            </div>
                            <div class="col_full" id="admin_edit">
                                <div class="col_full">
                                    <ul style="margin-bottom: 0" class="col-md-12 col-sm-12 col-xs-12 admin_edit_mb">
                                        <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/bold.png"></li>
                                        <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/italic.png"></li>
                                        <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/underline.png"></li>
                                        <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/left_just.png"></li>
                                        <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/centre.png"></li>
                                        <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/right_just.png"></li>
                                        <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/hr.png"></li>
                                        <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/numbered_list.png"></li>
                                        <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/list.png"></li>
                                        <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/outdent.png"></li>
                                        <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/indent.png"></li>
                                        <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/textcolor.png"></li>
                                    </ul>
                                    <ul class="col-md-12 col-sm-12 colxs-12 admin_edit_mb">
                                        <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/bgcolor.png"></li>
                                        <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/hyperlink.png"></li>
                                        <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/unlink.png"></li>
                                        <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/image.png"></li>
                                        <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/insert_table.png"></li>
                                        <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/video.png"></li>
                                        <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/undo.png"></li>
                                        <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/redo.png"></li>
                                        <li class="col-md-1 col-sm-1 col-xs-2 center"><img src="{{imgurl}}icons/removeformat.png"></li>
                                    </ul>
                                </div>
                            </div>
                            <iframe class="col_full" name="editable" src="" id="edit_content">

                            </iframe>
                        </div>
                        <!--Fin div oculto página nueva-->

                    </div>
                    <!--Fin slider item-->

                    <div class="line"></div>
                    <div class="col-md-4 fleft">
                        <a href="{{baseurl}}admin/slider" class="button button-rounded button-large button-black"><span>Volver</span></a>
                    </div>
                    <div class="col-md-4 fright">
                        <button class="button button-rounded button-reveal button-large button-yellow"><i class="material-icons">save</i><span>Guardar cambios</span></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section><!-- #content end -->
<!-- Page Title
============================================= -->
<section id="page-title">

    <div class="container clearfix">
        <h1>Mis Sliders</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">
    <div class="contact-widget">

        <div class="contact-form-result"></div>

        <div class="content-wrap">

            <div class="container clearfix">

                <!--Slider item-->
                <h3>Slider 1: Usuario Taller</h3>
                <div class="line-custom"></div>
                <div class="row">
                    <div class="col_half bottommargin-sm">
                        <label>Imagen <span style="font-weight: 500;"> Tamaño: 1665x500 pixels Extensión: .jpg, .png.</span></label><br>
                        <input id="input-1" type="file" class="file">
                    </div>
                    <div class="col_half">
                        <label for="slider-title">Título</label>
                        <input type="text" id="slider-title" name="slider-title" value="Promoción Mayo 2018" class="sm-form-control"/>
                    </div>
                    <div class="col_half col_last">
                        <label for="slider-subtitle">Subtítulo</label>
                        <input type="text" id="slider-subtitle" name="slider-subtitle" value="Oferta especial en neumáticos" class="sm-form-control"/>
                    </div>
                    <div class="col_half">
                        <label for="slider-button">Texto del botón</label>
                        <input type="text" id="slider-button" name="slider-button" value="Saber más" class="sm-form-control"/>
                    </div>
                    <div class="col_half col_last">
                        <label for="slider-subtitle">URL</label>
                        <input type="text" id="slider-url" name="slider-url" value="https://..." class="sm-form-control"/>
                    </div>
                    <div class="col_full">
                        <label for="slider-on">Estado</label><br>
                        <input class="bt-switch" type="checkbox" checked data-on-color="themecolor">
                    </div>
                </div>
                <!--Fin slider item-->

                <!--Slider item-->
                <h3>Slider 2: Usuario Taller</h3>
                <div class="line-custom"></div>
                <div class="row">
                    <div class="col_half bottommargin-sm">
                        <label>Imagen <span style="font-weight: 500;"> Tamaño: 1665x500 pixels Extensión: .jpg, .png.</span></label><br>
                        <input id="input-1" type="file" class="file">
                    </div>
                    <div class="col_half">
                        <label for="slider-title">Título</label>
                        <input type="text" id="slider-title" name="slider-title" value="Promoción Mayo 2018" class="sm-form-control"/>
                    </div>
                    <div class="col_half col_last">
                        <label for="slider-subtitle">Subtítulo</label>
                        <input type="text" id="slider-subtitle" name="slider-subtitle" value="Oferta especial en neumáticos" class="sm-form-control"/>
                    </div>
                    <div class="col_half">
                        <label for="slider-button">Texto del botón</label>
                        <input type="text" id="slider-button" name="slider-button" value="Saber más" class="sm-form-control"/>
                    </div>
                    <div class="col_half col_last">
                        <label for="slider-subtitle">URL</label>
                        <input type="text" id="slider-url" name="slider-url" value="https://..." class="sm-form-control"/>
                    </div>
                    <div class="col_full">
                        <label for="slider-on">Estado</label><br>
                        <input class="bt-switch" type="checkbox" checked data-on-color="themecolor">
                    </div>
                </div>
                <!--Fin slider item-->

                <!--Slider item-->
                <h3>Slider 1: Usuario Final</h3>
                <div class="line-custom"></div>
                <div class="row">
                    <div class="col_half bottommargin-sm">
                        <label>Imagen <span style="font-weight: 500;"> Tamaño: 1665x500 pixels Extensión: .jpg, .png.</span></label><br>
                        <input id="input-1" type="file" class="file">
                    </div>
                    <div class="col_half">
                        <label for="slider-title">Título</label>
                        <input type="text" id="slider-title" name="slider-title" value="Promoción Mayo 2018" class="sm-form-control"/>
                    </div>
                    <div class="col_half col_last">
                        <label for="slider-subtitle">Subtítulo</label>
                        <input type="text" id="slider-subtitle" name="slider-subtitle" value="Oferta especial en neumáticos" class="sm-form-control"/>
                    </div>
                    <div class="col_half">
                        <label for="slider-button">Texto del botón</label>
                        <input type="text" id="slider-button" name="slider-button" value="Saber más" class="sm-form-control"/>
                    </div>
                    <div class="col_half col_last">
                        <label for="slider-subtitle">URL</label>
                        <input type="text" id="slider-url" name="slider-url" value="https://..." class="sm-form-control"/>
                    </div>
                    <div class="col_full">
                        <label for="slider-on">Estado</label><br>
                        <input class="bt-switch" type="checkbox" checked data-on-color="themecolor">
                    </div>
                </div>
                <!--Fin slider item-->

                <!--Slider item-->
                <h3>Slider 2: Usuario Final</h3>
                <div class="line-custom"></div>
                <div class="row">
                    <div class="col_half bottommargin-sm">
                        <label>Imagen <span style="font-weight: 500;"> Tamaño: 1665x500 pixels Extensión: .jpg, .png.</span></label><br>
                        <input id="input-1" type="file" class="file">
                    </div>
                    <div class="col_half">
                        <label for="slider-title">Título</label>
                        <input type="text" id="slider-title" name="slider-title" value="Promoción Mayo 2018" class="sm-form-control"/>
                    </div>
                    <div class="col_half col_last">
                        <label for="slider-subtitle">Subtítulo</label>
                        <input type="text" id="slider-subtitle" name="slider-subtitle" value="Oferta especial en neumáticos" class="sm-form-control"/>
                    </div>
                    <div class="col_half">
                        <label for="slider-button">Texto del botón</label>
                        <input type="text" id="slider-button" name="slider-button" value="Saber más" class="sm-form-control"/>
                    </div>
                    <div class="col_half col_last">
                        <label for="slider-subtitle">URL</label>
                        <input type="text" id="slider-url" name="slider-url" value="https://..." class="sm-form-control"/>
                    </div>
                    <div class="col_full">
                        <label for="slider-on">Estado</label><br>
                        <input class="bt-switch" type="checkbox" checked data-on-color="themecolor">
                    </div>
                </div>
                <!--Fin slider item-->

                <!--New slider-->
                <div class="fright">
                    <a href="{{siteurl}}/admin/slider/add"><i class="material-icons add_admin">add_circle</i></a>
                </div>

                <div class="line"></div>
                <div class="fright">
                    <a href="#" class="button button-rounded button-reveal button-large button-yellow"><i class="material-icons">save</i><span>Guardar cambios</span></a>
                </div>
            </div>

        </div>
    </div>
</section><!-- #content end -->
