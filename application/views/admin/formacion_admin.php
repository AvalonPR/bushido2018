<!-- Page Title
        ============================================= -->
<section id="page-title" class="admin_header">

    <div class="container clearfix">
        <h1>Formaciones</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
        ============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">
            <h2>Próximas formaciones</h2>
            <div class="table-responsive">
                <table id="ProxFormTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Nombre formación</th>
                        </tr>
                    </thead>
                    <tbody>
                        {%for formacion in proxForms%}
                        <tr>
                            <td>{{formacion.fecha}}</td>
                            <td>{{formacion.nombre}}</td>
                        </tr>
                        {%endfor%}
                    </tbody>
                </table>
            </div>
            <h2>Formaciones realizadas</h2>
            <div class="table-responsive">
                <table id="FormReal_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Nombre formación</th>
                        </tr>
                    </thead>
                    <tbody>
                        {%for formacion in pastForms%}
                        <tr>
                            <td>{{formacion.fecha}}</td>
                            <td><a href="{{baseurl}}{%if userData.tipo=='2'%}comercial{%else%}admin{%endif%}/verFormaciones/{{formacion.idformacion}}">{{formacion.nombre}}</a></td>
                        </tr>
                        {%endfor%}
                    </tbody>
                </table>
            </div>

            <!--Fin tabla datos-->
        </div>
    </div>

</section><!-- #content end -->
