<!-- Page Title
    ============================================= -->
<section id="page-title" class="admin_header">

    <div class="container clearfix">
        <h1>Importar facturación</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
    ============================================= -->
<section id="content">
    <div class="content-wrap">

        <div class="container clearfix">
            
                <!--Facturación Item-->
                <h3 style="margin-top: 0px;">Facturación maestra</h3>
                <div class="line-custom"></div>
                <div class="contact-widget">
                    <div class="contact-form-result"></div>
                    
                        <form method="post" action="http://localhost/Rally2018/admin/factMaestra" enctype="multipart/form-data">
                            
                                <div class="col-md-9 col-sm-8 col-xs-12">
                                    <label>Archivo</label><br>
                                    <input id="input-1" type="file" name="factMaestra" class="file">
                                </div>
                            
                            <div class="clear"></div>
                                <div class="upload-button">
                                    <button class="center button button-rounded button-reveal button-large button-yellow" name="actionFactMaestro" type="submit" value="subir"><i class="material-icons">file_upload</i><span>Subir</span></button>
                                </div>
                            
                        </form>
                    
                </div>
                <div class="line line-narrow"></div>
                <!--Fin facturación item-->

                <!--Facturación Item-->
                <h3>Facturación</h3>
                <div class="line-custom"></div>
                <div class="contact-widget">
                    <div class="contact-form-result"></div>
                    
                        <form method="post" action="http://localhost/Rally2018/admin/fact" enctype="multipart/form-data">
                            
                                <div class="col-md-9 col-sm-8 col-xs-12">
                                    <label>Archivo</label><br>
                                    <input id="input-1" type="file" name="fact" class="file">
                                </div>
                            <div class="clear"></div>
                            <div class="upload-button">
                               
                                    <button class="center button button-rounded button-reveal button-large button-yellow" name="actionFact" type="submit" value="subir"><i class="material-icons">file_upload</i><span>Subir</span></button>
                            
                            </div>
                        </form>
                    
                </div>
                <div class="line line-narrow"></div>
                <!--Fin facturación item-->

                <!--Facturación Item-->
                <h3>Fichero cruce</h3>
                <div class="line-custom"></div>
                <div class="contact-widget">
                    <div class="contact-form-result"></div>
                    
                        <form method="post" action="http://localhost/Rally2018/admin/cruce" enctype="multipart/form-data">
                            
                            <div class="col-md-9 col-sm-8 col-xs-12">
                                <label>Archivo</label><br>
                                <input id="input-1" type="file" name="cruce" class="file">
                            </div>
                            
                            <div class="clear"></div>
                                <div class="upload-button">
                                    <button class="col-md-12 col-sm-12 col-xs-8 center button button-rounded button-reveal button-large button-yellow" name="actionCruce" type="submit" value="subir"><i class="material-icons">file_upload</i><span>Subir</span></button>
                                </div>
                        </form>
                    
                </div>
                
                <div class="line line-narrow"></div>
                <!--Fin facturación item-->
                <div class="col-md-3 col-sm-3 col-xs-12 fright back">
                    <a href="#" class="center col-md-12 col-xs-12 button button-rounded button-large button-black"><span>Volver</span></a>
                </div>
            
        </div>
    </div>
</section><!-- #content end -->