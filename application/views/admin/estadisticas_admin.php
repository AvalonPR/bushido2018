<!-- Page Title
============================================= -->
<section id="page-title" class="admin_header">

    <div class="container clearfix">
        <h1>Estadísticas</h1>
        <span>Información del progreso de tus usuarios</span>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">
            <!--<div class="col_full">
                <div class="col-sm-6 bottommargin-sm">
                    <label for="">Fecha de búsqueda</label>
                    <div class="input-daterange input-group">
                        <input type="text" value="" class="sm-form-control tleft" placeholder="MM/DD/YYYY">
                        <span class="input-group-addon">hasta</span>
                        <input type="text" value="" class="sm-form-control tleft" placeholder="MM/DD/YYYY">
                    </div>
                </div>
                <div class="col-sm-6 bottommargin-sm">
                    <label for="">Familias</label>
                    <select class="select-1 form-control" style="width:100%;">
                        <optgroup label="Neumáticos">
                            <option value="CA">Continental STD</option>
                            <option value="NV">Continental UHP</option>
                            <option value="OR">Barum</option>
                        </optgroup>
                        <optgroup label="Recambios">
                            <option value="AZ">Cepsa</option>
                            <option value="CO">Energy Expert</option>
                            <option value="ID">Lubri Expert</option>
                            <option value="MT">Tudor</option>
                            <option value="NE">Doga</option>
                        </optgroup>
                    </select>
                </div>
            </div>

            <div class="col_full">
                {%if userData.tipo=='3'%}
                <div class="col-sm-4 bottommargin-sm">
                    <label for="">Comercial</label>
                    <select class="select-1 form-control" style="width:100%;">
                        {%for c in comerciales%}
                        <option value="{{c.nombre_gerente}}">{{c.nombre_gerente}}</option>
                        {%endfor%}
                    </select>
                </div>
                {%endif%}
                <div class="col-sm-{%if userData.tipo=='3'%}4{%else%}6{%endif%} bottommargin-sm">
                    <label for="">Provincia</label>
                    <select class="select-1 form-control" style="width:100%;">
                        {%for p in provincia%}
                        <option value="{{p.provincia}}">{{p.provincia|capitalize}}</option>
                        {%endfor%}
                    </select>
                </div>
                <div class="col-sm-{%if userData.tipo=='3'%}4{%else%}6{%endif%} bottommargin-sm">
                    <label for="">Taller</label>
                    <select class="select-1 form-control" style="width:100%;">
                    </select>
                </div>
            </div>
            <div class="col_full">
                <a href="" class="fright col-md-2 button">Aplicar filtros</a>
            </div>
            <div class="line" ></div>-->
	
            <div class="graficas">
                <div>
                    <ul class="tabs-admin clearfix">
                        <li class="grafica-compras active">Compras</li>
                    </ul>
                </div>
                
                <div id="grafica1">
                    <div class="" id="lineChart">
                        <h3 class="center">Histórico en €</h3>
                        <canvas id="lineChartCanvas" width="1140" height="300"></canvas>
                    </div>
                    <div class="leyenda">
                        <div id="leyend18"><div class="leyend18" style="background-color:#ffa500;"></div><span> 2018 /</span></div>
                        <div id="leyend19"><div class="leyend19" style="background-color:#8c8c8c;"></div><span> 2019</span></div>
                    </div>
                </div>
                
                <div class="clear"></div>
            </div>
            
            
            <div class="line"></div>
            
            
            <br><br><h3 style="margin: 0;" >Datos</h3>
            <div style="margin: 1% 0 4% 0;" class="line-custom"></div>
            <!--<div class="col_full"><a class="button button-border button-rounded button-fill fill-from-top button-amber" download="#" href="#"><span>Descargar datos en excel </span><i class="material-icons" style="vertical-align: middle;">file_download</i></a></div>-->
            <div class="table-responsive">
                <table id="estadisticas_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="col-md-4">Nombre cliente</th>
                            <th class="col-md-3">Código usuario</th>
                            <th class="col-md-3">Puntos obtenidos</th>
                            <th class="col-md-3">Puntos disponibles</th>
                            <th class="col-md-3">Más información</th>
                        </tr>
                    </thead>
                    <tbody> 
                                {%set user = '0'%}
                    {%for key, fact in facturacion%}
                                          {%set user = key %}
                        {%for p in fact%}
                            <tr>
                                <td class="click"><a href="{{base_url()}}{%if userData.tipo=='S'%}comercial{%else%}admin{%endif%}/estadisticas/{{user}}">{{p.nombre}}</a></td>
                                <td class="click">{{user}}</td>
                                <td class="click">{%if viaje==1%}0{%else%}{{p.obtenidos|number_format(0, '.', '')}}{%endif%}</td>
                                <td class="click">{%if viaje==1%}0{%else%}{{p.disponibles|number_format(0, '.', '')}}{%endif%}</td>
                                <td class="click"><button class="button check_estads" data-toggle="modal" data-target="#informacion{{user}}"><i class="material-icons" style="color: #dc0026; cursor:pointer;">info</i></button></td>                                            
                            </tr>
                        {%endfor%}
                    {%endfor%}
                    </tbody>
                </table>
            </div>



        </div>

    </div>


</section><!-- #content end -->

                                {%set user = '0'%}
                    {%for key, fact in facturacion%}
                                          {%set user = key %}
<!-- Modal formación -->
<div class="modal fade" id="informacion{{user}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-body">
            <div class="modal-content">
                    {%for p in fact%}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title modal-title-formacion" id="myModalLabel">Más información sobre <span class="contitrade">{{p.nombre}}</span></h4>
                </div>
                <div class="modal-body">
                    <div class="opening-table">
                        
                        <div class="time-table-wrap clearfix">
                            <div class="time-table clearfix">
                                <h2 class="notopmargin">Facturación</h2>
                                <span class="col-md-9"><b>Facturación 2018: </b></span>
                                <span class="col-md-3">{{p.facturacionpv|number_format(2, '.', '')}}</span>
                                <span class="col-md-9"><b>Facturación 2019: </b></span>
                                <span class="col-md-3">{{p.facturacionact|number_format(2, '.', '')}}</span>
                        {%if p.viaje == 1%}
                        <h5 class="notopmargin nobottommargin">El usuario va al viaje</h5>
                                <span class="col-md-9"><b>Facturación viaje: </b></span>
                                <span class="col-md-3">{{p.facturacionViaje|number_format(2, '.', '')}}</span>
                        {%endif%}
                                <h2 class="notopmargin">Dirección:</h2>
                            </div>
                        </div>
                    </div>
                </div>
                {%endfor%}
            </div>
        </div>
    </div>
</div>
<!--Fin modal formación-->
{%endfor%}

                 
<script type="text/javascript">
    /*
    
jQuery(window).load(function () {
    
    var lineChartData = {
        
        labels: [
            {%for k, sum in factMeses.2016%}
            "{{k}}", 
            {%endfor%}
        ],
        datasets: [
            {   
                fillColor : "rgba(243,134,48,0.3)",
                strokeColor : "#F38630",
                pointColor : "#F38630",
                pointStrokeColor : "#fff",
                data: [
                    {%for sum in factMeses.2016%}    
                    {{sum|number_format(2, '.', '')}}, 
                        {%endfor%}
                    ]
            },
            
            {
                    fillColor : "rgba(51,51,51,0.3)",
                    strokeColor : "#333",
                    pointColor : "#333",
                    pointStrokeColor : "#fff",
                    data: [
                        {%for sum in factMeses.2017%}    
                        {{sum|number_format(2, '.', '')}}, 
                            {%endfor%}
                        ]
            },
            {   
                fillColor : "rgba(255,165,0,0.3)",
                strokeColor : "#ffa500",
                pointColor : "#ffa500",
                pointStrokeColor : "#fff",
                data: [
                    {%for sum in factMeses.2018%}    
                    {{sum|number_format(2, '.', '')}}, 
                        {%endfor%}
                    ]
            },
            {
                    fillColor : "rgba(140,140,140,0.3)",
                    strokeColor : "#8c8c8c",
                    pointColor : "#8c8c8c",
                    pointStrokeColor : "#fff",
                    data: [
                        {%for sum in factMeses.2019%}    
                        {{sum|number_format(2, '.', '')}}, 
                            {%endfor%}
                        ]
            }
            
        ]
    };    
    
        
    	var lineChartData2 = {
				labels : [
            
            {%for k, sum in consumo.2017%}
            "{{k}}", 
            {%endfor%}                    
            ],
				datasets : [
					{
                fillColor : "rgba(243,134,48,0.3)",
                strokeColor : "#F38630",
                pointColor : "#F38630",
                pointStrokeColor : "#fff",
						data : [
                                                    
                    {%for sum in consumo.2016%}    
                    {{sum|number_format(2, '.', '')}}, 
                        {%endfor%}
                    ]
					},
					{
                    fillColor : "rgba(51,51,51,0.3)",
                    strokeColor : "#333",
                    pointColor : "#333",
                    pointStrokeColor : "#fff",
						data : [
                    {%for sum in consumo.2017%}    
                    {{sum|number_format(2, '.', '')}}, 
                        {%endfor%}
                    ]
					},
					{
                fillColor : "rgba(255,165,0,0.3)",
                strokeColor : "#ffa500",
                pointColor : "#ffa500",
                pointStrokeColor : "#fff",
						data : [
                    {%for sum in consumo.2018%}    
                    {{sum|number_format(2, '.', '')}}, 
                        {%endfor%}
                    ]
					},
            {
                    fillColor : "rgba(140,140,140,0.3)",
                    strokeColor : "#8c8c8c",
                    pointColor : "#8c8c8c",
                    pointStrokeColor : "#fff",
                    data : [
                    {%for sum in consumo.2019%}    
                    {{sum|number_format(2, '.', '')}}, 
                        {%endfor%}
                    ]
            }
				]
			};
    
    var globalGraphSettings = {animation: Modernizr.canvas};
    
    function showLineChart(){
				var ctx = document.getElementById("lineChartCanvas").getContext("2d");
				new Chart(ctx).Line(lineChartData,globalGraphSettings);
				var ctx2 = document.getElementById("lineChartCanvas2").getContext("2d");
				new Chart(ctx2).Line(lineChartData2,globalGraphSettings);
			}
    
    $('#lineChart').appear( function(){ $(this).css({ opacity: 1 }); setTimeout(showLineChart,300); },{accX: 0, accY: -155},'easeInCubic');
    $('#lineChart2').appear( function(){ $(this).css({ opacity: 1 }); setTimeout(showLineChart,300); },{accX: 0, accY: -155},'easeInCubic');
   });*/
 </script>

       
