

<!-- Page Title
                ============================================= -->
<section id="page-title" class="usuarios_header">

    <div class="container clearfix">
        <h1>Usuario</h1>
        <span>{%if user.razon_social%}{{user.razon_social}}{%else%}Nuevo usuario{%endif%}</span>
    </div>

</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

            <div class="row clearfix">

                <div class="col-sm-12">

                    

                    <div class="clear"></div>
                    <div class="row clearfix">

                        <div class="col-md-12">

                <div class="contact-widget">
                    <div class="contact-form-result"></div>

                             {%if user.razon_social%}<p class="">Si necesitas editar algún dato que no puedas desde aquí. Por favor ponte en contacto con nosotros mandando un email a <a href="mailto:contacto@bestdrive.plus">contacto@bestdrive.plus</a></p>{%endif%}
                            <form id="user-form" name="user-form" action="{{baseurl}}/admin/upsertDatos/{{user.iduser}}" method="post">
                                <div class="col_half">
                                    <label for="billing-form-name">Razon Social:</label>
                                    <input type="text" id="billing-form-name" name="razonSocial" value="{{post.razonSocial}}{{user.razon_social}}" class="sm-form-control" {%if user.razon_social%}readonly{%endif%}/>
                                </div>
                                <div class="col_half col_last">
                                    <label for="billing-form-name">Nombre clave Continental:</label>
                                    <input type="text" id="billing-form-name" name="nombreClave" value="{{user.nombreClave}}" class="sm-form-control" {%if user.nombreClave%}readonly{%endif%}/>
                                </div>

                                <div class="col_one_third">
                                    <label for="billing-form-lname">NIF / CIF:</label>
                                    <input type="text" id="billing-form-lname" name="cif" value="{{user.cif}}" class="sm-form-control" {%if user.cif%}readonly{%endif%}/>
                                </div>
                                <div class="col_one_third">
                                    <label for="billing-form-lname">Payer:</label>
                                    <input type="text" id="billing-form-lname" name="cod_user" value="{{user.cod_user}}" class="sm-form-control" {%if user.cod_user%}readonly{%endif%}/>
                                </div>
                                <div class="col_one_third col_last">
                                    <label for="billing-form-name">Nombre persona de contacto:</label>
                                    <input type="text" id="billing-form-name" name="name" value="{{user.nombre_gerente|capitalize}}" class="sm-form-control"/>
                                </div>
                                <div class="col_half">
                                    <label for="billing-form-name">Nombre comercial:</label>
                                    <input type="text" id="billing-form-name" name="nombreComercial" value="{{user.nombre_comercial|capitalize}}" class="sm-form-control"/>
                                </div>
                                <div class="col_half col_last">
                                    <label >Coordinador:</label>
                                    <select name="coordinador" class="select-1 form-control" style="width:100%;">
                                        {%for c in comerciales%}
                                        <option{% if user.dni_comercial==c.cif%} selected {%endif%} value="{{c.cif}}">{{c.nombre_gerente}}</option>
                                        {%endfor%}
                                    </select>
                                </div>

                                <div class="clear"></div>

                                <div class="col_full">
                                    <label for="billing-form-companyname">Dirección</label>
                                    <input type="text" id="billing-form-companyname" name="direccion" value="{{user.direccion|capitalize}}" class="sm-form-control" />
                                </div>

                                <div class="col_one_third">
                                    <label for="billing-form-address">Población:</label>
                                    <input type="text" id="billing-form-address" name="poblacion" value="{{user.poblacion|capitalize}}" class="sm-form-control" />
                                </div>
                                <div class="col_one_third">
                                    <label for="billing-form-address">Provincia:</label>
                                    <input type="text" id="billing-form-address" name="provincia" value="{{user.provincia|capitalize}}" class="sm-form-control" />
                                </div>
                                <div class="col_one_third col_last">
                                    <label for="billing-form-address">Código Postal:</label>
                                    <input type="text" id="billing-form-address" name="cp" value="{{user.cp}}" class="sm-form-control" />
                                </div>

                                <div class="col_half ">
                                    <label for="billing-form-city">Email:</label>
                                    <input type="text" id="billing-form-city" name="email" value="{{user.email_gerente}}" class="sm-form-control" />
                                </div>

                                <div class="col_half col_last">
                                    <label for="billing-form-email">Teléfono:</label>
                                    <input type="number" id="billing-form-email" name="phone" value="{{user.telefono}}" class="sm-form-control" />
                                </div>

                                <div class="col_full">
                                    <label for="billing-form-email">Comentarios:</label>
                                    <textarea class="form-control" name="comentarios" id="exampleFormControlTextarea1" rows="3">{{user.comentarios}}</textarea>
                                </div>
                                
                                <div class="col-md-12">
                                    <button href="#" class="button button-3d fright" type="submit">Guardar datos</button>
                                </div>
                            </form>
                        </div>
                                <div class="line"></div>
                
                        </div>

                    </div>

                </div>

                <div class="line visible-xs-block"></div>

            </div>

        </div>

    </div>

</section><!-- #content end -->
