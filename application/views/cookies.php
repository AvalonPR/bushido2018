<!-- Page Title
============================================= -->
<section id="page-title" class="header programa_header">

    <div class="container clearfix">
        <h1>Política de cookies</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
        ============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix legal">
            <!-- Post Content
            ============================================= -->
            <div class="postcontent nobottommargin col_last clearfix">

                <h2>POLÍTICA DE COOKIES</h2>
                <p>YOKOHAMA IBERIA, S.A., de conformidad y en cumplimiento con el artículo 22.2 de la Ley 34/2002, de 11 de julio, de servicios de la sociedad de la información y de comercio electrónico (LSSI) establece que:</p>
                <p>Los prestadores de servicios podrán utilizar dispositivos de almacenamiento y recuperación de datos en equipos terminales de los destinatarios, a condición de que los mismos hayan dado su consentimiento después de que se les haya facilitado información clara y completa sobre su utilización, en particular, sobre los fines del tratamiento de los datos, con arreglo a lo dispuesto en la Ley Orgánica (...), de Protección de Datos de Carácter Personal.</p>
                <p>Cuando sea técnicamente posible y eficaz, el consentimiento del destinatario para aceptar el tratamiento de los datos podrá facilitarse mediante el uso de los parámetros adecuados del navegador o de otras aplicaciones, siempre que aquél deba proceder a su configuración durante su instalación o actualización mediante una acción expresa a tal efecto.</p>
                <p>Lo anterior no impedirá el posible almacenamiento o acceso de índole técnica al solo fin de efectuar la transmisión de una comunicación por una red de comunicaciones electrónicas o, en la medida que resulte estrictamente necesario, para la prestación de un servicio de la sociedad de la información expresamente solicitado por el destinatario.</p>
                <p>Qué son las cookies. Las cookies son ficheros creados en el navegador del usuario para registrar su actividad en el Sitio Web y permitirle una navegación más fluida y personalizada.</p>
                <h3>¿Qué tipos de cookies utiliza esta página web?</h3>

                <p>Estos son los tipos de cookies técnicas que se instalan a través de este Sitio Web:</p>
                
                <table class="table table-bordered table-striped" border="0" cellspacing="0" cellpadding="0" width="537">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Domain</th>
                            <th>Path</th>
                            <th>Expiration</th>
                            <th>Expiration in days</th>
                            <th>Value</th>
                        </tr>
                    </thead>
                    
                    <tr>
                        <td>_gid</td>
                        <td>.yokohamaiberia.es</td>
                        <td>/</td>
                        <td>Wed, 24 Jul 2019 07:58:54 GMT</td>
                        <td>1</td>
                        <td>GA1.2.1435507212.1563868727</td>
                    </tr>
                    <tr>
                        <td>_ga</td>
                        <td>.yokohamaiberia.es</td>
                        <td>/</td>
                        <td>Thu, 22 Jul 2021 07:58:54 GMT</td>
                        <td>730</td>
                        <td>GA1.2.2007855011.1563868727</td>
                    </tr>
                </table>
                <h4>Cookies de terceros: </h4>
                <p>En alguna página del Sitio Web se muestra contenido embebido o invocado a través del cual se pueden estar instalando cookies de terceros. Cuando incluimos estos contenidos en nuestro Sitio Web nos aseguramos de que la única finalidad de estas cookies sea técnica.</p>

                <p>En particular, este sitio Web utiliza Google Analytics, un servicio analítico de web prestado por Google, Inc. con domicilio en los Estados Unidos y sede en Google Building Gordon House, 4 Barrow St, Dublin, D04 E5W5, Irlanda.  Para la prestación de estos servicios, estos utilizan cookies que recopilan la información, incluida la dirección IP del usuario, que será transmitida, tratada y almacenada por Google en los términos fijados en la Web Google.com. Incluyendo la posible transmisión de dicha información a terceros por razones de exigencia legal o cuando dichos terceros procesen la información por cuenta de Google.</p>
                <p>El Usuario acepta expresamente, por la utilización de este Site, el tratamiento de la información recabada en la forma y con los fines anteriormente mencionados. Y asimismo reconoce conocer la posibilidad de rechazar el tratamiento de tales datos o información rechazando el uso de Cookies mediante la selección de la configuración apropiada a tal fin en su navegador. Si bien esta opción de bloqueo de Cookies en su navegador puede no permitirle el uso pleno de todas las funcionalidades del Website.</p>

                <p>Puede usted permitir, bloquear o eliminar las cookies instaladas en su equipo mediante la configuración de las opciones del navegador instalado en su ordenador:</p>

                <p>Cómo borrar las cookies. Para utilizar este Sitio Web no resulta necesaria la instalación de cookies. El usuario puede no aceptarlas o configurar su navegador para bloquearlas y, en su caso, eliminarlas. Aprenda a administrar sus cookies en Firefox, Chrome, IE, Opera y Safari.</p>
                <ul>
                    <li><a href="https://support.google.com/chrome/answer/95647?hl=es" target="_blank">Chrome</a></li>
                    <li><a href="https://support.microsoft.com/es-es/products/windows?os=windows-10" target="_blank">Explorer</a></li>
                    <li> <a href="https://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-sitios-web-rastrear-preferencias?redirectlocale=es&redirectslug=habilitar-y-deshabilitar-cookies-que-los-sitios-we" target="_blank">Firefox</a></li>
                    <li><a href="https://support.apple.com/kb/ph5042?locale=es_ES" target="_blank">Safari</a></li>
                </ul>
               
                <p>Si tiene dudas sobre esta política de cookies, puede contactar en <a href="mailto:contacta@yokohamaiberia.com">contacta@yokohamaiberia.com</a></p>
            </div>


            <!-- Sidebar
            ============================================= -->
            <div class="sidebar nobottommargin clearfix">
                <div class="sidebar-widgets-wrap">
                    <div class="widget widget_links clearfix">

                        <h3>El programa</h3>
                        <ul>
                            <li><a href="{{baseurl}}inicio/bases"><div>Bases legales</div></a></li>
                            <li><a href="{{baseurl}}inicio/generales"><div>Condiciones generales</div></a></li>
                            <li><a href="{{baseurl}}inicio/privacidad"><div>Política de privacidad</div></a></li>
                            <li><a href="{{baseurl}}inicio/canjeo"><div>Canjeo</div></a></li>
                            <li><a href="{{baseurl}}inicio/cookies" class="active"><div>Cookies</div></a></li> 

                        </ul>

                    </div>
                </div>
            </div>

            <!-- Sidebar END
            ============================================= -->

        </div>

    </div>

</section><!-- #content end -->
