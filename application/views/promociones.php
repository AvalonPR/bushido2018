<?php
include 'header.php';
?>
<!-- Page Title
============================================= -->
<section id="page-title" class="campanas_header">

    <div class="container clearfix">
        <h1>Campañas</h1>
        <span>Toda la información de promociones activas y pasadas</span>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">
            <h3>Campañas activas</h3>
            <div class="line-custom"></div>
            <div class="table-responsive">
                <table id="campas-activas" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="col-md-1">Imagen</th>
                            <th class="col-md-2">Nombre</th>
                            <th class="col-md-2">Desde - Hasta</th>
                            <th class="col-md-2">Usuarios adheridos</th>
                            <th class="col-md-1">Gastado</th>
                            <th class="col-md-1">Total</th>
                            <th class="col-md-1">Resto</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> <img src="{{imgurl}}/shop/thumbs/small/shoes-2.jpg"> </td>
                            <td class="click"><a href="{{siteurl}}taller/campanas/item">Decathlon</a></td>
                            <td>15/02/2018 - 01/04/2018</td>
                            <td class="click">5<button class="button" data-toggle="modal" data-target="#ModalPromos"><i class="material-icons">add_circle</i></button></td>
                            <td>1000€</td>
                            <td>5000€</td>
                            <td>4000€</td>
                        </tr>
                        <tr>
                            <td> <img src="{{imgurl}}/shop/thumbs/small/shoes-2.jpg"> </td>
                            <td class="click"><a href="{{siteurl}}taller/campanas/item">Decathlon</a></td>
                            <td>15/02/2018 - 01/04/2018</td>
                            <td class="click">5<button class="button" data-toggle="modal" data-target="#"><i class="material-icons">add_circle</i></button></td>
                            <td>1000€</td>
                            <td>5000€</td>
                            <td>4000€</td>
                        </tr>
                        <tr>
                            <td> <img src="{{imgurl}}/shop/thumbs/small/shoes-2.jpg"> </td>
                            <td class="click"><a href="{{siteurl}}taller/campanas/item">Decathlon</a></td>
                            <td>15/02/2018 - 01/04/2018</td>
                            <td class="click">5<button class="button" data-toggle="modal" data-target="#"><i class="material-icons">add_circle</i></button></td>
                            <td>1000€</td>
                            <td>5000€</td>
                            <td>4000€</td>
                        </tr>
                    </tbody>
                </table>
            </div>


            <br><br><h3>Histórico campañas</h3>
            <div class="line-custom"></div>
            <div class="table-responsive">
                <table id="historico-campanas" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="col-md-1">Imagen</th>
                            <th class="col-md-2">Nombre</th>
                            <th class="col-md-2">Desde - Hasta</th>
                            <th class="col-md-2">Usuarios adheridos</th>
                            <th class="col-md-1">Gastado</th>
                            <th class="col-md-1">Total</th>
                            <th class="col-md-1">Resto</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> <img src="{{imgurl}}/shop/thumbs/small/shoes-2.jpg"> </td>
                            <td class="click"><a href="{{siteurl}}taller/campanas/item">Decathlon</a></td>
                            <td>15/01/2018 - 01/02/2018</td>
                            <td class="click">5<button class="button" data-toggle="modal" data-target="#ModalPromos"><i class="material-icons">add_circle</i></button></td>
                            <td>1000 puntos</td>
                            <td>5000 puntos</td>
                            <td>4000 puntos</td>
                        </tr>
                        <tr>
                            <td> <img src="{{imgurl}}/shop/thumbs/small/shoes-2.jpg"> </td>
                            <td class="click"><a href="promociones_item.php">Decathlon</a></td>
                            <td>01/12/2017 - 01/01/2018</td>
                            <td class="click">5<button class="button" data-toggle="modal" data-target="#"><i class="material-icons">add_circle</i></button></td>
                            <td>1000 puntos</td>
                            <td>5000 puntos</td>
                            <td>4000 puntos</td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>

    </div>

</section><!-- #content end -->


<!--Modal promos activas-->
<div class="modal fade" id="ModalPromos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog promos_datos_mb">
        <div class="modal-body">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Usuarios a la promoción <span class="contitrade">Decathlon</span></h4>
                </div>
                <div class="modal-body">
                    <table class="table table-striped activas">
                        <thead>
                            <tr>
                                <th class="col-md-2">Nombre</th>
                                <th class="col-md-2">Importe</th>
                                <th class="col-md-2">Teléfono de contacto</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="click"><a href="{{siteurl}}taller/usuarios/info">Carla Rodríguez</a></td>
                                <td>250€</td>
                                <td>693258647 / <a href="mailto: carla_rodriguez@gmail.com">carla_rodriguez@gmail.com</a></td>
                            </tr>
                            <tr>
                                <td class="click"><a href="{{siteurl}}taller/usuarios/info">Carla Rodríguez</a></td>
                                <td>250€</td>
                                <td>693258647</td>
                            </tr>
                            <tr>
                                <td class="click"><a href="{{siteurl}}taller/usuarios/info">Carla Rodríguez</a></td>
                                <td>250€</td>
                                <td>693258647</td>
                            </tr>
                            <tr>
                                <td class="click"><a href="{{siteurl}}taller/usuarios/info">Carla Rodríguez</a></td>
                                <td>250€</td>
                                <td>693258647</td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
<?php
include 'footer.php'
?>