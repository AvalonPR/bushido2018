<?php

include 'header.php';
?>

<section id="slider" class="slider-parallax full-screen dark error404-wrap" style="background: url({{imgurl}}/bg-login.jpg) center;">
    <div class="slider-parallax-inner">

        <div class="container vertical-middle center clearfix">

            <div class="error404">404</div>

            <div class="heading-block nobottomborder">
                <h4>Parece que no podemos encontrar la página que estabas buscando.</h4>
            </div>

            

        </div>

    </div>
</section>
<?php

include 'footer.php'
?>