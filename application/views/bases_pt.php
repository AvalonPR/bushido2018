<!-- Page Title
============================================= -->
<section id="page-title" class="header programa_header">

    <div class="container clearfix">
        <h1>Bases Legais</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
        ============================================= -->
<section id="content">
    <div class="content-wrap">
    
    {% if user.bases_Bushido==0 %}
                 
    <div class="center" style="margin-bottom: 20px;">
        <a href="#myModal1" data-lightbox="inline" class="button button-large button-rounded">Aceite bases legais</a>						
    </div>
    
    <div class="modal-on-load" data-target="#myModal1"></div>

    <!-- Modal -->
    <div class="modal1 mfp-hide" id="myModal1">
            <div class="block divcenter" style="background-color: #dc0026; max-width: 520px;">
                    <div class="row nomargin clearfix">
                           
                            <div class="col-padding" data-height-xl="350" data-height-lg="400" data-height-md="456" data-height-sm="456" data-height-xs="456">

                                    <form class="form-legales clearfix" id="aceptar-condiciones" >
                                        <h3>Aceite as condições para participar de Yokohama Bushido</h3>
                                        <p style="color: #fff; text-align: right;opacity: 0.5">Os campos marcados são obrigatórios <span style="color: #444;">*</span></p>
                                        <p><input class="col-xs-1" type="checkbox" name="bases" id="bases" required><label class="col-xs-11 noleftpadding">Eu aceito as  <a href="#" style="color: grey;">condições legais</a> do programa <span class="contitrade">*</span></label></p>
                                        <p><input class="col-xs-1" type="checkbox" name="privacidad" id="privacidad" required><label class="col-xs-11 noleftpadding">Eu aceito as <a href="{{base_url()}}inicio/privacidad" style="color: grey;">condições de privacidade</a> de Yokohama iberia <span class="contitrade">*</span></label></p>
                                        <p><input class="col-xs-1" type="checkbox" name="comunicaciones" id="comunicaciones"><label class="col-xs-11 noleftpadding">Eu concordo em receber comunicações do programa</label></p>
                                        <p><button type="submit" class="button button-dark fright" onclick="aceptarLegales()">Aceitar</button></p>
                                        <div class="clear"></div>
                                    </form>
                                                                        
                            </div>
                    </div>
            </div>
    </div>
    
    <!--Fin Modal bases legales-->

  
    {%endif%}

    

        <div class="container clearfix legal">
            <!-- Post Content
            ============================================= -->
            <div class="postcontent nobottommargin col_last clearfix">
                


                <p>*Período válido de 1 de Janeiro de 2019 a 31 de Dezembro de 2019.</p>
                <h4>Denominação e objeto</h4>

                <p>YOKOHAMA IBERIA S.A. (adiante designada &ldquo;Yokohama&rdquo;) apresenta o Programa de Incentivos Bushido com o objetivo de dinamizar as vendas de pneus da marca Yokohama.</p>
                <p>Serão membros do Programa de Incentivos Yokohama Bushido os clientes com cobertura em Espanha e Portugal. Toda a comunicação deste programa será feita ao gerente das oficinas, a única pessoa que poderá participar neste programa mediante convite comercial prévio da Yokohama. Este, por sua vez, mediante sua responsabilidade, poderá dar acesso a outros membros da sua empresa para acederem ao programa e atuar em seu nome.</p>
                <p>Os participantes obterão quilómetros (pelas operações de compra de pneus no canal de troca (RE) que realizem com a Yokohama, segundo se especifica nesta base legal), que permitirá aceder a diferentes presentes.</p>
                <p>O período para efetuar as operações do programa Yokohama Bushido será de 1 de Janeiro de 2018 a 31 de Dezembro de 2018, ambos inclusive.</p>
                <h4>Inscrição</h4>

                <p>A inscrição no programa https://www.bushido.yokohamaiberia.es é automática para todos os utilizadores do B2B Yokohama. Uma comunicação será enviada para a base de dados para que possa aceder online ao programa e aceitar as bases legais. A inscrição estará sempre ativa e será contínua durante a vigência do programa.</p>
                <p>O registo de um cliente novo na base de dados da Yokohama dará acesso imediato ao programa e permitirá começar a somar pontos em todas as operações realizadas.</p>
                <h4>Pontuação e mecânica de obtenção de quilómetros</h4>

                <p>A <strong>PONTUAÇÃO</strong> do programa será medido por:</p>
                <ul>
                    <li>Você pode obter até <strong>5900</strong> km por ano.</li>
                </ul>
                <p><strong>MECÂNICA</strong> ser como se segue:</p>
                <ul>
                    <li><strong>QUILÓMETROS</strong> para ser obtido através das operações de compra de pneus da marca Yokohama e da marca Alliance (medidas em unidades de produto), realizadas com preços oficiais (incluídos nas campanhas oficiais), durante o período de vigência do programa e que não tenham sido anuladas. Estes quilómetros servem para estabelecer um ranking que dará acesso às viagens do programa e para serem trocados por presentes do catálogo online.</li>
                    <ul>
                      <li>PARA YOKOHAMA</li>
                      <li>1 Pneu de Código de Velocidade &ldquo;T&rdquo;: 1 Km</li>
                      <li>1 Pneu de Código de Velocidade &ldquo;H&rdquo;: 2 Km</li>
                      <li>1 Pneu de Código de Velocidade &ldquo;V&rdquo;: 4 Km</li>
                      <li>1 Pneu de Código de Velocidade &ldquo;W&rdquo;: 6 Km</li>
                      <li>1 Pneu de Código de Velocidade &ldquo;Y&rdquo;: 6 Km</li>
                      <li>1 Pneu de Código de Velocidade &ldquo;Z&rdquo;: 6 Km</li>
                      <li>1 Pneu 4X4: 3 Km</li>
                      <li>1 Pneu VAN: 3 Km</li>
                      <br>
                    </ul>
                    <ul>
                      <li><br>
                        · PARA ALLIANCE</li>
                      <li>· 1 Pneu de Código de Velocidade &ldquo;T&rdquo;: 0,5 Km</li>
                      <li>· 1 Pneu de Código de Velocidade &ldquo;H&rdquo;: 1 Km</li>
                      <li>· 1 Pneu de Código de Velocidade &ldquo;V&rdquo;: 2 Km</li>
                      <li>· 1 Pneu de Código de Velocidade &ldquo;W&rdquo;: 3 Km</li>
                      <li>· 1 Pneu de Código de Velocidade &ldquo;Y&rdquo;: 3 Km</li>
                      <li>· 1 Pneu de Código de Velocidade &ldquo;Z&rdquo;: 3 Km</li>
                      <li>· 1 Pneu 4X4: 1,5 Km</li>
                      <li>· 1 Pneu VAN: 1,5 Km</li>
                      <br>
                    </ul>
                  </li>
                </ul>
                <h4>Categorias dos participantes</h4>

                <p>>Os participantes do programa serão divididos por categorias segundo a classificação interna da Yokohama. Estes grupos são criados para poderem ser incentivados de forma independente e para estabelecer a justa medida do incentivo. As categorias são:</p>
                <ul>
                  <li>Categoria Geral: São todos os clientes Yokohama que estão na base de dados que dá acesso ao B2B de gestão de pedidos online da Yokohama.</li>
                  <li>Categoria YCN: São aqueles clientes que pertençam ao Clube Yokohama Network com um acesso especial a produtos e serviços específicos do clube. Estes produtos e serviços serão adicionados aos disponíveis para o resto dos participantes do programa.</li>
                </ul>
                <h4>Campanhas promocionais</h4>

                <p>Periodicamente será proposto aos Participantes promoções adicionais, com bases independentes cujo prémio será determinado pela Organização, podendo ser pontos extra ou outros prémios de obtenção direta.</p>
            </div>
            
            
            <!-- Sidebar
            ============================================= -->
            <div class="sidebar nobottommargin clearfix">
                <div class="sidebar-widgets-wrap">
                    <div class="widget widget_links clearfix">

                        <h3>El programa</h3>
                        <ul>
                            <li><a href="{{baseurl}}inicio/bases" class="active"><div>Bases legais</div></a></li>
                            <li><a href="{{baseurl}}inicio/generales"><div>Condições Gerais</div></a></li>
                            <li><a href="{{baseurl}}inicio/privacidad"><div>Política de Privacidade</div></a></li>
                            <li><a href="{{baseurl}}inicio/canjeo"><div>Condições de troca</div></a></li>
                            <li><a href="{{baseurl}}inicio/cookies"><div>Política de cookies</div></a></li> 
                        </ul>

                    </div>
                </div>
            </div>
            
            <!-- Sidebar END
            ============================================= -->
            
        </div>
    
    </div>

</section><!-- #content end -->
