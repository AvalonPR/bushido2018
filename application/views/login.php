
<section id="content" class="bg-login">

    <div class="content-wrap nopadding">

        <div class="section nopadding nomargin" style="width: 100%; height: 100%; position: absolute; left: 0; top: 0; background: url('images/bg-login.jpg') center center no-repeat; background-size: cover;"></div>

        <div class="section nobg full-screen nopadding nomargin">
            <div class="container vertical-middle divcenter clearfix">

                <div class="row center">
                    <img src="{{imgurl}}logo-dark-login.png" alt="Yokohama Bushido" style="margin-bottom: 20px;">
                </div>
                <!--<div class="center dark new-user">¿Aún no eres usuario? <a href="{{baseurl}}registro">Regístrate aquí</a></div>-->
                <div class="card" id="login_form" style="padding-bottom: 0px;">
                    {%if MSIE == true%}
                    <div class="col-md-11 advice divcenter bottommargin-sm" style="background-color: #dddddd">
                        <p class="center"><strong>Hemos detectado que está usando Microsoft Internet Explorer.</strong><br>Para el uso de esta web recomendamos el uso de: <a href="https://www.google.es/chrome/index.html" target="_blank">Google Chrome</a> o <a href="https://www.mozilla.org/es-ES/firefox/new/" target="_blank">Mozilla Firefox.</a></p>
                    </div>
                    {%endif%}

                    {%if MSIE == false%}
                    <div class="panel panel-default divcenter noradius noborder" style="max-width: 500px; background-color: rgba(255,255,255,0.93);">

                        <div class="panel-body">
                            <h3 class="notopmargin">Entra en el B2B <span style="font-weight: lighter">para acceder a Bushido</span></h3>
                                <p>El login de Bushido se realiza actualmente a través del B2B. Entra en la plataforma, loguéate con tu cuenta vuelve a esta página y recárgala.</p>
                                <a href="https://profesionales.yokohamaiberia.es/index.php" target="_blank" style="text-align: center;display: block;margin: auto;">
                                    <button class="button button-3d button-black" >Accede al B2B</button>
                                </a>
                                
                                <h4><span>*</span> El canjeo de los puntos se realizará hasta el 31 de enero</h3>
                                <form id="login-form" name="login-form" class="nobottommargin" action="#" method="post" style="display: none;">
                                
 
                                    
                                <div class="col_full">
                                    <label for="login-form-username">Introduzca su email:</label>
                                    <input type="text" id="login-form-username" name="user" value="" class="form-control not-dark" />
                                </div>

                                <div class="col_full">
                                    <label for="login-form-password">Contraseña:</label>
                                    <input type="password" id="login-form-password" name="pass" value="" class="form-control not-dark" />
                                    <input type="hidden" id="login-form-pais" name="pais" value="ES" class="form-control not-dark" />
                                </div>
                                <div class="col_full center">
                                    <button class="button button-3d button-black nomargin"  id="login-form-submit" name="login-form-submit" value="login">Entrar</button>
                                </div></form>

                                <div class="col_full toggle toggle-border nobottommargin" style="display: none;">
                                <div class="togglet" id="titinfo"><i class="toggle-closed material-icons">info</i><i class="toggle-open material-icons">info_outline</i>Pinche aquí si no recuerda su contraseña</div>
                                <div class="togglec">
                                    <form id="login-form" name="login-form" class="nobottommargin" action="#" method="post">

                                        <label for="login-form-password">Email:</label>
                                        <input type="text" id="email_recuperar" name="email_recuperar" value="" class="form-control not-dark" />
                                        <input type="hidden" id="login-form-pais" name="pais" value="ES" class="form-control not-dark" />
                                        <br>
                                        <div class="col_full center">
                                            <button class="button button-3d button-black send center" id="recuperar-form-sub" name="recuperar-form-sub" value="recuperar">Enviar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            
                        </div>
                    </div>
                    {%endif%}
                </div>
                <div class="clearfix"></div>
                <div class="row center dark"><small style="font-size: 11px;">2019 &copy; Yokohama España, todos los derechos reservados.</small></div>
            </div>

        </div>

    </div>
</section><!-- #content end -->
