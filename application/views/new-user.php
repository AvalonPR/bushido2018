<!-- Page Title
============================================= 
<section id="page-title">

    <div class="container clearfix">
        <h1>Nuevo usuario</h1>
        <span>Completa los datos para ser parte del programa</span>
    </div>

</section> #page-title end -->
<div class="row center"><img class="register-logo" src="{{imgurl}}logo-dark-login.png"></div>
     

<!-- Contact Form & Map Overlay Section
============================================= -->
<section id="map-overlay" class="bg-register" > 

    <div class="container clearfix">

        <!-- Contact Form Overlay
        ============================================= -->
        <div id="contact-form-overlay" class="clearfix">

            <div class="fancy-title title-dotted-border">
                <h3>Nuevo usuario</h3>
                <p>Completa los datos para ser parte del programa</p>
                <p> <small> (*)</small> Campos obligatorios</p>
            </div>

            <div class="contact-widget">

                <div class="contact-form-result"></div>

                <!-- Contact Form
                ============================================= -->
                <form action="{{baseurl}}registro/register" method="post" enctype="multipart/form-data" class="registerform">

                    <div class="col_half">
                        <input id="empresa" class="radio-style" name="empresaparticular" type="radio" value="empresa" checked>
                        <label for="empresa" class="radio-style-1-label">Soy empresa</label>
                    </div>
                    <div class="col_half col_last">
                        <input id="particular" class="radio-style" name="empresaparticular" type="radio">
                        <label for="particular" class="radio-style-1-label">Soy particular</label>
                    </div>

                    <!--div oculto-->
                    <div id="particular_show" style="display:none;">

                        <h4>Datos de particular</h4>
                        <div class="col_half">
                            <label for="template-contactform-name">Nombre <small>*</small> </label>
                            <input type="text" id="template-contactform-name" name="nombreParticular" value="" class="sm-form-control required" />
                        </div>

                        <div class="col_half col_last">
                            <label for="template-contactform-name">Apellidos <small>*</small></label>
                            <input type="text" id="template-contactform-name" name="apellidosParticular" value="" class="sm-form-control required" />
                        </div>

                        <div class="col_half">
                            <label for="template-contactform-name">DNI <small>*</small></label>
                            <input type="text" id="template-contactform-name" name="dniParticular" value="" class="sm-form-control required" />
                        </div>

                        <div class="col_half col_last">
                            <label for="template-contactform-name">Fecha de nacimiento <small>*</small></label>
                            <input type="text" id="template-contactform-name" name="dateParticular" value="" class="sm-form-control required" />
                        </div>
                    </div>
                    <!--fin div oculto-->

                    <!--div empresa-->
                    <div id="empresa_show">
                        <h4>Datos de empresa</h4>
                        <input type="hidden" value="empresa" name="empresa"/>
                        <div class="col_half">
                            <label for="template-contactform-name">Nombre persona de contacto <small>*</small></label>
                            <input type="text" id="template-contactform-name" name="nombreEmpresa" value="" class="sm-form-control required" />
                        </div>

                        <div class="col_half col_last">
                            <label for="template-contactform-name">DNI <small>*</small></label>
                            <input type="text" id="template-contactform-name" name="dniEmpresa" value="" class="sm-form-control required" />
                        </div>

                        <div class="col_half">
                            <label for="template-contactform-name">Empresa <small>*</small></label>
                            <input type="text" id="template-contactform-name" name="razonSocial" value="" class="sm-form-control required" />
                        </div>
                        <div class="col_half col_last">
                            <label for="template-contactform-name">CIF<small>*</small></label>
                            <input type="text" id="template-contactform-name" name="cif" value="" class="sm-form-control required" />
                        </div>
                    </div>	
                    <!-- fin div empresa-->


                    <div class="col_half">
                        <label for="template-contactform-email">Email <small>*</small></label>
                        <input type="email" id="email" name="email" value="" class="required email sm-form-control" />
                    </div>

                    <div class="col_half col_last">
                        <label for="template-contactform-email">Confirma tu email <small>*</small></label>
                        <input type="email" id="emailConfirm" name="emailConfirm" class="required email sm-form-control" />
                    </div>


                    <div class="col_half">
                        <label for="template-contactform-name">Teléfono <small>*</small></label>
                        <input type="text" id="template-contactform-name" name="phone" value="" class="sm-form-control required" />
                    </div>

                    <div class="col_half col_last">
                        <label for="template-contactform-email">Dirección </label>
                        <input type="text" id="template-contactform-email" name="direccionUser" value="" class="email sm-form-control" />
                    </div>

                    <div class="col_half">
                        <label for="template-contactform-name">C.P <small>*</small></label>
                        <input type="text" id="template-contactform-name" name="cpUser" value="" class="sm-form-control required" />
                    </div>

                    <div class="col_half col_last">
                        <label for="template-contactform-email">Provincia</label>
                        <input type="text" id="provinciaUser" name="provinciaUser" value="" class="sm-form-control" />
                    </div>

                    <div class="col_half">
                        <label for="template-contactform-email">Localidad</label>
                        <input type="text" id="localidadUser" name="localidadUser" value="" class="sm-form-control" />
                    </div>

                    <div class="col_full clearfix">


                        <div class="fancy-title title-dotted-border">
                            <h3>Datos de tu vehículo</h3>
                        </div>



                        <div class="col_half">
                            <div class="white-section">
                                <label class="select-label">Marca</label>
                                <select id="marca" name="marca" class="selectpicker fright">
                                    <option>Seleccione una marca</option>
                                    {%for marca in marcas%}
                                    <option value="{{marca.idmarca}}">{{marca.Nombre}}</option>
                                    {%endfor%}
                                </select>
                            </div>
                        </div>

                        <div class="col_half col_last">
                            <div class="white-section">
                                <label class="select-label">Modelo</label>
                                <select id="modelo" name="modelo" class="selectpicker fright">
                                </select>
                            </div>
                        </div>


                        <div class="col_half">
                            <label for="template-contactform-email">Kilómetros<small>*</small></label>
                            <input type="number" id="kilometros" name="kilometros" value="" class="sm-form-control" required/>
                        </div>
                    </div>
                    <div class="col_full clearfix">
                        <div class="fancy-title title-dotted-border">
                            <h3>Datos del taller de compra</h3>
                        </div>


                        <div class="col_half">
                            <div class="white-section">
                                <label class="select-label">Provincia</label>
                                <select name="provincia" id="provincia" class="selectpicker fright">
                                    <option>Seleccione una provincia</option>
                                    {%for pro in provincias%}
                                    <option value="{{pro.provincia}}">{{pro.provincia|capitalize}}</option>
                                    {%endfor%}
                                </select>
                            </div>
                        </div>

                        <div class="col_half col_last">
                            <div class="white-section">
                                <label class="select-label">Localidad</label>
                                <select name="localidad" id="localidad" class="selectpicker fright">
                                </select>
                            </div>
                        </div>

                        <div class="col_full">
                            <div class="white-section">
                                <label class="select-label">Taller</label>
                                <select name="taller" id="taller" class="selectpicker fright" data-width="82.5%">
                                </select>
                            </div>
                        </div>

                        <div class="col_full clearfix">
                            <h3>Facturas</h3>

                            <div class="col_half">
                                <label>Sube tu factura</label><br>
                                <input name="factura" id="input-1" type="file" class="file" accept="image/*, .pdf">
                            </div>
                            <div class="col_half  col_last">
                                <label for="importe">Importe (€)</label>
                                <input type="number" id="importe" name="importe" class="sm-form-control" placeholder="00,00 €"/>
                            </div>

                        </div>
                    </div>
                        <div class="col_full clearfix">
                            <h3>Introduce tu contraseña</h3>
                            
 

                            <div class="col_half">
                                <label>Contraseña<small>*</small></label><i class="material-icons" id="eye">visibility</i><br>
                                <input type="password" id="pass" name="pass" value="" class="contrasena sm-form-control"/>
                                <span class="progress-bar_text">Contraseña vacía</span>
                            </div>
                            <div class="col_half  col_last">
                                <label for="template-contactform-name">Confirma tu contraseña<small>*</small></label><i class="material-icons" id="eyeconfirm">visibility</i>
                        <input type="password" id="passconfirm" name="passConfirm" value="" class="sm-form-control" />
                            </div>

                        </div>


                    <div class="col_full">
                        <input id="bases" class="checkbox-style" name="bases" type="checkbox" required>
                        <label for="bases" class="checkbox-style-3-label">Acepto las <a href="#">bases legales</a> y la <a href="#">política de privacidad.</a></label>
                        <input id="comunicaciones" class="checkbox-style" name="comunicaciones" type="checkbox">
                        <label for="comunicaciones" class="checkbox-style-3-label">Acepto recibir comunicaciones comerciales.</label>
                    </div>

                    <div class="col_full">

                        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
                        <div class="g-recaptcha" data-sitekey="6LfijgUTAAAAACPt-XfRbQszAKAJY0yZDjjhMUQT"></div>

                    </div>

                    <div class="col_full">
                        <button class="button button-3d nomargin fright" type="submit" id="template-contactform-submit" name="template-contactform-submit">Registrarme</button>
                    </div>

                </form>
                    </div>
            </div>



        </div><!-- Contact Form Overlay End -->

    </div>


</section><!-- Contact Form & Map Overlay Section End -->
