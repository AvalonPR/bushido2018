<!-- Page Title
                ============================================= -->
<section id="page-title" class="header carrito_header">

    <div class="container clearfix">
        <h1>{{textos.index.titulo}}</h1>
    </div>

</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id="content" class="content-cart">
    
    <!--Process Tabs Mobile-->
    <div id="processTabsMobile" class="hidden-lg hidden-md hidden-sm">
        <ul class="process-steps bottommargin clearfix">
            <li>
                <a id="carrito"></a>
                <h5>{{textos.index.titulo}}</h5>
            </li>
            <li class="active">
                <a id="envio"></a>
                <h5>{{textos.index.envio}}</h5>
            </li>
            <li>
                <a id="pago"></a>
                <h5>{{textos.index.pago}}</h5>
            </li>
            <li>
                <a id="pedido"></a>
                <h5>{{textos.index.pedido}}</h5>
            </li>
        </ul>
    </div>
    <!--Fin Process Tabs Mobile-->

    <div class="content-wrap">

        <div class="container clearfix">
        <div id="processTabs" class="hidden-xs">
                <ul class="process-steps bottommargin clearfix">
                    <li >
                        <a class="i-circled i-bordered" id="carrito"></a>
                   <h5>{{textos.index.pasos.primero}}</h5>
                    </li>
                    <li class="active">
                        <a class="i-circled i-bordered" id="envio"></a>
                        <h5>{{textos.index.pasos.segundo}}</h5>
                    </li>
                    <li>
                        <a class="i-circled i-bordered" id="pago"></a>
                        <h5>{{textos.index.pasos.tercero}}</h5>
                    </li>
                    <li>
                        <a class="i-circled i-bordered" id="pedido"></a>
                        <h5>{{textos.index.pasos.cuarto}}</h5>
                    </li>
                </ul>
            </div>

            <div class="contact-widget">
                <div class="contact-form-result"></div>
                    <div class="row">
                <form action="{{baseurl}}carrito/saveDatos" method="post" novalidate="validate">
                        <div class="col-md-6">
                            <h3 style="margin: 0;">{{textos.process.tablaFact.titulo}}</h3>
                            <div class="line-custom"></div>
                            <p style="line-height: 35px;">&nbsp;</p>

                            <div class="col_full">
                                <label for="billing-form-companyname">{{textos.process.tablaFact.nombre}}:</label>
                                <input type="text" id="billing-form-companyname" name="billing-form-companyname" value="{{user.nombre|capitalize}}" class="sm-form-control" />
                            </div>

                            <div class="col_full">
                                <label for="billing-form-address">{{textos.process.tablaFact.dir}}:</label>
                                <input type="text" id="billing-form-address" name="billing-form-address" value="{{direccionesFact.Calle|capitalize}}, {{direccionesFact.CP}}" class="sm-form-control" />
                            </div>

                            <div class="col_full">
                                <label for="billing-form-city">{{textos.process.tablaFact.pob}}:</label>
                                <input type="text" id="billing-form-city" name="billing-form-city" value="{{direccionesFact.Ciudad|capitalize}}" class="sm-form-control" />
                            </div>

                            <div class="col_half">
                                <label for="billing-form-email">{{textos.process.tablaFact.email}}:</label>
                                <input type="email" id="billing-form-email" name="billing-form-email" value="{{user.email}}" class="sm-form-control" readonly />
                            </div>

                            <div class="col_half col_last">
                                <label for="billing-form-phone">{{textos.process.tablaFact.tfno}}:</label>
                                <input type="text" id="billing-form-phone" name="billing-form-phone" value="{{user.telefono}}" class="sm-form-control" />
                            </div>

                        </div>
                        <div class="col-md-6">
                            <h3 style="margin: 0;">{{textos.process.tablaEnvio.titulo}}</h3>
                            <div class="line-custom"></div>
                            <p><input type="checkbox" id="envioBox" style="margin-right: 15px;"><label> {{textos.process.tablaEnvio.same}}</label></p>

                            <div class="col_full">
                                <label for="shipping-form-name">{{textos.process.tablaEnvio.recev}}:</label>
                                <input type="text" id="shipping-form-name" name="shipping-form-name" value="" class="sm-form-control"  required/>
                            </div>

                            <div class="clear"></div>

                            <div class="col_full">
                                <label for="shipping-form-companyname">{{textos.process.tablaEnvio.nombre}}:</label>
                                <input type="text" id="shipping-form-companyname" name="shipping-form-companyname" class="sm-form-control" required/>
                            </div>

                            <div class="col_full">
                                <label for="shipping-form-address">{{textos.process.tablaEnvio.dir}}:</label>
                                <input type="text" id="shipping-form-address" name="shipping-form-address"  class="sm-form-control" required/>
                            </div>

                            <div class="col_full">
                                <label for="shipping-form-city">{{textos.process.tablaEnvio.pob}}:</label>
                                <input type="text" id="shipping-form-city" name="shipping-form-city" class="sm-form-control" required/>
                            </div>

                            <div class="col_full">
                                <label for="shipping-form-message">{{textos.process.tablaEnvio.obs}} <small>*</small></label>
                                <textarea class="sm-form-control" id="shipping-form-message" name="shipping-form-message" rows="6" cols="30"></textarea>
                            </div>

                        </div>
                        <script type="text/javascript">

                            function clickStopper(e){
                              e.preventDefault(); // equivalent to 'return false'
                            }

                        </script>


                        <div class="col-md-12">
                            <a href="{{siteurl}}carrito" class="fleft button button-3d button-black">{{textos.process.backButt}}</a>
                            <button onclick="this.addEventListener('click', clickStopper, false);" class="button button-3d fright">{{textos.process.checkButt}}</button>
                        </div>
                </form>
                    </div>
            </div>
        </div>
    </div>
</section><!-- #content end -->
