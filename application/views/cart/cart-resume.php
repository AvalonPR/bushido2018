<!-- Page Title
============================================= -->
<section id="page-title" class="header carrito_header">

    <div class="container clearfix">
        <h1>{{textos.index.titulo}}</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content" class="content-cart">
    
    <!--Process Tabs Mobile-->
    <div id="processTabsMobile" class="hidden-lg hidden-md hidden-sm">
        <ul class="process-steps bottommargin clearfix">
            <li>
                <a id="carrito"></a>
                <h5>{{textos.index.titulo}}</h5>
            </li>
            <li>
                <a id="envio"></a>
                <h5>{{textos.index.envio}}</h5>
            </li>
            <li class="active">
                <a id="pago"></a>
                <h5>{{textos.index.pago}}</h5>
            </li>
            <li>
                <a id="pedido"></a>
                <h5>{{textos.index.pedido}}</h5>
            </li>
        </ul>
    </div>
    <!--Fin Process Tabs Mobile-->

    <div class="contact-widget">
        <div class="contact-form-result"></div>
        <div class="content-wrap">

            <div class="container clearfix">
              <div id="processTabs" class="hidden-xs">
                <ul class="process-steps bottommargin clearfix">
                    <li >
                        <a class="i-circled i-bordered" id="carrito"></a>
                        <h5>{{textos.index.pasos.primero}}</h5>
                    </li>
                    <li>
                        <a class="i-circled i-bordered" id="envio"></a>
                        <h5>{{textos.index.pasos.segundo}}</h5>
                    </li>
                    <li class="active">
                        <a class="i-circled i-bordered" id="pago"></a>
                        <h5>{{textos.index.pasos.tercero}}</h5>
                    </li>
                    <li>
                        <a class="i-circled i-bordered" id="pedido"></a>
                        <h5>{{textos.index.pasos.cuarto}}</h5>
                    </li>
                </ul>
            </div>

                <h3>{{textos.resume.titulo}}</h3>
                <div class="line-custom"></div>
                <span class="col-md-12 col-xs-12">{{textos.resume.entradilla|raw}}</span>

                <div class="table-responsive col-md-12 col-xs-12">

                    <table class="table cart hidden-xs">
                        <thead>
                            <tr>
                                <th class="cart-product-thumbnail">&nbsp;</th>
                                <th class="cart-product-name">{{textos.resume.tabla.prod}}</th>
                                <th class="cart-product-price">{{textos.resume.tabla.price}}</th>
                                <th class="cart-product-quantity">{{textos.resume.tabla.qty}}</th>
                                <th class="cart-product-subtotal">{{textos.resume.tabla.total}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            {%for key, item in cart%}
                            <tr class="cart_item">

                                <td class="cart-product-thumbnail">
                                    <a href="#"><img width="64" height="64" src="{{item.options.img}}" alt="{{item.name}}"></a>
                                </td>

                                <td class="cart-product-name">
                                    <a>{{item.name}}</a>
                            {% if item.options.size is not null%}
                            {% for talla in item.options.size%}
                            </br><span>Talla: {{talla.0}} x {{talla.1}}uds</span>
                            {%endfor%}
                            {%endif%}
                            
                            {% if item.options.color is not null%}
                            </br><span>Color: {{item.options.color}}</span>
                            {%endif%}
                                </td>

                                <td class="cart-product-price">
                                    <span class="amount">{{item.price|number_format(0, ',', '.')}} Km</span>
                                </td>

                                <td class="cart-product-quantity">
                                    <div class="quantity clearfix">
                                        <input type="text" name="quantity" value="{{item.qty}}" class="qty qty-resume" disabled/>
                                    </div>
                                </td>

                                <td class="cart-product-subtotal">
                                    <span class="amount">{{(item.price*item.qty)|number_format(0, ',', '.')}} Km</span>
                                </td>
                            </tr>
                            {%endfor%}
                        </tbody>

                    </table>
                    
                    
                    <table class="table cartMobile hidden-lg hidden-md hidden-sm cart_item" width="300">
                    {%for key, item in cart%}
                    <tbody>
                        <tr>
                            
                            <td class="cart-product-thumbnail" align="center"><a href="{{siteurl}}productos/{{item.options.url}}"><img width="64" height="64" src="{{item.options.img}}" alt="{{item.name}}"></a></td>
                            <td class="cart-product-name" colspan="2" align="left" width="200">
                                Producto:<br>
                                <a href="{{siteurl}}productos/{{item.options.url}}">{{item.name}}</a>
                                {% if item.options.size is not null%}
                                {% for talla in item.options.size%}
                                </br><span>Talla: {{talla.0}} x {{talla.1}}uds</span>
                                {%endfor%}
                                {%endif%}

                                {% if item.options.color is not null%}
                                </br><span>Color: {{item.options.color}}</span>
                                {%endif%}
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="cart-product-remove" align="center">
                                <a class="remove" title="Eliminar artículo" onclick="removedatatable('{{key}}');"><i class="icon-trash2"></i></a>
                            </td>

                            <td align="left">Precio por unidad</td>
                            <td scope="col"><span class="amount" align="center" id="amount{{key}}">{{item.price|number_format(0, ',', '.')}} Km</span></td>
                            
                        </tr>
                        
                        <tr>
                            <td>&nbsp;</td>
                            <td align="left">Cantidad:</td>
                            
                            {% if item.options.size is not null%}
                            
                            <td class="cart-product-quantity">
                                <div class="quantityCart clearfix">
                                    <input type="text" name="quantity" data-id="{{key}}" value="{{item.qty}}" class="qty" disabled="true"/>
                                </div>
                            </td>
                            {%else%}
                            <td class="cart-product-quantity">
                                <div class="quantityCart clearfix">
                                    <!--<input type="button" value="-" class="minus">-->
                                    <input type="text" name="quantity" data-id="{{key}}" value="{{item.qty}}" class="qty" disabled='true'/>
                                    <!--<input type="button" value="+" class="plus">-->
                                </div>
                            </td>
                            {%endif%}

                            
                        </tr>
                        
                        <tr>
                            <td>&nbsp;</td>
                            <td align="left">{{textos.resume.tabla.total}}:</td>
                            <td class="cart-product-subtotal"><span class="amount" id="totalAmount{{key}}">{{(item.price*item.qty)|number_format(0, ',', '.')}} Km</span></td>
                        </tr>
                        {%endfor%}
                    </tbody>

                </table>
                    
                    

                </div>

                <div class="row clearfix">
                    <form id="resume-form" name="billing-form" class="nobottommargin" action="{{baseurl}}carrito/finalizarCompra" method="post">
                        <div class="col-md-6  col-sm-8 col-xs-12 fright">
                            <div>
                                <h4 class="crillee">{{textos.resume.tabla.totalCart}}</h4>

                                <table class="table cart">
                                    <tbody>
                                        <tr class="cart_item">
                                            <td class="cart-product-name">
                                                <strong>{{textos.resume.tabla.subtotal}}</strong>
                                            </td>

                                            <td class="" style="text-align:right;">
                                                <span class="amount">{{cart_total|number_format(0, ',', '.')}} Km</span>
                                            </td>
                                        </tr>
                                        <tr class="cart_item">
                                            <td class="cart-product-name">
                                                <strong>{{textos.resume.tabla.gastos}}</strong>
                                            </td>

                                            <td class="" style="text-align:right;">
                                                <span class="amount">{{textos.resume.tabla.gratis}}</span>
                                            </td>
                                        </tr>
                                        <tr class="cart_item">
                                            <td class="cart-product-name">
                                                <strong style="font-size: 21px;" >{{textos.resume.tabla.total}}</strong>
                                            </td>

                                            <td class="" style="text-align: right;">
                                                <span class="amount color lead"><strong>{{cart_total|number_format(0, ',', '.')}} Km</strong></span>
                                            </td>
                                        </tr>
                                    </tbody>

                                </table>

                            </div>
                        </div>
                        <div class="clear"></div>

                        <div class="col-md-12 clearfix">
                            <div class="table">
                                <h4>{{textos.resume.details}}</h4>

                                <table class="table cart">
                                    <tbody>
                                        <tr class="cart_item">
                                            <td class="cart-product-name">
                                                <strong>{{textos.resume.dirFact}}:</strong><br>
                                                <span class="amount">{{datosEnvio.billingname}}</span><br/>
                                                <span class="amount">{{datosEnvio.billingcompanyname}}</span><br/>
                                                <span class="amount">{{datosEnvio.billingaddress}}</span><br/>
                                                <span class="amount">{{datosEnvio.billingcity}}</span><br/>
                                                <span class="amount">{{datosEnvio.billingemail}}</span><br/>
                                                <span class="amount">{{datosEnvio.billingphone}}</span>
                                            </td>
                                        </tr>
                                        <tr class="cart_item">
                                            <td class="cart-product-name">
                                                <strong>{{textos.resume.dirEnvio}}:</strong><br>
                                                <span class="amount">{{datosEnvio.shippingname}}</span><br/>
                                                <span class="amount">{{datosEnvio.shippingcompanyname}}</span><br/>
                                                <span class="amount">{{datosEnvio.shippingaddress}}</span><br/>
                                                <span class="amount">{{datosEnvio.shippingcity}}</span>
                                            </td>
                                        </tr>
                                        {%if datosEnvio.shippingmessage !=''%}
                                        <tr class="cart_item">
                                            <td class="cart-product-name">
                                                <strong>{{textos.resume.obs}}:</strong><br>
                                                <span class="amount">{{datosEnvio.shippingmessage}}</span>
                                            </td>
                                        </tr>
                                        {%endif%}

                                    </tbody>

                                </table>

                            </div>
                        </div>
                        
                        <script type="text/javascript">

                            function clickStopper(e){
                              e.preventDefault(); // equivalent to 'return false'
                            }

                        </script>



                        <div class="col-md-12">
                            <a href="{{siteurl}}carrito/process" class="fleft button button-3d button-black">{{textos.resume.backButt}}</a>
                            <button type="submit" onclick="this.addEventListener('click', clickStopper, false);" class="button button-3d fright">{{textos.resume.checkButt}}</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</section><!-- #content end -->