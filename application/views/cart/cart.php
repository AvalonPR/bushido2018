<!-- Page Title
============================================= -->
<section id="page-title" class="header carrito_header">

    <div class="container clearfix">
        <h1>{{textos.index.titulo}}</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content" class="content-cart">
    
    <!--Process Tabs Mobile-->
    <div id="processTabsMobile" class="hidden-lg hidden-md hidden-sm">
        <ul class="process-steps bottommargin clearfix">
            <li class="active">
                <a id="carrito"></a>
                <h5>{{textos.index.titulo}}</h5>
            </li>
            <li>
                <a id="envio"></a>
                <h5>{{textos.index.envio}}</h5>
            </li>
            <li>
                <a id="pago"></a>
                <h5>{{textos.index.pago}}</h5>
            </li>
            <li>
                <a id="pedido"></a>
                <h5>{{textos.index.pedido}}</h5>
            </li>
        </ul>
    </div>
    <!--Fin Process Tabs Mobile-->
    
    <div class="content-wrap">
        
        
        
        <div class="container clearfix"  id="cart-data-context">
            <div id="processTabs" class="hidden-xs">
                <ul class="process-steps bottommargin clearfix">
                    <li class="active">
                        <a class="i-circled i-bordered" id="carrito"></a>
                        <h5>{{textos.index.pasos.primero}}</h5>
                    </li>
                    <li>
                        <a class="i-circled i-bordered" id="envio"></a>
                        <h5>{{textos.index.pasos.segundo}}</h5>
                    </li>
                    <li>
                        <a class="i-circled i-bordered" id="pago"></a>
                        <h5>{{textos.index.pasos.tercero}}</h5>
                    </li>
                    <li>
                        <a class="i-circled i-bordered" id="pedido"></a>
                        <h5>{{textos.index.pasos.cuarto}}</h5>
                    </li>
                </ul>
            </div>

            <h3>Carrito</h3>
            <div class="line-custom"></div>

            <div class="table-responsive nobottommargin">
                {%if cart is empty%}
                <div class="col-md-5 advice divcenter bottommargin-sm">
                    <p class="center no_items"> <i class="center material-icons">notification_important</i><br>{{textos.index.empty}}</p>
                </div>

                {%else%}
                
                <!-- INICIO TABLA desktop -->
                <table class="table cart hidden-xs">
                    <thead>
                        <tr>
                            <th class="cart-product-remove">&nbsp;</th>
                            <th class="cart-product-thumbnail">&nbsp;</th>
                            <th class="cart-product-name">{{textos.index.tabla.prod}}</th>
                            <th class="cart-product-price">{{textos.index.tabla.price}}</th>
                            <th class="cart-product-quantity">{{textos.index.tabla.qty}}</th>
                            <th class="cart-product-subtotal">{{textos.index.tabla.total}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {%for key, item in cart%}
                        <tr class="cart_item">
                            <td class="cart-product-remove">
                                <a class="remove" title="Eliminar artículo" onclick="removedatatable('{{key}}');"><i class="icon-trash2"></i></a>
                            </td>

                            <td class="cart-product-thumbnail">
                                <a href="{{siteurl}}productos/{{item.options.url}}"><img width="64" height="64" src="{{item.options.img}}" alt="{{item.name}}"></a>
                            </td>

                            <td class="cart-product-name">
                                <a href="{{siteurl}}productos/{{item.options.url}}">{{item.name}}</a>
                            {% if item.options.size is not null%}
                            {% for talla in item.options.size%}
                            </br><span>Talla: {{talla.0}} x {{talla.1}}uds</span>
                            {%endfor%}
                            {%endif%}
                            
                            {% if item.options.color is not null%}
                            </br><span>Color: {{item.options.color}}</span>
                            {%endif%}
                            </td>

                            <td class="cart-product-price">
                                <span class="amount"  id="amount{{key}}">{{item.price|number_format(0, ',', '.')}} Km</span>
                            </td>
                            {% if item.options.size is not null%}
                            
                            <td class="cart-product-quantity">
                                <div class="quantityCart clearfix">
                                    <input type="text" name="quantity" data-id="{{key}}" value="{{item.qty}}" class="qty" disabled="true"/>
                                </div>
                            </td>
                            {%else%}
                            <td class="cart-product-quantity">
                                <div class="quantityCart clearfix">
                                    <!--<input type="button" value="-" class="minus">-->
                                    <input type="text" name="quantity" data-id="{{key}}" value="{{item.qty}}" class="qty" disabled='true'/>
                                    <!--<input type="button" value="+" class="plus">-->
                                </div>
                            </td>
                            {%endif%}

                            <td class="cart-product-subtotal">
                                <span class="amount" id="totalAmount{{key}}">{{(item.price*item.qty)|number_format(0, ',', '.')}} Km</span>
                            </td>
                        </tr>
                        {%endfor%}
                    </tbody>

                </table>
                <!-- FIN TABLA desktop -->
                
                <!-- INICIO TABLA MOBILE -->
                
                <table class="table cartMobile hidden-lg hidden-md hidden-sm cart_item" width="300">
                    {%for key, item in cart%}
                    <tbody>
                        <tr>
                            
                            <td class="cart-product-thumbnail" align="center"><a href="{{siteurl}}productos/{{item.options.url}}"><img width="64" height="64" src="{{item.options.img}}" alt="{{item.name}}"></a></td>
                            <td class="cart-product-name" colspan="2" align="left" width="200">
                                Producto:<br>
                                <a href="{{siteurl}}productos/{{item.options.url}}">{{item.name}}</a>
                                {% if item.options.size is not null%}
                                {% for talla in item.options.size%}
                                </br><span>Talla: {{talla.0}} x {{talla.1}}uds</span>
                                {%endfor%}
                                {%endif%}

                                {% if item.options.color is not null%}
                                </br><span>Color: {{item.options.color}}</span>
                                {%endif%}
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="cart-product-remove" align="center">
                                <a class="remove" title="Eliminar artículo" onclick="removedatatable('{{key}}');"><i class="icon-trash2"></i></a>
                            </td>

                            <td align="left">Precio por unidad</td>
                            <td scope="col"><span class="amount" align="center" id="amount{{key}}">{{item.price|number_format(0, ',', '.')}} Km</span></td>
                            
                        </tr>
                        
                        <tr>
                            <td>&nbsp;</td>
                            <td align="left">Cantidad:</td>
                            
                            {% if item.options.size is not null%}
                            
                            <td class="cart-product-quantity">
                                <div class="quantityCart clearfix">
                                    <input type="text" name="quantity" data-id="{{key}}" value="{{item.qty}}" class="qty" disabled="true"/>
                                </div>
                            </td>
                            {%else%}
                            <td class="cart-product-quantity">
                                <div class="quantityCart clearfix">
                                    <!--<input type="button" value="-" class="minus">-->
                                    <input type="text" name="quantity" data-id="{{key}}" value="{{item.qty}}" class="qty" disabled='true'/>
                                    <!--<input type="button" value="+" class="plus">-->
                                </div>
                            </td>
                            {%endif%}

                            
                        </tr>
                        
                        <tr>
                            <td>&nbsp;</td>
                            <td align="left">Total:</td>
                            <td class="cart-product-subtotal"><span class="amount" id="totalAmount{{key}}">{{(item.price*item.qty)|number_format(0, ',', '.')}} Km</span></td>
                        </tr>
                        {%endfor%}
                    </tbody>

                </table>
                
                <!-- FIN TABLA MOBILE -->

            </div>


            <div class="row clearfix">

                <div class="col-md-6  col-sm-8 col-xs-12 clearfix fright">
                    <div class="table-responsive">
                        <h4>{{textos.index.tabla.totalCart}}</h4>

                        <table class="table cart">
                            <tbody>
                                <tr class="cart_item">
                                    <td class="cart-product-name">
                                        <strong>{{textos.index.tabla.subtotal}}</strong>
                                    </td>

                                    <td style="text-align: right;">
                                        <span class="amount">{{cart_total|number_format(0, ',', '.')}} Km</span>
                                    </td>
                                </tr>
                                <tr class="cart_item">
                                    <td class="cart-product-name">
                                        <strong>{{textos.index.tabla.gastos}}</strong>
                                    </td>

                                    <td style="text-align: right;">
                                        <span class="amount">{{textos.index.tabla.gratis}}</span>
                                    </td>
                                </tr>
                                <tr class="cart_item">
                                    <td class="cart-product-name">
                                        <strong style="font-size: 21px;">{{textos.index.tabla.total}}</strong>
                                    </td>

                                    <td style="text-align: right;">
                                        <span class="amount color lead"><strong>{{cart_total|number_format(0, ',', '.')}} Km</strong></span>
                                    </td>
                                </tr>
                            </tbody>

                        </table>

                    </div>
                </div>
            </div>
            {%endif%}
            <div class="clearfix tramit_step1">
                
                    <a href="{{baseurl}}carrito/process" class="button button-3d  fright {%if cart is empty%}button-black no_item_disabled{%endif%}">{{textos.index.checkButt}}</a>
                    <a href="{{baseurl}}productos" class="button button-3d button-black  fleft"><img src="{{imgurl}}volver-carrito.png" class="hidden-lg hidden-md hidden-sm"/><span class="hidden-xs">{{textos.index.backButt}}</span></a>
                
            </div>

        </div>
    </div>


</section><!-- #content end -->
