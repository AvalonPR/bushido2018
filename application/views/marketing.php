<!-- Page Title
============================================= -->
<section id="page-title" class="header marketing_header">

    <div class="container clearfix">
        <h1>Marketing</h1>
    </div>

</section><!-- #page-title end -->



<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">
            
            
            <!-- Portfolio Filter
            ============================================= -->
            <ul class="portfolio-filter clearfix" data-container="#portfolio">

                    <li class="activeFilter"><a href="#" data-filter="*">Mostrar todos</a></li>
                    <li><a href="#" data-filter=".pf-OTR">OTR</a></li>
                    <li><a href="#" data-filter=".pf-TBS">TBS</a></li>
                    <li><a href="#" data-filter=".pf-OE">OE</a></li>
                    <li><a href="#" data-filter=".pf-Baremo">Baremos</a></li>
            </ul><!-- #portfolio-filter end -->
            
            <div class="clear"></div>
            
            <!-- Portfolio Items
            ============================================= -->
            
            <div id="portfolio" class="portfolio grid-container clearfix">
                {%for catalogo in catalogos%}
                <article class="portfolio-item pf-{{catalogo.filtros.Filtros.0}} pf-icons">
                        <div class="portfolio-image">
                                {% if catalogo.tipo == 'pdf'%}
                                <a href="#">
                                        <img src="{{base_url()}}assets/catalogo/{{catalogo.id}}/portada/{{catalogo.portada}}" alt="{{catalogo.descripcion}}">
                                </a>
                                {%else%}
                                <a href="#"><img src="{{base_url()}}assets/catalogo/icono-excel.png" alt="{{catalogo.descripcion}}"></a>
                                {%endif%}
                                <div class="portfolio-overlay">
                                    <a href="{{base_url()}}assets/catalogo/{{catalogo.id}}/pdf/{{catalogo.pdf}}" class="left-icon" target="_blank"><i class="material-icons">remove_red_eye</i></a>
                                    <a href="{{base_url()}}assets/catalogo/{{catalogo.id}}/pdf/{{catalogo.pdf}}" class="right-icon"><i class="material-icons">file_download</i></a>
                                </div>
                        </div>
                        <div class="portfolio-desc">
                                <h3><a href="{{base_url()}}assets/catalogo/{{catalogo.id}}/pdf/{{catalogo.pdf}}"><h3>{{catalogo.titulo}}</h3></a></h3>
                                <span><a href="#"> {{catalogo.filtros.Filtros.0}}</a></span>
                        </div>
                </article>
                {%endfor%}
            </div>
            
            
            
            
              
        </div>
        <!--Container end-->

    </div>
</section>
