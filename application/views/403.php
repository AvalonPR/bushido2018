<?php

include 'header.php';
?>

<section id="slider" class="slider-parallax full-screen dark error404-wrap" style="background: url({{imgurl}}/bg-login.jpg) center;">
    <div class="slider-parallax-inner">

        <div class="container vertical-middle center clearfix">

            <div class="error404">403</div>

            <div class="heading-block nobottomborder">
                <h4>No tienes permisos para acceder a esta información.</h4>
                <span>Sigue navegando por el programa para desubrir todas la información que tienes a tu alcance</span>
            </div>

        </div>

    </div>
</section>
<?php

include 'footer.php'
?>