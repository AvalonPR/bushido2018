<!-- Page Title
============================================= -->
<section id="page-title" class="header programa_header">

    <div class="container clearfix">
        <h1>Política de cookies</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
        ============================================= -->
<section id="content">
    
    <div class="content-wrap">
    
        <div class="container clearfix legal">
            <!-- Post Content
            ============================================= -->
            <div class="postcontent nobottommargin col_last clearfix">

                <h2>POLÍTICA DE COOKIES</h2>
                
                <p>A YOKOHAMA IBERIA S.A. em conformidade e em cumprimento com o artigo 22.2 da Lei 34/2002 de 11 de julho de serviços da sociedade de informação e de comércio eletrónico (LSSI) estabelece que:</p>
                <p>Os prestadores de serviços poderão utilizar dispositivos de armazenamento e recuperação de dados nos equipamentos dos destinatários, na condição de que os mesmos terão dado o seu consentimento depois de lhes ter sido facultado a informação clara e completa sobre a sua utilização, em particular sobre a finalidade do tratamento dos dados, conforme disposto na Lei Orgânica de Proteção de Dados de Caráter Pessoal.</p>
                <p>Quando seja tecnicamente possível e eficaz, o consentimento do destinatário para aceitar o tratamento dos dados poderá ser facilitado mediante o uso dos parâmetros adequados do browser e de outras aplicações, sempre que seja efetuada a configuração durante a instalação ou atualização, mediante uma ação expressa para esse efeito.</p>
                <p>O disposto no anterior não impedirá o possível armazenamento ou acesso de índole técnica a fim de efetuar a transmissão de uma comunicação por uma rede de comunicações eletrónica, ou na medida que resulte estritamente necessária, para a prestação de um serviço da sociedade de informação expressamente solicitado pelo destinatário.</p>
                <p>O que são os cookies. Os cookies são ficheiros criados no navegador do utilizador para registar a sua atividade no Site e permitir uma navegação mais fluida e personalizada. </p>
                <h3>Quais tipos de cookies esse site usa?</h3>
                
                <p>Estes são os tipos de cookies técnicos que são instalados através deste Site:	</p>
                <table class="table table-bordered table-striped" border="0" cellspacing="0" cellpadding="0" width="537">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Domain</th>
                            <th>Path</th>
                            <th>Expiration</th>
                            <th>Expiration in days</th>
                            <th>Value</th>
                        </tr>
                    </thead>
                    
                    <tr>
                        <td>_gid</td>
                        <td>.yokohamaiberia.es</td>
                        <td>/</td>
                        <td>Wed, 24 Jul 2019 07:58:54 GMT</td>
                        <td>1</td>
                        <td>GA1.2.1435507212.1563868727</td>
                    </tr>
                    <tr>
                        <td>_ga</td>
                        <td>.yokohamaiberia.es</td>
                        <td>/</td>
                        <td>Thu, 22 Jul 2021 07:58:54 GMT</td>
                        <td>730</td>
                        <td>GA1.2.2007855011.1563868727</td>
                    </tr>
                </table>
                
                <p>Cookies de terceiros. Em algumas páginas do Site mostra-se conteúdo incorporado ou invocado, através do qual os cookies de terceiros podem ser instalados. Quando incluímos estes conteúdos no nosso Site, certificamo-nos de que o único propósito desses cookies é técnico.</p>

                <p>En particular, este sitio Web utiliza Google Analytics, un servicio analítico de web prestado por Google, Inc. con domicilio en los Estados Unidos con sede Google Building Gordon House, 4 Barrow St, Dublin, D04 E5W5, Irlanda.  Para la prestación de estos servicios, estos utilizan cookies que recopilan la información, incluida la dirección IP del usuario, que será transmitida, tratada y almacenada por Google en los términos fijados en la Web Google.com. Incluyendo la posible transmisión de dicha información a terceros por razones de exigencia legal o cuando dichos terceros procesen la información por cuenta de Google.</p>
                <p>El Usuario acepta expresamente, por la utilización de este Site, el tratamiento de la información recabada en la forma y con los fines anteriormente mencionados. Y asimismo reconoce conocer la posibilidad de rechazar el tratamiento de tales datos o información rechazando el uso de Cookies mediante la selección de la configuración apropiada a tal fin en su navegador. Si bien esta opción de bloqueo de Cookies en su navegador puede no permitirle el uso pleno de todas las funcionalidades del Website.</p>
                
                <p>Você pode permitir, bloquear ou excluir os cookies instalados no seu computador, configurando as opções do navegador instaladas no seu computador:</p>
                
                <p>Como apagar os cookies. Para utilizar esta página Web não é preciso instalar cookies. O utilizador pode não aceitá-los ou configurar o seu navegador para bloqueá-los e, se necessário, eliminá-los. Aprenda a administrar os seus  cookies em Firefox, Chrome, IE,  Opera e Safari.</p>
                <ul>
                    <li><a href="https://support.google.com/chrome/answer/95647?hl=es" target="_blank">Chrome</a></li>
                    <li><a href="https://support.microsoft.com/es-es/products/windows?os=windows-10" target="_blank">Explorer</a></li>
                    <li> <a href="https://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-sitios-web-rastrear-preferencias?redirectlocale=es&redirectslug=habilitar-y-deshabilitar-cookies-que-los-sitios-we" target="_blank">Firefox</a></li>
                    <li><a href="https://support.apple.com/kb/ph5042?locale=es_ES" target="_blank">Safari</a></li>
                </ul>
                <p>Se você tiver dúvidas sobre essa política de cookies, entre em contato <a href="mailto:contacta@yokohamaiberia.com">contacta@yokohamaiberia.com</a></p>
            </div>
            
            
            <!-- Sidebar
            ============================================= -->
            <div class="sidebar nobottommargin clearfix">
                <div class="sidebar-widgets-wrap">
                    <div class="widget widget_links clearfix">

                        <h3>El programa</h3>
                        <ul>
                            <li><a href="{{baseurl}}inicio/bases"><div>Bases legais</div></a></li>
                            <li><a href="{{baseurl}}inicio/generales"><div>Condições Gerais</div></a></li>
                            <li><a href="{{baseurl}}inicio/privacidad"><div>Política de Privacidade</div></a></li>
                            <li><a href="{{baseurl}}inicio/canjeo"><div>Condições de troca</div></a></li>
                            <li><a href="{{baseurl}}inicio/cookies" class="active"><div>Política de cookies</div></a></li>                                
                        </ul>

                    </div>
                </div>
            </div>
            
            <!-- Sidebar END
            ============================================= -->
            
        </div>
    
    </div>

</section><!-- #content end -->
