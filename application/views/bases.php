<!-- Page Title
============================================= -->
<section id="page-title" class="header programa_header">

    <div class="container clearfix">
        <h1>Bases legales</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
        ============================================= -->
<section id="content">
    <div class="content-wrap">
    
    {% if user.bases_Bushido==0 %}
                 
    <div class="center" style="margin-bottom: 20px;">
        <a href="#myModal1" data-lightbox="inline" class="button button-large button-rounded">Aceptar bases legales</a>						
    </div>
    
    <div class="modal-on-load" data-target="#myModal1"></div>

    <!-- Modal -->
    <div class="modal1 mfp-hide" id="myModal1">
            <div class="block divcenter" style="background-color: #dc0026; max-width: 520px;">
                    <div class="row nomargin clearfix">
                           
                            <div class="col-padding" data-height-xl="350" data-height-lg="400" data-height-md="456" data-height-sm="456" data-height-xs="456">

                                    <form class="form-legales clearfix" id="aceptar-condiciones" >
                                        <h3>Acepta las condiciones para poder participar en Yokohama Bushido</h3>
                                        <p style="color: #fff; text-align: right;opacity: 0.5">Los campos marcados son obligatorios <span style="color: #444;">*</span></p>
                                        <p><input class="col-xs-1" type="checkbox" name="bases" id="bases" required><label class="col-xs-11 noleftpadding">Acepto las <a href="#" style="color: grey;">condiciones legales</a> del programa <span class="contitrade">*</span></label></p>
                                        <p><input class="col-xs-1" type="checkbox" name="privacidad" id="privacidad" required><label class="col-xs-11 noleftpadding">Acepto las <a href="{{base_url()}}inicio/privacidad" style="color: grey;">condiciones de privacidad</a> de yokohama iberia <span class="contitrade">*</span></label></p>
                                        <p><input class="col-xs-1" type="checkbox" name="comunicaciones" id="comunicaciones"><label class="col-xs-11 noleftpadding">Acepto recibir comunicaciones del programa</label></p>
                                        <p><button type="submit" class="button button-dark fright" onclick="aceptarLegales()">Aceptar</button></p>
                                        <div class="clear"></div>
                                    </form>
                                                                        
                            </div>
                    </div>
            </div>
    </div>
    
    <!--Fin Modal bases legales-->

  
    {%endif%}

    

        <div class="container clearfix legal">
            <!-- Post Content
            ============================================= -->
            <div class="postcontent nobottommargin col_last clearfix">
                


                <p>*Periodo validez del 1 de enero de 2019 al 31 de Diciembre de 2019.</p>
                <h4>Denominación y objeto</h4>

                <p>YOKOHAMA IBERIA S.A. (en adelante &ldquo;Yokohama&rdquo;) presenta el Programa de Incentivos Yokohama Bushido con el objetivo de dinamizar las ventas de los neumáticos de la marca Yokohama.</p>
                <p>Serán miembros del Programa de Incentivos Yokohama Bushido los talleres participantes en el programa Bushido de Yokohama con cobertura en España y Portugal. Toda la comunicación de este programa se realizará al gerente del taller/es, única persona que podrá participar en este programa previa invitación comercial de Yokohama. Éste, a su vez, bajo su responsabilidad, podrá dar acceso a otros miembros de su empresa para acceder al programa y actuar en su nombre. Los participantes obtendrán kilómetros (por las operaciones de compra de neumáticos del canal de reemplazo (RE) que realicen con Yokohama, según se especifica en estas bases), que les permitirán acceder a diferentes regalos.</p>
                <p>El periodo de cómputo de las operaciones del programa Yokohama Bushido será del 1 de enero de 2019 al 31 de diciembre del 2019, ambos inclusive.</p>
                <h4>Inscripción</h4>

                <p>La inscripción en el programa https://www.bushido.yokohamaiberia.es será automática para todos aquellos usuarios del B2B de Yokohama. Se enviará una comunicación a toda la base de datos para que puedan acceder online al programa y aceptar las presentes bases legales. La inscripción permanecerá abierta y será continua durante la vigencia del programa.</p>
                <p>El alta de un cliente nuevo en la base de datos de Yokohama dará acceso inmediato al programa y comenzará a sumar puntuación por las operaciones que realice.</p>
                <h4>Puntuación y mecánica de obtención de kilómetros</h4>

                <p>La PUNTUACIÓN del programa se medirá en:</p>
                <ul>
                  <li>Podrás obtener un máximo de 5900 km anuales.</li>
                </ul>
                <p>La MECÁNICA será como sigue:</p>
                <ul>
                  <li>Los participantes acumularán kilómetros en función de las compras realizadas de neumáticos de la marca Yokohama y de la marca Alliance (medidas en unidades de producto). La puntuación por tamaño de llanta de cada neumático adquirido, variará de la siguiente manera:
                    <ul>
                      <li>PARA YOKOHAMA</li>
                      <li>1 Unidad de neumático de Código de Velocidad &ldquo;T&rdquo;: 1 Km</li>
                      <li>1 Unidad de neumático de Código de Velocidad &ldquo;H&rdquo;: 2 Km</li>
                      <li>1 Unidad de neumático de Código de Velocidad &ldquo;V&rdquo;: 4 Km</li>
                      <li>1 Unidad de neumático de Código de Velocidad &ldquo;W&rdquo;: 6 Km</li>
                      <li>Unidad de neumático de Código de Velocidad &ldquo;Y&rdquo;: 6 Km</li>
                      <li>1 Unidad de neumático de Código de Velocidad &ldquo;Z&rdquo;: 6 Km</li>
                      <li>1 Unidad de neumático 4X4: 3 Km</li>
                      <li>1 Unidad de neumático VAN: 3 Km</li>
                      <br>
                    </ul>
                    <ul>
                      <li><br>
                        · PARA ALLIANCE</li>
                      <li>· 1 Unidad de neumático de Código de Velocidad &ldquo;T&rdquo;: 0,5 Km</li>
                      <li>· 1 Unidad de neumático de Código de Velocidad &ldquo;H&rdquo;: 1 Km</li>
                      <li>· 1 Unidad de neumático de Código de Velocidad &ldquo;V&rdquo;: 2 Km</li>
                      <li>· 1 Unidad de neumático de Código de Velocidad &ldquo;W&rdquo;: 3 Km</li>
                      <li>· 1 Unidad de neumático de Código de Velocidad &ldquo;Y&rdquo;: 3 Km</li>
                      <li>· 1 Unidad de neumático de Código de Velocidad &ldquo;Z&rdquo;: 3 Km</li>
                      <li>· 1 Unidad de neumático 4X4: 1,5 Km</li>
                      <li>· 1 Unidad de neumático VAN: 1,5 Km</li>
                      <br>
                    </ul>
                  </li>
                </ul>
                <h4>Categorías de participantes</h4>

                <p>Los participantes en el programa serán divididos por categorías según la clasificación interna de Yokohama. Estos grupos se crean para poder ser incentivados independientemente por campañas y para establecer la justa medida de la incentivación. Las categorías serán:</p>
                <ul>
                  <li>Categoría General: Son todos aquellos clientes de Yokohama que están en la base de datos que da acceso al B2B de gestión de pedidos online de Yokohama.</li>
                  <li>Categoría YCN: Son aquellos clientes pertenecientes al Club Yokohama Network con un acceso especial a productos y servicios específicos para el Club. Estos productos y servicios serán añadidos a los disponibles para el resto de participantes del programa.</li>
                </ul>
                <h4>Campañas promocionales</h4>

                <p>Periódicamente se propondrá a los Participantes promociones adicionales, con bases independientes cuyo premio se determinará por la Organización, pudiendo ser puntos extra o bien premios de obtención directa.</p>


                  </div>
            
            
            <!-- Sidebar
            ============================================= -->
            <div class="sidebar nobottommargin clearfix">
                <div class="sidebar-widgets-wrap">
                    <div class="widget widget_links clearfix">

                        <h3>El programa</h3>
                        <ul>
                            <li><a href="{{baseurl}}inicio/bases" class="active"><div>Bases legales</div></a></li>
                            <li><a href="{{baseurl}}inicio/generales"><div>Condiciones generales</div></a></li>
                            <li><a href="{{baseurl}}inicio/privacidad"><div>Política de privacidad</div></a></li>
                            <li><a href="{{baseurl}}inicio/canjeo"><div>Canjeo</div></a></li>
                            <li><a href="{{baseurl}}inicio/cookies"><div>Política de Cookies</div></a></li> 
                        </ul>

                    </div>
                </div>
            </div>
            
            <!-- Sidebar END
            ============================================= -->
            
        </div>
    
    </div>

</section><!-- #content end -->
