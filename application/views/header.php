<!DOCTYPE html>
<html dir="ltr" lang="es-ES">
    <head>

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />

        {% for style in cssstyles %}
        {{style|raw}}
        {% endfor %}
        {%if user.puntos%}
        <script>
            window.onscroll = function () {
                if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
                    document.getElementById("contador").className = "contador_user_1";
                } else {
                    document.getElementById("contador").className = "contador_user_2";
                }
            }
        </script>
        {%endif%}

        <link rel=icon href={{imgurl}}favicon.png sizes="16x16" type="image/png">

        <!-- Document Title
        ============================================= -->
        <title>{{sitetitle}}</title>

    </head>

    <body class="stretched sticky-responsive-menu">

        <!-- Document Wrapper
        ============================================= -->
        <div id="wrapper" class="clearfix">

            {% if user.tipo %}
            <!-- Top Bar
                    ============================================= -->
            <div id="top-bar">
                <!--Contador mobile-->
                        {% if user.puntos %}
                        <div class=" visible-xs" style="" id="contador_mobile">
                            {{user.puntos|number_format(0,',','.')}} km
                        </div>
                        {%endif%}

                <div class="container clearfix contenedor-top-bar">

                    <div class="col_half nobottommargin float-left">

                        
                        

                        
                        
                        
                        <!--User name
                             =============================================-->

                    
                    <div id="" class="user-name hidden-xs">
                        {{menu.bienvenido}} <a href='{{base_url()}}inicio/perfil'>{{user.nombre}}</a>
                    </div>
                    
                    <!--End user-->
                        

                    </div>

                    <div class="col_half fright col_last nobottommargin">

                        <!-- Top Social
                        ============================================= -->
                        <div id="top-social">
                            
                            <!-- Enlace B2B-->
                            
                            <div class="b2b">
                                {% if user.nacionalidad == 'PT'%}
                                    <a href="https://profissionais.yokohamaiberia.pt/inicio/login" target="_blank"></a>
                                {% else %}
                                    <a href="https://profesionales.yokohamaiberia.es/inicio/login" target="_blank"></a>
                               {% endif %}
                            </div>
                            
                            <!--End enlace B2B-->
                            
                            <!--User
                             =============================================-->
                            <div id="" class="close-sesion">
                            {%if user.tipo=='C'%}
                                <a href='{{base_url()}}inicio/logout'>
                                    {%elseif user.tipo=='S'%}
                                <a href='{{base_url()}}comercial/logout'>
                                    {%else%}
                                <a href='{{base_url()}}admin/logout'>
                                    {%endif%}
                                    <span class="close-text">{{menu.logout}}</span>
                                    <i class="material-icons close-icon">power_settings_new</i>                     
                                </a>
                            </div>

                            <div class="hidden-lg hidden-md hidden-sm" id="user-mobile">
                                {%if user.tipo=='C'%}
                                <div id="" class="top-user">
                                    <a href='{{base_url()}}inicio/perfil'>
                                        <span class="user-text">{{menu.perfil}}</span>
                                        <i class="material-icons user-icon">person</i>
                                    </a>
                                </div>
                                     {%elseif user.tipo=='S'%}
                                <div id="" class="top-user">
                                    <a href='{{base_url()}}comercial/perfil'>
                                        <span class="user-text">{{menu.perfil}}</span>
                                        <i class="material-icons user-icon">person</i>
                                    </a>
                                </div>     {%elseif user.tipo=='A'%}
                                <div id="" class="top-user">
                                    <a href='{{base_url()}}admin/perfil'>
                                        <span class="user-text">{{menu.perfil}}</span>
                                        <i class="material-icons user-icon">person</i>
                                    </a>
                                </div>



                                {%endif%}
                                
                            </div>
                            <!--End user-->
                        
                            
                        
                        </div>
                        <!-- #top-social end -->

                    </div>
                    <div class="clear"></div>
                </div>

            </div><!-- #top-bar end -->

            <!-- Header
            ============================================= -->
            <header id="header">

                <div id="header-wrap">

                    <div class="container clearfix" id="menu-bar">

                        <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                        <!-- Logo
                        ============================================= -->
                        <div id="logo">
                            
                            <a href="{{base_url()}}inicio" class="standard-logo" data-dark-logo="{{imgurl}}logo-yokohama.png"><img src="{{imgurl}}logo-yokohama.png" alt="Yokohama"></a>
                            <a href="{{base_url()}}inicio" class="retina-logo" data-dark-logo="{{imgurl}}logo-yokohama.png"><img src="{{imgurl}}logo-yokohama.png" alt="Yokohama"></a>
                         
                            
                        </div><!-- #logo end -->
                        {% if user.tipo=='C'%}
                        <!-- Primary Navigation
                                ============================================= -->

                        <nav id="primary-menu" class="style-2">
                            <ul>
                                
                                <li><a href="{{base_url()}}inicio/perfil">{{menu.area}}</a>
                                    <ul>
                                        <li><a href="{{base_url()}}inicio/perfil">{{menu.perfil}}</a></li>
                                        <li><a href="{{base_url()}}inicio/trayectoria">{{menu.trayectoria}}</a></li>
                                        <!--<li><a href="{{base_url()}}estadisticas">{{menu.estadisticas}}</a></li>-->
                                        <li><a href="{{base_url()}}inicio/pedidos">{{menu.pedidos}}</a></li>
                                    </ul>
                                </li>
                                {%if user.viaje != 1 and user.viaje !=2%}
                                <li><a href="{{base_url()}}productos">{{menu.catalogo}}</a></li>
                                {%endif%}
                                <li><a href="{{base_url()}}inicio/viaje">{{menu.viaje}}</a></li>
                                <!--<li><a href="{{base_url()}}marketing">MARKETING</a></li>-->
                                <li><a href="{{base_url()}}inicio/bases">{{menu.programa}}</a>
                                    <ul>
                                        <li><a href="{{base_url()}}inicio/generales">{{menu.condiciones}}</a></li>
                                        <li><a href="{{base_url()}}inicio/privacidad">{{menu.privacidad}}</a></li>
                                        <li><a href="{{base_url()}}inicio/canjeo">{{menu.canjeo}}</a></li>
                                        <li><a href="{{base_url()}}inicio/cookies">{{menu.cookies}}</a></li>
                                    </ul>
                                    
                                    
                                </li>
                                <li><a href="{{base_url()}}inicio/contacto">{{menu.contact}}</a></li>
                            </ul>
                        </nav>
                        
						
                        <!-- Top Cart ============================================= -->
                        <div id="top-cart" class="top-cart">
                            <a id="top-cart-trigger"{%if cart_bushido is empty%}style="pointer-events:none; cursor:default;"{%endif%}><i class="icon-shopping-cart"></i><span class="count">{%if cart_bushido is not empty%}{{cart_bushido_items}}{%else%}0{%endif%}</span></a>
                            <div class="top-cart-content">
                                <div class="top-cart-title">
                                    <h4>{{menu.carrito}}</h4>
                                </div>
                                <div class="top-cart-items">
                                    <div class="top-cart-item clearfix">
                                        <ul id="top-cart-ul">
                                            {%for key, item in cart_bushido%}
                                            <li class="top-cart-list">
                                                <div class="top-cart-item-image">
                                                    <a href="{{base_url()}}productos/{{item.options.url}}"><img src="{{item.options.img}}" alt="{{item.name}}" /></a>
                                                </div>
                                                <div class="top-cart-item-desc">
                                                    <a href="{{base_url()}}productos/{{item.options.url}}"><span class="fleft" style="width: 85%;">{{item.name}}</span></a><span class="top-cart-item-quantity fright">x {{item.qty}}</span>
                                                    <div class="clear"></div>
                                                    <span class="top-cart-item-price fleft">{{item.price|number_format(0, ',', '.')}} Km</span>
                                                    <a class="remove fright" title="Eliminar artículo" onclick="removeCart('{{key}}');"><i class="icon-trash2"></i></a>
                                                </div>
                                                <div class="clear"></div>
                                            </li>
                                            
                                            
                                            {%endfor%}
                                        </ul>
                                    </div>
                                    
                                </div>
                                        <div class="top-cart-action clearfix">
                                            <span class="fleft top-checkout-price">{%if cart_bushido is not empty%}{{cart_bushido_total|number_format(0, ',', '.')}}{%else%}0{%endif%} Km</span>
                                            <a href="{{base_url()}}carrito" class="button button-3d button-small nomargin fright">{{menu.carrito}}</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- #top-cart end -->

                                <!-- #primary-menu end -->
                                {%elseif user.tipo=='S'%}
                                <nav id="primary-menu" class="style-2">

                                    <ul>
                                        <li><a href="{{base_url()}}comercial/estadisticas"><div>Estadísticas</div></a></li>
                                        <li><a href="#"><div>Formación</div></a>
                                            <ul>
                                                <li><a href="{{base_url()}}comercial/formacion"><div>Ver Formaciones</div></a></li>
                                            </ul>
                                        </li>
                                        <li><a href="{{base_url()}}comercial/pedidos" ><div>Pedidos</div></a>
                                        </li>
                                        <li><a href="{{base_url()}}productos" ><div>Ver catálogo</div></a>
                                        </li>
                                    </ul>



                                </nav><!-- #primary-menu end -->

                                {%elseif user.tipo=='A'%}
                                <nav id="primary-menu" class="style-2">

                                    <ul>
                                        <li><a href="{{base_url()}}admin/estadisticas"><div>Estadísticas</div></a></li>
                                        
                                        <li><a href="#"><div>Editor </div></a>
                                            <ul>
                                                <li><a href="{{base_url()}}admin/slider/">Sliders</a>
                                                </li>
                                                <li><a href="{{base_url()}}admin/home">Textos Home</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="{{base_url()}}inicio/bases">EL PROGRAMA</a>
                                            <ul>
                                                <li><a href="{{base_url()}}inicio/bases">Bases legales</a></li>
                                                <li><a href="{{base_url()}}estadisticas">Canjeo</a></li>
                                                <li><a href="{{base_url()}}inicio/legal">Privacidad</a></li>
                                                <li><a href="{{base_url()}}inicio/cookies">Cookies</a></li>
                                            </ul>


                                        </li>
                                        
                                        <li><a href="{{base_url()}}admin/pedidos" ><div>Pedidos</div></a></li>
                                        
                                        <li><a href="{{base_url()}}productos" ><div>Catálogo</div></a></li>
                                       <li><a href="#" ><div>Usuarios</div></a>
                                            <ul>
                                                <li><a href="{{base_url()}}admin/usuarios">Ver</a>
                                                </li>
                                                <li><a href="{{base_url()}}admin/usuarios/add">Añadir</a>
                                                </li>

                                            </ul>
                                        </li>
                                    </ul>
                                </nav><!-- #primary-menu end -->

                                {%endif%}                   
                            </div>



                        </div>

                        </header>
            <!-- #header end -->
                        {%endif%}
                        {% if user.puntos %}
                        <!--Contador-->
                            <div style="font:normal 15px/15px sans-serif">
                                <div id="contador"><span class="puntos_contador">{{user.puntos|number_format(0,',','.')}} {%if user.viaje==0%}km{%else%}€{%endif%}</span></div>
                            </div>
                        
                        <!--Contador end-->
                        {%endif%}
