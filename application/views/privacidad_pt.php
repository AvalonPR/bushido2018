<!-- Page Title
============================================= -->
<section id="page-title" class="header programa_header">

    <div class="container clearfix">
        <h1>Política de Privacidade</h1>
        <span>&nbsp;</span>
    </div>

</section><!-- #page-title end -->

<!-- Content
        ============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix legal">
            
            <!-- Post Content
            ============================================= -->
            <div class="postcontent nobottommargin col_last clearfix">
                
                <h2>Política de Privacidade</h2>
                <h3>1.1 Responsável (pelo tratamento)</h3>
                <p>Dados de contacto do responsável:</p>
                <p>Este Website foi criado por YOKOHAMA IBERIA, S.A. (doravante YOKOHAMA) com carácter informativo, comercial e para uso pessoal ou profissional.</p>
                <p>Yokohama Iberia, S.A., Sucursal em Portugal sita na Rua 12 Zona industrial da Varziela 4480-109 Vila do Conde.</p>
                <p>N.I.F. PT980131065</p>
                <p>T. (00351) 252 249 070 </p>
                <p>Sociedade Anónima de Direito Espanhol registada na Conservatória do Registo comercial do Porto sob o numero 7609 </p>
                <p>Dados de contacto do DPD (ou DPO): LVS2, (+34) 910 164 155, DPOYOKOHAMA @lvs2.es </p>
                
                <h3>1.2 Direitos de proteção de dados – ARCO</h3>
                <p>Como exercer os direitos: Os utilizadores podem enviar uma comunicação por escrito para a sede da YOKOHAMA ou para o endereço de e-mail indicado no cabeçalho deste aviso legal, incluindo, em ambos os casos, uma fotocópia do seu CC/BI ou de outro documento de identificação similar, para solicitar o exercício dos direitos que se seguem:</p>
                <p>Direito a solicitar acesso a dados pessoais: pode perguntar à YOKOHAMA se esta empresa estiver a tratar os seus dados</p>
                <p>Direito de solicitar retificação (caso estejam incorretos) ou exclusão.</p>
                <p>Direito a pedir a limitação do tratamento dos seus dados, caso em que apenas serão conservados pela Yokohama para o exercício ou defesa de reclamações.</p>
                <p>Direito de oposição ao tratamento: A Yokohama deixará de tratar os dados da maneira que você indicar, a não ser por motivos legítimos de força maior ou o exercício ou a defesa de possíveis reclamações que continuem a ter de serem tratadas.</p>
                <p>Direito à portabilidade de dados: no caso de desejar que os seus dados sejam processados por outra empresa, a Yokohama facilitará a portabilidade dos seus dados ao novo responsável.</p>
                <p>Modelos, formulários e mais informação sobre os direitos referidos: Página oficial da Comissão Nacional de Proteção de Dados	</p>
                <p>Possibilidade de retirar o consentimento: no caso de o consentimento ter sido concedido para um propósito específico, tem o direito de retirar o consentimento a qualquer momento, sem afetar a legalidade do tratamento com base no consentimento antes da retirada.</p>
                <p>Como reclamar perante a Autoridade de Controlo: Se um utilizador considerar que existe um problema com a forma como a Yokohama está a gerir os seus dados, pode enviar as suas reclamações para a DPD (ou DPO) de Yokohama , ou para a autoridade de proteção de dados que corresponder, sendo a Comissão Nacional de Proteção de Dados a indicada no caso de Espanha.</p>
                
                <h3>1.3 Conservação dos dados</h3>
                <p>O prazo de conservação dos dados deve ser definido tendo em atenção o período necessário para a prossecução das finalidades determinantes da recolha (alínea e) do n.º 1 do artigo 5.º da Lei n.º 67/98, de 26 de Outubro).</p>
                <p>Dados anonimizados: Os dados anonimizados serão conservados sem prazo de supressão.</p>
                <p>Dados dos Clientes: O período de conservação de dados pessoais variará de acordo com o serviço contratado pelo Cliente. Em qualquer caso, será o mínimo necessário, podendo manter-se até: </p>
                <p>Devem ser conservadas pelo tempo de duração do contrato, acrescido do prazo de caducidade ou prescrição que, nestes casos, é de 6 meses, com o limite máximo de 30 (trinta) meses.</p>
                <p>A Lei do Combate ao Branqueamento de Capitais e do Financiamento ao Terrorismo exige que quaisquer documentos, registos ou dados, incluindo a gravação de chamadas, sejam conservados por um período de 7 (sete) anos, de modo a permitir a reconstituição da operação.</p>
                <p>Prazo de arquivo e conservação de livros, registos e documentos de suporte. ante os 10 anos civis subsequentes todos os livros, registos e respectivos documentos de suporte, incluindo, quando a contabilidade é estabelecida por meios informáticos, os relativos à análise, programação e execução dos tratamentos.</p>
                <p>Newsletter A informação contemplada em “Receber Newsletter” da YOKOHAMA requer determinados dados pessoais com a finalidade de identificar o utilizador e no caso contactar com o mesmo. Esses dados serão conservados até ao momento em que o utilizador solicite o cancelamento do serviço.</p>
                <p>Informação obtida por sorteios e promoções</p>
                <p>A Yokohama usará as informações obtidas das referidas ações para entrar em contato com o Utilizador, quando apropriado. Esta informação só será armazenada durante o tempo necessário para realizar o sorteio ou promoção em questão. </p>
                <p>Dados de utilizador enviados por Yokohama , para páginas e perfis em redes sociais: Desde que o utilizador dá o seu consentimento até que o retira.</p>
                
                <h3>1.4 Procedência, finalidades e legitimidade</h3>
                <h4>1.4.1   E-mail, formulários de contacto e meios de comunicação tradicionais</h4>
                <p>Web e hosting: O site da Yokohama possui uma criptografia SSL que permite aos utilizadores enviarem, de forma segura, os seus dados pessoais por formulários de contacto de tipo padrão, hospedados nos servidores que a OVH oferece à Yokohama . </p>
                <p>Dados recolhidos pela Web: Os dados pessoais recolhidos serão processados automaticamente e incorporados nos ficheiros correspondentes dos quais a Yokohama é titular.</p>
                <p>Também pode dar-nos os seus dados por telefone, e-mail e outros meios de comunicação indicados na secção de contacto.</p>
                <p>E-mail: O nosso serviço de e-mail é prestado pela Google Inc. através de Google Apps para empresas.</p>
                <p>Mensagens instantâneas: A Yokohama não presta serviços através de mensagens instantâneas como, por exemplo, de WhatsApp, Facebook Messenger ou Line.</p>
                <p>Prestadores de serviços de pagamento: Através do Site, o cliente NÃO pode aceder, através de links, a sites de terceiros, para fazer pagamentos dos produtos fornecidos pela Yokohama.</p>
                <p>Outros serviços: Certos serviços prestados através do Site (por exemplo, a possibilidade de participar num sorteio ou concurso) podem implicar condições específicas com disposições específicas sobre proteção de dados pessoais. É essencial ler e aceitá-las antes de solicitar o serviço em questão.</p>
                <p>Finalidade e legitimidade: A finalidade do tratamento destes dados é apenas para lhe fornecer a informação ou serviços que nos solicitar. </p>
                
                <h4>1.4.2 Newsletter</h4>
                <p>Finalidad y legitimación: La finalidad del tratamiento de estos datos será únicamente la de prestarle el servicio de suscripción que el usuario solicite.</p>
                <p>Derechos: Si usted está suscrito vía e-mail del Sitio Web, puede darse de baja en cualquier momento pulsando el enlace que le aparece al final del mensaje que recibe en su correo electrónico o puede ponerse en contacto con nosotros en el correo electrónico getintouch@yokohama-online.com con las palabras claves “Darse de Baja/Unsubscribe” y sus datos serán borrados.</p>
                
                <h4>1.4.3 Sorteios, concursos e outras ações promocionais</h4>
                <p>Ao participar em ações promocionais organizadas exclusivamente pela Yokohama , os dados que fornecer serão tratados apenas pela Yokohama para o propósito que indicou na declaração da ação.</p>
                
                <h4>1.4.4 Emprego e estágios.</h4>
                <p>Receção de CV: Apenas as candidaturas de emprego ou de estágio que o candidato envie através dos meios disponíveis para isso nas áreas de emprego e estágio em sites onde a Yokohama tenha publicado uma oferta específica serão avaliadas pela Yokohama . O resto das solicitações será rejeitado.</p>
                <p>Finalidade: O candidato autoriza a Yokohama a analisar os documentos que lhe são enviados, todo o conteúdo diretamente acessível através dos motores de busca (Bing, Google, etc.), os perfis que ele mantém nas redes sociais profissionais (LinkedIn, etc.), os dados obtidos nos testes de acesso e as informações reveladas na entrevista de emprego, com o objetivo de avaliar a sua candidatura e, se necessário, oferecer-lhe um cargo. A Yokohama pode usar os dados devidamente anonimizados para fazer estatísticas sobre o tipo de pessoa que é apresentada como candidato, com a ideia de publicar relatórios sobre isso.</p>
            
                
                <h3>1.6 Confidencialidade e destruição documental</h3>
                <p>Encriptação de e-mails. Se deseja entrar em contato com a Yokohama , é altamente recomendável que assine as suas mensagens. A Yokohama não possui perfis de mensagens instantâneas, nem permite que o seu pessoal os use para o cliente ou para fins profissionais de qualquer tipo.</p>
                <p>Destruição documental. Com o objetivo de preservar e garantir a confidencialidade, a Yokohama compromete-se a destruir todas as informações confidenciais a que, devido a uma prestação de serviços, tenha tido acesso, depois de decorridos 90 dias desde o fim do serviço ao Cliente, a menos que haja uma obrigação legal para a sua conservação. Se o Cliente desejar manter o original ou uma cópia dessas informações, deve imprimi-lo ou guardá-lo pelos seus próprios meios ou recorrer à sede da Yokohama para recolhê-los antes da sua destruição.</p>
            </div>
        <!-- POST CONTENT END
            ============================================= -->
        
        
        <!-- Sidebar
            ============================================= -->
            <div class="sidebar nobottommargin clearfix">
                <div class="sidebar-widgets-wrap">
                    <div class="widget widget_links clearfix">

                        <h3>El programa</h3>
                        
                        <ul>
                            <li><a href="{{baseurl}}inicio/bases"><div>Bases legais</div></a></li>
                            <li><a href="{{baseurl}}inicio/generales"><div>Condições Gerais</div></a></li>
                            <li><a href="{{baseurl}}inicio/privacidad" class="active"><div>Política de Privacidade</div></a></li>
                            <li><a href="{{baseurl}}inicio/canjeo"><div>Condições de troca</div></a></li>
                            <li><a href="{{baseurl}}inicio/cookies"><div>Política de cookies</div></a></li>
                        </ul>

                    </div>
                </div>
            </div>
            
            <!-- Sidebar END
            ============================================= -->
        
        
        </div>
            
            
    </div>

</section><!-- #content end -->
