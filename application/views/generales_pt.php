<!-- Page Title
============================================= -->
<section id="page-title" class="header programa_header">

    <div class="container clearfix">
        <h1>Condições Gerais</h1>
        <span>&nbsp;</span>
    </div>

</section><!-- #page-title end -->

<!-- Content
        ============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix legal">
            
            <!-- Post Content
            ============================================= -->
            <div class="postcontent nobottommargin col_last clearfix">
                
                <h2></h2>
                <h3>Condições Gerais</h3>
                <p>O domínio https://www.bushido.yokohamaiberia.es, é propriedade da YOKOHAMA IBERIA SA, sendo alojado e coordenado pela Avalon Diseño, Comunicación y Marketing, S.L. A agência disponibiliza aos ditos domínios o espaço necessário nos seus servidores para alojar todos os arquivos necessários para o correto funcionamento do programa online. Avalon Diseño, Comunicación y Marketing, S.L. aplicará a mesma garantia de serviço que aplica ao seu provedor de serviços de alojamento digital.</p>
                <p>As operações de compra devem ser realizadas pelo participante que tenha efetivamente concluído a compra com a Yokohama, e não poderão ser cedidas a outros participantes. Esta prática será considerada como uma infração grave e originará o respetivo cancelamento do programa aos participantes envolvidos.</p>
                <p>Para poder aceder aos prémios, será condição indispensável que o participante seja cliente ativo no momento da troca dos presentes e na data de entrega dos prémios para o disfrute dos mesmos.</p>
                <p>Os prémios serão entregues num máximo de 45 dias úteis, desde que se confirme o pedido. O prazo de entrega dos prémios não é garantido durante o período de férias: Semana Santa, Julho, Agosto, feriados nacionais e Natal.</p>
                <p>Os prémios adquiridos não podem ser trocados por prémios diferentes aos definidos nem por dinheiro. As fotografias dos prémios não são vinculativas.</p>
                <p>É da responsabilidade do participante comprovar que o prémio se encontra em bom estado. É da responsabilidade do participante a confirmação da garantia em caso de conformidade com o prémio.</p>
                <p>A Yokohama reserva o direito de modificar ou substituir os prémios por outros de valor equivalente ou elimina-los definitivamente quando, por causas alheias à sua vontade, não seja possível entregar o solicitado. Nesse caso, enviará, para troca, artigos de igual ou valor superior, comunicando oportunamente ao solicitante. Se o participante não está de acordo com a troca proposta, é anulada a troca e são reestabelecidos os km e pontos utilizados no mesmo.</p>
                <p>Sempre que ocorra qualquer variação na gerência da empresa dos participantes, a própria deverá comunicar ao comercial respetivo e à Secretaria do programa através do e-mail: info.bushido@yokohamaiberia.com.</p>
                <p>As omissões ou reclamações devem ser apresentadas pelos participantes na Secretaria do programa através do e-mail: info.bushido@yokohamaiberia.com.  Em caso de ser verificar qualquer reclamação cuja importância justifique uma correção ou complemento ao seguinte regulamento, a Yokohama o fará e comunicará aos participantes.</p>
                <p>A Secretaria do programa dispõe da linha de telefone +34 915 631 011 de atendimento aos participantes no horário de Segunda a Quinta das 10h ás 14h e das 15h ás 18h e Sexta das 9h às 15h onde todas as dúvidas que os participantes possam ter serão esclarecidas, apesar de a dita comunicação não ter caráter vinculativo para a resolução dos problemas/reclamações apresentados.</p>
                <p>O programa está sujeito á legislação fiscal vigente.</p>
                <p>O presente regulamento poderá ser modificado pela Yokohama quando se verifique a necessidade de o adequar ao espírito (de apoio e incentivo aos participantes para alcançarem os objetivos comerciais) com que foi criado. A alteração será comunicada a todos os participantes com 10 dias de antecedência.</p>
                <p>A Yokohama reserva o direito de alterar o sistema de adjudicação de pontos (valorização de cada tipo de operação – variáveis) bem como os prémios (Catálogo de Presentes, Viagens e Serviços).</p>
                <p>O desejo de participar neste programa implica a aceitação destas bases e dos possíveis anexos que possam ser adicionados durante o período de vigência do mesmo. Os participantes do programa aceitam o critério da Yokohama para esclarecer qualquer discrepância que surja na interpretação destas bases ou assuntos comtemplados na mesma.</p>
                <p>A Yokohama avaliará e decidirá a respeito de tudo aquilo que não está comtemplado nestas bases e normas do programa.</p>

                
            </div>
        <!-- POST CONTENT END
            ============================================= -->
        
        
        <!-- Sidebar
            ============================================= -->
            <div class="sidebar nobottommargin clearfix">
                <div class="sidebar-widgets-wrap">
                    <div class="widget widget_links clearfix">

                        <h3>El programa</h3>
                        
                        <ul>
                            <li><a href="{{baseurl}}inicio/bases"><div>Bases legais</div></a></li>
                            <li><a href="{{baseurl}}inicio/generales" class="active"><div>Condições Gerais</div></a></li>
                            <li><a href="{{baseurl}}inicio/privacidad"><div>Política de Privacidade</div></a></li>
                            <li><a href="{{baseurl}}inicio/canjeo"><div>Condições de troca</div></a></li>
                            <li><a href="{{baseurl}}inicio/cookies"><div>Política de cookies</div></a></li>                                
                        </ul>

                    </div>
                </div>
            </div>
            
            <!-- Sidebar END
            ============================================= -->
        
        
        </div>
            
            
    </div>

</section><!-- #content end -->
