
<section id="content" class="bg-login">

    <div class="content-wrap nopadding">

        <div class="section nopadding nomargin" style="width: 100%; height: 100%; position: absolute; left: 0; top: 0; background: url('images/bg-login.jpg') center center no-repeat; background-size: cover;"></div>

        <div class="section nobg full-screen nopadding nomargin">
            <div class="container vertical-middle divcenter clearfix">

                <div class="row center">
                    <img src="{{imgurl}}logo-dark-login.png" alt="Yokohama Bushido" style="margin-bottom: 20px;">
                </div>
                <!--<div class="center dark new-user">¿Aún no eres usuario? <a href="{{baseurl}}registro">Regístrate aquí</a></div>-->
                <div class="card" id="login_form" style="padding-bottom: 0px;">
                    {%if MSIE == true%}
                    <div class="col-md-11 advice divcenter bottommargin-sm" style="background-color: #dddddd">
                        <p class="center"><strong>Detectamos que você está usando o Microsoft Internet Explorer.</strong><br>Para o uso deste site, recomendamos o uso de:<a href="https://www.google.es/chrome/index.html" target="_blank">Google Chrome</a> ou <a href="https://www.mozilla.org/es-ES/firefox/new/" target="_blank">Mozilla Firefox.</a></p>
                    </div>
                    {%endif%}

                    {%if MSIE == false%}
                    <div class="panel panel-default divcenter noradius noborder" style="max-width: 500px; background-color: rgba(255,255,255,0.93);">

                        <div class="panel-body">
                            <h3 class="notopmargin">Entre no B2B <span style="font-weight: lighter">para ter acesso ao Bushido</span></h3>
                                <p>O login do Bushido é feito atualmente através do B2B. Entre na plataforma, faça login com os seus dados nesta página e recarregue-a.</p>
                                <a href="https://profissionais.yokohamaiberia.pt/" target="_blank" style="text-align: center;display: block;margin: auto;">
                                    <button class="button button-3d button-black" >Accede al B2B</button>
                                </a>
                                
                                <h4><span>*</span> A troca de pontos será feita até 31 de janeiro</h4>
                                <form id="login-form" name="login-form" class="nobottommargin" action="#" method="post" style="display: none;">
                                <h3>Digite suas informações <span style="font-weight: lighter">para acessar</span></h3>
                                <div class="col_full">
                                    <label for="login-form-username">Email:</label>
                                    <input type="text" id="login-form-username" name="user" value="" class="form-control not-dark" />
                                </div>

                                <div class="col_full">
                                    <label for="login-form-password">PALAVRA-PASSE:</label>
                                    <input type="password" id="login-form-password" name="pass" value="" class="form-control not-dark" />
                                    <input type="hidden" id="login-form-pais" name="pais" value="PT" class="form-control not-dark" />
                                </div>
                                <div class="col_full center">
                                    <button class="button button-3d button-black nomargin"  id="login-form-submit" name="login-form-submit" value="login">Entrar</button>
                                </div></form>

                                <div class="col_full toggle toggle-border nobottommargin" style="display: none;">
                                <div class="togglet" id="titinfo"><i class="toggle-closed material-icons">info</i><i class="toggle-open material-icons">info_outline</i>Clique aqui se não se recorda da palavra-passe</div>
                                <div class="togglec">
                                    <form id="login-form" name="login-form" class="nobottommargin" action="#" method="post">

                                        <label for="login-form-password">Email:</label>
                                        <input type="text" id="email_recuperar" name="email_recuperar" value="" class="form-control not-dark" />
                                        <input type="hidden" id="login-form-pais" name="pais" value="PT" class="form-control not-dark" />
                                        <br>
                                        <div class="col_full center">
                                            <button class="button button-3d button-black send center" id="recuperar-form-sub" name="recuperar-form-sub" value="recuperar">Enviar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            
                        </div>
                    </div>
                    {%endif%}
                </div>
                <div class="clearfix"></div>
                <div class="row center dark"><small style="font-size: 11px;">2019 &copy; Yokohama Spain, todos os direitos reservados.</small></div>
            </div>

        </div>

    </div>
</section><!-- #content end -->
