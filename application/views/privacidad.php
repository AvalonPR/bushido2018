<!-- Page Title
============================================= -->
<section id="page-title" class="header programa_header">

    <div class="container clearfix">
        <h1>Política de privacidad</h1>
        <span>&nbsp;</span>
    </div>

</section><!-- #page-title end -->

<!-- Content
        ============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix legal">
            
            <!-- Post Content
            ============================================= -->
            <div class="postcontent nobottommargin col_last clearfix">
                
                <h2>POLÍTICA DE PRIVACIDAD</h2>
                <h3>1.1 Responsable (del tratamiento)</h3>
                <p>Esta Website ha sido creada por YOKOHAMA IBERIA, S.A. (en adelante, YOKOHAMA) con carácter informativo, comercial y para uso personal o profesional.</p>
                <p>YOKOHAMA IBERIA, S.A., sociedad de nacionalidad española, domiciliada en Paseo de las doce estrellas, 2 Planta 1ºC - C.P. 28042, Madrid.</p>
                <p>N.I.F. A-96118971</p>
                <p>T. (91) 659 15 60</p>
                <p>Inscrita en el Registro Mercantil Madrid, al Tomo 26947, Folio 86, Sección 8ª del Libro de Sociedades, Hoja número M-110282</p>
                <p>https://www.yokohamaiberia.es. Inscrito el nombre de dominio en el Registro Mercantil de Madrid, el 21 de octubre de 2.003, al Tomo 14.476, Libro 0, Folio 208, Hoja M-110282, Inscripción 26 M.</p>
                <p>Datos de contacto del DPD (o DPO): LVS2, (+34) 918 409 964,  DPOYokohama@lvs2.es</p>
                
                <h3>1.2 Derechos de Protección de Datos – ARCO</h3>
                <p>Cómo ejercitar los derechos: Los usuarios pueden dirigir una comunicación por escrito al domicilio social de Yokohama Iberia, o a la dirección de correo electrónico indicado en el encabezamiento de este aviso legal, incluyendo en ambos casos fotocopia de su DNI u otro documento identificativo similar, para solicitar el ejercicio de los derechos siguientes:</p>
                <p>Derecho a solicitar el acceso a los datos personales: usted podrá preguntar a Yokohama Iberia, si esta empresa está tratando sus datos.</p>
                <p>Derecho a solicitar su rectificación (en caso de que sean incorrectos) o supresión.</p>
                <p>Derecho a solicitar la limitación de su tratamiento, en cuyo caso únicamente serán conservados por Yokohama Iberia, para el ejercicio o la defensa de reclamaciones.</p>
                <p>Derecho a oponerse al tratamiento: Yokohama Iberia, dejará de tratar los datos en la forma que usted indique, salvo que por motivos legítimos imperiosos o el ejercicio o la defensa de posibles reclamaciones se tengan que seguir tratando.</p>
                <p>Derecho a la portabilidad de los datos: en caso de que quiera que sus datos sean tratados por otra firma, Yokohama Iberia, le facilitará la portabilidad de sus datos al nuevo responsable.</p>
                <p>Modelos, formularios y más información sobre los derechos referidos: Página oficial de la Agencia Española de Protección de Datos.</p>
                <p>Posibilidad de retirar el consentimiento: en el caso de que se haya otorgado el consentimiento para alguna finalidad específica, usted tiene derecho a retirar el consentimiento en cualquier momento, sin que ello afecte a la licitud del tratamiento basado en el consentimiento previo a su retirada.</p>
                <p>Cómo reclamar ante la Autoridad de Control: Si un usuario considera que hay un problema con la forma en que Yokohama Iberia,  está manejando sus datos, puede dirigir sus reclamaciones al DPD (o DPO) de Yokohama Iberia, o a la autoridad de protección de datos que corresponda, siendo la Agencia Española de Protección de Datos la indicada en el caso de España.</p>
                
                <h3>1.3 Conservación de los datos</h3>
                <p>Datos anonimizados: Los datos anonimizados serán conservados sin plazo de supresión.</p>
                <p>Datos de los Clientes: El periodo de conservación de los datos personales variará en función del servicio que el Cliente contrate. En cualquier caso, será el mínimo necesario, pudiendo mantenerse hasta:</p>
                <p>4 años: Ley sobre Infracciones y Sanciones en el Orden Social (obligaciones en materia de afiliación, altas, bajas, cotización, pago de salarios…); Arts. 66 y sig. Ley General Tributaria (libros de contabilidad…);</p>
                <p>5 años: Art. 1964 Código Civil (acciones personales sin plazo especial)</p>
                <p>6 años: Art. 30 Código de Comercio (libros de contabilidad, facturas…)</p>
                <p>10 años: Art. 25 Ley de Prevención del Blanqueo de Capitales y Financiación del Terrorismo.</p>
                <p>Boletín de Noticias La información contemplada en “Recibir Boletín de Noticias” de YOKOHAMA requiere determinados datos personales con el fin de identificar al Usuario y, en su caso, contactar con él. Dichos datos, se conservarán hasta el momento en que el usuario proceda a solicitar su baja en ese servicio.</p>
                <p>Información obtenida de sorteos y promociones: YOKOHAMA utilizará la información obtenida de dichas acciones para, en su caso, contactar con el Usuario. Esta información únicamente será almacenada durante el tiempo necesario para llevar a cabo el sorteo o promoción en cuestión.</p>
                <p>Datos de usuarios subidos por Yokohama Iberia, a páginas y perfiles en redes sociales: Desde que el usuario ofrece su consentimiento hasta que lo retira.</p>
                
                <h3>1.4 Procedencia, finalidades y legitimidad</h3>
                <h4>1.4.1 E-mail, formularios de contacto y medios de comunicación tradicionales</h4>
                <p>Web y hosting: El Sitio Web de Yokohama Iberia,  cuenta con un cifrado SSL que permite usuario el envío seguro de sus datos personales a través de formularios de contacto de tipo estándar, alojados en los servidores que OVH ofrece a Yokohama Iberia.</p>
                <p>Datos recabados a través de la web: Los datos personales recogidos serán objeto de tratamiento automatizado e incorporados a los correspondientes ficheros de los que Yokohama es titular.</p>
                <p>Asimismo, podrá facilitarnos sus datos a través de teléfono, correo electrónico y otros medios de comunicación indicados en la sección de contacto.</p>
                <p>Correo electrónico: Nuestro prestador de servicios de correo electrónico es Google, Inc, a través de Google Apps para empresas.</p>
                <p>Mensajería instantánea: Yokohama Iberia,  no presta servicio a través de mensajería instantánea como, por ejemplo, WhatsApp, Facebook Messenger o Line.</p>
                <p>Prestadores de servicios de pago: A través del Sitio Web, el cliente NO puede acceder, por medio de enlaces, a sitios web de terceros, para realizar pagos de los productos prestados por Yokohama Iberia.</p>
                <p>Otros servicios: Ciertos servicios prestados a través del Sitio Web (por ejemplo, la posibilidad de participar en un concurso o sorteo) pueden contener condiciones particulares con previsiones específicas en materia de protección de datos personales. Se hace indispensable su lectura y aceptación con carácter previo a la solicitud del servicio de que se trate.</p>
                <p>Finalidad y legitimación: La finalidad del tratamiento de estos datos será únicamente la de prestarle la información o servicios que nos solicite.</p>
  
                
                <h4>1.4.2 Boletín de Noticias</h4>
                <p>Finalidad y legitimación: La finalidad del tratamiento de estos datos será únicamente la de prestarle el servicio de suscripción que el usuario solicite.</p>
                <p>Derechos: Si usted está suscrito vía e-mail del Sitio Web, puede darse de baja en cualquier momento pulsando el enlace que le aparece al final del mensaje que recibe en su correo electrónico o puede ponerse en contacto con nosotros en el correo electrónico getintouch@yokohama-online.com con las palabras claves “Darse de Baja/Unsubscribe” y sus datos serán borrados.</p>
                
                <h4>1.4.3 Sorteos, concursos y otras acciones promocionales</h4>
                <p>Al participar en acciones promocionales organizadas en exclusiva por Yokohama Iberia los datos que ceda serán tratados únicamente por Yokohama Iberia para el fin que esta haya indicado en el enunciado de la acción.</p>
                
                <h4>1.4.4 Empleo y prácticas</h4>
                <p>Recepción de CV: Únicamente serán valoradas por Yokohama Iberia las solicitudes de empleo o prácticas que el candidato remita a través de los medios dispuestos para ello en las áreas de empleo y prácticas en sitios web en los que Yokohama Iberia haya publicado una oferta concreta. El resto de solicitudes serán rechazadas.</p>
                <p>Finalidad: El aspirante autoriza a Yokohama Iberia a analizar los documentos que le remita, todo el contenido que sea directamente accesible a través de los buscadores (Bing, Google, etc.), los perfiles que mantenga en redes sociales profesionales (LinkedIn, etc.), los datos obtenidos en las pruebas de acceso y la información que revele en la entrevista de trabajo, con el objetivo de valorar su candidatura y poder, en su caso, ofrecerle un puesto. Yokohama Iberia podrá usar los datos debidamente anonimizados para hacer estadísticas sobre el tipo de persona que se presenta como candidato, con la idea de publicar informes sobre ello.</p>
            
                <h3>1.5 Confidencialidad y destrucción documental</h3>
                <p>Cifrado de correos. Si desea ponerse en contacto con Yokohama, es muy recomendable que firme sus mensajes. Yokohama Iberia no cuenta con perfiles de mensajería instantánea ni permite a su personal su uso hacia el cliente ni con fines profesionales de cualquier tipo.</p>
                <p>Destrucción documental. Con el objetivo de preservar y garantizar la confidencialidad, Yokohama Iberia se compromete a destruir toda la información confidencial a la que por razón de una prestación de servicios haya tenido acceso una vez transcurridos 90 días desde la finalización del servicio al Cliente, salvo que exista una obligación legal para su conservación. Si el Cliente desea conservar el original o una copia de dicha información, deberá imprimirla o guardarla por sus propios medios o acudir a la sede de Yokohama para recogerla antes de su destrucción.</p>
            </div>
        <!-- POST CONTENT END
            ============================================= -->
        
        
        <!-- Sidebar
            ============================================= -->
            <div class="sidebar nobottommargin clearfix">
                <div class="sidebar-widgets-wrap">
                    <div class="widget widget_links clearfix">

                        <h3>El programa</h3>
                        
                        <ul>
                            <li><a href="{{baseurl}}inicio/bases"><div>Bases legales</div></a></li>
                            <li><a href="{{baseurl}}inicio/generales"><div>Condiciones generales</div></a></li>
                            <li><a href="{{baseurl}}inicio/privacidad" class="active"><div>Política de privacidad</div></a></li>
                            <li><a href="{{baseurl}}inicio/canjeo"><div>Canjeo</div></a></li>
                            <li><a href="{{baseurl}}inicio/cookies"><div>Cookies</div></a></li> 
                                
                        </ul>

                    </div>
                </div>
            </div>
            
            <!-- Sidebar END
            ============================================= -->
        
        
        </div>
            
            
    </div>

</section><!-- #content end -->
