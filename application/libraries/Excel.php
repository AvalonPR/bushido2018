<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . "/third_party/PHPExcel.php";

class Excel extends PHPExcel {

    public function __construct() {
        parent::__construct();
    }

    private function get_column($id) {
        $chars = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'Y', 'W', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AX', 'AY', 'AW', 'AZ');
        return $chars[$id];
    }

    public function get_dataMaestro($file) {
        $objPHPExcel = PHPExcel_IOFactory::load($file);
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
        foreach ($cell_collection as $cell) {
            $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            if ($column == 'F' || $column == 'G' || $column == 'H' || $column == 'I' || $column == 'J' || $column == 'K' || $column == 'L' || $column == 'M' || $column == 'N' || $column == 'O') {
                if ($row > 15) {
                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
                    if ($row == 16) {
                        $header[$row][$column] = $data_value;
                    } else {
                        $arr_data[$row][$column] = $data_value;
                    }
                }
            }
        }

        //send the data in an array format
        $final = array();
        foreach ($arr_data as $row => $d) {
            $data_temp = array();
            foreach ($d as $k => $v) {
                if (isset($header[16][$k])) {
                    $data_temp[$header[16][$k]] = $v;
                }
            }
            $final[] = $data_temp;
        }
        return $final;
    }

    public function get_data($file) {
        $objPHPExcel = PHPExcel_IOFactory::load($file);
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
        foreach ($cell_collection as $cell) {
            $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
            if ($row == 1) {
                $header[$row][$column] = $data_value;
            } else {
                $arr_data[$row][$column] = $data_value;
            }
        }
        //send the data in an array format

        $final = array();
        foreach ($arr_data as $row => $d) {
            $data_temp = array();
            foreach ($d as $k => $v) {
                if (isset($header[1][$k])) {
                    $data_temp[$header[1][$k]] = $v;
                }
            }
            $final[] = $data_temp;
        }
        return $final;
    }

    public function download_excel($filename, $s, $l, $m, $t, $km) {
        $objPHPExcel = new PHPExcel();
        /* ##### Hoja de Estadísticas ##### */
        $headings = array(
            'Fecha',
            'Código cliente',
            'Código Destino',
            'Empresa',
            'CIF',
            'Provincia',
            'Comercial',
            'Marca',
            'Total',
            'llanta',
            'Unidades',
            'Kilometros',
            'Tipo'
        );
        $sheetTitle = 'Estadísticas';
        $this->createExcel($headings, 0, $sheetTitle, $s);

        /* ##### Hoja de Llanta ##### */
        $headings = array(
            'Tamaño',
            'unidades'
        );
        $sheetTitle = 'Llantas';
        $this->createSheet();
        $this->createExcel($headings, 1, $sheetTitle, $l);
        $i = count($l) + 1;
        $objWorksheet = $this->getActiveSheet();
        // ####### ETIQUETA ####### 
        $dataseriesLabels1 = array(
            new PHPExcel_Chart_DataSeriesValues('String', '' . $sheetTitle . '!$B$1', NULL, 1), //  Unidades
        );
        //###### VALORES EJE X ##########
        $xAxisTickValues1 = array(
            new PHPExcel_Chart_DataSeriesValues('String', '' . $sheetTitle . '!$A$2:$A$' . $i . '', NULL, 10), //  A2 to A11 Tamaño de llanta
        );
        //###### VALORES  ##########
        $dataSeriesValues1 = array(
            new PHPExcel_Chart_DataSeriesValues('Number', '' . $sheetTitle . '!$B$2:$B$' . $i . '', NULL, 10), // B2 to B11 Unidades por llanta
        );

        //  Build the dataseries
        $series1 = new PHPExcel_Chart_DataSeries(
                PHPExcel_Chart_DataSeries::TYPE_PIECHART, // plotType
                //PHPExcel_Chart_DataSeries::GROUPING_STANDARD,           // plotGrouping
                NULL, range(0, count($dataSeriesValues1) - 1), // plotOrder
                $dataseriesLabels1, // plotLabel
                $xAxisTickValues1, // plotCategory
                $dataSeriesValues1                                      // plotValues
        );
        //  Set up a layout object for the Pie chart
        $layout1 = new PHPExcel_Chart_Layout();
        $layout1->setShowVal(TRUE);
        $layout1->setShowPercent(TRUE);
        //  Set the series in the plot area
        $plotarea1 = new PHPExcel_Chart_PlotArea($layout1, array($series1));
        //  Set the chart legend
        $legend1 = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);

        $title1 = new PHPExcel_Chart_Title('Tarta de llantas');


        //  Create the chart
        $chart1 = new PHPExcel_Chart(
                'chart1', // name
                $title1, // title
                $legend1, // legend
                $plotarea1, // plotArea
                true, // plotVisibleOnly
                0, // displayBlanksAs
                NULL, // xAxisLabel
                NULL            // yAxisLabel       - Pie charts don't have a Y-Axis
        );

        //  Set the position where the chart should appear in the worksheet
        $chart1->setTopLeftPosition('A14');
        $chart1->setBottomRightPosition('J37');

        //  Add the chart to the worksheet
        $objWorksheet->addChart($chart1);

        /* ##### Hoja de Tipo ##### */
        $headings = array(
            'Tipo Neumático',
            'unidades'
        );
        $sheetTitle = 'Tipo';
        $this->createSheet();
        $this->createExcel($headings, 2, $sheetTitle, $t);
        $i = count($t) + 1;
        $objWorksheet = $this->getActiveSheet();
        // ####### ETIQUETA ####### 
        $dataseriesLabels1 = array(
            new PHPExcel_Chart_DataSeriesValues('String', '' . $sheetTitle . '!$B$1', NULL, 1), //  Unidades
        );
        //###### VALORES EJE X ##########
        $xAxisTickValues1 = array(
            new PHPExcel_Chart_DataSeriesValues('String', '' . $sheetTitle . '!$A$2:$A$' . $i . '', NULL, 10), //  A2 to A11 Tamaño de llanta
        );
        //###### VALORES  ##########
        $dataSeriesValues1 = array(
            new PHPExcel_Chart_DataSeriesValues('Number', '' . $sheetTitle . '!$B$2:$B$' . $i . '', NULL, 10), // B2 to B11 Unidades por llanta
        );

        //  Build the dataseries
        $series1 = new PHPExcel_Chart_DataSeries(
                PHPExcel_Chart_DataSeries::TYPE_PIECHART, // plotType
                //PHPExcel_Chart_DataSeries::GROUPING_STANDARD,           // plotGrouping
                NULL, range(0, count($dataSeriesValues1) - 1), // plotOrder
                $dataseriesLabels1, // plotLabel
                $xAxisTickValues1, // plotCategory
                $dataSeriesValues1                                      // plotValues
        );
        //  Set up a layout object for the Pie chart
        $layout1 = new PHPExcel_Chart_Layout();
        $layout1->setShowVal(TRUE);
        $layout1->setShowPercent(TRUE);
        //  Set the series in the plot area
        $plotarea1 = new PHPExcel_Chart_PlotArea($layout1, array($series1));
        //  Set the chart legend
        $legend1 = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);

        $title1 = new PHPExcel_Chart_Title('Tarta de Tipos');


        //  Create the chart
        $chart1 = new PHPExcel_Chart(
                'chart1', // name
                $title1, // title
                $legend1, // legend
                $plotarea1, // plotArea
                true, // plotVisibleOnly
                0, // displayBlanksAs
                NULL, // xAxisLabel
                NULL            // yAxisLabel       - Pie charts don't have a Y-Axis
        );

        //  Set the position where the chart should appear in the worksheet
        $chart1->setTopLeftPosition('A7');
        $chart1->setBottomRightPosition('J30');

        //  Add the chart to the worksheet
        $objWorksheet->addChart($chart1);

        /* ##### Hoja de marca ##### */
        $headings = array(
            'Marca',
            'unidades'
        );
        $sheetTitle = 'Marca';
        $this->createSheet();
        $this->createExcel($headings, 3, $sheetTitle, $m);
        $i = count($m) + 1;
        $objWorksheet = $this->getActiveSheet();
        // ####### ETIQUETA ####### 
        $dataseriesLabels1 = array(
            new PHPExcel_Chart_DataSeriesValues('String', '' . $sheetTitle . '!$B$1', NULL, 1), //  Unidades
        );
        //###### VALORES EJE X ##########
        $xAxisTickValues1 = array(
            new PHPExcel_Chart_DataSeriesValues('String', '' . $sheetTitle . '!$A$2:$A$' . $i . '', NULL, 10), //  A2 to A11 Tamaño de llanta
        );
        //###### VALORES  ##########
        $dataSeriesValues1 = array(
            new PHPExcel_Chart_DataSeriesValues('Number', '' . $sheetTitle . '!$B$2:$B$' . $i . '', NULL, 10), // B2 to B11 Unidades por llanta
        );

        //  Build the dataseries
        $series1 = new PHPExcel_Chart_DataSeries(
                PHPExcel_Chart_DataSeries::TYPE_PIECHART, // plotType
                //PHPExcel_Chart_DataSeries::GROUPING_STANDARD,           // plotGrouping
                NULL, range(0, count($dataSeriesValues1) - 1), // plotOrder
                $dataseriesLabels1, // plotLabel
                $xAxisTickValues1, // plotCategory
                $dataSeriesValues1                                      // plotValues
        );
        //  Set up a layout object for the Pie chart
        $layout1 = new PHPExcel_Chart_Layout();
        $layout1->setShowVal(TRUE);
        $layout1->setShowPercent(TRUE);
        //  Set the series in the plot area
        $plotarea1 = new PHPExcel_Chart_PlotArea($layout1, array($series1));
        //  Set the chart legend
        $legend1 = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);

        $title1 = new PHPExcel_Chart_Title('Tarta de marcas');


        //  Create the chart
        $chart1 = new PHPExcel_Chart(
                'chart1', // name
                $title1, // title
                $legend1, // legend
                $plotarea1, // plotArea
                true, // plotVisibleOnly
                0, // displayBlanksAs
                NULL, // xAxisLabel
                NULL            // yAxisLabel       - Pie charts don't have a Y-Axis
        );

        //  Set the position where the chart should appear in the worksheet
        $chart1->setTopLeftPosition('A7');
        $chart1->setBottomRightPosition('J30');

        //  Add the chart to the worksheet
        $objWorksheet->addChart($chart1);

        /* ##### Hoja de Km ##### */
        $headings = array(
            'kmBarum',
            'kmContinentalUHP',
            'kmContinental',
            'kmGeneralT',
            'kmCamion',
            'kmIndustria',
            'kmMoto',
            'kmRecambio'
        );
        $sheetTitle = 'Kilómetros';
        $this->createSheet();
        $this->createExcel($headings, 4, $sheetTitle, $km);
        $objWorksheet = $this->getActiveSheet();

        //###### VALORES EJE X ##########
        $xAxisTickValues1 = array(
            new PHPExcel_Chart_DataSeriesValues('String', '' . $sheetTitle . '!$A$1:$H$1', NULL, 10), //  A2 to A11 Tamaño de llanta
        );
        //###### VALORES  ##########
        $dataSeriesValues1 = array(
            new PHPExcel_Chart_DataSeriesValues('Number', '' . $sheetTitle . '!$A$2:$H$2', NULL, 10), // B2 to B11 Unidades por llanta
        );

        //  Build the dataseries
        $series1 = new PHPExcel_Chart_DataSeries(
                PHPExcel_Chart_DataSeries::TYPE_PIECHART, // plotType
                //PHPExcel_Chart_DataSeries::GROUPING_STANDARD,           // plotGrouping
                NULL, range(0, count($dataSeriesValues1) - 1), // plotOrder
                $dataseriesLabels1, // plotLabel
                $xAxisTickValues1, // plotCategory
                $dataSeriesValues1                                      // plotValues
        );
        //  Set up a layout object for the Pie chart
        $layout1 = new PHPExcel_Chart_Layout();
        $layout1->setShowVal(TRUE);
        $layout1->setShowPercent(TRUE);
        //  Set the series in the plot area
        $plotarea1 = new PHPExcel_Chart_PlotArea($layout1, array($series1));
        //  Set the chart legend
        $legend1 = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);

        $title1 = new PHPExcel_Chart_Title('Tarta de Km');


        //  Create the chart
        $chart1 = new PHPExcel_Chart(
                'chart1', // name
                $title1, // title
                $legend1, // legend
                $plotarea1, // plotArea
                true, // plotVisibleOnly
                0, // displayBlanksAs
                NULL, // xAxisLabel
                NULL            // yAxisLabel       - Pie charts don't have a Y-Axis
        );

        //  Set the position where the chart should appear in the worksheet
        $chart1->setTopLeftPosition('A7');
        $chart1->setBottomRightPosition('J30');

        //  Add the chart to the worksheet
        $objWorksheet->addChart($chart1);

        $this->setActiveSheetIndex(0);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this, 'Excel2007');
        $objWriter->setIncludeCharts(TRUE);
        ob_end_clean();

        $objWriter->save('php://output');
    }

    private function createExcel($headings, $sheetIndex, $sheetTitle, $sheetArray) {
        $this->setActiveSheetIndex($sheetIndex);
        $this->getActiveSheet($sheetIndex)->setTitle($sheetTitle);
        $row_index = 1;
        foreach ($headings as $j => $heading) {
            $cell = $this->get_column($j) . $row_index;
            $this->getActiveSheet()->setCellValue($cell, $heading);
            $this->getActiveSheet()->getStyle($cell)->applyFromArray(
                    array(
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFA500')
                        )
                    )
            );
            $this->getActiveSheet()->getStyle($cell)->getFont()->setSize(10);
            $this->getActiveSheet()->getStyle($cell)->getFont()->setBold(true);
        }
        // generamos el autofiltro
        $celda_inicio = 'A' . $row_index;
        $celda_fin = $this->get_column(count($headings) - 1) . $row_index;
        $this->getActiveSheet()->setAutoFilter($celda_inicio . ':' . $celda_fin);
        $row_index++;
        foreach ($sheetArray as $a) {
            $k = 0;
            foreach ($a as $data) {
                $cell = $this->get_column($k) . $row_index;
                $this->getActiveSheet()->setCellValue($cell, $data);
                $this->getActiveSheet()->getStyle($cell)->getAlignment()->setWrapText(true);
                $k++;
            }
            $row_index++;
        }

        foreach (range('A', $this->getActiveSheet()->getHighestDataColumn()) as $col) {
            $this->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }
    }

    public function download_excel_km($filename, $s/* , $km */) {
        $objPHPExcel = new PHPExcel();
        /* ##### Hoja de Estadísticas ##### */
        $headings = array(
            'Código cliente',
            'Empresa',
            'Nombre clave',
            'CIF',
            'Provincia',
            'Comercial',
            'Kilómetros obtenidos 2016',
            'Kilómetros obtenidos 2017',
            'Kilómetros obtenidos totales',
            'Kilómetros redimidos',
            'Kilómetros disponibles',
            'Créditos obtenidos 2016',
            'Créditos obtenidos 2017',
            'Créditos obtenidos totales',
            'Créditos redimidos',
            'Créditos disponibles',
            'Kilómetros Barum 2016',
            'Kilómetros Barum 2017',
            'Kilómetros Barum totales',
            'Kilómetros Continental STD',
            'Kilómetros Continental UHP',
            'Kilómetros GT',
            'Kilómetros moto',
            'Kilómetros camión',
            'Kilómetros industria',
            '1er sem 2016 recambios',
            '2do sem 2016 recambios',
            'Recambios en 2016',
            '1er sem 2017 recambios',
            '2do sem 2017 recambios',
            'Recambios en 2017',
            'Recambios totales',
            'Totales 1er sem 2016',
            'Totales 2do sem 2016',
            'Totales en 2016',
            'Totales 1er sem 2016',
            'Totales 2do sem 2016',
            'Totales en 2017',
            'Acumulados totales'
        );
        $sheetTitle = 'Kilómetros totales';
        $this->createExcel($headings, 0, $sheetTitle, $s);

        /* ##### Hoja de Km ##### */
        /* $headings = array(
          'Kilómetros Continental STD',
          'Kilómetros Continental UHP',
          'Kilómetros Barum',
          'Kilómetros GT',
          'Kilómetros moto',
          'Kilómetros camión',
          'Kilómetros industria',
          'Kilómetros recambio',
          );
          $sheetTitle = 'Kilómetros';
          $this->createSheet();
          $this->createExcel($headings, 1, $sheetTitle, $km);
          $objWorksheet = $this->getActiveSheet();

          //###### VALORES EJE X ##########
          $xAxisTickValues1 = array(
          new PHPExcel_Chart_DataSeriesValues('String', ''.$sheetTitle.'!$A$1:$H$1', NULL, 10), //  A1 to H11 Tamaño de llanta
          );
          //###### VALORES  ##########
          $dataSeriesValues1 = array(
          new PHPExcel_Chart_DataSeriesValues('Number', ''.$sheetTitle.'!$A$2:$H$2', NULL, 10), // B2 to B11 Unidades por llanta
          );

          //  Build the dataseries
          $series1 = new PHPExcel_Chart_DataSeries(
          PHPExcel_Chart_DataSeries::TYPE_PIECHART,               // plotType
          //PHPExcel_Chart_DataSeries::GROUPING_STANDARD,           // plotGrouping
          NULL,
          range(0, count($dataSeriesValues1)-1),                  // plotOrder
          $dataseriesLabels1,                                     // plotLabel
          $xAxisTickValues1,                                      // plotCategory
          $dataSeriesValues1                                      // plotValues
          );
          //  Set up a layout object for the Pie chart
          $layout1 = new PHPExcel_Chart_Layout();
          $layout1->setShowVal(TRUE);
          $layout1->setShowPercent(TRUE);
          //  Set the series in the plot area
          $plotarea1 = new PHPExcel_Chart_PlotArea($layout1, array($series1));
          //  Set the chart legend
          $legend1 = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);

          $title1 = new PHPExcel_Chart_Title('Tarta de Km');


          //  Create the chart
          $chart1 = new PHPExcel_Chart(
          'chart1',       // name
          $title1,        // title
          $legend1,       // legend
          $plotarea1,     // plotArea
          true,           // plotVisibleOnly
          0,              // displayBlanksAs
          NULL,           // xAxisLabel
          NULL            // yAxisLabel       - Pie charts don't have a Y-Axis
          );

          //  Set the position where the chart should appear in the worksheet
          $chart1->setTopLeftPosition('A7');
          $chart1->setBottomRightPosition('J30');

          //  Add the chart to the worksheet
          $objWorksheet->addChart($chart1); */

        $this->setActiveSheetIndex(0);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this, 'Excel2007');
        $objWriter->setIncludeCharts(TRUE);
        ob_end_clean();

        $objWriter->save('php://output');
    }

}
