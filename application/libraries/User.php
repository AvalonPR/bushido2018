<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User {

    private $CI;
    private $data = array();

    public function __construct() {
        $this->CI = & get_instance();
    }

    public function is_logged_in() {
        if ($this->CI->session->userdata('logged_in') == 'true')
            return true;
        return false;
    }

    public function check_login() {
        $log = array();
        if ($this->CI->input->post()) {
            if ($this->CI->Users_m->get_by_login($this->CI->input->post('user'), $this->CI->input->post('pass'))) {
                $check = $this->CI->Users_m->get_by_login($this->CI->input->post('user'), $this->CI->input->post('pass'));
                $cipher64 = "";
            } else {
                $usuario = $this->CI->client->obtenerUsuarios($this->CI->input->post('user'), $this->CI->input->post('pais'));
                if ($usuario != array()) {
                    if ($usuario['Usuario']['TipoUsuario'] != 'C' || $usuario['Usuario']['TipoUsuario'] != 'A') {
                        $cipher64 = $this->CI->client->createPassword($this->CI->input->post('pass'));
                        $access = $this->CI->client->iniciarSesion($this->CI->input->post('user'), $cipher64['0'], $usuario['Usuario']['TipoUsuario'], $cipher64['2'], $this->CI->input->post('pais'));
                        $this->CI->session->set_userdata('cookie', $access['headers']);
                        if ($access['data']['TextError'] == array()) {
                            if ($usuario['Usuario']['TipoUsuario'] == 'C') {
                                $cliente = array();
                                $cliente = $this->CI->client->obtenerClientes($this->CI->input->post('user'), $cipher64['0'], $usuario['Usuario']['TipoUsuario'], $cipher64['1']);
                                $facturacion = $this->CI->client->obtenerFacturacion($this->CI->input->post('user'), $cipher64['0'], $usuario['Usuario']['TipoUsuario'], $cipher64['1']);
                                //print_r($cliente);exit;
                                /* COMPRUEBO SI EL USUARIO EXISTE EN NUESTRA BBDD SINO LO METO */
                                $time = time();
                                if (!$this->CI->Users_m->get_by_coduser($cliente['IC']['CodigoIC'])) {
                                    $item = new stdClass();
                                    if ($cliente != array()) {
                                        $item->nacionalidad = substr($cliente['IC']['NIF'], 0, 2);
                                        $nacionalidad = $item->nacionalidad;
                                        if ($nacionalidad == 'AD') {
                                            $nacionalidad = 'ES';
                                        }
                                        if ($nacionalidad != $this->CI->input->post('pais')) {
                                            $wdb = 0;
                                            if ($item->nacionalidad == 'PT') {
                                                $log['tipo'] = 'error';
                                                $log['msg'] = 'A conexão não foi possível, não se esqueça de estar na Web <a href="https://bushido.yokohamaiberia.pt">www.bushido.yokohamaiberia.pt</a>';
                                                return $log;
                                            } else {
                                                $log['tipo'] = 'error';
                                                $log['msg'] = 'No fue posible la conexión, asegúrese de estar en la web <a href="https://bushido.yokohamaiberia.es">www.bushido.yokohamaiberia.es</a>';
                                                return $log;
                                            }
                                        } else {
                                            $wdb = 1;
                                            $this->CI->Users_m->upsert($item);
                                        }
                                        $item->cod_user = $cliente['IC']['CodigoIC'];
                                        $item->nombreComercial = $cliente['IC']['NombreComercial'];
                                        $item->NIF = substr($cliente['IC']['NIF'], 2);
                                        $item->ranking = $cliente['IC']['Ranking'];
                                        $item->telefono = $cliente['IC']['Telefono'];
                                        $item->nombre = $cliente['IC']['NombreIC'];
                                        $item->fax = $cliente['IC']['FAX'];
                                        $direcciones = $cliente['IC']['Direccion'];
                                        foreach ($direcciones as $k => $v) {
                                            unset($v['CodigoIC']);
                                            unset($v['NombreDir']);
                                            unset($v['Estado']);
                                            $direcciones[$k] = $v;
                                        }
                                        $direcciones = json_encode($direcciones);
                                        $item->direccion = $direcciones;
                                        $item->limiteCredito = $cliente['IC']['LimiteCredito'];
                                        $item->saldo = $cliente['IC']['Saldo'];
                                        $item->creditoConsumido = $cliente['IC']['CreditoConsumido'];
                                        $item->contConex_Bushido = "1";
                                        $item->email = $cliente['IC']['email'];
                                        $item->tipo = $usuario['Usuario']['TipoUsuario'];
                                        $item->FechaHora_Bushido = $time;
                                        $item->idioma = $usuario['Usuario']['NombreCortoIdioma'];
                                        if ($facturacion != array()) {
                                            if (isset($facturacion['Facturado']['0'])) {
                                                foreach ($facturacion['Facturado'] as $k => $v) {
                                                    if ($v['EJERCICIO'] == '2018') {
                                                        $item->facturacionpv = $v['VALOR'];
                                                    }
                                                    if ($v['EJERCICIO'] == '2019') {
                                                        $item->facturacionact = $v['VALOR'];
                                                    }
                                                }
                                            } else {
                                                foreach ($facturacion as $k => $v) {
                                                    if ($v['EJERCICIO'] == '2018') {
                                                        $item->facturacionpv = $v['VALOR'];
                                                    }
                                                    if ($v['EJERCICIO'] == '2019') {
                                                        $item->facturacionact = $v['VALOR'];
                                                    }
                                                }
                                            }
                                        }
                                        $this->CI->load->library('Puntos');
                                        $puntos = $this->CI->puntos->calcularPuntos($cliente['IC']['CodigoIC'], $this->CI->input->post('user'), $usuario['Usuario']['TipoUsuario'], $cliente['IC']['Ranking'], $cipher64, $time);
                                        $this->CI->session->set_userdata('puntos', $puntos);
                                    }
                                } else {
                                    $nacionalidad = substr($cliente['IC']['NIF'], 0, 2);
                                    if ($nacionalidad == 'AD') {
                                        $nacionalidad = 'ES';
                                    }
                                    if ($nacionalidad != $this->CI->input->post('pais')) {
                                        $wdb = 0;
                                        if ($nacionalidad == 'PT') {
                                            $log['tipo'] = 'error';
                                            $log['msg'] = 'A conexão não foi possível, não se esqueça de estar na Web <a href="https://bushido.yokohamaiberia.pt">www.profissionais.yokohamaiberia.pt</a>';

                                            return $log;
                                        } else {
                                            $log['tipo'] = 'error';
                                            $log['msg'] = 'No fue posible la conexión, asegúrese de estar en la web <a href="https://bushido.yokohamaiberia.es">www.profesionales.yokohamaiberia.es</a>';
                                            return $log;
                                        }
                                    } else {
                                        $wdb = 1;
                                    }
                                    $id = $this->CI->Users_m->getID($cliente['IC']['CodigoIC']);
                                    $contConex = $this->CI->Users_m->getConex($cliente['IC']['CodigoIC']);
                                    $item = new stdClass();
                                    $item->FechaHora_Bushido = $time;
                                    $item->contConex_Bushido = $contConex->contConex_Bushido + 1;
                                    $item->email = $cliente['IC']['email'];
                                    $item->limiteCredito = $cliente['IC']['LimiteCredito'];
                                    $item->saldo = $cliente['IC']['Saldo'];
                                    $item->creditoConsumido = $cliente['IC']['CreditoConsumido'];
                                    $direcciones = $cliente['IC']['Direccion'];
                                    foreach ($direcciones as $k => $v) {
                                        unset($v['CodigoIC']);
                                        unset($v['NombreDir']);
                                        unset($v['Estado']);
                                        $direcciones[$k] = $v;
                                    }
                                    $direcciones = json_encode($direcciones);
                                    $item->direccion = $direcciones;
                                    if ($facturacion != array()) {
                                        if (isset($facturacion['Facturado']['0'])) {
                                            foreach ($facturacion['Facturado'] as $k => $v) {
                                                if ($v['EJERCICIO'] == '2018') {
                                                    $item->facturacionpv = $v['VALOR'];
                                                }
                                                if ($v['EJERCICIO'] == '2019') {
                                                    $item->facturacionact = $v['VALOR'];
                                                }
                                            }
                                        } else {
                                            foreach ($facturacion as $k => $v) {
                                                if ($v['EJERCICIO'] == '2018') {
                                                    $item->facturacionpv = $v['VALOR'];
                                                }
                                                if ($v['EJERCICIO'] == '2019') {
                                                    $item->facturacionact = $v['VALOR'];
                                                }
                                            }
                                        }
                                    }
                                    
                                    $estadoViaje =  $cliente['IC']['OpcionesViaje'];
                                    $this->CI->session->set_userdata('viaje', $estadoViaje);
                                    if ($estadoViaje == 0) {
                                        $this->CI->load->library('Puntos');
                                        $puntos = $this->CI->puntos->calcularPuntos($cliente['IC']['CodigoIC'], $this->CI->input->post('user'), $usuario['Usuario']['TipoUsuario'], $cliente['IC']['Ranking'], $cipher64, $time);
                                        $this->CI->session->set_userdata('puntos', $puntos);
                                    } else {
                                        $estadoViaje = 1;
                                        $facturacionViaje = $this->CI->client->obtenerFacturacion($this->CI->input->post('user'), $cipher64['0'], $usuario['Usuario']['TipoUsuario'], $cipher64['1'], 'Y');
                                        if (isset($facturacionViaje['Facturado']['0'])) {
                                            foreach ($facturacionViaje['Facturado'] as $k => $v) {
                                                if ($v['EJERCICIO'] == '2019') {
                                                    $item->facturacionViaje = $v['VALOR'];
                                                }
                                            }
                                        } else {
                                            foreach ($facturacionViaje as $k => $v) {
                                                if ($v['EJERCICIO'] == '2019') {
                                                    $item->facturacionViaje = $v['VALOR'];
                                                }
                                            }
                                        }
                                        $this->CI->session->set_userdata('puntos', $item->facturacionViaje);
                                    }
                                    $this->CI->Users_m->upsert($item, $id->id);
                                }
                                if ($wdb == 1) {
                                    /***** GUARDADO DE LAS VARIABLES PARA SESION *****/
                                    $check = new stdClass();
                                    $id = $this->CI->Users_m->getID($cliente['IC']['CodigoIC']);
                                    $check->id = $id->id;
                                    $check->cod_user = $id->cod_user;
                                    $variables = $this->CI->Users_m->getVariables($check->id);
                                    $check->nacionalidad = $variables->nacionalidad;
                                    $check->nombre = $variables->nombre;
                                    $check->viaje = $estadoViaje;
                                    $check->bases_Bushido = $variables->bases_Bushido;
                                    $check->bases_B2B = $variables->bases_B2B;
                                    $check->privacidad_B2B = $variables->privacidad_B2B;
                                    $check->com_B2B = $variables->com_B2B;
                                    $check->mostrarPrecio = $variables->mostrarPrecio;
                                    $check->tipo = $usuario['Usuario']['TipoUsuario'];
                                    $check->bushido = $usuario['Usuario']['Bushido'];
                                    $check->ycn = $usuario['Usuario']['YCN'];
                                    $check->idioma = $usuario['Usuario']['NombreCortoIdioma'];
                                }
                            } elseif ($usuario['Usuario']['TipoUsuario'] == 'A') {                                
                                /** SCRIPT PARA EXPORTAR A SQL LOS EMAILS DE TODOS LOS USUARIOS, ADMIN ** */
                                /*$cliente = array();
                                $cliente = $this->CI->client->obtenerClientes($this->CI->input->post('user'), $cipher64['0'], $usuario['Usuario']['TipoUsuario'], $cipher64['1']);
                                
                                foreach($cliente as $k=>$v){
                                    foreach($v as $t){
                                        if($t['email']!=array()){
                                            $item = new stdClass();
                                            $item->email = $t['email'];
                                            $item->Bushido = $t['Bushido'];
                                            $item->viaje = $t['OpcionesViaje'];
                                            $item->YCN = $t['YCN'];
                                            $item->NombreIC =$t['NombreIC'];
                                            $item->alliance = $t['ComprarAlliance'];
                                            $item->visPromo = $t['VisPromo'];
                                            $item->ranking = $t['Ranking'];
                                            $item->PAIS =substr($t['NIF'],0,2);
                                            $this->CI->Emails_m->upsert($item);
                                        }
                                    }
                                }
                                $this->CI->client->cerrarSesion($this->CI->input->post('user'),$cipher64['0'],$usuario['Usuario']['TipoUsuario'],$cipher64['1']);
                                echo "mail actualizado";
                                exit;
                                /***  FIN SCRIPT   ** */
                                if (!$this->CI->Users_m->get_by_email($this->CI->input->post('user'))) {
                                    $this->CI->session->set_userdata('logged_in', true);
                                    $this->CI->session->set_userdata('tipo', 'A');
                                    $this->CI->session->set_userdata('cipher64', $cipher64);
                                    $item = new stdClass();
                                    $item->email = $this->CI->input->post('user');
                                    $item->tipo = $usuario['Usuario']['TipoUsuario'];
                                    $this->CI->Users_m->upsert($item);
                                    $id = $this->CI->Users_m->getID($this->CI->input->post('user'));
                                    $this->CI->session->set_userdata('user_id', $id->id);
                                } else {
                                    $this->CI->session->set_userdata('logged_in', true);
                                    $this->CI->session->set_userdata('tipo', 'A');
                                    $this->CI->session->set_userdata('cipher64', $cipher64);
                                    $item = new stdClass();
                                    $item->email = $this->CI->input->post('user');
                                    $item->tipo = $usuario['Usuario']['TipoUsuario'];
                                    $id = $this->CI->Users_m->getAdminID($this->CI->input->post('user'));
                                    $this->CI->session->set_userdata('user_id', $id->id);
                                    $this->CI->Users_m->upsert($item, $id->id);
                                }
                            } elseif ($usuario['Usuario']['TipoUsuario'] == 'S') {
                                if (!$this->CI->Users_m->get_by_email($this->CI->input->post('user'))) {
                                    $this->CI->session->set_userdata('logged_in', true);
                                    $this->CI->session->set_userdata('tipo', 'S');
                                    $this->CI->session->set_userdata('cipher64', $cipher64);
                                    $item = new stdClass();
                                    $item->email = $this->CI->input->post('user');
                                    $item->tipo = $usuario['Usuario']['TipoUsuario'];
                                    $item->nombreComercial = $usuario['Usuario']['NombreComercial'];
                                    $id = $this->CI->Users_m->upsert($item);
                                    $this->CI->session->set_userdata('user_id', $id->id);
                                } else {
                                    $this->CI->session->set_userdata('logged_in', true);
                                    $this->CI->session->set_userdata('tipo', 'S');
                                    $this->CI->session->set_userdata('cipher64', $cipher64);
                                    $item = new stdClass();
                                    $item->email = $this->CI->input->post('user');
                                    $item->tipo = $usuario['Usuario']['TipoUsuario'];
                                    $item->nombreComercial = $usuario['Usuario']['NombreComercial'];
                                    $id = $this->CI->Users_m->getAdminID($this->CI->input->post('user'));
                                    $this->CI->session->set_userdata('user_id', $id->id);
                                    $this->CI->Users_m->upsert($item, $id->id);
                                }
                            }
                        } else {
                            $log['tipo'] = 'error';
                            $log['msg'] = $access['data']['TextError'];
                            return $log;
                        }
                    } else {
                        $log['tipo'] = 'error';
                        $log['msg'] = 'Su tipo de usuario no tiene permitido el acceso';
                        return $log;
                    }
                } else {
                    $log['tipo'] = 'error';
                    $log['msg'] = 'No se encuentra el mail en la BBDD';
                    return $log;
                }
            }
        }
        if (isset($check)) {
            $atributos = array(
                'ComprarAlliance' => $cliente['IC']['ComprarAlliance'],
                'Bushido' => $cliente['IC']['Bushido'],
                'YCN' => $cliente['IC']['YCN'],
                'viaje' => $check->viaje,
                'VisPromo' => $cliente['IC']['VisPromo'],
            );
            $this->CI->session->set_userdata('mostrarPrecio', $check->mostrarPrecio);
            $this->CI->session->set_userdata('logged_in', true);
            $this->CI->session->set_userdata('user_id', $check->id);
            $this->CI->session->set_userdata('cod_user', $check->cod_user);
            $this->CI->session->set_userdata('tipo', $check->tipo);
            $this->CI->session->set_userdata('nombre', $cliente['IC']['NombreIC']);
            $this->CI->session->set_userdata('bases_Bushido', $check->bases_Bushido);
            $this->CI->session->set_userdata('bases_B2B', $check->bases_B2B);
            $this->CI->session->set_userdata('privacidad_B2B', $check->privacidad_B2B);
            $this->CI->session->set_userdata('com_B2B', $check->com_B2B);
            switch ($usuario['Usuario']['NombreCortoIdioma']) {
                case 'PT': $this->CI->session->set_userdata('site_lang', 'portuguese');
                    break;
                default : $this->CI->session->set_userdata('site_lang', 'spanish');
                    break;
            }
            $this->CI->session->set_userdata('neumStar', $cliente['IC']['NeumStar']);
            $this->CI->session->set_userdata('antesDe12', $cliente['IC']['AntesDe12']);
            $this->CI->session->set_userdata('condicionPago', $cliente['IC']['NomCondicionPago']);
            $this->CI->session->set_userdata('viaPago', $cliente['IC']['ViaPagoDfl']);
            $this->CI->session->set_userdata('distribuidor', $cliente['IC']['Distribuidor']);
            $this->CI->session->set_userdata('nacionalidad', $check->nacionalidad);
            $this->CI->session->set_userdata('iva', $cliente['IC']['PrcntIVA']);
            $this->CI->session->set_userdata('ycn', $check->ycn);
            $this->CI->session->set_userdata('style', $this->CI->session->userdata('ycn'));
            $this->CI->session->set_userdata('cipher64', $cipher64);
            $this->CI->session->set_userdata('atributos', $atributos);
            $this->CI->session->set_userdata('popup', 0);
        }
        $log['tipo'] = $usuario['Usuario']['TipoUsuario'];
        $log['msg'] = '';
        return $log;
    }

    public function userLogged() {
        if ($this->is_logged_in()) {
            $userLogged = $this->CI->Users_m->get($this->CI->session->userdata('user_id'));
            return $userLogged;
        }
    }

}
