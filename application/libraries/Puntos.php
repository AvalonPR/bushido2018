<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Puntos {
	private $CI;
	private $data = array();
	public function __construct(){
		$this->CI = & get_instance();
	}

	public function calcularPuntos($cod_user,$email,$tipo,$ranking,$cipher64,$time){
            ini_set('max_execution_time', 300);
            $paramObtPed = array(
                'sFechaDesde' => '2019-01-01',
                'sFechaHasta'=> '2019-12-31',
                'sBushido' => 'Y',
                'sYCN' => ''
            );
            $facturacion = $this->CI->client->obtenerVentasReales($email,$cipher64['0'],$tipo,$cipher64['1'],$paramObtPed);
            if(isset($facturacion['TextError'])){
                $this->CI->template->new_toast($facturacion['TextError'], 5000);
            }else{
                $obtenidos = 0;
                $obtenidos = $this->calcular($obtenidos, $facturacion);
                $this->actualizarDB($cod_user, $ranking, $obtenidos);
                $puntos = $this->CI->PuntosTotales_m->get_by_coduser($cod_user);
                return $puntos->disponibles;
            }
	}


	public function calcular($obtenidos, $facturacion = false){
            $puntos = array();
	    if (!empty($facturacion) ) {
	        foreach($facturacion as $fa){
	            if(isset($fa['0'])){
	                foreach($fa as $f){
	                        $obtenidos += $f['PuntosTotales'];
	                }
	            } else {
	                    $obtenidos += $fa['PuntosTotales'];
	            }
	        }
	    }
	    return $obtenidos;
        }


        /*
         * Clase que actualiza la tabla de puntosTotales
         *
         * @autor: Carlos Sáenz Domínguez.
         * @param:
         *         $cod_user: Código del usuario.
         *         $ranking: Ranking yokohama del usuario.
         *         $obtenidos: Sumatorio de puntos obtenidos.
         * @see: switchLimite(@deprecated); calcularLimite
         * @version: 1.0
         *
         */

        public function actualizarDB($cod_user, $ranking, $obtenidos){
            $id = $this->CI->PuntosTotales_m->getID($cod_user);
            $item = new stdClass();
            if(!empty($id)){
                $puntos = $this->CI->PuntosTotales_m->get_by_coduser($cod_user);
                $item = $this->calcularLimite($puntos,$obtenidos,'5900');
                $item->fechaActu = time();
                $this->CI->PuntosTotales_m->upsert($item, $id->idpuntos);
            } else {
                $puntos = new stdClass();
                $puntos->obtenidos = 0;
                $puntos->redimidos = 0;
                $puntos->acumulado = 0;
                $item = $this->calcularLimite($puntos, $obtenidos, '5900');
                $item->cod_user = $cod_user;
                $item->fechaActu = time();
                $this->CI->PuntosTotales_m->upsert($item);
            }
        }
        
        /*
         * Método para encapsular el switch del ranking
         *
         * @autor: Carlos Sáenz Domínguez.
         * @deprecated: Método para calcular en función del RANKING yokohama.
         * Se cambió el método de calculo a fecha 28/2/2017 en mail de María
         * Señorans.
         * @param:
         *         $puntos: Array de los puntos obtenidos, disponibles, obtenidos de la BBDD.
         *         $obtenidos: Sumatorio de puntos obtenidos.
         *         $ranking: Ranking obtenido de SAP para el usuario.
         * @return:
         *          $item: Stdclass para actualizar la base de datos.
         * @see: calcularLimite
         * @version: 1.0
         *
         */
        private function switchLimite($puntos,$obtenidos,$ranking){
            switch($ranking){
                case "A":
                    $item = $this->calcularLimite($puntos, $obtenidos, '5900');
                break;

                case "B":
                    $item = $this->calcularLimite($puntos, $obtenidos, '4700');
                break;

                case "C":
                    $item = $this->calcularLimite($puntos, $obtenidos, '3200');
                break;

                case "D":
                    $item = $this->calcularLimite($puntos, $obtenidos, '2000');
                break;

                case "E":
                    $item = $this->calcularLimite($puntos, $obtenidos, '800');
                break;

                case "F":
                    $item = $this->calcularLimite($puntos, $obtenidos, '800');
                break;
            }
            return $item;
        }

        /*
         * Método para calcular el límite de puntos.
         *
         * @autor: Carlos Sáenz Domínguez.
         * @param:
         *         $puntos: Array de los puntos obtenidos, disponibles, obtenidos de la BBDD.
         *         $obtenidos: Sumatorio de puntos obtenidos.
         *         $maxPuntos: Puntos máximos que puede alcanzar.
         * @return:
         *          $item: Stdclass para actualizar la tabla puntosTotales.
         * @version: 1.0
         *
         */
        private function calcularLimite($puntos, $obtenidos, $maxPuntos){
            $item = new stdClass();
            if($obtenidos>=$maxPuntos){
                $item->obtenidos = '5900';
                $item->disponibles = '5900' - $puntos->redimidos;
                $item->acumulado = $obtenidos;
            } else {
                $item->obtenidos = $obtenidos;
                $item->disponibles = $obtenidos - $puntos->redimidos;
                $item->acumulado = $obtenidos ;
            }
            return $item;
        }
}
