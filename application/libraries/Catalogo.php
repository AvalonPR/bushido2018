<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Catalogo {

    const CATALOGO_CONFIG_FILE = 'catalogo';

    private $CI;
    private $apikey;
    private $data = array();

    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->config->load(self::CATALOGO_CONFIG_FILE); // load config file
        $config = $this->CI->config->item('catalogo');
        $this->get_productos($config['apikey']);
        $this->ordenar_por_precio();
    }

    private function get_productos($apikey) {
        $url = 'https://apps.avalonprplus.com/api/productos/' . $apikey;
        $json = json_decode($this->file_get_contents_curl($url), true);
        if ($json['error']) {
            show_error('Catalogo: ' . $json['message']);
        } else {
            $this->data = $json['response'];
        }
    }

    private function file_get_contents_curl($url) {
        $userAgent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);       

        $data = curl_exec($ch);
        
        curl_close($ch);
        return $data;
    }

    private function ordenar_por_precio() {
        $precios = array();
        foreach ($this->data as $k => $item) {
            $precios[$k] = (float) $item['precio'];
        }
        if (is_array($precios)) {
            array_multisort($precios, SORT_ASC, $this->data);
        }
    }

    public function productos($categoria = false, $subcategoria = false, $familia = false, $limit = false, $minprice = false, $random = false) {
        $out = $this->data;
        foreach ($out as $k => $data) {
            if ($minprice && $data['precio'] <= $minprice) {
                unset($out[$k]);
            }
            if (!$data['estado']) {
                unset($out[$k]);
            }
            if ($familia && $data['parent']['familia']['slug'] != $familia) {
                unset($out[$k]);
            }
            if ($subcategoria && $data['parent']['subcategoria']['slug'] != $subcategoria) {
                unset($out[$k]);
            }
            if ($categoria && $data['parent']['categoria']['slug'] != $categoria) {
                unset($out[$k]);
            }
        }
        if ($random && $limit) {
            shuffle($out);
            $out = array_slice($out, 0, $limit);
        }

        if ($limit) {
            $out = array_slice($out, 0, $limit);
        }
        sort($out, SORT_NUMERIC);
        if($this->CI->session->userdata['site_lang']!='spanish'){
            $i=0;
            foreach($out as $o){
                $out[$i]['nombre'] = $o['nombrePT'];
                $out[$i]['precio'] = $o['precioPT'];
                if(isset($out[$i]['precioDto'])){
                    $out[$i]['precioDto'] = $o['precioPTDto'];
                }
                $out[$i]['descripcion'] = $o['descripcionPT'];
                $i++;
            }
        }
        return $out;
    }

    public function get_producto_by_slug($slug) {
        if (!$slug)
            return false;
        foreach ($this->data as $producto) {
            if ($producto['slug'] == $slug) {
                if($this->CI->session->userdata['site_lang']!='spanish'){
                    $producto['nombre'] = $producto['nombrePT'];
                    $producto['precio'] = $producto['precioPT'];
                    if(isset($producto['precioDto'])){
                        $producto['precioDto'] = $producto['precioPTDto'];
                    }
                    $producto['descripcion'] = $producto['descripcionPT'];
                }
                return $producto;
            }
        }
        return false;
    }
    
    
    public function get_producto_by_id($id) {
        if (!$id)
            return false;
        foreach ($this->data as $producto) {
            if ($producto['id'] == $id) { 
                if($this->CI->session->userdata['site_lang']!='spanish'){
                    $producto['nombre'] = $producto['nombrePT'];
                    $producto['precio'] = $producto['precioPT'];
                    if(isset($producto['precioDto'])){
                        $producto['precioDto'] = $producto['precioPTDto'];
                    }
                    $producto['descripcion'] = $producto['descripcionPT'];
                }
                return $producto;
            }
        }
        return false;
    }

    public function productos_by_slug($categoria = false, $subcategoria = false, $familia = false, $limit = false, $minprice = false) {
        $out = $this->data;

        foreach ($out as $k => $data) {
            if ($minprice && $data['precio'] <= $minprice) {
                unset($out[$k]);
            }
            if (!$data['estado']) {
                unset($out[$k]);
            }
            if ($familia && $data['parent']['familia']['slug'] != $familia) {
                unset($out[$k]);
            }
            if ($subcategoria && $data['parent']['subcategoria']['slug'] != $subcategoria) {
                unset($out[$k]);
            }
            if ($categoria && $data['parent']['categoria']['slug'] != $categoria) {
                unset($out[$k]);
            }
        }
        if ($limit) {
            $out = array_slice($out, 0, $limit);
        }
        if($this->CI->session->userdata['site_lang']!='spanish'){
            $i=0;
            foreach($out as $o){
                $out[$i]['nombre'] = $o['nombrePT'];
                $out[$i]['precio'] = $o['precioPT'];
                if(isset($out[$i]['precioDto'])){
                    $out[$i]['precioDto'] = $o['precioPTDto'];
                }
                $out[$i]['descripcion'] = $out[$i]['descripcionPT'];
                $i++;
            }
        }
        return $out;
    }

    public function jerarquia($categoria = false, $subcategoria = false, $familia = false) {
        $out = $this->data;
        $j = array();
        foreach ($out as $k => $data) {
            if ($categoria == $data['parent']['categoria']['slug'] || !$categoria) {
                if (!isset($j[$data['parent']['categoria']['slug']])) {
                    $j[$data['parent']['categoria']['slug']] = array(
                        'children' => array(),
                        'nombre' => $data['parent']['categoria']['nombre'],
                    );
                }
            }
            if (
                    ($subcategoria == $data['parent']['subcategoria']['slug'] || !$subcategoria) &&
                    ($categoria == $data['parent']['categoria']['slug'] || !$categoria)
            ) {
                if (!isset($j[$data['parent']['categoria']['slug']]['children'][$data['parent']['subcategoria']['slug']])) {
                    $j[$data['parent']['categoria']['slug']]['children'][$data['parent']['subcategoria']['slug']] = array(
                        'children' => array(),
                        'nombre' => $data['parent']['subcategoria']['nombre'],
                    );
                }
            }

            if (
                    ($familia == $data['parent']['familia']['slug'] || !$familia) &&
                    ($subcategoria == $data['parent']['subcategoria']['slug'] || !$subcategoria) &&
                    ($categoria == $data['parent']['categoria']['slug'] || !$categoria)
            ) {
                if (!isset($j[$data['parent']['categoria']['slug']]['children'][$data['parent']['subcategoria']['slug']]['children'][$data['parent']['familia']['slug']])) {
                    $j[$data['parent']['categoria']['slug']]['children'][$data['parent']['subcategoria']['slug']]['children'][$data['parent']['familia']['slug']] = array(
                        'children' => array(),
                        'nombre' => $data['parent']['familia']['nombre'],
                    );
                }
            }
        }
        return $j;
    }
    
    public function jerarquiaPT($categoria = false, $subcategoria = false, $familia = false) {
        $out = $this->data;
        $j = array();
        foreach ($out as $k => $data) {
            if ($categoria == $data['parent']['categoria']['slug'] || !$categoria) {
                if (!isset($j[$data['parent']['categoria']['slug']])) {
                    $j[$data['parent']['categoria']['slug']] = array(
                        'children' => array(),
                        'nombre' => $data['parent']['categoria']['nombrePT'],
                    );
                }
            }
            if (
                    ($subcategoria == $data['parent']['subcategoria']['slug'] || !$subcategoria) &&
                    ($categoria == $data['parent']['categoria']['slug'] || !$categoria)
            ) {
                if (!isset($j[$data['parent']['categoria']['slug']]['children'][$data['parent']['subcategoria']['slug']])) {
                    $j[$data['parent']['categoria']['slug']]['children'][$data['parent']['subcategoria']['slug']] = array(
                        'children' => array(),
                        'nombre' => $data['parent']['subcategoria']['nombrePT'],
                    );
                }
            }

            if (
                    ($familia == $data['parent']['familia']['slug'] || !$familia) &&
                    ($subcategoria == $data['parent']['subcategoria']['slug'] || !$subcategoria) &&
                    ($categoria == $data['parent']['categoria']['slug'] || !$categoria)
            ) {
                if (!isset($j[$data['parent']['categoria']['slug']]['children'][$data['parent']['subcategoria']['slug']]['children'][$data['parent']['familia']['slug']])) {
                    $j[$data['parent']['categoria']['slug']]['children'][$data['parent']['subcategoria']['slug']]['children'][$data['parent']['familia']['slug']] = array(
                        'children' => array(),
                        'nombre' => $data['parent']['familia']['nombrePT'],
                    );
                }
            }
        }
        return $j;
    }

}
