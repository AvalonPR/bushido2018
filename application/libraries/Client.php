<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH."third_party/phpseclib/Crypt/RSA.php";
class Client{
	private $CI;
        //private $wsdl='https://137.74.67.245:30444/YokohamaTestWS/YokohamaService.asmx?wsdl'; //Test
        private $wsdl= 'https://137.74.67.245:30443/YokohamaWS/YokohamaService.asmx?wsdl';
        private $endpoint = 'http://www.expertone.es/YokohamaService/';
	private $data = array();
	public function __construct(){
		$this->CI = & get_instance();
                $this->wsdl;
                $this->options= array(
                    'uri'=>'http://schemas.xmlsoap.org/soap/envelope/',
                    'style'=>SOAP_RPC,
                    'use'=>SOAP_ENCODED,
                    'soap_version'=>SOAP_1_1,
                    'cache_wsdl'=>WSDL_CACHE_NONE,
                    'keep_alive' => false,
                    'encoding'=>'UTF-8'
                );
                $this->endpoint;
                $this->soap = new SoapClient($this->wsdl, $this->options);
	}
    
      
    public function obtenerUsuarios($user, $pais) {
        $paramGetUser = array(
            'sUsuario' => $user,
            'sPaisDB' => $pais
        );
        try {
            $valor = $this->soap->obtenerUsuarios($paramGetUser);
            
        } catch (Exception $e) {
            die($e->getMessage());
        }
        $usuario = $this->createArray(new SimpleXMLElement($valor->ObtenerUsuariosResult->any));
        return $usuario;
    }
    
    public function obtenerClavePublica() {
        try {
            $valor = $this->soap->obtenerClavePublica();          
        }

        catch(Exception $e) {
                die($e->getMessage());
        }
        return $valor->ObtenerClavePublicaResult->any;
    }
      
    public function createPassword($password, $publickey=false){
        if($publickey==false){
            $publickey = $this->obtenerClavePublica();
        }
        $password64 = mb_convert_encoding($password,'UCS-2LE','auto');
        $rsa = new Crypt_RSA();
        $rsa->setEncryptionMode(CRYPT_RSA_ENCRYPTION_PKCS1);
        $rsa->loadKey($publickey);
        $rsa->setPublicKey($publickey);
        $ciphertext = $rsa->encrypt($password64);
        $cipher64 = base64_encode($ciphertext);
        $certPublicKey = $rsa->getPublicKey();
        return array($cipher64,
                    $publickey,
                    $certPublicKey);
    }
    
    public function getheaderAuth($user, $cipher64, $tipo, $certPublicKey){
        $headerbodyAuth = array(
            'PublicKey'=> $certPublicKey,
            'Username'=>$user,
            'Password'=>$cipher64,
            'NewPassword'=>'',
            'UserType'=>$tipo
        ); 
        return $headerbodyAuth;
    }
    
    public function getheader($user, $cipher64, $tipo, $publicKey, $newpass=false){
        if($newpass==false){
            $newpass = '';
        }
        $headerbody = array(
            'PublicKey'=>$publicKey,
            'Username'=>$user,
            'Password'=>$cipher64,
            'NewPassword'=>$newpass,
            'UserType'=>$tipo
        );
        return $headerbody;
    }
    
    public function iniciarSesion($user, $password, $tipo, $certPublicKey,$pais) {
        $paramInSess = array(
            'sPaisDB' => $pais
        );
        
        $headerbodyAuth = $this->getheaderAuth($user, $password, $tipo, $certPublicKey);
        $headerAuth = new SOAPHeader($this->endpoint, 'AuthHeader', $headerbodyAuth);
        $this->soap->__setSoapHeaders($headerAuth);
        try {
            $data = $this->soap->IniciarSesion($paramInSess);            
        }
        
        catch(Exception $e) {
            die($e->getMessage());
        }
        
        $headers = $this->soap->_cookies;
        $data = $this->createArray(new SimpleXMLElement($data->IniciarSesionResult->any));
        $return = array(
            'data'=>$data,
            'headers'=>$headers
        );
        return $return;
    }
    
    public function restablecerContrasenna($user,$tipo,$pass,$pais){
        $paramRes = array(
            'sUser' => $user,
            'sTipoUsuario' => $tipo,            
            'sNuevaPass' => $pass,
            'sPaisDB' => $pais
        );
        try {
             $valor = $this->soap->RestablecerContrasenna($paramRes);
        }

        catch(Exception $e) {
            die($e->getMessage());
        }
        $data = $this->createArray(new SimpleXMLElement($valor->RestablecerContrasennaResult->any));
        return $data;
    }
        
    public function obtenerClientes($user, $password, $tipo, $publicKey){
        $headerbody = $this->getheader($user, $password, $tipo, $publicKey);
        $header = new SOAPHeader($this->endpoint, 'AuthHeader', $headerbody);
        
        $this->soap->__setSoapHeaders($header);
        if(isset($this->CI->session->userdata['cookie'])){
            $this->soap->__setCookie('ASP.NET_SessionId', $this->CI->session->userdata['cookie']['ASP.NET_SessionId'][0]);
        }
        try {
            $data = $this->soap->ObtenerClientes();            

        }

        catch(Exception $e) {
            die($e->getMessage());
        }
        
        $data = $this->createArray(new SimpleXMLElement($data->ObtenerClientesResult->any));
        return $data;
    }
    
        public function obtenerVencimientosPendientes($user, $password, $tipo, $publicKey){
        $headerbody = $this->getheader($user, $password, $tipo, $publicKey);
        $header = new SOAPHeader($this->endpoint, 'AuthHeader', $headerbody);
        
        $this->soap->__setSoapHeaders($header);
        if(isset($this->CI->session->userdata['cookie'])){
            $this->soap->__setCookie('ASP.NET_SessionId', $this->CI->session->userdata['cookie']['ASP.NET_SessionId'][0]);
        }
        try {
            $data = $this->soap->ObtenerVencimientosPendientes();            

        }

        catch(Exception $e) {
            die($e->getMessage());
        }
        
        $data = $this->createArray(new SimpleXMLElement($data->ObtenerVencimientosPendientesResult->any));
        return $data;
    }
            
    public function cambiarContrasenna($user, $password, $tipo, $publicKey, $newpass){
        $newpass = $this->createPassword($newpass, $publicKey);
        $headerbody = $this->getheader($user, $password, $tipo, $publicKey, $newpass['0']);
        $header = new SOAPHeader($this->endpoint, 'AuthHeader', $headerbody);
        
        $this->soap->__setSoapHeaders($header);
        $this->soap->__setCookie('ASP.NET_SessionId', $this->CI->session->userdata['cookie']['ASP.NET_SessionId'][0]);
        try {
            $data = $this->soap->cambiarContrasenna();
        }

        catch(Exception $e) {
            die($e->getMessage());
        }
        
        $data = $this->createArray(new SimpleXMLElement($data->CambiarContrasennaResult->any));
        return $data;
    }
    
    public function obtenerFacturacion($user, $password, $tipo, $publicKey){
        $headerbody = $this->getheader($user, $password, $tipo, $publicKey);
        $header = new SOAPHeader($this->endpoint, 'AuthHeader', $headerbody);
        
        $this->soap->__setSoapHeaders($header);
        $this->soap->__setCookie('ASP.NET_SessionId', $this->CI->session->userdata['cookie']['ASP.NET_SessionId'][0]);
        try {
            $data = $this->soap->ObtenerFacturacion();
        }

        catch(Exception $e) {
            die($e->getMessage());
        }
        
        $data = $this->createArray(new SimpleXMLElement($data->ObtenerFacturacionResult->any));
        return $data;
    }
        
    public function obtenerAspecto($user, $password, $tipo, $publicKey, $param){
        $headerbody = $this->getheader($user, $password, $tipo, $publicKey);
        $header = new SOAPHeader($this->endpoint, 'AuthHeader', $headerbody);
        $this->soap->__setSoapHeaders($header);
        $this->soap->__setCookie('ASP.NET_SessionId', $this->CI->session->userdata['cookie']['ASP.NET_SessionId'][0]);
        
        try {
            $data = $this->soap->ObtenerAspecto($param);
        }
        catch(Exception $e) {
            die($e->getMessage());
        }
        
        
        $data = $this->createArray(new SimpleXMLElement($data->ObtenerAspectoResult->any));
        
        return $data;
    }
        
    public function obtenerWIDTH($user, $password, $tipo, $publicKey, $param){
        $headerbody = $this->getheader($user, $password, $tipo, $publicKey);
        $header = new SOAPHeader($this->endpoint, 'AuthHeader', $headerbody);
        
        $this->soap->__setSoapHeaders($header);
        $this->soap->__setCookie('ASP.NET_SessionId', $this->CI->session->userdata['cookie']['ASP.NET_SessionId'][0]);
        try {
            $data = $this->soap->ObtenerWIDTH($param);
        }

        catch(Exception $e) {
            die($e->getMessage());
        }
        
        $data = $this->createArray(new SimpleXMLElement($data->ObtenerWIDTHResult->any));
        return $data;
    }        
    public function obtenerPulgadas($user, $password, $tipo, $publicKey, $param){
        $headerbody = $this->getheader($user, $password, $tipo, $publicKey);
        $header = new SOAPHeader($this->endpoint, 'AuthHeader', $headerbody);
        
        $this->soap->__setSoapHeaders($header);
        $this->soap->__setCookie('ASP.NET_SessionId', $this->CI->session->userdata['cookie']['ASP.NET_SessionId'][0]);
        try {
            $data = $this->soap->ObtenerPulgadas($param);
        }

        catch(Exception $e) {
            die($e->getMessage());
        }
        
        $data = $this->createArray(new SimpleXMLElement($data->ObtenerPulgadasResult->any));
        return $data;
    }
        
    public function obtenerModelos($user, $password, $tipo, $publicKey, $param){
        $headerbody = $this->getheader($user, $password, $tipo, $publicKey);
        $header = new SOAPHeader($this->endpoint, 'AuthHeader', $headerbody);
        
        $this->soap->__setSoapHeaders($header);
        try {
            $data = $this->soap->ObtenerModelos($param);
        }

        catch(Exception $e) {
            die($e->getMessage());
        }
        
        $data = $this->createArray(new SimpleXMLElement($data->ObtenerModelosResult->any));
        return $data;
    }
        
    public function ObtenerIndexLoadAndCV($user, $password, $tipo, $publicKey, $param){
        $headerbody = $this->getheader($user, $password, $tipo, $publicKey);
        $header = new SOAPHeader($this->endpoint, 'AuthHeader', $headerbody);
        
        $this->soap->__setSoapHeaders($header);
        $this->soap->__setCookie('ASP.NET_SessionId', $this->CI->session->userdata['cookie']['ASP.NET_SessionId'][0]);
        try {
            $data = $this->soap->ObtenerIndexLoadAndCV($param);
        }

        catch(Exception $e) {
            die($e->getMessage());
        }
        
        $data = $this->createArray(new SimpleXMLElement($data->ObtenerIndexLoadAndCVResult->any));
        return $data;
    }
    
    public function obtenerFacturas($user, $password, $tipo, $publicKey,$paramObtPed=false){
        if($paramObtPed==false){
            $paramObtPed = array(
              'sFechaDesde' => '',  
              'sFechaHasta'=> '',
              'sBushido'=>'',
              'sYCN'=>''
            );
        }
        $headerbody = $this->getheader($user, $password, $tipo, $publicKey);
        $header = new SOAPHeader($this->endpoint, 'AuthHeader', $headerbody);
        
        $this->soap->__setSoapHeaders($header);
        $this->soap->__setCookie('ASP.NET_SessionId', $this->CI->session->userdata['cookie']['ASP.NET_SessionId'][0]);
        try {
            $data = $this->soap->ObtenerFacturas($paramObtPed);
        }
        catch(Exception $e) {
            die($e->getMessage());
        }
        
        $data = $this->createArray(new SimpleXMLElement($data->ObtenerFacturasResult->any));
        if(isset($data['TextError'])){
            redirect('inicio/logout', $data['TextError']);
        }else{
            return $data;
        }
    }
    
      public function obtenerViasPagoCliente($user, $password, $tipo, $publicKey,$paramObtPed){
        $paramObtPed = array(
          'sCodigoIC' => $paramObtPed
        );
        $headerbody = $this->getheader($user, $password, $tipo, $publicKey);
        $header = new SOAPHeader($this->endpoint, 'AuthHeader', $headerbody);
        
        $this->soap->__setSoapHeaders($header);
        $this->soap->__setCookie('ASP.NET_SessionId', $this->CI->session->userdata['cookie']['ASP.NET_SessionId'][0]);
        try {
            $data = $this->soap->ObtenerViasPagoCliente($paramObtPed);
        }
        catch(Exception $e) {
            die($e->getMessage());
        }
        
        $data = $this->createArray(new SimpleXMLElement($data->ObtenerViasPagoClienteResult->any));
        return $data;
    }
    
    public function obtenerPedidos($user, $password, $tipo, $publicKey, $paramObtPed=false){
        if($paramObtPed==false){
            $paramObtPed = array(
              'sFechaDesde' => '',  
              'sFechaHasta'=> ''
            );
        }
        $headerbody = $this->getheader($user, $password, $tipo, $publicKey);
        $header = new SOAPHeader($this->endpoint, 'AuthHeader', $headerbody);
        
        $this->soap->__setSoapHeaders($header);
        $this->soap->__setCookie('ASP.NET_SessionId', $this->CI->session->userdata['cookie']['ASP.NET_SessionId'][0]);
        try {
            $data = $this->soap->ObtenerPedidos($paramObtPed);
        }

        catch(Exception $e) {
            die($e->getMessage());
        }
        
        $data = $this->createArray(new SimpleXMLElement($data->ObtenerPedidosResult->any));
        if(isset($data['TextError'])){
            $this->CI->template->new_toast($data['TextError'],5000);
            $this->CI->session->sess_destroy();
            sleep(3);
            redirect('inicio/login');
        }
        return $data;
    }
    
    

    public function obtenerEntregas($user, $password, $tipo, $publicKey, $paramObtPed){
        ini_set('max_execution_time', '600');
        if($paramObtPed==false){
            echo "no puede";
        }
        $headerbody = $this->getheader($user, $password, $tipo, $publicKey);
        $header = new SOAPHeader($this->endpoint, 'AuthHeader', $headerbody);
        
        $this->soap->__setSoapHeaders($header);
        $this->soap->__setCookie('ASP.NET_SessionId', $this->CI->session->userdata['cookie']['ASP.NET_SessionId'][0]);
        try {
            $data = $this->soap->ObtenerEntregas($paramObtPed);
        }

        catch(Exception $e) {
            die($e->getMessage());
        }
        
        $data = $this->createArray(new SimpleXMLElement($data->ObtenerEntregasResult->any));
        return $data;
    }
    
    public function obtenerArticulos($user, $password, $tipo, $publicKey, $paramObtPed){
        ini_set('max_execution_time', '600');
        if($paramObtPed==false){
            echo "no puede";
        }
        $headerbody = $this->getheader($user, $password, $tipo, $publicKey);
        $header = new SOAPHeader($this->endpoint, 'AuthHeader', $headerbody);
        
        $this->soap->__setSoapHeaders($header);
        $this->soap->__setCookie('ASP.NET_SessionId', $this->CI->session->userdata['cookie']['ASP.NET_SessionId'][0]);
        try {
            $data = $this->soap->ObtenerArticulos($paramObtPed);
        }

        catch(Exception $e) {
            die($e->getMessage());
        }
        
        $data = $this->createArray(new SimpleXMLElement($data->ObtenerArticulosResult->any));
        return $data;
    }
    
    public function obtenerArticulosEstrella($user, $password, $tipo, $publicKey, $paramObtPed){
        if($paramObtPed==false){
            echo "no puede";
        }
        $headerbody = $this->getheader($user, $password, $tipo, $publicKey);
        $header = new SOAPHeader($this->endpoint, 'AuthHeader', $headerbody);
        
        $this->soap->__setSoapHeaders($header);
        $this->soap->__setCookie('ASP.NET_SessionId', $this->CI->session->userdata['cookie']['ASP.NET_SessionId'][0]);
        try {
            $data = $this->soap->ObtenerArticulosEstrella($paramObtPed);       

        }

        catch(Exception $e) {
            die($e->getMessage());
        }
        
        $data = $this->createArray(new SimpleXMLElement($data->ObtenerArticulosEstrellaResult->any));
        return $data;
    }


    public function obtenerVentasReales($user, $password, $tipo, $publicKey, $paramObtPed=false){
        ini_set('max_execution_time', '600');
        if($paramObtPed==false){
            $paramObtPed = array(
              'sFechaDesde' => '',  
              'sFechaHasta'=> ''
            );
        }
        $headerbody = $this->getheader($user, $password, $tipo, $publicKey);
        $header = new SOAPHeader($this->endpoint, 'AuthHeader', $headerbody);
        
        $this->soap->__setSoapHeaders($header);
        $this->soap->__setCookie('ASP.NET_SessionId', $this->CI->session->userdata['cookie']['ASP.NET_SessionId'][0]);
        try {
            $data = $this->soap->ObtenerVentasReales($paramObtPed);
        }

        catch(Exception $e) {
            die($e->getMessage());
        }
        
        $data = $this->createArray(new SimpleXMLElement($data->ObtenerVentasRealesResult->any));
        return $data;
    }
        
    public function cerrarSesion($user, $password, $tipo, $publicKey) {
        $headerbody = $this->getheader($user, $password, $tipo, $publicKey);
        $header = new SOAPHeader($this->endpoint, 'AuthHeader', $headerbody);
        try {
            $soap = new SoapClient($this->wsdl, $this->options);
        }

        catch(Exception $e) {
                die($e->getMessage());
        }
        
        $this->soap->__setSoapHeaders($header);
        $this->soap->__setCookie('ASP.NET_SessionId', $this->CI->session->userdata['cookie']['ASP.NET_SessionId'][0]);
        try {
            $valor = $this->soap->CerrarSesion();
        }

        catch(Exception $e) {
                die($e->getMessage());
        }
        return $valor;
    }
    
    
    public function crearPedido($user, $password, $tipo, $publicKey, $xml){
        ini_set('max_execution_time', '600');
        $headerbody = $this->getheader($user, $password, $tipo, $publicKey);
        $header = new SOAPHeader($this->endpoint, 'AuthHeader', $headerbody);
        $xml = array(
            'sXmlPedido' => $xml
        );
        
        $this->soap->__setSoapHeaders($header);
        $this->soap->__setCookie('ASP.NET_SessionId', $this->CI->session->userdata['cookie']['ASP.NET_SessionId'][0]);
        try {
            $data = $this->soap->CrearPedido($xml);
        }

        catch(Exception $e) {
            die($e->getMessage());
        }
        
        $data = $this->createArray(new SimpleXMLElement($data->CrearPedidoResult->any));
        return $data;
    }

    public function createArray($xml){
        ini_set('memory_limit', '2048m');
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);
        return $array;
    }

}               



