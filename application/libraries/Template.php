<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Template {

    const TWIG_CONFIG_FILE = 'template';

    private $CI;
    private $data = array();

    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->config->load(self::TWIG_CONFIG_FILE); // load config file
        $this->data = $this->CI->config->item('template');
    }

    public function get_data() {
        $this->data['userlogged'] = $this->CI->user->userLogged();
        $this->data['baseurl'] = base_url();
        $this->data['imgurl'] = site_url('assets/images/');
        $this->data['siteurl'] = $_SERVER['HTTP_HOST'] == 'localhost' ? 'http://localhost/Bushido2018/' : 'https://' . $_SERVER['HTTP_HOST'].'/';
        $this->data['cart_bushido'] = $this->CI->cart_bushido->contents();
        $this->data['cart_bushido_total'] = $this->CI->cart_bushido->total();
        $this->data['cart_bushido_items'] = $this->CI->cart_bushido->total_items();
        if ($this->data['userlogged']) {
            switch ($this->data['userlogged']->tipo) {
                case 'C': 
                    if($this->data['userlogged']->cod_user==0){
                        $puntos = $this->CI->PuntosTotales_m->get_by_coduser($this->data['userlogged']->cod_user);
                        $this->data['userlogged']->puntos = $puntos->disponibles;        
                        $this->CI->session->set_userdata('puntos', $puntos->disponibles);
                        break;
                    }
            }
        }
        return $this->data;
    }

    public function set_data($key, $value) {
        $this->CI->data[$key] = $value;
    }

    public function new_toast($tipe, $text) {
        echo '<div data-notify-position="bottom-right" id="dialog-sup" class="alert alert-' . $tipe . ' alert-dismissible" style="margin-bottom:0px;">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' . $text . '</div>';
    }

}
