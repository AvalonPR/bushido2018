<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Check whether the site is offline or not.
 *
 */
class Maintenance_hook
{
    public function __construct(){
        log_message('debug','Accessing maintenance hook!');
    }
    
    public function offline_check(){
        if(file_exists(APPPATH.'config/config.php')){
            include(APPPATH.'config/config.php');
            
            if(isset($config['maintenance_mode']) && $config['maintenance_mode'] === TRUE){
                
                $url = 'https://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
                if (strpos($url,'profissionais')){
                include(APPPATH.'views/mantenimiento/mantenimiento_pt.php');
                exit;
                } else{
                    include(APPPATH.'views/mantenimiento/mantenimiento.php');
                    exit;
                }
            }
        }
    }
}
