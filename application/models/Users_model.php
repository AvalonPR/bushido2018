<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Users_model extends CI_Model {
	var $table = 'users';
	var $active_column = 'estado';
	var $model_name_row = 'nombre';
	var $search_fields = array('nombre','descripcion');
	var $model_definition = array(
		'id'=>array(
			'type'=>'hidden',
			'label'=>'ID',
		),
	);

    function __construct(){
        parent::__construct();
    }

	function get_by_login($user,$pass){
		$pass =  md5($pass);
		$this->db->select('*')
		->from($this->table)
		->where('email',$user)
		->where('pass',$pass);
		$data = $this->db->get();
		if($data->num_rows())	return $data->row();
		return false;
	}

	function get_by_email($email){
		$this->db->select('*')
		->from($this->table)
		->where('email',$email);
		$data = $this->db->get();
		if($data->num_rows())	return $data->row();
		return false;
	}
        
        public function checkUser($nombre){
		$this->db->select('id, FechaHora')
		->from($this->table)
		->where('nombre',$nombre);
		$data = $this->db->get();
		if($data->num_rows())	return $data->row();
		return false;
        }

    function get($id){
		$this->db->select('*')
		->from($this->table)
		->where('id',$id);
		$data = $this->db->get();
		if($data->num_rows())	return $data->row();
		return false;
    }

    function get_by_coduser($coduser){
		$this->db->select('*')
		->from($this->table)
		->where('cod_user',$coduser);
		$data = $this->db->get();
		if($data->num_rows()){
			foreach ($data->result() as $row) $rows[] = $row;
			return $rows;
		}
		return false;
    }
    
        function get_conPuntos_logged($cod_user) {
        $this->db->select('*')
                ->from($this->table)
                ->where($this->table . '.cod_user', $cod_user);
        $this->db->join('puntosTotales', $this->table . '.cod_user = puntosTotales.cod_user');
        $data = $this->db->get();
        if ($data->num_rows())
            return $data->row();
        return false;
    }
    
function get_by_field($field,$value,$limit = '',$start = ''){
	$this->db->limit($limit,$start);
	$this->db->select('*')
	->from($this->table)
	->where($field,$value);
	$data = $this->db->get();
	if($data->num_rows()){
		foreach ($data->result() as $row) $rows[] = $row;
		return $rows;
	}
	return false;
}
    function getID($coduser){
            $this->db->select('id, cod_user')
                    ->from($this->table)
                    ->where('cod_user',$coduser);
		$data = $this->db->get();
		if($data->num_rows())	return $data->row();
		return false;
        }
    function getAdminID($email){
            $this->db->select('id, cod_user')
                    ->from($this->table)
                    ->where('email',$email);
		$data = $this->db->get();
		if($data->num_rows())	return $data->row();
		return false;
        }

            function getViaje($email){
            $this->db->select('viaje')
                    ->from($this->table)
                    ->where('email',$email);
		$data = $this->db->get();
		if($data->num_rows())	return $data->row();
		return false;
        }
        function getConex($coduser){
            $this->db->select('contConex_Bushido')
                    ->from($this->table)
                    ->where('cod_user',$coduser);
		$data = $this->db->get();
		if($data->num_rows())	return $data->row();
		return false;
        }

    function getVariables($id){            
        $this->db->select('nif, nombre, bases_B2B, bases_Bushido, privacidad_B2B, com_B2B, privacidad_Bushido, com_Bushido, nacionalidad, mostrarPrecio, viaje')
                    ->from($this->table)
                    ->where('id',$id);
		$data = $this->db->get();
		if($data->num_rows())	return $data->row();
		return false;
        }

	function count_users(){
		$this->db->select('count(*)')
		->from($this->table);

		$data = $this->db->get();
		if($data->num_rows())	return $data->row();
		return false;
	}

	public function get_all_users_pag_g($limit = '',$start = '',$field = '',$value = ''){
		if($limit){$this->db->limit($limit,$start);}
		$this->db->select('*')
		->from($this->table);
		if($field == 'nombre'){
			$this->db->like($field,$value);
		}elseif($field){
			$this->db->where($field,$value);
		}
		$this->db->where('pass','');
		$this->db->order_by('id', 'asc');
		$data = $this->db->get();

		if($data->num_rows()){
			$rows = array();
			foreach ($data->result() as $row) $rows[] = $row;
			return $rows;
		}
		return false;
	}

public function get_all_users_pag($limit = '',$start = '',$field = '',$value = ''){
	if($limit){$this->db->limit($limit,$start);}
        $this->db->select('(SELECT count(*)FROM puntosTotales as P2 WHERE P2.disponibles > P1.disponibles)+1 as rank, U1.ranking,U1.nacionalidad,U1.cod_user,U1.nombre, U1.nombreComercial,U1.email,U1.telefono,U1.viaje,U1.ano_viaje,U1.contConex,U1.direccion,U1.facturacionpv,U1.facturacionact,U1.facturacionViaje,U1.FechaHora,P1.obtenidos, P1.redimidos, P1.disponibles,P1.acumulado')
	->from('puntosTotales AS P1, users AS U1');
	if($field == 'nombre'){
		$this->db->like('U1.'.$field,$value);
        }elseif($field){
		$this->db->where('U1.'.$field,$value);
	}
	$this->db->where('P1.cod_user = U1.cod_user');
	$this->db->where('U1.tipo = "C"');
	$this->db->order_by('rank');
	$data = $this->db->get();

	if($data->num_rows()){
		$rows = array();
		foreach ($data->result() as $row) $rows[] = $row;
		return $rows;
	}
	return false;
}


	function get_all($page=false,$limit=15,$ordering = false){
		$this->db->limit($limit,$page);
		$this->db->select('*')
		->from($this->table);
		$this->db->order_by('id', 'desc');
/*
		if($ordering){
			$orden = explode('-',$ordering);
			$direccion = 'asc';
			if(count($orden)>1) $direccion = 'desc';
			$this->db->order_by($orden[0],$direccion);
		}
*/
//		if($page) $this->db->limit($limit,($page-1)*$limit);
		$data = $this->db->get();

		if($data->num_rows()){
			$rows = array();
			foreach ($data->result() as $row) $rows[] = $row;
			return $rows;
		}
		return false;
	}

	function get_all_active($page=false,$limit=15,$ordering = false){
		$this->db->select('*')
		->from($this->table)
		->where($this->active_column,1);

		$data = $this->db->get();

		if($data->num_rows()){
			$rows = array();
			foreach ($data->result() as $row) $rows[] = $row;
			return $rows;
		}
		return false;
	}
	public function upsert($item,$id=false){
		if($id){
			$this->db->where('id', $id);
			$this->db->update($this->table, $item);
			return true;
		} else {
			$this->db->insert($this->table, $item);
			return $this->db->insert_id();
		}
	}

	public function delete($id){
		$data = $this->get($id);
//		$this->papelera->elimina($this->table,$data);
		$this->db->flush_cache();
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	public function definition(){
		return $this->model_definition;
	}
}
