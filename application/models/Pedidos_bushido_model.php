<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pedidos_bushido_model extends CI_Model {

    var $table = 'pedidos_bushido';
    var $search_fields = array('id', 'usuario');

    /* 	var $model_definition = array(
      'id'=>array(
      'type'=>'hidden',
      'label'=>'ID',
      ),
      );
     */

    function __construct() {
        parent::__construct();
    }

    function get_pedido_byId($id, $user) {
        $this->db->select('*')
                ->from($this->table)
                ->where('id', $id)
                ->where('usuario', $user);
        $data = $this->db->get();
        if ($data->num_rows())
            return $data->row();
        return false;
    }

    public function upsert($data, $id = false) {
        if ($id) {
            $this->db->where('id', $id);
            $this->db->update($this->table, $data);
            return true;
        } else {
            $this->db->insert($this->table, $data);
            return $this->db->insert_id();
        }
    }

    function get($id) {
        $this->db->select('*')
                ->from($this->table)
                ->where('id', $id);
        $data = $this->db->get();
        if ($data->num_rows())
            return $data->row();
        return false;
    }

    public function delete($id) {
        $data = $this->get($id);
//		$this->papelera->elimina($this->table,$data);
        $this->db->flush_cache();
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    public function definition() {
        return $this->model_definition;
    }

    public function getPedidos() {
        $this->db->select('*')
                ->from($this->table);
        $this->db->order_by('fecha', 'desc');
        $data = $this->db->get();

        if ($data->num_rows()) {
            $rows = array();
            foreach ($data->result_array() as $row)
                $rows[] = $row;
            return $rows;
        }
        return false;
    }

    public function getPedidos_ByCod_user($cod_user) {
        $this->db->select('*')
                ->from($this->table)
                ->where('cod_user',$cod_user);
        $this->db->order_by('fecha', 'desc');
        $data = $this->db->get();

        if ($data->num_rows()) {
            $rows = array();
            foreach ($data->result_array() as $row)
                $rows[] = $row;
            return $rows;
        }
        return false;
    }
    
    public function getPedidosComercial($coordinador) {
        $this->db->select('*')
                ->from('pedidos as P, users as U')
                ->where('P.cod_user = U.cod_user AND U.dni_comercial="'.$coordinador.'"');
        $this->db->order_by('fecha', 'desc');
        $data = $this->db->get();

        if ($data->num_rows()) {
            $rows = array();
            foreach ($data->result() as $row)
                $rows[] = $row;
            return $rows;
        }
        return false;
    }

}
