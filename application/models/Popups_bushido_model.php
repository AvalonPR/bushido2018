<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Popups_bushido_model extends CI_Model {

    var $table = 'popups_bushido';
    var $active_column = 'estado';
    var $search_fields = array('estado', 'idioma');
    var $model_definition = array(
        'id' => array(
            'type' => 'hidden',
            'label' => 'ID',
        ),
    );

    function __construct() {
        parent::__construct();
    }

    function get($id = false) {
        $this->db->select('*')
                ->from($this->table);
        if ($id) {
            $this->db->where('id', $id);
        }
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
        if ($id) {
            if ($data->num_rows())
                return $data->row_array();
            return false;
        }else {
            if ($data->num_rows()) {
                $rows = array();
                foreach ($data->result() as $row)
                    $rows[] = $row;
                return $rows;
            }
        }
    }

    public function upsert($item, $id = false) {
        if ($id) {
            $this->db->where('id', $id);
            $this->db->update($this->table, $item);
            return true;
        } else {
            $this->db->insert($this->table, $item);
            return $this->db->insert_id();
        }
    }

    function get_by_title($titulo) {
        $this->db->select('*')
                ->from($this->table)
                ->where('titulo', $titulo);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
        if ($data->num_rows())
            return $data->row();
        return false;
    }

    function getActPopup() {
        $this->db->select('*')
                ->from($this->table)
                ->where('activo', '1');
                $this->db->order_by("id", "desc");
        $data = $this->db->get();
        if ($data->num_rows()) {
            $rows = array();
            foreach ($data->result_array() as $row)
                $rows[] = $row;
            return $rows;
        }
    }

}
