<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sumas_model extends CI_Model {
	var $table = 'sumas';
	var $active_column = 'estado';
	var $model_name_row = 'nombre';
	var $search_fields = array('nombre','descripcion');
	var $model_definition = array(
		'id'=>array(
			'type'=>'hidden',
			'label'=>'ID',
		),
	);

    function __construct(){
        parent::__construct();
    }

  function get(){
		$this->db->select('*')
		->from($this->table);
		$data = $this->db->get(); 
                if($data->num_rows()){
			$rows = array();
			foreach ($data->result_array() as $row) $rows[] = $row;
			return $rows;
		}
		return false;
  }
  
    public function upsert($item,$id=false){
            if($id){
                    $this->db->where('id', $id);
                    $this->db->update($this->table, $item);
                    return true;
            } else {
                    $this->db->insert($this->table, $item);
                    return $this->db->insert_id();
            }
    }
    
    function get_by_coduser($coduser){
		$this->db->select('*')
		->from($this->table)
		->where('cod_user',$coduser);
		$data = $this->db->get();
		if($data->num_rows()){
			foreach ($data->result() as $row) $rows[] = $row;
			return $rows;
		}
		return false;
    }
  
}