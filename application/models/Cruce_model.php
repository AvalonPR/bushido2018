<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Cruce_model extends CI_Model {
	var $table = 'cruce';
	var $active_column = '';
	var $model_name_row = '';
	var $search_fields = array('');
	var $model_definition = array(
		
	);
	
    function __construct(){
        parent::__construct();
    }
    
    function get($id){
		$this->db->select('*')
		->from($this->table)
		->where('id',$id);
		$data = $this->db->get();
		if($data->num_rows())	return $data->row();
		return false;
    }
	function check_data($data,$debug=false){
		$this->db->select('*')
		->from($this->table);
		foreach($data as $k=>$v){
			$this->db->where($k,$v);
		}
		$data = $this->db->get();
		if($debug) echo $this->db->last_query().' - '.$data->num_rows();
		if($data->num_rows()==1) return true;
		return false;
    }
	function get_by_name($str,$extra = false){
		$this->db->select('*')
		->from($this->table)
		->where($this->model_name_row,$str);
		if(is_array($extra)){
			foreach($extra as $k=>$v){
				$this->db->where($k,$v);
			}
		}
		$data = $this->db->get();
		if($data->num_rows())	return $data->row();
		return false;
    }
	function get_campos(){
		return $this->db->list_fields($this->table);
	}
	
	function get_all($page=false,$limit=15,$ordering = false){
		$search = false;
		if($this->session->userdata('busqueda')){
			$busqueda = $this->session->userdata('busqueda');
			if(isset($busqueda[str_replace('_model','',get_class($this))])){
				$search = $busqueda[str_replace('_model','',get_class($this))];
			}
		}
		$this->db->select('*')
		->from($this->table);
		if($page) $this->db->limit($limit,($page-1)*$limit);
		if($ordering){
			$orden = explode('-',$ordering);
			$direccion = 'asc';
			if(count($orden)>1) $direccion = 'desc';
			$this->db->order_by($orden[0],$direccion);
		}
		if($search){
			foreach($this->search_fields as $field){
				$this->db->or_like($field,$search);
			}
		}
		$filter=$this->session->userdata('filter');
		if(is_array($filter)){
			if(isset($filter[str_replace('_model','',get_class($this))])){
				foreach($filter[str_replace('_model','',get_class($this))] as $kf=>$vf){
					$this->db->where($kf,$vf);
				}
			}
		}
		
		$data = $this->db->get();
		
		if($data->num_rows()){
			$rows = array();
			foreach ($data->result() as $row) $rows[] = $row;
			return $rows;
		}
		return false;
	}
	function get_all_active($page=false,$limit=15,$ordering = false){
		$this->db->select('*')
		->from($this->table)
		->where($this->active_column,1);
		
		$data = $this->db->get();
		
		if($data->num_rows()){
			$rows = array();
			foreach ($data->result() as $row) $rows[] = $row;
			return $rows;
		}
		return false;
	}
	
	public function getNomClave($cod_user){
		$this->db->select('*')
		->from($this->table)
		->where('codDestino',$cod_user);
		$data = $this->db->get();
		if($data->num_rows())	return $data->row();
		return false;
	}

	
	public function upsert($item,$id=false){
		if($id){
			$this->db->where('id', $id);
			$this->db->update($this->table, $item); 
			return true;
		} else {
			$this->db->insert($this->table, $item);
			return $this->db->insert_id();
		}
	}
	
	
	public function delete($id){
		$data = $this->get($id);
		$this->papelera->elimina($this->table,$data);
		$this->db->flush_cache();	
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}
	
	
	
	public function definition(){
		return $this->model_definition;
	}
	
	
}