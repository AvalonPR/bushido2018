<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pedidos_bushido_v1_model extends CI_Model {

    var $table = 'pedidos_bushido_v1';
    var $search_fields = array('id', 'cod_user');

    /* 	var $model_definition = array(
      'id'=>array(
      'type'=>'hidden',
      'label'=>'ID',
      ),
      );
     */

    function __construct() {
        parent::__construct();
    }

    function get_pedido_byId($id, $user) {
        $this->db->select('*')
                ->from($this->table)
                ->where('id', $id)
                ->where('cod_user', $user);
        $data = $this->db->get();
        if ($data->num_rows())
            return $data->row();
        return false;
    }

    public function upsert($data, $id = false) {
        if ($id) {
            $this->db->where('id', $id);
            $this->db->update($this->table, $data);
            return true;
        } else {
            $this->db->insert($this->table, $data);
            return $this->db->insert_id();
        }
    }

    function get($id) {
        $this->db->select('*')
                ->from($this->table)
                ->where('id', $id);
        $data = $this->db->get();
        if ($data->num_rows())
            return $data->row();
        return false;
    }

    public function delete($id) {
        $data = $this->get($id);
//		$this->papelera->elimina($this->table,$data);
        $this->db->flush_cache();
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    public function definition() {
        return $this->model_definition;
    }

    public function getPedidos() {
        $this->db->select('*')
                ->from($this->table);
        $this->db->order_by('fecha', 'desc');
        $data = $this->db->get();

        if ($data->num_rows()) {
            $rows = array();
            foreach ($data->result_array() as $row)
                $rows[] = $row;
            return $rows;
        }
        return false;
    }
    
    public function getPedidosEst() {
        $this->db->select('*')
                ->from($this->table);
        $this->db->order_by('fecha', 'asc');
        $data = $this->db->get();

        if ($data->num_rows()) {
            $rows = array();
            foreach ($data->result_array() as $row)
                $rows[] = $row;
            return $rows;
        }
        return false;
    }
    
    public function getPedidosEstCom($comercial) {
        $this->db->select('*')
                ->from($this->table)
                ->where('comercial', $comercial);
        $this->db->order_by('fecha', 'asc');
        $data = $this->db->get();

        if ($data->num_rows()) {
            $rows = array();
            foreach ($data->result_array() as $row)
                $rows[] = $row;
            return $rows;
        }
        return false;
    }

    public function getPedidosAdmin($comercial = false, $provincia = false, $empresa = false) {
        if ($comercial) {
            $comercial = " AND U.coordinador='$comercial'";
        }
        if ($provincia) {
            $provincia = " AND U.provincia='$provincia'";
        }
        if ($empresa) {
            $empresa = " AND U.nombreClave='$empresa'";
        }
        $this->db->select('*')
                ->from('users as U, pedidos as P')
                ->where('P.usuario=U.cod_user' . $comercial . $provincia . $empresa);
        $this->db->order_by('fecha', 'desc');
        $data = $this->db->get();

        if ($data->num_rows()) {
            $rows = array();
            foreach ($data->result() as $row)
                $rows[] = $row;
            return $rows;
        }
        return false;
    }

    public function getPedidosComercial($comercial) {
        $this->db->select('*')
                ->from($this->table)
                ->where('comercial', $comercial);
        $this->db->order_by('fecha', 'desc');
        $data = $this->db->get();

        if ($data->num_rows()) {
            $rows = array();
            foreach ($data->result() as $row)
                $rows[] = $row;
            return $rows;
        }
        return false;
    }

    public function getPedidosUser($cod_user) {
        $this->db->select('*')
                ->from($this->table)
                ->where('cod_user', $cod_user);
        $data = $this->db->get();

        if ($data->num_rows()) {
            $rows = array();
            foreach ($data->result_array() as $row)
                $rows[] = $row;
            return $rows;
        }
        return false;
    }
}
