<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['template']['sitetitle'] = 'Yokohama Bushido';
$config['template']['apptitle'] = 'Yokohama Bushido';
$config['template']['logo'] = site_url('assets/images/logo-yokohama.png');
$config['template']['homeurl'] = site_url();
$config['template']['uri'] = $_SERVER['SERVER_NAME'] . $_SERVER["REQUEST_URI"];
$config['template']['imgurl'] = site_url('assets/images/');
$CI = &get_instance();

$config['template']['cssstyles'] = array(
    /* Fuentes */
    '<link href="https://fonts.googleapis.com/css?family=Lato|Passion+One" rel="stylesheet">'
    , '<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">'
    , '<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">'
    , '<link href="https://fonts.googleapis.com/css?family=Francois+One" rel="stylesheet">'
    , '<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">'
    , '<link rel="stylesheet" href="'. site_url('asets/css/fonts/stylesheet.css') . '" type="text/css" charset="utf-8" />'
    
    /* Ficheros CSS */
    , '<link rel="stylesheet" href="' . site_url('assets/css/bootstrap.css') . '" type="text/css" />'
    , '<link rel="stylesheet" href="' . site_url('assets/css/style.css') . '" type="text/css" />'
    , '<link rel="stylesheet" href="' . site_url('assets/css/swiper.css') . '" type="text/css" />'
    , '<link rel="stylesheet" href="' . site_url('assets/css/dark.css') . '" type="text/css" />'
    , '<link rel="stylesheet" href="' . site_url('assets/css/font-icons.css') . '" type="text/css" />'
    , '<link rel="stylesheet" href="' . site_url('assets/css/animate.css') . '" type="text/css" />'
    , '<link rel="stylesheet" href="' . site_url('assets/css/magnific-popup.css') . '" type="text/css" />'
    
    /* Subir ficheros */
    , '<link rel="stylesheet" href="' . site_url('assets/css/components/bs-filestyle.css') . '" type="text/css" />'
    /* Date & Time Picker CSS */
    , '<link rel="stylesheet" href="' . site_url('assets/css/datepicker.css') . '" type="text/css" />'
    , '<link rel="stylesheet" href="' . site_url('assets/css/components/timepicker.css') . '" type="text/css" />'
    , '<link rel="stylesheet" href="' . site_url('assets/css/components/daterangepicker.css') . '" type="text/css" />'
    , '<link rel="stylesheet" href="' . site_url('assets/css/components/bs-select.css') . '" type="text/css" />'
    , '<link rel="stylesheet" href="' . site_url('assets/css/components/radio-checkbox.css') . '" type="text/css" />'
    , '<link rel="stylesheet" href="' . site_url('assets/css/components/bs-datatable.css') . '" type="text/css" />'
    , '<link rel="stylesheet" href="' . site_url('assets/css/components/bs-switches.css') . '" type="text/css" />'
    , '<link rel="stylesheet" href="' . site_url('assets/css/responsive.css') . '" type="text/css" />'
    , '<link rel="stylesheet" href="' . site_url('assets/css/custom.css?v=21') . '" type="text/css" />'
    , '<link rel="stylesheet" href="' . site_url('assets/css/custom_responsive.css') . '" type="text/css" />'
    , '<meta name="viewport" content="width=device-width, initial-scale=1" />'
    , '<link rel="stylesheet" href="' . site_url('assets/css/components/select-boxes.css') . '" type="text/css" />'
    ,    '<script type="text/javascript" src="' . site_url('assets/js/jquery.js') . '"></script>'
    , '<style type="text/css">

    /* fuentes de google */
    .fuente-Lato {font-family:"Lato" sans-serif; font-size:20px;}
    </style>'
);

$config['template']['footerscripts'] = array(
     '<script type="text/javascript" src="' . site_url('assets/js/plugins.js') . '"></script>'
    , '<script type="text/javascript" src="' . site_url('assets/js/components/bs-filestyle.js') . '"></script>'
    , '<script type="text/javascript" src="' . site_url('assets/js/components/bs-select.js') . '"></script>'
    , '<script type="text/javascript" src="' . site_url('assets/js/components/bs-switches.js') . '"></script>'
    , '<script type="text/javascript" src="' . site_url('assets/js/components/datepicker.js') . '"></script>'
    , '<script type="text/javascript" src="' . site_url('assets/js/components/timepicker.js') . '"></script>'
    , '<script type="text/javascript" src="' . site_url('assets/js/components/daterangepicker.js') . '"></script>'
    , '<script type="text/javascript" src="' . site_url('assets/js/functions.js?v=11') . '"></script>'
    , '<script type="text/javascript" src="' . site_url('assets/js/cookie.js') . '"></script>'
    , '<script type="text/javascript" src="' . site_url('assets/js/components/bs-datatable.js') . '"></script>'
    , '<script type="text/javascript" src="' . site_url('assets/js/bootbox.min.js') . '"></script>'
    , '<script type="text/javascript" src="' . site_url('assets/js/datatables/jquery.dataTables.min.js') . '"></script>'
    , '<script type="text/javascript" src="' . site_url('assets/js/datatables/dataTables.bootstrap.min.js') . '"></script>'
    , '<script type="text/javascript" src="' . site_url('assets/js/datatables/dataTables.responsive.js') . '"></script>'
    , '<script type="text/javascript" src="' . site_url('assets/js/components/bs-switches.js') . '"></script>'
    , '<script type="text/javascript" src="' . site_url('assets/js/components/select-boxes.js') . '"></script>'
    , '<script type="text/javascript" src="' . site_url('assets/js/components/selectsplitter.js') . '"></script>'
    , '<script type="text/javascript" src="' . site_url('assets/js/typeahead.js/bloodhound.js') . '"></script>'
    , '<script type="text/javascript" src="' . site_url('assets/js/typeahead.js/bloodhound.min.js') . '"></script>'
    , '<script type="text/javascript" src="' . site_url('assets/js/typeahead.js/typeahead.bundle.js') . '"></script>'
    , '<script type="text/javascript" src="' . site_url('assets/js/typeahead.js/typeahead.jquery.js') . '"></script>'
    , '<script type="text/javascript" src="' . site_url('assets/js/typeahead.js/typeahead.jquery.min.js') . '"></script>'
    , '<script type="text/javascript" src="' . site_url('assets/js/typeahead.js/typeahead.bundle.min.js') . '"></script>'
    , '<script type="text/javascript" src="' . site_url('assets/js/the-basics.js') . '"></script>'
    , '<script type="text/javascript" src="' . site_url('assets/js/custom.js?v=21') . '"></script>'
    , '<script type="text/javascript" src="' . site_url('assets/js/jquery.gmap.js') . '"></script>'
    , '<script type="text/javascript" src="' . site_url('assets/js/plugins/jquery.magnific.js') . '"></script>'
    
);


$config['template']['user'] = $CI->session->userdata;


// MENU USER

/* Menu landing */
$menu['ES'] = array(
    'bienvenido' => 'Bienvenido',
    'area' => 'ÁREA PERSONAL',
    'perfil' => 'Mi Perfil',
    'trayectoria' => 'Trayectoria',
    'estadisticas' => 'Estadísticas',
    'pedidos' => 'Pedidos',
    'catalogo' => 'CATÁLOGO',
    'viaje' => 'VIAJE',
    'programa' => 'EL PROGRAMA',
    'condiciones' => 'Condiciones',
    'privacidad' => 'Privacidad',
    'canjeo' => 'Canjeo',
    'cookies' => 'Cookies',
    'contact' => 'Contacto',
    'logout' => 'Cerrar sesión',
    'carrito' => 'Mi carrito',
    'procesando' => 'Estamos procesando tu pedido'
);

$menu['PT'] = array(
    'bienvenido' => 'Bem-vindo',
    'area' => 'Área pessoal',
    'perfil' => 'Meu perfil',
    'trayectoria' => 'Trajetória',
    'pedidos' => 'Encomendas',
    'catalogo' => 'Catálogo',
    'viaje' => 'Viagem',
    'programa' => 'O programa',
    'condiciones' => 'Condições',
    'privacidad' => 'Privacidade',
    'canjeo' => 'Troca',
    'cookies' => 'Cookies',
    'contact' => 'Contacto',
    'logout' => 'Logout',
    'carrito' => 'Carrinho',
    'procesando' => 'Estamos processando seu pedido'
);

$footer['ES'] = array(
    'aviso' => 'Bases legales',
    'privacidad' => 'Privacidad',
    'cookies' => 'Política de cookies',
    'tfno' => '91 212 43 31',
    'direccion' => 'Paseo Doce Estrellas, 2',
    'direccion2'=> '28042 - Madrid',
    'direccion3' => 'Madrid'
);

$footer['PT'] = array(
    'aviso' => 'Bases legais',
    'privacidad' => 'Privacidade',
    'cookies' => 'Política de cookies',
    'tfno' => '252 249 070',
    'direccion' => 'Rua 12, Zona Industrial da Varziela',
    'direccion2'=> '4480-109 Vila do Conde',
    'direccion3' => 'Portugal'
);

$CI->load->library('session');
if(isset($CI->session->userdata['site_lang']) && $CI->session->userdata['site_lang'] == 'portuguese'){
        $config['template']['menu'] = $menu['PT'];
        $config['template']['footer'] = $footer['PT'];
}else{
        $config['template']['menu'] = $menu['ES'];
        $config['template']['footer'] = $footer['ES'];
}



