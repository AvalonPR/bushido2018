<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
ini_set("precision", 12);

$config['excel']['diferenciacion'] = array(
    'Payer (W/o C)' => 'maestro',
    'Brand Segment Dsc'/* 'Importe' */ => 'recambios',
    'Nombre del curso' => 'formacion',
    'Nº Dest.' => 'cruce'
);


$config['excel']['maestro']['cod_user'] = 'Payer (W/o C)';
$config['excel']['maestro']['nomCliente'] = 'Nombre';
$config['excel']['maestro']['llanta'] = 'RimSize Inch (Article no.)';
$config['excel']['maestro']['fecha'] ='Calendar day';
$config['excel']['maestro']['marca'] = 'Single Brand (td)';
$config['excel']['maestro']['total'] = 'Invoiced Sales, FreeCur';
$config['excel']['maestro']['unidades'] = 'Quantity';
$config['excel']['maestro']['archivo'] = 'POG Product Group (td)';

$config['excel']['recambios']['nomCliente'] = 'Debtor Name';
$config['excel']['recambios']['cif'] = 'VAT Number';
$config['excel']['recambios']['marca'] = 'Brand Segment Dsc';
$config['excel']['recambios']['total'] = 'Net Sales SUM';
$config['excel']['recambios']['unidades'] = 'Quantity SUM';
$config['excel']['recambios']['fecha'] = 'Calendar Month Desc';
$config['excel']['recambios']['archivo'] = 'Recambios';

$config['excel']['formacion']['cod_user'] = 'Payer';

$config['excel']['cruce']['codDestino'] = 'Nº Dest.';
$config['excel']['cruce']['codSede'] = 'Nº Sede';
$config['excel']['cruce']['nombreSede'] = 'Cliente sede';
$config['excel']['cruce']['nombreDestino'] = 'Cliente destino';
$config['excel']['cruce']['nombreClave'] = 'Cliente clave';

