<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Error404 extends CI_Controller { 
    
        var $data;

    function __construct() {
        parent::__construct();
        if ($this->router->fetch_method() != 'login') {
            if (!isset($this->session->userdata['logged_in'])) {
                redirect('inicio/login');
            } 
        }

        $this->data = $this->template->get_data();
    }

    public function index() {
        $vista = '404.php';
        $this->data['content'] = $this->twig->render($vista, array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl']));
        $this->twig->display('index.php', $this->data);
    }
    
}