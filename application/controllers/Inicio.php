<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Inicio extends CI_Controller {

    var $data;

    function __construct() {
        parent::__construct();
        if ($this->router->fetch_method() != 'login') {
            if (!isset($this->session->userdata['logged_in'])) {
                redirect('inicio/login');
            } elseif ($this->session->userdata['tipo'] === 'A') {
                redirect('admin');
            } elseif ($this->session->userdata['tipo'] === 'S') {
                redirect('comercial');
            } elseif ($this->session->userdata['bases_Bushido'] === '0') {
                if ($this->router->fetch_method() != 'bases' && $this->router->fetch_method() != 'canjeo' && $this->router->fetch_method() != 'privacidad' && $this->router->fetch_method() != 'cookies' && $this->router->fetch_method() != 'generales' && $this->router->fetch_method() != 'aceptarLegales' && $this->router->fetch_method() != 'logout') {
                    redirect('inicio/bases', 'refresh');
                }
            } elseif (is_null($this->session->userdata['viaje'])) {
                if ($this->router->fetch_method() != 'viaje' && $this->router->fetch_method() != 'aceptarViaje') {
                    redirect('inicio/viaje', 'refresh');
                }
            }
        }
        $this->data = $this->template->get_data();
    }

    public function login() {
        
        if(isset($this->session->userdata['logged_in'])){
            redirect('inicio');
        } else{
            if (isset($_POST['recuperar-form-sub'])) {
            $this->recuperar();
            } else if (isset($_POST['user'])) {
                $check = $this->user->check_login();
                if ($check['tipo'] == 'C') {
                    redirect('/inicio');
                } else if ($check['tipo'] == 'A') {
                    redirect('/admin');
                } elseif ($check['tipo'] == 'error') {
                    $this->template->new_toast('danger', $check['msg']);
                }
            } 
            $url = 'https://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
            if (strpos($url, 'pt')) {
                $vista = 'login_pt.php';
            } else {
                $vista = 'login.php';
            }
            $user_agent = $_SERVER['HTTP_USER_AGENT'];
            if (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident')) {
                $MSIE = true;
            } else {
                $MSIE = false;
            }
            $this->data['content'] = $this->twig->render($vista, array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'user' => $this->session->userdata, 'MSIE' => $MSIE));
            $this->twig->display('index.php', $this->data);
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('/');
    }

    public function index() {
        $sliders = $this->Sliders_bushido_m->getActSliders();
        if (!empty($sliders)) {
            $sliders = $this->setViewer($sliders, 'sliders');
        }
        $popups = $this->Popups_bushido_m->getActPopup();
        if (!empty($popups)) {
            $popups = $this->setViewer($popups, 'popups');
        }
        $fecha_actual = strtotime(date("Y-m-d", time()));
        $jerarquia = $this->catalogo->jerarquia();
        $productos = $this->catalogo->productos(false, false, false, 4, false, 1);
        $i = 0;
        foreach ($productos as $p) {
            $sort = preg_replace('!<[^>]+>!', '', $p['descripcion']);
            if (strlen($p['descripcion']) > 100) {
                $sort = substr($sort, 0, 100);
                $productos[$i]['descripcion'] = $sort . '...';
            } else {
                $productos[$i]['descripcion'] = $sort;
            }
            $i++;
        }
        $vista = 'taller/home.php';
        $this->data['content'] = $this->twig->render($vista, array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'sliders' => $sliders, 'productos' => $productos, 'jerarquia' => $jerarquia, 'textos' => $this->lang->line('home'), 'userData' => $this->data['userlogged'], 'popups' => $popups, 'user' => $this->session->userdata));
        $this->twig->display('index.php', $this->data);
    }

    public function viaje() {
        $vista = 'viaje.php';
        $this->data['content'] = $this->twig->render($vista, array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'textos' => $this->lang->line('viaje'), 'userData' => $this->data['userlogged']));
        $this->twig->display('index.php', $this->data);
    }
    
    public function marketing() {
        $vista = 'marketing.php';
        $this->data['content'] = $this->twig->render($vista, array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl']));
        $this->twig->display('index.php', $this->data);
    }

    public function canjeo() {
        $url = 'https://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        if (strpos($url, 'pt')) {
            $vista = 'canjeo_pt.php';
        } else {
            $vista = 'canjeo.php';
        }
        $this->data['content'] = $this->twig->render($vista, array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl']));
        $this->twig->display('index.php', $this->data);
    }
    
    public function cookies() {
        $url = 'https://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        if (strpos($url, 'pt')) {
            $vista = 'cookies_pt.php';
        } else {
            $vista = 'cookies.php';
        }
        $this->data['content'] = $this->twig->render($vista, array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl']));
        $this->twig->display('index.php', $this->data);
    }
    public function generales() {
        $url = 'https://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        if (strpos($url, 'pt')) {
            $vista = 'generales_pt.php';
        } else {
            $vista = 'generales.php';
        }
        $this->data['content'] = $this->twig->render($vista, array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl']));
        $this->twig->display('index.php', $this->data);
    }
    
    public function privacidad() {
        $url = 'https://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        if (strpos($url, 'pt')) {
            $vista = 'privacidad_pt.php';
        } else {
            $vista = 'privacidad.php';
        }
        $this->data['content'] = $this->twig->render($vista, array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl']));
        $this->twig->display('index.php', $this->data);
    }
    
    public function trayectoria() {
        $vista = 'taller/trayectoria.php';
        $this->data['content'] = $this->twig->render($vista, array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'],  'textos' => $this->lang->line('trayectoria')));
        $this->twig->display('index.php', $this->data);
        
    }

    public function bases() {
        $url = 'https://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        if (strpos($url, 'pt')) {
            $vista = 'bases_pt.php';
        } else {
            $vista = 'bases.php';
        }
        $this->data['content'] = $this->twig->render($vista, array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'user'=>$this->session->userdata));
        $this->twig->display('index.php', $this->data);
    }

    public function pedidos() {
        $pedidos = $this->Pedidos_bushido_m->getPedidos_ByCod_user($this->session->userdata['cod_user']);
        if($pedidos){
            $i = 0;
            foreach ($pedidos as $p) {
                $pedidos[$i]['pedido'] = json_decode($p['pedido'], true);
                $pedidos[$i]['datosEnvio'] = json_decode($p['datosEnvio'], true);
                $i++;
            }

            $i = 0;
            foreach ($pedidos as $p) {
                $p['idpedidos'] = str_pad($p['idpedidos'], 4, "0", STR_PAD_LEFT);
                foreach ($p['pedido'] as $k => $l) {
                    if (isset($l['options']['size'])) {
                        foreach ($l['options']['size'] as $linea) {
                            $o[] = json_decode($linea);
                        }
                        $pedidos[$i]['pedido'][$k]['options']['size'] = $o;
                    }
                }
                $i++;
            }
        }
        $this->data['content'] = $this->twig->render('taller/pedidos_taller.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl'], 'pedidos' => $pedidos, 'userData' => $this->data['userlogged']));
        $this->twig->display('index.php', $this->data);
    }
    
    
    public function pedidos_anteriores() {
        $pedidos = $this->Pedidos_bushido_v1_m->getPedidosUser($this->session->userdata['cod_user']);
        $i = 0;
        if($pedidos){
            foreach ($pedidos as $p) {
                $pedidos[$i]['pedido'] = json_decode($p['pedido'], true);
                $i++;
            }
        }
        $this->data['content'] = $this->twig->render('taller/pedidos_taller_bushido.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl'], 'pedidos' => $pedidos, 'userData' => $this->data['userlogged']));
        $this->twig->display('index.php', $this->data);
    }
    

    public function perfil() {
        $user = $this->Users_m->get_by_email($this->data['userlogged']->email);
        $direcciones = json_decode($user->direccion);
        $direcciones = json_decode(json_encode($direcciones), True);
        $direccionesEnvio = json_decode($user->direccionesEnvio);
        $direccionesEnvio = json_decode(json_encode($direccionesEnvio), True);
        $vista = 'taller/profile_taller_edit.php';
        $this->data['content'] = $this->twig->render($vista, array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'user' => $user, 'direcciones' => $direcciones, 'direccionesEnvio' => $direccionesEnvio, 'textos'=>$this->lang->line('perfil')));
        $this->twig->display('index.php', $this->data);
    }

    public function contacto() {
        $user = $this->Users_m->get_by_email($this->data['userlogged']->email);
        $vista = 'contact.php';
        $this->data['content'] = $this->twig->render($vista, array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'user' => $user, 'textos'=>$this->lang->line('contacto')));
        $this->twig->display('index.php', $this->data);
    }
    
    public function sendContact() {        
        $this->load->library("PhpMailerLib");
        $mail = $this->phpmailerlib->load();
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.office365.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'info.bushido@yokohamaiberia.com';                 // SMTP username
        $mail->Password = 'Bushido2017';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                   // TCP port to connect to
        //Recipients
        $mail->setFrom('info.bushido@yokohamaiberia.com');
        $mail->addAddress($this->data['userlogged']->email);     // Add a recipient
        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = utf8_decode('Has contacto en Yokohama Bushido');

        $body = $this->twig->render('mails/contacto_user.php', array('contacto'=>$_POST));
        $mail->Body = utf8_decode($body);
        $mail->send();
        $mail2 = $this->phpmailerlib->load();
        //Server settings
        $mail2->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail2->isSMTP();                                      // Set mailer to use SMTP
        $mail2->Host = 'smtp.office365.com';  // Specify main and backup SMTP servers
        $mail2->SMTPAuth = true;                               // Enable SMTP authentication
        $mail2->Username = 'info.bushido@yokohamaiberia.com';                 // SMTP username
        $mail2->Password = 'Bushido2017';                           // SMTP password
        $mail2->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail2->Port = 587;                                   // TCP port to connect to
        //Recipients
        $mail2->setFrom('info.bushido@yokohamaiberia.com');
        $mail2->addAddress('info.bushido@yokohamaiberia.com');     // Add a recipient
        //Content
        $mail2->isHTML(true);
        $mail2->Subject = utf8_decode('Nuevo contacto en Yokohama Bushido de ' . $this->data['userlogged']->razon_social);

        $body = $this->twig->render('mails/contacto_admin.php', array('contacto'=>$_POST));
        $mail2->Body = utf8_decode($body);
        $mail2->send();
        echo '{ "alert": "success", "message": "Gracias por ponerte en contacto con nosotros te contestaremos a la mayor brevedad." }';
    }

    public function error403() {
        $vista = '403.php';
        $this->data['content'] = $this->twig->render($vista, array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl']));
        $this->twig->display('index.php', $this->data);
    }

    public function aceptarLegales() {
        if ($_POST['bases'] == 'on') {
            $item = new stdClass();
            $item->bases_Bushido = '1';
            $item->privacidad_Bushido = '1';
            $this->session->set_userdata('bases_Bushido', 1);
            $this->session->set_userdata('privacidad_Bushido', 1);
            if($_POST['comunicaciones'] == 'on'){
                $this->session->set_userdata('com_Bushido', 1);
                $item->com_Bushido = '1';
                $texto = ' el cliente también ha aceptado las comunicaciones';
            } else{
                $this->session->set_userdata('com_Bushido', 0);
                $item->com_Bushido = '0';
            }
            $this->Users_m->upsert($item, $this->data['userlogged']->id);

            /*             * * INICIO ENVÍO EMAIL 3.0 **** */
            $this->load->library("PhpMailerLib");
            $mail = $this->phpmailerlib->load();
            try {
                //Server settings
                $mail->SMTPDebug = 0;                                 // Enable verbose debug output
                $mail->isSMTP();                                      // Set mailer to use SMTP
                $mail->Host = 'smtp.office365.com';  // Specify main and backup SMTP servers
                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                $mail->Username = 'info.bushido@yokohamaiberia.com';                 // SMTP username
                $mail->Password = 'Bushido2017';                           // SMTP password
                $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                $mail->Port = 587;                                   // TCP port to connect to
                //Recipients
                $mail->setFrom('info.bushido@yokohamaiberia.com');
                $mail->addAddress('marketing@yokohamaiberia.com');     // Add a recipient
                //Content
                $mail->isHTML(true);                                  // Set email format to HTML
                $mail->Subject = utf8_decode('El cliente ' . $this->data['userlogged']->nombre . ' aceptó las bases legales');
                $body = 'El cliente ' . $this->data['userlogged']->nombre . ' con código de usuario ' . $this->data['userlogged']->cod_user . ' aceptó las bases legales y la política de privacidad, a fecha ' . date("d-m-Y H:i:s") . ' desde la IP: ' . $this->get_client_ip() . $texto;
                $mail->Body = utf8_decode($body);
                $mail->send();
                echo "Gracias por aceptar nuestras bases legales.";
                exit;
            } catch (Exception $e) {
                $this->template->new_toast('error', 'Error al enviar el mensaje: ' . $mail->ErrorInfo);
            }
        } else {
            echo "Debe aceptar las bases legales.";
            exit;
        }
    }
    
    public function aceptarViaje() {
        $item = new stdClass();
        $item->viaje = $_POST['viaje'];
        $item->ano_viaje = $_POST['ano'];
        $this->session->set_userdata('viaje', $_POST['viaje']);
        $this->Users_m->upsert($item, $this->data['userlogged']->id);
        if($_POST['viaje']!=0){
            $cipher64 = $this->session->userdata['cipher64'];
            $facturacionViaje = $this->client->obtenerFacturacion($this->data['userlogged']->email, $cipher64['0'], $usuario['Usuario']['TipoUsuario'], $cipher64['1'], 'Y');
            if (isset($facturacionViaje['Facturado']['0'])) {
                foreach ($facturacionViaje['Facturado'] as $k => $v) {
                    if ($v['EJERCICIO'] == '2019') {
                        $item->facturacionViaje = $v['VALOR'];
                    }
                }
            } else {
                foreach ($facturacionViaje as $k => $v) {
                    if ($v['EJERCICIO'] == '2019') {
                        $item->facturacionViaje = $v['VALOR'];
                    }
                }
            }
            $this->session->set_userdata('puntos', $item->facturacionViaje);
        }
        echo "Gracias por aceptar nuestras bases legales.";
        exit;
    }
    
    public function changePw() {
        $cipher64 = $this->session->userdata('cipher64');
        if ($_POST['newpass'] != $_POST['newpass2']) {
            $respuesta =array(
              "error" => "0",
              "toast" => $this->lang->line('perfil')['datos']['messages']['nocoin']
            );
            print json_encode($respuesta, true);
        } else {
            $data = $this->client->cambiarContrasenna($this->data['userlogged']->email, $cipher64['0'], $this->data['userlogged']->ranking, $cipher64['1'], $_POST['newpass']);
            if (isset($data['TextError']) && $data['TextError'] != array()) {
                $respuesta =array(
                  "error" => "0",
                  "toast" => $data['TextError']
                );
                print json_encode($respuesta, true);
            } else if ($data['TextError'] == array()) {
                $respuesta =array(
                  "error" => "1",
                  "toast" => $this->lang->line('perfil')['datos']['messages']['changePw']
                );
                print json_encode($respuesta, true);
            } else {
                $respuesta =array(
                  "error" => "0",
                  "toast" => $this->lang->line('perfil')['datos']['messages']['error']
                );
                print json_encode($respuesta, true);
            }
        }
    }
    
    public function saveDir(){
        $user = $this->Users_m->get_by_email($this->data['userlogged']->email);
        $direccionesEnvio = json_decode($user->direccionesEnvio);
        if($direccionesEnvio == null){
            $dirEnvio[0] = array(
                'direccion' => $_POST['direccion'],
                'poblacion' => $_POST['poblacion'],
                'provincia' => $_POST['provincia'],
                'cp' => $_POST['cp'],
                'tfno' => $_POST['tfno'],
            );
            $dirEnvio = json_encode($dirEnvio, true);
        } else{
            $direccionesEnvio = json_decode(json_encode($direccionesEnvio), True);
            $dirEnvio = array(
                'direccion' => $_POST['direccion'],
                'poblacion' => $_POST['poblacion'],
                'provincia' => $_POST['provincia'],
                'cp' => $_POST['cp'],
                'tfno' => $_POST['tfno'],
            );
            array_push($direccionesEnvio, $dirEnvio);
            $dirEnvio = json_encode($direccionesEnvio, true);
        }
        $item = new stdClass();
        $item->direccionesEnvio = $dirEnvio;
        $this->Users_m->upsert($item, $this->data['userlogged']->id);
        echo '{ "alert": "successProfile", "message": "Dirección guardada con éxito." }';
        
    }

    public function changeAva() {
        # definimos la carpeta destino
        $carpetaDestino = "assets/images/avatares/".$this->data['userlogged']->cod_user."/";
        if (!file_exists($carpetaDestino)) {
            mkdir($carpetaDestino, 0777, true);
        }
        # si hay algun archivo que subir
        $config['encrypt_name'] = TRUE;
        $config['upload_path'] = $carpetaDestino;
        $config['allowed_types'] = '*';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('cambiarAvatar')) {
            $error = strip_tags($this->upload->display_errors());
            echo '{ "alert": "error", "message": "' . $error . '" }';exit(1);
        } else {
            $file = $this->upload->data('file_name');
            $item = new stdClass();
            $item->avatar = $file;
            $this->Users_m->upsert($item, $this->data['userlogged']->id);
            echo '{ "alert": "error", "message": "'.$this->lang->line('perfil')['datos']['messages']['avatarerrtype'].'" }';
        }
    }
        
    
     //Obtiene la IP del cliente
    function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
    
    
    private function setViewer($sliders, $tipo) {
        $sliders = $this->checkFecha($sliders, $tipo);
        $sliders = $this->checkIdioma($sliders);
        $sliders = $this->checkVars($sliders);
        return $sliders;
    }

    private function checkFecha($sliders, $tipo) {
        $i = 0;
        foreach ($sliders as $s) {
            if ($s['fechai'] != '' || $s['fechai'] != null) {
                $fecha_actual = strtotime(date("Y-m-d", time()));
                $fecha_fin = strtotime($s['fechaf']);
                if ($fecha_actual > $fecha_fin) {
                    $item = new stdClass();
                    $item->activo = 0;
                    $this->Admin_m->upsertView($tipo, $item, $s['id']);
                    unset($sliders[$i]);
                    continue;
                }
            }
            $i++;
        }
        return $sliders;
    }

    private function checkIdioma($sliders) {
        $setView = array();
        $i = 0;
        foreach ($sliders as $s) {
            $atributos = json_decode($s['atributos'], true);
            if (!isset($atributos['Pais']) || empty($atributos['Pais'])) {
                array_push($setView, $sliders[$i]);
            } else {
                foreach ($atributos['Pais'] as $v) {
                    if ($v == $this->session->userdata['nacionalidad']) {
                        array_push($setView, $sliders[$i]);
                    }
                }
            }
            $i++;
        }
        return $setView;
    }

    private function checkVars($sliders) {
        $setView = array();
        $i = 0;
        foreach ($sliders as $s) {
            $atributos = json_decode($s['atributos'], true);
            if (isset($atributos['Pais'])) {
                unset($atributos['Pais']);
            }
            if ($atributos == null || empty($atributos)) {
                array_push($setView, $sliders[$i]);
            } else {
                $match = count($atributos);
                $goal = 0;
                foreach ($atributos as $k => $v) {
                    if (array_key_exists($k, $this->session->userdata['atributos'])) {
                        if ($this->session->userdata['atributos'][$k] == $v) {
                            $goal++;
                        }
                    }
                }
                if ($match == $goal) {
                    array_push($setView, $sliders[$i]);
                }
            }
            $i++;
        }
        return $setView;
    }
    
    public function viewPopup() {
        $this->session->set_userdata('popup', 1);
    }
}
