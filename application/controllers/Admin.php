<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    var $data;

    function __construct() {
        parent::__construct();
        if (!isset($this->session->userdata['logged_in'])) {
            redirect('inicio/login');
        } else if ($this->session->userdata['tipo'] != 'A') {
            redirect('inicio');
        }
        $this->load->library('Excel');
        $this->load->library('Puntos');
        $this->data = $this->template->get_data();
    }

    public function ajax($action = false) {
        if ($action == 'saveForm') {
            $item = new stdClass();
            $item->nombre = $_POST['nombre'];
            $item->fecha = $_POST['fecha'];
            $this->Formacion_m->upsert($item);
            $formaciones = $this->Formacion_m->get();
            $fecha_actual = strtotime(date("Y-m-d", time()));
            $pastForms = array();
            $proxForms = array();
            foreach ($formaciones as $form) {
                $fechaprom = strtotime($form->fecha);
                if ($fecha_actual > $fechaprom) {
                    $pastForms[] = $form;
                } else {
                    $proxForms[] = $form;
                }
            }
            $table = '<table id="ProxFormTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Nombre formación</th>
                            </tr>
                        </thead>
                        <tbody>';
            foreach ($proxForms as $form) {
                $table .= '<tr>
                                <td>' . $form->fecha . '</td>
                                <td>' . $form->nombre . '</td>
                            </tr>';
            }
            $table .= '</tbody>
                    </table>';
            $respuesta = array(
                "table" => $table,
                "toast" => 'Formación actualizada'
            );
            print json_encode($respuesta, true);
        } else if($action == 'modifyUser'){
            $item = new stdClass();
            $item->estado = $_POST['estado'];
            $this->Users_m->upsert($item, $_POST['id']);
            print_r($_POST);
            echo "success";
        }
    }

    public function index() {
        $this->data['content'] = $this->twig->render('admin/home_admin.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl']));
        $this->twig->display('index.php', $this->data);
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('/');
    }

    public function home() {
        $textos = $this->Textos_m->getTextos();
        $textosTaller = json_decode($textos->footerSliderTaller, true);
        $textosUfinal = json_decode($textos->footerSliderUfinal, true);
        $this->data['content'] = $this->twig->render('admin/home_edit.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl'], 'textosTaller' => $textosTaller, 'textosUfinal' => $textosUfinal));
        $this->twig->display('index.php', $this->data);
    }

    public function saveTextos() {
        if ($_POST['textosTaller-estado'] == 'on') {
            $estadoTaller = 1;
        } else {
            $estadoTaller = 0;
        }
        if ($_POST['textosUfinal-estado'] == 'on') {
            $estadoUfinal = 1;
        } else {
            $estadoUfinal = 0;
        }
        $footerSliderTaller = array(
            'titulo' => $_POST['textosTaller-titulo'],
            'texto' => $_POST['textosTaller-texto'],
            'boton' => $_POST['textosTaller-boton'],
            'url' => $_POST['textosTaller-url'],
            'estado' => $estadoTaller
        );
        $footerSliderTaller = json_encode($footerSliderTaller, true);
        $footerSliderUfinal = array(
            'titulo' => $_POST['textosUfinal-titulo'],
            'texto' => $_POST['textosUfinal-texto'],
            'boton' => $_POST['textosUfinal-boton'],
            'url' => $_POST['textosUfinal-url'],
            'estado' => $estadoUfinal
        );
        $footerSliderUfinal = json_encode($footerSliderUfinal, true);
        $item = new stdClass();
        $item->footerSliderTaller = $footerSliderTaller;
        $item->footerSliderUfinal = $footerSliderUfinal;
        $this->Textos_m->upsert($item, '1');
        echo '{ "alert": "successTextos", "message": "Se modificó correctamente" }';
    }

    public function slider($action = false, $id = false) {
        if ($action == 'edit') {
            $slider = $this->Sliders_m->get($id);
            $this->data['content'] = $this->twig->render('admin/sliders_edit.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl'], 'slider' => $slider));
            $this->twig->display('index.php', $this->data);
        } else if ($action == 'add') {
            $this->data['content'] = $this->twig->render('admin/sliders_add.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl']));
            $this->twig->display('index.php', $this->data);
        } else {
            $sliders = $this->Sliders_m->getSliders();
            $this->data['content'] = $this->twig->render('admin/sliders.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl'], 'sliders' => $sliders));
            $this->twig->display('index.php', $this->data);
        }
    }

    public function addSlider($id = false) {
        $item = new stdClass();
        if (isset($_POST['estado'])) {
            $item->estado = 1;
        } else {
            $item->estado = 0;
        }
        $item->url = $_POST['url'];
        $item->fechaI = $_POST['fechaI'];
        $item->fechaF = $_POST['fechaF'];
        $item->titulo = $_POST['titulo'];
        $item->subtitulo = $_POST['subtitulo'];
        $item->boton = $_POST['boton'];
        //$item->alt=$_POST['alt']
        $item->zona = $_POST['zona'];
        if (isset($_FILES['imagen'])) {
            $item->imagen = $_FILES['imagen']['name'];
        }
        if ($id != null) {
            if (isset($_FILES['imagen'])) {
                $this->upload($_FILES['imagen'], 'sliders', $id);
            }
            $this->Sliders_m->upsert($item, $id);
            echo '{ "alert": "successSliders", "message": "Se modificó correctamente" }';
        } else {
            $this->Sliders_m->upsert($item);
            $id = $this->Sliders_m->getId($item->imagen);
            $this->upload($_FILES['imagen'], 'sliders', $id->idsliders);
            echo '{ "alert": "successSliders", "message": "Se subió correctamente" }';
        }
    }

    public function upload($file, $type, $id) {
        # definimos la carpeta destino
        if ($type == 'catalogo') {
            if ($file['type'] == "application/pdf" || $file["type"] == "csv" || $file["type"] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || $file["type"] == "application/vnd.ms-excel") {
                $carpetaDestino = "assets/catalogo/" . $id . "/pdf/";
            } else {
                $carpetaDestino = "assets/catalogo/" . $id . "/portada/";
            }
        } else if ($type == 'sliders') {
            $carpetaDestino = "assets/images/sliders/" . $id . "/";
        } else if ($type == 'pop-up') {
            $carpetaDestino = "assets/images/pop-up/" . $id . "/";
        } else {
            echo '{ "alert": "error", "message": "Ha ocurrido un error desconocido intentelo más tarde." }';
        }

        # si hay algun archivo que subir
        if ($file["name"]) {
            # si es un formato de imagen o pdf
            if ($file["type"] == "image/jpeg" || $file["type"] == "image/pjpeg" || $file["type"] == "image/gif" || $file["type"] == "image/png" || $file["type"] == "application/pdf" || $file["type"] == "csv" || $file["type"] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || $file["type"] == "application/vnd.ms-excel") {
                # si exsite la carpeta o se ha creado
                if (file_exists($carpetaDestino) || @mkdir($carpetaDestino, 0777, true)) {
                    $origen = $file["tmp_name"];
                    $destino = $carpetaDestino . $file["name"];
                    # movemos el archivo
                    if (@move_uploaded_file($origen, $destino)) {
                        //echo '{ "alert": "successCatalogo", "message": "El catálogo se subió correctamente." }';
                    } else {
                        echo '{ "alert": "error", "message": "No se ha podido mover el archivo: ".$file["archivo"]["name"]."" }';
                    }
                } else {
                    echo '{ "alert": "error", "message": "No se ha podido crear la carpeta en: $carpetaDestino" }';
                }
            } else {
                echo '{ "alert": "error", "message": "".$file["archivo"]["name"]." - NO es un archivo válido" }';
            }
        } else {
            echo '{ "alert": "error", "message": "No se ha subido ningún archivo" }';
        }
    }

    public function campana($action) {
        if ($action == 'add') {
            $this->data['content'] = $this->twig->render('admin/campaign_add.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl']));
            $this->twig->display('index.php', $this->data);
        } else if ($action == 'resume') {
            $this->data['content'] = $this->twig->render('admin/campaign_resume.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl']));
            $this->twig->display('index.php', $this->data);
        } else if ($action == 'single') {
            $this->data['content'] = $this->twig->render('admin/campaign_single.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl']));
            $this->twig->display('index.php', $this->data);
        }
    }
    
    public function usuarios($action=false, $cod_user=false){
        if($action=='add'){
            $comerciales = $this->Users_m->getComerciales();
            $this->data['content'] = $this->twig->render('admin/user_single.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl'], 'comerciales' => $comerciales));
            $this->twig->display('index.php', $this->data);
            
        } else if($action == 'edit'){
            $comerciales = $this->Users_m->getComerciales();
            $user = $this->Users_m-> get_by_cod_user($cod_user);
            $this->data['content'] = $this->twig->render('admin/user_single.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl'], 'userData' => $this->data['userlogged'], 'user' => $user, 'comerciales' => $comerciales, 'post'=>$_POST));
            $this->twig->display('index.php', $this->data);
             
        } else{
            $users = $this->Users_m->get();
            $this->data['content'] = $this->twig->render('admin/user_list.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl'], 'userData' => $this->data['userlogged'], 'users' => $users));
            $this->twig->display('index.php', $this->data);
            
        }
    }

    public function reCalcularPuntos() {
        $this->puntos->calcularPuntos();
    }

    public function facturacion() {
        $this->data['content'] = $this->twig->render('admin/facturacion_admin.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl']));
        $this->twig->display('index.php', $this->data);
    }

    public function pedidos() {
        $pedidos = $this->Pedidos_m->getPedidos();
        if($pedidos){
            $i = 0;
            foreach ($pedidos as $p) {
                $pedidos[$i]['pedido'] = json_decode($p['pedido'], true);
                $pedidos[$i]['datosEnvio'] = json_decode($p['datosEnvio'], true);
                $i++;
            }
            $i = 0;
            foreach ($pedidos as $p) {
                $p['idpedidos'] = str_pad($p['idpedidos'], 4, "0", STR_PAD_LEFT);
                foreach ($p['pedido'] as $k => $l) {
                    if (isset($l['options']['size'])) {
                        foreach ($l['options']['size'] as $linea) {
                            $o[] = json_decode($linea);
                        }
                        $pedidos[$i]['pedido'][$k]['options']['size'] = $o;
                    }
                }
                $i++;
            }
        }
        $this->data['content'] = $this->twig->render('admin/pedidos_admin.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl'], 'pedidos' => $pedidos, 'userData' => $this->data['userlogged']));
        $this->twig->display('index.php', $this->data);
    }

    public function pedidos_2017() {
        $pedidos = $this->Pedidos_rally_m->getPedidos();
        if($pedidos){
            $i = 0;
            foreach ($pedidos as $p) {
                $pedidos[$i]['pedido'] = json_decode($p['pedido'], true);
                $i++;
            }
        }
        $this->data['content'] = $this->twig->render('admin/pedidos_admin_rally.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl'], 'pedidos' => $pedidos, 'userData' => $this->data['userlogged']));
        $this->twig->display('index.php', $this->data);
    }

    public function estadisticas($cod_user = false) {
        ini_set('memory_limit', '1024M');
        if ($cod_user != false) {
            $puntosTotales = $this->PuntosTotales_m->getUser_bycodUser($cod_user);
            $puntosTotales = $this->groupArray($puntosTotales, 'cod_user');
            $formacion = $this->FormacionUser_m->getallForms_bycodUser($cod_user);
            $formacion = $this->groupArray($formacion, 'cod_user');
            $facturacion = $this->Facturacion_m->get_by_cod_user($cod_user);
            $facturacionGroup = $this->groupArray($facturacion, 'cod_user');
            $firstPie = $this->Facturacion_m->getFirstPie($cod_user);
            $firstPie = $this->groupArray($firstPie, 'archivo');
            $secondPie = $this->Facturacion_m->getSecondPie($cod_user);
            $secondPie = $this->groupArray($secondPie, 'llanta');
            $thirdPie = $this->Facturacion_m->getThirdPie($cod_user);
            $thirdPie = $this->groupArray($thirdPie, 'marca');
            $color = array(
                '0' => '#F38630',
                '1' => '#333',
                '2' => '#EEE',
                '3' => '#8c8c8c',
                '4' => '#ffa500',
                '5' => '#ffedbe',
                '6' => '#d8d2c7',
                '7' => '#373e47',
                '8' => '#DEB17B',
                '9' => '#E84E1C',
                '10' => '#aeaeae'
            );
            $factForUser = array();
            foreach ($facturacionGroup as $k => $f) {
                $factbyFile[$k] = $this->groupArray($f, 'archivo');
                if (isset($puntosTotales[$k])) {
                    $factForUser[$k]['facturacion'] = $puntosTotales[$k];
                }
                if (isset($formacion[$k])) {
                    $factForUser[$k]['Formacion'] = $formacion[$k];
                } else {
                    $factForUser[$k]['Formacion'] = null;
                }
                if (isset($factbyFile[$k]['Recambios'])) {
                    $factForUser[$k]['Recambios'] = $recambios = $this->calcularRecambios($factbyFile[$k]['Recambios']);
                } else {
                    $factForUser[$k]['Recambios'] = null;
                }
            }
            $factForUser[$cod_user]['Facturacion'] = $facturacion;
            $this->data['content'] = $this->twig->render('taller/estadisticas_taller.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl'], 'facturacion' => $factForUser
                ,'firstPie' => $firstPie, 'secondPie' => $secondPie, 'thirdPie' => $thirdPie, 'color' => $color));
            $this->twig->display('index.php', $this->data);
        } else {
            $puntosTotales = $this->PuntosTotales_m->getUsers();
            $puntosTotales = $this->groupArray($puntosTotales, 'cod_user');
            $formacion = $this->FormacionUser_m->getallForms();
            $formacion = $this->groupArray($formacion, 'cod_user');
            $facturacion = $this->Facturacion_m->get_all_bestdrive();
            $factMeses = $this->facturacionPorMeses($facturacion, 'nueva');
            $factMeses['2018'] = $this->groupArray($factMeses['2018'], 'fecha');
            foreach ($factMeses['2018'] as $k => $f) {
                $sumaMes = 0;
                foreach ($f as $sum) {
                    $sumaMes += $sum[0];
                }
                $factMeses['2018'][$k] = $sumaMes;
            }
            $facturacion = $this->groupArray($facturacion, 'cod_user');
            $factForAdmin = array();
            foreach ($facturacion as $k => $f) {
                $factbyFile[$k] = $this->groupArray($f, 'archivo');
                if (isset($puntosTotales[$k])) {
                    $factForAdmin[$k]['puntosTotales'] = $puntosTotales[$k];
                }
                if (isset($formacion[$k])) {
                    $factForAdmin[$k]['Formacion'] = $formacion[$k];
                } else {
                    $factForAdmin[$k]['Formacion'] = null;
                }
                if (isset($factbyFile[$k]['Recambios'])) {
                    $factForAdmin[$k]['Recambios'] = $recambios = $this->calcularRecambios($factbyFile[$k]['Recambios']);
                } else {
                    $factForAdmin[$k]['Recambios'] = null;
                }
            }
            $facturacionRally = $this->Facturacion_rally_m->get_all_bestdrive();
            $factMesesRally = $this->facturacionPorMeses($facturacionRally, 'rally');
            $factMesesRally['2017'] = $this->groupArray($factMesesRally['2017'], 'fecha');
            $factMesesRally['2016'] = $this->groupArray($factMesesRally['2016'], 'fecha');
            foreach ($factMesesRally['2016'] as $k => $f) {
                $sumaMes = 0;
                foreach ($f as $sum) {
                    $sumaMes += $sum[0];
                }
                $factMeses['2016'][$k] = $sumaMes;
            }
            foreach ($factMesesRally['2017'] as $k => $f) {
                $sumaMes = 0;
                foreach ($f as $sum) {
                    $sumaMes += $sum[0];
                }
                $factMeses['2017'][$k] = $sumaMes;
            }
            $consumo = $this->Pedidos_rally_m->getPedidosEst();
            $consumo = $this->facturacionPorMeses($consumo, 'pedidos');
            $consumo['2018'] = $this->groupArray($consumo['2018'], 'fecha');
            $consumo['2017'] = $this->groupArray($consumo['2017'], 'fecha');
            $consumo['2016'] = $this->groupArray($consumo['2016'], 'fecha');
            foreach ($consumo['2016'] as $k => $f) {
                $sumaMes = 0;
                foreach ($f as $sum) {
                    $sumaMes += $sum[0];
                }
                $consumo['2016'][$k] = $sumaMes*0.05;
            }
            foreach ($consumo['2017'] as $k => $f) {
                $sumaMes = 0;
                foreach ($f as $sum) {
                    $sumaMes += $sum[0];
                }
                $consumo['2017'][$k] = $sumaMes*0.05;
            }
            foreach ($consumo['2018'] as $k => $f) {
                $sumaMes = 0;
                foreach ($f as $sum) {
                    $sumaMes += $sum[0];
                }
                $consumo['2018'][$k] = $sumaMes*0.04;
            }
            $comerciales = $this->Users_m->getComerciales();
            $this->data['content'] = $this->twig->render('admin/estadisticas_admin.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl'], 'facturacion' => $facturacion, 'comerciales' => $comerciales, 
                'factForAdmin' => $factForAdmin, 'factMeses' => $factMeses, 'consumo' => $consumo, 'userData' => $this->data['userlogged']));
            $this->twig->display('index.php', $this->data);
        }
    }

    public function formacion($action = false) {
        $pastForms = array();
        $proxForms = array();
        $formaciones = $this->Formacion_m->get();
        $fecha_actual = strtotime(date("Y-m-d", time()));
        if ($formaciones != null) {
            foreach ($formaciones as $form) {
                $fechaprom = strtotime($form->fecha);
                if ($fecha_actual > $fechaprom) {
                    $pastForms[] = $form;
                } else {
                    $proxForms[] = $form;
                }
            }
        }
        if ($action == 'anadir') {
            $this->data['content'] = $this->twig->render('admin/formacion_add.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl'], 'proxForms' => $proxForms, 'pastForms' => $pastForms));
            $this->twig->display('index.php', $this->data);
        } else if ($action == 'subir') {
            $this->data['content'] = $this->twig->render('admin/formacion_upload.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl'], 'proxForms' => $proxForms, 'pastForms' => $pastForms));
            $this->twig->display('index.php', $this->data);
        } else {
            $this->data['content'] = $this->twig->render('admin/formacion_admin.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl'], 'proxForms' => $proxForms, 'pastForms' => $pastForms));
            $this->twig->display('index.php', $this->data);
        }
    }

    public function verFormaciones($id) {
        $formacion = $this->Formacion_m->get($id);
        $users = $this->FormacionUser_m->getall($id);
        $this->data['content'] = $this->twig->render('formacion.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl'], 'users' => $users, 'formacion' => $formacion));
        $this->twig->display('index.php', $this->data);
    }

    public function factMaestra() {
        $config['upload_path'] = 'uploads/';
        $config['allowed_types'] = 'xlsx';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('factMaestra')) {
            $error = strip_tags($this->upload->display_errors());
            echo '{ "alert": "error", "message": "' . $error . '" }';
        } else {
            $archivo = $this->upload->data();
            $this->load->library('Excel');
            $facturacionMaestro = $this->excel->get_dataMaestro($archivo['full_path']);
            ini_set('max_execution_time', 600);
            set_time_limit(0);
            $this->config->load('excel');
            $config = $this->config->item('excel');
            $keys_file = array_keys($facturacionMaestro[0]);
            foreach ($config['diferenciacion'] as $k => $v) {
                if (in_array($k, $keys_file)) {
                    $tipo = $v;
                }
            }
            $config = $config[$tipo];
            $facturacion_final = array();
            $columnas = count($facturacionMaestro[0]);
            foreach ($facturacionMaestro as $f) {
                $data = array();

                if (count($f) == $columnas) {
                    foreach ($config as $kc => $c) {
                        if ($kc == 'cod_user') {
                            $data[$kc] = (int) preg_replace("/[^0-9,.]/", "", $f[$c]);
                            $data['codDestino'] = (int) preg_replace("/[^0-9,.]/", "", $f[$c]);
                        } else if ($kc == 'archivo') {
                            if ($f[$c] == 'Material Handling' || $f[$c] == 'Earthmoving' || $f[$c] == 'Agricultural') {
                                $data[$kc] = 'Industria';
                                $item = array(
                                    "tipo" => 'Industria',
                                    "cod_user" => $data['cod_user'],
                                    "obtenidos" => $data['total'] * 0.01
                                );
                            } else if ($f[$c] == 'Passenger and Light') {
                                $data[$kc] = $f[$c];
                                if ($data['marca'] == 'CONTINENTAL') {
                                    $marca = $data['marca'];
                                    if ($data['llanta'] < 18) {
                                        $obtenidos = $data['unidades'] * 7;
                                        $prest = 'STD';
                                    } else {
                                        $obtenidos = $data['unidades'] * 8;
                                        $prest = 'UHP';
                                    }
                                } else if ($data['marca'] == 'BARUM') {
                                    $marca = $data['marca'];
                                    $obtenidos = $data['unidades'] * 2;
                                }
                                $item = array(
                                    "tipo" => $f[$c],
                                    "cod_user" => $data['cod_user'],
                                    "obtenidos" => $obtenidos,
                                    "marca" => $marca,
                                    "prestacion" => $prest
                                );
                            } else if ($f[$c] == 'Truck Tires') {
                                $data[$kc] = $f[$c];
                                if ($data['marca'] == 'CONTINENTAL') {
                                    $item = array(
                                        "tipo" => $f[$c],
                                        "cod_user" => $data['cod_user'],
                                        "obtenidos" => $data['unidades'] * 7
                                    );
                                } else {
                                    $item = array(
                                        "tipo" => $f[$c],
                                        "cod_user" => $data['cod_user'],
                                        "obtenidos" => 0
                                    );
                                }
                            } else if ($f[$c] == '2Wheel') {
                                $data[$kc] = 'Moto';
                                $item = array(
                                    "tipo" => 'Moto',
                                    "cod_user" => $data['cod_user'],
                                    "obtenidos" => $data['unidades'] * 1
                                );
                            }
                        } else if ($kc == 'importe') {
                            $data[$kc] = round($f[$c], 2);
                        } else {
                            $data[$kc] = $f[$c];
                        }
                    }
                    $data['kilometros'] = $item['obtenidos'];
                    if ($data['unidades'] != '') {
                        $user = $this->Users_m->get_by_nomClave($data['nomCliente']);
                        if ($user) {
                            $data['cod_user'] = $user->cod_user;
                            $data['cif'] = $user->cif;
                        }
                        if (!$this->Facturacion_m->check_data($data)) {
                            $this->Facturacion_m->upsert($data);
                            $facturacion_final[] = $data;
                        }
                    }
                }
            }
            $this->load->library('Puntos');
            $this->Puntos->calcularPuntos();
            echo '{ "alert": "success", "message": "Archivo subido correctamente" }';
        }
    }

    public function generarContrasenas() {
        $users = $this->Users_m->get_users();
        foreach ($users as $u) {
            $part = explode(" ", $u->razon_social);
            if (strlen($part[0]) < 4) {
                if (strlen($part[0] . $part[1]) <= 4) {
                    $sort = ucfirst(strtolower(preg_replace('/[¿!¡;,:&\.\?#@()"]/', '', $part[0] . $part[1] . $part[2])));
                } else {
                    $sort = ucfirst(strtolower(preg_replace('/[¿!¡;,:&\.\?#@()"]/', '', $part[0] . $part[1])));
                }
            } else {
                $sort = ucfirst(strtolower(preg_replace('/[¿!¡;,:&\.\?#@()"]/', '', $part[0])));
            }
            $item = new stdClass();
            $item->pass = md5($sort . '1234');
            $this->Users_m->upsert($item, $u->iduser);
            print_r($u->email_gerente);
            echo " usuario: ";
            print_r($u->cif);
            echo " contraseña: ";
            print_r($sort . '1234');
            echo " nombre_comercial: ";
            print_r('"' . $u->razon_social . '"');
            echo " nombre_gerente: ";
            print_r('"' . $u->nombre_gerente . '"');
            echo " telefono: ";
            print_r($u->telefono);
            echo " direccion: ";
            print_r('"' . $u->direccion . ',' . $u->poblacion . ',' . $u->provincia . ',' . $u->cp . '"');
            echo '<br />';
        }
    }

    public function fact() {
        $config['upload_path'] = 'uploads/';
        $config['allowed_types'] = 'xlsx';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('fact')) {
            $error = strip_tags($this->upload->display_errors());
            echo '{ "alert": "error", "message": "' . $error . '" }';
        } else {
            $archivo = $this->upload->data();
            $this->load->library('Excel');
            $facturacion = $this->excel->get_data($archivo['full_path']);

            ini_set('max_execution_time', 300);
            set_time_limit(0);
            $this->config->load('excel');
            $config = $this->config->item('excel');

            $keys_file = array_keys($facturacion[0]);
            foreach ($config['diferenciacion'] as $k => $v) {
                if (in_array($k, $keys_file)) {
                    $tipo = $v;
                }
            }
            $config = $config[$tipo];
            $facturacion_final = array();
            $columnas = count($facturacion[0]);
            foreach ($facturacion as $f) {
                $data = array();

                if (count($f) == $columnas) {
                    foreach ($config as $kc => $c) {
                        if ($kc == 'cod_user') {
                            $data[$kc] = (int) preg_replace("/[^0-9,.]/", "", $f[$c]);
                            $data['codDestino'] = (int) preg_replace("/[^0-9,.]/", "", $f[$c]);
                        } else if ($kc == 'archivo') {
                            $data[$kc] = $c;
                            if ($c == 'Recambios') {
                                $item = array(
                                    "tipo" => $data['archivo'],
                                    "obtenidos" => ($data['total'] * 0.05)
                                );
                            }
                        } else if ($kc == 'importe') {
                            $data[$kc] = round($f[$c], 2);
                        } else if ($kc == 'fecha' && count(explode('-', $f[$c]))) {
                            $data[$kc] = $f[$c];
                        } else {
                            $data[$kc] = $f[$c];
                        }
                    }
                    $data['kilometros'] = $item['obtenidos'];
                    if (substr($data['cif'], 0, 2) == 'ES') {
                        $data['cif'] = substr($data['cif'], 2);
                    }
                    $user = $this->Users_m->get_by_cif($data['cif']);
                    if ($user) {
                        $data['cod_user'] = $user->cod_user;
                        $data['codDestino'] = $user->cod_user;
                    }
                    if (!$this->Facturacion_m->check_data($data)) {
                        $this->Facturacion_m->upsert($data);
                        $facturacion_final[] = $data;
                    }
                }
            }
            $this->load->library('Puntos');
            $this->Puntos->calcularPuntos();
            echo '{ "alert": "success", "message": "Archivo subido correctamente" }';
        }
    }

    public function cruce() {
        $config['upload_path'] = 'uploads/';
        $config['allowed_types'] = 'xlsx';
        $this->load->library('upload', $config);
        ini_set('memory_limit', '1024M');
        if (!$this->upload->do_upload('cruce')) {
            $error = strip_tags($this->upload->display_errors());
            echo '{ "alert": "error", "message": "' . $error . '" }';
        } else {
            $archivo = $this->upload->data();
            $this->load->library('Excel');
            set_time_limit(0);
            $ficheroCruce = $this->excel->get_data($archivo['full_path']);
            $this->config->load('excel');
            $config = $this->config->item('excel');
            $keys_file = array_keys($ficheroCruce[0]);
            foreach ($config['diferenciacion'] as $k => $v) {
                if (in_array($k, $keys_file)) {
                    $tipo = $v;
                }
            }
            $config = $config[$tipo];
            $fichero_final = array();
            $columnas = count($ficheroCruce[0]);
            foreach ($ficheroCruce as $f) {
                $data = array();

                if (count($f) == $columnas) {
                    foreach ($config as $kc => $c) {
                        $data[$kc] = $f[$c];
                    }
                    if (!$this->Cruce_m->check_data($data)) {
                        $this->Cruce_m->upsert($data);
                    }
                    $fichero_final[] = $data;
                }
            }
            echo '{ "alert": "success", "message": "Archivo subido correctamente" }';
        }
    }

    public function uploadForm() {
        $config['upload_path'] = 'uploads/';
        $config['allowed_types'] = 'xlsx';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('formExcel')) {
            $error = strip_tags($this->upload->display_errors());
            echo '{ "alert": "error", "message": "' . $error . '" }';
        } else {
            $idformacion = $_POST['idformacion'];
            $archivo = $this->upload->data();
            $this->load->library('Excel');
            $formaciones = $this->excel->get_data($archivo['full_path']);
            ini_set('max_execution_time', 300);
            set_time_limit(0);
            $this->config->load('excel');
            $config = $this->config->item('excel');

            $keys_file = array_keys($formaciones[0]);
            foreach ($config['diferenciacion'] as $k => $v) {
                if (in_array($k, $keys_file)) {
                    $tipo = $v;
                }
            }
            $config = $config[$tipo];
            $facturacion_final = array();
            $columnas = count($formaciones[0]);
            foreach ($formaciones as $f) {
                $data = array();
                if (count($f) == $columnas) {
                    foreach ($config as $kc => $c) {
                        if ($f[$c] != 0) {
                            $item = new stdClass();
                            $item->idformacion = $idformacion;
                            $item->cod_user = (int) preg_replace("/[^0-9,.]/", "", $f[$c]);
                            $this->FormacionUser_m->upsert($item);
                        }
                    }
                }
            }
            echo '{ "alert": "success", "message": "La formación ha sido subida con éxito." }';
        }
    }

    private function groupArray($array, $groupkey) {
        if (count($array) > 0) {
            if (isset($array[0])) {
                $keys = array_keys($array[0]);
            } else {
                $tempArray = $array;
                $array = null;
                $array[0] = $tempArray;
                $keys = array_keys($array[0]);
            }
            $removekey = array_search($groupkey, $keys);
            if ($removekey === false) {
                return array("Clave \"$groupkey\" no existe");
            } else {
                unset($keys[$removekey]);
                $groupcriteria = array();
                $return = array();
                foreach ($array as $value) {
                    $item = null;
                    foreach ($keys as $key) {
                        $item[$key] = $value[$key];
                    }
                    $busca = array_search($value[$groupkey], $groupcriteria);
                    if ($busca === false) {
                        $groupcriteria[] = $value[$groupkey];
                        $return[] = array($value[$groupkey] => array());
                        $busca = count($return) - 1;
                    }
                    $return[$busca][$value[$groupkey]][] = $item;
                }
                $return = $this->keyArray($return);
                return $return;
            }
        } else {
            return array();
        }
    }

    private function keyArray($array) {
        foreach ($array as $k => $a) {
            $nameArray = key($a);
            $keyArray[$nameArray] = $a[$nameArray];
        }
        return $keyArray;
    }

    private function calcularRecambios($fac) {
        date_default_timezone_set('europe/madrid');
        $sumPrimSem2018 = 0;
        $sumSegSem2018 = 0;
        $sumPrimSem2019 = 0;
        $sumSegSem2019 = 0;
        $primSem2018 = strtotime("31-05-2018 23:59:59");
        $segSem2018 = strtotime("31-12-2018 23:59:59");
        $primSem2019 = strtotime("31-05-2019 23:59:59");
        $segSem2019 = strtotime("31-12-2019 23:59:59");
        foreach ($fac as $f) {
            if (strtotime($f['fecha']) < $primSem2018) {
                $sumPrimSem2018 = $sumPrimSem2018 + $f['kilometros'];
            } else if (strtotime($f['fecha']) < $segSem2018) {
                $sumSegSem2018 = $sumSegSem2018 + $f['kilometros'];
            } else if (strtotime($f['fecha']) < $primSem2019) {
                $sumPrimSem2019 = $sumPrimSem2019 + $f['kilometros'];
            } else if (strtotime($f['fecha']) < $segSem2019) {
                $sumSegSem2019 = $sumSegSem2019 + $f['kilometros'];
            }
        }
        return $recambios = array(
            'sumPrimSem2018' => $sumPrimSem2018,
            'sumSegSem2018' => $sumSegSem2018,
            'sumPrimSem2019' => $sumPrimSem2019,
            'sumSegSem2019' => $sumSegSem2019
        );
    }

    private function facturacionPorMeses($facturacion, $type) {
        if($type=='nueva'){
            $i = 0;
            foreach ($facturacion as $f) {
                if ($f['archivo'] == 'Recambios') {
                    $ano =  $this->returnAno($f['fecha']);
                    $factBymeses[$ano][$i]['fecha'] = $this->returnMes($f['fecha']);
                } else {
                    $fecha = $this->convertirFecha_SpanishToEnglish($f['fecha']);
                    $ano =  $this->returnAno($fecha);
                    $factBymeses[$ano][$i]['fecha'] = $this->returnMes($fecha);
                }
                $factBymeses[$ano][$i][] = $f['total'];
                $i++;
            }
            return $factBymeses;
        } else if ($type=='rally'){
            $factBymeses = array();
            $years = array();
            $i = 0;
            foreach ($facturacion as $f) {
                $fecha = $this->convertirFecha_SpanishToEnglish($f['fecha']);
                $ano =  $this->returnAno($fecha);
                if(!in_array($ano, $years)){
                    array_push($years, $ano);
                    $factBymeses[$ano][0]['fecha'] = $this->returnMes($fecha);
                    $factBymeses[$ano][0][] = $f['total'];
                } else{
                    $factBymeses[$ano][$i]['fecha'] = $this->returnMes($fecha);
                    $factBymeses[$ano][$i][] = $f['total'];
                }
                $i++;
            }
            return $factBymeses;
        } else if($type = 'pedidos'){
            $factBymeses = array();
            $years = array();
            $i = 0;
            foreach ($facturacion as $f) {
                $ano =  $this->returnAno($f['fecha']);
                if(!in_array($ano, $years)){
                    array_push($years, $ano);
                        $meses = array(
                            0 => 'Enero',
                            1 => 'Febrero',
                            2 => 'Marzo',
                            3 => 'Abril',
                            4 => 'Mayo',
                            5 => 'Junio',
                            6 => 'Julio',
                            7 => 'Agosto',
                            8 => 'Septiembre',
                            9 => 'Octubre',
                            10 => 'Noviembre',
                            11 => 'Diciembre',
                        );
                        $j = 0;
                        $mes = $this->returnMes($f['fecha']);
                        foreach($meses as $m){
                            $fechaFichero= $ano.'-'.($j+1);
                            if($m == $mes){
                                $factBymeses[$ano][$j]['fecha'] = $mes;
                                $factBymeses[$ano][$j][] = $f['kmtotal'];
                            } else if(strtotime($fechaFichero) > strtotime(date("Y-m",time()))){
                               //Los meses futuros no se almacenan a cero. 
                            }else{
                                $factBymeses[$ano][$j]['fecha'] = $m;
                                $factBymeses[$ano][$j][] = 0;
                            }
                            $j++;
                        }
                        $i+=$j;
                } else{
                    $factBymeses[$ano][$i]['fecha'] = $this->returnMes($f['fecha']);
                    $factBymeses[$ano][$i][] = $f['kmtotal'];
                    $i++;
                }
            }
            return $factBymeses;
        }
    }

    private function returnMes($fecha) {
        $mes = date('F', strtotime($fecha));
        $meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        $nombreMes = str_replace($meses_EN, $meses_ES, $mes);
        return $nombreMes;
    }

    private function returnAno($fecha) {
        $ano = date('Y', strtotime($fecha));
        return $ano;
    }

    private function convertirFecha_SpanishToEnglish($date) {
        if ($date) {
            $fecha = $date;
            $hora = "";

            # separamos la fecha recibida por el espacio de separación entre
            # la fecha y la hora
            $fechaHora = explode(" ", $date);
            if (count($fechaHora) == 2) {
                $fecha = $fechaHora[0];
                $hora = $fechaHora[1];
            }

            # cogemos los valores de la fecha
            $values = preg_split('/(\/|-)/', $fecha);
            if (count($values) == 3) {
                # devolvemos la fecha en formato ingles
                if ($hora && count(explode(":", $hora)) == 3) {
                    # si la hora esta separada por : y hay tres valores...
                    $hora = explode(":", $hora);
                    return date("Y/m/d H:i:s", mktime($hora[0], $hora[1], $hora[2], $values[1], $values[0], $values[2]));
                } else {
                    return date("Y/m/d", mktime(0, 0, 0, $values[1], $values[0], $values[2]));
                }
            }
        }
        return "";
    }
    
    public function perfil() {
        $user = $this->Users_m->get_by_iduser($this->data['userlogged']->iduser);
        $vista = 'admin/profile_admin_edit.php';
        $this->data['content'] = $this->twig->render($vista, array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'user' => $user));
        $this->twig->display('index.php', $this->data);
    }
    
    public function cambiarPass() {
        if ($_POST['new_pass'] != $_POST['repeat']) {
            echo '{ "alert": "error", "message": "Las contraseñas no coinciden." }';
        } elseif (!$this->Users_m->get_correct_pass($this->data['userlogged']->iduser, $_POST['pass'])) {
            echo '{ "alert": "error", "message": "Las contraseña anterior no es correcta." }';
        } else {
            $item = new stdClass();
            $item->pass = md5($_POST['new_pass']);
            $this->Users_m->upsert($item, $this->data['userlogged']->iduser);
            echo '{ "alert": "successPassAdmin", "message": "La contraseña ha sido cambiada, vuelva a loguearse por motivos de seguridad." }';
        }
    }
    
    
    public function upsertDatos($id=false) {
        if($id!=false){
            $item = new stdClass();
            $item->cif = $_POST['cif'];
            $item->nombreClave = $_POST['nombreClave'];
            $item->razon_social = $_POST['razonSocial'];
            $item->nombre_comercial = $_POST['nombreComercial'];
            $item->dni_comercial = $_POST['coordinador'];
            $item->cod_user = $_POST['cod_user'];
            $item->direccion = $_POST['direccion'];
            $item->poblacion = $_POST['poblacion'];
            $item->provincia = $_POST['provincia'];
            $item->cp = $_POST['cp'];
            $item->telefono = $_POST['phone'];
            $item->nombre_gerente = $_POST['name'];
            $item->email_gerente = $_POST['email'];
            $item->comentarios = $_POST['comentarios'];
            $this->Users_m->upsert($item, $id);
            echo '{ "alert": "successEdit", "message": "Usuario actualizado con éxito." }';
        } else{
            $item = new stdClass();
            $item->cif = $_POST['cif'];
            $item->nombreClave = $_POST['nombreClave'];
            $item->razon_social = $_POST['razonSocial'];
            $item->nombre_comercial = $_POST['nombreComercial'];
            $item->dni_comercial = $_POST['coordinador'];
            $item->cod_user = $_POST['cod_user'];
            $item->direccion = $_POST['direccion'];
            $item->poblacion = $_POST['poblacion'];
            $item->provincia = $_POST['provincia'];
            $item->cp = $_POST['cp'];
            $item->telefono = $_POST['phone'];
            $item->nombre_gerente = $_POST['name'];
            $item->email_gerente = $_POST['email'];
            $item->comentarios = $_POST['comentarios'];
            $this->Users_m->upsert($item);
            /**** FUTURO MAIL DE BIENVFENIDA AL PROGRAMA  ****/
            /*$this->load->library("PhpMailerLib");
            $mail = $this->phpmailerlib->load();
            //Server settings
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.bestdrive.plus';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'info@bestdrive.plus';                 // SMTP username
            $mail->Password = 'Rally@@2018';                           // SMTP password                               // TCP port to connect to
            //Recipients
            $mail->setFrom('info@bestdrive.plus');
            $mail->addAddress('contacto@bestdrive.plus');     // Add a recipient
            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = utf8_decode($this->data['userlogged']->razon_social.' modificó sus datos en Yokohama Bushido');

            $body = $this->twig->render('mails/modificacion.php', array('modificacion' => $_POST, 'user' => $this->data['userlogged']));
            $mail->Body = utf8_decode($body);
            $mail->send();
            echo '{ "alert": "successProfile", "message": "Sus datos han sido actualizados." }';*/
            echo '{ "alert": "successEdit", "message": "Usuario creado con éxito." }';
        }
    }
    
    public function bases() {
        $vista = 'bases.php';
        $this->data['content'] = $this->twig->render($vista, array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'user' => $this->session->userdata));
        $this->twig->display('index.php', $this->data);
    }
    
    
    public function actualizar() {
        set_time_limit(0);
        $cipher64 = $this->session->userdata('cipher64');
        $tipo = $this->session->userdata('tipo');
        if ($cipher64) {
            $cliente = $this->client->obtenerClientes($this->data['userlogged']->email, $cipher64['0'], $tipo, $cipher64['1']);
            //print_r($cliente);exit;
            $facturacion = $this->client->obtenerFacturacion($this->data['userlogged']->email, $cipher64['0'], $tipo, $cipher64['1']);
            $facturacionViaje = $this->client->obtenerFacturacion($this->data['userlogged']->email, $cipher64['0'], $tipo, $cipher64['1'], 'Y');
            $paramObtPed = array(
                'sFechaDesde' => '2019-01-01 00:00',
                'sFechaHasta' => '',
                'sBushido' => 'Y',
                'sYCN' => ''
            );
            $ventasReales = $this->client->obtenerVentasReales($this->data['userlogged']->email, $cipher64['0'], $tipo, $cipher64['1'], $paramObtPed);
            $sumas = $this->Sumas_m->get();
        }
        foreach ($cliente as $k => $v) {
            foreach ($v as $t) {
                if ($t['Bushido'] == 'Y') {
                    if ($t['email'] != array()) {
                        if (!$this->Users_m->get_by_coduser($t['CodigoIC'])) {
                            $item = new stdClass();
                            $item->contConex_Bushido = "0";
                            $item->contConex_B2B = "0";
                            $item->bases_Bushido = "0";
                            $item->privacidad_Bushido = "0";
                            $item->bases_B2B = "0";
                            $item->privacidad_B2B = "0";
                            $item->com_Bushido = "0";
                            $item->com_B2B = "0";
                            $item->nombre = $t['NombreIC'];
                            $item->email = $t['email'];
                            $item->tipo = 'C';
                            $item->ranking = $t['Ranking'];
                            $item->FechaHora_B2B = null;
                            $item->NIF = substr($t['NIF'], 2);
                            $item->cod_user = $t['CodigoIC'];
                            $item->nombreComercial = $t['NombreComercial'];
                            $direcciones = $t['Direccion'];
                            foreach ($direcciones as $k => $v) {
                                unset($v['CodigoIC']);
                                unset($v['TipoDir']);
                                unset($v['NombreDir']);
                                unset($v['Estado']);
                                unset($v['CodigoDir']);
                                unset($v['ParaOrden']);
                                $direcciones[$k] = $v;
                            }
                            $direcciones = json_encode($direcciones);
                            $item->direccion = $direcciones;
                            $item->telefono = $t['Telefono'];
                            $item->fax = $t['FAX'];
                            $item->nacionalidad = substr($t['NIF'], 0, 2);
                            $item->idioma = substr($t['NIF'], 0, 2);
                            $item->ycn = $t['YCN'];
                            $item->viaje = 0;
                            $item->ano_viaje = 2020;
                            $this->Users_m->upsert($item);
                        } else {
                            $id = $this->Users_m->get_by_coduser($t['CodigoIC']); 
                            $item = new stdClass();
                            $item->nombre = $t['NombreIC'];
                            $item->email = $t['email'];
                            $item->ranking = $t['Ranking'];
                            $item->nombreComercial = $t['NombreComercial'];
                            $direcciones = $t['Direccion'];
                            foreach ($direcciones as $k => $v) {
                                unset($v['CodigoIC']);
                                unset($v['TipoDir']);
                                unset($v['NombreDir']);
                                unset($v['Estado']);
                                unset($v['CodigoDir']);
                                unset($v['ParaOrden']);
                                $direcciones[$k] = $v;
                            }
                            $direcciones = json_encode($direcciones);
                            $item->direccion = $direcciones;
                            $item->telefono = $t['Telefono'];
                            $item->fax = $t['FAX'];
                            $this->Users_m->upsert($item, $id[0]->id);
                            
                        }
                    }
                }
            }
        }
        foreach ($facturacion['Facturado'] as $k => $v) {
            $user = $this->Users_m->get_by_coduser($v['CLIENTE']);
            if ($user) {
                $item = new stdClass();
                $item = $user[0];
                if ($v['EJERCICIO'] == '2018') {
                    $item->facturacionpv = $v['VALOR'];
                }
                if ($v['EJERCICIO'] == '2019') {
                    $item->facturacionact = $v['VALOR'];
                }
                $this->Users_m->upsert($item, $item->id);
            }
        }
        foreach ($facturacionViaje['Facturado'] as $k => $v) {
            $user = $this->Users_m->get_by_coduser($v['CLIENTE']);
            if ($user) {
                $item = new stdClass();
                $item = $user[0];
                if ($v['EJERCICIO'] == '2019') {
                    $item->facturacionViaje = $v['VALOR'];
                }
                $this->Users_m->upsert($item, $item->id);
            }
        }
        $CIC = $ventasReales['VentaReal'][0]['CodigoIC'];
        $user = $this->Users_m->get_by_coduser($CIC);
        $obtenidos = 0;
        foreach ($ventasReales as $vr) {
            foreach ($vr as $v) {
                if ($CIC != $v['CodigoIC']) {
                    if ($user != 'userFlag') {
                        $this->load->library('puntos');
                        $this->puntos->actualizarDB($CIC, $user[0]->ranking, $obtenidos);
                        $CIC = $v['CodigoIC'];
                        $user = $this->Users_m->get_by_coduser($CIC);
                        if ($user == null) {
                            $user = 'userFlag';
                        }
                        $obtenidos = 0;
                        foreach($sumas as $suma){
                            if(in_array($CIC, $suma)){
                                if($suma['cod_user']==$CIC){
                                    $obtenidos = $suma['kilometros'];
                                }
                            }
                        }
                    } else if ($user == 'userFlag') {
                        $CIC = $v['CodigoIC'];
                        $user = $this->Users_m->get_by_coduser($CIC);
                        if ($user == null) {
                            $user = 'userFlag';
                        }
                        $obtenidos = 0;
                        foreach($sumas as $suma){  
                            if(in_array($CIC, $suma)){
                                if($suma['cod_user']==$CIC){
                                    $obtenidos = $suma['kilometros'];
                                }
                            }
                        }
                    }
                }
                $obtenidos += $v['PuntosTotales'];
            }
        }
        print_r("listo!!");
    }

    
}
