<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Comercial extends CI_Controller {

    var $data;

    function __construct() {
        parent::__construct();
        if (!isset($this->session->userdata['logged_in'])) {
            redirect('inicio/login');
        } else if ($this->session->userdata['tipo'] != 'S') {
            redirect('inicio');
        }
        $this->load->library('Excel');
        $this->load->library('Puntos');
        $this->data = $this->template->get_data();
    }

    public function ajax($action = false) {
        
    }

    public function index() {
        $this->data['content'] = $this->twig->render('admin/home_admin.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl']));
        $this->twig->display('index.php', $this->data);
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('/');
    }

    public function pedidos() {
        $pedidos = $this->Pedidos_m->getPedidosComercial($this->data['userlogged']->cif);
        $i = 0;
        if($pedidos){
            foreach ($pedidos as $p) {
                $pedidos[$i]['pedido'] = json_decode($p['pedido'], true);
                $i++;
            }
        }
        $this->data['content'] = $this->twig->render('admin/pedidos_admin.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl'], 'pedidos' => $pedidos, 'userData' => $this->data['userlogged']));
        $this->twig->display('index.php', $this->data);
    }
    
    public function pedidos_2017() {
        $pedidos = $this->Pedidos_rally_m->getPedidosEstCom($this->data['userlogged']->cif);
        if($pedidos){
            $i = 0;
            foreach ($pedidos as $p) {
                $pedidos[$i]['pedido'] = json_decode($p['pedido'], true);
                $i++;
            }
        }
        $this->data['content'] = $this->twig->render('admin/pedidos_admin_rally.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl'], 'pedidos' => $pedidos, 'userData' => $this->data['userlogged']));
        $this->twig->display('index.php', $this->data);
    }

    public function estadisticas($cod_user = false) {
        if ($cod_user != false) {
            $user = $this->Users_m->get_by_cod_user($cod_user);
            if ($user == null) {
                redirect('error404', 'refresh');
            } else if ($user->dni_comercial != $this->data['userlogged']->cif) {
                redirect('error403', 'refresh');
            } else {
                $puntosTotales = $this->PuntosTotales_m->getUser_bycodUser($cod_user);
                $puntosTotales = $this->groupArray($puntosTotales, 'cod_user');
                $formacion = $this->FormacionUser_m->getallForms_bycodUser($cod_user);
                $formacion = $this->groupArray($formacion, 'cod_user');
                $facturacion = $this->Facturacion_m->get_by_cod_user($cod_user);
                $facturacionGroup = $this->groupArray($facturacion, 'cod_user');
                $firstPie = $this->Facturacion_m->getFirstPie($cod_user);
                $firstPie = $this->groupArray($firstPie, 'archivo');
                $secondPie = $this->Facturacion_m->getSecondPie($cod_user);
                $secondPie = $this->groupArray($secondPie, 'llanta');
                $thirdPie = $this->Facturacion_m->getThirdPie($cod_user);
                $thirdPie = $this->groupArray($thirdPie, 'marca');
                $color = array(
                    '0' => '#F38630',
                    '1' => '#333',
                    '2' => '#EEE',
                    '3' => '#8c8c8c',
                    '4' => '#ffa500',
                    '5' => '#ffedbe',
                    '6' => '#d8d2c7',
                    '7' => '#373e47',
                    '8' => '#DEB17B',
                    '9' => '#E84E1C',
                    '10' => '#aeaeae'
                );
                $factForUser = array();
                foreach ($facturacionGroup as $k => $f) {
                    $factbyFile[$k] = $this->groupArray($f, 'archivo');
                    if (isset($puntosTotales[$k])) {
                        $factForUser[$k]['facturacion'] = $puntosTotales[$k];
                    }
                    if (isset($formacion[$k])) {
                        $factForUser[$k]['Formacion'] = $formacion[$k];
                    } else {
                        $factForUser[$k]['Formacion'] = null;
                    }
                    if (isset($factbyFile[$k]['Recambios'])) {
                        $factForUser[$k]['Recambios'] = $recambios = $this->calcularRecambios($factbyFile[$k]['Recambios']);
                    } else {
                        $factForUser[$k]['Recambios'] = null;
                    }
                }
                $factForUser[$cod_user]['Facturacion'] = $facturacion;
                $this->data['content'] = $this->twig->render('taller/estadisticas_taller.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl'], 'facturacion' => $factForUser
                    , 'firstPie' => $firstPie, 'secondPie' => $secondPie, 'thirdPie' => $thirdPie, 'color' => $color));
                $this->twig->display('index.php', $this->data);
            }
        } else {
            $usuarios = $this->PuntosTotales_m->getByComercial($this->data['userlogged']->nombreComercial);
            $facturacion = Array(
                '2018' => 0,
                '2019' => 0
            );
            foreach($usuarios as $u){
                $facturacion['2018'] += $u['facturacionpv'];
                $facturacion['2019'] += $u['facturacionact'];                
            }
            $usuarios = $this->groupArray($usuarios, 'cod_user');
            $this->data['content'] = $this->twig->render('admin/estadisticas_admin.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl'], 'facturacion' => $usuarios,
            'userData' => $this->data['userlogged'], ));
            $this->twig->display('index.php', $this->data);
        }
    }

    private function groupArray($array, $groupkey) {
        if (count($array) > 0) {
            if (isset($array[0])) {
                $keys = array_keys($array[0]);
            } else {
                $tempArray = $array;
                $array = null;
                $array[0] = $tempArray;
                $keys = array_keys($array[0]);
            }
            $removekey = array_search($groupkey, $keys);
            if ($removekey === false) {
                return array("Clave \"$groupkey\" no existe");
            } else {
                unset($keys[$removekey]);
                $groupcriteria = array();
                $return = array();
                foreach ($array as $value) {
                    $item = null;
                    foreach ($keys as $key) {
                        $item[$key] = $value[$key];
                    }
                    $busca = array_search($value[$groupkey], $groupcriteria);
                    if ($busca === false) {
                        $groupcriteria[] = $value[$groupkey];
                        $return[] = array($value[$groupkey] => array());
                        $busca = count($return) - 1;
                    }
                    $return[$busca][$value[$groupkey]][] = $item;
                }
                $return = $this->keyArray($return);
                return $return;
            }
        } else {
            return array();
        }
    }

    private function keyArray($array) {
        foreach ($array as $k => $a) {
            $nameArray = key($a);
            $keyArray[$nameArray] = $a[$nameArray];
        }
        return $keyArray;
    }

    private function calcularRecambios($fac) {
        date_default_timezone_set('europe/madrid');
        $sumPrimSem2018 = 0;
        $sumSegSem2018 = 0;
        $sumPrimSem2019 = 0;
        $sumSegSem2019 = 0;
        $primSem2018 = strtotime("31-05-2018 23:59:59");
        $segSem2018 = strtotime("31-12-2018 23:59:59");
        $primSem2019 = strtotime("31-05-2019 23:59:59");
        $segSem2019 = strtotime("31-12-2019 23:59:59");
        foreach ($fac as $f) {
            if (strtotime($f['fecha']) < $primSem2018) {
                $sumPrimSem2018 = $sumPrimSem2018 + $f['kilometros'];
            } else if (strtotime($f['fecha']) < $segSem2018) {
                $sumSegSem2018 = $sumSegSem2018 + $f['kilometros'];
            } else if (strtotime($f['fecha']) < $primSem2019) {
                $sumPrimSem2019 = $sumPrimSem2019 + $f['kilometros'];
            } else if (strtotime($f['fecha']) < $segSem2019) {
                $sumSegSem2019 = $sumSegSem2019 + $f['kilometros'];
            }
        }
        return $recambios = array(
            'sumPrimSem2018' => $sumPrimSem2018,
            'sumSegSem2018' => $sumSegSem2018,
            'sumPrimSem2019' => $sumPrimSem2019,
            'sumSegSem2019' => $sumSegSem2019
        );
    }


    private function facturacionPorMeses($facturacion, $type) {
        if($type=='nueva'){
            $factBymeses = array();
            $i = 0;
            foreach ($facturacion as $f) {
                if ($f['archivo'] == 'Recambios') {
                    $ano =  $this->returnAno($f['fecha']);
                    $factBymeses[$ano][$i]['fecha'] = $this->returnMes($f['fecha']);
                } else {
                    $fecha = $this->convertirFecha_SpanishToEnglish($f['fecha']);
                    $ano =  $this->returnAno($fecha);
                    $factBymeses[$ano][$i]['fecha'] = $this->returnMes($fecha);
                }
                $factBymeses[$ano][$i][] = $f['total'];
                $i++;
            }
            return $factBymeses;
        } else if ($type=='rally'){
            $factBymeses = array();
            $years = array();
            $i = 0;
            foreach ($facturacion as $f) {
                $fecha = $this->convertirFecha_SpanishToEnglish($f['fecha']);
                $ano =  $this->returnAno($fecha);
                if(!in_array($ano, $years)){
                    array_push($years, $ano);
                    $factBymeses[$ano][0]['fecha'] = $this->returnMes($fecha);
                    $factBymeses[$ano][0][] = $f['total'];
                } else{
                    $factBymeses[$ano][$i]['fecha'] = $this->returnMes($fecha);
                    $factBymeses[$ano][$i][] = $f['total'];
                }
                $i++;
            }
            return $factBymeses;
        }else if($type = 'pedidos'){
            $factBymeses = array();
            $years = array();
            $i = 0;
            foreach ($facturacion as $f) {
                $ano =  $this->returnAno($f['fecha']);
                if(!in_array($ano, $years)){
                    array_push($years, $ano);
                        $meses = array(
                            0 => 'Enero',
                            1 => 'Febrero',
                            2 => 'Marzo',
                            3 => 'Abril',
                            4 => 'Mayo',
                            5 => 'Junio',
                            6 => 'Julio',
                            7 => 'Agosto',
                            8 => 'Septiembre',
                            9 => 'Octubre',
                            10 => 'Noviembre',
                            11 => 'Diciembre',
                        );
                        $j = 0;
                        $mes = $this->returnMes($f['fecha']);
                        foreach($meses as $m){
                            $fechaFichero= $ano.'-'.($j+1);
                            if($m == $mes){
                                $factBymeses[$ano][$j]['fecha'] = $mes;
                                $factBymeses[$ano][$j][] = $f['kmtotal'];
                            } else if(strtotime($fechaFichero) > strtotime(date("Y-m",time()))){
                               //Los meses futuros no se almacenan a cero. 
                            }else{
                                $factBymeses[$ano][$j]['fecha'] = $m;
                                $factBymeses[$ano][$j][] = 0;
                            }
                            $j++;
                        }
                        $i+=$j;
              } else{
                    $factBymeses[$ano][$i]['fecha'] = $this->returnMes($f['fecha']);
                    $factBymeses[$ano][$i][] = $f['kmtotal'];
                    $i++;
                }
            }
            return $factBymeses;
        }
    }

    private function returnMes($fecha) {
        $mes = date('F', strtotime($fecha));
        $meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        $nombreMes = str_replace($meses_EN, $meses_ES, $mes);
        return $nombreMes;
    }

    private function returnAno($fecha) {
        $ano = date('Y', strtotime($fecha));
        return $ano;
    }

    private function convertirFecha_SpanishToEnglish($date) {
        if ($date) {
            $fecha = $date;
            $hora = "";

            # separamos la fecha recibida por el espacio de separación entre
            # la fecha y la hora
            $fechaHora = explode(" ", $date);
            if (count($fechaHora) == 2) {
                $fecha = $fechaHora[0];
                $hora = $fechaHora[1];
            }

            # cogemos los valores de la fecha
            $values = preg_split('/(\/|-)/', $fecha);
            if (count($values) == 3) {
                # devolvemos la fecha en formato ingles
                if ($hora && count(explode(":", $hora)) == 3) {
                    # si la hora esta separada por : y hay tres valores...
                    $hora = explode(":", $hora);
                    return date("Y/m/d H:i:s", mktime($hora[0], $hora[1], $hora[2], $values[1], $values[0], $values[2]));
                } else {
                    return date("Y/m/d", mktime(0, 0, 0, $values[1], $values[0], $values[2]));
                }
            }
        }
        return "";
    }

    public function perfil() {
        $user = $this->Users_m->get_by_iduser($this->data['userlogged']->iduser);
        $vista = 'admin/profile_admin_edit.php';
        $this->data['content'] = $this->twig->render($vista, array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'user' => $user));
        $this->twig->display('index.php', $this->data);
    }

    public function cambiarPass() {
        if ($_POST['new_pass'] != $_POST['repeat']) {
            echo '{ "alert": "error", "message": "Las contraseñas no coinciden." }';
        } elseif (!$this->Users_m->get_correct_pass($this->data['userlogged']->iduser, $_POST['pass'])) {
            echo '{ "alert": "error", "message": "Las contraseña anterior no es correcta." }';
        } else {
            $item = new stdClass();
            $item->pass = md5($_POST['new_pass']);
            $this->Users_m->upsert($item, $this->data['userlogged']->iduser);
            echo '{ "alert": "successPassComercial", "message": "La contraseña ha sido cambiada, vuelva a loguearse por motivos de seguridad." }';
        }
    }
    
    public function bases() {
        $vista = 'bases.php';
        $this->data['content'] = $this->twig->render($vista, array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'user' => $this->session->userdata));
        $this->twig->display('index.php', $this->data);
    }


}
