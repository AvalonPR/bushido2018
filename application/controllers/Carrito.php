<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Carrito extends CI_Controller {

    var $data;

    function __construct() {
        parent::__construct();
        if (!isset($this->session->userdata['logged_in'])) {
            redirect('inicio/login');
        } elseif ($this->session->userdata['tipo'] === 'A') {
            redirect('admin');
        } elseif ($this->session->userdata['tipo'] === 'S') {
            redirect('admin');
        }elseif ($this->session->userdata['bases_Bushido'] === '0') {
            redirect('inicio/bases');
        } elseif (is_null($this->session->userdata['viaje'])) {
                redirect('inicio/viaje', 'refresh');
        }

        $this->data = $this->template->get_data();
    }

    public function ajax($action) {
        if ($action == 'insert') {
            $producto = $this->catalogo->get_producto_by_id($_POST['id']);
            if($this->session->userdata['site_lang']!='spanish'){
                $producto['nombre'] = $producto['nombrePT'];
                if(isset($producto['precioDto'])){
                    $producto['precio'] = $producto['precioPTDto'];
                } else {
                    $producto['precio'] = $producto['precioPT'];
                }
                $producto['descripcion'] = $producto['descripcionPT'];
            } else{
                if(isset($producto['precioDto'])){
                    $producto['precio'] = $producto['precioDto'];
                }
            }
            if((round($producto['precio']*$_POST['qty'])+$this->cart_bushido->total())>$this->session->userdata['puntos']){
                $respuesta = array(
                    "error" => "1",
                    "carrito" => '"Parece que estas intentando introducir en el carrito más puntos de los que tienes actualmente."'
                );
            } else{
                $arr = null;
                if (isset($_POST['arr'])) {
                    $sumArr = 0;
                    $arr = $_POST['arr'];
                    foreach($arr as $a){
                        $a = json_decode($a);
                        $sumArr += $a[1];
                    }
                    if(($producto['unidades']*$_POST['qty'])>$sumArr){
                        $respuesta = array(
                            "error" => "1",
                            "toast" => 'No has introducido las tallas suficientes.'
                        );
                        print_r(json_encode($respuesta, true));exit;
                    } else if(($producto['unidades']*$_POST['qty'])<$sumArr) {
                        $respuesta = array(
                            "error" => "1",
                            "toast" => 'Has introducido más tallas de las que tiene el pack.'
                        );
                        print_r(json_encode($respuesta, true));exit;
                    }
                } 
                if(isset($_POST['color'])){
                    $color = explode(',', $_POST['color']);
                }
                if($color[0] === ''){
                    $color[0] = null;
                    $color[1] = null;
                }
                $data = array(
                    'id' => $producto['id'],
                    'qty' => $_POST['qty'],
                    'price' => round($producto['precio']),
                    'name' => $producto['nombre'],
                    'options' => array(
                        'img' => 'https://apps.avalonprplus.com/uploads/' . $producto['imagen'],
                        'size' => $arr,
                        'colorHex' => $color[1],
                        'color' => $color[0],
                        'url'=> $producto['parent']['categoria']['slug'].'/'.$producto['parent']['subcategoria']['slug'].'/'.$producto['parent']['familia']['slug'].'/'.$producto['slug']
                    )
                );
                $this->cart_bushido->insert($data);
                $item = json_encode($this->cart_bushido->contents(), true);
                $respuesta = array(
                    "error" => "0",
                    "carrito" => $item
                );
            }
            print json_encode($respuesta, true);
        } else if ($action == 'remove') {
            $this->cart_bushido->remove($_POST['id']);
            $item = $this->cart_bushido->contents();
            $total = $this->cart_bushido->total();
            $dataCart = ' <div id="processTabs" class="hidden-xs">
                <ul class="process-steps bottommargin clearfix">
                    <li class="active">
                        <a class="i-circled i-bordered" id="carrito"></a>
                        <h5>Revisa tu pedido</h5>
                    </li>
                    <li>
                        <a class="i-circled i-bordered" id="envio"></a>
                        <h5>Introduce la información de envío</h5>
                    </li>
                    <li>
                        <a class="i-circled i-bordered" id="pago"></a>
                        <h5>Completa el pago</h5>
                    </li>
                    <li>
                        <a class="i-circled i-bordered" id="pedido"></a>
                        <h5>Finaliza el pedido</h5>
                    </li>
                </ul>
            </div>

            <h3>Carrito</h3>
            <div class="line-custom"></div>

            <div class="table-responsive nobottommargin">';
            if (empty($item)) {
                $dataCart .= '
                <div class="col-md-5 advice divcenter bottommargin-sm">
                    <p class="center no_items"> <i class="center material-icons">notification_important</i><br>¡No tiene nada en su carrito!</p>
                </div>';
            } else {
                $dataCart .= '<table class="table cart hidden-xs">
                    <thead>
                        <tr>
                            <th class="cart-product-remove">&nbsp;</th>
                            <th class="cart-product-thumbnail">&nbsp;</th>
                            <th class="cart-product-name">Producto</th>
                            <th class="cart-product-price">Precio por unidad</th>
                            <th class="cart-product-quantity">Cantidad</th>
                            <th class="cart-product-subtotal">Total</th>
                        </tr>
                    </thead>
                    <tbody>';
                $j = 0;
                foreach ($item as $i) {
                    $dataCart .= '<tr class="cart_item">
                        <td class="cart-product-remove">
                          <a class="remove" title="Eliminar artículo" onclick="removedatatable(\'' . $i['rowid'] . '\');" class="removeCart"><i class="icon-trash2"></i></a>
                        </td>
                        <td class="cart-product-thumbnail">
                          <a href="'.$this->data['siteurl'].'productos/'.$i['options']['url'].'"><img width="64" height="64" src="' . $i['options']['img'] . '" alt="' . $i['name'] . '"></a>
                        </td>

                        <td class="cart-product-name">
                          <a href="'.$this->data['siteurl'].'productos/'.$i['options']['url'].'">' . $i['name'] . '</a>';
                    if($i['options']['size']!=null){
                        foreach($i['options']['size'] as $talla){
                            $o = json_decode($talla);
                            $dataCart .= '</br><span>Talla: '.$o['0'].' x '.$o['1'].'uds</span>';
                        }
                    }
                    if($i['options']['color']!=null){
                        $dataCart .= '</br><span>Color: '.$i['options']['color'].'</span>';
                    }      
                        $dataCart .='</td>

                        <td class="cart-product-price">
                          <span id="p5" class="amount">' . $i['price'] . ' puntos</span>
                        </td>';
                            
                        if( $i['options']['size'] != null){
                        $dataCart .= '<td class="cart-product-quantity">
                          <div class="quantityCart clearfix">
                            <input type="text" name="quantity" data-id="' . $i['rowid'] . '" value="'. $i['qty'] .'" class="qty" disabled="true"/>
                            
                          </div>
                        </td>';
                        }else{
                            $dataCart .= '<td class="cart-product-quantity">
                              <div class="quantityCart clearfix">
                                <!--<input type="button" value="-" class="minus">-->
                                <input type="text" name="quantity" data-id="' . $i['rowid'] . '" value="'. $i['qty'] .'" class="qty" disabled="true"/>
                                <!--<input type="button" value="+" class="plus">-->
                              </div>
                            </td>';
                        }

                        $dataCart .= '<td class="cart-product-subtotal">
                          <span id="t5" class="amount">' . $i['subtotal'] . ' puntos</span>
                        </td>
                      </tr>';
                    $j++;
                }
                $dataCart .= ' </tbody>

                        </table>';
                
                $dataCart .= '<!-- INICIO TABLA MOBILE -->
                
                <table class="table cartMobile hidden-lg hidden-md hidden-sm cart_item" width="300">';
                
                     $j = 0;
                foreach ($item as $i) {
                    
                    $dataCart .= '<tbody>
                        <tr>
                            
                            <td class="cart-product-thumbnail" align="center"><a href="'.$this->data['siteurl'].'productos/'.$i['options']['url'].'"><img width="64" height="64" src="' . $i['options']['img'] . '" alt="' . $i['name'] . '"></a></td>
                            <td class="cart-product-name" colspan="2" align="left" width="200">
                                Producto:<br>
                                <a href="'.$this->data['siteurl'].'productos/'.$i['options']['url'].'">' . $i['name'] . '</a>';
                    
                        
                            if($i['options']['size']!=null){
                                foreach($i['options']['size'] as $talla){
                                    $o = json_decode($talla);
                                    $dataCart .= '</br><span>Talla: '.$o['0'].' x '.$o['1'].'uds</span>';
                                }
                            }
                            if($i['options']['color']!=null){
                                $dataCart .= '</br><span>Color: '.$i['options']['color'].'</span>';
                            } 
                            $dataCart .= '</td>
                        </tr>
                        
                        <tr>
                            <td class="cart-product-remove" align="center">
                                <a class="remove" title="Eliminar artículo" onclick="removedatatable(\'' . $i['rowid'] . '\');"><i class="icon-trash2"></i></a>
                            </td>

                            <td align="left">Precio por unidad</td>
                            <td scope="col"><span class="amount" align="center" id="amount{{key}}">' . $i['price'] . ' puntos</span></td>
                            
                        </tr>
                        
                        <tr>
                            <td>&nbsp;</td>
                            <td align="left">Cantidad:</td>';
                            
                            if( $i['options']['size'] != null){
                            
                            $dataCart .= '<td class="cart-product-quantity">
                                <div class="quantityCart clearfix">
                                    <input type="text" name="quantity" data-id="{{key}}" value="'. $i['qty'] .'" class="qty" disabled="true"/>
                                </div>
                            </td>';
                            
                            }else{
                            $dataCart .= '<td class="cart-product-quantity">
                                <div class="quantityCart clearfix">
                                    <!--<input type="button" value="-" class="minus">-->
                                    <input type="text" name="quantity" data-id="{{key}}" value="'. $i['qty'] .'" class="qty" disabled="true"/>
                                    <!--<input type="button" value="+" class="plus">-->
                                </div>
                            </td>';
                            }

                            
                        $dataCart .= '</tr>
                        
                        <tr>
                            <td>&nbsp;</td>
                            <td align="left">Total:</td>
                            <td class="cart-product-subtotal"><span class="amount" id="totalAmoun' . $i['rowid'] . '">' . $i['subtotal'] . ' puntos</span></td>
                        </tr>';
                } 
                        
                   $dataCart .= '</tbody>

                </table>';
                
                
                
                
                $dataCart .= '</div>   <div class="row clearfix">

                <div class="col-md-6  col-sm-8 col-xs-12 clearfix fright">
                    <div class="table-responsive">
                         <h4 class="crillee">Total carrito</h4>

                        <table class="table cart">
                            <tbody>
                                <tr class="cart_item">
                                    <td class="cart-product-name">
                                        <strong>Subtotal carrito</strong>
                                    </td>

                                    <td  style="text-align: right;">
                                        <span class="amount">' . number_format($total, 0, ',', '.') . ' puntos</span>
                                    </td>
                                </tr>
                                <tr class="cart_item">
                                    <td class="cart-product-name">
                                        <strong>Gastos de envío</strong>
                                    </td>

                                    <td  style="text-align: right;">
                                        <span class="amount">Envío gratis</span>
                                    </td>
                                </tr>
                                <tr class="cart_item">
                                    <td class="cart-product-name">
                                        <strong style="font-size: 21px;">Total</strong>
                                    </td>

                                    <td  style="text-align: right;">
                                        <span class="amount color lead"><strong>' . number_format($total, 0, ',', '.') . ' puntos</strong></span>
                                    </td>
                                </tr>
                            </tbody>

                        </table>

                    </div>
                </div>
            </div>
            <div class="row clearfix tramit_step1">
                
                    <a href="'.$this->data['siteurl'].'carrito/process" class="button button-3d fright">Tramitar pedido</a> 
                    <a href="'.$this->data['siteurl'].'productos" class="button button-3d button-black  fleft"><img src="'.$this->data['imgurl'].'volver-carrito.png" class="hidden-lg hidden-md hidden-sm"/><span class="hidden-xs">Volver al catálogo</span></a>

               
            </div>
        ';
            }
            echo $dataCart;
        } else if ($action == 'removeCart') {
            $this->cart_bushido->remove($_POST['id']);
            $item = json_encode($this->cart_bushido->contents(), true);
            echo $item;
        }
    }

    public function index() {
        foreach ($this->data['cart_bushido'] as $k => $cart) {
            if ($cart['options']['size'] != null) {
                $o = null;
                foreach ($cart['options']['size'] as $op) {
                    $o[] = json_decode($op);
                }
                $this->data['cart_bushido'][$k]['options']['size'] = $o;
            }
        }
        $user = $this->session->userdata;
        $this->data['content'] = $this->twig->render('cart/cart.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'cart' => $this->data['cart_bushido'], 'cart_total' => $this->data['cart_bushido_total'], 'user' => $user, 'textos'=>$this->lang->line('cart')));
        $this->twig->display('index.php', $this->data);
    }

    public function process() {
        $user = $this->Users_m->get_by_email($this->data['userlogged']->email);
        $direcciones = json_decode($user->direccion);
        $direcciones = json_decode(json_encode($direcciones), True);
        $i=0;
        foreach ($direcciones as $dir){
            if($dir['TipoDir'] == 'B'){
                $direccionesFact = $direcciones[$i];
            }
            $i++;
        }
        $direccionesEnvio = json_decode($user->direccionesEnvio);
        $direccionesEnvio = json_decode(json_encode($direccionesEnvio), True);
        $this->data['content'] = $this->twig->render('cart/cart-process.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl'], 'user' => $user, 'direccionesFact' => $direccionesFact, 'direccionesEnvio' => $direccionesEnvio, 'textos'=>$this->lang->line('cart')));
        $this->twig->display('index.php', $this->data);
    }

    public function resume() {
        foreach ($this->data['cart_bushido'] as $k => $cart) {
            if ($cart['options']['size'] != null) {
                $o = null;
                foreach ($cart['options']['size'] as $op) {
                    $o[] = json_decode($op);
                }
                $this->data['cart_bushido'][$k]['options']['size'] = $o;
            }
        }
        $this->data['content'] = $this->twig->render('cart/cart-resume.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl'], 'cart' => $this->data['cart_bushido'], 'cart_total' => $this->data['cart_bushido_total'], 'datosEnvio' => $_SESSION['datosEnvio'], 'textos'=>$this->lang->line('cart')));
        $this->twig->display('index.php', $this->data);
    }

    public function complete() {
        foreach ($this->data['cart_bushido'] as $k => $cart) {
            if ($cart['options']['size'] != null) {
                $o = null;
                foreach ($cart['options']['size'] as $op) {
                    $o[] = json_decode($op);
                }
                $this->data['cart_bushido'][$k]['options']['size'] = $o;
            }
        }
        $cart_temp = $this->data['cart_bushido'];
        $cart_total_temp = $this->data['cart_bushido_total'];
        $datosEnvio = $_SESSION['datosEnvio'];
        unset($_SESSION['datosEnvio']);
        $this->data['cart_bushido'] = null;
        $this->data['cart_bushido_total'] = null;
        $this->cart_bushido->destroy();
        $this->data['content'] = $this->twig->render('cart/cart-complete.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'siteurl' => $this->data['siteurl'], 'cart' => $cart_temp, 'cart_total' => $cart_total_temp, 'datosEnvio' => $datosEnvio, 'textos'=>$this->lang->line('cart')));
        $this->twig->display('index.php', $this->data);
    }

    public function saveDatos() {
        $datosEnvio = array(
            'billingcompanyname' => $_POST['billing-form-companyname'],
            'billingaddress' => $_POST['billing-form-address'],
            'billingcity' => $_POST['billing-form-city'],
            'billingemail' => $_POST['billing-form-email'],
            'billingphone' => $_POST['billing-form-phone'],
            'shippingname' => $_POST['shipping-form-name'],
            'shippingcompanyname' => $_POST['shipping-form-companyname'],
            'shippingaddress' => $_POST['shipping-form-address'],
            'shippingcity' => $_POST['shipping-form-city'],
            'shippingmessage' => $_POST['shipping-form-message']
        );
        $_SESSION['datosEnvio'] = $datosEnvio;
        echo '{ "alert": "successCarrito", "message": "Datos de envío guardados correctamente" }';
    }

    public function finalizarCompra() {
        $item = new stdClass();
        $item->cod_user = $this->data['userlogged']->cod_user;
        $item->pedido = json_encode($this->data['cart_bushido']);
        $item->kmtotal = $this->data['cart_bushido_total'];
        $item->datosEnvio = json_encode($_SESSION['datosEnvio']);
        $fecha = date("Y-m-d H:i:s");
        $item->fecha = $fecha;
        $idpedido = $this->Pedidos_bushido_m->upsert($item);
        $puntosTotales = $this->PuntosTotales_m->get_by_coduser($this->data['userlogged']->cod_user);
        $itemPuntos = new stdClass();
        $itemPuntos->redimidos = $puntosTotales->redimidos + $this->data['cart_bushido_total'];
        $itemPuntos->disponibles = ($puntosTotales->disponibles - $this->data['cart_bushido_total']);
        $this->PuntosTotales_m->upsert($itemPuntos, $puntosTotales->idpuntos);
        $check = $this->Users_m->get_conPuntos_logged($this->data['userlogged']->cod_user);
        $this->session->set_userdata('puntos', $check->disponibles);
        foreach ($this->data['cart_bushido'] as $k => $cart) {
            if ($cart['options']['size'] != null) {
                $o = null;
                foreach ($cart['options']['size'] as $op) {
                    $o[] = json_decode($op);
                }
                $this->data['cart_bushido'][$k]['options']['size'] = $o;
            }
        }
        /*$this->load->library("PhpMailerLib");
        $mail = $this->phpmailerlib->load();
        //Server settings
        $mail->SMTPDebug = 0;                              // SMTP password                               // TCP port to connect to
        //Recipients
        $mail->setFrom('canjeos.bushido@yokohamaiberia.com');
        $mail->addAddress($this->data['userlogged']->email);     // Add a recipient
        //$mail->addAddress('carlos.saenz@avalonprplus.com');
        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = utf8_decode($this->lang->line('cart')['mailUser']['asunto']);

        $body = $this->twig->render('mails/pedido_user.php', array('cart' => $this->data['cart_bushido'], 'cart_total' => $this->data['cart_bushido_total'], 'user' => $this->data['userlogged'],
            'datosEnvio' => $_SESSION['datosEnvio'], 'idpedido' => str_pad($idpedido, 4, "0", STR_PAD_LEFT), 'fecha' => $fecha, 'textos'=>$this->lang->line('cart')));
        $mail->Body = utf8_decode($body);
        $mail->send();
        $mail2 = $this->phpmailerlib->load();
        //Server settings
        $mail2->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail2->isSMTP();                                      // Set mailer to use SMTP
        $mail2->Host = 'smtp.office365.com';  // Specify main and backup SMTP servers
        $mail2->SMTPAuth = true;                               // Enable SMTP authentication
        $mail2->Username = 'canjeos.bushido@yokohamaiberia.com';                 // SMTP username
        $mail2->Password = 'Bushido2017';                           // SMTP password
        //Recipients
        $mail2->setFrom('canjeos.bushido@yokohamaiberia.com');
        $mail2->addAddress('canjeos.bushido@yokohamaiberia.com');     // Add a recipient
        //$mail2->addAddress('carlos.saenz@avalonprplus.com');
        //Content
        $mail2->isHTML(true);
        $mail2->Subject = utf8_decode('Nuevo pedido en Yokohama Bushido de ' . $this->data['userlogged']->nombre);
        $body = $this->twig->render('mails/pedido_admin.php', array('cart' => $this->data['cart_bushido'], 'cart_total' => $this->data['cart_bushido_total'], 'user' => $this->data['userlogged'],
            'datosEnvio' => $_SESSION['datosEnvio'], 'idpedido' => str_pad($idpedido, 5, "0", STR_PAD_LEFT), 'fecha' => $fecha));
        $mail2->Body = utf8_decode($body);
        $mail2->send();*/
        echo '{ "alert": "successPedido", "message": "Pedido ' . date('Y') . '-' . str_pad($idpedido, 4, "0", STR_PAD_LEFT) . ' creado correctamente" }';
    }

}
