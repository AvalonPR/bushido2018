<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Marketing extends CI_Controller {

    var $data;

    function __construct() {
        parent::__construct();
        if ($this->router->fetch_method() != 'login') {
            if (!isset($this->session->userdata['logged_in'])) {
                redirect('inicio/login');
            } elseif ($this->session->userdata['bases_Bushido'] === '0') {
                redirect('inicio/bases', 'refresh');
            }
        }

        $this->data = $this->template->get_data();
    }
    
    public function index(){
        $catalogos = $this->Catalogos_bushido_m->get();
        if (!empty($catalogos)) {
            $catalogos = $this->setViewer($catalogos, 'catalogos');
        }
        foreach ($catalogos as $k=>$c) {
            $filtros = json_decode($c['filtros']);
            $catalogos[$k]['filtros'] = $filtros;
        }
        $vista = 'marketing.php';
        $this->data['content'] = $this->twig->render($vista, array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'catalogos' => $catalogos));
        $this->twig->display('index.php', $this->data);
    }
    

    private function setViewer($sliders, $tipo) {
        $sliders = $this->checkFecha($sliders, $tipo);
        $sliders = $this->checkIdioma($sliders);
        $sliders = $this->checkVars($sliders);
        return $sliders;
    }

    private function checkFecha($sliders, $tipo) {
        $i = 0;
        foreach ($sliders as $s) {
            if ($s['fechai'] != '' || $s['fechai'] != null) {
                $fecha_actual = strtotime(date("Y-m-d", time()));
                $fecha_fin = strtotime($s['fechaf']);
                if ($fecha_actual > $fecha_fin) {
                    $item = new stdClass();
                    $item->activo = 0;
                    $this->Admin_m->upsertView($tipo, $item, $s['id']);
                    unset($sliders[$i]);
                    continue;
                }
            }
            $i++;
        }
        return $sliders;
    }

    private function checkIdioma($sliders) {
        $setView = array();
        $i = 0;
        foreach ($sliders as $s) {
            $atributos = json_decode($s['atributos'], true);
            if (!isset($atributos['Pais']) || empty($atributos['Pais'])) {
                array_push($setView, $sliders[$i]);
            } else {
                foreach ($atributos['Pais'] as $v) {
                    if ($v == 'ES') {//cambiar por atributo de sesion
                        array_push($setView, $sliders[$i]);
                    }
                }
            }
            $i++;
        }
        return $setView;
    }

    private function checkVars($sliders) {
        $setView = array();
        $i = 0;
        foreach ($sliders as $s) {
            $atributos = json_decode($s['atributos'], true);
            if (isset($atributos['Pais'])) {
                unset($atributos['Pais']);
            }
            if ($atributos == null || empty($atributos)) {
                array_push($setView, $sliders[$i]);
            } else {
                $match = count($atributos);
                $goal = 0;
                foreach ($atributos as $k => $v) {
                    if (array_key_exists($k, $this->session->userdata['atributos'])) {
                        if ($this->session->userdata['atributos'][$k] == $v) {
                            $goal++;
                        }
                    }
                }
                if ($match == $goal) {
                    array_push($setView, $sliders[$i]);
                }
            }
            $i++;
        }
        return $setView;
    }
    
    
}