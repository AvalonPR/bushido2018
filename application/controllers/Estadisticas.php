<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Estadisticas extends CI_Controller {

    var $data;

    function __construct() {
        parent::__construct();
        if (!isset($this->session->userdata['logged_in'])) {
            redirect('inicio/login');
        } elseif ($this->session->userdata['tipo'] === 'A') {
            redirect('admin');
        } elseif ($this->session->userdata['tipo'] === 'S') {
            redirect('comercial');
        } elseif ($this->session->userdata['bases_Bushido'] === '0') {
            redirect('inicio/bases');
        } elseif (is_null($this->session->userdata['viaje'])) {
            redirect('inicio/viaje', 'refresh');
        }

        $this->data = $this->template->get_data();
    }

    public function index() {
        /***
         * PENDIENTE KPI'S
         */
    }
    
    private function groupArray($array, $groupkey) {
        if (count($array) > 0) {
            if (isset($array[0])) {
                $keys = array_keys($array[0]);
            } else {
                $tempArray = $array;
                $array = null;
                $array[0] = $tempArray;
                $keys = array_keys($array[0]);
            }
            $removekey = array_search($groupkey, $keys);
            if ($removekey === false) {
                return array("Clave \"$groupkey\" no existe");
            } else {
                unset($keys[$removekey]);
                $groupcriteria = array();
                $return = array();
                foreach ($array as $value) {
                    $item = null;
                    foreach ($keys as $key) {
                        $item[$key] = $value[$key];
                    }
                    $busca = array_search($value[$groupkey], $groupcriteria);
                    if ($busca === false) {
                        $groupcriteria[] = $value[$groupkey];
                        $return[] = array($value[$groupkey] => array());
                        $busca = count($return) - 1;
                    }
                    $return[$busca][$value[$groupkey]][] = $item;
                }
                $return = $this->keyArray($return);
                return $return;
            }
        } else {
            return array();
        }
    }

    private function keyArray($array) {
        foreach ($array as $k => $a) {
            $nameArray = key($a);
            $keyArray[$nameArray] = $a[$nameArray];
        }
        return $keyArray;
    }

    private function calcularRecambios($fac) {
        date_default_timezone_set('europe/madrid');
        $sumPrimSem2018 = 0;
        $sumSegSem2018 = 0;
        $sumPrimSem2019 = 0;
        $sumSegSem2019 = 0;
        $primSem2018 = strtotime("31-05-2018 23:59:59");
        $segSem2018 = strtotime("31-12-2018 23:59:59");
        $primSem2019 = strtotime("31-05-2019 23:59:59");
        $segSem2019 = strtotime("31-12-2019 23:59:59");
        foreach ($fac as $f) {
            if (strtotime($f['fecha']) < $primSem2018) {
                $sumPrimSem2018 = $sumPrimSem2018 + $f['kilometros'];
            } else if (strtotime($f['fecha']) < $segSem2018) {
                $sumSegSem2018 = $sumSegSem2018 + $f['kilometros'];
            } else if (strtotime($f['fecha']) < $primSem2019) {
                $sumPrimSem2019 = $sumPrimSem2019 + $f['kilometros'];
            } else if (strtotime($f['fecha']) < $segSem2019) {
                $sumSegSem2019 = $sumSegSem2019 + $f['kilometros'];
            }
        }
        return $recambios = array(
            'sumPrimSem2018' => $sumPrimSem2018,
            'sumSegSem2018' => $sumSegSem2018,
            'sumPrimSem2019' => $sumPrimSem2019,
            'sumSegSem2019' => $sumSegSem2019
        );
    }

    private function facturacionPorMeses($facturacion) {
        $factBymeses = array();
        $i = 0;
        foreach ($facturacion as $f) {
            if ($f['archivo'] == 'Recambios') {
                $factBymeses[$i]['fecha'] = $this->returnMes($f['fecha']);
            } else {
                $fecha = $this->convertirFecha_SpanishToEnglish($f['fecha']);
                $factBymeses[$i]['fecha'] = $this->returnMes($fecha);
            }
            $factBymeses[$i][] = $f['total'];
            $i++;
        }
        return $factBymeses;
    }

    private function returnMes($fecha) {
        $mes = date('F', strtotime($fecha));
        $meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        $nombreMes = str_replace($meses_EN, $meses_ES, $mes);
        return $nombreMes;
    }

    private function convertirFecha_SpanishToEnglish($date) {
        if ($date) {
            $fecha = $date;
            $hora = "";

            # separamos la fecha recibida por el espacio de separación entre
            # la fecha y la hora
            $fechaHora = explode(" ", $date);
            if (count($fechaHora) == 2) {
                $fecha = $fechaHora[0];
                $hora = $fechaHora[1];
            }

            # cogemos los valores de la fecha
            $values = preg_split('/(\/|-)/', $fecha);
            if (count($values) == 3) {
                # devolvemos la fecha en formato ingles
                if ($hora && count(explode(":", $hora)) == 3) {
                    # si la hora esta separada por : y hay tres valores...
                    $hora = explode(":", $hora);
                    return date("Y/m/d H:i:s", mktime($hora[0], $hora[1], $hora[2], $values[1], $values[0], $values[2]));
                } else {
                    return date("Y/m/d", mktime(0, 0, 0, $values[1], $values[0], $values[2]));
                }
            }
        }
        return "";
    }

}
