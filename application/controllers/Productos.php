<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends CI_Controller {

    var $data;

    function __construct() {
        parent::__construct();
            if (!isset($this->session->userdata['logged_in'])) {
                redirect('inicio/login');
            } elseif($this->session->userdata['bases_Bushido']==='0'){
                    redirect('inicio/bases');
            } elseif (is_null($this->session->userdata['viaje']) || $this->session->userdata['viaje']!=0) {
                redirect('inicio/viaje', 'refresh');
            }

        $this->data = $this->template->get_data();
    }

    public function index($categoria = false, $subcategoria = false, $familia = false, $slug = false) {
        $breadcumb = array(
            'categoria' => $categoria,
            'subcategoria' => $subcategoria,
            'familia' => $familia
        );
        foreach($breadcumb as $k=>$b){
            if($b==''){
                    unset($breadcumb[$k]);
                }
            }
            if($this->session->userdata['site_lang']=='portuguese'){
                $jerarquia = $this->catalogo->jerarquiaPT();
            } else{
                $jerarquia = $this->catalogo->jerarquia();
            }
        if($slug!=false){
            $productos = $this->catalogo->get_producto_by_slug($slug);
            if($productos['atributos']!=null){
                $productos['atributos'] = json_decode($productos['atributos'], true);
            }
            $this->data['content'] = $this->twig->render('shop-single.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'productos' => $productos, 'jerarquia'=>$jerarquia, 'user' => $this->session->userdata, 'cart_total' => $this->data['cart_bushido_total'], 'userData' => $this->data['userlogged'], 'textos'=>$this->lang->line('catalogo')));
            $this->twig->display('index.php', $this->data);
            
        }else{
            if($categoria!=false){
                $productos = $this->catalogo->productos($categoria, $subcategoria, $familia, false, false, false);
            }else{
                $productos = $this->catalogo->productos(false, false, false, 6, false, 1);
            }
            $i = 0;
            $this->data['content'] = $this->twig->render('shop.php', array('imgurl' => $this->data['imgurl'], 'baseurl' => $this->data['baseurl'], 'productos' => $productos, 'breadcumb' => $breadcumb, 'jerarquia'=>$jerarquia, 'user' => $this->session->userdata, 'cart_total' => $this->data['cart_bushido_total'], 'userData' => $this->data['userlogged'], 'textos'=>$this->lang->line('catalogo')));
            $this->twig->display('index.php', $this->data);
        }
    }


}