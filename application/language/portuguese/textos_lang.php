<?php
// PORTUGUES //

/* INICIO */
$lang['home'] = [
    'buscador'  => [
       'titulo'       => 'ENCONTRE O SEU PRODUTO NO CATÁLOGO'
      ,'subtitulo'    => 'Pesquise nosso catálogo para o produto que você precisa'
      ,'categoria'    => ['label'=>'Categorias','select'=>'Selecione uma categoria']
      ,'subcategoria' => ['label'=>'Subcategorias']
      ,'familia'      => ['label'=>'Famílias']
      ,'boton'        => 'PROCURAR'
    ],
      'viaje' => [
          'titulo'=>'Faça as malas',
          'subtitulo'=>'Nós já temos o programa de viagem',
          'boton'=>'Junte-se a nós no nosso destino'
      ]
    ,'secciones' => [
       'entrada' => '"Se você entrar no caminho inexplorado, no final infinitos segredos aparecerão."'
     , 'trayectoria' => ['label' => 'DESCUBRA','boton' => 'A SUA TRAJETÓRIA','url'=>base_url().'inicio/trayectoria']
   ]
 ];

  $lang['perfil'] = [ 
    'datos' => [
        'titulo' => 'Meu perfil',
        'info' => 'Para efetuar alterações aos seus dados pessoais, deverá contactar o seu comercial.',
        'avatar' =>[
            'cambiar' => 'Alterar Imagem',
            'imagen' => 'Medidas da imagen'
        ],
        'tittabla' => 'Perfil de usuario',
        'tabla' => [
            'nif' => 'NIF', 
            'razon' => 'Nome da pessoa de contato',
            'tfno' => 'Teléfone',
            'email' => 'Email',
            'dir' => 'Morada',
            'pob' => 'Localidade',
            'prov' => 'Provincia',
            'cp' => 'Código postal'
        ],
        'pass' =>[
            'entradilla' => 'Se você quiser alterar sua senha, você pode fazê-lo abaixo.',
            'cambiar' => 'Alterar Password',
            'new' => 'Nova password',
            'rep' => 'Repita a nova password'
        ],
        'messages' => [
            'nocoin' => 'As senhas não correspondem',
            'changePw' => 'Mudou sua senha corretamente',
            'error' => 'Ocorreu um erro, tente novamente mais tarde.',
            'avatarok' => 'O imagem do perfil foi carregado.',
            'avatarerrmv' => 'O arquivo não pôde ser movido.',
            'avatarerrfol' => 'A pasta não pôde ser criada.',
            'avatarerrtype' => 'Não é um arquivo válido.',
            'avatarerrnofile' => 'Nenhum arquivo foi carregado.'          
        ],
        'direcciones' => 'Morada de cobrança',
        'direccion' => 'Morada de Entrega (Para Bushido)',
        'nodir' => 'Ele ainda não incluiu nenhum morada',
        'nodirdetails' => 'Aqui seus endereços serão mostrados para receber seus produtos Bushido'
    ]
  ];
  
    $lang['contacto'] = [
      'titulo' => 'Contacto',
      'entradilla' => 'Preencha os campos para entrar em contato conosco e explicar sua incidência.',
      'tabla' => [
          'nombre' => 'Nome',
          'email' => 'Email',
          'tfno' => 'Telefone',
          'asunto' => 'Assunto',
          'msn' => 'Mensagem'          
      ],
      'butt' => 'Enviar',
      'claim' => 'Yokohama Bushido é um programa de Yokohama Iberia S.A. Para entrar em contato diretamente com a entidade, você pode fazê-lo através de  <a href="mailto:info.bushido@yokohamaiberia.com">info.bushido@yokohamaiberia.com</a>ou pelo telefone 252 249 070.',
      'dir' => 'Rua 12 - Zona Industrial da Varziela',
      'complete' => '4480-109 Vila do Conde',
      'tfno' => 'Telefone: 252 249 070',
      'email' => 'Email:<a href:"mailto:info.bushido@yokohamaiberia.com"> info.bushido@yokohamaiberia.com</a>'
  ];

   $lang['viaje'] = [
      'titulo' => 'A grande viagem'
       ,'subtitulo' => 'Você está pronto para viver uma viagem inesquecível?'
       ,'boton' => 'escolha a opção'
       ,'estimado' => 'Estimado Cliente,'
       ,'bienvenida' => 'Damos as boas vindas ao BUSHIDO 2019! Antes de continuar deverá selecionar entre participar na promoção Viagem de Incentivos 2019, com destino final ainda a confirmar, ou participar no Programa de Incentivos Yokohama Bushido, que permite acumular KM durante o ano todo para trocar por presentes dum catálogo que poderá consultar nesta mesma página.'
       ,'condiciones' => 'Condições para participar na Viagem de Incentivos 2019:'
       ,'dosplazas' => 'Faturação 2 lugares: 100.000€ de faturação onde o valor médio de compra do pneu deverá ser superior a 50€. Faturação na gama PCR (Turismo, VAN e 4x4).'
       ,'unaplaza' => 'Faturação 1 lugar: 65.000€ de faturação onde o valor médio de compra do pneu deverá ser superior a 50€. Faturação na gama PCR (Turismo, VAN e 4x4).'
       ,'sinviaje' => 'O cliente, uma vez selecionada a viagem deixará de aceder aos KM do Bushido; selecione 1 ou 2 lugares'
       ,'corte' => 'Será efetuado uma contabilização a 1 de julho de 2019, data a qual o cliente deverá ter conseguido pelo menos 49% do objetivo ( 2 lugares = 49.000€ de faturação e 1 lugar = 31.850€ de faturação, ambos com um média mínima de 50€/pneu). No caso de não ter conseguido o objetivo definido passará automaticamente para o programa de KM sem aviso prévio.'
       ,'eleccion' => 'Escolha quantos lugares você deseja ou se preferir participar do nosso catálogo de presentes'
       ,'eleccionyear' => 'Escolha o ano em que deseja trocar sua viagem:'
       ,'participar' => 'Eu vou participar do Bushido'
       ,'maletas' => 'Você já embalado?'
       ,'aceptar' => 'Aceitar'
       ,'plaza' => 'Assento'
       ,'plazas' => 'Assentos'
       ,'tituloviaje' => 'Nós vamos a '
       ,'destinoviaje' => 'VIETNAM'
       ,'descarga' => 'Descarrega aqui o programa de viagens de incentivo Bushido 2019-2020'
       ,'botonprograma' => 'Descarrega o programa'
    ];
   
   $lang['cart'] = [
     'index' => [
         'titulo' => 'Carrinho',
         'envio' => 'Envio',
         'pago' => 'Pagamento',
         'pedido' => 'Ordem',
         'pasos' =>[
           'primero' => 'Verifique seu pedido',
           'segundo' => 'Insira as informações de envio',
           'tercero' => 'Complete o pagamento',
           'cuarto' => 'Termine o pedido'
         ],
         'tabla' => [
             'prod' => 'Produto',
             'price' => 'Preço Unitário',
             'qty' => 'Quantidade',
             'total' => 'Total',
             'totalCart' => 'Carrinho total',
             'subtotal' => 'Carrinho subtotal',
             'gastos' => 'Gastos de envio',
             'gratis' => 'Envio grátis'
         ],
         'backButt' => 'Voltar para o catálogo',
         'checkButt' => 'Ordem de processo',
         'empty' => 'Você não tem nada no seu carrinho!'
     ], 
     'process' => [
         'tablaFact' => [
             'titulo' => 'Endereço de cobrança',
             'nombre' => 'Nome da empresa',
             'dir' => 'direção',
             'pob' => 'população',
             'email' => 'e-mail',
             'tfno' => 'telefone'
         ],
         'tablaEnvio' => [
             'titulo' => 'Endereço para envio',
             'same' => 'Os dados de envio são os mesmos que as informações de cobrança',
             'recev' => 'Nomes e sobrenomes',
             'obs' => 'Observações',
             'nombre' => 'Nome da companhia',
             'dir' => 'direção',
             'pob' => 'população',
         ],
         'backButt' => 'voltar',
         'checkButt' => 'Salvar dados'         
     ],
     'resume' => [
         'titulo' => 'Resumo do seu pedido',
         'entradilla' => 'Abaixo você tem em detalhes o resumo do pedido que você fez. Por favor, verifique se todos os dados estão corretos. Se não for esse o caso, entre em contato com <a href="mailto:contacto.bushido@yokohamaiberia.com">contacto.bushido@yokohamaiberia.com</a><br> Você também receberá um email com todas essas informações.',
         'tabla' => [
             'prod' => 'Produto',
             'price' => 'Preço Unitário',
             'qty' => 'Quantidade',
             'total' => 'Total',
             'totalCart' => 'Carrinho total',
             'subtotal' => 'Carrinho subtotal',
             'gastos' => 'Gastos de envio',
             'gratis' => 'Envio grátis'
         ],
         'details' => 'Detalhes do pedido',
         'dirFact' => 'Endereço de cobrança',
         'dirEnvio' => 'Endereço para envio',
         'obs' => 'Observações para o transportador',
         'backButt' => 'voltar',
         'checkButt' => 'Confirme o pedido'
     ],
     'complete' => [
         'titulo' => 'Pedido recebido',
         'entradilla' => 'Obrigado, seu pedido foi recebido',
         'tabla' => [
             'prod' => 'Produto',
             'price' => 'Preço Unitário',
             'qty' => 'Quantidade',
             'total' => 'Total',
             'totalCart' => 'Carrinho total',
             'subtotal' => 'Carrinho subtotal',
             'gastos' => 'Gastos de envio',
             'gratis' => 'Envio grátis'
         ],
         'details' => 'Informação do pedido',
         'dirFact' => 'Endereço de cobrança',
         'dirEnvio' => 'Endereço para envio',
         'obs' => 'Observações para o transportador',
         'butt' => 'Meus pedidos'
     ],
     'mailUser' => [
         'asunto' => 'Novo pedido em Yokohama Bushido',
         'fecha' => 'Data',
         'num' => 'Número de pedido',
         'revisar' => 'Rever pedido',
         'hola' => 'Olá',
         'texto' => 'Aqui está um resumo dos dados do pedido. Se você encontrar algum erro ou quiser fazer alterações no pedido, entre em contato conosco <a href="mailto:info.bushido@yokohamaiberia.com">info.bushido@yokohamaiberia.com</a> ou ligando para o telefone <strong>915 63 10 11</strong>',
         'pedido' => 'Encomenda enviada para:',
         'dirigido' => 'Endereçado a:',
         'info' => 'INFORMAÇÃO DE PEDIDO',
         'qty' => 'Quantidade:',
         'talla' => 'Tamanho:',
         'color' => 'Cor:',
         'total' => 'Quilômetros totais:',
         'subtotal' => 'Subtotal',
         'gastos' => 'Gastos de envio',
         'totalKim' => 'Quilômetros totais',
         'obs' => 'Observações'
     ]
   ];
   
 
 $lang['folletos'] = [
    'buscador'  => [
       'titulo' => 'Veja as nossas promoções ou se preferir procurar por intervalo de datas ou tipo de presente:'
      ,'fecha'  => ['desde' => 'Desde','hasta'=>'Até']
      ,'busqueda' => 'Procurar por tipo'
      ,'mas'    => 'Ver descrição completa'
      ,'boton'  => 'PROCURAR'
    ]
   ,'botones'   => ['descarga' => 'Download','ver'=>'Ver']
 ];

 $lang['estadisticas'] = [
   'titulo' => 'ESTATÍSTICAS'
     ,'content' => [
         'aviso' => 'Aqui você pode ir às compras em pneus de controle de Yokohama, irá aparecer assim que você faça a sua primeira compra de 2017.'
         ,'aviso1' => 'Ele não encontrou qualquer informação nesta pesquisa.'
         ,'aviso2' => 'Este cliente ainda não entrou Yokohama Bushido. Entre em contato com!'
         ,'aviso3' => 'Este cliente último acesso'
         ,'facturacion' => 'Faturamento'
         ,'empresa' => 'Companhia'
         ,'fecha' => 'Data'
         ,'factura' => 'Fatura'
         ,'familia' => 'Família'
         ,'CV' => 'CV'
         ,'descripcion' => 'Descrição'
         ,'unidades' => 'Unidades'
         ,'mas' => 'Mas'

      ]
    ,'buscador'  => [
        'buscar' => 'PROCURAR'
        ,'fechaI' => 'Data de Início'
        ,'fechaF' => 'Data de Fim'
        ,'estacion' => 'ESTAÇÃO'
        ,'empresa' => 'EMPRESA'
        ,'familia' => 'FAMÍLIA'
        ,'modelo' => 'MODELO'
        ,'llanta' => 'LLANTA'
        ,'ancho' => 'LARGURA'
        ,'serie' => 'SÉRIE'
        ,'ic' => 'IC'
        ,'cv' => 'CV'
        ,'enviar' => 'ENVIAR'
      ]
      ,'grafico' => [
          'ntcos' => 'Pneus.'
         , 'familias' => 'Famílias'
          ,'modelos' => 'Modelos'
          ,'Cv'=>'Código de velocidade'
          ,'Ic'=>'Índice de carga'
          ,'llanta'=>'llanta'
          ,'ncv'=>'Pneus por'
      ]
   ];


  $lang['pedidos'] = [
	 'titulo' => 'MEUS PEDIDOS'
  ,'tituloA' => 'PEDIDOS'
	,'tabla' => [
		 'num'   => 'Pedido'
		,'fecha' => 'Data'
		,'direc' => 'Morada de Entrega'
		,'km'    => 'Quilómetros'
		,'mas'   => 'Mais'
		,'info'  => 'Informação do pedido'
		]
	,'boton' => 'Volver'
  ];

  $lang['verpedidos'] = [
	 'titulo'  => 'MEU PEDIDO'
  ,'tituloA'  => 'PEDIDO'
	,'content' => [
		 'titulo' => 'Meu pedido'
    ,'tituloA' => 'Pedido'
		,'fecha'  => 'Data'
		,'direc'  => 'O seu pedido foi enviado a'
    ,'direcA'  => 'O pedido foi enviado a (pessoa de contato)'
		,'quien'  => 'Dirigido a'
    ,'realizado'  => '¡Parabéns, o seu pedido foi realizado com êxito!'
		,'info'   => 'Informação do pedido'
		,'uds'    => 'Unidades'
		,'import' => 'Custo dos produtos'
		,'envio'  => 'Transporte e expedição'
		,'total'  => 'Custo total do produto (IVA incluído)'
		,'canje'  => 'Troca de pontos'
		,'observ' => 'OBSERVAÇÕES'
		,'hola'	  => 'Olá'
		,'linfo'  => 'Informamos que o seu pedido foi efetuado corretamente e procederemos ao seu envio num período estimado de entrega de 30 a 45 dias. Se desejar cancelar o seu pedido, deverá enviar um correio eletrónico para <a href="mailto:canjeos.bushido@yokohamaiberia.com" id="pagfinc" title="canjeos.bushido@yokohamaiberia.com" target="_blank">canjeos.bushido@yokohamaiberia.com</a> indicando o número do pedido e a informar a razão do cancelamento.'
		,'donde'  => 'O seu pedido foi enviado para'
		]
	,'volver'  => 'Voltar a Meus Pedidos'
  ,'volverA' => 'Voltar a Pedidos'

  ];


 $lang['catalogo'] = [
   'titulo' => 'Catálogo'
  ,'subtitulo' => 'Catálogo completo'
  ,'filtros' => 'Selecione seus filtros de pesquisa'
  ,'cats' => 'Categorias'
  ,'item' => [
     'desc' => 'Ver descrição completa'
    ,'talla' => 'Selecione o tamanho: '
    ,'color' => 'Selecione Cor: '
  ]
  ,'botones' => ['ver' => 'Ver','add' => 'Adicionar','addmore' => 'Adicionar','volver' => 'Voltar', 'vista' => 'Vista rápida']
  ,'content' => [ 
                'promo' => 'Promoçao',
  	'noviaje' => 'Caso pretenda voltar a aceder ao catálogo tem a opção de renunciar à viagem utilizando para tal o seguinte correio eletrónico:'
    ]
 ];

 $lang['administracion'] = [
        'buscadorTab' =>[
            'admin' => 'Gestão de usuários'
            ,'volver'=>'Voltar para a gestão'
            ,'pais'=>'País'
            ,'esp'=>'Espanha'
            ,'pt'=>'Portugal'
            ,'ad'=>'Andorra'
            ,'buscar'=>'PROCURAR'
            ,'introducir'=>'Introduzir apenas valores numéricos.'
            ,'cod'=>'Código de cliente'
            ,'email'=>'E-mail'
            ,'empresa'=>'>Companhia'
            ,'filtrar'=>'Filtro'
            ,'restaurar'=>'RESTAURAR'
        ]
	,'tablaUsuario' => [
            'Kdisponibles' => 'Km disponíveis para resgate'
            ,'Kobtenidos' => 'Km obtenidos'
            ,'ranking' => 'Ranking'
            ,'idioma' => 'Idioma'
            ,'codCliente' => 'Cod. Cliente'
            ,'empresa' => 'Companhia'
            ,'email' => 'Email'
            ,'telefono' => 'Telefone'
            ,'viaje' => 'Viagem'
            ,'anoV' => 'Ano de viagem'
            ,'numVisitas' => 'Número de visitas'
            ,'mas' => 'Mais detalhes'
            ,'info' => 'Companhia'
            ,'punt' => 'Quilômetros'
            ,'ifViaje' => 'Este cliente escolheu a viagem, pontuação de caráter orientativo.'
            ,'obtenidos' => 'Obtido'
            ,'redimidos' => 'Resgatadas'
            ,'disponibles' => 'Disponíveis'
            ,'acumulados' => 'Acumulados'
            ,'fecha' => 'Última conexão'
            ,'fechaActu' => 'atualização de Km'
            ,'fact' => 'Faturação total incluíndo TBS/OTR'
            ,'factViaje' => 'Faturação total da viagem, não inclui TBS/OTR'
            ,'ano' => 'Ano'
            ,'zona' => 'Área comercial'
            ,
        ]
    ,
    'puntuaciones' => []
   ,'paginas'  => [
        'tabs' => [
          'pag'   => 'Páginas'
         ,'promo' => 'Promoções'
         ,'mark'  => 'Marketing'
         ,'cat'   => 'Categorias'
       ]
       ,'boton'   => 'Retorno'
       ,'forms' => [
          'titulopro' => 'Promoção'
         ,'nombre'    => 'Nome da Página'
         ,'slug'      => 'URL da página'
         ,'titulo'    => 'Título da página'
         ,'tipopro'   => 'Tipo de promoção'
         ,'pais'      => 'País da Promoção'
         ,'imagen'    => 'Imagem'
         ,'fechaini'  => 'Data de Início'
         ,'fechafin'  => 'Data de Fim'
         ,'guardar'   => 'Guardar'
         /* Marketing */
         ,'titulocat' => 'Catálogo'
         ,'titulotit' => 'Título'
         ,'tipocat'   => 'Tipo de catálogo'
         ,'titulocat' => 'Catálogo'
         ,'see'       => 'URL para visualizar'
         ,'download'  => 'URL para download'
         ,'activo'    => 'Activo'
         ,'noactivo'  => 'No activo'
         ,'paiscat'   => 'País de Publicação'
         /* Paginas*/
         ,'titulopag' => 'Páginas'
         ,'tipopag'   => 'Tipo de página'


         /* Modals Editor */
         ,'link'      => [
            'titulo'  => 'Inserir Link'
           ,'tip'     => '<b>Dica:</b> Para inserir um link a um email, começe o URL com “mailto:”'
           ,'url'     => 'URL (direção)'
           ,'texto'   => 'Texto'
           ,'donde'   => 'Onde abrir o link'
           ,'nueva'   => 'Um novo separador irá abrir'
           ,'self'    => 'A página atual irá abrir'
           ,'insert'  => 'Inserir Link'
           ,'cancel'  => 'Cancelar'
         ]
         ,'imagenes'  => [
            'titulo'  => 'Inserir uma ou várias imagens'
           ,'placeho' => 'Inserir um ou mais ficheiros'
           ,'insert'  => 'Inserir imagem(ns)'
         ]
         ,'tablas'    => [
            'titulo'  => 'Inserir tabela'
           ,'filas'   => 'Filas'
           ,'column'  => 'Colunas'
           ,'ancho'   => 'Largura'
           ,'pixel'   => 'pixels'
           ,'percent' => 'Percentagem'
           ,'insert'  => 'Inserir Tabela'
         ]
       ]
   ]
   ,'catprod' => [
      'titulo' => 'Categorías de conteúdos'
     ,'nombre' => 'Nome da categoría'
     ,'nombrePT' => 'Nome da categoría português'
     ,'tab'    => 'Nome da guia'
     ,'tabPT'  => 'Nome da guia português'
     ,'tipo'   => 'Tipo de categoría'
     ,'tipom'  => 'Marketing'
     ,'tipop'  => 'Promoções'
   ]
   ,'usuarios' => [
      'idioma'   => 'País'
     ,'cliente'  => 'Cod. Cliente'
     ,'empresa'  => 'Companhia'
     ,'email'    => 'E-mail'
     ,'telefono' => 'Telefone'
     ,'visitas'  => 'Número de visitas'
     ,'viaje'    => 'Viajem (Num.Assentos)'
     ,'mas'      => 'Mais Detalhes'
     ,'info'     => 'Endereços da Empresa'
     ,'noexisten'=>'Há usuários no banco de dados'
     ,'buscador' =>[
       'volver' => 'RETORNO'
       ,'buscar' => 'BUSCAR'
       ,'pais' => 'PAÍS'
       ,'espa' => 'ESPANHA'
       ,'portu' => 'PORTUGAL'
       ,'ando' => 'ANDORRA'
       ,'codigo' => 'Código do Cliente'
       ,'label'=>'Introduzir apenas valores numéricos'
       ,'filtrar'=>'TRIAGEM'
     ]
   ]
 ];
   $lang['trayectoria'] =[
      'camino'  => [
       'tabtitulo' => 'Trajetória'
      ,'titulo'    => 'As sete virtudes do código Bushido '
        ,'content' => [
                'gi' => [
                    'traduccion' => 'Justiça',
                    'titulo' => 'Justiça',
                    'texto' => 'Sê honrado nas tuas ações com todas as pessoas. Crê na justiça, mas não na que emana dos demais, mas sim na tua própria.'
                ],
                'yu' => [
                    'traduccion' => 'Valor',
                    'titulo' => 'Valor',
                    'texto' => 'É arriscado, é perigoso, mas sem dúvida que também é viver a vida de forma plena. <br/> Substitui o medo por respeito e precaução.'
                ],
                'jin' => [
                    'traduccion' => 'Compaixão',
                    'titulo' => 'Compaixão',
                    'texto' => 'Desenvolverás um poder tão grande que deverás usa-lo somente para o bem de todos.<br/>Você deve ter compaixão.'
                ],
                'rei' => [
                    'traduccion' => 'Cortesia',
                    'titulo' => 'Cortesia',
                    'texto' => 'Ser um guerreiro não justifica a crueldade. Não necessitas de demonstrar a tua força a ninguém exceto a ti mesmo. Sê especialmente cortês com os teus inimigos, senão não serás melhor que os animais.'
                ],
                'makoto' => [
                    'traduccion' => 'Sinceridade',
                    'titulo' => 'Sinceridade',
                    'texto' => 'Quando um samurai diz que vai fazer algo, é como se já estivesse feito. “Falar” e “fazer” são, para um samurai, a mesma ação.'
                ],
                'meyo' => [
                    'traduccion' => 'Honra',
                    'titulo' => 'Honra',
                    'texto' => 'Somente existe um juiz da tua honra, tu próprio. As decisões que tomas e como as levas a cabo são o reflexo de quem és. Não se pode esconder de si próprio.'
                ],
                'chu' => [
                    'traduccion' => 'Lealdade',
                    'titulo' => 'Lealdade',
                    'texto' => 'Todas as tuas ações são como as tuas pegadas: podem ser seguidas onde quer que vás, por isso deves ter cuidado com o caminho que segues.'
                ],
            ]
      ]
    ];

$lang['emails'] = [
   'contacto'  => [
      'title'  => 'Mensaje de contacto | Yokohama Bushido'
     ,'saludo' => 'Ol&aacute;'
     ,'texto'  => 'A sua mensagem foi enviada corretamente.<br>
            Entraremos em contacto consigo brevemente<br>
            <br>
            Muito Obrigado.'
     ,'equipo' => 'Att. <span style="color:#D40017">A equipe de Yokohama Bushido</span>'
     ,'frase'  => 'Siga o caminho do Guerreiro e consiga presentes fant&aacute;sticos.'
   ]
  ,'pedido'   => [
     'title'  => 'Meu Pedido | Yokohama Bushido'
    ,'titulo' => 'Meu Pedido'
    ,'fecha'  => 'Data'
    ,'enhor1' => '¡Parab&eacute;ns'
    ,'enhor2' => 'o seu pedido foi realizado com &ecirc;xito!'
    ,'quien'  => 'Dirigido a:'
    ,'donde'  => 'To seu pedido foi enviado a seguinte morada:'
    ,'info'   => 'Informa&ccedil;&atilde;o do pedido:'
    ,'uds'    => 'Unidades:'
    ,'import' => 'Custo dos produtos:'
    ,'envio'  => 'Transporte e expedi&ccedil;&atilde;o'
    ,'include'=> 'Inclu&iacute;do'
    ,'total'  => 'Custo total do produto (IVA inclu&iacute;do):'
    ,'canje'  => 'Troca de pontos:'
    ,'observ' => 'Observa&ccedil;oes:'
    ,'info1'  => 'Informamos que o seu pedido foi efetuado corretamente e procederemos ao seu envio num per&iacute;odo estimado de entrega de 30 a 45 dias.'
    ,'info2'  => 'Se desejar cancelar o seu pedido, dever&aacute; enviar um correio eletr&oacute;nico para </span><span style="color:#DC0026">canjeos.bushido@yokohamaiberia.com</span><span style="color:#808080"> indicando o n&uacute;mero do pedido e a informar a raz&atilde;o do cancelamento.'
  ]
  ,'viaje'    => [
     'no' => [
         'titulo'  => 'Voc&ecirc; selecionou a op&ccedil;&atilde;o de n&atilde;o se qualificar para a viagem.'
        ,'content' => 'Como indicado na nossa base jur&iacute;dica, significa que&nbsp;pode escolher os produtos do cat&aacute;logo de quil&oacute;metros acumulados.&nbsp;<br>
            <br>Se a qualquer momento optar por mudar&nbsp;basta contactar-nos atrav&eacute;s do seguinte endere&ccedil;o eletr&oacute;nico: <a href="mailto:administracion.bushido@yokohamaiberia.com" target="_blank">administracion.bushido@yokohamaiberia.com</a><br>
            <br>Muito Obrigado.'
        ,'equipo'  => 'Yokohama Equipa Bushido'
     ]
    ,'si' => [
       'titulo' => 'Voc&ecirc; selecionou a op&ccedil;&atilde;o de escolher a viagem.'
      ,'txt1'   => 'Voc&ecirc; tem reservado'
      ,'txt2'   => 'X lugares para a viagem do ano'
      ,'rest'   => 'que pode escolher, tendo em conta as condi&ccedil;&otilde;es que pode encontrar no separador <strong>Reden&ccedil;&atilde;o</strong> dentro de <strong>Meu Yokohama.</strong>.&nbsp;<br>
            <br>Como indicado na nossa base jur&iacute;dica, isto significa que n&atilde;o pode resgatar produtos do cat&aacute;logo de quil&oacute;metros acumulados.&nbsp;<br>
            <br>Se a qualquer momento optar por mudar basta contactar-nos atrav&eacute;s do seguinte endere&ccedil;o eletr&oacute;nico: <a href="mailto:administracion.bushido@yokohamaiberia.com" target="_blank">administracion.bushido@yokohamaiberia.com</a>'
      ,'txtend' => 'Fique ligado! Brevemente vamos revelar o destino.'

    ]
  ]
];
