<?php
  // ESPAÑOL //

  /* INICIO */
  $lang['home'] = [
  	  'buscador'  => [
  			 'titulo'       => 'ENCUENTRA TU PRODUCTO DEL CATÁLOGO'
                        ,'subtitulo'    => 'Busca en nuestro catalogo el producto que necesitas'
  			,'categoria'    => ['label'=>'Categorias','select'=>'Elige una categoria']
  			,'subcategoria' => ['label'=>'Subcategorias']
  			,'familia'      => ['label'=>'Familias']
  			,'boton'        => 'BUSCAR'
      ],
      'viaje' => [
          'titulo'=>'Haz las maletas',
          'subtitulo'=>'Ya tenemos el programa del viaje',
          'boton'=>'Acompáñanos a nuestro destino'
      ]
    ,'secciones' => [
       'entrada' => '“Si os adentráis en el camino inexplorado, al final aparecerán infinitos secretos.”'
     ,'trayectoria' => ['label' => 'DESCUBRE','boton' => 'TU TRAYECTORIA','url'=>base_url().'inicio/trayectoria']
     ]
   ];
  
  $lang['perfil'] = [ 
    'datos' => [
        'titulo' => 'Mi perfil',
        'info' => 'Para realizar cambios en sus datos personales, deberá ponerse en contacto con su comercial.',
        'avatar' =>[
            'cambiar' => 'Cambiar avatar' ,
            'imagen' => 'Medidas de la imagen',
        ],
        'tittabla' => 'Perfil de usuario',
        'tabla' => [
            'nif' => 'NIF/CIF', 
            'razon' => 'Nombre persona de contacto',
            'tfno' => 'Teléfono',
            'email' => 'Email',
            'dir' => 'Dirección',
            'pob' => 'Población',
            'prov' => 'Provincia',
            'cp' => 'Código postal'
        ],
        'pass' =>[
            'entradilla' => 'Si quieres cambiar de contraseña puedes hacerlo a continuación.',
            'cambiar' => 'Cambiar contraseña',
            'new' => 'Nueva contraseña',
            'rep' => 'Repita la contraseña'
        ],
        'messages' => [
            'nocoin' => 'Las contraseñas no coinciden',
            'changePw' => 'Cambió correctamente su contraseña',
            'error' => 'Ha sucedido un error, por favor intentelo más tarde.',
            'avatarok' => 'El avatar ha sido subido.',
            'avatarerrmv' => 'No se pudo mover el archivo.',
            'avatarerrfol' => 'No se pudo crear la carpeta.',
            'avatarerrtype' => 'No es un archivo válido',
            'avatarerrnofile' => 'No se ha subido ningún archivo.'
        ],
        'direcciones' => 'Direcciones',
        'direccion' => 'Dirección de envío (Sólo Bushido)',
        'nodir' => 'Todavía no ha incluido ninguna dirección',
        'nodirdetails' => 'Aquí se mostrarán tus direcciones para recibir tus productos Bushido.'
    ]
  ];
  
  $lang['contacto'] = [
      'titulo' => 'Contacto',
      'entradilla' => 'Rellena los campos para ponerte en contacto con nosotros y explicarnos tu incidencia.',
      'tabla' => [
          'nombre' => 'Nombre',
          'email' => 'Email',
          'tfno' => 'Teléfono',
          'asunto' => 'Asunto',
          'msn' => 'Mensaje'          
      ],
      'butt' => 'Enviar',
      'claim' => 'Yokohama Bushido es un programa de Yokohama Iberia S.A. Para ponerse en contacto directamente con la entidad puede hacerlo a través de <a href="mailto:info.bushido@yokohamaiberia.com">info.bushido@yokohamaiberia.com</a> o a través del teléfono 91 212 43 31',
      'dir' => 'Paseo Doce Estrellas, 2',
      'complete' => '28042-Madrid',
      'tfno' => 'Teléfono: 91 212 43 31',
      'email' => 'Email:<a href:"mailto:info.bushido@yokohamaiberia.com"> info.bushido@yokohamaiberia.com</a>'
  ];

   $lang['viaje'] = [
      'titulo' => 'El gran viaje'
       ,'subtitulo' => '¿Estás preparado para vivir un viaje inolvidable?'
       ,'boton' => 'escoger opción'
       ,'estimado' => 'Estimado Cliente:'
       ,'bienvenida' => '¡Le damos la bienvenida a BUSHIDO 2019! Antes de continuar debe seleccionar entre participar de la promoción Viaje de Incentivos 2019, con destino final a confirmar, o participar del Programa de Incentivos Yokohama Bushido, que le permite acumular KM durante todo el año para canjearlos por regalos de un catálogo que puede consultar en esta misma página.'
       ,'condiciones' => 'Condiciones de participación para el Viaje de Incentivos 2019:'
       ,'dosplazas' => 'Facturación 2 plazas: 100.000€ de facturación donde el valor medio de compra del neumático debe ser superior a 55€. Facturación en la gama PCR (Turismo, furgoneta y 4x4).'
       ,'unaplaza' => 'Facturación 1 plaza: 65.000€ de facturación donde el valor medio de compra del neumático debe ser superior a 55€. Facturación en la gama PCR (Turismo, furgoneta y 4x4).'
       ,'sinviaje' => 'El cliente una vez seleccionado el viaje no accederá a KM Bushido; seleccione 1 o 2 plazas.'
       ,'corte' => 'Se realizará un corte el 1 de julio de 2019, fecha en la cual el cliente deberá haber conseguido al menos el 49% del objetivo (2 plazas = 49.000€ de facturación y 1 plaza = 31.850€ de facturación, ambos con una media mínima de 55€/neumático). En caso de no haber conseguido el objetivo marcado pasará al programa de KM automáticamente sin consulta previa.'
       ,'eleccion' => 'Escoge cuantas plazas quieres o si prefieres participar en nuestro catálogo de Regalos'
       ,'eleccionyear' => 'Escoge el año en el que deseas canjear tu viaje:'
       ,'participar' => 'Participaré en Bushido'
       ,'maletas' => '¿Ya has hecho las maletas?'
       ,'aceptar' => 'Aceptar'
       ,'plaza' => 'Plaza'
       ,'plazas' => 'Plazas'
       ,'tituloviaje' => '¡Nos vamos a '
       ,'destinoviaje' => 'VIETNAM'
       ,'descarga' => 'Descarga aquí el programa del viaje de incentivos Bushido 2019-2020'
       ,'botonprograma' => 'Descargar el programa'
    ];
   
   $lang['cart'] = [
     'index' => [
         'titulo' => 'Carrito',
         'envio' => 'Envío',
         'pago' => 'Pago',
         'pedido' => 'Pedido',
         'pasos' =>[
           'primero' => 'Revisa tu pedido',
           'segundo' => 'Introduce la información de envío',
           'tercero' => 'Completa el pago',
           'cuarto' => 'Finaliza el pedido'
         ],
         'tabla' => [
             'prod' => 'Producto',
             'price' => 'Precio por unidad',
             'qty' => 'Cantidad',
             'total' => 'Total',
             'totalCart' => 'Total carrito',
             'subtotal' => 'Subtotal carrito',
             'gastos' => 'Gastos de envío',
             'gratis' => 'Envío gratis'
         ],
         'backButt' => 'Volver al catálogo',
         'checkButt' => 'Tramitar pedido',
         'empty' => '¡No tiene nada en su carrito!'
     ], 
     'process' => [
         'tablaFact' => [
             'titulo' => 'Dirección de facturación',
             'nombre' => 'nombre de la empresa',
             'dir' => 'dirección',
             'pob' => 'población',
             'email' => 'email',
             'tfno' => 'teléfono'
         ],
         'tablaEnvio' => [
             'titulo' => 'Dirección de envío',
             'same' => 'Los datos de envío son los mismos que de facturación',
             'recev' => 'Nombre y apellidos',
             'obs' => 'observaciones',
             'nombre' => 'nombre de la compañía',
             'dir' => 'dirección',
             'pob' => 'población',
         ],
         'backButt' => 'atrás',
         'checkButt' => 'Guardar Datos'         
     ],
     'resume' => [
         'titulo' => 'Resumen de tu pedido',
         'entradilla' => 'A continuación tienes en detalle el resumen del pedido ha realizar. Por favor, comprueba que todos los datos son correctos. En caso de que no sea así ponte en contacto con <a href="mailto:contacto.bushido@yokohamaiberia.com">contacto.bushido@yokohamaiberia.com</a><br> También recibirás un correo con todos estos datos.',
         'tabla' => [
             'prod' => 'Producto',
             'price' => 'Precio por unidad',
             'qty' => 'Cantidad',
             'total' => 'Total',
             'totalCart' => 'Total carrito',
             'subtotal' => 'Subtotal carrito',
             'gastos' => 'Gastos de envío',
             'gratis' => 'Envío gratis'
         ],
         'details' => 'Detalles del pedido',
         'dirFact' => 'Dirección de facturación',
         'dirEnvio' => 'Dirección de envío',
         'obs' => 'Observaciones para el transportista',
         'backButt' => 'atrás',
         'checkButt' => 'Confirmar pedido'
     ],
     'complete' => [
         'titulo' => 'Pedido recibido',
         'entradilla' => 'Gracias, tu pedido ha sido recibido',
         'tabla' => [
             'prod' => 'Producto',
             'price' => 'Precio por unidad',
             'qty' => 'Cantidad',
             'total' => 'Total',
             'totalCart' => 'Total carrito',
             'subtotal' => 'Subtotal carrito',
             'gastos' => 'Gastos de envío',
             'gratis' => 'Envío gratis'
         ],
         'details' => 'Detalles del pedido',
         'dirFact' => 'Dirección de facturación',
         'dirEnvio' => 'Dirección de envío',
         'obs' => 'Observaciones para el transportista',
         'butt' => 'ver todos mis pedidos'
     ],
     'mailUser' => [
         'asunto' => 'Nuevo pedido en Yokohama Bushido',
         'fecha' => 'Fecha',
         'num' => 'Número de pedido',
         'revisar' => 'Revisar pedido',
         'hola' => 'Hola',
         'texto' => 'A continuación te mostramos un resumen de los datos del pedido. En caso de encontrar algún error o querer realizar alguna modificación en el pedido, ponte en contacto con nosotros a través de <a href="mailto:info.bushido@yokohamaiberia.com">info.bushido@yokohamaiberia.com</a> o llamando al teléfono <strong>915 63 10 11</strong>',
         'pedido' => 'Pedido enviado a:',
         'dirigido' => 'Dirigido a:',
         'info' => 'INFORMACIÓN DEL PEDIDO',
         'qty' => 'Cantidad:',
         'talla' => 'Talla:',
         'color' => 'Color:',
         'total' => 'Total kilómetros:',
         'subtotal' => 'Subtotal',
         'gastos' => 'Gastos de envío',
         'totalKim' => 'Total Kilómetros',
         'obs' => 'Observaciones'
     ]
   ];
           


   $lang['folletos'] = [
      'buscador'  => [
         'titulo' => 'Mira nuestras promociones o si lo prefieres encuéntrala por rango de fechas o tipo de regalo:'
        ,'fecha'  => ['desde' => 'Desde','hasta'=>'Hasta']
        ,'busqueda' => 'Búsqueda por tipo'
        ,'mas'    => 'Leer descripción completa'
        ,'boton'  => 'BUSCAR'
      ]
     ,'botones'   => ['descarga' => 'Descargar','ver'=>'Ver']
   ];

   $lang['estadisticas'] = [
     'titulo' => 'ESTADÍSTICAS'
       ,'content' => [
           'aviso' => 'Aquí podrás ir controlando tus compras de neumáticos Yokohama, aparecerán tan pronto como hagas tu primera compra 2017.'
           ,'aviso1' => 'No se ha encontrado ningún dato para está búsqueda.'
           ,'aviso2' => 'Este cliente no ha entrado en Yokohama Bushido todavía. ¡Ponte en contacto con el!'
           ,'aviso3' => 'Este cliente se conectó por última vez el'
           ,'facturacion' => 'Facturación'
           ,'empresa' => 'Empresa'
           ,'fecha' => 'Fecha'
           ,'factura' => 'Factura'
           ,'familia' => 'Familia'
           ,'CV' => 'CV'
           ,'descripcion' => 'Descripción'
           ,'unidades' => 'Unidades'
           ,'mas' => 'Más'

        ]
      ,'buscador'  => [
          'buscar' => 'BUSCAR'
          ,'fechaI' => 'Fecha Inicio'
          ,'fechaF' => 'Fecha Fin'
          ,'estacion' => 'ESTACION'
          ,'empresa' => 'EMPRESA'
          ,'familia' => 'FAMILIA'
          ,'modelo' => 'MODELO'
          ,'llanta' => 'LLANTA'
          ,'ancho' => 'ANCHO'
          ,'serie' => 'SERIE'
          ,'ic' => 'IC'
          ,'cv' => 'CV'
          ,'enviar' => 'ENVIAR'
        ]
      ,'grafico' => [
          'ntcos' => 'Ntcos.'
         , 'familias' => 'Familias'
          ,'modelos' => 'Modelos'
          ,'Cv'=>'Código de velocidad'
          ,'Ic'=>'Índice de carga'
          ,'llanta'=>'llanta'
          ,'ncv'=>'Neumáticos por '
      ]
    ];

  $lang['pedidos'] = [
	 'titulo' => 'MIS PEDIDOS'
  ,'tituloA' => 'PEDIDOS'
	,'tabla' => [
		 'num'   => 'Pedido'
		,'fecha' => 'Fecha'
		,'direc' => 'Dirección de Envío'
		,'km'    => 'Kilómetros'
		,'mas'   => 'Más'
		,'info'  => 'Información pedido'
		]
	,'boton' => 'Volver'
  ];

  $lang['verpedidos'] = [
	 'titulo'  => 'MI PEDIDO'
  ,'tituloA' => 'PEDIDO'
	,'content' => [
		 'titulo' => 'Mi pedido'
		,'tituloA' => 'Pedido'
		,'fecha'  => 'Fecha'
		,'direc'  => 'Tu pedido fue enviado a'
    ,'direcA'  => 'El pedido fue enviado a'
		,'quien'  => 'Dirigido a (persona de contacto)'
    ,'realizado'  => '¡Enhorabuena tu pedido se ha realizado con éxito!'
		,'info'   => 'Información del pedido'
		,'uds'    => 'Unidades'
		,'import' => 'Importe de los productos'
		,'envio'  => 'Envío y manipulación'
		,'total'  => 'Importe total del producto (iva incluido)'
		,'canje'  => 'Canjeo de puntos'
		,'observ' => 'OBSERVACIONES'
		,'hola'	  => 'Hola'
		,'linfo'  => 'Te informamos que has tramitado correctamente tu pedido, y procederemos a enviarlo en un periodo estimado de entrega de 30-45 días. Si deseas cancelar tu pedido, deberás enviar un correo a <a href="mailto:canjeos.bushido@yokohamaiberia.com" id="pagfinc" title="canjeos.bushido@yokohamaiberia.com" target="_blank">canjeos.bushido@yokohamaiberia.com</a> especificando el número de pedido e informando de las causas de cancelaci&oacute;n.'
		,'donde'  => 'Tu pedido fue enviado a'
		]
	,'volver'  => 'Volver a Mis Pedidos'
  ,'volverA' => 'Volver a Pedidos'
  ];

 $lang['cesta'] = [
	 'titulo'  => 'TU CESTA'
	,'content' => [
		 'prod'   => 'Tus productos'
		,'precio' => 'Precio unitario'
		,'cant'   => 'Cantidad'
		,'impor'  => 'Importe'
    ,'mascesta'  => 'Más'
		,'total'  => 'Total Km'
	]
	,'volver' => 'Volver'
	,'fincom' => 'Finalizar Compra'
  ];

  $lang['fincompra'] = [
	 'titulo'  => 'FINALIZAR COMPRA'
	,'content' => [
		 'datos' => 'DATOS DE ENVÍO'
		,'texto'  => 'Enviaremos el pedido a esta dirección. Por favor comprueba que es correcta.'
		,'direc'  => 'Dirección'
		,'taller' => 'TALLER'
		,'dirig'  => 'Dirigido a (por favor, indíquenos una persona de contacto)'
		,'duda'   => 'Si tienes alguna duda, aclaración u observación sobre tu pedido, comunícanoslo aquí. <strong>Recuerda, si has comprado prendas de ropa o calzado, por favor, indícanos las tallas.</strong>'
		,'observ' => 'Observaciones'
		,'guardar'=> 'Finalizar pedido'
		,'pedido' => 'TU PEDIDO'
		,'resume' => 'Resumen de tu compra'
		,'total'  => 'Total Km'
  ]
  ];

  $lang['catalogo'] = [
     'titulo' => 'Catálogo '
    ,'subtitulo' => 'Catálogo completo'
    ,'filtros' => 'Selecciona tus filtros de búsqueda'
    ,'cats' => 'Categorías'
    ,'item'    => [
       'desc'  => 'Leer descripción completa'
      ,'talla' => 'Elige tu talla: '
      ,'color' => 'Eleige color: '
    ]
    ,'botones' => [ 'ver' => 'Ver','add' => 'Añadir','addmore' => 'Añadir al carrito','volver' => 'Volver', 'vista' => 'Vista rápida']
    ,'content' => [
                'promo' => 'Promoción',
  		 'noviaje' => 'Si quieres volver a acceder al catálogo tienes la opción de renunciar al viaje contactando con nosotros a través del siguiente email:'
    ]
  ];


  $lang['administracion'] = [
        'buscadorTab' =>[
            'admin' => 'Administración de usuarios'
            ,'volver'=>'Volver a administración'
            ,'pais'=>'País'
            ,'esp'=>'España'
            ,'pt'=>'Portugal'
            ,'ad'=>'Andorra'
            ,'buscar'=>'BUSCAR'
            ,'introducir'=>'Introducir sólo valores númericos.'
            ,'cod'=>'Código de cliente'
            ,'email'=>'Email'
            ,'empresa'=>'Empresa'
            ,'filtrar'=>'Filtrar'
            ,'restaurar'=>'RESTAURAR'
        ]
	,'tablaUsuario' => [
            'Kdisponibles' => 'Km disponibles para canjeo'
            ,'Kobtenidos' => 'Km obtenidos'
            ,'ranking' => 'Ranking'
            ,'idioma' => 'Idioma'
            ,'codCliente' => 'Cod. Cliente'
            ,'empresa' => 'Empresa'
            ,'email' => 'Email'
            ,'telefono' => 'Teléfono'
            ,'viaje' => 'Viaje'
            ,'anoV' => 'Año viaje'
            ,'numVisitas' => 'Número de visitas'
            ,'mas' => 'Más datos'
            ,'info' => 'Información empresa'
            ,'punt' => 'Kilómetros'
            ,'ifViaje' => 'Este cliente escogió el viaje, puntuación de carácter orientativo.'
            ,'obtenidos' => 'Obtenidos'
            ,'redimidos' => 'Redimidos'
            ,'disponibles' => 'Disponibles'
            ,'acumulados' => 'Acumulados'
            ,'fecha' => 'Última conexión'
            ,'fechaActu' => 'Actualización de KM'
            ,'fact' => 'Facturación total incluido TBS/OTR'
            ,'factViaje' => 'Facturación total viaje, no incluye TBS/OTR'
            ,'ano' => 'Año'
            ,'zona' => 'Zona Comercial'
            ,
        ]
    ,
      'puntuaciones' => []
    ,'paginas'  => [
         'tabs' => [
           'pag'   => 'Páginas'
          ,'promo' => 'Promociones'
          ,'mark'  => 'Marketing'
          ,'cat'   => 'Categorías'
        ]
        ,'boton'   => 'Volver'
        ,'forms'   => [
           'titulopro' => 'Promoción'
          ,'nombre'    => 'Nombre de la página'
          ,'slug'      => 'URL de la página'
          ,'titulo'    => 'Título de la página'
          ,'tipopro'   => 'Tipo de promoción'
          ,'pais'      => 'País de la promoción'
          ,'imagen'    => 'Imagen'
          ,'fechaini'  => 'Fecha de Inicio'
          ,'fechafin'  => 'Fecha de Fin'
          ,'guardar'   => 'Guardar'
          /* Marketing */
          ,'titulocat' => 'Catálogo'
          ,'titulotit' => 'Título'
          ,'tipocat'   => 'Tipo de catálogo'
          ,'titulocat' => 'Catálogo'
          ,'see'       => 'URL para visualizar'
          ,'download'  => 'URL para descargar'
          ,'activo'    => 'Activo'
          ,'noactivo'  => 'No activo'
          ,'paiscat'   => 'País de publicación'
          /* Paginas*/
          ,'titulopag' => 'Páginas'
          ,'tipopag'   => 'Tipo de página'


          /* Modals Editor */
          ,'link'      => [
             'titulo'  => 'Insertar Enlace'
            ,'tip'     => '<b>Tip:</b> Para insertar un link a un email, comience su URL con "mailto:"'
            ,'url'     => 'URL (dirección)'
            ,'texto'   => 'Texto'
            ,'donde'   => 'Dónde abrir el enlace'
            ,'nueva'   => 'Se abrirá en una pestaña nueva'
            ,'self'    => 'Se abrirá en la página actual'
            ,'insert'  => 'Insertar Link'
            ,'cancel'  => 'Cancelar'
          ]
          ,'imagenes'  => [
             'titulo'  => 'Inserte una o varias imagenes'
            ,'placeho' => 'Inserte uno o más archivos'
            ,'insert'  => 'Insertar Imagen(es)'
          ]
          ,'tablas'    => [
             'titulo'  => 'Insertar tabla'
            ,'filas'   => 'Filas'
            ,'column'  => 'Columnas'
            ,'ancho'   => 'Ancho'
            ,'pixel'   => 'pixels'
            ,'percent' => 'porcentaje'
            ,'insert'  => 'Insertar Tabla'
          ]
        ]
    ]
    ,'catprod' => [
       'titulo' => 'Categorías de contenidos'
      ,'nombre' => 'Nombre de categoría'
      ,'nombrePT' => 'Nombre de categoría portugués'
      ,'tab'    => 'Nombre de la pestaña'
      ,'tabPT'  => 'Nombre de la pestaña portugués'
      ,'tipo'   => 'Tipo de categoría'
      ,'tipom'  => 'Marketing'
      ,'tipop'  => 'Promociones'
    ]
    ,'usuarios' => [
       'idioma'   => 'País'
      ,'cliente'  => 'Cod. Cliente'
      ,'empresa'  => 'Empresa'
      ,'email'    => 'Email'
      ,'telefono' => 'Teléfono'
      ,'visitas'  => 'Número de visitas del cliente'
      ,'viaje'    => 'Viaje (Num.Plazas)'
      ,'mas'      => 'Más datos'
      ,'info'     => 'Direcciones de la empresa'
      ,'noexisten'=>'No existen usuarios en la base de datos'
      ,'buscador' =>[
        'volver' => 'VOLVER'
        ,'buscar' => 'BUSCAR'
        ,'pais' => 'PAÍS'
        ,'espa' => 'ESPAÑA'
        ,'portu' => 'PORTUGAL'
        ,'ando' => 'ANDORRA'
        ,'codigo' => 'Código de Cliente'
        ,'label'=>'Introducir sólo valores numéricos'
        ,'filtrar'=>'FILTRAR'
      ]
    ]
  ];
  
  $lang['trayectoria'] =[
      'camino'  => [
         'tabtitulo' => 'Trayectoria'
        ,'titulo'  => 'Las siete virtudes del código Bushido '
        ,'content' => [
                'gi' => [
                    'traduccion' => 'Justicia',
                    'titulo' => 'Justicia',
                    'texto' => 'Sé honrado en tus tratos con todo el mundo. Cree en la justicia, pero no en la que emana de los demás, sino en la tuya propia.'
                ],
                'yu' => [
                    'traduccion' => 'Valor',
                    'titulo' => 'Valor',
                    'texto' => 'Es arriesgado, es peligroso, pero sin duda también es vivir la vida de forma plena. Reemplaza el miedo por el respeto y la precaución.'
                ],
                'jin' => [
                    'traduccion' => 'Compasión',
                    'titulo' => 'Compasión',
                    'texto' => 'Desarollarás un poder tan grande que deberás usarlo solo para el bien de todos. Debes tener compasión.'
                ],
                'rei' => [
                    'traduccion' => 'Cortesía',
                    'titulo' => 'Cortesía',
                    'texto' => 'Ser un guerrero no justifica la crueldad. No necesitas demostrar tu fuerza a nadie salvo a tí mismo. Se especialmente cortés con tus enemigos, si no, no serás mejor que los animales.'
                ],
                'makoto' => [
                    'traduccion' => 'Sinceridad',
                    'titulo' => 'Sinceridad',
                    'texto' => 'Cuando un samurai dice que hará algo, es como si ya estuviera hecho. "Hablar" y "hacer" son, para un samurai, la misma acción.'
                ],
                'meyo' => [
                    'traduccion' => 'Honor',
                    'titulo' => 'Honor',
                    'texto' => 'Solo tienes un juez de tu propio honor, tú mismo. Las decisiones que tomas y cómo las llevas a cabo son un reflejo de quién eres. Nadie puede ocultarse de sí mismo.'
                ],
                'chu' => [
                    'traduccion' => 'Lealtad',
                    'titulo' => 'Lealtad',
                    'texto' => 'Todas tus acciones son como tus huellas: pueden seguirlas donde quiera que vayas, por ello debes tener cuidado con el camino que sigues.'
                ],
            ]
        ]
    ];

$lang['emails'] = [
   'contacto'  => [
      'title'  => 'Mensaje de contacto | Yokohama Bushido'
     ,'saludo' => 'Hola'
     ,'texto'  => 'Su mensaje ha sido enviado correctamente.<br>
            En breve nos pondremos en contacto&nbsp;con usted.<br>
            <br>
            Muchas gracias.'
     ,'equipo' => 'Att. <span style="color:#D40017">El Equipo de Yokohama Bushido</span>'
     ,'frase'  => 'Sigue el camino del Guerrero y consigue fant&aacute;sticos regalos'
   ]
  ,'pedido'   => [
     'title'  => 'Mi pedido | Yokohama Bushido'
    ,'titulo' => 'Mi Pedido'
    ,'fecha'  => 'Fecha'
    ,'enhor1' => '¡Enhorabuena'
    ,'enhor2' => 'tu pedido se ha realizado con &eacute;xito!'
    ,'quien'  => 'Dirigido a:'
    ,'donde'  => 'Tu pedido fue enviado a la siguiente direcci&oacute;n:'
    ,'info'   => 'Informaci&oacute;n del pedido:'
    ,'uds'    => 'Unidades:'
    ,'import' => 'Importe de los productos:'
    ,'envio'  => 'Env&iacute;o y manipulaci&oacute;n:'
    ,'include'=> 'Incluido'
    ,'total'  => 'Importe total del producto (iva incluido):'
    ,'canje'  => 'Canjeo de puntos:'
    ,'observ' => 'Observaciones:'
    ,'info1'  => 'Te informamos que has tramitado correctamente tu pedido, y procederemos a enviarlo en un periodo estimado de entrega de 30-45 d&iacute;as.'
    ,'info2'  => 'Si deseas cancelar tu pedido, deber&aacute;s enviar un correo a </span><span style="color:#DC0026">canjeos.bushido@yokohamaiberia.com</span><span style="color:#808080"> especificando el n&uacute;mero de pedido e informando de las causas de cancelaci&oacute;n.'
  ]
  ,'viaje'    => [
     'no' => [
         'titulo'  => 'Has elegido la opción de no poder optar al viaje.'
        ,'content' => 'Como se indica en nuestras bases legales, esto conlleva que&nbsp;puedes canjear tus kil&oacute;metros acumulados por productos del cat&aacute;logo.&nbsp;<br>
            <br>Si en alg&uacute;n momento cambias de opini&oacute;n y quieres optar al viaje&nbsp;s&oacute;lo tienes que ponerte en contacto con nosotros a trav&eacute;s del siguiente email: <a href="mailto:administracion.bushido@yokohamaiberia.com" target="_blank">administracion.bushido@yokohamaiberia.com</a><br>
            <br>Muchas gracias.'
        ,'equipo'  => 'El Equipo de Yokohama Bushido'
     ]
    ,'si' => [
       'titulo' => 'Has elegido la opci&oacute;n de optar al viaje.'
      ,'txt1'   => 'Tienes reservada'
      ,'txt2'   => 'plaza(s) para tu viaje para el a&ntilde;o'
      ,'rest'   => 'al que podr&aacute;s optar teniendo en cuenta siempre las condiciones que podr&aacute;s encontrar en la pesta&ntilde;a <strong>Canjeo </strong>dentro de <strong>Mi Yokohama</strong>.&nbsp;<br>
            <br>Como se indica en nuestras bases legales, esto conlleva que no puedes canjear tus kil&oacute;metros acumulados por productos del cat&aacute;logo.&nbsp;<br>
            <br>Si en alg&uacute;n momento cambias de opini&oacute;n s&oacute;lo tienes que ponerte en contacto con nosotros a trav&eacute;s del siguiente email: <a href="mailto:administracion.bushido@yokohamaiberia.com" target="_blank">administracion.bushido@yokohamaiberia.com</a>'
      ,'txtend' => '¡Permanece atento! Muy pronto desvelaremos el destino'

    ]
  ]
];
