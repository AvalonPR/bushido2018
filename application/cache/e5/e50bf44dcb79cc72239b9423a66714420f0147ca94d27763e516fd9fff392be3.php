<?php

/* shop.php */
class __TwigTemplate_e1d909ee0cd8db8ac4d7df486df8ba42789987d1472f3d4c5db699988448e443 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<!-- Page Sub Menu
                ============================================= -->
<div id=\"page-menu\" class=\"hidden-xs hidden-sm\">

    <div id=\"page-menu-wrap\">

        <div class=\"container clearfix\">

            <nav id=\"primary-menu\" class=\"catalogo-completo\">

                <ul>
                    <li id=\"catalogo_menu\" class=\"current mega-menu sub-menu\"><a href=\"#\" style=\"color: #fff;\">";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "subtitulo", array()), "html", null, true);
        echo "<i class=\"icon-angle-down\"></i></a>
                        <div id=\"catalogo_menu_in\" class=\"mega-menu-content clearfix\">
                            ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["jerarquia"]) ? $context["jerarquia"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["j"]) {
            // line 16
            echo "                            <ul class=\"mega-menu-column col-5\">
                                <li><a class=\"menu_white\" href=\"";
            // line 17
            echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
            echo "productos/";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "nombre", array()), "html", null, true);
            echo "</a></li>
                            </ul>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['j'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "                        </div>
                    </li>
                </ul>
                        

            </nav><!-- #primary-menu end -->
        </div>
    </div>

</div><!-- #page-menu end -->

<!-- Page Title
============================================= -->
<section id=\"page-title\" class=\"catalogo_header header\">

    <div class=\"container clearfix\">
        <h1>";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "titulo", array()), "html", null, true);
        echo "</h1>
    </div>

</section><!-- #page-title end -->



<!-- Content
============================================= -->
<section id=\"content\" class=\"catalogo\">
    <!--MIGA DE PAN-->
<div class=\"container\">
    ";
        // line 48
        if (($this->getAttribute((isset($context["breadcumb"]) ? $context["breadcumb"] : null), "categoria", array()) != "")) {
            // line 49
            echo "    <ul class=\"migapan\">
            <li class=\"migapan-item\"><a href=\"";
            // line 50
            echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
            echo "productos\">&nbsp;";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "titulo", array()), "html", null, true);
            echo "&nbsp;</a></li>
            ";
            // line 51
            if (($this->getAttribute((isset($context["breadcumb"]) ? $context["breadcumb"] : null), "categoria", array()) != "")) {
                // line 52
                echo "                ";
                if ((twig_last($this->env, (isset($context["breadcumb"]) ? $context["breadcumb"] : null)) == $this->getAttribute((isset($context["breadcumb"]) ? $context["breadcumb"] : null), "categoria", array()))) {
                    // line 53
                    echo "                <li  class=\"migapan-item active\" aria-current=\"page\" >&nbsp;";
                    echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute((isset($context["breadcumb"]) ? $context["breadcumb"] : null), "categoria", array())), "html", null, true);
                    echo "&nbsp;</li>
                ";
                } else {
                    // line 55
                    echo "                <li class=\"migapan-item\"><a href=\"";
                    echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
                    echo "productos/";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["breadcumb"]) ? $context["breadcumb"] : null), "categoria", array()), "html", null, true);
                    echo "\">&nbsp;";
                    echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute((isset($context["breadcumb"]) ? $context["breadcumb"] : null), "categoria", array())), "html", null, true);
                    echo "&nbsp;</a></li>
                ";
                }
                // line 57
                echo "            ";
            }
            // line 58
            echo "            ";
            if (($this->getAttribute((isset($context["breadcumb"]) ? $context["breadcumb"] : null), "subcategoria", array()) != "")) {
                // line 59
                echo "                ";
                if ((twig_last($this->env, (isset($context["breadcumb"]) ? $context["breadcumb"] : null)) == $this->getAttribute((isset($context["breadcumb"]) ? $context["breadcumb"] : null), "subcategoria", array()))) {
                    // line 60
                    echo "                <li  class=\"migapan-item active\" aria-current=\"page\" >&nbsp;";
                    echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute((isset($context["breadcumb"]) ? $context["breadcumb"] : null), "subcategoria", array())), "html", null, true);
                    echo "&nbsp;</li>
                ";
                } else {
                    // line 62
                    echo "                <li class=\"migapan-item\"><a href=\"";
                    echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
                    echo "productos/";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["breadcumb"]) ? $context["breadcumb"] : null), "categoria", array()), "html", null, true);
                    echo "/";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["breadcumb"]) ? $context["breadcumb"] : null), "subcategoria", array()), "html", null, true);
                    echo "\">&nbsp;";
                    echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute((isset($context["breadcumb"]) ? $context["breadcumb"] : null), "subcategoria", array())), "html", null, true);
                    echo "&nbsp;</a></li>
                ";
                }
                // line 64
                echo "            ";
            }
            // line 65
            echo "            ";
            if (($this->getAttribute((isset($context["breadcumb"]) ? $context["breadcumb"] : null), "familia", array()) != "")) {
                // line 66
                echo "                ";
                if ((twig_last($this->env, (isset($context["breadcumb"]) ? $context["breadcumb"] : null)) == $this->getAttribute((isset($context["breadcumb"]) ? $context["breadcumb"] : null), "familia", array()))) {
                    // line 67
                    echo "                <li  class=\"migapan-item active\" aria-current=\"page\" >&nbsp;";
                    echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute((isset($context["breadcumb"]) ? $context["breadcumb"] : null), "familia", array())), "html", null, true);
                    echo "&nbsp;</li>
                ";
                } else {
                    // line 69
                    echo "                <li  class=\"migapan-item\"><a href=\"";
                    echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
                    echo "productos/";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["breadcumb"]) ? $context["breadcumb"] : null), "categoria", array()), "html", null, true);
                    echo "/";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["breadcumb"]) ? $context["breadcumb"] : null), "subcategoria", array()), "html", null, true);
                    echo "/";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["breadcumb"]) ? $context["breadcumb"] : null), "familia", array()), "html", null, true);
                    echo "\">&nbsp;";
                    echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute((isset($context["breadcumb"]) ? $context["breadcumb"] : null), "familia", array())), "html", null, true);
                    echo "&nbsp;</a></li>
                ";
                }
                // line 71
                echo "            ";
            }
            // line 72
            echo "    </ul>
    ";
        }
        // line 74
        echo "</div>

<!--FIN MIGA DE PAN-->

    <div class=\"content-wrap\">

        <div class=\"container clearfix\">

            <!--Busqueda catálogo mobile-->
            <form action=\"#\" method=\"post\" class=\"nobottommargin hidden-lg hidden-md\">
                <div class=\"bottommargin-sm\">
                    <label for=\"\">";
        // line 85
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "filtros", array()), "html", null, true);
        echo "</label>
                    <select class=\"select-1 form-control\" style=\"width:100%;\" id=\"mobileSelect\">
                                ";
        // line 87
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["jerarquia"]) ? $context["jerarquia"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["j"]) {
            // line 88
            echo "                                ";
            $context["categoria"] = $context["key"];
            // line 89
            echo "                                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["j"], "children", array()));
            foreach ($context['_seq'] as $context["key"] => $context["c"]) {
                // line 90
                echo "                                        ";
                $context["familia"] = $context["key"];
                // line 91
                echo "                                        <optgroup label=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "nombre", array()), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "nombre", array()), "html", null, true);
                echo "\">
                                        <li><a href=\"#\"></a>
                                            <ul>
                                                ";
                // line 94
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["c"], "children", array()));
                foreach ($context['_seq'] as $context["key"] => $context["g"]) {
                    // line 95
                    echo "                                                <option value=\"";
                    echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
                    echo "productos/";
                    echo twig_escape_filter($this->env, (isset($context["categoria"]) ? $context["categoria"] : null), "html", null, true);
                    echo "/";
                    echo twig_escape_filter($this->env, (isset($context["familia"]) ? $context["familia"] : null), "html", null, true);
                    echo "/";
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["g"], "nombre", array()), "html", null, true);
                    echo "</option>
                                                 ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['g'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 97
                echo "                                        </optgroup>
                                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['c'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 99
            echo "                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['j'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 100
        echo "                                </ul>
                    </select>
                </div>
            </form>
            <!-- Aviso no se pueden realizar pedidos
            ============================================= -->
            ";
        // line 106
        if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "tipo", array()) != "C")) {
            // line 107
            echo "            <div class=\"alert style-msg errormsg center\"><i class=\"material-icons\">error</i><b>Comerciales y administradores no pueden canjear productos.</b></div>
            ";
        } elseif (($this->getAttribute(        // line 108
(isset($context["user"]) ? $context["user"] : null), "viaje", array()) != 0)) {
            // line 109
            echo "            <div class=\"alert style-msg errormsg center\"><i class=\"material-icons\">error</i><b>";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "content", array()), "noviaje", array()), "html", null, true);
            echo "</b></div>            
            ";
        }
        // line 111
        echo "            <!-- Post Content
            ============================================= -->



            <div class=\"postcontent nobottommargin col_last\">

                <!-- Shop
                ============================================= -->
                
                <div id=\"bannerPromocion\" class=\"col_full bottommargin-xs\">
                    <a href=\"";
        // line 122
        echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
        echo "productos/promocion/especial/lanzamiento\" target=\"_self\">
                        <img src=\"";
        // line 123
        if (($this->getAttribute((isset($context["userData"]) ? $context["userData"] : null), "nacionalidad", array()) != "PT")) {
            echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
            echo "banners/banner-promocion-catalogo.jpg ";
        } else {
            echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
            echo "banners/banner-promocion-catalogo-pt.jpg";
        }
        echo "\" alt=\"\" class=\"img-responsive\">
                    </a>
                </div>
                <div id=\"shop\" class=\"shop product-3 clearfix\" data-layout=\"fitRows\">
                    ";
        // line 127
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["productos"]) ? $context["productos"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["producto"]) {
            // line 128
            echo "                    <div class=\"product clearfix producto\">
                        <div class=\"product-image product-image-catalog\">
                            ";
            // line 130
            $context["url"] = array(0 => $this->getAttribute($this->getAttribute($this->getAttribute($context["producto"], "parent", array()), "categoria", array()), "slug", array()), 1 => $this->getAttribute($this->getAttribute($this->getAttribute($context["producto"], "parent", array()), "subcategoria", array()), "slug", array()), 2 => $this->getAttribute($this->getAttribute($this->getAttribute($context["producto"], "parent", array()), "familia", array()), "slug", array()), 3 => $this->getAttribute($context["producto"], "slug", array()));
            // line 131
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
            echo "productos/";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["producto"], "parent", array()), "categoria", array()), "slug", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["producto"], "parent", array()), "subcategoria", array()), "slug", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["producto"], "parent", array()), "familia", array()), "slug", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["producto"], "slug", array()), "html", null, true);
            echo "\" class=\"pdt_image\" style=\"background-image: url('https://apps.avalonprplus.com/uploads/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["producto"], "imagen", array()), "html", null, true);
            echo "')\"></a>
                            ";
            // line 132
            if ( !(null === $this->getAttribute($context["producto"], "precioDto", array()))) {
                // line 133
                echo "                            <div class=\"sale-flash\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "content", array()), "promo", array()), "html", null, true);
                echo "</div>
                            ";
            }
            // line 135
            echo "                            <!--alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["producto"], "nombre", array()), "html", null, true);
            echo "\"-->
                            <div class=\"product-overlay\">
                                ";
            // line 137
            if ( !(null === $this->getAttribute($context["producto"], "precioDto", array()))) {
                // line 138
                echo "                                <a ";
                if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "viaje", array()) != 0)) {
                } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array()) < $this->getAttribute($context["producto"], "precioDto", array()))) {
                } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array()) < ((isset($context["cart_total"]) ? $context["cart_total"] : null) + $this->getAttribute($context["producto"], "precioDto", array())))) {
                } else {
                    if (twig_test_empty($this->getAttribute($context["producto"], "atributos", array()))) {
                        echo "onclick=\"addtoCart(";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["producto"], "id", array()), "html", null, true);
                        echo ")\"";
                    } else {
                        echo "onclick=\"sendToProduct(";
                        echo twig_escape_filter($this->env, twig_jsonencode_filter((isset($context["url"]) ? $context["url"] : null)), "html", null, true);
                        echo ")\"";
                    }
                }
                echo " class=\"add-to-cart";
                if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "viaje", array()) != 0)) {
                    echo "-disabled";
                } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array()) < $this->getAttribute($context["producto"], "precioDto", array()))) {
                    echo "-disabled";
                } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array()) < ((isset($context["cart_total"]) ? $context["cart_total"] : null) + $this->getAttribute($context["producto"], "precioDto", array())))) {
                    echo "-disabled";
                }
                echo "\"><i class=\"icon-shopping-cart\"></i><span> ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "botones", array()), "addmore", array()), "html", null, true);
                echo "</span></a>
                                ";
            } else {
                // line 140
                echo "                                <a ";
                if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "viaje", array()) != 0)) {
                } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array()) < $this->getAttribute($context["producto"], "precio", array()))) {
                } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array()) < ((isset($context["cart_total"]) ? $context["cart_total"] : null) + $this->getAttribute($context["producto"], "precio", array())))) {
                } else {
                    if (twig_test_empty($this->getAttribute($context["producto"], "atributos", array()))) {
                        echo "onclick=\"addtoCart(";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["producto"], "id", array()), "html", null, true);
                        echo ")\"";
                    } else {
                        echo "onclick=\"sendToProduct(";
                        echo twig_escape_filter($this->env, twig_jsonencode_filter((isset($context["url"]) ? $context["url"] : null)), "html", null, true);
                        echo ")\"";
                    }
                }
                echo " class=\"add-to-cart";
                if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "viaje", array()) != 0)) {
                    echo "-disabled";
                } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array()) < $this->getAttribute($context["producto"], "precio", array()))) {
                    echo "-disabled";
                } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array()) < ((isset($context["cart_total"]) ? $context["cart_total"] : null) + $this->getAttribute($context["producto"], "precio", array())))) {
                    echo "-disabled";
                }
                echo "\"><i class=\"icon-shopping-cart\"></i><span> ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "botones", array()), "addmore", array()), "html", null, true);
                echo "</span></a>
                                ";
            }
            // line 142
            echo "                                <a class=\"under\" href=\"#\" data-toggle=\"modal\" data-target=\"#";
            echo twig_escape_filter($this->env, $this->getAttribute($context["producto"], "id", array()), "html", null, true);
            echo "\"><i class=\"icon-zoom-in2\"></i><span> ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "botones", array()), "vista", array()), "html", null, true);
            echo "</span></a>
                            </div>
                        </div>
                        <div class=\"product-data col-md-12 col-sm-12 col-xs-12 bottommargin-sm\">
                        
                            <a href=\"";
            // line 147
            echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
            echo "productos/";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["producto"], "parent", array()), "categoria", array()), "slug", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["producto"], "parent", array()), "subcategoria", array()), "slug", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["producto"], "parent", array()), "familia", array()), "slug", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["producto"], "slug", array()), "html", null, true);
            echo "\"><h4>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["producto"], "nombre", array()), "html", null, true);
            echo "</h4></a>
                        
                            <p class=\"precio-home\"><i class=\"fas fa-angle-double-right\"></i>";
            // line 149
            if ( !(null === $this->getAttribute($context["producto"], "precioDto", array()))) {
                echo "<del>";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["producto"], "precio", array()), 0, ",", "."), "html", null, true);
                echo " Km</del> <ins>";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["producto"], "precioDto", array()), 0, ",", "."), "html", null, true);
                echo " Km</ins> ";
            } else {
                echo " ";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["producto"], "precio", array()), 0, ",", "."), "html", null, true);
                echo " Km";
            }
            echo "</p>

                        </div>
                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['producto'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 154
        echo "
                </div><!-- #shop end -->

            </div><!-- .postcontent end -->


            <!--Sidebar
            ============================================= -->
            <div class=\"sidebar nobottommargin hidden-sm hidden-xs\">
                <div class=\"sidebar-widgets-wrap\">

                    <div id=\"catalogoMenuLat\" class=\"widget widget_links clearfix\">

                        <h4 class=\"notopmargin\">";
        // line 167
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "cats", array()), "html", null, true);
        echo "</h4>
                        <nav class=\"nav-tree nobottommargin nav-tabs\">
                            <ul class=\"menu-jerarquia\">
                                ";
        // line 170
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["jerarquia"]) ? $context["jerarquia"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["j"]) {
            // line 171
            echo "                                ";
            $context["categoria"] = $context["key"];
            // line 172
            echo "                                <li";
            if (((isset($context["categoria"]) ? $context["categoria"] : null) == $this->getAttribute((isset($context["breadcumb"]) ? $context["breadcumb"] : null), "categoria", array()))) {
                echo " class=\"active\" ";
            }
            echo "><a href=\"#\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "nombre", array()), "html", null, true);
            echo "</a>
                                    <ul ";
            // line 173
            if (((isset($context["categoria"]) ? $context["categoria"] : null) == $this->getAttribute((isset($context["breadcumb"]) ? $context["breadcumb"] : null), "categoria", array()))) {
                echo "style=\"display: block;\"";
            }
            echo ">
                                        ";
            // line 174
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["j"], "children", array()));
            foreach ($context['_seq'] as $context["key"] => $context["c"]) {
                // line 175
                echo "                                        ";
                $context["familia"] = $context["key"];
                // line 176
                echo "                                        <li";
                if (((isset($context["familia"]) ? $context["familia"] : null) == $this->getAttribute((isset($context["breadcumb"]) ? $context["breadcumb"] : null), "subcategoria", array()))) {
                    echo " class=\"active\" ";
                }
                echo "><a href=\"#\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "nombre", array()), "html", null, true);
                echo "</a>
                                            <ul ";
                // line 177
                if (((isset($context["familia"]) ? $context["familia"] : null) == $this->getAttribute((isset($context["breadcumb"]) ? $context["breadcumb"] : null), "subcategoria", array()))) {
                    echo "style=\"display: block;\"";
                }
                echo ">
                                                ";
                // line 178
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["c"], "children", array()));
                foreach ($context['_seq'] as $context["key"] => $context["g"]) {
                    // line 179
                    echo "                                                <li class=\"";
                    if (($context["key"] == $this->getAttribute((isset($context["breadcumb"]) ? $context["breadcumb"] : null), "familia", array()))) {
                        echo " active ";
                    }
                    echo " side-submenu-catalogo\"><a href=\"";
                    echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
                    echo "productos/";
                    echo twig_escape_filter($this->env, (isset($context["categoria"]) ? $context["categoria"] : null), "html", null, true);
                    echo "/";
                    echo twig_escape_filter($this->env, (isset($context["familia"]) ? $context["familia"] : null), "html", null, true);
                    echo "/";
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["g"], "nombre", array()), "html", null, true);
                    echo "</a></li>
                                                 ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['g'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 181
                echo "                                            </ul>
                                        </li>
                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['c'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 184
            echo "                                    </ul>
                                </li>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['j'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 187
        echo "                            </ul>
                        </nav>

                        </div>

                    </div>
                </div> <!--.sidebar end -->

            </div>

        </div>

    </section><!-- #content end -->

    ";
        // line 201
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["productos"]) ? $context["productos"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["producto"]) {
            // line 202
            echo "    <!-- Modal -->
    <div class=\"modal fade\" id=\"";
            // line 203
            echo twig_escape_filter($this->env, $this->getAttribute($context["producto"], "id", array()), "html", null, true);
            echo "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"Modalproduct-";
            echo twig_escape_filter($this->env, $this->getAttribute($context["producto"], "id", array()), "html", null, true);
            echo "\" aria-hidden=\"true\">

        <div class=\"modal-dialog \">

            <div class=\"modal-body col-sm-12 col-xs-12 producto-modal\">
                <button class=\"close\" type=\"button\" data-dismiss=\"modal\" aria-hidden=\"true\"><b>x</b></button>
                <div class=\"single-product clearfix\">

                    <div class=\"product-modal-title\">
                        <h2>";
            // line 212
            echo twig_escape_filter($this->env, $this->getAttribute($context["producto"], "nombre", array()), "html", null, true);
            echo "</h2>
                    </div>

                    <div class=\"product modal-padding clearfix\">

                        <div class=\"col_half nobottommargin\">
                            <div class=\"product-image\">
                                <a href=\"#\" title=\"#\"><img src=\"https://apps.avalonprplus.com/uploads/";
            // line 219
            echo twig_escape_filter($this->env, $this->getAttribute($context["producto"], "imagen", array()), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["producto"], "nombre", array()), "html", null, true);
            echo "\"></a></div>

                        </div>
                        <div class=\"col_half nobottommargin col_last product-desc\">
                            <div class=\"product-price product-price-modal\">";
            // line 223
            if ( !(null === $this->getAttribute($context["producto"], "precioDto", array()))) {
                echo "<del>";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["producto"], "precio", array()), 0, ",", "."), "html", null, true);
                echo " </del> <ins>";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["producto"], "precioDto", array()), 0, ",", "."), "html", null, true);
                echo " </ins> ";
            } else {
                echo "<ins> ";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["producto"], "precio", array()), 0, ",", "."), "html", null, true);
                echo " </ins> ";
            }
            echo "</div>
                            <div class=\"clear\"></div>
                            <div class=\"line\"></div>
                            
                            <!-- Product Modal - Tallas y colores
                                ============================================= -->
                            
                            <input type=\"hidden\" value=\"";
            // line 230
            echo twig_escape_filter($this->env, $this->getAttribute($context["producto"], "unidades", array()), "html", null, true);
            echo "\" id=\"unidades\">
                   ";
            // line 231
            if ( !twig_test_empty($this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "atributos", array()))) {
                // line 232
                echo "                            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "atributos", array()));
                foreach ($context['_seq'] as $context["k"] => $context["atributo"]) {
                    // line 233
                    echo "                                <!-- Product Single - Quantity & Cart Button
                                ============================================= -->
                                ";
                    // line 235
                    if (($context["k"] == "2")) {
                        // line 236
                        echo "                                <p class=\"\">";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "item", array()), "talla", array()), "html", null, true);
                        echo "</p>
                                <div class=\"col_full talla grid-container\" data-layout=\"fitRows\">
                                    
                                    ";
                        // line 239
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($context["atributo"]);
                        foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
                            // line 240
                            echo "                                    <div class=\"size-item fleft\">
                                        <input onclick=\"selectSize('size";
                            // line 241
                            echo twig_escape_filter($this->env, $context["a"], "html", null, true);
                            echo "')\" data-id=\"\" type=\"button\" class=\"clearfix\" value=\"";
                            echo twig_escape_filter($this->env, $context["a"], "html", null, true);
                            echo "\" id=\"size";
                            echo twig_escape_filter($this->env, $context["a"], "html", null, true);
                            echo "\">
                                        <div class=\"quantity-size\" id=\"divsize";
                            // line 242
                            echo twig_escape_filter($this->env, $context["a"], "html", null, true);
                            echo "\" style=\"display:none;\">
                                            
                                                <input type=\"button\" value=\"-\" class=\"minus size-minus\">
                                                <input type=\"text\" step=\"1\" min=\"1\" value=\"1\" title=\"Qty\" class=\"qty\" name=\"qtysize[]\" size=\"4\" />
                                                <input type=\"button\" value=\"+\" class=\"plus size-plus\">
                                            
                                        </div>
                                    </div>
                                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 251
                        echo "                                </div>
                                <div class=\"line\"></div>
                                ";
                    }
                    // line 254
                    echo "                                ";
                    if (($context["k"] == "1")) {
                        // line 255
                        echo "                                <p class=\"\">";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "item", array()), "color", array()), "html", null, true);
                        echo "</p>
                                <div class=\"col_full color_ grid-container\" data-layout=\"fitRows\">
                                    ";
                        // line 257
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($context["atributo"]);
                        foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
                            // line 258
                            echo "                                    <input type=\"button\" class=\"color-item fleft\" style=\"background-color: #";
                            echo twig_escape_filter($this->env, $context["a"], "html", null, true);
                            echo ";\">
                                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 260
                        echo "                                </div>
                                ";
                    }
                    // line 262
                    echo "                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['k'], $context['atributo'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 263
                echo "                        ";
            }
            // line 264
            echo "                        
                        <!-- Product Modal - Fin Tallas y colores
                                ============================================= -->
                            

                            <!-- Product Single - Quantity & Cart Button
                            ============================================= -->
                                <div class=\"quantity\">
                                    <input type=\"button\" value=\"-\" class=\"minus\">
                                    <input type=\"text\" step=\"1\" min=\"1\" name=\"quantity\" value=\"1\" title=\"Qty\" class=\"qty\" size=\"4\" id=\"quantity";
            // line 273
            echo twig_escape_filter($this->env, $this->getAttribute($context["producto"], "id", array()), "html", null, true);
            echo "\"/>
                                    <input type=\"button\" value=\"+\" class=\"plus\">
                                </div>   
                            ";
            // line 276
            $context["url"] = array(0 => $this->getAttribute($this->getAttribute($this->getAttribute($context["producto"], "parent", array()), "categoria", array()), "slug", array()), 1 => $this->getAttribute($this->getAttribute($this->getAttribute($context["producto"], "parent", array()), "subcategoria", array()), "slug", array()), 2 => $this->getAttribute($this->getAttribute($this->getAttribute($context["producto"], "parent", array()), "familia", array()), "slug", array()), 3 => $this->getAttribute($context["producto"], "slug", array()));
            echo " 
                        ";
            // line 277
            if ( !(null === $this->getAttribute($context["producto"], "precioDto", array()))) {
                // line 278
                echo "                        <button onclick=\"addtoCart(";
                echo twig_escape_filter($this->env, $this->getAttribute($context["producto"], "id", array()), "html", null, true);
                echo ")\" class=\"single_product_add add-to-cart";
                if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "viaje", array()) != 0)) {
                    echo "-disabled";
                } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array()) < $this->getAttribute($context["producto"], "precioDto", array()))) {
                    echo "-disabled";
                } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array()) < ((isset($context["cart_total"]) ? $context["cart_total"] : null) + $this->getAttribute($context["producto"], "precioDto", array())))) {
                    echo "-disabled";
                }
                echo " button nomargin fright\" ";
                if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "viaje", array()) != 0)) {
                    echo "disabled";
                } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array()) < $this->getAttribute($context["producto"], "precioDto", array()))) {
                    echo "disabled";
                } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array()) < ((isset($context["cart_total"]) ? $context["cart_total"] : null) + $this->getAttribute($context["producto"], "precioDto", array())))) {
                    echo "disabled";
                }
                echo " >";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "botones", array()), "addmore", array()), "html", null, true);
                echo "</button>
                        ";
            } else {
                // line 280
                echo "                        <button onclick=\"addtoCart(";
                echo twig_escape_filter($this->env, $this->getAttribute($context["producto"], "id", array()), "html", null, true);
                echo ")\" class=\"single_product_add add-to-cart";
                if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "viaje", array()) != 0)) {
                    echo "-disabled";
                } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array()) < $this->getAttribute($context["producto"], "precio", array()))) {
                    echo "-disabled";
                } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array()) < ((isset($context["cart_total"]) ? $context["cart_total"] : null) + $this->getAttribute($context["producto"], "precio", array())))) {
                    echo "-disabled";
                }
                echo " button nomargin fright\" ";
                if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "viaje", array()) != 0)) {
                    echo "disabled";
                } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array()) < $this->getAttribute($context["producto"], "precio", array()))) {
                    echo "disabled";
                } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array()) < ((isset($context["cart_total"]) ? $context["cart_total"] : null) + $this->getAttribute($context["producto"], "precio", array())))) {
                    echo "disabled";
                }
                echo " >";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "botones", array()), "addmore", array()), "html", null, true);
                echo "</button>
                        ";
            }
            // line 282
            echo "
                            <!-- Product Single - Quantity & Cart Button End -->

                            <div class=\"clear\"></div>
                            <div class=\"line\"></div>
                            <p>";
            // line 287
            echo $this->getAttribute($context["producto"], "descripcion", array());
            echo "</p>
                        </div>
                    </div>

                </div>

            </div>


            <!--  Fin modal -->
        </div>
    </div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['producto'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "shop.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  813 => 287,  806 => 282,  782 => 280,  758 => 278,  756 => 277,  752 => 276,  746 => 273,  735 => 264,  732 => 263,  726 => 262,  722 => 260,  713 => 258,  709 => 257,  703 => 255,  700 => 254,  695 => 251,  680 => 242,  672 => 241,  669 => 240,  665 => 239,  658 => 236,  656 => 235,  652 => 233,  647 => 232,  645 => 231,  641 => 230,  621 => 223,  612 => 219,  602 => 212,  588 => 203,  585 => 202,  581 => 201,  565 => 187,  557 => 184,  549 => 181,  528 => 179,  524 => 178,  518 => 177,  509 => 176,  506 => 175,  502 => 174,  496 => 173,  487 => 172,  484 => 171,  480 => 170,  474 => 167,  459 => 154,  438 => 149,  423 => 147,  412 => 142,  383 => 140,  354 => 138,  352 => 137,  346 => 135,  340 => 133,  338 => 132,  323 => 131,  321 => 130,  317 => 128,  313 => 127,  300 => 123,  296 => 122,  283 => 111,  277 => 109,  275 => 108,  272 => 107,  270 => 106,  262 => 100,  256 => 99,  249 => 97,  232 => 95,  228 => 94,  219 => 91,  216 => 90,  211 => 89,  208 => 88,  204 => 87,  199 => 85,  186 => 74,  182 => 72,  179 => 71,  165 => 69,  159 => 67,  156 => 66,  153 => 65,  150 => 64,  138 => 62,  132 => 60,  129 => 59,  126 => 58,  123 => 57,  113 => 55,  107 => 53,  104 => 52,  102 => 51,  96 => 50,  93 => 49,  91 => 48,  76 => 36,  58 => 20,  45 => 17,  42 => 16,  38 => 15,  33 => 13,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("
<!-- Page Sub Menu
                ============================================= -->
<div id=\"page-menu\" class=\"hidden-xs hidden-sm\">

    <div id=\"page-menu-wrap\">

        <div class=\"container clearfix\">

            <nav id=\"primary-menu\" class=\"catalogo-completo\">

                <ul>
                    <li id=\"catalogo_menu\" class=\"current mega-menu sub-menu\"><a href=\"#\" style=\"color: #fff;\">{{textos.subtitulo}}<i class=\"icon-angle-down\"></i></a>
                        <div id=\"catalogo_menu_in\" class=\"mega-menu-content clearfix\">
                            {%for key, j in jerarquia%}
                            <ul class=\"mega-menu-column col-5\">
                                <li><a class=\"menu_white\" href=\"{{baseurl}}productos/{{key}}\">{{j.nombre}}</a></li>
                            </ul>
                            {%endfor%}
                        </div>
                    </li>
                </ul>
                        

            </nav><!-- #primary-menu end -->
        </div>
    </div>

</div><!-- #page-menu end -->

<!-- Page Title
============================================= -->
<section id=\"page-title\" class=\"catalogo_header header\">

    <div class=\"container clearfix\">
        <h1>{{textos.titulo}}</h1>
    </div>

</section><!-- #page-title end -->



<!-- Content
============================================= -->
<section id=\"content\" class=\"catalogo\">
    <!--MIGA DE PAN-->
<div class=\"container\">
    {%if breadcumb.categoria !='' %}
    <ul class=\"migapan\">
            <li class=\"migapan-item\"><a href=\"{{baseurl}}productos\">&nbsp;{{textos.titulo}}&nbsp;</a></li>
            {% if breadcumb.categoria !='' %}
                {%if breadcumb|last == breadcumb.categoria %}
                <li  class=\"migapan-item active\" aria-current=\"page\" >&nbsp;{{breadcumb.categoria|capitalize}}&nbsp;</li>
                {%else%}
                <li class=\"migapan-item\"><a href=\"{{baseurl}}productos/{{breadcumb.categoria}}\">&nbsp;{{breadcumb.categoria|capitalize}}&nbsp;</a></li>
                {%endif%}
            {%endif%}
            {% if breadcumb.subcategoria !='' %}
                {%if breadcumb|last == breadcumb.subcategoria %}
                <li  class=\"migapan-item active\" aria-current=\"page\" >&nbsp;{{breadcumb.subcategoria|capitalize}}&nbsp;</li>
                {%else%}
                <li class=\"migapan-item\"><a href=\"{{baseurl}}productos/{{breadcumb.categoria}}/{{breadcumb.subcategoria}}\">&nbsp;{{breadcumb.subcategoria|capitalize}}&nbsp;</a></li>
                {%endif%}
            {%endif%}
            {% if breadcumb.familia !='' %}
                {%if breadcumb|last == breadcumb.familia %}
                <li  class=\"migapan-item active\" aria-current=\"page\" >&nbsp;{{breadcumb.familia|capitalize}}&nbsp;</li>
                {%else%}
                <li  class=\"migapan-item\"><a href=\"{{baseurl}}productos/{{breadcumb.categoria}}/{{breadcumb.subcategoria}}/{{breadcumb.familia}}\">&nbsp;{{breadcumb.familia|capitalize}}&nbsp;</a></li>
                {%endif%}
            {%endif%}
    </ul>
    {%endif%}
</div>

<!--FIN MIGA DE PAN-->

    <div class=\"content-wrap\">

        <div class=\"container clearfix\">

            <!--Busqueda catálogo mobile-->
            <form action=\"#\" method=\"post\" class=\"nobottommargin hidden-lg hidden-md\">
                <div class=\"bottommargin-sm\">
                    <label for=\"\">{{textos.filtros}}</label>
                    <select class=\"select-1 form-control\" style=\"width:100%;\" id=\"mobileSelect\">
                                {% for key, j  in jerarquia%}
                                {%set categoria = key %}
                                        {% for key, c in j.children%}
                                        {%set familia = key %}
                                        <optgroup label=\"{{j.nombre}}/{{c.nombre}}\">
                                        <li><a href=\"#\"></a>
                                            <ul>
                                                {% for key, g in c.children%}
                                                <option value=\"{{baseurl}}productos/{{categoria}}/{{familia}}/{{key}}\">{{g.nombre}}</option>
                                                 {%endfor%}
                                        </optgroup>
                                            {%endfor%}
                                    {%endfor%}
                                </ul>
                    </select>
                </div>
            </form>
            <!-- Aviso no se pueden realizar pedidos
            ============================================= -->
            {% if user.tipo != 'C' %}
            <div class=\"alert style-msg errormsg center\"><i class=\"material-icons\">error</i><b>Comerciales y administradores no pueden canjear productos.</b></div>
            {%elseif user.viaje != 0 %}
            <div class=\"alert style-msg errormsg center\"><i class=\"material-icons\">error</i><b>{{textos.content.noviaje}}</b></div>            
            {%endif%}
            <!-- Post Content
            ============================================= -->



            <div class=\"postcontent nobottommargin col_last\">

                <!-- Shop
                ============================================= -->
                
                <div id=\"bannerPromocion\" class=\"col_full bottommargin-xs\">
                    <a href=\"{{baseurl}}productos/promocion/especial/lanzamiento\" target=\"_self\">
                        <img src=\"{%if userData.nacionalidad!='PT' %}{{imgurl}}banners/banner-promocion-catalogo.jpg {%else%}{{imgurl}}banners/banner-promocion-catalogo-pt.jpg{%endif%}\" alt=\"\" class=\"img-responsive\">
                    </a>
                </div>
                <div id=\"shop\" class=\"shop product-3 clearfix\" data-layout=\"fitRows\">
                    {%for producto in productos%}
                    <div class=\"product clearfix producto\">
                        <div class=\"product-image product-image-catalog\">
                            {%set url= [producto.parent.categoria.slug, producto.parent.subcategoria.slug, producto.parent.familia.slug, producto.slug] %}
                            <a href=\"{{baseurl}}productos/{{producto.parent.categoria.slug}}/{{producto.parent.subcategoria.slug}}/{{producto.parent.familia.slug}}/{{producto.slug}}\" class=\"pdt_image\" style=\"background-image: url('https://apps.avalonprplus.com/uploads/{{producto.imagen}}')\"></a>
                            {%if producto.precioDto is not null%}
                            <div class=\"sale-flash\">{{textos.content.promo}}</div>
                            {%endif%}
                            <!--alt=\"{{producto.nombre}}\"-->
                            <div class=\"product-overlay\">
                                {%if producto.precioDto is not null%}
                                <a {%if user.viaje != 0 %}{%elseif user.puntos < producto.precioDto %}{%elseif user.puntos < (cart_total + producto.precioDto) %}{%else%}{%if producto.atributos is empty%}onclick=\"addtoCart({{producto.id}})\"{%else%}onclick=\"sendToProduct({{url|json_encode}})\"{%endif%}{%endif%} class=\"add-to-cart{%if user.viaje !=0 %}-disabled{%elseif user.puntos < producto.precioDto %}-disabled{%elseif user.puntos < (cart_total + producto.precioDto) %}-disabled{%endif%}\"><i class=\"icon-shopping-cart\"></i><span> {{textos.botones.addmore}}</span></a>
                                {%else%}
                                <a {%if user.viaje != 0 %}{%elseif user.puntos < producto.precio %}{%elseif user.puntos < (cart_total + producto.precio) %}{%else%}{%if producto.atributos is empty%}onclick=\"addtoCart({{producto.id}})\"{%else%}onclick=\"sendToProduct({{url|json_encode}})\"{%endif%}{%endif%} class=\"add-to-cart{%if user.viaje !=0 %}-disabled{%elseif user.puntos < producto.precio %}-disabled{%elseif user.puntos < (cart_total + producto.precio) %}-disabled{%endif%}\"><i class=\"icon-shopping-cart\"></i><span> {{textos.botones.addmore}}</span></a>
                                {%endif%}
                                <a class=\"under\" href=\"#\" data-toggle=\"modal\" data-target=\"#{{producto.id}}\"><i class=\"icon-zoom-in2\"></i><span> {{textos.botones.vista}}</span></a>
                            </div>
                        </div>
                        <div class=\"product-data col-md-12 col-sm-12 col-xs-12 bottommargin-sm\">
                        
                            <a href=\"{{baseurl}}productos/{{producto.parent.categoria.slug}}/{{producto.parent.subcategoria.slug}}/{{producto.parent.familia.slug}}/{{producto.slug}}\"><h4>{{producto.nombre}}</h4></a>
                        
                            <p class=\"precio-home\"><i class=\"fas fa-angle-double-right\"></i>{%if producto.precioDto is not null%}<del>{{producto.precio|number_format(0, ',', '.')}} Km</del> <ins>{{producto.precioDto|number_format(0, ',', '.')}} Km</ins> {%else%} {{producto.precio|number_format(0, ',', '.')}} Km{% endif%}</p>

                        </div>
                    </div>
                    {%endfor%}

                </div><!-- #shop end -->

            </div><!-- .postcontent end -->


            <!--Sidebar
            ============================================= -->
            <div class=\"sidebar nobottommargin hidden-sm hidden-xs\">
                <div class=\"sidebar-widgets-wrap\">

                    <div id=\"catalogoMenuLat\" class=\"widget widget_links clearfix\">

                        <h4 class=\"notopmargin\">{{textos.cats}}</h4>
                        <nav class=\"nav-tree nobottommargin nav-tabs\">
                            <ul class=\"menu-jerarquia\">
                                {% for key, j  in jerarquia%}
                                {%set categoria = key %}
                                <li{%if categoria == breadcumb.categoria%} class=\"active\" {%endif%}><a href=\"#\">{{j.nombre}}</a>
                                    <ul {%if categoria == breadcumb.categoria %}style=\"display: block;\"{%endif%}>
                                        {% for key, c in j.children%}
                                        {%set familia = key %}
                                        <li{%if familia == breadcumb.subcategoria%} class=\"active\" {%endif%}><a href=\"#\">{{c.nombre}}</a>
                                            <ul {%if familia == breadcumb.subcategoria %}style=\"display: block;\"{%endif%}>
                                                {% for key, g in c.children%}
                                                <li class=\"{%if key == breadcumb.familia%} active {%endif%} side-submenu-catalogo\"><a href=\"{{baseurl}}productos/{{categoria}}/{{familia}}/{{key}}\">{{g.nombre}}</a></li>
                                                 {%endfor%}
                                            </ul>
                                        </li>
                                        {%endfor%}
                                    </ul>
                                </li>
                                {%endfor%}
                            </ul>
                        </nav>

                        </div>

                    </div>
                </div> <!--.sidebar end -->

            </div>

        </div>

    </section><!-- #content end -->

    {%for producto in productos%}
    <!-- Modal -->
    <div class=\"modal fade\" id=\"{{producto.id}}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"Modalproduct-{{producto.id}}\" aria-hidden=\"true\">

        <div class=\"modal-dialog \">

            <div class=\"modal-body col-sm-12 col-xs-12 producto-modal\">
                <button class=\"close\" type=\"button\" data-dismiss=\"modal\" aria-hidden=\"true\"><b>x</b></button>
                <div class=\"single-product clearfix\">

                    <div class=\"product-modal-title\">
                        <h2>{{producto.nombre}}</h2>
                    </div>

                    <div class=\"product modal-padding clearfix\">

                        <div class=\"col_half nobottommargin\">
                            <div class=\"product-image\">
                                <a href=\"#\" title=\"#\"><img src=\"https://apps.avalonprplus.com/uploads/{{producto.imagen}}\" alt=\"{{producto.nombre}}\"></a></div>

                        </div>
                        <div class=\"col_half nobottommargin col_last product-desc\">
                            <div class=\"product-price product-price-modal\">{%if producto.precioDto is not null%}<del>{{producto.precio|number_format(0, ',', '.')}} </del> <ins>{{producto.precioDto|number_format(0, ',', '.')}} </ins> {%else%}<ins> {{producto.precio|number_format(0, ',', '.')}} </ins> {% endif%}</div>
                            <div class=\"clear\"></div>
                            <div class=\"line\"></div>
                            
                            <!-- Product Modal - Tallas y colores
                                ============================================= -->
                            
                            <input type=\"hidden\" value=\"{{producto.unidades}}\" id=\"unidades\">
                   {%if productos.atributos is not empty%}
                            {%for k,atributo in productos.atributos%}
                                <!-- Product Single - Quantity & Cart Button
                                ============================================= -->
                                {%if k=='2'%}
                                <p class=\"\">{{textos.item.talla}}</p>
                                <div class=\"col_full talla grid-container\" data-layout=\"fitRows\">
                                    
                                    {%for a in atributo%}
                                    <div class=\"size-item fleft\">
                                        <input onclick=\"selectSize('size{{a}}')\" data-id=\"\" type=\"button\" class=\"clearfix\" value=\"{{a}}\" id=\"size{{a}}\">
                                        <div class=\"quantity-size\" id=\"divsize{{a}}\" style=\"display:none;\">
                                            
                                                <input type=\"button\" value=\"-\" class=\"minus size-minus\">
                                                <input type=\"text\" step=\"1\" min=\"1\" value=\"1\" title=\"Qty\" class=\"qty\" name=\"qtysize[]\" size=\"4\" />
                                                <input type=\"button\" value=\"+\" class=\"plus size-plus\">
                                            
                                        </div>
                                    </div>
                                    {%endfor%}
                                </div>
                                <div class=\"line\"></div>
                                {%endif%}
                                {%if k=='1'%}
                                <p class=\"\">{{textos.item.color}}</p>
                                <div class=\"col_full color_ grid-container\" data-layout=\"fitRows\">
                                    {%for a in atributo%}
                                    <input type=\"button\" class=\"color-item fleft\" style=\"background-color: #{{a}};\">
                                    {%endfor%}
                                </div>
                                {%endif%}
                            {%endfor%}
                        {%endif%}
                        
                        <!-- Product Modal - Fin Tallas y colores
                                ============================================= -->
                            

                            <!-- Product Single - Quantity & Cart Button
                            ============================================= -->
                                <div class=\"quantity\">
                                    <input type=\"button\" value=\"-\" class=\"minus\">
                                    <input type=\"text\" step=\"1\" min=\"1\" name=\"quantity\" value=\"1\" title=\"Qty\" class=\"qty\" size=\"4\" id=\"quantity{{producto.id}}\"/>
                                    <input type=\"button\" value=\"+\" class=\"plus\">
                                </div>   
                            {%set url= [producto.parent.categoria.slug, producto.parent.subcategoria.slug, producto.parent.familia.slug, producto.slug] %} 
                        {%if producto.precioDto is not null%}
                        <button onclick=\"addtoCart({{producto.id}})\" class=\"single_product_add add-to-cart{%if user.viaje != 0 %}-disabled{%elseif user.puntos < producto.precioDto %}-disabled{%elseif user.puntos < (cart_total + producto.precioDto) %}-disabled{%endif%} button nomargin fright\" {%if user.viaje != 0 %}disabled{%elseif user.puntos < producto.precioDto %}disabled{%elseif user.puntos < (cart_total + producto.precioDto) %}disabled{%endif%} >{{textos.botones.addmore}}</button>
                        {%else%}
                        <button onclick=\"addtoCart({{producto.id}})\" class=\"single_product_add add-to-cart{%if user.viaje != 0 %}-disabled{%elseif user.puntos < producto.precio %}-disabled{%elseif user.puntos < (cart_total + producto.precio) %}-disabled{%endif%} button nomargin fright\" {%if user.viaje != 0 %}disabled{%elseif user.puntos < producto.precio %}disabled{%elseif user.puntos < (cart_total + producto.precio) %}disabled{%endif%} >{{textos.botones.addmore}}</button>
                        {%endif%}

                            <!-- Product Single - Quantity & Cart Button End -->

                            <div class=\"clear\"></div>
                            <div class=\"line\"></div>
                            <p>{{producto.descripcion|raw}}</p>
                        </div>
                    </div>

                </div>

            </div>


            <!--  Fin modal -->
        </div>
    </div>

    {%endfor%}
", "shop.php", "/Applications/MAMP/htdocs/Bushido2018/application/views/shop.php");
    }
}
