<?php

/* login.php */
class __TwigTemplate_e79d2ad2a1cbfc1b38c3205c8ee4af6e18db5277af05333573c624fcc2dd21b7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<section id=\"content\" class=\"bg-login\">

    <div class=\"content-wrap nopadding\">

        <div class=\"section nopadding nomargin\" style=\"width: 100%; height: 100%; position: absolute; left: 0; top: 0; background: url('images/bg-login.jpg') center center no-repeat; background-size: cover;\"></div>

        <div class=\"section nobg full-screen nopadding nomargin\">
            <div class=\"container vertical-middle divcenter clearfix\">

                <div class=\"row center\">
                    <img src=\"";
        // line 12
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "logo-dark-login.png\" alt=\"Yokohama Bushido\" style=\"margin-bottom: 20px;\">
                </div>
                <!--<div class=\"center dark new-user\">¿Aún no eres usuario? <a href=\"";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
        echo "registro\">Regístrate aquí</a></div>-->
                <div class=\"card\" id=\"login_form\" style=\"padding-bottom: 0px;\">
                    ";
        // line 16
        if (((isset($context["MSIE"]) ? $context["MSIE"] : null) == true)) {
            // line 17
            echo "                    <div class=\"col-md-11 advice divcenter bottommargin-sm\" style=\"background-color: #dddddd\">
                        <p class=\"center\"><strong>Hemos detectado que está usando Microsoft Internet Explorer.</strong><br>Para el uso de esta web recomendamos el uso de: <a href=\"https://www.google.es/chrome/index.html\" target=\"_blank\">Google Chrome</a> o <a href=\"https://www.mozilla.org/es-ES/firefox/new/\" target=\"_blank\">Mozilla Firefox.</a></p>
                    </div>
                    ";
        }
        // line 21
        echo "
                    ";
        // line 22
        if (((isset($context["MSIE"]) ? $context["MSIE"] : null) == false)) {
            // line 23
            echo "                    <div class=\"panel panel-default divcenter noradius noborder\" style=\"max-width: 500px; background-color: rgba(255,255,255,0.93);\">

                        <div class=\"panel-body\">
                            <form id=\"login-form\" name=\"login-form\" class=\"nobottommargin\" action=\"#\" method=\"post\">
                                <h3>Introduzca sus datos <span style=\"font-weight: lighter\">para acceder</span></h3>
                                <div class=\"col_full\">
                                    <label for=\"login-form-username\">Introduzca su email:</label>
                                    <input type=\"text\" id=\"login-form-username\" name=\"user\" value=\"\" class=\"form-control not-dark\" />
                                </div>

                                <div class=\"col_full\">
                                    <label for=\"login-form-password\">Contraseña:</label>
                                    <input type=\"password\" id=\"login-form-password\" name=\"pass\" value=\"\" class=\"form-control not-dark\" />
                                    <input type=\"hidden\" id=\"login-form-pais\" name=\"pais\" value=\"ES\" class=\"form-control not-dark\" />
                                </div>
                                <div class=\"col_full center\">
                                    <button class=\"button button-3d button-black nomargin\"  id=\"login-form-submit\" name=\"login-form-submit\" value=\"login\">Entrar</button>
                                </div></form>

                            <div class=\"col_full toggle toggle-border nobottommargin\">
                                <div class=\"togglet\" id=\"titinfo\"><i class=\"toggle-closed material-icons\">info</i><i class=\"toggle-open material-icons\">info_outline</i>Pinche aquí si no recuerda su contraseña</div>
                                <div class=\"togglec\">
                                    <form id=\"login-form\" name=\"login-form\" class=\"nobottommargin\" action=\"#\" method=\"post\">

                                        <label for=\"login-form-password\">Email:</label>
                                        <input type=\"text\" id=\"email_recuperar\" name=\"email_recuperar\" value=\"\" class=\"form-control not-dark\" />
                                        <input type=\"hidden\" id=\"login-form-pais\" name=\"pais\" value=\"ES\" class=\"form-control not-dark\" />
                                        <br>
                                        <div class=\"col_full center\">
                                            <button class=\"button button-3d button-black send center\" id=\"recuperar-form-sub\" name=\"recuperar-form-sub\" value=\"recuperar\">Enviar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div>
                                <br/>
                            </div>
                        </div>
                    </div>
                    ";
        }
        // line 64
        echo "                </div>
                <div class=\"clearfix\"></div>
                <div class=\"row center dark\"><small style=\"font-size: 11px;\">2019 &copy; Yokohama España, todos los derechos reservados.</small></div>
            </div>

        </div>

    </div>
</section><!-- #content end -->
";
    }

    public function getTemplateName()
    {
        return "login.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 64,  55 => 23,  53 => 22,  50 => 21,  44 => 17,  42 => 16,  37 => 14,  32 => 12,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("
<section id=\"content\" class=\"bg-login\">

    <div class=\"content-wrap nopadding\">

        <div class=\"section nopadding nomargin\" style=\"width: 100%; height: 100%; position: absolute; left: 0; top: 0; background: url('images/bg-login.jpg') center center no-repeat; background-size: cover;\"></div>

        <div class=\"section nobg full-screen nopadding nomargin\">
            <div class=\"container vertical-middle divcenter clearfix\">

                <div class=\"row center\">
                    <img src=\"{{imgurl}}logo-dark-login.png\" alt=\"Yokohama Bushido\" style=\"margin-bottom: 20px;\">
                </div>
                <!--<div class=\"center dark new-user\">¿Aún no eres usuario? <a href=\"{{baseurl}}registro\">Regístrate aquí</a></div>-->
                <div class=\"card\" id=\"login_form\" style=\"padding-bottom: 0px;\">
                    {%if MSIE == true%}
                    <div class=\"col-md-11 advice divcenter bottommargin-sm\" style=\"background-color: #dddddd\">
                        <p class=\"center\"><strong>Hemos detectado que está usando Microsoft Internet Explorer.</strong><br>Para el uso de esta web recomendamos el uso de: <a href=\"https://www.google.es/chrome/index.html\" target=\"_blank\">Google Chrome</a> o <a href=\"https://www.mozilla.org/es-ES/firefox/new/\" target=\"_blank\">Mozilla Firefox.</a></p>
                    </div>
                    {%endif%}

                    {%if MSIE == false%}
                    <div class=\"panel panel-default divcenter noradius noborder\" style=\"max-width: 500px; background-color: rgba(255,255,255,0.93);\">

                        <div class=\"panel-body\">
                            <form id=\"login-form\" name=\"login-form\" class=\"nobottommargin\" action=\"#\" method=\"post\">
                                <h3>Introduzca sus datos <span style=\"font-weight: lighter\">para acceder</span></h3>
                                <div class=\"col_full\">
                                    <label for=\"login-form-username\">Introduzca su email:</label>
                                    <input type=\"text\" id=\"login-form-username\" name=\"user\" value=\"\" class=\"form-control not-dark\" />
                                </div>

                                <div class=\"col_full\">
                                    <label for=\"login-form-password\">Contraseña:</label>
                                    <input type=\"password\" id=\"login-form-password\" name=\"pass\" value=\"\" class=\"form-control not-dark\" />
                                    <input type=\"hidden\" id=\"login-form-pais\" name=\"pais\" value=\"ES\" class=\"form-control not-dark\" />
                                </div>
                                <div class=\"col_full center\">
                                    <button class=\"button button-3d button-black nomargin\"  id=\"login-form-submit\" name=\"login-form-submit\" value=\"login\">Entrar</button>
                                </div></form>

                            <div class=\"col_full toggle toggle-border nobottommargin\">
                                <div class=\"togglet\" id=\"titinfo\"><i class=\"toggle-closed material-icons\">info</i><i class=\"toggle-open material-icons\">info_outline</i>Pinche aquí si no recuerda su contraseña</div>
                                <div class=\"togglec\">
                                    <form id=\"login-form\" name=\"login-form\" class=\"nobottommargin\" action=\"#\" method=\"post\">

                                        <label for=\"login-form-password\">Email:</label>
                                        <input type=\"text\" id=\"email_recuperar\" name=\"email_recuperar\" value=\"\" class=\"form-control not-dark\" />
                                        <input type=\"hidden\" id=\"login-form-pais\" name=\"pais\" value=\"ES\" class=\"form-control not-dark\" />
                                        <br>
                                        <div class=\"col_full center\">
                                            <button class=\"button button-3d button-black send center\" id=\"recuperar-form-sub\" name=\"recuperar-form-sub\" value=\"recuperar\">Enviar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div>
                                <br/>
                            </div>
                        </div>
                    </div>
                    {%endif%}
                </div>
                <div class=\"clearfix\"></div>
                <div class=\"row center dark\"><small style=\"font-size: 11px;\">2019 &copy; Yokohama España, todos los derechos reservados.</small></div>
            </div>

        </div>

    </div>
</section><!-- #content end -->
", "login.php", "C:\\MAMP\\htdocs\\Bushido2018\\application\\views\\login.php");
    }
}
