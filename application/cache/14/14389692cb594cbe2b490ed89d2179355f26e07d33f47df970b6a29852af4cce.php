<?php

/* cart/cart-complete.php */
class __TwigTemplate_7acb55a83e66beb5d55b0812d6381b6e95f1b791513729d6fba1fcd0d04af25d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<!-- Page Title
============================================= -->
<section id=\"page-title\"class=\"carrito_header\">

    <div class=\"container clearfix\">
        <h1 style=\"color: #ffa500\">Carrito</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id=\"content\" class=\"content-cart\">
    
    <!--Process Tabs Mobile-->
    <div id=\"processTabsMobile\" class=\"hidden-lg hidden-md hidden-sm\">
        <ul class=\"process-steps bottommargin clearfix\">
            <li>
                <a id=\"carrito\"></a>
                <h5>";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "titulo", array()), "html", null, true);
        echo "</h5>
            </li>
            <li>
                <a id=\"envio\"></a>
                <h5>";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "envio", array()), "html", null, true);
        echo "</h5>
            </li>
            <li>
                <a id=\"pago\"></a>
                <h5>";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "pago", array()), "html", null, true);
        echo "</h5>
            </li>
            <li class=\"active\">
                <a id=\"pedido\"></a>
                <h5>";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "pedido", array()), "html", null, true);
        echo "</h5>
            </li>
        </ul>
    </div>
    <!--Fin Process Tabs Mobile-->

    <div class=\"content-wrap\">

        <div class=\"container clearfix\">
        <div id=\"processTabs\" class=\"hidden-xs\">
                <ul class=\"process-steps bottommargin clearfix\">
                    <li >
                        <a class=\"i-circled i-bordered\" id=\"carrito\"></a>
                        <h5>";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "pasos", array()), "primero", array()), "html", null, true);
        echo "</h5>
                    </li>
                    <li>
                        <a class=\"i-circled i-bordered\" id=\"envio\"></a>
                        <h5>";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "pasos", array()), "segundo", array()), "html", null, true);
        echo "</h5>
                    </li>
                    <li>
                        <a class=\"i-circled i-bordered\" id=\"pago\"></a>
                        <h5>";
        // line 54
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "pasos", array()), "tercero", array()), "html", null, true);
        echo "</h5>
                    </li>
                    <li class=\"active\">
                        <a class=\"i-circled i-bordered\" id=\"pedido\"></a>
                        <h5>";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "pasos", array()), "cuarto", array()), "html", null, true);
        echo "</h5>
                    </li>
                </ul>
            </div>

            <h3>";
        // line 63
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "complete", array()), "titulo", array()), "html", null, true);
        echo "</h3>
            <div class=\"line-custom\"></div>
            <span class=\"col-md-12 col-xs-12\">";
        // line 65
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "complete", array()), "entradilla", array()), "html", null, true);
        echo "<br><br><br>\t</span>

            <div class=\"table-responsive bottommargin col-md-12 col-xs-12\">
                
                <!-- INICIO TABLA desktop -->
                <table class=\"table cart hidden-xs\">
                    <thead>
                        <tr>
                            <th class=\"cart-product-thumbnail\">&nbsp;</th>
                            <th class=\"cart-product-name\">";
        // line 74
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "complete", array()), "tabla", array()), "prod", array()), "html", null, true);
        echo "</th>
                            <th class=\"cart-product-price\">";
        // line 75
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "complete", array()), "tabla", array()), "price", array()), "html", null, true);
        echo "</th>
                            <th class=\"cart-product-quantity\">";
        // line 76
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "complete", array()), "tabla", array()), "qty", array()), "html", null, true);
        echo "</th>
                            <th class=\"cart-product-subtotal\">";
        // line 77
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "complete", array()), "tabla", array()), "total", array()), "html", null, true);
        echo "</th>
                        </tr>
                    </thead>
                    <tbody>
                        ";
        // line 81
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["cart"]) ? $context["cart"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["item"]) {
            // line 82
            echo "                        <tr class=\"cart_item\">

                            <td class=\"cart-product-thumbnail\">
                                <a href=\"#\"><img width=\"64\" height=\"64\" src=\"";
            // line 85
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "options", array()), "img", array()), "html", null, true);
            echo "\" alt=\"#\"></a>
                            </td>

                            <td class=\"cart-product-name\">
                                <a>";
            // line 89
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "name", array()), "html", null, true);
            echo "</a>
                            ";
            // line 90
            if ( !(null === $this->getAttribute($this->getAttribute($context["item"], "options", array()), "size", array()))) {
                // line 91
                echo "                            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($context["item"], "options", array()), "size", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["talla"]) {
                    // line 92
                    echo "                            </br><span>Talla: ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["talla"], 0, array()), "html", null, true);
                    echo " x ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["talla"], 1, array()), "html", null, true);
                    echo "uds</span>
                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['talla'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 94
                echo "                            ";
            }
            // line 95
            echo "                            
                            ";
            // line 96
            if ( !(null === $this->getAttribute($this->getAttribute($context["item"], "options", array()), "color", array()))) {
                // line 97
                echo "                            </br><span>Color: ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "options", array()), "color", array()), "html", null, true);
                echo "</span>
                            ";
            }
            // line 99
            echo "                            </td>

                            <td class=\"cart-product-price\">
                                <span class=\"amount\">";
            // line 102
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["item"], "price", array()), 0, ",", "."), "html", null, true);
            echo " km.</span>
                            </td>

                            <td class=\"cart-product-quantity\">
                                <div class=\"quantity clearfix\">
                                    <input type=\"text\" name=\"quantity\" value=\"";
            // line 107
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "qty", array()), "html", null, true);
            echo "\" class=\"qty qty-resume\" disabled/>
                                </div>
                            </td>

                            <td class=\"cart-product-subtotal\">
                                <span class=\"amount\">";
            // line 112
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ($this->getAttribute($context["item"], "price", array()) * $this->getAttribute($context["item"], "qty", array())), 0, ",", "."), "html", null, true);
            echo " km.</span>
                            </td>
                        </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 116
        echo "                    </tbody>

                </table>
                <!-- FIN TABLA desktop -->
                
                <!-- INICIO TABLA MOBILE -->
                <table class=\"table cartMobile hidden-lg hidden-md hidden-sm cart_item\" width=\"300\">
                    ";
        // line 123
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["cart"]) ? $context["cart"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["item"]) {
            // line 124
            echo "                    <tbody>
                        <tr>
                            
                            <td class=\"cart-product-thumbnail\" align=\"center\"><a href=\"";
            // line 127
            echo twig_escape_filter($this->env, (isset($context["siteurl"]) ? $context["siteurl"] : null), "html", null, true);
            echo "productos/";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "options", array()), "url", array()), "html", null, true);
            echo "\"><img width=\"64\" height=\"64\" src=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "options", array()), "img", array()), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "name", array()), "html", null, true);
            echo "\"></a></td>
                            <td class=\"cart-product-name\" colspan=\"2\" align=\"left\" width=\"200\">
                                ";
            // line 129
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "complete", array()), "tabla", array()), "prod", array()), "html", null, true);
            echo ":<br>
                                <a href=\"";
            // line 130
            echo twig_escape_filter($this->env, (isset($context["siteurl"]) ? $context["siteurl"] : null), "html", null, true);
            echo "productos/";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "options", array()), "url", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "name", array()), "html", null, true);
            echo "</a>
                                ";
            // line 131
            if ( !(null === $this->getAttribute($this->getAttribute($context["item"], "options", array()), "size", array()))) {
                // line 132
                echo "                                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($context["item"], "options", array()), "size", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["talla"]) {
                    // line 133
                    echo "                                </br><span>Talla: ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["talla"], 0, array()), "html", null, true);
                    echo " x ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["talla"], 1, array()), "html", null, true);
                    echo "uds</span>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['talla'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 135
                echo "                                ";
            }
            // line 136
            echo "
                                ";
            // line 137
            if ( !(null === $this->getAttribute($this->getAttribute($context["item"], "options", array()), "color", array()))) {
                // line 138
                echo "                                </br><span>Color: ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "options", array()), "color", array()), "html", null, true);
                echo "</span>
                                ";
            }
            // line 140
            echo "                            </td>
                        </tr>
                        
                        <tr>
                            <td class=\"cart-product-remove\" align=\"center\">
                                <a class=\"remove\" title=\"Eliminar artículo\" onclick=\"removedatatable('";
            // line 145
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "');\"><i class=\"icon-trash2\"></i></a>
                            </td>

                            <td align=\"left\">Precio por unidad</td>
                            <td scope=\"col\"><span class=\"amount\" align=\"center\" id=\"amount";
            // line 149
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["item"], "price", array()), 0, ",", "."), "html", null, true);
            echo " puntos</span></td>
                            
                        </tr>
                        
                        <tr>
                            <td>&nbsp;</td>
                            <td align=\"left\">";
            // line 155
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "complete", array()), "tabla", array()), "qty", array()), "html", null, true);
            echo ":</td>
                            
                            ";
            // line 157
            if ( !(null === $this->getAttribute($this->getAttribute($context["item"], "options", array()), "size", array()))) {
                // line 158
                echo "                            
                            <td class=\"cart-product-quantity\">
                                <div class=\"quantityCart clearfix\">
                                    <input type=\"text\" name=\"quantity\" data-id=\"";
                // line 161
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "qty", array()), "html", null, true);
                echo "\" class=\"qty\" disabled=\"true\"/>
                                </div>
                            </td>
                            ";
            } else {
                // line 165
                echo "                            <td class=\"cart-product-quantity\">
                                <div class=\"quantityCart clearfix\">
                                    <!--<input type=\"button\" value=\"-\" class=\"minus\">-->
                                    <input type=\"text\" name=\"quantity\" data-id=\"";
                // line 168
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "qty", array()), "html", null, true);
                echo "\" class=\"qty\" disabled='true'/>
                                    <!--<input type=\"button\" value=\"+\" class=\"plus\">-->
                                </div>
                            </td>
                            ";
            }
            // line 173
            echo "
                            
                        </tr>
                        
                        <tr>
                            <td>&nbsp;</td>
                            <td align=\"left\">";
            // line 179
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "complete", array()), "tabla", array()), "total", array()), "html", null, true);
            echo ":</td>
                            <td class=\"cart-product-subtotal\"><span class=\"amount\" id=\"totalAmount";
            // line 180
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ($this->getAttribute($context["item"], "price", array()) * $this->getAttribute($context["item"], "qty", array())), 0, ",", "."), "html", null, true);
            echo " puntos</span></td>
                        </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 183
        echo "                    </tbody>

                </table>
                
                <!-- FIN TABLA MOBILE -->

            </div>

            <div class=\"row clearfix\">

                <div class=\"col-md-6  col-sm-8 col-xs-12 clearfix fright\">
                    <div class=\"table-responsive\">
                        <h4 class=\"crillee\">";
        // line 195
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "complete", array()), "tabla", array()), "total", array()), "html", null, true);
        echo "</h4>

                        <table class=\"table cart\">
                            <tbody>
                                <tr class=\"cart_item\">
                                    <td class=\"cart-product-name\">
                                        <strong>";
        // line 201
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "complete", array()), "tabla", array()), "subtotal", array()), "html", null, true);
        echo "</strong>
                                    </td>

                                    <td style=\"text-align:right;\">
                                        <span class=\"amount\">";
        // line 205
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["cart_total"]) ? $context["cart_total"] : null), 0, ",", "."), "html", null, true);
        echo " puntos</span>
                                    </td>
                                </tr>
                                <tr class=\"cart_item\">
                                    <td class=\"cart-product-name\">
                                        <strong>";
        // line 210
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "complete", array()), "tabla", array()), "gastos", array()), "html", null, true);
        echo "</strong>
                                    </td>

                                    <td style=\"text-align:right;\">
                                        <span class=\"amount\">";
        // line 214
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "complete", array()), "tabla", array()), "gratis", array()), "html", null, true);
        echo "</span>
                                    </td>
                                </tr>
                                <tr class=\"cart_item\">
                                    <td class=\"cart-product-name\">
                                        <strong style=\"font-size: 21px;\">";
        // line 219
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "complete", array()), "tabla", array()), "total", array()), "html", null, true);
        echo "</strong>
                                    </td>

                                    <td style=\"text-align:right;\">
                                        <span class=\"amount color lead\"><strong>";
        // line 223
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["cart_total"]) ? $context["cart_total"] : null), 0, ",", "."), "html", null, true);
        echo " puntos</strong></span>
                                    </td>
                                </tr>
                            </tbody>

                        </table>

                    </div>
                </div>

                <div class=\"col-md-12 clearfix fright\">
                    <div class=\"table\">
                        <h4>";
        // line 235
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "complete", array()), "details", array()), "html", null, true);
        echo "</h4>

                        <table class=\"table cart\">
                            <tbody>
                                <tr class=\"cart_item\">
                                    <td class=\"cart-product-name\">
                                        <strong>";
        // line 241
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "complete", array()), "dirFact", array()), "html", null, true);
        echo ":</strong><br>
                                        <span class=\"amount\">";
        // line 242
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "billingname", array()), "html", null, true);
        echo "</span><br/>
                                        <span class=\"amount\">";
        // line 243
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "billingcompanyname", array()), "html", null, true);
        echo "</span><br/>
                                        <span class=\"amount\">";
        // line 244
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "billingaddress", array()), "html", null, true);
        echo "</span><br/>
                                        <span class=\"amount\">";
        // line 245
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "billingcity", array()), "html", null, true);
        echo "</span><br/>
                                        <span class=\"amount\">";
        // line 246
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "billingemail", array()), "html", null, true);
        echo "</span><br/>
                                        <span class=\"amount\">";
        // line 247
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "billingphone", array()), "html", null, true);
        echo "</span>
                                    </td>
                                </tr>
                                <tr class=\"cart_item\">
                                    <td class=\"cart-product-name\">
                                        <strong>";
        // line 252
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "complete", array()), "dirEnvio", array()), "html", null, true);
        echo ":</strong><br>
                                        <span class=\"amount\">";
        // line 253
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "shippingname", array()), "html", null, true);
        echo "</span><br/>
                                        <span class=\"amount\">";
        // line 254
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "shippingcompanyname", array()), "html", null, true);
        echo "</span><br/>
                                        <span class=\"amount\">";
        // line 255
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "shippingaddress", array()), "html", null, true);
        echo "</span><br/>
                                        <span class=\"amount\">";
        // line 256
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "shippingcity", array()), "html", null, true);
        echo "</span>
                                    </td>
                                </tr>
                                ";
        // line 259
        if (($this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "shippingmessage", array()) != "")) {
            // line 260
            echo "                                <tr class=\"cart_item\">
                                    <td class=\"cart-product-name\">
                                        <strong>";
            // line 262
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "complete", array()), "obs", array()), "html", null, true);
            echo ":</strong><br>
                                        <span class=\"amount\">";
            // line 263
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "shippingmessage", array()), "html", null, true);
            echo "</span>
                                    </td>
                                </tr>
                                ";
        }
        // line 267
        echo "
                            </tbody>

                        </table>

                    </div>
                </div>


            </div>

                <div class=\"col-md-12\">
                    <a href=\"";
        // line 279
        echo twig_escape_filter($this->env, (isset($context["siteurl"]) ? $context["siteurl"] : null), "html", null, true);
        echo "inicio/pedidos\" class=\"button button-3d fright\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "complete", array()), "butt", array()), "html", null, true);
        echo "</a>
                </div>
        </div>

</section><!-- #content end -->

";
    }

    public function getTemplateName()
    {
        return "cart/cart-complete.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  552 => 279,  538 => 267,  531 => 263,  527 => 262,  523 => 260,  521 => 259,  515 => 256,  511 => 255,  507 => 254,  503 => 253,  499 => 252,  491 => 247,  487 => 246,  483 => 245,  479 => 244,  475 => 243,  471 => 242,  467 => 241,  458 => 235,  443 => 223,  436 => 219,  428 => 214,  421 => 210,  413 => 205,  406 => 201,  397 => 195,  383 => 183,  372 => 180,  368 => 179,  360 => 173,  350 => 168,  345 => 165,  336 => 161,  331 => 158,  329 => 157,  324 => 155,  313 => 149,  306 => 145,  299 => 140,  293 => 138,  291 => 137,  288 => 136,  285 => 135,  274 => 133,  269 => 132,  267 => 131,  259 => 130,  255 => 129,  244 => 127,  239 => 124,  235 => 123,  226 => 116,  216 => 112,  208 => 107,  200 => 102,  195 => 99,  189 => 97,  187 => 96,  184 => 95,  181 => 94,  170 => 92,  165 => 91,  163 => 90,  159 => 89,  152 => 85,  147 => 82,  143 => 81,  136 => 77,  132 => 76,  128 => 75,  124 => 74,  112 => 65,  107 => 63,  99 => 58,  92 => 54,  85 => 50,  78 => 46,  62 => 33,  55 => 29,  48 => 25,  41 => 21,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("
<!-- Page Title
============================================= -->
<section id=\"page-title\"class=\"carrito_header\">

    <div class=\"container clearfix\">
        <h1 style=\"color: #ffa500\">Carrito</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id=\"content\" class=\"content-cart\">
    
    <!--Process Tabs Mobile-->
    <div id=\"processTabsMobile\" class=\"hidden-lg hidden-md hidden-sm\">
        <ul class=\"process-steps bottommargin clearfix\">
            <li>
                <a id=\"carrito\"></a>
                <h5>{{textos.index.titulo}}</h5>
            </li>
            <li>
                <a id=\"envio\"></a>
                <h5>{{textos.index.envio}}</h5>
            </li>
            <li>
                <a id=\"pago\"></a>
                <h5>{{textos.index.pago}}</h5>
            </li>
            <li class=\"active\">
                <a id=\"pedido\"></a>
                <h5>{{textos.index.pedido}}</h5>
            </li>
        </ul>
    </div>
    <!--Fin Process Tabs Mobile-->

    <div class=\"content-wrap\">

        <div class=\"container clearfix\">
        <div id=\"processTabs\" class=\"hidden-xs\">
                <ul class=\"process-steps bottommargin clearfix\">
                    <li >
                        <a class=\"i-circled i-bordered\" id=\"carrito\"></a>
                        <h5>{{textos.index.pasos.primero}}</h5>
                    </li>
                    <li>
                        <a class=\"i-circled i-bordered\" id=\"envio\"></a>
                        <h5>{{textos.index.pasos.segundo}}</h5>
                    </li>
                    <li>
                        <a class=\"i-circled i-bordered\" id=\"pago\"></a>
                        <h5>{{textos.index.pasos.tercero}}</h5>
                    </li>
                    <li class=\"active\">
                        <a class=\"i-circled i-bordered\" id=\"pedido\"></a>
                        <h5>{{textos.index.pasos.cuarto}}</h5>
                    </li>
                </ul>
            </div>

            <h3>{{textos.complete.titulo}}</h3>
            <div class=\"line-custom\"></div>
            <span class=\"col-md-12 col-xs-12\">{{textos.complete.entradilla}}<br><br><br>\t</span>

            <div class=\"table-responsive bottommargin col-md-12 col-xs-12\">
                
                <!-- INICIO TABLA desktop -->
                <table class=\"table cart hidden-xs\">
                    <thead>
                        <tr>
                            <th class=\"cart-product-thumbnail\">&nbsp;</th>
                            <th class=\"cart-product-name\">{{textos.complete.tabla.prod}}</th>
                            <th class=\"cart-product-price\">{{textos.complete.tabla.price}}</th>
                            <th class=\"cart-product-quantity\">{{textos.complete.tabla.qty}}</th>
                            <th class=\"cart-product-subtotal\">{{textos.complete.tabla.total}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {%for key, item in cart%}
                        <tr class=\"cart_item\">

                            <td class=\"cart-product-thumbnail\">
                                <a href=\"#\"><img width=\"64\" height=\"64\" src=\"{{item.options.img}}\" alt=\"#\"></a>
                            </td>

                            <td class=\"cart-product-name\">
                                <a>{{item.name}}</a>
                            {% if item.options.size is not null%}
                            {% for talla in item.options.size%}
                            </br><span>Talla: {{talla.0}} x {{talla.1}}uds</span>
                            {%endfor%}
                            {%endif%}
                            
                            {% if item.options.color is not null%}
                            </br><span>Color: {{item.options.color}}</span>
                            {%endif%}
                            </td>

                            <td class=\"cart-product-price\">
                                <span class=\"amount\">{{item.price|number_format(0, ',', '.')}} km.</span>
                            </td>

                            <td class=\"cart-product-quantity\">
                                <div class=\"quantity clearfix\">
                                    <input type=\"text\" name=\"quantity\" value=\"{{item.qty}}\" class=\"qty qty-resume\" disabled/>
                                </div>
                            </td>

                            <td class=\"cart-product-subtotal\">
                                <span class=\"amount\">{{(item.price*item.qty)|number_format(0, ',', '.')}} km.</span>
                            </td>
                        </tr>
                        {%endfor%}
                    </tbody>

                </table>
                <!-- FIN TABLA desktop -->
                
                <!-- INICIO TABLA MOBILE -->
                <table class=\"table cartMobile hidden-lg hidden-md hidden-sm cart_item\" width=\"300\">
                    {%for key, item in cart%}
                    <tbody>
                        <tr>
                            
                            <td class=\"cart-product-thumbnail\" align=\"center\"><a href=\"{{siteurl}}productos/{{item.options.url}}\"><img width=\"64\" height=\"64\" src=\"{{item.options.img}}\" alt=\"{{item.name}}\"></a></td>
                            <td class=\"cart-product-name\" colspan=\"2\" align=\"left\" width=\"200\">
                                {{textos.complete.tabla.prod}}:<br>
                                <a href=\"{{siteurl}}productos/{{item.options.url}}\">{{item.name}}</a>
                                {% if item.options.size is not null%}
                                {% for talla in item.options.size%}
                                </br><span>Talla: {{talla.0}} x {{talla.1}}uds</span>
                                {%endfor%}
                                {%endif%}

                                {% if item.options.color is not null%}
                                </br><span>Color: {{item.options.color}}</span>
                                {%endif%}
                            </td>
                        </tr>
                        
                        <tr>
                            <td class=\"cart-product-remove\" align=\"center\">
                                <a class=\"remove\" title=\"Eliminar artículo\" onclick=\"removedatatable('{{key}}');\"><i class=\"icon-trash2\"></i></a>
                            </td>

                            <td align=\"left\">Precio por unidad</td>
                            <td scope=\"col\"><span class=\"amount\" align=\"center\" id=\"amount{{key}}\">{{item.price|number_format(0, ',', '.')}} puntos</span></td>
                            
                        </tr>
                        
                        <tr>
                            <td>&nbsp;</td>
                            <td align=\"left\">{{textos.complete.tabla.qty}}:</td>
                            
                            {% if item.options.size is not null%}
                            
                            <td class=\"cart-product-quantity\">
                                <div class=\"quantityCart clearfix\">
                                    <input type=\"text\" name=\"quantity\" data-id=\"{{key}}\" value=\"{{item.qty}}\" class=\"qty\" disabled=\"true\"/>
                                </div>
                            </td>
                            {%else%}
                            <td class=\"cart-product-quantity\">
                                <div class=\"quantityCart clearfix\">
                                    <!--<input type=\"button\" value=\"-\" class=\"minus\">-->
                                    <input type=\"text\" name=\"quantity\" data-id=\"{{key}}\" value=\"{{item.qty}}\" class=\"qty\" disabled='true'/>
                                    <!--<input type=\"button\" value=\"+\" class=\"plus\">-->
                                </div>
                            </td>
                            {%endif%}

                            
                        </tr>
                        
                        <tr>
                            <td>&nbsp;</td>
                            <td align=\"left\">{{textos.complete.tabla.total}}:</td>
                            <td class=\"cart-product-subtotal\"><span class=\"amount\" id=\"totalAmount{{key}}\">{{(item.price*item.qty)|number_format(0, ',', '.')}} puntos</span></td>
                        </tr>
                        {%endfor%}
                    </tbody>

                </table>
                
                <!-- FIN TABLA MOBILE -->

            </div>

            <div class=\"row clearfix\">

                <div class=\"col-md-6  col-sm-8 col-xs-12 clearfix fright\">
                    <div class=\"table-responsive\">
                        <h4 class=\"crillee\">{{textos.complete.tabla.total}}</h4>

                        <table class=\"table cart\">
                            <tbody>
                                <tr class=\"cart_item\">
                                    <td class=\"cart-product-name\">
                                        <strong>{{textos.complete.tabla.subtotal}}</strong>
                                    </td>

                                    <td style=\"text-align:right;\">
                                        <span class=\"amount\">{{cart_total|number_format(0, ',', '.')}} puntos</span>
                                    </td>
                                </tr>
                                <tr class=\"cart_item\">
                                    <td class=\"cart-product-name\">
                                        <strong>{{textos.complete.tabla.gastos}}</strong>
                                    </td>

                                    <td style=\"text-align:right;\">
                                        <span class=\"amount\">{{textos.complete.tabla.gratis}}</span>
                                    </td>
                                </tr>
                                <tr class=\"cart_item\">
                                    <td class=\"cart-product-name\">
                                        <strong style=\"font-size: 21px;\">{{textos.complete.tabla.total}}</strong>
                                    </td>

                                    <td style=\"text-align:right;\">
                                        <span class=\"amount color lead\"><strong>{{cart_total|number_format(0, ',', '.')}} puntos</strong></span>
                                    </td>
                                </tr>
                            </tbody>

                        </table>

                    </div>
                </div>

                <div class=\"col-md-12 clearfix fright\">
                    <div class=\"table\">
                        <h4>{{textos.complete.details}}</h4>

                        <table class=\"table cart\">
                            <tbody>
                                <tr class=\"cart_item\">
                                    <td class=\"cart-product-name\">
                                        <strong>{{textos.complete.dirFact}}:</strong><br>
                                        <span class=\"amount\">{{datosEnvio.billingname}}</span><br/>
                                        <span class=\"amount\">{{datosEnvio.billingcompanyname}}</span><br/>
                                        <span class=\"amount\">{{datosEnvio.billingaddress}}</span><br/>
                                        <span class=\"amount\">{{datosEnvio.billingcity}}</span><br/>
                                        <span class=\"amount\">{{datosEnvio.billingemail}}</span><br/>
                                        <span class=\"amount\">{{datosEnvio.billingphone}}</span>
                                    </td>
                                </tr>
                                <tr class=\"cart_item\">
                                    <td class=\"cart-product-name\">
                                        <strong>{{textos.complete.dirEnvio}}:</strong><br>
                                        <span class=\"amount\">{{datosEnvio.shippingname}}</span><br/>
                                        <span class=\"amount\">{{datosEnvio.shippingcompanyname}}</span><br/>
                                        <span class=\"amount\">{{datosEnvio.shippingaddress}}</span><br/>
                                        <span class=\"amount\">{{datosEnvio.shippingcity}}</span>
                                    </td>
                                </tr>
                                {%if datosEnvio.shippingmessage !=''%}
                                <tr class=\"cart_item\">
                                    <td class=\"cart-product-name\">
                                        <strong>{{textos.complete.obs}}:</strong><br>
                                        <span class=\"amount\">{{datosEnvio.shippingmessage}}</span>
                                    </td>
                                </tr>
                                {%endif%}

                            </tbody>

                        </table>

                    </div>
                </div>


            </div>

                <div class=\"col-md-12\">
                    <a href=\"{{siteurl}}inicio/pedidos\" class=\"button button-3d fright\">{{textos.complete.butt}}</a>
                </div>
        </div>

</section><!-- #content end -->

", "cart/cart-complete.php", "/Applications/MAMP/htdocs/Bushido2018/application/views/cart/cart-complete.php");
    }
}
