<?php

/* taller/pedidos_taller_rally.php */
class __TwigTemplate_505354b6011334a1b7c7eb3f7098258c8389694f93899637c732b27fe559968b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- Page Title
============================================= -->
<section id=\"page-title\" class=\"pedidos-taller\">

    <div class=\"container clearfix\">
        <h1>Pedidos 2017</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id=\"content\">

    <div class=\"content-wrap\">

        <div class=\"container clearfix\">
            <div class=\"col_full clearfix\">
            <a class=\"fright button button-rounded button-yellow\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["siteurl"]) ? $context["siteurl"] : null), "html", null, true);
        echo "inicio/pedidos\">Volver a pedidos BestDrive Plus</a>
            </div>

            <!--Tabla pedidos-->
            <div class=\"row\">
                <div class=\"table-responsive\">
                    <table id=\"pedidos_table\" class=\"table table-striped table-bordered\" cellspacing=\"0\" width=\"100%\">
                        <thead>
                            <tr>
                                <th>Número</th>
                                <th>Fecha</th>
                                <th>Usuario</th>
                                <th>Dirección</th>
                                <th>Estado</th>
                                <th>Artículos</th>
                                <th>Km totales</th>
                            </tr>
                        </thead>
                        <tbody>
                            ";
        // line 38
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pedidos"]) ? $context["pedidos"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["pedido"]) {
            // line 39
            echo "                            <tr>
                                <td class=\"click\">";
            // line 40
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["pedido"], "fecha", array()), "Y"), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, $this->getAttribute($context["pedido"], "id", array()), "html", null, true);
            echo "</td>
                                <td>";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute($context["pedido"], "fecha", array()), "html", null, true);
            echo "</td>
                                <td class=\"click\"><a href=\"";
            // line 42
            echo twig_escape_filter($this->env, (isset($context["siteurl"]) ? $context["siteurl"] : null), "html", null, true);
            echo "inicio/pedidos\" target=\"_blank\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["pedido"], "usuario", array()), "html", null, true);
            echo "</td>
                                <td>";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute($context["pedido"], "direccion", array()), "html", null, true);
            echo ", ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["pedido"], "poblacion", array()), "html", null, true);
            echo ", ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["pedido"], "provincia", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["pedido"], "cp", array()), "html", null, true);
            echo "</td>
                                <td>Entregado</td>
                                <td class=\"click center\"><button class=\"button\" data-toggle=\"modal\" data-target=\"#";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($context["pedido"], "id", array()), "html", null, true);
            echo "\"><i class=\"material-icons\">add_circle</i></button></td>
                                <td class=\"click center\" >";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute($context["pedido"], "kmtotal", array()), "html", null, true);
            echo "</td>
                            </tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pedido'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "                        </tbody>
                    </table>
                </div>
            </div>
            <!--Fin tabla pedidos-->
            ";
        // line 54
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pedidos"]) ? $context["pedidos"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["pedido"]) {
            // line 55
            echo "            <!--Modal pedidos-->
            <div class=\"modal fade\" id=\"";
            // line 56
            echo twig_escape_filter($this->env, $this->getAttribute($context["pedido"], "id", array()), "html", null, true);
            echo "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
                <div class=\"modal-dialog\">
                    <div class=\"modal-body\">
                        <div class=\"modal-content\">
                            <div class=\"modal-header\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                                <h4 class=\"modal-title\" id=\"myModalLabel\">Artículos del pedido <span class=\"contitrade\"> ";
            // line 62
            echo twig_escape_filter($this->env, $this->getAttribute($context["pedido"], "id", array()), "html", null, true);
            echo "</span></h4>
                            </div>
                            <div class=\"modal-body\">
                                <table class=\"table table-striped activas table-bordered\">
                                    <thead>
                                        <tr>
                                            <th class=\"col-md-8\">Artículo</th>
                                            <th class=\"col-md-1\">Cantidad</th>
                                            <th class=\"col-md-2\">Puntos</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        ";
            // line 74
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["pedido"], "pedido", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["linea"]) {
                // line 75
                echo "                                        <tr>
                                            <td class=\"click\">";
                // line 76
                echo twig_escape_filter($this->env, $this->getAttribute($context["linea"], "nombre", array()), "html", null, true);
                echo "</td>
                                            <td>";
                // line 77
                echo twig_escape_filter($this->env, $this->getAttribute($context["linea"], "uds", array()), "html", null, true);
                echo "</td>
                                            <td>";
                // line 78
                echo twig_escape_filter($this->env, $this->getAttribute($context["linea"], "kmx", array()), "html", null, true);
                echo "</td>
                                        </tr>
                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['linea'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 81
            echo "                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Fin modal pedidos-->
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pedido'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 91
        echo "            
        </div>

    </div>

</section><!-- #content end -->
";
    }

    public function getTemplateName()
    {
        return "taller/pedidos_taller_rally.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  185 => 91,  170 => 81,  161 => 78,  157 => 77,  153 => 76,  150 => 75,  146 => 74,  131 => 62,  122 => 56,  119 => 55,  115 => 54,  108 => 49,  99 => 46,  95 => 45,  84 => 43,  78 => 42,  74 => 41,  68 => 40,  65 => 39,  61 => 38,  39 => 19,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- Page Title
============================================= -->
<section id=\"page-title\" class=\"pedidos-taller\">

    <div class=\"container clearfix\">
        <h1>Pedidos 2017</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id=\"content\">

    <div class=\"content-wrap\">

        <div class=\"container clearfix\">
            <div class=\"col_full clearfix\">
            <a class=\"fright button button-rounded button-yellow\" href=\"{{siteurl}}inicio/pedidos\">Volver a pedidos BestDrive Plus</a>
            </div>

            <!--Tabla pedidos-->
            <div class=\"row\">
                <div class=\"table-responsive\">
                    <table id=\"pedidos_table\" class=\"table table-striped table-bordered\" cellspacing=\"0\" width=\"100%\">
                        <thead>
                            <tr>
                                <th>Número</th>
                                <th>Fecha</th>
                                <th>Usuario</th>
                                <th>Dirección</th>
                                <th>Estado</th>
                                <th>Artículos</th>
                                <th>Km totales</th>
                            </tr>
                        </thead>
                        <tbody>
                            {% for pedido in pedidos%}
                            <tr>
                                <td class=\"click\">{{ pedido.fecha|date(\"Y\") }}-{{pedido.id}}</td>
                                <td>{{pedido.fecha}}</td>
                                <td class=\"click\"><a href=\"{{siteurl}}inicio/pedidos\" target=\"_blank\">{{pedido.usuario}}</td>
                                <td>{{pedido.direccion}}, {{pedido.poblacion}}, {{pedido.provincia}} - {{pedido.cp}}</td>
                                <td>Entregado</td>
                                <td class=\"click center\"><button class=\"button\" data-toggle=\"modal\" data-target=\"#{{pedido.id}}\"><i class=\"material-icons\">add_circle</i></button></td>
                                <td class=\"click center\" >{{pedido.kmtotal}}</td>
                            </tr>
                            {%endfor%}
                        </tbody>
                    </table>
                </div>
            </div>
            <!--Fin tabla pedidos-->
            {%for pedido in pedidos%}
            <!--Modal pedidos-->
            <div class=\"modal fade\" id=\"{{pedido.id}}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
                <div class=\"modal-dialog\">
                    <div class=\"modal-body\">
                        <div class=\"modal-content\">
                            <div class=\"modal-header\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                                <h4 class=\"modal-title\" id=\"myModalLabel\">Artículos del pedido <span class=\"contitrade\"> {{pedido.id}}</span></h4>
                            </div>
                            <div class=\"modal-body\">
                                <table class=\"table table-striped activas table-bordered\">
                                    <thead>
                                        <tr>
                                            <th class=\"col-md-8\">Artículo</th>
                                            <th class=\"col-md-1\">Cantidad</th>
                                            <th class=\"col-md-2\">Puntos</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {%for linea in pedido.pedido%}
                                        <tr>
                                            <td class=\"click\">{{linea.nombre}}</td>
                                            <td>{{linea.uds}}</td>
                                            <td>{{linea.kmx}}</td>
                                        </tr>
                                        {%endfor%}
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Fin modal pedidos-->
            {%endfor%}
            
        </div>

    </div>

</section><!-- #content end -->
", "taller/pedidos_taller_rally.php", "E:\\Programas\\MAMP\\htdocs\\Bushido2018\\application\\views\\taller\\pedidos_taller_rally.php");
    }
}
