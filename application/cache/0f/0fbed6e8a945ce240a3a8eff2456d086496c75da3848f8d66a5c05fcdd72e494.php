<?php

/* taller/pedidos_taller.php */
class __TwigTemplate_6d9aeeacaeef1db00d0524ce2c24a4b842a29ca045e9578e48d2db1e8e5a9bb3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- Page Title
============================================= -->
<section id=\"page-title\" class=\"header pedidos_header\">

    <div class=\"container clearfix\">
        <h1>Pedidos</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id=\"content\">

    <div class=\"content-wrap\">

        <div class=\"container clearfix\">
            
            <!--Tabla pedidos-->
            <div class=\"row\">
                <div class=\"table-responsive\">
                    <table id=\"pedidos_table\" class=\"table table-striped table-bordered\" cellspacing=\"0\" width=\"100%\">
                        <thead>
                            <tr>
                                <th>Número</th>
                                <th>Fecha</th>
                                <th>Destinatario</th>
                                <th>Dirección</th>
                                <th>Estado</th>
                                <th>Artículos</th>
                                <th>Puntos totales</th>
                            </tr>
                        </thead>
                        <tbody>
                            ";
        // line 35
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pedidos"]) ? $context["pedidos"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["pedido"]) {
            // line 36
            echo "                            <tr>
                                <td class=\"click\">";
            // line 37
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["pedido"], "fecha", array()), "Y"), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, $this->getAttribute($context["pedido"], "idpedidos", array()), "html", null, true);
            echo "</td>
                                <td>";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($context["pedido"], "fecha", array()), "html", null, true);
            echo "</td>
                                <td class=\"click\"><a data-toggle=\"modal\" href=\"#shipping";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($context["pedido"], "idpedidos", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["pedido"], "datosEnvio", array()), "shippingname", array()), "html", null, true);
            echo "</td>
                                <td>";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["pedido"], "datosEnvio", array()), "shippingaddress", array()), "html", null, true);
            echo "</td>
                                <td class=\"center\">";
            // line 41
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute($context["pedido"], "estado", array())), "html", null, true);
            echo "</td>
                                <td class=\"click center\"><button class=\"button plus-table\" data-toggle=\"modal\" data-target=\"#producto";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute($context["pedido"], "idpedidos", array()), "html", null, true);
            echo "\"><i class=\"material-icons\">add_circle</i></button></td>
                                <td class=\"click center\" >";
            // line 43
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["pedido"], "kmtotal", array()), 0, ",", "."), "html", null, true);
            echo "</td>
                            </tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pedido'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "                        </tbody>
                    </table>
                </div>
            </div>
            <!--Fin tabla pedidos-->
            ";
        // line 51
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pedidos"]) ? $context["pedidos"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["pedido"]) {
            // line 52
            echo "            <!--Modal pedidos-->
            <div class=\"modal fade\" id=\"producto";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($context["pedido"], "idpedidos", array()), "html", null, true);
            echo "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
                <div class=\"modal-dialog\">
                    <div class=\"modal-body\">
                        <div class=\"modal-content\">
                            <div class=\"modal-header\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                                <h4 class=\"modal-title\" id=\"myModalLabel\">Artículos del pedido <span class=\"contitrade\"> ";
            // line 59
            echo twig_escape_filter($this->env, $this->getAttribute($context["pedido"], "idpedidos", array()), "html", null, true);
            echo "</span></h4>
                            </div>
                            <div class=\"modal-body\">
                                <table class=\"table table-striped activas table-bordered\">
                                    <thead>
                                        <tr>
                                            <th class=\"col-md-8\">Artículo</th>
                                            <th class=\"col-md-1\">Cantidad</th>
                                            <th class=\"col-md-2\">Puntos</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        ";
            // line 71
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["pedido"], "pedido", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["linea"]) {
                // line 72
                echo "                                        <tr>
                                            <td class=\"click\">";
                // line 73
                echo twig_escape_filter($this->env, $this->getAttribute($context["linea"], "name", array()), "html", null, true);
                echo "
                                        ";
                // line 74
                if ( !(null === $this->getAttribute($this->getAttribute($context["linea"], "options", array()), "size", array()))) {
                    // line 75
                    echo "                                        ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($context["linea"], "options", array()), "size", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["talla"]) {
                        // line 76
                        echo "                                        </br><span>Talla: ";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["talla"], 0, array()), "html", null, true);
                        echo " x ";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["talla"], 1, array()), "html", null, true);
                        echo "uds</span>
                                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['talla'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 77
                    echo "</td>
                                        ";
                }
                // line 79
                echo "                                            <td>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["linea"], "qty", array()), "html", null, true);
                echo "</td>
                                            <td>";
                // line 80
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["linea"], "price", array()), 0, ",", "."), "html", null, true);
                echo "</td>
                                        </tr>

                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['linea'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 84
            echo "                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Fin modal pedidos-->
            
            <!--Inicio Modal Datos de envío-->
             <div class=\"modal fade\" id=\"shipping";
            // line 95
            echo twig_escape_filter($this->env, $this->getAttribute($context["pedido"], "idpedidos", array()), "html", null, true);
            echo "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
                <div class=\"modal-dialog\">
                    <div class=\"modal-body\">
                        <div class=\"modal-content\">
                            <div class=\"modal-header\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                                <h3 class=\"modal-title\" id=\"myModalLabel\">Datos de envío <span class=\"contitrade\">pedido ";
            // line 101
            echo twig_escape_filter($this->env, $this->getAttribute($context["pedido"], "idpedidos", array()), "html", null, true);
            echo "</span></h3>
                            </div>
                            <div class=\"modal-body\">
                                <table class=\"table table-striped activas table-bordered\">
                                    <thead>
                                        <tr>
                                            <th class=\"col-half crillee\">Datos facturación</th>
                                            <th class=\"col-half crillee\">Datos envío</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <tr>
                                            <td>";
            // line 115
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["pedido"], "datosEnvio", array()), "billingname", array()), "html", null, true);
            echo "</td>
                                            <td>";
            // line 116
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["pedido"], "datosEnvio", array()), "shippingname", array()), "html", null, true);
            echo "</td>
                                        </tr>

                                        <tr>
                                            <td>";
            // line 120
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["pedido"], "datosEnvio", array()), "billingcompanyname", array()), "html", null, true);
            echo "</td>
                                            <td>";
            // line 121
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["pedido"], "datosEnvio", array()), "shippingcompanyname", array()), "html", null, true);
            echo "</td>
                                        </tr>
                                        
                                        <tr>
                                            <td>";
            // line 125
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["pedido"], "datosEnvio", array()), "billingaddress", array()), "html", null, true);
            echo "</td>
                                            <td>";
            // line 126
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["pedido"], "datosEnvio", array()), "shippingaddress", array()), "html", null, true);
            echo "</td>
                                        </tr>
                                        
                                        <tr>
                                            <td>";
            // line 130
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["pedido"], "datosEnvio", array()), "billingcity", array()), "html", null, true);
            echo "</td>
                                            <td>";
            // line 131
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["pedido"], "datosEnvio", array()), "shippingcity", array()), "html", null, true);
            echo "</td>
                                        </tr>

                                        
                                    </tbody>
                                </table>
                                ";
            // line 137
            if (($this->getAttribute($this->getAttribute($context["pedido"], "datosEnvio", array()), "shippingmessage", array()) != "")) {
                // line 138
                echo "                                <table class=\"table table-striped activas table-bordered col-half\">
                                    <thead>
                                        <th class=\"crillee\">Observaciones</th>
                                    </thead>
                                    <tbody>
                                        <td>";
                // line 143
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["pedido"], "datosEnvio", array()), "shippingmessage", array()), "html", null, true);
                echo "</td>
                                    </tbody>
                                </table>
                                ";
            }
            // line 147
            echo "
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Fin modal datos de envío-->
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pedido'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 155
        echo "            <div class=\"line\"></div>
           
            <div class=\"\">
                <a class=\"fright button button-rounded button-dark\" href=\"";
        // line 158
        echo twig_escape_filter($this->env, (isset($context["siteurl"]) ? $context["siteurl"] : null), "html", null, true);
        echo "inicio/pedidos_2017\">Ver pedidos 2017</a>
            </div>
        </div>

    </div>

</section><!-- #content end -->";
    }

    public function getTemplateName()
    {
        return "taller/pedidos_taller.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  304 => 158,  299 => 155,  286 => 147,  279 => 143,  272 => 138,  270 => 137,  261 => 131,  257 => 130,  250 => 126,  246 => 125,  239 => 121,  235 => 120,  228 => 116,  224 => 115,  207 => 101,  198 => 95,  185 => 84,  175 => 80,  170 => 79,  166 => 77,  155 => 76,  150 => 75,  148 => 74,  144 => 73,  141 => 72,  137 => 71,  122 => 59,  113 => 53,  110 => 52,  106 => 51,  99 => 46,  90 => 43,  86 => 42,  82 => 41,  78 => 40,  72 => 39,  68 => 38,  62 => 37,  59 => 36,  55 => 35,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- Page Title
============================================= -->
<section id=\"page-title\" class=\"header pedidos_header\">

    <div class=\"container clearfix\">
        <h1>Pedidos</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id=\"content\">

    <div class=\"content-wrap\">

        <div class=\"container clearfix\">
            
            <!--Tabla pedidos-->
            <div class=\"row\">
                <div class=\"table-responsive\">
                    <table id=\"pedidos_table\" class=\"table table-striped table-bordered\" cellspacing=\"0\" width=\"100%\">
                        <thead>
                            <tr>
                                <th>Número</th>
                                <th>Fecha</th>
                                <th>Destinatario</th>
                                <th>Dirección</th>
                                <th>Estado</th>
                                <th>Artículos</th>
                                <th>Puntos totales</th>
                            </tr>
                        </thead>
                        <tbody>
                            {% for pedido in pedidos%}
                            <tr>
                                <td class=\"click\">{{ pedido.fecha|date(\"Y\") }}-{{pedido.idpedidos}}</td>
                                <td>{{pedido.fecha}}</td>
                                <td class=\"click\"><a data-toggle=\"modal\" href=\"#shipping{{pedido.idpedidos}}\" >{{pedido.datosEnvio.shippingname}}</td>
                                <td>{{pedido.datosEnvio.shippingaddress}}</td>
                                <td class=\"center\">{{pedido.estado|capitalize}}</td>
                                <td class=\"click center\"><button class=\"button plus-table\" data-toggle=\"modal\" data-target=\"#producto{{pedido.idpedidos}}\"><i class=\"material-icons\">add_circle</i></button></td>
                                <td class=\"click center\" >{{pedido.kmtotal|number_format(0,',','.')}}</td>
                            </tr>
                            {%endfor%}
                        </tbody>
                    </table>
                </div>
            </div>
            <!--Fin tabla pedidos-->
            {%for pedido in pedidos%}
            <!--Modal pedidos-->
            <div class=\"modal fade\" id=\"producto{{pedido.idpedidos}}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
                <div class=\"modal-dialog\">
                    <div class=\"modal-body\">
                        <div class=\"modal-content\">
                            <div class=\"modal-header\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                                <h4 class=\"modal-title\" id=\"myModalLabel\">Artículos del pedido <span class=\"contitrade\"> {{pedido.idpedidos}}</span></h4>
                            </div>
                            <div class=\"modal-body\">
                                <table class=\"table table-striped activas table-bordered\">
                                    <thead>
                                        <tr>
                                            <th class=\"col-md-8\">Artículo</th>
                                            <th class=\"col-md-1\">Cantidad</th>
                                            <th class=\"col-md-2\">Puntos</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {%for linea in pedido.pedido%}
                                        <tr>
                                            <td class=\"click\">{{linea.name}}
                                        {% if linea.options.size is not null%}
                                        {% for talla in linea.options.size%}
                                        </br><span>Talla: {{talla.0}} x {{talla.1}}uds</span>
                                        {%endfor%}</td>
                                        {%endif%}
                                            <td>{{linea.qty}}</td>
                                            <td>{{linea.price|number_format(0,',','.')}}</td>
                                        </tr>

                                        {%endfor%}
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Fin modal pedidos-->
            
            <!--Inicio Modal Datos de envío-->
             <div class=\"modal fade\" id=\"shipping{{pedido.idpedidos}}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
                <div class=\"modal-dialog\">
                    <div class=\"modal-body\">
                        <div class=\"modal-content\">
                            <div class=\"modal-header\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                                <h3 class=\"modal-title\" id=\"myModalLabel\">Datos de envío <span class=\"contitrade\">pedido {{pedido.idpedidos}}</span></h3>
                            </div>
                            <div class=\"modal-body\">
                                <table class=\"table table-striped activas table-bordered\">
                                    <thead>
                                        <tr>
                                            <th class=\"col-half crillee\">Datos facturación</th>
                                            <th class=\"col-half crillee\">Datos envío</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <tr>
                                            <td>{{pedido.datosEnvio.billingname}}</td>
                                            <td>{{pedido.datosEnvio.shippingname}}</td>
                                        </tr>

                                        <tr>
                                            <td>{{pedido.datosEnvio.billingcompanyname}}</td>
                                            <td>{{pedido.datosEnvio.shippingcompanyname}}</td>
                                        </tr>
                                        
                                        <tr>
                                            <td>{{pedido.datosEnvio.billingaddress}}</td>
                                            <td>{{pedido.datosEnvio.shippingaddress}}</td>
                                        </tr>
                                        
                                        <tr>
                                            <td>{{pedido.datosEnvio.billingcity}}</td>
                                            <td>{{pedido.datosEnvio.shippingcity}}</td>
                                        </tr>

                                        
                                    </tbody>
                                </table>
                                {%if pedido.datosEnvio.shippingmessage !=''%}
                                <table class=\"table table-striped activas table-bordered col-half\">
                                    <thead>
                                        <th class=\"crillee\">Observaciones</th>
                                    </thead>
                                    <tbody>
                                        <td>{{pedido.datosEnvio.shippingmessage}}</td>
                                    </tbody>
                                </table>
                                {%endif%}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Fin modal datos de envío-->
            {%endfor%}
            <div class=\"line\"></div>
           
            <div class=\"\">
                <a class=\"fright button button-rounded button-dark\" href=\"{{siteurl}}inicio/pedidos_2017\">Ver pedidos 2017</a>
            </div>
        </div>

    </div>

</section><!-- #content end -->", "taller/pedidos_taller.php", "E:\\Programas\\MAMP\\htdocs\\Bushido2018\\application\\views\\taller\\pedidos_taller.php");
    }
}
