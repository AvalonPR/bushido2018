<?php

/* footer.php */
class __TwigTemplate_b92e397e90691d0ad1dc2cd6f8edb7540485ded79b4d00bd834b818f43427ff4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "logged_in", array())) {
            // line 2
            echo "<!-- Footer
============================================= -->
<footer id=\"footer\" class=\"dark\">    
    <div id=\"cookie_directive_container\" class=\"container\" style=\"display: none\">
            <nav class=\"navbar navbar-default navbar-fixed-bottom\" id=\"cookiesf\">

                <div class=\"container \">
                <div class=\"navbar-inner navbar-content-center\" id=\"cookie_accept\">

                    
                    <p id=\"cookdesk\">
                     Solicitamos su permiso para obtener datos estadísticos de su navegación en esta web, en cumplimiento del Real 
                     Decreto-ley 13/2012. Si continúa navegando consideramos que acepta el uso de cookies. <a target=\"_blank\" href=\"inicio/legal/\" style=\"font-size: 120%\">Más información.</a>
                    </p>
                <div class=\"closecok\"><a href=\"#\"><i class=\"icon-line-cross\"></i></a></div>
                    <br>

                </div>
              </div>

            </nav>
        </div>   
    <div class=\"imagen-footer\">
    <div class=\"container \">

        <!-- Footer Widgets
        ============================================= -->
        <div class=\"footer-widgets-wrap clearfix\">

            

                <div class=\"col_one_third\">

                    <div class=\"widget clearfix\">

                        <img src=\"";
            // line 37
            echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
            echo "logo-footer-white.png\" alt=\"\" class=\"footer-logo\">

                    </div>
                </div>
                

                <div class=\"col_one_third\">

                    <div class=\"widget widget_links clearfix\">

                        <h4>Páginas</h4>

                        <ul>
                            ";
            // line 50
            if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "tipo", array()) == "C")) {
                // line 51
                echo "                            <li><a href=\"";
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "inicio/viaje\">";
                echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "viaje", array())), "html", null, true);
                echo "</a></li>
                            <li><a href=\"";
                // line 52
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "productos\">";
                echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "catalogo", array())), "html", null, true);
                echo "</a></li>
                            <li><a href=\"";
                // line 53
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "inicio/trayectoria\">";
                echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "trayectoria", array())), "html", null, true);
                echo "</a></li>
                            <li><a href=\"";
                // line 54
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "inicio/bases\">";
                echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "programa", array())), "html", null, true);
                echo "</a></li>
                            ";
            }
            // line 56
            echo "                            <!--<li><a href=\"";
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "tipo", array()) == "1")) {
                echo "inicio/";
            } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "tipo", array()) == "2")) {
                echo "comercial/";
            } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "tipo", array()) == "3")) {
                echo "admin/";
            }
            echo "estadisticas\">Mis Estadísticas</a></li>-->
                            <li><a href=\"";
            // line 57
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "tipo", array()) == "C")) {
                echo "inicio/";
            } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "tipo", array()) == "S")) {
                echo "comercial/";
            } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "tipo", array()) == "A")) {
                echo "admin/";
            }
            echo "perfil\">";
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "perfil", array())), "html", null, true);
            echo "</a></li>
                        </ul>

                    </div>

                </div>

                

            

            <div class=\"col_one_third col_last\">

                
                    <h5>Sede:</h5>
                    <p>";
            // line 72
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["footer"]) ? $context["footer"] : null), "direccion", array()), "html", null, true);
            echo "<br>
                    ";
            // line 73
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["footer"]) ? $context["footer"] : null), "direccion2", array()), "html", null, true);
            echo " <br>
                    ";
            // line 74
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["footer"]) ? $context["footer"] : null), "direccion3", array()), "html", null, true);
            echo "</p>

                    <p>Teléfono: ";
            // line 76
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["footer"]) ? $context["footer"] : null), "tfno", array()), "html", null, true);
            echo "<br>
                    Email: info.bushido@yokohamaiberia.com</p>
                    
                

            </div>

        </div><!-- .footer-widgets-wrap end -->

    </div>
    </div>
    <!-- Copyrights
    ============================================= -->
    <div id=\"copyrights\">

        

            <div class=\"center\">
                &copy; Yokohama Iberia ";
            // line 94
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
            echo "<br>
                <div class=\"copyright-links\"><a href=\"";
            // line 95
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            echo "inicio/bases\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["footer"]) ? $context["footer"] : null), "aviso", array()), "html", null, true);
            echo "</a> / <a href=\"";
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            echo "inicio/privacidad\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["footer"]) ? $context["footer"] : null), "privacidad", array()), "html", null, true);
            echo "</a> / <a href=\"";
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            echo "inicio/legal\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["footer"]) ? $context["footer"] : null), "cookies", array()), "html", null, true);
            echo "</a></div>
            </div>

            

        

    </div><!-- #copyrights end -->

</footer><!-- #footer end -->
<!-- Go To Top
============================================= -->
<div id=\"gotoTop\" class=\"icon-angle-up\"></div>

";
        }
        // line 110
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["footerscripts"]) ? $context["footerscripts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            // line 111
            echo $context["script"];
            echo "
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 112
        echo "    

</body>
</html>



";
    }

    public function getTemplateName()
    {
        return "footer.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  219 => 112,  211 => 111,  207 => 110,  179 => 95,  175 => 94,  154 => 76,  149 => 74,  145 => 73,  141 => 72,  114 => 57,  102 => 56,  95 => 54,  89 => 53,  83 => 52,  76 => 51,  74 => 50,  58 => 37,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{%if user.logged_in%}
<!-- Footer
============================================= -->
<footer id=\"footer\" class=\"dark\">    
    <div id=\"cookie_directive_container\" class=\"container\" style=\"display: none\">
            <nav class=\"navbar navbar-default navbar-fixed-bottom\" id=\"cookiesf\">

                <div class=\"container \">
                <div class=\"navbar-inner navbar-content-center\" id=\"cookie_accept\">

                    
                    <p id=\"cookdesk\">
                     Solicitamos su permiso para obtener datos estadísticos de su navegación en esta web, en cumplimiento del Real 
                     Decreto-ley 13/2012. Si continúa navegando consideramos que acepta el uso de cookies. <a target=\"_blank\" href=\"inicio/legal/\" style=\"font-size: 120%\">Más información.</a>
                    </p>
                <div class=\"closecok\"><a href=\"#\"><i class=\"icon-line-cross\"></i></a></div>
                    <br>

                </div>
              </div>

            </nav>
        </div>   
    <div class=\"imagen-footer\">
    <div class=\"container \">

        <!-- Footer Widgets
        ============================================= -->
        <div class=\"footer-widgets-wrap clearfix\">

            

                <div class=\"col_one_third\">

                    <div class=\"widget clearfix\">

                        <img src=\"{{imgurl}}logo-footer-white.png\" alt=\"\" class=\"footer-logo\">

                    </div>
                </div>
                

                <div class=\"col_one_third\">

                    <div class=\"widget widget_links clearfix\">

                        <h4>Páginas</h4>

                        <ul>
                            {%if user.tipo == 'C' %}
                            <li><a href=\"{{base_url()}}inicio/viaje\">{{menu.viaje|capitalize}}</a></li>
                            <li><a href=\"{{base_url()}}productos\">{{menu.catalogo|capitalize}}</a></li>
                            <li><a href=\"{{base_url()}}inicio/trayectoria\">{{menu.trayectoria|capitalize}}</a></li>
                            <li><a href=\"{{base_url()}}inicio/bases\">{{menu.programa|capitalize}}</a></li>
                            {%endif%}
                            <!--<li><a href=\"{{base_url()}}{%if user.tipo== '1' %}inicio/{%elseif user.tipo == '2' %}comercial/{%elseif user.tipo == '3'%}admin/{%endif%}estadisticas\">Mis Estadísticas</a></li>-->
                            <li><a href=\"{{base_url()}}{%if user.tipo == 'C' %}inicio/{%elseif user.tipo == 'S'%}comercial/{%elseif user.tipo == 'A'%}admin/{%endif%}perfil\">{{menu.perfil|capitalize}}</a></li>
                        </ul>

                    </div>

                </div>

                

            

            <div class=\"col_one_third col_last\">

                
                    <h5>Sede:</h5>
                    <p>{{footer.direccion}}<br>
                    {{footer.direccion2}} <br>
                    {{footer.direccion3}}</p>

                    <p>Teléfono: {{footer.tfno}}<br>
                    Email: info.bushido@yokohamaiberia.com</p>
                    
                

            </div>

        </div><!-- .footer-widgets-wrap end -->

    </div>
    </div>
    <!-- Copyrights
    ============================================= -->
    <div id=\"copyrights\">

        

            <div class=\"center\">
                &copy; Yokohama Iberia {{\"now\"|date(\"Y\")}}<br>
                <div class=\"copyright-links\"><a href=\"{{base_url()}}inicio/bases\">{{footer.aviso}}</a> / <a href=\"{{base_url()}}inicio/privacidad\">{{footer.privacidad}}</a> / <a href=\"{{base_url()}}inicio/legal\">{{footer.cookies}}</a></div>
            </div>

            

        

    </div><!-- #copyrights end -->

</footer><!-- #footer end -->
<!-- Go To Top
============================================= -->
<div id=\"gotoTop\" class=\"icon-angle-up\"></div>

{%endif%}
{% for script in footerscripts %}
{{script|raw}}
{% endfor %}    

</body>
</html>



", "footer.php", "/Applications/MAMP/htdocs/Bushido2018/application/views/footer.php");
    }
}
