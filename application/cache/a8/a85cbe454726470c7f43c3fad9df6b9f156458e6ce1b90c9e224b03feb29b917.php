<?php

/* taller/profile_taller_edit.php */
class __TwigTemplate_00d4bf9a20c9a7b5326c664acef1947b7b72dc2fa20c3697737ee2f5acd00bfd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!--  ======================Page Title======================= -->
<section id=\"page-title\" class=\"header perfil_header\">

    <div class=\"container clearfix\">
        <h1>";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "datos", array()), "titulo", array()), "html", null, true);
        echo "</h1>
    </div>

</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id=\"content\">

    <div class=\"content-wrap\">

        <div class=\"container clearfix\">


            <div class=\"row clearfix\">


                <div class=\"col-sm-12\">
                    <div class=\"col-md-3\">

                        ";
        // line 24
        if ( !(null === $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "avatar", array()))) {
            // line 25
            echo "
                        <div class=\"imgav\" style=\"max-width: 225px;\"><span width=\"220px\" height=\"220px\" id=\"avatarImg\" style=\"background-image: url(";
            // line 26
            echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
            echo "avatares/";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "cod_user", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "avatar", array()), "html", null, true);
            echo ");\" class=\"alignleft img-circle img-thumbnail notopmargin nobottommargin\" ></span></div>
                        ";
        } else {
            // line 28
            echo "                        <div class=\"imgav\"><img src=\"";
            echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
            echo "avatares/avatar.jpg\" class=\"alignleft img-circle img-thumbnail notopmargin nobottommargin\" style=\"max-width: 225px;\"></div>
                        ";
        }
        // line 30
        echo "
                        <div class=\"clear bottommargin-sm\"></div>

                        <div class=\"contact-widget\">
                            <div class=\"contact-form-result\"></div>
                            <form id=\"avatarForm\" name=\"frmava\" class=\"nomargin\" action=\"";
        // line 35
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "inicio/changeAva\" method=\"post\" enctype=\"multipart/form-data\">
                                <div>
                                    <label>";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "datos", array()), "avatar", array()), "cambiar", array()), "html", null, true);
        echo "</label><br>
                                    <input id=\"input-1\" type=\"file\" class=\"file\" accept=\"image\" name=\"cambiarAvatar\" required>
                                    ";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "datos", array()), "avatar", array()), "imagen", array()), "html", null, true);
        echo ": 220px x 220px 
                                </div>  
                            </form>
                        </div>

                    </div>
                    <div class=\"col-md-9\">
                        <div class=\"heading-block noborder bottommargin-sm\">
                            <h3 id=\"user-name\">";
        // line 47
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "nombre", array()), "html", null, true);
        echo "</h3>
                            <span>";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "datos", array()), "tittabla", array()), "html", null, true);
        echo "</span>
                        </div>

                        <div class=\"style-msg errormsg\">
                            <div class=\"sb-msg\"><i class=\"icon-remove-sign\"></i>";
        // line 52
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "datos", array()), "info", array()), "html", null, true);
        echo "</div>
                            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
                        </div>
                        <div class=\"col_half bottommargin-sm\">
                            <label for=\"billing-form-name\">";
        // line 56
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "datos", array()), "tabla", array()), "razon", array()), "html", null, true);
        echo ":</label>
                            <input type=\"text\" id=\"billing-form-name\" name=\"name\" value=\"";
        // line 57
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "nombre", array())), "html", null, true);
        echo "\" class=\"sm-form-control\" readonly/>
                        </div>

                        <div class=\"col_half col_last bottommargin-sm\">
                            <label for=\"billing-form-lname\">";
        // line 61
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "datos", array()), "tabla", array()), "nif", array()), "html", null, true);
        echo ":</label>
                            <input type=\"text\" id=\"billing-form-lname\" name=\"cif\" value=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "nif", array()), "html", null, true);
        echo "\" class=\"sm-form-control\" readonly/>
                        </div>
                        <div class=\"col_two_third bottommargin-sm\">
                            <label for=\"billing-form-name\">";
        // line 65
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "datos", array()), "tabla", array()), "email", array()), "html", null, true);
        echo ":</label>
                            <input type=\"text\" id=\"billing-form-name\" name=\"name\" value=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "email", array()), "html", null, true);
        echo "\" class=\"sm-form-control\" readonly/>
                        </div>

                        <div class=\"col_one_third col_last bottommargin-sm\">
                            <label for=\"billing-form-lname\">";
        // line 70
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "datos", array()), "tabla", array()), "tfno", array()), "html", null, true);
        echo ":</label>
                            <input type=\"number\" id=\"billing-form-email\" name=\"phone\" value=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "telefono", array()), "html", null, true);
        echo "\" class=\"sm-form-control\" readonly/>
                        </div>


                    </div>


                    <div class=\"clear bottommargin-sm\"></div>

                    <div class=\"col-md-12 clearfix\">
                        <div class=\"fancy-title title-dotted-border\">
                            <h3 class=\"notopmargin\">";
        // line 82
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "datos", array()), "direcciones", array()), "html", null, true);
        echo "</h3>
                        </div>

                        <div class=\"col_half\">
                            <h4 class=\"notopmargin\"><i class=\"fas fa-angle-double-right bushido\"></i>&nbsp;";
        // line 86
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "datos", array()), "direccion", array()), "html", null, true);
        echo "</h4>

                            ";
        // line 88
        if ( !(null === (isset($context["direccionesEnvio"]) ? $context["direccionesEnvio"] : null))) {
            // line 89
            echo "

                            <div class=\"accordion clearfix\">

                                ";
            // line 93
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["direccionesEnvio"]) ? $context["direccionesEnvio"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["dirEnvio"]) {
                // line 94
                echo "                                <div class=\"acctitle\"><i class=\"acc-closed fas fa-map-marker-alt bushido\"></i><i class=\"acc-open icon-remove-circle bushido\"></i>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["dirEnvio"], "direccion", array()), "html", null, true);
                echo "</div>
                                <div class=\"acc_content clearfix\">";
                // line 95
                echo twig_escape_filter($this->env, $this->getAttribute($context["dirEnvio"], "poblacion", array()), "html", null, true);
                echo ", ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["dirEnvio"], "provincia", array()), "html", null, true);
                echo ", ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["dirEnvio"], "cp", array()), "html", null, true);
                echo " TFNO:";
                echo twig_escape_filter($this->env, $this->getAttribute($context["dirEnvio"], "tfno", array()), "html", null, true);
                echo "</div>

                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dirEnvio'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 98
            echo "                            </div>

                            ";
        } else {
            // line 101
            echo "                            <div class=\"accordion clearfix\">

                                <div class=\"acctitle\"><i class=\"acc-closed fas fa-map-marker-alt bushido\"></i><i class=\"acc-open icon-remove-circle bushido\"></i>";
            // line 103
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "datos", array()), "nodir", array()), "html", null, true);
            echo "</div>
                                <div class=\"acc_content clearfix\">";
            // line 104
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "datos", array()), "nodirdetails", array()), "html", null, true);
            echo "</div>

                            </div>
                            ";
        }
        // line 108
        echo "
                            <h4 class=\"notopmargin bottommargin-xs\"><i class=\"fas fa-angle-double-right bushido\"></i>Añadir nueva dirección de envío</h4>

                            <div class=\"contact-widget\">
                                <div class=\"contact-form-result\"></div>
                                <form id=\"billing-form\" name=\"billing-form\" class=\"nobottommargin\" action=\"";
        // line 113
        echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
        echo "/inicio/saveDir\" method=\"post\">

                                    <div class=\"col_full bottommargin-xs\">
                                        <label for=\"billing-form-companyname\">";
        // line 116
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "datos", array()), "tabla", array()), "dir", array()), "html", null, true);
        echo "</label>
                                        <input type=\"text\" id=\"billing-form-companyname\" name=\"direccion\" class=\"sm-form-control\" />
                                    </div>

                                    <div class=\"col_half bottommargin-xs\">
                                        <label for=\"billing-form-address\">";
        // line 121
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "datos", array()), "tabla", array()), "pob", array()), "html", null, true);
        echo ":</label>
                                        <input type=\"text\" id=\"billing-form-address\" name=\"poblacion\" class=\"sm-form-control\" />
                                    </div>
                                    <div class=\"col_half bottommargin-xs col_last\">
                                        <label for=\"billing-form-address\">";
        // line 125
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "datos", array()), "tabla", array()), "prov", array()), "html", null, true);
        echo ":</label>
                                        <input type=\"text\" id=\"billing-form-address\" name=\"provincia\"  class=\"sm-form-control\" />
                                    </div>
                                    <div class=\"col_half bottommargin-xs\">
                                        <label for=\"billing-form-address\">";
        // line 129
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "datos", array()), "tabla", array()), "tfno", array()), "html", null, true);
        echo ":</label>
                                        <input type=\"text\" id=\"billing-form-address\" name=\"tfno\" class=\"sm-form-control\" />
                                    </div>
                                    <div class=\"col_half bottommargin-xs col_last\">
                                        <label for=\"billing-form-address\">";
        // line 133
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "datos", array()), "tabla", array()), "cp", array()), "html", null, true);
        echo ":</label>
                                        <input type=\"text\" id=\"billing-form-address\" name=\"cp\" class=\"sm-form-control\" />
                                    </div>


                                    <div class=\"fright\">
                                        <input type=\"submit\" class=\"button button-rounded button-dark topmargin-sm fright\" name=\"guardarDir\" onclick='saveNewDir(\"";
        // line 139
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "iduser", array()), "html", null, true);
        echo "\")' style=\"width: 100%;\">
                                    </div>
                                </form>
                            </div>

                        </div>

                        <div class=\"col_half col_last\">
                            <h4 class=\"notopmargin\"><i class=\"fas fa-angle-double-right bushido\"></i>&nbsp;Dirección de facturación</h4>

                            <div class=\"accordion clearfix\" id=\"dirFacturacion\">
                                ";
        // line 150
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["direcciones"]) ? $context["direcciones"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["direccion"]) {
            // line 151
            echo "                                <div class=\"acctitle\"><i class=\"acc-closed fas fa-map-marker-alt bushido\"></i><i class=\"acc-open icon-remove-circle bushido\"></i>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["direccion"], "Calle", array()), "html", null, true);
            echo "</div>
                                <div class=\"acc_content clearfix\">";
            // line 152
            echo twig_escape_filter($this->env, $this->getAttribute($context["direccion"], "Ciudad", array()), "html", null, true);
            echo ", ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["direccion"], "CP", array()), "html", null, true);
            echo ". ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["direccion"], "Pais", array()), "html", null, true);
            echo "</div>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['direccion'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 154
        echo "                            </div>
                        </div>
                        <div class=\"line\"></div>
                        <div class=\"contact-widget\">
                            <div class=\"contact-form-result\"></div>
                            <h3 id=\"user-name\">";
        // line 159
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "datos", array()), "pass", array()), "cambiar", array()), "html", null, true);
        echo "</h3>

                            <div class=\"col_full\">";
        // line 161
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "datos", array()), "pass", array()), "entradilla", array()), "html", null, true);
        echo "</div>  
                            <div class=\"col_half\">
                                <label for=\"billing-form-email\">";
        // line 163
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "datos", array()), "pass", array()), "new", array()), "html", null, true);
        echo "</label>
                                <input type=\"password\" name=\"newpass\" id=\"newpass\" placeholder=\"******\" class=\"sm-form-control\"/>
                            </div>
                            <div class=\"col_half col_last\">
                                <label for=\"billing-form-city\">";
        // line 167
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "datos", array()), "pass", array()), "rep", array()), "html", null, true);
        echo "</label>
                                <input type=\"password\" name=\"newpass2\" id=\"newpass2\"  placeholder=\"****\" class=\"sm-form-control\"/>
                            </div>
                            <div class=\"col-md-12\">
                                <button id=\"changebutpw\" href=\"#\" class=\"button button-3d fright\">";
        // line 171
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "datos", array()), "pass", array()), "cambiar", array()), "html", null, true);
        echo "</button>  
                            </div>
                        </div>
                    </div>

                    <div class=\"line visible-xs-block\"></div>
                </div>

                <div class=\"line visible-xs-block\"></div>

            </div>

        </div>

    </div>

</section><!-- #content end -->
";
    }

    public function getTemplateName()
    {
        return "taller/profile_taller_edit.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  344 => 171,  337 => 167,  330 => 163,  325 => 161,  320 => 159,  313 => 154,  301 => 152,  296 => 151,  292 => 150,  278 => 139,  269 => 133,  262 => 129,  255 => 125,  248 => 121,  240 => 116,  234 => 113,  227 => 108,  220 => 104,  216 => 103,  212 => 101,  207 => 98,  192 => 95,  187 => 94,  183 => 93,  177 => 89,  175 => 88,  170 => 86,  163 => 82,  149 => 71,  145 => 70,  138 => 66,  134 => 65,  128 => 62,  124 => 61,  117 => 57,  113 => 56,  106 => 52,  99 => 48,  95 => 47,  84 => 39,  79 => 37,  74 => 35,  67 => 30,  61 => 28,  52 => 26,  49 => 25,  47 => 24,  25 => 5,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!--  ======================Page Title======================= -->
<section id=\"page-title\" class=\"header perfil_header\">

    <div class=\"container clearfix\">
        <h1>{{textos.datos.titulo}}</h1>
    </div>

</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id=\"content\">

    <div class=\"content-wrap\">

        <div class=\"container clearfix\">


            <div class=\"row clearfix\">


                <div class=\"col-sm-12\">
                    <div class=\"col-md-3\">

                        {% if user.avatar is not null %}

                        <div class=\"imgav\" style=\"max-width: 225px;\"><span width=\"220px\" height=\"220px\" id=\"avatarImg\" style=\"background-image: url({{imgurl}}avatares/{{user.cod_user}}/{{user.avatar}});\" class=\"alignleft img-circle img-thumbnail notopmargin nobottommargin\" ></span></div>
                        {%else%}
                        <div class=\"imgav\"><img src=\"{{imgurl}}avatares/avatar.jpg\" class=\"alignleft img-circle img-thumbnail notopmargin nobottommargin\" style=\"max-width: 225px;\"></div>
                        {%endif%}

                        <div class=\"clear bottommargin-sm\"></div>

                        <div class=\"contact-widget\">
                            <div class=\"contact-form-result\"></div>
                            <form id=\"avatarForm\" name=\"frmava\" class=\"nomargin\" action=\"{{base_url()}}inicio/changeAva\" method=\"post\" enctype=\"multipart/form-data\">
                                <div>
                                    <label>{{textos.datos.avatar.cambiar}}</label><br>
                                    <input id=\"input-1\" type=\"file\" class=\"file\" accept=\"image\" name=\"cambiarAvatar\" required>
                                    {{textos.datos.avatar.imagen}}: 220px x 220px 
                                </div>  
                            </form>
                        </div>

                    </div>
                    <div class=\"col-md-9\">
                        <div class=\"heading-block noborder bottommargin-sm\">
                            <h3 id=\"user-name\">{{user.nombre}}</h3>
                            <span>{{textos.datos.tittabla}}</span>
                        </div>

                        <div class=\"style-msg errormsg\">
                            <div class=\"sb-msg\"><i class=\"icon-remove-sign\"></i>{{textos.datos.info}}</div>
                            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
                        </div>
                        <div class=\"col_half bottommargin-sm\">
                            <label for=\"billing-form-name\">{{textos.datos.tabla.razon}}:</label>
                            <input type=\"text\" id=\"billing-form-name\" name=\"name\" value=\"{{user.nombre|capitalize}}\" class=\"sm-form-control\" readonly/>
                        </div>

                        <div class=\"col_half col_last bottommargin-sm\">
                            <label for=\"billing-form-lname\">{{textos.datos.tabla.nif}}:</label>
                            <input type=\"text\" id=\"billing-form-lname\" name=\"cif\" value=\"{{user.nif}}\" class=\"sm-form-control\" readonly/>
                        </div>
                        <div class=\"col_two_third bottommargin-sm\">
                            <label for=\"billing-form-name\">{{textos.datos.tabla.email}}:</label>
                            <input type=\"text\" id=\"billing-form-name\" name=\"name\" value=\"{{user.email}}\" class=\"sm-form-control\" readonly/>
                        </div>

                        <div class=\"col_one_third col_last bottommargin-sm\">
                            <label for=\"billing-form-lname\">{{textos.datos.tabla.tfno}}:</label>
                            <input type=\"number\" id=\"billing-form-email\" name=\"phone\" value=\"{{user.telefono}}\" class=\"sm-form-control\" readonly/>
                        </div>


                    </div>


                    <div class=\"clear bottommargin-sm\"></div>

                    <div class=\"col-md-12 clearfix\">
                        <div class=\"fancy-title title-dotted-border\">
                            <h3 class=\"notopmargin\">{{textos.datos.direcciones}}</h3>
                        </div>

                        <div class=\"col_half\">
                            <h4 class=\"notopmargin\"><i class=\"fas fa-angle-double-right bushido\"></i>&nbsp;{{textos.datos.direccion}}</h4>

                            {% if direccionesEnvio is not null%}


                            <div class=\"accordion clearfix\">

                                {% for dirEnvio in direccionesEnvio%}
                                <div class=\"acctitle\"><i class=\"acc-closed fas fa-map-marker-alt bushido\"></i><i class=\"acc-open icon-remove-circle bushido\"></i>{{dirEnvio.direccion}}</div>
                                <div class=\"acc_content clearfix\">{{dirEnvio.poblacion}}, {{dirEnvio.provincia}}, {{dirEnvio.cp}} TFNO:{{dirEnvio.tfno}}</div>

                                {% endfor %}
                            </div>

                            {% else %}
                            <div class=\"accordion clearfix\">

                                <div class=\"acctitle\"><i class=\"acc-closed fas fa-map-marker-alt bushido\"></i><i class=\"acc-open icon-remove-circle bushido\"></i>{{textos.datos.nodir}}</div>
                                <div class=\"acc_content clearfix\">{{textos.datos.nodirdetails}}</div>

                            </div>
                            {% endif %}

                            <h4 class=\"notopmargin bottommargin-xs\"><i class=\"fas fa-angle-double-right bushido\"></i>Añadir nueva dirección de envío</h4>

                            <div class=\"contact-widget\">
                                <div class=\"contact-form-result\"></div>
                                <form id=\"billing-form\" name=\"billing-form\" class=\"nobottommargin\" action=\"{{baseurl}}/inicio/saveDir\" method=\"post\">

                                    <div class=\"col_full bottommargin-xs\">
                                        <label for=\"billing-form-companyname\">{{textos.datos.tabla.dir}}</label>
                                        <input type=\"text\" id=\"billing-form-companyname\" name=\"direccion\" class=\"sm-form-control\" />
                                    </div>

                                    <div class=\"col_half bottommargin-xs\">
                                        <label for=\"billing-form-address\">{{textos.datos.tabla.pob}}:</label>
                                        <input type=\"text\" id=\"billing-form-address\" name=\"poblacion\" class=\"sm-form-control\" />
                                    </div>
                                    <div class=\"col_half bottommargin-xs col_last\">
                                        <label for=\"billing-form-address\">{{textos.datos.tabla.prov}}:</label>
                                        <input type=\"text\" id=\"billing-form-address\" name=\"provincia\"  class=\"sm-form-control\" />
                                    </div>
                                    <div class=\"col_half bottommargin-xs\">
                                        <label for=\"billing-form-address\">{{textos.datos.tabla.tfno}}:</label>
                                        <input type=\"text\" id=\"billing-form-address\" name=\"tfno\" class=\"sm-form-control\" />
                                    </div>
                                    <div class=\"col_half bottommargin-xs col_last\">
                                        <label for=\"billing-form-address\">{{textos.datos.tabla.cp}}:</label>
                                        <input type=\"text\" id=\"billing-form-address\" name=\"cp\" class=\"sm-form-control\" />
                                    </div>


                                    <div class=\"fright\">
                                        <input type=\"submit\" class=\"button button-rounded button-dark topmargin-sm fright\" name=\"guardarDir\" onclick='saveNewDir(\"{{user.iduser}}\")' style=\"width: 100%;\">
                                    </div>
                                </form>
                            </div>

                        </div>

                        <div class=\"col_half col_last\">
                            <h4 class=\"notopmargin\"><i class=\"fas fa-angle-double-right bushido\"></i>&nbsp;Dirección de facturación</h4>

                            <div class=\"accordion clearfix\" id=\"dirFacturacion\">
                                {% for direccion in direcciones %}
                                <div class=\"acctitle\"><i class=\"acc-closed fas fa-map-marker-alt bushido\"></i><i class=\"acc-open icon-remove-circle bushido\"></i>{{direccion.Calle}}</div>
                                <div class=\"acc_content clearfix\">{{direccion.Ciudad}}, {{direccion.CP}}. {{direccion.Pais}}</div>
                                {% endfor %}
                            </div>
                        </div>
                        <div class=\"line\"></div>
                        <div class=\"contact-widget\">
                            <div class=\"contact-form-result\"></div>
                            <h3 id=\"user-name\">{{textos.datos.pass.cambiar}}</h3>

                            <div class=\"col_full\">{{textos.datos.pass.entradilla}}</div>  
                            <div class=\"col_half\">
                                <label for=\"billing-form-email\">{{textos.datos.pass.new}}</label>
                                <input type=\"password\" name=\"newpass\" id=\"newpass\" placeholder=\"******\" class=\"sm-form-control\"/>
                            </div>
                            <div class=\"col_half col_last\">
                                <label for=\"billing-form-city\">{{textos.datos.pass.rep}}</label>
                                <input type=\"password\" name=\"newpass2\" id=\"newpass2\"  placeholder=\"****\" class=\"sm-form-control\"/>
                            </div>
                            <div class=\"col-md-12\">
                                <button id=\"changebutpw\" href=\"#\" class=\"button button-3d fright\">{{textos.datos.pass.cambiar}}</button>  
                            </div>
                        </div>
                    </div>

                    <div class=\"line visible-xs-block\"></div>
                </div>

                <div class=\"line visible-xs-block\"></div>

            </div>

        </div>

    </div>

</section><!-- #content end -->
", "taller/profile_taller_edit.php", "/Applications/MAMP/htdocs/Bushido2018/application/views/taller/profile_taller_edit.php");
    }
}
