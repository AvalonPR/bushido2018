<?php

/* shop-single.php */
class __TwigTemplate_86254520f9df2de2ab3ec739098826bf4cf59cad56a8a3fd41f57e35f84174b1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- Page Sub Menu
                ============================================= -->
<div id=\"page-menu\" class=\"hidden-xs hidden-sm\">

    <div id=\"page-menu-wrap\">

        <div class=\"container clearfix\">
            <nav id=\"primary-menu\" class=\"catalogo-completo\">

                <ul>
                    <li id=\"catalogo_menu\" class=\"current mega-menu sub-menu\"><a href=\"#\" style=\"color: #fff;\">";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "subtitulo", array()), "html", null, true);
        echo " <i class=\"icon-angle-down\"></i></a>
                        <div id=\"catalogo_menu_in\" class=\"mega-menu-content clearfix\">
                            ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["jerarquia"]) ? $context["jerarquia"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["j"]) {
            // line 14
            echo "                            <ul class=\"mega-menu-column col-5\">
                                <li><a class=\"menu_white\" href=\"";
            // line 15
            echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
            echo "productos/";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "nombre", array()), "html", null, true);
            echo "</a></li>
                            </ul>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['j'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "                        </div>
                    </li>
                </ul>

            </nav><!-- #primary-menu end -->
            <!-- #primary-menu end -->
        </div>
    </div>

</div>
<!-- #page-menu end -->


<!--MIGA DE PAN-->
<div class=\"container\">
    <ul class=\"migapan\">
            <li class=\"migapan-item\"><a href=\"";
        // line 34
        echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
        echo "productos\">&nbsp;";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "titulo", array()), "html", null, true);
        echo "&nbsp;</a></li>
            <li class=\"migapan-item\"><a href=\"";
        // line 35
        echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
        echo "productos/";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "parent", array()), "categoria", array()), "slug", array()), "html", null, true);
        echo "\">&nbsp;";
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "parent", array()), "categoria", array()), "slug", array())), "html", null, true);
        echo "&nbsp;</a></li>
            <li class=\"migapan-item\"><a href=\"";
        // line 36
        echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
        echo "productos/";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "parent", array()), "categoria", array()), "slug", array()), "html", null, true);
        echo "/";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "parent", array()), "subcategoria", array()), "slug", array()), "html", null, true);
        echo "\">&nbsp;";
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "parent", array()), "subcategoria", array()), "slug", array())), "html", null, true);
        echo "&nbsp;</a></li>
            <li class=\"migapan-item\"><a href=\"";
        // line 37
        echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
        echo "productos/";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "parent", array()), "categoria", array()), "slug", array()), "html", null, true);
        echo "/";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "parent", array()), "subcategoria", array()), "slug", array()), "html", null, true);
        echo "/";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "parent", array()), "familia", array()), "slug", array()), "html", null, true);
        echo "\">&nbsp;";
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "parent", array()), "familia", array()), "slug", array())), "html", null, true);
        echo "&nbsp;</a></li>
            <li class=\"migapan-item active\" aria-current=\"page\">&nbsp;";
        // line 38
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "nombre", array())), "html", null, true);
        echo "</li>
    </ul>
</div>

<!--FIN MIGA DE PAN-->

<!-- Content
============================================= -->
<section id=\"content\">

    <div class=\"content-wrap\">

        <div class=\"container clearfix\">

            <div class=\"nobottommargin clearfix col_last\">

                <div class=\"single-product\">

                    <div class=\"product\">


                        <div class=\"col-lg-5 col-md-5 col-sm-4 col-xs-12 center\">
                            <img src=\"https://apps.avalonprplus.com/uploads/";
        // line 60
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "imagen", array()), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "nombre", array()), "html", null, true);
        echo "\">
                        </div>

                    </div>
                    <div class=\"col-lg-7 col-md-7 col-sm-8 col-xs-12 nobottommargin col_last product-desc\">

                        <!-- Product Single - Name
                                                                    ============================================= -->
                        <div class=\"product-title product-title-single\">
                            <h2>";
        // line 69
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "nombre", array()), "html", null, true);
        echo "</h2>
                        </div>

                        <!-- Product Single - Price
                                                                    ============================================= -->
                        <div class=\"product-price product-price-single\">";
        // line 74
        if ( !(null === $this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "precioDto", array()))) {
            echo "<del>";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "precio", array()), 0, ",", "."), "html", null, true);
            echo " </del> <ins>";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "precioDto", array()), 0, ",", "."), "html", null, true);
            echo " </ins> ";
        } else {
            echo "<ins> ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "precio", array()), 0, ",", "."), "html", null, true);
            echo " </ins> ";
        }
        echo "</div>
                        <div class=\"clear\"></div>
                        <div class=\"line\"></div>
                        <input type=\"hidden\" value=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "unidades", array()), "html", null, true);
        echo "\" id=\"unidades\">
                        <input type=\"hidden\" value=\"";
        // line 78
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "id", array()), "html", null, true);
        echo "\" id=\"id\">
                        ";
        // line 79
        if ( !twig_test_empty($this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "atributos", array()))) {
            // line 80
            echo "                            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "atributos", array()));
            foreach ($context['_seq'] as $context["k"] => $context["atributo"]) {
                // line 81
                echo "                                <!-- Product Single - Quantity & Cart Button
                                ============================================= -->
                                ";
                // line 83
                if (($context["k"] == "2")) {
                    // line 84
                    echo "                                <p class=\"\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "item", array()), "talla", array()), "html", null, true);
                    echo "</p>
                                <div class=\"col_full talla grid-container\" data-layout=\"fitRows\">
                                    
                                    ";
                    // line 87
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($context["atributo"]);
                    foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
                        // line 88
                        echo "                                    <div class=\"size-item fleft\">
                                        <input onclick=\"selectSize('size";
                        // line 89
                        echo twig_escape_filter($this->env, $context["a"], "html", null, true);
                        echo "', '";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "id", array()), "html", null, true);
                        echo "')\"  type=\"button\" class=\"clearfix\" value=\"";
                        echo twig_escape_filter($this->env, $context["a"], "html", null, true);
                        echo "\" id=\"size";
                        echo twig_escape_filter($this->env, $context["a"], "html", null, true);
                        echo "\">
                                        <div class=\"quantity-size\" id=\"divsize";
                        // line 90
                        echo twig_escape_filter($this->env, $context["a"], "html", null, true);
                        echo "\" data-size=\"";
                        echo twig_escape_filter($this->env, $context["a"], "html", null, true);
                        echo "\" style=\"display:none;\">
                                                <input type=\"button\" value=\"-\" class=\"minus size-minus\">
                                                <input type=\"text\" step=\"1\" min=\"1\" value=\"1\" title=\"Qty\" class=\"qty\" name=\"qtysize[]\" size=\"4\" />
                                                <input type=\"button\" value=\"+\" class=\"plus size-plus\">
                                        </div>
                                    </div>
                                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 97
                    echo "                                </div>
                                <div class=\"line\"></div>
                                ";
                }
                // line 100
                echo "                                ";
                if (($context["k"] == "1")) {
                    // line 101
                    echo "                                <p class=\"\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "item", array()), "color", array()), "html", null, true);
                    echo "</p>
                                <div class=\"col_full color_ grid-container\" data-layout=\"fitRows\">
                                    ";
                    // line 103
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($context["atributo"]);
                    foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
                        // line 104
                        echo "                                    <label class=\"color-item fleft\"
                                           ";
                        // line 105
                        $context["color"] = twig_split_filter($this->env, $context["a"], ",");
                        echo ">
                                        <input  type=\"checkbox\" id=\"color\" data-id=\"";
                        // line 106
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["color"]) ? $context["color"] : null), 1, array()), "html", null, true);
                        echo ",";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["color"]) ? $context["color"] : null), 0, array()), "html", null, true);
                        echo "\" name=\"color[1][]\">
                                        <span class=\"checkmark\" style=\"background-color: ";
                        // line 107
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["color"]) ? $context["color"] : null), 0, array()), "html", null, true);
                        echo ";\"></span>
                                    </label>
                                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 110
                    echo "                                </div>
                                <div class=\"line\"></div>
                                ";
                }
                // line 113
                echo "                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['k'], $context['atributo'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 114
            echo "                        ";
        }
        // line 115
        echo "                        
                       

                        
                        <div class=\"quantity quantitySingle\">
                            <input type=\"button\" value=\"-\" class=\"minus\">
                            <input type=\"text\" step=\"1\" min=\"1\" name=\"quantity\" value=\"1\" title=\"Qty\" class=\"qty\" size=\"4\" id=\"quantity";
        // line 121
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "id", array()), "html", null, true);
        echo "\"/>
                            <input type=\"button\" value=\"+\" class=\"plus\">
                        </div>
                        ";
        // line 124
        if ( !(null === $this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "precioDto", array()))) {
            // line 125
            echo "                        <button onclick=\"addtoCart(";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "id", array()), "html", null, true);
            echo ")\" class=\"single_product_add add-to-cart";
            if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "viaje", array()) != 0)) {
                echo "-disabled";
            } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array()) < $this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "precioDto", array()))) {
                echo "-disabled";
            } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array()) < ((isset($context["cart_total"]) ? $context["cart_total"] : null) + $this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "precioDto", array())))) {
                echo "-disabled";
            }
            echo " button nomargin fright\" ";
            if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "viaje", array()) != 0)) {
                echo "disabled";
            } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array()) < $this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "precioDto", array()))) {
                echo "disabled";
            } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array()) < ((isset($context["cart_total"]) ? $context["cart_total"] : null) + $this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "precioDto", array())))) {
                echo "disabled";
            }
            echo " >";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "botones", array()), "addmore", array()), "html", null, true);
            echo "</button>
                        ";
        } else {
            // line 127
            echo "                        <button onclick=\"addtoCart(";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "id", array()), "html", null, true);
            echo ")\" class=\"single_product_add add-to-cart";
            if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "viaje", array()) != 0)) {
                echo "-disabled";
            } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array()) < $this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "precio", array()))) {
                echo "-disabled";
            } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array()) < ((isset($context["cart_total"]) ? $context["cart_total"] : null) + $this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "precio", array())))) {
                echo "-disabled";
            }
            echo " button nomargin fright\" ";
            if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "viaje", array()) != 0)) {
                echo "disabled";
            } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array()) < $this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "precio", array()))) {
                echo "disabled";
            } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array()) < ((isset($context["cart_total"]) ? $context["cart_total"] : null) + $this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "precio", array())))) {
                echo "disabled";
            }
            echo " >";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "botones", array()), "addmore", array()), "html", null, true);
            echo "</button>
                        ";
        }
        // line 129
        echo "                        <!-- Product Single - Quantity & Cart Button End -->

                        <div class=\"clear\"></div>
                        
                        <div class=\"line\"></div>

                        <p>";
        // line 135
        echo $this->getAttribute((isset($context["productos"]) ? $context["productos"] : null), "descripcion", array());
        echo "</p>


                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</section>
";
    }

    public function getTemplateName()
    {
        return "shop-single.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  359 => 135,  351 => 129,  327 => 127,  303 => 125,  301 => 124,  295 => 121,  287 => 115,  284 => 114,  278 => 113,  273 => 110,  264 => 107,  258 => 106,  254 => 105,  251 => 104,  247 => 103,  241 => 101,  238 => 100,  233 => 97,  218 => 90,  208 => 89,  205 => 88,  201 => 87,  194 => 84,  192 => 83,  188 => 81,  183 => 80,  181 => 79,  177 => 78,  173 => 77,  157 => 74,  149 => 69,  135 => 60,  110 => 38,  98 => 37,  88 => 36,  80 => 35,  74 => 34,  56 => 18,  43 => 15,  40 => 14,  36 => 13,  31 => 11,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- Page Sub Menu
                ============================================= -->
<div id=\"page-menu\" class=\"hidden-xs hidden-sm\">

    <div id=\"page-menu-wrap\">

        <div class=\"container clearfix\">
            <nav id=\"primary-menu\" class=\"catalogo-completo\">

                <ul>
                    <li id=\"catalogo_menu\" class=\"current mega-menu sub-menu\"><a href=\"#\" style=\"color: #fff;\">{{textos.subtitulo}} <i class=\"icon-angle-down\"></i></a>
                        <div id=\"catalogo_menu_in\" class=\"mega-menu-content clearfix\">
                            {%for key, j in jerarquia%}
                            <ul class=\"mega-menu-column col-5\">
                                <li><a class=\"menu_white\" href=\"{{baseurl}}productos/{{key}}\">{{j.nombre}}</a></li>
                            </ul>
                            {%endfor%}
                        </div>
                    </li>
                </ul>

            </nav><!-- #primary-menu end -->
            <!-- #primary-menu end -->
        </div>
    </div>

</div>
<!-- #page-menu end -->


<!--MIGA DE PAN-->
<div class=\"container\">
    <ul class=\"migapan\">
            <li class=\"migapan-item\"><a href=\"{{baseurl}}productos\">&nbsp;{{textos.titulo}}&nbsp;</a></li>
            <li class=\"migapan-item\"><a href=\"{{baseurl}}productos/{{productos.parent.categoria.slug}}\">&nbsp;{{productos.parent.categoria.slug|capitalize}}&nbsp;</a></li>
            <li class=\"migapan-item\"><a href=\"{{baseurl}}productos/{{productos.parent.categoria.slug}}/{{productos.parent.subcategoria.slug}}\">&nbsp;{{productos.parent.subcategoria.slug|capitalize}}&nbsp;</a></li>
            <li class=\"migapan-item\"><a href=\"{{baseurl}}productos/{{productos.parent.categoria.slug}}/{{productos.parent.subcategoria.slug}}/{{productos.parent.familia.slug}}\">&nbsp;{{productos.parent.familia.slug|capitalize}}&nbsp;</a></li>
            <li class=\"migapan-item active\" aria-current=\"page\">&nbsp;{{productos.nombre|capitalize}}</li>
    </ul>
</div>

<!--FIN MIGA DE PAN-->

<!-- Content
============================================= -->
<section id=\"content\">

    <div class=\"content-wrap\">

        <div class=\"container clearfix\">

            <div class=\"nobottommargin clearfix col_last\">

                <div class=\"single-product\">

                    <div class=\"product\">


                        <div class=\"col-lg-5 col-md-5 col-sm-4 col-xs-12 center\">
                            <img src=\"https://apps.avalonprplus.com/uploads/{{productos.imagen}}\" alt=\"{{productos.nombre}}\">
                        </div>

                    </div>
                    <div class=\"col-lg-7 col-md-7 col-sm-8 col-xs-12 nobottommargin col_last product-desc\">

                        <!-- Product Single - Name
                                                                    ============================================= -->
                        <div class=\"product-title product-title-single\">
                            <h2>{{productos.nombre}}</h2>
                        </div>

                        <!-- Product Single - Price
                                                                    ============================================= -->
                        <div class=\"product-price product-price-single\">{%if productos.precioDto is not null%}<del>{{productos.precio|number_format(0, ',', '.')}} </del> <ins>{{productos.precioDto|number_format(0, ',', '.')}} </ins> {%else%}<ins> {{productos.precio|number_format(0, ',', '.')}} </ins> {% endif%}</div>
                        <div class=\"clear\"></div>
                        <div class=\"line\"></div>
                        <input type=\"hidden\" value=\"{{productos.unidades}}\" id=\"unidades\">
                        <input type=\"hidden\" value=\"{{productos.id}}\" id=\"id\">
                        {%if productos.atributos is not empty%}
                            {%for k,atributo in productos.atributos%}
                                <!-- Product Single - Quantity & Cart Button
                                ============================================= -->
                                {%if k=='2'%}
                                <p class=\"\">{{textos.item.talla}}</p>
                                <div class=\"col_full talla grid-container\" data-layout=\"fitRows\">
                                    
                                    {%for a in atributo%}
                                    <div class=\"size-item fleft\">
                                        <input onclick=\"selectSize('size{{a}}', '{{productos.id}}')\"  type=\"button\" class=\"clearfix\" value=\"{{a}}\" id=\"size{{a}}\">
                                        <div class=\"quantity-size\" id=\"divsize{{a}}\" data-size=\"{{a}}\" style=\"display:none;\">
                                                <input type=\"button\" value=\"-\" class=\"minus size-minus\">
                                                <input type=\"text\" step=\"1\" min=\"1\" value=\"1\" title=\"Qty\" class=\"qty\" name=\"qtysize[]\" size=\"4\" />
                                                <input type=\"button\" value=\"+\" class=\"plus size-plus\">
                                        </div>
                                    </div>
                                    {%endfor%}
                                </div>
                                <div class=\"line\"></div>
                                {%endif%}
                                {%if k=='1'%}
                                <p class=\"\">{{textos.item.color}}</p>
                                <div class=\"col_full color_ grid-container\" data-layout=\"fitRows\">
                                    {%for a in atributo%}
                                    <label class=\"color-item fleft\"
                                           {%set color = a|split(',') %}>
                                        <input  type=\"checkbox\" id=\"color\" data-id=\"{{color.1}},{{color.0}}\" name=\"color[1][]\">
                                        <span class=\"checkmark\" style=\"background-color: {{color.0}};\"></span>
                                    </label>
                                    {%endfor%}
                                </div>
                                <div class=\"line\"></div>
                                {%endif%}
                            {%endfor%}
                        {%endif%}
                        
                       

                        
                        <div class=\"quantity quantitySingle\">
                            <input type=\"button\" value=\"-\" class=\"minus\">
                            <input type=\"text\" step=\"1\" min=\"1\" name=\"quantity\" value=\"1\" title=\"Qty\" class=\"qty\" size=\"4\" id=\"quantity{{productos.id}}\"/>
                            <input type=\"button\" value=\"+\" class=\"plus\">
                        </div>
                        {%if productos.precioDto is not null%}
                        <button onclick=\"addtoCart({{productos.id}})\" class=\"single_product_add add-to-cart{%if user.viaje != 0 %}-disabled{%elseif user.puntos < productos.precioDto %}-disabled{%elseif user.puntos < (cart_total + productos.precioDto) %}-disabled{%endif%} button nomargin fright\" {%if user.viaje != 0 %}disabled{%elseif user.puntos < productos.precioDto %}disabled{%elseif user.puntos < (cart_total + productos.precioDto) %}disabled{%endif%} >{{textos.botones.addmore}}</button>
                        {%else%}
                        <button onclick=\"addtoCart({{productos.id}})\" class=\"single_product_add add-to-cart{%if user.viaje != 0 %}-disabled{%elseif user.puntos < productos.precio %}-disabled{%elseif user.puntos < (cart_total + productos.precio) %}-disabled{%endif%} button nomargin fright\" {%if user.viaje != 0 %}disabled{%elseif user.puntos < productos.precio %}disabled{%elseif user.puntos < (cart_total + productos.precio) %}disabled{%endif%} >{{textos.botones.addmore}}</button>
                        {%endif%}
                        <!-- Product Single - Quantity & Cart Button End -->

                        <div class=\"clear\"></div>
                        
                        <div class=\"line\"></div>

                        <p>{{productos.descripcion|raw}}</p>


                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</section>
", "shop-single.php", "/Applications/MAMP/htdocs/Bushido2018/application/views/shop-single.php");
    }
}
