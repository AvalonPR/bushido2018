<?php

/* taller/home.php */
class __TwigTemplate_e6aeb2705546f04ac2477704f2fd851f943f968351b029e9b04defb73de6450e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section id=\"slider\" class=\"slider-parallax swiper_wrapper clearfix\" data-autoplay=\"7000\" data-speed=\"650\" data-loop=\"true\">

    <div class=\"slider-parallax-inner\">

        <div class=\"swiper-container swiper-parent\">
            <div class=\"swiper-wrapper\">
                ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["sliders"]) ? $context["sliders"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["slider"]) {
            // line 8
            echo "                <div class=\"swiper-slide dark\" style=\"background-image: url('";
            echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
            echo "sliders/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["slider"], "id", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["slider"], "imagen", array()), "html", null, true);
            echo "');\">
                    <div class=\"container clearfix\">
                        <div class=\"slider-caption slider-caption-center\">
                            <h2 data-caption-animate=\"fadeInUp\">";
            // line 11
            echo $this->getAttribute($context["slider"], "titulo", array());
            echo "</h2>
                            <p data-caption-animate=\"fadeInUp\" data-caption-delay=\"200\">";
            // line 12
            echo $this->getAttribute($context["slider"], "subtitulo", array());
            echo "</p>
                            <a data-caption-animate=\"fadeInUp\" href=\"";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($context["slider"], "url", array()), "html", null, true);
            echo "\" class=\"button\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["slider"], "boton", array()), "html", null, true);
            echo "</a>
                        </div>
                    </div>
                </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['slider'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "            </div>
            <div id=\"slider-arrow-left\"><i class=\"icon-angle-left\"></i></div>
            <div id=\"slider-arrow-right\"><i class=\"icon-angle-right\"></i></div>
        </div>

    </div>

</section>



<!-- Content
============================================= -->
<section id=\"content\">

    <div class=\"nobottompadding\">
        
        ";
        // line 35
        if ((($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "viaje", array()) != 1) && ($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "viaje", array()) != 2))) {
            // line 36
            echo "        <div class=\"fondo-productos\">
        <div class=\"container clearfix\">
            <div class=\"col-lg-5 col-md-6 hidden-sm hidden-xs\" style=\"margin-top: -20px;\">
                <img class=\"samurai-home\" alt=\"samurai\" src=\"";
            // line 39
            echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
            echo "img-catalogo-paint.png\"/>
            </div>
            
            <div class=\"col-lg-7 col-md-6 col-sm-12 col-xs-12 buscador\">
                <div class=\"clear bottommargin-lg hidden-xs\"></div>
                <h2>";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "buscador", array()), "titulo", array()), "html", null, true);
            echo "</h2>
                <p>";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "buscador", array()), "subtitulo", array()), "html", null, true);
            echo "</p>
                <div class=\"line-custom bottommargin-sm\"></div>
                <form>
                    <select>
                        <option value=\"\" disabled selected hidden>";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "buscador", array()), "categoria", array()), "label", array()), "html", null, true);
            echo "</option>
                        ";
            // line 50
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["jerarquia"]) ? $context["jerarquia"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["j"]) {
                // line 51
                echo "                        <ul class=\"mega-menu-column col-5\">
                            <option value=\"";
                // line 52
                echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "nombre", array()), "html", null, true);
                echo "\"><a class=\"menu_white\" href=\"";
                echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
                echo "productos/";
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "nombre", array()), "html", null, true);
                echo "</a></option>
                        </ul>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['j'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 55
            echo "                    </select>
                    <select>
                        <option value=\"\" disabled selected hidden>";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "buscador", array()), "subcategoria", array()), "label", array()), "html", null, true);
            echo "</option>

                        ";
            // line 59
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["jerarquia"]) ? $context["jerarquia"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["j"]) {
                // line 60
                echo "                                        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["j"], "children", array()));
                foreach ($context['_seq'] as $context["key"] => $context["c"]) {
                    // line 61
                    echo "                        <option value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "nombre", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "nombre", array()), "html", null, true);
                    echo "</option>
                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['c'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 63
                echo "                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['j'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 64
            echo "                    </select>
                    <select style=\"margin-right: 0;\">
                        <option value=\"\" disabled selected hidden>";
            // line 66
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "buscador", array()), "familia", array()), "label", array()), "html", null, true);
            echo "</option>
                            ";
            // line 67
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["jerarquia"]) ? $context["jerarquia"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["j"]) {
                // line 68
                echo "                                        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["j"], "children", array()));
                foreach ($context['_seq'] as $context["key"] => $context["c"]) {
                    // line 69
                    echo "                                                ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["c"], "children", array()));
                    foreach ($context['_seq'] as $context["key"] => $context["g"]) {
                        // line 70
                        echo "                        <option value=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["g"], "nombre", array()), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["g"], "nombre", array()), "html", null, true);
                        echo "</option>
                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['key'], $context['g'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 72
                    echo "                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['c'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 73
                echo "                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['j'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 74
            echo "                    </select>
                    <input type=\"submit\" value=\"BUSCAR\"/>
                </form>
            </div>
            
        </div>
        
        
        
        <div class=\"container clearfix\">


            <div class=\"row galeria\">
                ";
            // line 87
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["productos"]) ? $context["productos"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["producto"]) {
                // line 88
                echo "                <div class=\"col-lg-3 col-md-6 col-sm-6 col-xs-12 producto\">
                    <div class=\"product-image product-image-catalog\">
                            ";
                // line 90
                $context["url"] = array(0 => $this->getAttribute($this->getAttribute($this->getAttribute($context["producto"], "parent", array()), "categoria", array()), "slug", array()), 1 => $this->getAttribute($this->getAttribute($this->getAttribute($context["producto"], "parent", array()), "subcategoria", array()), "slug", array()), 2 => $this->getAttribute($this->getAttribute($this->getAttribute($context["producto"], "parent", array()), "familia", array()), "slug", array()), 3 => $this->getAttribute($context["producto"], "slug", array()));
                // line 91
                echo "                            <a href=\"";
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "productos/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["producto"], "parent", array()), "categoria", array()), "slug", array()), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["producto"], "parent", array()), "subcategoria", array()), "slug", array()), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["producto"], "parent", array()), "familia", array()), "slug", array()), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["producto"], "slug", array()), "html", null, true);
                echo "\" class=\"pdt_image\" style=\"background-image: url('https://apps.avalonprplus.com/uploads/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["producto"], "imagen", array()), "html", null, true);
                echo "')\"></a>
                                                        
                    </div>
                    <div class=\"product-data col-md-12 col-sm-12 col-xs-12 bottommargin-sm\">
                        
                        <a href=\"";
                // line 96
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "productos/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["producto"], "parent", array()), "categoria", array()), "slug", array()), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["producto"], "parent", array()), "subcategoria", array()), "slug", array()), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["producto"], "parent", array()), "familia", array()), "slug", array()), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["producto"], "slug", array()), "html", null, true);
                echo "\"><h4>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["producto"], "nombre", array()), "html", null, true);
                echo "</h4></a>
                        
                        <p class=\"precio-home\"><i class=\"fas fa-angle-double-right\"></i>";
                // line 98
                if ( !(null === $this->getAttribute($context["producto"], "precioDto", array()))) {
                    echo "<del>";
                    echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["producto"], "precio", array()), 0, ",", "."), "html", null, true);
                    echo " Km</del> <ins>";
                    echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["producto"], "precioDto", array()), 0, ",", "."), "html", null, true);
                    echo " Km</ins> ";
                } else {
                    echo " ";
                    echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["producto"], "precio", array()), 0, ",", "."), "html", null, true);
                    echo " Km";
                }
                echo "</p>

                    </div>
                </div>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['producto'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 103
            echo "            </div>
            

        </div>
        </div>
        ";
        }
        // line 109
        echo "        <div class=\"promo promo-dark promo-full nobottommargin header-stick notopborder\" style=\"overflow: hidden;\">
            <div class=\"container clearfix homes\" >
                <div class=\"promoText col-md-7 col-xs-12 topmargin-sm\">
                    <h3>";
        // line 112
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "viaje", array()), "titulo", array()), "html", null, true);
        echo "</h3>
                    <div class=\"line-custom\"></div>
                    <span>
                        <p>";
        // line 115
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "viaje", array()), "subtitulo", array()), "html", null, true);
        echo "</p>
                        <div class=\"clear\"></div>
                        <a href=\"";
        // line 117
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "inicio/viaje\" class=\"button\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "viaje", array()), "boton", array()), "html", null, true);
        echo " <i class=\"material-icons\">flight_takeoff</i></a>
                    </span>
                </div>
                <div class=\"promoImg col-md-5 col-xs-12\">
                        <img src=\"";
        // line 121
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "barco-viaje-yokohama-2019.png\" data-animate=\"fadeInUp\" data-delay=\"100\" alt=\"el gran viaje está a punto de empezar\">
\t\t
                </div>
            </div>
        </div>
          

        <div class=\"parallax light nobottommargin\" id=\"testimonial-taller\" style=\"padding: 100px 0px; background-position: 50% 216.6px;\" data-stellar-background-ratio=\"0.4\">

            <div class=\"fslider testimonial testimonial-full\" data-animation=\"fade\" data-arrows=\"false\">
                <div class=\"flexslider\" style=\"height: 227px;\">
                    <div class=\"slider-wrap\">
                        <div class=\"slide\" data-thumb-alt=\"\" style=\"width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;\">
                            <div class=\"testi-content\">
                                <div class=\"testi-meta\">
                                ";
        // line 136
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "secciones", array()), "entrada", array()), "html", null, true);
        echo "                           
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class=\"trayectoria-home\">
        <div class=\"container clearfix bg-trayectoria nobottommargin\">
            <div>
                <div id=\"justicia\" class=\"steps\">
                    <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Justicia\">Gi</span>
                    <span class=\"km\">820 km</span>
                    <img src=\"";
        // line 151
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "trayectoria/steps-trayectoria-01.png\" alt=\"Gi, Justicia\">
                </div>
                <div id=\"valor\" class=\"steps\">
                    <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Valor\">Yu</span>
                    <span class=\"km\">1.640 km</span>
                    <img src=\"";
        // line 156
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "trayectoria/steps-trayectoria-02.png\" alt=\"Yu, Valor\">
                </div>
                <div id=\"compasion\" class=\"steps\">
                    <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Compasión\">Jin</span>
                    <span class=\"km\">2.460 km</span>
                    <img src=\"";
        // line 161
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "trayectoria/steps-trayectoria-03.png\" alt=\"Gi, Justicia\">
                </div>
                <div id=\"cortesia\" class=\"steps\">
                    <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Cortesía\">Rei</span>
                    <span class=\"km\">3.280 km</span>
                    <img src=\"";
        // line 166
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "trayectoria/steps-trayectoria-04.png\" alt=\"Gi, Justicia\">
                </div>
                <div id=\"sinceridad\" class=\"steps\">
                    <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Sinceridad\">Makoto</span>
                    <span class=\"km\">4.100 km</span>
                    <img src=\"";
        // line 171
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "trayectoria/steps-trayectoria-05.png\" alt=\"Gi, Justicia\">
                </div>
                <div id=\"honor\" class=\"steps\">
                    <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Honor\">Meyo</span>
                    <span class=\"km\">4.920 km</span>
                    <img src=\"";
        // line 176
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "trayectoria/steps-trayectoria-06.png\" alt=\"Gi, Justicia\">
                </div>
                <div id=\"lealtad\" class=\"steps\">
                    <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Lealtad\">Chu</span>
                    <span class=\"km\">5.900 km</span>
                    <img src=\"";
        // line 181
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "trayectoria/steps-trayectoria-07.png\" alt=\"Gi, Justicia\">
                </div>
            </div>
        </div>    
        </div>
        <div class=\"promo promo-dark promo-full nobottommargin\">
            <div class=\"container clearfix center\">
                <h3>";
        // line 188
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "secciones", array()), "trayectoria", array()), "label", array()), "html", null, true);
        echo "</h3>
                
                <a class=\"btn-trayectoria\" href=\"";
        // line 190
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "secciones", array()), "trayectoria", array()), "url", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "secciones", array()), "trayectoria", array()), "boton", array()), "html", null, true);
        echo "  </a>
            </div>
        </div>

        
    </div>

</section><!-- #content end -->

";
        // line 199
        if (( !(null === (isset($context["popups"]) ? $context["popups"] : null)) && ($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "popup", array()) == 0))) {
            // line 200
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["popups"]) ? $context["popups"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["popup"]) {
                echo " 
<section id=\"content\" onclick=\"popupViewed();\">
    <div class=\"content-wrap\" onclick=\"popupViewed();\">

        <div class=\"container clearfix\" onclick=\"popupViewed();\">

            <div class=\"modal-on-load\" data-target=\"#myModalHome\" onclick=\"popupViewed();\"></div>

            <!-- Modal -->
            <div class=\"modal1 mfp-hide\" id=\"myModalHome\">
                <div class=\"block divcenter\" style=\"background-color: #FFF; max-width: 500px;\">
                    <div class=\"center\" style=\"padding: 2%;\">
                        <a href=\"#\" id=\"closemodalini\" onClick=\"\$.magnificPopup.close(); popupViewed(); return false;\">&times;</a>
                        <h3>";
                // line 213
                echo twig_escape_filter($this->env, $this->getAttribute($context["popup"], "titulo", array()), "html", null, true);
                echo "</h3> 
                        ";
                // line 214
                if (($this->getAttribute($context["popup"], "url", array()) != "")) {
                    echo "<a href=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["popup"], "url", array()), "html", null, true);
                    echo "\" ";
                    if (!twig_in_filter("mailto", $this->getAttribute($context["popup"], "url", array()))) {
                        echo "target=\"_blank\"";
                    }
                    echo ">";
                }
                // line 215
                echo "                                                 <img src=\"";
                echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
                echo "pop-up/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["popup"], "id", array()), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["popup"], "imagen", array()), "html", null, true);
                echo "\" alt=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["popup"], "descripcion", array()), "html", null, true);
                echo "\">
                            ";
                // line 216
                if (($this->getAttribute($context["popup"], "url", array()) != "")) {
                    echo "</a>";
                }
                // line 217
                echo "                    </div>
                    <div class=\"section center nomargin\" style=\"padding: 30px;\">
                        <a href=\"#\" class=\"button\" onClick=\"\$.magnificPopup.close(); popupViewed(); return false;\">Ok</a>
                    </div>
                </div>
            </div>

        </div>

    </div>

</section>

";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['popup'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
    }

    public function getTemplateName()
    {
        return "taller/home.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  489 => 217,  485 => 216,  474 => 215,  464 => 214,  460 => 213,  442 => 200,  440 => 199,  426 => 190,  421 => 188,  411 => 181,  403 => 176,  395 => 171,  387 => 166,  379 => 161,  371 => 156,  363 => 151,  345 => 136,  327 => 121,  318 => 117,  313 => 115,  307 => 112,  302 => 109,  294 => 103,  273 => 98,  258 => 96,  239 => 91,  237 => 90,  233 => 88,  229 => 87,  214 => 74,  208 => 73,  202 => 72,  191 => 70,  186 => 69,  181 => 68,  177 => 67,  173 => 66,  169 => 64,  163 => 63,  152 => 61,  147 => 60,  143 => 59,  138 => 57,  134 => 55,  119 => 52,  116 => 51,  112 => 50,  108 => 49,  101 => 45,  97 => 44,  89 => 39,  84 => 36,  82 => 35,  63 => 18,  50 => 13,  46 => 12,  42 => 11,  31 => 8,  27 => 7,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"slider\" class=\"slider-parallax swiper_wrapper clearfix\" data-autoplay=\"7000\" data-speed=\"650\" data-loop=\"true\">

    <div class=\"slider-parallax-inner\">

        <div class=\"swiper-container swiper-parent\">
            <div class=\"swiper-wrapper\">
                {%for slider in sliders%}
                <div class=\"swiper-slide dark\" style=\"background-image: url('{{imgurl}}sliders/{{slider.id}}/{{slider.imagen}}');\">
                    <div class=\"container clearfix\">
                        <div class=\"slider-caption slider-caption-center\">
                            <h2 data-caption-animate=\"fadeInUp\">{{slider.titulo|raw}}</h2>
                            <p data-caption-animate=\"fadeInUp\" data-caption-delay=\"200\">{{slider.subtitulo|raw}}</p>
                            <a data-caption-animate=\"fadeInUp\" href=\"{{slider.url}}\" class=\"button\">{{slider.boton}}</a>
                        </div>
                    </div>
                </div>
                {%endfor%}
            </div>
            <div id=\"slider-arrow-left\"><i class=\"icon-angle-left\"></i></div>
            <div id=\"slider-arrow-right\"><i class=\"icon-angle-right\"></i></div>
        </div>

    </div>

</section>



<!-- Content
============================================= -->
<section id=\"content\">

    <div class=\"nobottompadding\">
        
        {% if user.viaje != 1 and user.viaje !=2 %}
        <div class=\"fondo-productos\">
        <div class=\"container clearfix\">
            <div class=\"col-lg-5 col-md-6 hidden-sm hidden-xs\" style=\"margin-top: -20px;\">
                <img class=\"samurai-home\" alt=\"samurai\" src=\"{{imgurl}}img-catalogo-paint.png\"/>
            </div>
            
            <div class=\"col-lg-7 col-md-6 col-sm-12 col-xs-12 buscador\">
                <div class=\"clear bottommargin-lg hidden-xs\"></div>
                <h2>{{textos.buscador.titulo}}</h2>
                <p>{{textos.buscador.subtitulo}}</p>
                <div class=\"line-custom bottommargin-sm\"></div>
                <form>
                    <select>
                        <option value=\"\" disabled selected hidden>{{textos.buscador.categoria.label}}</option>
                        {%for key, j in jerarquia%}
                        <ul class=\"mega-menu-column col-5\">
                            <option value=\"{{j.nombre}}\"><a class=\"menu_white\" href=\"{{baseurl}}productos/{{key}}\">{{j.nombre}}</a></option>
                        </ul>
                        {%endfor%}
                    </select>
                    <select>
                        <option value=\"\" disabled selected hidden>{{textos.buscador.subcategoria.label}}</option>

                        {%for key, j in jerarquia%}
                                        {% for key, c in j.children%}
                        <option value=\"{{c.nombre}}\">{{c.nombre}}</option>
                        {%endfor%}
                        {%endfor%}
                    </select>
                    <select style=\"margin-right: 0;\">
                        <option value=\"\" disabled selected hidden>{{textos.buscador.familia.label}}</option>
                            {%for key, j in jerarquia%}
                                        {% for key, c in j.children%}
                                                {% for key, g in c.children%}
                        <option value=\"{{g.nombre}}\">{{g.nombre}}</option>
                        {%endfor%}
                            {%endfor%}
                        {%endfor%}
                    </select>
                    <input type=\"submit\" value=\"BUSCAR\"/>
                </form>
            </div>
            
        </div>
        
        
        
        <div class=\"container clearfix\">


            <div class=\"row galeria\">
                {%for producto in productos%}
                <div class=\"col-lg-3 col-md-6 col-sm-6 col-xs-12 producto\">
                    <div class=\"product-image product-image-catalog\">
                            {%set url= [producto.parent.categoria.slug, producto.parent.subcategoria.slug, producto.parent.familia.slug, producto.slug] %}
                            <a href=\"{{base_url()}}productos/{{producto.parent.categoria.slug}}/{{producto.parent.subcategoria.slug}}/{{producto.parent.familia.slug}}/{{producto.slug}}\" class=\"pdt_image\" style=\"background-image: url('https://apps.avalonprplus.com/uploads/{{producto.imagen}}')\"></a>
                                                        
                    </div>
                    <div class=\"product-data col-md-12 col-sm-12 col-xs-12 bottommargin-sm\">
                        
                        <a href=\"{{base_url()}}productos/{{producto.parent.categoria.slug}}/{{producto.parent.subcategoria.slug}}/{{producto.parent.familia.slug}}/{{producto.slug}}\"><h4>{{producto.nombre}}</h4></a>
                        
                        <p class=\"precio-home\"><i class=\"fas fa-angle-double-right\"></i>{%if producto.precioDto is not null%}<del>{{producto.precio|number_format(0, ',', '.')}} Km</del> <ins>{{producto.precioDto|number_format(0, ',', '.')}} Km</ins> {%else%} {{producto.precio|number_format(0, ',', '.')}} Km{% endif%}</p>

                    </div>
                </div>
                {%endfor%}
            </div>
            

        </div>
        </div>
        {%endif%}
        <div class=\"promo promo-dark promo-full nobottommargin header-stick notopborder\" style=\"overflow: hidden;\">
            <div class=\"container clearfix homes\" >
                <div class=\"promoText col-md-7 col-xs-12 topmargin-sm\">
                    <h3>{{textos.viaje.titulo}}</h3>
                    <div class=\"line-custom\"></div>
                    <span>
                        <p>{{textos.viaje.subtitulo}}</p>
                        <div class=\"clear\"></div>
                        <a href=\"{{base_url()}}inicio/viaje\" class=\"button\">{{textos.viaje.boton}} <i class=\"material-icons\">flight_takeoff</i></a>
                    </span>
                </div>
                <div class=\"promoImg col-md-5 col-xs-12\">
                        <img src=\"{{imgurl}}barco-viaje-yokohama-2019.png\" data-animate=\"fadeInUp\" data-delay=\"100\" alt=\"el gran viaje está a punto de empezar\">
\t\t
                </div>
            </div>
        </div>
          

        <div class=\"parallax light nobottommargin\" id=\"testimonial-taller\" style=\"padding: 100px 0px; background-position: 50% 216.6px;\" data-stellar-background-ratio=\"0.4\">

            <div class=\"fslider testimonial testimonial-full\" data-animation=\"fade\" data-arrows=\"false\">
                <div class=\"flexslider\" style=\"height: 227px;\">
                    <div class=\"slider-wrap\">
                        <div class=\"slide\" data-thumb-alt=\"\" style=\"width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;\">
                            <div class=\"testi-content\">
                                <div class=\"testi-meta\">
                                {{textos.secciones.entrada}}                           
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class=\"trayectoria-home\">
        <div class=\"container clearfix bg-trayectoria nobottommargin\">
            <div>
                <div id=\"justicia\" class=\"steps\">
                    <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Justicia\">Gi</span>
                    <span class=\"km\">820 km</span>
                    <img src=\"{{imgurl}}trayectoria/steps-trayectoria-01.png\" alt=\"Gi, Justicia\">
                </div>
                <div id=\"valor\" class=\"steps\">
                    <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Valor\">Yu</span>
                    <span class=\"km\">1.640 km</span>
                    <img src=\"{{imgurl}}trayectoria/steps-trayectoria-02.png\" alt=\"Yu, Valor\">
                </div>
                <div id=\"compasion\" class=\"steps\">
                    <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Compasión\">Jin</span>
                    <span class=\"km\">2.460 km</span>
                    <img src=\"{{imgurl}}trayectoria/steps-trayectoria-03.png\" alt=\"Gi, Justicia\">
                </div>
                <div id=\"cortesia\" class=\"steps\">
                    <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Cortesía\">Rei</span>
                    <span class=\"km\">3.280 km</span>
                    <img src=\"{{imgurl}}trayectoria/steps-trayectoria-04.png\" alt=\"Gi, Justicia\">
                </div>
                <div id=\"sinceridad\" class=\"steps\">
                    <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Sinceridad\">Makoto</span>
                    <span class=\"km\">4.100 km</span>
                    <img src=\"{{imgurl}}trayectoria/steps-trayectoria-05.png\" alt=\"Gi, Justicia\">
                </div>
                <div id=\"honor\" class=\"steps\">
                    <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Honor\">Meyo</span>
                    <span class=\"km\">4.920 km</span>
                    <img src=\"{{imgurl}}trayectoria/steps-trayectoria-06.png\" alt=\"Gi, Justicia\">
                </div>
                <div id=\"lealtad\" class=\"steps\">
                    <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Lealtad\">Chu</span>
                    <span class=\"km\">5.900 km</span>
                    <img src=\"{{imgurl}}trayectoria/steps-trayectoria-07.png\" alt=\"Gi, Justicia\">
                </div>
            </div>
        </div>    
        </div>
        <div class=\"promo promo-dark promo-full nobottommargin\">
            <div class=\"container clearfix center\">
                <h3>{{textos.secciones.trayectoria.label}}</h3>
                
                <a class=\"btn-trayectoria\" href=\"{{textos.secciones.trayectoria.url}}\">{{textos.secciones.trayectoria.boton}}  </a>
            </div>
        </div>

        
    </div>

</section><!-- #content end -->

{%if popups is not null and user.popup == 0%}
{%for popup in popups%} 
<section id=\"content\" onclick=\"popupViewed();\">
    <div class=\"content-wrap\" onclick=\"popupViewed();\">

        <div class=\"container clearfix\" onclick=\"popupViewed();\">

            <div class=\"modal-on-load\" data-target=\"#myModalHome\" onclick=\"popupViewed();\"></div>

            <!-- Modal -->
            <div class=\"modal1 mfp-hide\" id=\"myModalHome\">
                <div class=\"block divcenter\" style=\"background-color: #FFF; max-width: 500px;\">
                    <div class=\"center\" style=\"padding: 2%;\">
                        <a href=\"#\" id=\"closemodalini\" onClick=\"\$.magnificPopup.close(); popupViewed(); return false;\">&times;</a>
                        <h3>{{popup.titulo}}</h3> 
                        {%if popup.url != ''%}<a href=\"{{popup.url}}\" {%if 'mailto' not in popup.url%}target=\"_blank\"{%endif%}>{%endif%}
                                                 <img src=\"{{imgurl}}pop-up/{{popup.id}}/{{popup.imagen}}\" alt=\"{{popup.descripcion}}\">
                            {%if popup.url != ''%}</a>{%endif%}
                    </div>
                    <div class=\"section center nomargin\" style=\"padding: 30px;\">
                        <a href=\"#\" class=\"button\" onClick=\"\$.magnificPopup.close(); popupViewed(); return false;\">Ok</a>
                    </div>
                </div>
            </div>

        </div>

    </div>

</section>

{%endfor%}
{%endif%}", "taller/home.php", "/Applications/MAMP/htdocs/Bushido2018/application/views/taller/home.php");
    }
}
