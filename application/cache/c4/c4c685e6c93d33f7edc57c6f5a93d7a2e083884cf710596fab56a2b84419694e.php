<?php

/* index.php */
class __TwigTemplate_9f5c18b76a4834b9b4e2df5b42127584b93041c272a2122114ef5423132feb00 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("header.php", "index.php", 1)->display($context);
        // line 2
        $this->loadTemplate("content.php", "index.php", 2)->display($context);
        // line 3
        $this->loadTemplate("footer.php", "index.php", 3)->display($context);
    }

    public function getTemplateName()
    {
        return "index.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 3,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include 'header.php' %}
{% include 'content.php' %}
{% include 'footer.php' %}
", "index.php", "E:\\Programas\\MAMP\\htdocs\\Bushido2018\\application\\views\\index.php");
    }
}
