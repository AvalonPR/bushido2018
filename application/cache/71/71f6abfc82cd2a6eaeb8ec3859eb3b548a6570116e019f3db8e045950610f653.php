<?php

/* footer.php */
class __TwigTemplate_be6b21428df677753c67e22f52472d3880cced7f8017438af672295eee203ad7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "logged_in", array())) {
            // line 2
            echo "<!-- Footer
============================================= -->
<footer id=\"footer\" class=\"dark\">    
    <div id=\"cookie_directive_container\" class=\"container\" style=\"display: none\">
            <nav class=\"navbar navbar-default navbar-fixed-bottom\" id=\"cookiesf\">

                <div class=\"container \">
                <div class=\"navbar-inner navbar-content-center\" id=\"cookie_accept\">

                    
                    <p id=\"cookdesk\">
                     Solicitamos su permiso para obtener datos estadísticos de su navegación en esta web, en cumplimiento del Real 
                     Decreto-ley 13/2012. Si continúa navegando consideramos que acepta el uso de cookies. <a target=\"_blank\" href=\"inicio/legal/\" style=\"font-size: 120%\">Más información.</a>
                    </p>
                <div class=\"closecok\"><a href=\"#\"><i class=\"icon-line-cross\"></i></a></div>
                    <br>

                </div>
              </div>

            </nav>
        </div>   
    <div class=\"imagen-footer\">
    <div class=\"container \">

        <!-- Footer Widgets
        ============================================= -->
        <div class=\"footer-widgets-wrap clearfix\">

            

                <div class=\"col_one_third\">

                    <div class=\"widget clearfix\">

                        <img src=\"";
            // line 37
            echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
            echo "logo-footer-white.png\" alt=\"\" class=\"footer-logo\">

                    </div>
                </div>
                

                <div class=\"col_one_third\">

                    <div class=\"widget widget_links clearfix\">

                        <h4>Páginas</h4>

                        <ul>
                            ";
            // line 50
            if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "tipo", array()) == "1")) {
                // line 51
                echo "                            <li><a href=\"";
                echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
                echo "inicio/viaje\">Viaje</a></li>
                            <li><a href=\"";
                // line 52
                echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
                echo "productos\">Catálogo</a></li>
                            <li><a href=\"";
                // line 53
                echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
                echo "inicio/trayectoria\">Trayectoria</a></li>
                            <li><a href=\"";
                // line 54
                echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
                echo "inicio/legal\">Marketing</a></li>
                            ";
            }
            // line 56
            echo "                            <li><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
            if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "tipo", array()) == "1")) {
                echo "inicio/";
            } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "tipo", array()) == "2")) {
                echo "comercial/";
            } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "tipo", array()) == "3")) {
                echo "admin/";
            }
            echo "estadisticas\">Mis Estadísticas</a></li>
                            <li><a href=\"";
            // line 57
            echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
            if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "tipo", array()) == "1")) {
                echo "inicio/";
            } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "tipo", array()) == "2")) {
                echo "comercial/";
            } elseif (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "tipo", array()) == "3")) {
                echo "admin/";
            }
            echo "perfil\">Mi Perfil</a></li>
                        </ul>

                    </div>

                </div>

                

            

            <div class=\"col_one_third col_last\">

                
                    <h5>Sede:</h5>
                    <br>
                    <p>Paseo Doce Estrellas, 2 <br>
                    28042 - Madrid <br>
                    Madrid</p>

                    <p>Teléfono: 916 59 15 60<br>
                    Email: info@yokohamaiberia.com</p>
                    
                

            </div>

        </div><!-- .footer-widgets-wrap end -->

    </div>
    </div>
    <!-- Copyrights
    ============================================= -->
    <div id=\"copyrights\">

        

            <div class=\"center\">
                &copy; Yokohama Iberia ";
            // line 95
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
            echo "<br>
                <div class=\"copyright-links\"><a href=\"";
            // line 96
            echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
            echo "inicio/bases\">Bases legales</a> / <a href=\"";
            echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
            echo "inicio/privacidad\">Privacidad</a> / <a href=\"";
            echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
            echo "inicio/legal\">Política de cookies</a></div>
            </div>

            

        

    </div><!-- #copyrights end -->

</footer><!-- #footer end -->
<!-- Go To Top
============================================= -->
<div id=\"gotoTop\" class=\"icon-angle-up\"></div>

";
        }
        // line 111
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["footerscripts"]) ? $context["footerscripts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            // line 112
            echo $context["script"];
            echo "
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 113
        echo "    

</body>
</html>



";
    }

    public function getTemplateName()
    {
        return "footer.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  192 => 113,  184 => 112,  180 => 111,  158 => 96,  154 => 95,  106 => 57,  94 => 56,  89 => 54,  85 => 53,  81 => 52,  76 => 51,  74 => 50,  58 => 37,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{%if user.logged_in%}
<!-- Footer
============================================= -->
<footer id=\"footer\" class=\"dark\">    
    <div id=\"cookie_directive_container\" class=\"container\" style=\"display: none\">
            <nav class=\"navbar navbar-default navbar-fixed-bottom\" id=\"cookiesf\">

                <div class=\"container \">
                <div class=\"navbar-inner navbar-content-center\" id=\"cookie_accept\">

                    
                    <p id=\"cookdesk\">
                     Solicitamos su permiso para obtener datos estadísticos de su navegación en esta web, en cumplimiento del Real 
                     Decreto-ley 13/2012. Si continúa navegando consideramos que acepta el uso de cookies. <a target=\"_blank\" href=\"inicio/legal/\" style=\"font-size: 120%\">Más información.</a>
                    </p>
                <div class=\"closecok\"><a href=\"#\"><i class=\"icon-line-cross\"></i></a></div>
                    <br>

                </div>
              </div>

            </nav>
        </div>   
    <div class=\"imagen-footer\">
    <div class=\"container \">

        <!-- Footer Widgets
        ============================================= -->
        <div class=\"footer-widgets-wrap clearfix\">

            

                <div class=\"col_one_third\">

                    <div class=\"widget clearfix\">

                        <img src=\"{{imgurl}}logo-footer-white.png\" alt=\"\" class=\"footer-logo\">

                    </div>
                </div>
                

                <div class=\"col_one_third\">

                    <div class=\"widget widget_links clearfix\">

                        <h4>Páginas</h4>

                        <ul>
                            {%if user.tipo == '1' %}
                            <li><a href=\"{{baseurl}}inicio/viaje\">Viaje</a></li>
                            <li><a href=\"{{baseurl}}productos\">Catálogo</a></li>
                            <li><a href=\"{{baseurl}}inicio/trayectoria\">Trayectoria</a></li>
                            <li><a href=\"{{baseurl}}inicio/legal\">Marketing</a></li>
                            {%endif%}
                            <li><a href=\"{{baseurl}}{%if user.tipo== '1' %}inicio/{%elseif user.tipo == '2' %}comercial/{%elseif user.tipo == '3'%}admin/{%endif%}estadisticas\">Mis Estadísticas</a></li>
                            <li><a href=\"{{baseurl}}{%if user.tipo == '1' %}inicio/{%elseif user.tipo == '2'%}comercial/{%elseif user.tipo == '3'%}admin/{%endif%}perfil\">Mi Perfil</a></li>
                        </ul>

                    </div>

                </div>

                

            

            <div class=\"col_one_third col_last\">

                
                    <h5>Sede:</h5>
                    <br>
                    <p>Paseo Doce Estrellas, 2 <br>
                    28042 - Madrid <br>
                    Madrid</p>

                    <p>Teléfono: 916 59 15 60<br>
                    Email: info@yokohamaiberia.com</p>
                    
                

            </div>

        </div><!-- .footer-widgets-wrap end -->

    </div>
    </div>
    <!-- Copyrights
    ============================================= -->
    <div id=\"copyrights\">

        

            <div class=\"center\">
                &copy; Yokohama Iberia {{\"now\"|date(\"Y\")}}<br>
                <div class=\"copyright-links\"><a href=\"{{baseurl}}inicio/bases\">Bases legales</a> / <a href=\"{{baseurl}}inicio/privacidad\">Privacidad</a> / <a href=\"{{baseurl}}inicio/legal\">Política de cookies</a></div>
            </div>

            

        

    </div><!-- #copyrights end -->

</footer><!-- #footer end -->
<!-- Go To Top
============================================= -->
<div id=\"gotoTop\" class=\"icon-angle-up\"></div>

{%endif%}
{% for script in footerscripts %}
{{script|raw}}
{% endfor %}    

</body>
</html>



", "footer.php", "E:\\Programas\\MAMP\\htdocs\\Bushido2018\\application\\views\\footer.php");
    }
}
