<?php

/* header.php */
class __TwigTemplate_d54565efc0e08c765f068e56c1d932f9bf5d52937f0839dde8a79f3362c597c2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html dir=\"ltr\" lang=\"es-ES\">
    <head>

        <meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />

        ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["cssstyles"]) ? $context["cssstyles"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["style"]) {
            // line 8
            echo "        ";
            echo $context["style"];
            echo "
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['style'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "        ";
        if ($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array())) {
            // line 11
            echo "        <script>
            window.onscroll = function () {
                if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
                    document.getElementById(\"contador\").className = \"contador_user_1\";
                } else {
                    document.getElementById(\"contador\").className = \"contador_user_2\";
                }
            }
        </script>
        ";
        }
        // line 21
        echo "
        <link rel=icon href=";
        // line 22
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "favicon.png sizes=\"16x16\" type=\"image/png\">

        <!-- Document Title
        ============================================= -->
        <title>";
        // line 26
        echo twig_escape_filter($this->env, (isset($context["sitetitle"]) ? $context["sitetitle"] : null), "html", null, true);
        echo "</title>

    </head>

    <body class=\"stretched sticky-responsive-menu\">

        <!-- Document Wrapper
        ============================================= -->
        <div id=\"wrapper\" class=\"clearfix\">

            ";
        // line 36
        if ($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "tipo", array())) {
            // line 37
            echo "            <!-- Top Bar
                    ============================================= -->
            <div id=\"top-bar\">
                <!--Contador mobile-->
                        ";
            // line 41
            if ($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array())) {
                // line 42
                echo "                        <div class=\" visible-xs\" style=\"\" id=\"contador_mobile\">
                            ";
                // line 43
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array()), 0, ",", "."), "html", null, true);
                echo " km
                        </div>
                        ";
            }
            // line 46
            echo "
                <div class=\"container clearfix contenedor-top-bar\">

                    <div class=\"col_half nobottommargin float-left\">

                        
                        

                        
                        
                        
                        <!--User name
                             =============================================-->

                    
                    <div id=\"\" class=\"user-name hidden-xs\">
                        ";
            // line 62
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "bienvenido", array()), "html", null, true);
            echo " <a href='";
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            echo "inicio/perfil'>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "nombre", array()), "html", null, true);
            echo "</a>
                    </div>
                    
                    <!--End user-->
                        

                    </div>

                    <div class=\"col_half fright col_last nobottommargin\">

                        <!-- Top Social
                        ============================================= -->
                        <div id=\"top-social\">
                            
                            <!-- Enlace B2B-->
                            
                            <div class=\"b2b\">
                                ";
            // line 79
            if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "nacionalidad", array()) == "PT")) {
                // line 80
                echo "                                    <a href=\"https://profissionais.yokohamaiberia.pt/inicio/login\" target=\"_blank\"></a>
                                ";
            } else {
                // line 82
                echo "                                    <a href=\"https://profesionales.yokohamaiberia.es/inicio/login\" target=\"_blank\"></a>
                               ";
            }
            // line 84
            echo "                            </div>
                            
                            <!--End enlace B2B-->
                            
                            <!--User
                             =============================================-->
                            <div id=\"\" class=\"close-sesion\">
                            ";
            // line 91
            if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "tipo", array()) == "C")) {
                // line 92
                echo "                                <a href='";
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "inicio/logout'>
                                    ";
            } elseif (($this->getAttribute(            // line 93
(isset($context["user"]) ? $context["user"] : null), "tipo", array()) == "S")) {
                // line 94
                echo "                                <a href='";
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "comercial/logout'>
                                    ";
            } else {
                // line 96
                echo "                                <a href='";
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "admin/logout'>
                                    ";
            }
            // line 98
            echo "                                    <span class=\"close-text\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "logout", array()), "html", null, true);
            echo "</span>
                                    <i class=\"material-icons close-icon\">power_settings_new</i>                     
                                </a>
                            </div>

                            <div class=\"hidden-lg hidden-md hidden-sm\" id=\"user-mobile\">
                                ";
            // line 104
            if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "tipo", array()) == "C")) {
                // line 105
                echo "                                <div id=\"\" class=\"top-user\">
                                    <a href='";
                // line 106
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "inicio/perfil'>
                                        <span class=\"user-text\">";
                // line 107
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "perfil", array()), "html", null, true);
                echo "</span>
                                        <i class=\"material-icons user-icon\">person</i>
                                    </a>
                                </div>
                                     ";
            } elseif (($this->getAttribute(            // line 111
(isset($context["user"]) ? $context["user"] : null), "tipo", array()) == "S")) {
                // line 112
                echo "                                <div id=\"\" class=\"top-user\">
                                    <a href='";
                // line 113
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "comercial/perfil'>
                                        <span class=\"user-text\">";
                // line 114
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "perfil", array()), "html", null, true);
                echo "</span>
                                        <i class=\"material-icons user-icon\">person</i>
                                    </a>
                                </div>     ";
            } elseif (($this->getAttribute(            // line 117
(isset($context["user"]) ? $context["user"] : null), "tipo", array()) == "A")) {
                // line 118
                echo "                                <div id=\"\" class=\"top-user\">
                                    <a href='";
                // line 119
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "admin/perfil'>
                                        <span class=\"user-text\">";
                // line 120
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "perfil", array()), "html", null, true);
                echo "</span>
                                        <i class=\"material-icons user-icon\">person</i>
                                    </a>
                                </div>



                                ";
            }
            // line 128
            echo "                                
                            </div>
                            <!--End user-->
                        
                            
                        
                        </div>
                        <!-- #top-social end -->

                    </div>
                    <div class=\"clear\"></div>
                </div>

            </div><!-- #top-bar end -->

            <!-- Header
            ============================================= -->
            <header id=\"header\">

                <div id=\"header-wrap\">

                    <div class=\"container clearfix\" id=\"menu-bar\">

                        <div id=\"primary-menu-trigger\"><i class=\"icon-reorder\"></i></div>

                        <!-- Logo
                        ============================================= -->
                        <div id=\"logo\">
                            
                            <a href=\"";
            // line 157
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            echo "inicio\" class=\"standard-logo\" data-dark-logo=\"";
            echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
            echo "logo-yokohama.png\"><img src=\"";
            echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
            echo "logo-yokohama.png\" alt=\"Yokohama\"></a>
                            <a href=\"";
            // line 158
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            echo "inicio\" class=\"retina-logo\" data-dark-logo=\"";
            echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
            echo "logo-yokohama.png\"><img src=\"";
            echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
            echo "logo-yokohama.png\" alt=\"Yokohama\"></a>
                         
                            
                        </div><!-- #logo end -->
                        ";
            // line 162
            if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "tipo", array()) == "C")) {
                // line 163
                echo "                        <!-- Primary Navigation
                                ============================================= -->

                        <nav id=\"primary-menu\" class=\"style-2\">
                            <ul>
                                
                                <li><a href=\"";
                // line 169
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "inicio/perfil\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "area", array()), "html", null, true);
                echo "</a>
                                    <ul>
                                        <li><a href=\"";
                // line 171
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "inicio/perfil\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "perfil", array()), "html", null, true);
                echo "</a></li>
                                        <li><a href=\"";
                // line 172
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "inicio/trayectoria\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "trayectoria", array()), "html", null, true);
                echo "</a></li>
                                        <!--<li><a href=\"";
                // line 173
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "estadisticas\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "estadisticas", array()), "html", null, true);
                echo "</a></li>-->
                                        <li><a href=\"";
                // line 174
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "inicio/pedidos\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "pedidos", array()), "html", null, true);
                echo "</a></li>
                                    </ul>
                                </li>
                                ";
                // line 177
                if ((($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "viaje", array()) != 1) && ($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "viaje", array()) != 2))) {
                    // line 178
                    echo "                                <li><a href=\"";
                    echo twig_escape_filter($this->env, base_url(), "html", null, true);
                    echo "productos\">";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "catalogo", array()), "html", null, true);
                    echo "</a></li>
                                ";
                }
                // line 180
                echo "                                <li><a href=\"";
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "inicio/viaje\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "viaje", array()), "html", null, true);
                echo "</a></li>
                                <!--<li><a href=\"";
                // line 181
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "marketing\">MARKETING</a></li>-->
                                <li><a href=\"";
                // line 182
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "inicio/bases\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "programa", array()), "html", null, true);
                echo "</a>
                                    <ul>
                                        <li><a href=\"";
                // line 184
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "inicio/generales\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "condiciones", array()), "html", null, true);
                echo "</a></li>
                                        <li><a href=\"";
                // line 185
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "inicio/privacidad\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "privacidad", array()), "html", null, true);
                echo "</a></li>
                                        <li><a href=\"";
                // line 186
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "inicio/canjeo\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "canjeo", array()), "html", null, true);
                echo "</a></li>
                                        <li><a href=\"";
                // line 187
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "inicio/cookies\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "cookies", array()), "html", null, true);
                echo "</a></li>
                                    </ul>
                                    
                                    
                                </li>
                                <li><a href=\"";
                // line 192
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "inicio/contacto\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "contact", array()), "html", null, true);
                echo "</a></li>
                            </ul>
                        </nav>
                        
\t\t\t\t\t\t
                        <!-- Top Cart ============================================= -->
                        <div id=\"top-cart\" class=\"top-cart\">
                            <a id=\"top-cart-trigger\"";
                // line 199
                if (twig_test_empty((isset($context["cart_bushido"]) ? $context["cart_bushido"] : null))) {
                    echo "style=\"pointer-events:none; cursor:default;\"";
                }
                echo "><i class=\"icon-shopping-cart\"></i><span class=\"count\">";
                if ( !twig_test_empty((isset($context["cart_bushido"]) ? $context["cart_bushido"] : null))) {
                    echo twig_escape_filter($this->env, (isset($context["cart_bushido_items"]) ? $context["cart_bushido_items"] : null), "html", null, true);
                } else {
                    echo "0";
                }
                echo "</span></a>
                            <div class=\"top-cart-content\">
                                <div class=\"top-cart-title\">
                                    <h4>";
                // line 202
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "carrito", array()), "html", null, true);
                echo "</h4>
                                </div>
                                <div class=\"top-cart-items\">
                                    <div class=\"top-cart-item clearfix\">
                                        <ul id=\"top-cart-ul\">
                                            ";
                // line 207
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["cart_bushido"]) ? $context["cart_bushido"] : null));
                foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                    // line 208
                    echo "                                            <li class=\"top-cart-list\">
                                                <div class=\"top-cart-item-image\">
                                                    <a href=\"";
                    // line 210
                    echo twig_escape_filter($this->env, base_url(), "html", null, true);
                    echo "productos/";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "options", array()), "url", array()), "html", null, true);
                    echo "\"><img src=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "options", array()), "img", array()), "html", null, true);
                    echo "\" alt=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "name", array()), "html", null, true);
                    echo "\" /></a>
                                                </div>
                                                <div class=\"top-cart-item-desc\">
                                                    <a href=\"";
                    // line 213
                    echo twig_escape_filter($this->env, base_url(), "html", null, true);
                    echo "productos/";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "options", array()), "url", array()), "html", null, true);
                    echo "\"><span class=\"fleft\" style=\"width: 85%;\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "name", array()), "html", null, true);
                    echo "</span></a><span class=\"top-cart-item-quantity fright\">x ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "qty", array()), "html", null, true);
                    echo "</span>
                                                    <div class=\"clear\"></div>
                                                    <span class=\"top-cart-item-price fleft\">";
                    // line 215
                    echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["item"], "price", array()), 0, ",", "."), "html", null, true);
                    echo " Km</span>
                                                    <a class=\"remove fright\" title=\"Eliminar artículo\" onclick=\"removeCart('";
                    // line 216
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo "');\"><i class=\"icon-trash2\"></i></a>
                                                </div>
                                                <div class=\"clear\"></div>
                                            </li>
                                            
                                            
                                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 223
                echo "                                        </ul>
                                    </div>
                                    
                                </div>
                                        <div class=\"top-cart-action clearfix\">
                                            <span class=\"fleft top-checkout-price\">";
                // line 228
                if ( !twig_test_empty((isset($context["cart_bushido"]) ? $context["cart_bushido"] : null))) {
                    echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["cart_bushido_total"]) ? $context["cart_bushido_total"] : null), 0, ",", "."), "html", null, true);
                } else {
                    echo "0";
                }
                echo " Km</span>
                                            <a href=\"";
                // line 229
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "carrito\" class=\"button button-3d button-small nomargin fright\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "carrito", array()), "html", null, true);
                echo "</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- #top-cart end -->

                                <!-- #primary-menu end -->
                                ";
            } elseif (($this->getAttribute(            // line 236
(isset($context["user"]) ? $context["user"] : null), "tipo", array()) == "S")) {
                // line 237
                echo "                                <nav id=\"primary-menu\" class=\"style-2\">

                                    <ul>
                                        <li><a href=\"";
                // line 240
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "comercial/estadisticas\"><div>Estadísticas</div></a></li>
                                        <li><a href=\"#\"><div>Formación</div></a>
                                            <ul>
                                                <li><a href=\"";
                // line 243
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "comercial/formacion\"><div>Ver Formaciones</div></a></li>
                                            </ul>
                                        </li>
                                        <li><a href=\"";
                // line 246
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "comercial/pedidos\" ><div>Pedidos</div></a>
                                        </li>
                                        <li><a href=\"";
                // line 248
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "productos\" ><div>Ver catálogo</div></a>
                                        </li>
                                    </ul>



                                </nav><!-- #primary-menu end -->

                                ";
            } elseif (($this->getAttribute(            // line 256
(isset($context["user"]) ? $context["user"] : null), "tipo", array()) == "A")) {
                // line 257
                echo "                                <nav id=\"primary-menu\" class=\"style-2\">

                                    <ul>
                                        <li><a href=\"";
                // line 260
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "admin/estadisticas\"><div>Estadísticas</div></a></li>
                                        
                                        <li><a href=\"#\"><div>Editor </div></a>
                                            <ul>
                                                <li><a href=\"";
                // line 264
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "admin/slider/\">Sliders</a>
                                                </li>
                                                <li><a href=\"";
                // line 266
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "admin/home\">Textos Home</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href=\"";
                // line 270
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "inicio/bases\">EL PROGRAMA</a>
                                            <ul>
                                                <li><a href=\"";
                // line 272
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "inicio/bases\">Bases legales</a></li>
                                                <li><a href=\"";
                // line 273
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "estadisticas\">Canjeo</a></li>
                                                <li><a href=\"";
                // line 274
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "inicio/legal\">Privacidad</a></li>
                                                <li><a href=\"";
                // line 275
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "inicio/cookies\">Cookies</a></li>
                                            </ul>


                                        </li>
                                        
                                        <li><a href=\"";
                // line 281
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "admin/pedidos\" ><div>Pedidos</div></a></li>
                                        
                                        <li><a href=\"";
                // line 283
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "productos\" ><div>Catálogo</div></a></li>
                                       <li><a href=\"#\" ><div>Usuarios</div></a>
                                            <ul>
                                                <li><a href=\"";
                // line 286
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "admin/usuarios\">Ver</a>
                                                </li>
                                                <li><a href=\"";
                // line 288
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "admin/usuarios/add\">Añadir</a>
                                                </li>

                                            </ul>
                                        </li>
                                    </ul>
                                </nav><!-- #primary-menu end -->

                                ";
            }
            // line 296
            echo "                   
                            </div>



                        </div>

                        </header>
            <!-- #header end -->
                        ";
        }
        // line 306
        echo "                        ";
        if ($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array())) {
            // line 307
            echo "                        <!--Contador-->
                            <div style=\"font:normal 15px/15px sans-serif\">
                                <div id=\"contador\"><span class=\"puntos_contador\">";
            // line 309
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "puntos", array()), 0, ",", "."), "html", null, true);
            echo " ";
            if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "viaje", array()) == 0)) {
                echo "km";
            } else {
                echo "€";
            }
            echo "</span></div>
                            </div>
                        
                        <!--Contador end-->
                        ";
        }
    }

    public function getTemplateName()
    {
        return "header.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  632 => 309,  628 => 307,  625 => 306,  613 => 296,  601 => 288,  596 => 286,  590 => 283,  585 => 281,  576 => 275,  572 => 274,  568 => 273,  564 => 272,  559 => 270,  552 => 266,  547 => 264,  540 => 260,  535 => 257,  533 => 256,  522 => 248,  517 => 246,  511 => 243,  505 => 240,  500 => 237,  498 => 236,  486 => 229,  478 => 228,  471 => 223,  458 => 216,  454 => 215,  443 => 213,  431 => 210,  427 => 208,  423 => 207,  415 => 202,  401 => 199,  389 => 192,  379 => 187,  373 => 186,  367 => 185,  361 => 184,  354 => 182,  350 => 181,  343 => 180,  335 => 178,  333 => 177,  325 => 174,  319 => 173,  313 => 172,  307 => 171,  300 => 169,  292 => 163,  290 => 162,  279 => 158,  271 => 157,  240 => 128,  229 => 120,  225 => 119,  222 => 118,  220 => 117,  214 => 114,  210 => 113,  207 => 112,  205 => 111,  198 => 107,  194 => 106,  191 => 105,  189 => 104,  179 => 98,  173 => 96,  167 => 94,  165 => 93,  160 => 92,  158 => 91,  149 => 84,  145 => 82,  141 => 80,  139 => 79,  115 => 62,  97 => 46,  91 => 43,  88 => 42,  86 => 41,  80 => 37,  78 => 36,  65 => 26,  58 => 22,  55 => 21,  43 => 11,  40 => 10,  31 => 8,  27 => 7,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html dir=\"ltr\" lang=\"es-ES\">
    <head>

        <meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />

        {% for style in cssstyles %}
        {{style|raw}}
        {% endfor %}
        {%if user.puntos%}
        <script>
            window.onscroll = function () {
                if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
                    document.getElementById(\"contador\").className = \"contador_user_1\";
                } else {
                    document.getElementById(\"contador\").className = \"contador_user_2\";
                }
            }
        </script>
        {%endif%}

        <link rel=icon href={{imgurl}}favicon.png sizes=\"16x16\" type=\"image/png\">

        <!-- Document Title
        ============================================= -->
        <title>{{sitetitle}}</title>

    </head>

    <body class=\"stretched sticky-responsive-menu\">

        <!-- Document Wrapper
        ============================================= -->
        <div id=\"wrapper\" class=\"clearfix\">

            {% if user.tipo %}
            <!-- Top Bar
                    ============================================= -->
            <div id=\"top-bar\">
                <!--Contador mobile-->
                        {% if user.puntos %}
                        <div class=\" visible-xs\" style=\"\" id=\"contador_mobile\">
                            {{user.puntos|number_format(0,',','.')}} km
                        </div>
                        {%endif%}

                <div class=\"container clearfix contenedor-top-bar\">

                    <div class=\"col_half nobottommargin float-left\">

                        
                        

                        
                        
                        
                        <!--User name
                             =============================================-->

                    
                    <div id=\"\" class=\"user-name hidden-xs\">
                        {{menu.bienvenido}} <a href='{{base_url()}}inicio/perfil'>{{user.nombre}}</a>
                    </div>
                    
                    <!--End user-->
                        

                    </div>

                    <div class=\"col_half fright col_last nobottommargin\">

                        <!-- Top Social
                        ============================================= -->
                        <div id=\"top-social\">
                            
                            <!-- Enlace B2B-->
                            
                            <div class=\"b2b\">
                                {% if user.nacionalidad == 'PT'%}
                                    <a href=\"https://profissionais.yokohamaiberia.pt/inicio/login\" target=\"_blank\"></a>
                                {% else %}
                                    <a href=\"https://profesionales.yokohamaiberia.es/inicio/login\" target=\"_blank\"></a>
                               {% endif %}
                            </div>
                            
                            <!--End enlace B2B-->
                            
                            <!--User
                             =============================================-->
                            <div id=\"\" class=\"close-sesion\">
                            {%if user.tipo=='C'%}
                                <a href='{{base_url()}}inicio/logout'>
                                    {%elseif user.tipo=='S'%}
                                <a href='{{base_url()}}comercial/logout'>
                                    {%else%}
                                <a href='{{base_url()}}admin/logout'>
                                    {%endif%}
                                    <span class=\"close-text\">{{menu.logout}}</span>
                                    <i class=\"material-icons close-icon\">power_settings_new</i>                     
                                </a>
                            </div>

                            <div class=\"hidden-lg hidden-md hidden-sm\" id=\"user-mobile\">
                                {%if user.tipo=='C'%}
                                <div id=\"\" class=\"top-user\">
                                    <a href='{{base_url()}}inicio/perfil'>
                                        <span class=\"user-text\">{{menu.perfil}}</span>
                                        <i class=\"material-icons user-icon\">person</i>
                                    </a>
                                </div>
                                     {%elseif user.tipo=='S'%}
                                <div id=\"\" class=\"top-user\">
                                    <a href='{{base_url()}}comercial/perfil'>
                                        <span class=\"user-text\">{{menu.perfil}}</span>
                                        <i class=\"material-icons user-icon\">person</i>
                                    </a>
                                </div>     {%elseif user.tipo=='A'%}
                                <div id=\"\" class=\"top-user\">
                                    <a href='{{base_url()}}admin/perfil'>
                                        <span class=\"user-text\">{{menu.perfil}}</span>
                                        <i class=\"material-icons user-icon\">person</i>
                                    </a>
                                </div>



                                {%endif%}
                                
                            </div>
                            <!--End user-->
                        
                            
                        
                        </div>
                        <!-- #top-social end -->

                    </div>
                    <div class=\"clear\"></div>
                </div>

            </div><!-- #top-bar end -->

            <!-- Header
            ============================================= -->
            <header id=\"header\">

                <div id=\"header-wrap\">

                    <div class=\"container clearfix\" id=\"menu-bar\">

                        <div id=\"primary-menu-trigger\"><i class=\"icon-reorder\"></i></div>

                        <!-- Logo
                        ============================================= -->
                        <div id=\"logo\">
                            
                            <a href=\"{{base_url()}}inicio\" class=\"standard-logo\" data-dark-logo=\"{{imgurl}}logo-yokohama.png\"><img src=\"{{imgurl}}logo-yokohama.png\" alt=\"Yokohama\"></a>
                            <a href=\"{{base_url()}}inicio\" class=\"retina-logo\" data-dark-logo=\"{{imgurl}}logo-yokohama.png\"><img src=\"{{imgurl}}logo-yokohama.png\" alt=\"Yokohama\"></a>
                         
                            
                        </div><!-- #logo end -->
                        {% if user.tipo=='C'%}
                        <!-- Primary Navigation
                                ============================================= -->

                        <nav id=\"primary-menu\" class=\"style-2\">
                            <ul>
                                
                                <li><a href=\"{{base_url()}}inicio/perfil\">{{menu.area}}</a>
                                    <ul>
                                        <li><a href=\"{{base_url()}}inicio/perfil\">{{menu.perfil}}</a></li>
                                        <li><a href=\"{{base_url()}}inicio/trayectoria\">{{menu.trayectoria}}</a></li>
                                        <!--<li><a href=\"{{base_url()}}estadisticas\">{{menu.estadisticas}}</a></li>-->
                                        <li><a href=\"{{base_url()}}inicio/pedidos\">{{menu.pedidos}}</a></li>
                                    </ul>
                                </li>
                                {%if user.viaje != 1 and user.viaje !=2%}
                                <li><a href=\"{{base_url()}}productos\">{{menu.catalogo}}</a></li>
                                {%endif%}
                                <li><a href=\"{{base_url()}}inicio/viaje\">{{menu.viaje}}</a></li>
                                <!--<li><a href=\"{{base_url()}}marketing\">MARKETING</a></li>-->
                                <li><a href=\"{{base_url()}}inicio/bases\">{{menu.programa}}</a>
                                    <ul>
                                        <li><a href=\"{{base_url()}}inicio/generales\">{{menu.condiciones}}</a></li>
                                        <li><a href=\"{{base_url()}}inicio/privacidad\">{{menu.privacidad}}</a></li>
                                        <li><a href=\"{{base_url()}}inicio/canjeo\">{{menu.canjeo}}</a></li>
                                        <li><a href=\"{{base_url()}}inicio/cookies\">{{menu.cookies}}</a></li>
                                    </ul>
                                    
                                    
                                </li>
                                <li><a href=\"{{base_url()}}inicio/contacto\">{{menu.contact}}</a></li>
                            </ul>
                        </nav>
                        
\t\t\t\t\t\t
                        <!-- Top Cart ============================================= -->
                        <div id=\"top-cart\" class=\"top-cart\">
                            <a id=\"top-cart-trigger\"{%if cart_bushido is empty%}style=\"pointer-events:none; cursor:default;\"{%endif%}><i class=\"icon-shopping-cart\"></i><span class=\"count\">{%if cart_bushido is not empty%}{{cart_bushido_items}}{%else%}0{%endif%}</span></a>
                            <div class=\"top-cart-content\">
                                <div class=\"top-cart-title\">
                                    <h4>{{menu.carrito}}</h4>
                                </div>
                                <div class=\"top-cart-items\">
                                    <div class=\"top-cart-item clearfix\">
                                        <ul id=\"top-cart-ul\">
                                            {%for key, item in cart_bushido%}
                                            <li class=\"top-cart-list\">
                                                <div class=\"top-cart-item-image\">
                                                    <a href=\"{{base_url()}}productos/{{item.options.url}}\"><img src=\"{{item.options.img}}\" alt=\"{{item.name}}\" /></a>
                                                </div>
                                                <div class=\"top-cart-item-desc\">
                                                    <a href=\"{{base_url()}}productos/{{item.options.url}}\"><span class=\"fleft\" style=\"width: 85%;\">{{item.name}}</span></a><span class=\"top-cart-item-quantity fright\">x {{item.qty}}</span>
                                                    <div class=\"clear\"></div>
                                                    <span class=\"top-cart-item-price fleft\">{{item.price|number_format(0, ',', '.')}} Km</span>
                                                    <a class=\"remove fright\" title=\"Eliminar artículo\" onclick=\"removeCart('{{key}}');\"><i class=\"icon-trash2\"></i></a>
                                                </div>
                                                <div class=\"clear\"></div>
                                            </li>
                                            
                                            
                                            {%endfor%}
                                        </ul>
                                    </div>
                                    
                                </div>
                                        <div class=\"top-cart-action clearfix\">
                                            <span class=\"fleft top-checkout-price\">{%if cart_bushido is not empty%}{{cart_bushido_total|number_format(0, ',', '.')}}{%else%}0{%endif%} Km</span>
                                            <a href=\"{{base_url()}}carrito\" class=\"button button-3d button-small nomargin fright\">{{menu.carrito}}</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- #top-cart end -->

                                <!-- #primary-menu end -->
                                {%elseif user.tipo=='S'%}
                                <nav id=\"primary-menu\" class=\"style-2\">

                                    <ul>
                                        <li><a href=\"{{base_url()}}comercial/estadisticas\"><div>Estadísticas</div></a></li>
                                        <li><a href=\"#\"><div>Formación</div></a>
                                            <ul>
                                                <li><a href=\"{{base_url()}}comercial/formacion\"><div>Ver Formaciones</div></a></li>
                                            </ul>
                                        </li>
                                        <li><a href=\"{{base_url()}}comercial/pedidos\" ><div>Pedidos</div></a>
                                        </li>
                                        <li><a href=\"{{base_url()}}productos\" ><div>Ver catálogo</div></a>
                                        </li>
                                    </ul>



                                </nav><!-- #primary-menu end -->

                                {%elseif user.tipo=='A'%}
                                <nav id=\"primary-menu\" class=\"style-2\">

                                    <ul>
                                        <li><a href=\"{{base_url()}}admin/estadisticas\"><div>Estadísticas</div></a></li>
                                        
                                        <li><a href=\"#\"><div>Editor </div></a>
                                            <ul>
                                                <li><a href=\"{{base_url()}}admin/slider/\">Sliders</a>
                                                </li>
                                                <li><a href=\"{{base_url()}}admin/home\">Textos Home</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href=\"{{base_url()}}inicio/bases\">EL PROGRAMA</a>
                                            <ul>
                                                <li><a href=\"{{base_url()}}inicio/bases\">Bases legales</a></li>
                                                <li><a href=\"{{base_url()}}estadisticas\">Canjeo</a></li>
                                                <li><a href=\"{{base_url()}}inicio/legal\">Privacidad</a></li>
                                                <li><a href=\"{{base_url()}}inicio/cookies\">Cookies</a></li>
                                            </ul>


                                        </li>
                                        
                                        <li><a href=\"{{base_url()}}admin/pedidos\" ><div>Pedidos</div></a></li>
                                        
                                        <li><a href=\"{{base_url()}}productos\" ><div>Catálogo</div></a></li>
                                       <li><a href=\"#\" ><div>Usuarios</div></a>
                                            <ul>
                                                <li><a href=\"{{base_url()}}admin/usuarios\">Ver</a>
                                                </li>
                                                <li><a href=\"{{base_url()}}admin/usuarios/add\">Añadir</a>
                                                </li>

                                            </ul>
                                        </li>
                                    </ul>
                                </nav><!-- #primary-menu end -->

                                {%endif%}                   
                            </div>



                        </div>

                        </header>
            <!-- #header end -->
                        {%endif%}
                        {% if user.puntos %}
                        <!--Contador-->
                            <div style=\"font:normal 15px/15px sans-serif\">
                                <div id=\"contador\"><span class=\"puntos_contador\">{{user.puntos|number_format(0,',','.')}} {%if user.viaje==0%}km{%else%}€{%endif%}</span></div>
                            </div>
                        
                        <!--Contador end-->
                        {%endif%}
", "header.php", "/Applications/MAMP/htdocs/Bushido2018/application/views/header.php");
    }
}
