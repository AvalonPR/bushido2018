<?php

/* contact.php */
class __TwigTemplate_e7e2a9f73425e638ef805b1f1a5de9b8068aa297970530252ea43e34ee85545b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<!-- Page Title
============================================= -->
<section id=\"page-title\" class=\"header contacto_header\">

    <div class=\"container clearfix\">
        <h1>";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "titulo", array()), "html", null, true);
        echo "</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id=\"content\" class=\"bg-contacto\">

    <div class=\"content-wrap \">

        <div class=\"container clearfix\">
            
            

            <!-- Postcontent
            ============================================= -->
            <div class=\"postcontent nobottommargin\">

                <h3>";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "entradilla", array()), "html", null, true);
        echo "</h3>

                <div class=\"contact-widget formulario_contacto\">

                    <div class=\"contact-form-result\"></div>
                <div class=\"contact-widget\">
                    <div class=\"contact-form-result\"></div>

                    <form class=\"nobottommargin\" id=\"template-contactform\" name=\"template-contactform\" action=\"";
        // line 34
        echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
        echo "inicio/sendContact\" method=\"post\">

                        <div class=\"form-process\"></div>

                        <div class=\"col_one_third\">
                            <label for=\"template-contactform-name\">";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "tabla", array()), "nombre", array()), "html", null, true);
        echo " <small>*</small></label>
                            <input type=\"text\" id=\"template-contactform-name\" name=\"name\" value=\"\" class=\"sm-form-control required\" />
                        </div>

                        <div class=\"col_one_third\">
                            <label for=\"template-contactform-email\">";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "tabla", array()), "email", array()), "html", null, true);
        echo " <small>*</small></label>
                            <input type=\"email\" id=\"template-contactform-email\" name=\"email\" value=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "email", array()), "html", null, true);
        echo "\" class=\"required email sm-form-control\" readonly=\"true\"/>
                        </div>

                        <div class=\"col_one_third col_last\">
                            <label for=\"template-contactform-phone\">";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "tabla", array()), "tfno", array()), "html", null, true);
        echo "</label>
                            <input type=\"text\" id=\"template-contactform-phone\" name=\"phone\" value=\"\" class=\"sm-form-control\" />
                        </div>

                        <div class=\"clear\"></div>

                        <div class=\"col_full\">
                            <label for=\"template-contactform-subject\">";
        // line 56
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "tabla", array()), "asunto", array()), "html", null, true);
        echo " <small>*</small></label>
                            <input type=\"text\" id=\"template-contactform-subject\" name=\"subject\" value=\"\" class=\"required sm-form-control\" />
                        </div>

                        <div class=\"clear\"></div>

                        <div class=\"col_full\">
                            <label for=\"template-contactform-message\">";
        // line 63
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "tabla", array()), "msn", array()), "html", null, true);
        echo " <small>*</small></label>
                            <textarea class=\"required sm-form-control\" id=\"template-contactform-message\" name=\"message\" rows=\"6\" cols=\"30\"></textarea>
                        </div>

                        <div class=\"col_full hidden\">
                            <input type=\"text\" id=\"template-contactform-botcheck\" name=\"template-contactform-botcheck\" value=\"\" class=\"sm-form-control\" />
                        </div>

                        <div class=\"col_full\">
                            <button class=\"button\" type=\"submit\" id=\"template-contactform-submit\" name=\"template-contactform-submit\" value=\"submit\">";
        // line 72
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "butt", array()), "html", null, true);
        echo "</button>
                        </div>

                    </form>
                </div>

                </div>
                <p>";
        // line 79
        echo $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "claim", array());
        echo "</p>

            </div><!-- .postcontent end -->
            
            
            <!-- Sidebar
            ============================================= -->
            <div class=\"sidebar nobottommargin col_last clearfix\">
                    <div class=\"sidebar-widgets-wrap\">

                        <div class=\"widget clearfix\">
                            <h3><img src=\"";
        // line 90
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "logo-yokohama-dark.png\" style=\"\"></h3>
                            <br>
                            <div id=\"dir-sidebar\">
                                
                                    <p>";
        // line 94
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "dir", array()), "html", null, true);
        echo "<br>
                                    ";
        // line 95
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "complete", array()), "html", null, true);
        echo " <br>

                                    <p style=\"margin-bottom: 0;\">";
        // line 97
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "tfno", array()), "html", null, true);
        echo "<br>
                                        ";
        // line 98
        echo $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "email", array());
        echo "</p>
                            </div>

                        </div>

                            <div class=\"widget clearfix\">

\t\t\t\t<div id=\"google-map1\" style=\"height: 250px;\" class=\"gmap\"></div>\t\t\t\t

                            </div>\t\t\t\t

                    </div>
            </div>
            <!-- .sidebar end -->

        </div>

    </div>

</section>
";
    }

    public function getTemplateName()
    {
        return "contact.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  163 => 98,  159 => 97,  154 => 95,  150 => 94,  143 => 90,  129 => 79,  119 => 72,  107 => 63,  97 => 56,  87 => 49,  80 => 45,  76 => 44,  68 => 39,  60 => 34,  49 => 26,  27 => 7,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("
<!-- Page Title
============================================= -->
<section id=\"page-title\" class=\"header contacto_header\">

    <div class=\"container clearfix\">
        <h1>{{textos.titulo}}</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id=\"content\" class=\"bg-contacto\">

    <div class=\"content-wrap \">

        <div class=\"container clearfix\">
            
            

            <!-- Postcontent
            ============================================= -->
            <div class=\"postcontent nobottommargin\">

                <h3>{{textos.entradilla}}</h3>

                <div class=\"contact-widget formulario_contacto\">

                    <div class=\"contact-form-result\"></div>
                <div class=\"contact-widget\">
                    <div class=\"contact-form-result\"></div>

                    <form class=\"nobottommargin\" id=\"template-contactform\" name=\"template-contactform\" action=\"{{baseurl}}inicio/sendContact\" method=\"post\">

                        <div class=\"form-process\"></div>

                        <div class=\"col_one_third\">
                            <label for=\"template-contactform-name\">{{textos.tabla.nombre}} <small>*</small></label>
                            <input type=\"text\" id=\"template-contactform-name\" name=\"name\" value=\"\" class=\"sm-form-control required\" />
                        </div>

                        <div class=\"col_one_third\">
                            <label for=\"template-contactform-email\">{{textos.tabla.email}} <small>*</small></label>
                            <input type=\"email\" id=\"template-contactform-email\" name=\"email\" value=\"{{user.email}}\" class=\"required email sm-form-control\" readonly=\"true\"/>
                        </div>

                        <div class=\"col_one_third col_last\">
                            <label for=\"template-contactform-phone\">{{textos.tabla.tfno}}</label>
                            <input type=\"text\" id=\"template-contactform-phone\" name=\"phone\" value=\"\" class=\"sm-form-control\" />
                        </div>

                        <div class=\"clear\"></div>

                        <div class=\"col_full\">
                            <label for=\"template-contactform-subject\">{{textos.tabla.asunto}} <small>*</small></label>
                            <input type=\"text\" id=\"template-contactform-subject\" name=\"subject\" value=\"\" class=\"required sm-form-control\" />
                        </div>

                        <div class=\"clear\"></div>

                        <div class=\"col_full\">
                            <label for=\"template-contactform-message\">{{textos.tabla.msn}} <small>*</small></label>
                            <textarea class=\"required sm-form-control\" id=\"template-contactform-message\" name=\"message\" rows=\"6\" cols=\"30\"></textarea>
                        </div>

                        <div class=\"col_full hidden\">
                            <input type=\"text\" id=\"template-contactform-botcheck\" name=\"template-contactform-botcheck\" value=\"\" class=\"sm-form-control\" />
                        </div>

                        <div class=\"col_full\">
                            <button class=\"button\" type=\"submit\" id=\"template-contactform-submit\" name=\"template-contactform-submit\" value=\"submit\">{{textos.butt}}</button>
                        </div>

                    </form>
                </div>

                </div>
                <p>{{textos.claim|raw}}</p>

            </div><!-- .postcontent end -->
            
            
            <!-- Sidebar
            ============================================= -->
            <div class=\"sidebar nobottommargin col_last clearfix\">
                    <div class=\"sidebar-widgets-wrap\">

                        <div class=\"widget clearfix\">
                            <h3><img src=\"{{imgurl}}logo-yokohama-dark.png\" style=\"\"></h3>
                            <br>
                            <div id=\"dir-sidebar\">
                                
                                    <p>{{textos.dir}}<br>
                                    {{textos.complete}} <br>

                                    <p style=\"margin-bottom: 0;\">{{textos.tfno}}<br>
                                        {{textos.email|raw}}</p>
                            </div>

                        </div>

                            <div class=\"widget clearfix\">

\t\t\t\t<div id=\"google-map1\" style=\"height: 250px;\" class=\"gmap\"></div>\t\t\t\t

                            </div>\t\t\t\t

                    </div>
            </div>
            <!-- .sidebar end -->

        </div>

    </div>

</section>
", "contact.php", "E:\\Programas\\MAMP\\htdocs\\Bushido2018\\application\\views\\contact.php");
    }
}
