<?php

/* 404.php */
class __TwigTemplate_bc426aff73b7b655eb4cb431c3a8528a86cfd7911bd3599754901f684ac7059c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<?php

include 'header.php';
?>

<section id=\"slider\" class=\"slider-parallax full-screen dark error404-wrap\" style=\"background: url(";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "/bg-login.jpg) center;\">
    <div class=\"slider-parallax-inner\">

        <div class=\"container vertical-middle center clearfix\">

            <div class=\"error404\">404</div>

            <div class=\"heading-block nobottomborder\">
                <h4>Parece que no podemos encontrar la página que estabas buscando.</h4>
            </div>

            

        </div>

    </div>
</section>
<?php

include 'footer.php'
?>";
    }

    public function getTemplateName()
    {
        return "404.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 6,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php

include 'header.php';
?>

<section id=\"slider\" class=\"slider-parallax full-screen dark error404-wrap\" style=\"background: url({{imgurl}}/bg-login.jpg) center;\">
    <div class=\"slider-parallax-inner\">

        <div class=\"container vertical-middle center clearfix\">

            <div class=\"error404\">404</div>

            <div class=\"heading-block nobottomborder\">
                <h4>Parece que no podemos encontrar la página que estabas buscando.</h4>
            </div>

            

        </div>

    </div>
</section>
<?php

include 'footer.php'
?>", "404.php", "E:\\Programas\\MAMP\\htdocs\\Bushido2018\\application\\views\\404.php");
    }
}
