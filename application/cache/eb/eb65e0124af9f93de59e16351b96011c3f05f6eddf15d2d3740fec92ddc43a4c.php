<?php

/* viaje.php */
class __TwigTemplate_6968c45be669ac9b8c0aa01c11b2d69e8056ca9d398193542836927fe88a8e10 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "

<!-- Page Title
                ============================================= -->
<section id=\"page-title\" class=\"header viaje_header\">

    <div class=\"container clearfix\">
        <h1 style=\"\">";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "titulo", array()), "html", null, true);
        echo "</h1>
    </div>

</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id=\"content\">


    <div class=\"content-wrap\" style=\"padding-bottom: 0;\">

        <div class=\"container clearfix\">

            <div class=\"row clearfix\">

                <div class=\"col-sm-12\">

                    <div class=\"clear\"></div>

                    <div class=\"row clearfix\">

                        <div class=\"condicionesViaje col-md-12 bottommargin-lg\">
                            
                            

                            ";
        // line 33
        if ((null === $this->getAttribute((isset($context["userData"]) ? $context["userData"] : null), "viaje", array()))) {
            // line 34
            echo "                            
                            <h3 style=\"text-align: center;\">";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "subtitulo", array()), "html", null, true);
            echo "</h3>
                            
                            <p>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "bienvenida", array()), "html", null, true);
            echo "</p>
                            
                            <p>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "condiciones", array()), "html", null, true);
            echo "*</p>
                            
                            <div class=\"col-md-6 col-xs-12\">
                                <img class=\"img-responsive\" src=\"";
            // line 42
            echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
            echo "facturacion_01.jpg\" alt=\"\">
                            </div>
                            <div class=\"col-md-6 col-xs-12\">
                                <img class=\"img-responsive\" src=\"";
            // line 45
            echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
            echo "facturacion_02.jpg\" alt=\"\">
                            </div>
                            
                            <ul>
                                <li>";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "dosplazas", array()), "html", null, true);
            echo "</li>
                                <li>";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "unaplaza", array()), "html", null, true);
            echo "</li>
                            </ul>
                            <div class=\"style-msg\" style=\"background-color: #EEE;\">
                                <div class=\"sb-msg\"><b>";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "sinviaje", array()), "html", null, true);
            echo " </b></div>
                            </div>
                            
                            
                            <div class=\"center\" style=\"margin-bottom: 20px;\">
                                <a href=\"#myModal1\" data-lightbox=\"inline\" class=\"button button-large button-rounded\">";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "boton", array()), "html", null, true);
            echo "</a>\t\t\t\t\t\t
                            </div>
                            <small>*";
            // line 60
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "corte", array()), "html", null, true);
            echo "</small>
                            <img src=\"";
            // line 61
            echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
            echo "viaje-bushido.jpg\" alt=\"Viaje Yokohama Bushido 2020-2021\">
                        
                            <!-- Modal -->
                            <div class=\"modal1 mfp-hide\" id=\"myModal1\">
                                <div class=\"block divcenter\" style=\"background-color: #dc0026; max-width: 520px;\">
                                    <button title=\"Close (Esc)\" type=\"button\" class=\"mfp-close\">×</button>
                                    <div class=\"row nomargin clearfix\">

                                        <div class=\"col-padding\" data-height-xl=\"350\" data-height-lg=\"400\" data-height-md=\"456\" data-height-sm=\"456\" data-height-xs=\"456\">

                                            <h3>";
            // line 71
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "estimado", array()), "html", null, true);
            echo "</h3>
                                            
                                            <p>";
            // line 73
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "eleccion", array()), "html", null, true);
            echo "</p>
                                            <form class=\"form-legales clearfix\" id=\"aceptar-viaje\" >
                                                <div>
                                                    <input id=\"radio-10\" class=\"radio-style\" name=\"radio-viaje\" type=\"radio\" value=\"1\">
                                                    <label for=\"radio-10\" class=\"radio-style-3-label\">1 ";
            // line 77
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "plaza", array()), "html", null, true);
            echo "</label>
                                                </div>
                                                <div id=\"yearSelect\" style='display: none;'>
                                                    <p>";
            // line 80
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "eleccionyear", array()), "html", null, true);
            echo "</p>
                                                    <div>
                                                        <input id=\"radio-2020\" class=\"radio-style\"name=\"radio-year\" type=\"radio\" value=\"2020\" checked>
                                                        <label for=\"radio-2020\" class=\"radio-style-3-label radio-small\">2020</label>
                                                    </div>
                                                    <div>
                                                        <input id=\"radio-2021\" class=\"radio-style\" name=\"radio-year\" type=\"radio\" value=\"2021\">
                                                        <label for=\"radio-2021\" class=\"radio-style-3-label radio-small\">2021</label>
                                                    </div>
                                                </div>
                                                <div>
                                                    <input id=\"radio-11\" class=\"radio-style\"name=\"radio-viaje\" type=\"radio\" value=\"2\">
                                                    <label for=\"radio-11\" class=\"radio-style-3-label\">2 ";
            // line 92
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "plazas", array()), "html", null, true);
            echo "</label>
                                                </div>
                                                <div>
                                                    <input id=\"radio-12\" class=\"radio-style\" name=\"radio-viaje\" type=\"radio\" value=\"0\">
                                                    <label for=\"radio-12\" class=\"radio-style-3-label\">";
            // line 96
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "participar", array()), "html", null, true);
            echo "</label>
                                                </div>
                                                <button class=\"button button-dark fright\" onclick=\"aceptarViaje()\">";
            // line 98
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "aceptar", array()), "html", null, true);
            echo "</button></p>
                                                <div class=\"clear\"></div>
                                            </form>
                                            

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Fin Modal bases legales-->    
                            ";
        } else {
            // line 109
            echo "                            
                            <h3>";
            // line 110
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "subtitulo", array()), "html", null, true);
            echo "</h3>
                            <p>";
            // line 111
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "descarga", array()), "html", null, true);
            echo "</p>
                            
                            ";
            // line 113
            if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "idioma", array()) == "PT")) {
                // line 114
                echo "                            <a href=\"";
                echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
                echo "uploads/programa_viajem_Bushido_2019_Oporto.pdf\" class=\"button button-rounded button-reveal button-large button-red tright bottommargin-sm\"><i class=\"far fa-file-pdf\"></i><span>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "botonprograma", array()), "html", null, true);
                echo "/OPORTO</span></a>
                            <a href=\"";
                // line 115
                echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
                echo "uploads/programa_viajem_Bushido_2019_Lisboa.pdf\" class=\"button button-rounded button-reveal button-large button-red tright bottommargin-sm\"><i class=\"far fa-file-pdf\"></i><span>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "botonprograma", array()), "html", null, true);
                echo "/LISBOA</span></a>
                            <div class=\"divider\"><i class=\"icon-circle\"></i></div>
                            ";
            } else {
                // line 118
                echo "                            
                            <a href=\"";
                // line 119
                echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
                echo "uploads/programa_viaje_Bushido_2019.pdf\" class=\"button button-rounded button-reveal button-large button-red tright bottommargin-sm\"><i class=\"far fa-file-pdf\"></i><span>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "botonprograma", array()), "html", null, true);
                echo "</span></a>
                            <div class=\"divider\"><i class=\"icon-circle\"></i></div>

                            ";
            }
            // line 123
            echo "                            

                            <img src=\"";
            // line 125
            echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
            echo "viaje-bushido-vietnam.jpg\" alt=\"Viaje Yokohama Bushido 2020-2021\">
                            
                            

                            
                            <h3>";
            // line 130
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "condiciones", array()), "html", null, true);
            echo "</h3>
                            <div class=\"col-md-6 col-xs-12\">
                                <img class=\"img-responsive\" src=\"";
            // line 132
            echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
            echo "facturacion_01.jpg\" alt=\"\">
                            </div>
                            <div class=\"col-md-6 col-xs-12\">
                                <img class=\"img-responsive\" src=\"";
            // line 135
            echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
            echo "facturacion_02.jpg\" alt=\"\">
                            </div>
                            <ul>
                                <li>";
            // line 138
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "dosplazas", array()), "html", null, true);
            echo "</li>
                                <li>";
            // line 139
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "unaplaza", array()), "html", null, true);
            echo "</li>
                            </ul>
                            <div class=\"style-msg\" style=\"background-color: #EEE;\">
                                <div class=\"sb-msg\"><b>";
            // line 142
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "sinviaje", array()), "html", null, true);
            echo " </b></div>
                            </div>
                            
                            <p>*";
            // line 145
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "corte", array()), "html", null, true);
            echo "</p>
                            
                            
                            ";
        }
        // line 149
        echo "                            <div class=\"divider\"><i class=\"icon-circle\"></i></div>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!--<div class=\"col_full viaje_call center\">
        <h3 style=\"color:white;\">¿Ya has hecho las maletas?</h3>
        <a href=\"";
        // line 164
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "uploads/agenda_CruceroBushido_2018.pdf\" class=\"button button-xlarge tright button-dark\" id=\"travel_button\">Descarga aquí la agenda del viaje <i class=\"material-icons\">cloud_download</i></a></br>
    <span>&nbsp;</span>
</div>-->
</section><!-- #content end -->
";
    }

    public function getTemplateName()
    {
        return "viaje.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  299 => 164,  282 => 149,  275 => 145,  269 => 142,  263 => 139,  259 => 138,  253 => 135,  247 => 132,  242 => 130,  234 => 125,  230 => 123,  221 => 119,  218 => 118,  210 => 115,  203 => 114,  201 => 113,  196 => 111,  192 => 110,  189 => 109,  175 => 98,  170 => 96,  163 => 92,  148 => 80,  142 => 77,  135 => 73,  130 => 71,  117 => 61,  113 => 60,  108 => 58,  100 => 53,  94 => 50,  90 => 49,  83 => 45,  77 => 42,  71 => 39,  66 => 37,  61 => 35,  58 => 34,  56 => 33,  28 => 8,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("

<!-- Page Title
                ============================================= -->
<section id=\"page-title\" class=\"header viaje_header\">

    <div class=\"container clearfix\">
        <h1 style=\"\">{{textos.titulo}}</h1>
    </div>

</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id=\"content\">


    <div class=\"content-wrap\" style=\"padding-bottom: 0;\">

        <div class=\"container clearfix\">

            <div class=\"row clearfix\">

                <div class=\"col-sm-12\">

                    <div class=\"clear\"></div>

                    <div class=\"row clearfix\">

                        <div class=\"condicionesViaje col-md-12 bottommargin-lg\">
                            
                            

                            {% if userData.viaje is null%}
                            
                            <h3 style=\"text-align: center;\">{{textos.subtitulo}}</h3>
                            
                            <p>{{textos.bienvenida}}</p>
                            
                            <p>{{textos.condiciones}}*</p>
                            
                            <div class=\"col-md-6 col-xs-12\">
                                <img class=\"img-responsive\" src=\"{{imgurl}}facturacion_01.jpg\" alt=\"\">
                            </div>
                            <div class=\"col-md-6 col-xs-12\">
                                <img class=\"img-responsive\" src=\"{{imgurl}}facturacion_02.jpg\" alt=\"\">
                            </div>
                            
                            <ul>
                                <li>{{textos.dosplazas}}</li>
                                <li>{{textos.unaplaza}}</li>
                            </ul>
                            <div class=\"style-msg\" style=\"background-color: #EEE;\">
                                <div class=\"sb-msg\"><b>{{textos.sinviaje}} </b></div>
                            </div>
                            
                            
                            <div class=\"center\" style=\"margin-bottom: 20px;\">
                                <a href=\"#myModal1\" data-lightbox=\"inline\" class=\"button button-large button-rounded\">{{textos.boton}}</a>\t\t\t\t\t\t
                            </div>
                            <small>*{{textos.corte}}</small>
                            <img src=\"{{imgurl}}viaje-bushido.jpg\" alt=\"Viaje Yokohama Bushido 2020-2021\">
                        
                            <!-- Modal -->
                            <div class=\"modal1 mfp-hide\" id=\"myModal1\">
                                <div class=\"block divcenter\" style=\"background-color: #dc0026; max-width: 520px;\">
                                    <button title=\"Close (Esc)\" type=\"button\" class=\"mfp-close\">×</button>
                                    <div class=\"row nomargin clearfix\">

                                        <div class=\"col-padding\" data-height-xl=\"350\" data-height-lg=\"400\" data-height-md=\"456\" data-height-sm=\"456\" data-height-xs=\"456\">

                                            <h3>{{textos.estimado}}</h3>
                                            
                                            <p>{{textos.eleccion}}</p>
                                            <form class=\"form-legales clearfix\" id=\"aceptar-viaje\" >
                                                <div>
                                                    <input id=\"radio-10\" class=\"radio-style\" name=\"radio-viaje\" type=\"radio\" value=\"1\">
                                                    <label for=\"radio-10\" class=\"radio-style-3-label\">1 {{textos.plaza}}</label>
                                                </div>
                                                <div id=\"yearSelect\" style='display: none;'>
                                                    <p>{{textos.eleccionyear}}</p>
                                                    <div>
                                                        <input id=\"radio-2020\" class=\"radio-style\"name=\"radio-year\" type=\"radio\" value=\"2020\" checked>
                                                        <label for=\"radio-2020\" class=\"radio-style-3-label radio-small\">2020</label>
                                                    </div>
                                                    <div>
                                                        <input id=\"radio-2021\" class=\"radio-style\" name=\"radio-year\" type=\"radio\" value=\"2021\">
                                                        <label for=\"radio-2021\" class=\"radio-style-3-label radio-small\">2021</label>
                                                    </div>
                                                </div>
                                                <div>
                                                    <input id=\"radio-11\" class=\"radio-style\"name=\"radio-viaje\" type=\"radio\" value=\"2\">
                                                    <label for=\"radio-11\" class=\"radio-style-3-label\">2 {{textos.plazas}}</label>
                                                </div>
                                                <div>
                                                    <input id=\"radio-12\" class=\"radio-style\" name=\"radio-viaje\" type=\"radio\" value=\"0\">
                                                    <label for=\"radio-12\" class=\"radio-style-3-label\">{{textos.participar}}</label>
                                                </div>
                                                <button class=\"button button-dark fright\" onclick=\"aceptarViaje()\">{{textos.aceptar}}</button></p>
                                                <div class=\"clear\"></div>
                                            </form>
                                            

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Fin Modal bases legales-->    
                            {%else%}
                            
                            <h3>{{textos.subtitulo}}</h3>
                            <p>{{textos.descarga}}</p>
                            
                            {%if user.idioma =='PT'%}
                            <a href=\"{{base_url}}uploads/programa_viajem_Bushido_2019_Oporto.pdf\" class=\"button button-rounded button-reveal button-large button-red tright bottommargin-sm\"><i class=\"far fa-file-pdf\"></i><span>{{textos.botonprograma}}/OPORTO</span></a>
                            <a href=\"{{base_url}}uploads/programa_viajem_Bushido_2019_Lisboa.pdf\" class=\"button button-rounded button-reveal button-large button-red tright bottommargin-sm\"><i class=\"far fa-file-pdf\"></i><span>{{textos.botonprograma}}/LISBOA</span></a>
                            <div class=\"divider\"><i class=\"icon-circle\"></i></div>
                            {%else%}
                            
                            <a href=\"{{base_url}}uploads/programa_viaje_Bushido_2019.pdf\" class=\"button button-rounded button-reveal button-large button-red tright bottommargin-sm\"><i class=\"far fa-file-pdf\"></i><span>{{textos.botonprograma}}</span></a>
                            <div class=\"divider\"><i class=\"icon-circle\"></i></div>

                            {%endif%}
                            

                            <img src=\"{{imgurl}}viaje-bushido-vietnam.jpg\" alt=\"Viaje Yokohama Bushido 2020-2021\">
                            
                            

                            
                            <h3>{{textos.condiciones}}</h3>
                            <div class=\"col-md-6 col-xs-12\">
                                <img class=\"img-responsive\" src=\"{{imgurl}}facturacion_01.jpg\" alt=\"\">
                            </div>
                            <div class=\"col-md-6 col-xs-12\">
                                <img class=\"img-responsive\" src=\"{{imgurl}}facturacion_02.jpg\" alt=\"\">
                            </div>
                            <ul>
                                <li>{{textos.dosplazas}}</li>
                                <li>{{textos.unaplaza}}</li>
                            </ul>
                            <div class=\"style-msg\" style=\"background-color: #EEE;\">
                                <div class=\"sb-msg\"><b>{{textos.sinviaje}} </b></div>
                            </div>
                            
                            <p>*{{textos.corte}}</p>
                            
                            
                            {%endif%}
                            <div class=\"divider\"><i class=\"icon-circle\"></i></div>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!--<div class=\"col_full viaje_call center\">
        <h3 style=\"color:white;\">¿Ya has hecho las maletas?</h3>
        <a href=\"{{base_url()}}uploads/agenda_CruceroBushido_2018.pdf\" class=\"button button-xlarge tright button-dark\" id=\"travel_button\">Descarga aquí la agenda del viaje <i class=\"material-icons\">cloud_download</i></a></br>
    <span>&nbsp;</span>
</div>-->
</section><!-- #content end -->
", "viaje.php", "/Applications/MAMP/htdocs/Bushido2018/application/views/viaje.php");
    }
}
