<?php

/* cookies.php */
class __TwigTemplate_b3a8df50f72c9bd6b7bd6f093e1929569ada2950bd1a10ec3954c1b1552d3e98 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- Page Title
============================================= -->
<section id=\"page-title\" class=\"header programa_header\">

    <div class=\"container clearfix\">
        <h1>Política de cookies</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
        ============================================= -->
<section id=\"content\">
    
    <div class=\"content-wrap\">
    
        <div class=\"container clearfix legal\">
            <!-- Post Content
            ============================================= -->
            <div class=\"postcontent nobottommargin col_last clearfix\">

                <h2>POLÍTICA DE COOKIES</h2>
                <p>Solo usamos cookies técnicas. En el Sitio Web instalamos solo cookies técnicas, por lo que nos estamos obligados a mostrar el banner, pop-up o aviso emergente de cookies. Tampoco tenemos obligación de informar en este aviso legal sobre las cookies técnicas que instalamos. Sin embargo, mantenemos esta sección sobre cookies para ofrecer más transparencia sobre el funcionamiento del Sitio Web.</p>
                <p>Qué son las cookies. Las cookies son ficheros creados en el navegador del usuario para registrar su actividad en el Sitio Web y permitirle una navegación más fluida y personalizada.</p>
                <h3>¿Qué tipos de cookies utiliza esta página web?</h3>
                
                <p>Estos son los tipos de cookies técnicas que se instalan a través de este Sitio Web:</p>
                <ul>
                    <li>\"domain\": \".yokohamaiberia.es\" \"__utma\",</li>
                    <li>\"domain\": \".yokohamaiberia.es\" \"__utmb\",</li>
                    <li>\"domain\": \".yokohamaiberia.es\" \"__utmc\",</li>
                    <li>\"domain\": \".yokohamaiberia.es\" \"__utmz\",</li>
                </ul>
                
                <p>Cookies de terceros. En alguna página del Sitio Web se muestra contenido embebido o invocado a través del cual se pueden estar instalando cookies de terceros. Cuando incluimos estos contenidos en nuestro Sitio Web nos aseguramos de que la única finalidad de estas cookies sea técnica.
</p>

                <p>En particular, este sitio Web utiliza Google Analytics, un servicio analítico de web prestado por Google, Inc. con domicilio en los Estados Unidos con sede central en 1600 Amphitheatre Parkway, Mountain View, California 94043.  Para la prestación de estos servicios, estos utilizan cookies que recopilan la información, incluida la dirección IP del usuario, que será transmitida, tratada y almacenada por Google en los términos fijados en la Web Google.com. Incluyendo la posible transmisión de dicha información a terceros por razones de exigencia legal o cuando dichos terceros procesen la información por cuenta de Google.</p>
                <p>El Usuario acepta expresamente, por la utilización de este Site, el tratamiento de la información recabada en la forma y con los fines anteriormente mencionados. Y asimismo reconoce conocer la posibilidad de rechazar el tratamiento de tales datos o información rechazando el uso de Cookies mediante la selección de la configuración apropiada a tal fin en su navegador. Si bien esta opción de bloqueo de Cookies en su navegador puede no permitirle el uso pleno de todas las funcionalidades del Website.</p>
                
                <p>Puede usted permitir, bloquear o eliminar las cookies instaladas en su equipo mediante la configuración de las opciones del navegador instalado en su ordenador:</p>
                
                <p>Cómo borrar las cookies. Para utilizar este Sitio Web no resulta necesaria la instalación de cookies. El usuario puede no aceptarlas o configurar su navegador para bloquearlas y, en su caso, eliminarlas. Aprenda a administrar sus cookies en Firefox, Chrome, IE, Opera y Safari.</p>
                <ul>
                    <li><a href=\"https://support.google.com/chrome/answer/95647?hl=es\" target=\"_blank\">Chrome</a></li>
                    <li><a href=\"https://support.microsoft.com/es-es/products/windows?os=windows-10\" target=\"_blank\">Explorer</a></li>
                    <li> <a href=\"https://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-sitios-web-rastrear-preferencias?redirectlocale=es&redirectslug=habilitar-y-deshabilitar-cookies-que-los-sitios-we\" target=\"_blank\">Firefox</a></li>
                    <li><a href=\"https://support.apple.com/kb/ph5042?locale=es_ES\" target=\"_blank\">Safari</a></li>
                </ul>
                <p>Redes Sociales. En otros sitios en los que Yokohama Iberia tiene página o perfil social se instalan cookies de terceros a todos sus visitantes, aunque no sean usuarios registrados en las correspondientes plataformas: Página de Cookies de Facebook, Página de Privacidad de Twitter, Página de Cookies de LinkedIn.</p>
                <p>Si tiene dudas sobre esta política de cookies, puede contactar en <a href=\"mailto:contacto@bestdrive.plus\">contacto@bestdrive.plus</a></p>
            </div>
            
            
            <!-- Sidebar
            ============================================= -->
            <div class=\"sidebar nobottommargin clearfix\">
                <div class=\"sidebar-widgets-wrap\">
                    <div class=\"widget widget_links clearfix\">

                        <h3>El programa</h3>
                        <ul>
                            <li><a href=\"";
        // line 63
        echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
        echo "inicio/bases\"><div>Bases legales</div></a></li>
                            <li><a href=\"";
        // line 64
        echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
        echo "inicio/generales\"><div>Condiciones generales</div></a></li>
                            <li><a href=\"";
        // line 65
        echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
        echo "inicio/privacidad\"><div>Privacidad</div></a></li>
                            <li><a href=\"";
        // line 66
        echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
        echo "inicio/canjeo\"><div>Canjeo</div></a></li>
                            <li><a href=\"";
        // line 67
        echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
        echo "inicio/cookies\" class=\"active\"><div>Cookies</div></a></li> 
                                
                        </ul>

                    </div>
                </div>
            </div>
            
            <!-- Sidebar END
            ============================================= -->
            
        </div>
    
    </div>

</section><!-- #content end -->
";
    }

    public function getTemplateName()
    {
        return "cookies.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 67,  95 => 66,  91 => 65,  87 => 64,  83 => 63,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- Page Title
============================================= -->
<section id=\"page-title\" class=\"header programa_header\">

    <div class=\"container clearfix\">
        <h1>Política de cookies</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
        ============================================= -->
<section id=\"content\">
    
    <div class=\"content-wrap\">
    
        <div class=\"container clearfix legal\">
            <!-- Post Content
            ============================================= -->
            <div class=\"postcontent nobottommargin col_last clearfix\">

                <h2>POLÍTICA DE COOKIES</h2>
                <p>Solo usamos cookies técnicas. En el Sitio Web instalamos solo cookies técnicas, por lo que nos estamos obligados a mostrar el banner, pop-up o aviso emergente de cookies. Tampoco tenemos obligación de informar en este aviso legal sobre las cookies técnicas que instalamos. Sin embargo, mantenemos esta sección sobre cookies para ofrecer más transparencia sobre el funcionamiento del Sitio Web.</p>
                <p>Qué son las cookies. Las cookies son ficheros creados en el navegador del usuario para registrar su actividad en el Sitio Web y permitirle una navegación más fluida y personalizada.</p>
                <h3>¿Qué tipos de cookies utiliza esta página web?</h3>
                
                <p>Estos son los tipos de cookies técnicas que se instalan a través de este Sitio Web:</p>
                <ul>
                    <li>\"domain\": \".yokohamaiberia.es\" \"__utma\",</li>
                    <li>\"domain\": \".yokohamaiberia.es\" \"__utmb\",</li>
                    <li>\"domain\": \".yokohamaiberia.es\" \"__utmc\",</li>
                    <li>\"domain\": \".yokohamaiberia.es\" \"__utmz\",</li>
                </ul>
                
                <p>Cookies de terceros. En alguna página del Sitio Web se muestra contenido embebido o invocado a través del cual se pueden estar instalando cookies de terceros. Cuando incluimos estos contenidos en nuestro Sitio Web nos aseguramos de que la única finalidad de estas cookies sea técnica.
</p>

                <p>En particular, este sitio Web utiliza Google Analytics, un servicio analítico de web prestado por Google, Inc. con domicilio en los Estados Unidos con sede central en 1600 Amphitheatre Parkway, Mountain View, California 94043.  Para la prestación de estos servicios, estos utilizan cookies que recopilan la información, incluida la dirección IP del usuario, que será transmitida, tratada y almacenada por Google en los términos fijados en la Web Google.com. Incluyendo la posible transmisión de dicha información a terceros por razones de exigencia legal o cuando dichos terceros procesen la información por cuenta de Google.</p>
                <p>El Usuario acepta expresamente, por la utilización de este Site, el tratamiento de la información recabada en la forma y con los fines anteriormente mencionados. Y asimismo reconoce conocer la posibilidad de rechazar el tratamiento de tales datos o información rechazando el uso de Cookies mediante la selección de la configuración apropiada a tal fin en su navegador. Si bien esta opción de bloqueo de Cookies en su navegador puede no permitirle el uso pleno de todas las funcionalidades del Website.</p>
                
                <p>Puede usted permitir, bloquear o eliminar las cookies instaladas en su equipo mediante la configuración de las opciones del navegador instalado en su ordenador:</p>
                
                <p>Cómo borrar las cookies. Para utilizar este Sitio Web no resulta necesaria la instalación de cookies. El usuario puede no aceptarlas o configurar su navegador para bloquearlas y, en su caso, eliminarlas. Aprenda a administrar sus cookies en Firefox, Chrome, IE, Opera y Safari.</p>
                <ul>
                    <li><a href=\"https://support.google.com/chrome/answer/95647?hl=es\" target=\"_blank\">Chrome</a></li>
                    <li><a href=\"https://support.microsoft.com/es-es/products/windows?os=windows-10\" target=\"_blank\">Explorer</a></li>
                    <li> <a href=\"https://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-sitios-web-rastrear-preferencias?redirectlocale=es&redirectslug=habilitar-y-deshabilitar-cookies-que-los-sitios-we\" target=\"_blank\">Firefox</a></li>
                    <li><a href=\"https://support.apple.com/kb/ph5042?locale=es_ES\" target=\"_blank\">Safari</a></li>
                </ul>
                <p>Redes Sociales. En otros sitios en los que Yokohama Iberia tiene página o perfil social se instalan cookies de terceros a todos sus visitantes, aunque no sean usuarios registrados en las correspondientes plataformas: Página de Cookies de Facebook, Página de Privacidad de Twitter, Página de Cookies de LinkedIn.</p>
                <p>Si tiene dudas sobre esta política de cookies, puede contactar en <a href=\"mailto:contacto@bestdrive.plus\">contacto@bestdrive.plus</a></p>
            </div>
            
            
            <!-- Sidebar
            ============================================= -->
            <div class=\"sidebar nobottommargin clearfix\">
                <div class=\"sidebar-widgets-wrap\">
                    <div class=\"widget widget_links clearfix\">

                        <h3>El programa</h3>
                        <ul>
                            <li><a href=\"{{baseurl}}inicio/bases\"><div>Bases legales</div></a></li>
                            <li><a href=\"{{baseurl}}inicio/generales\"><div>Condiciones generales</div></a></li>
                            <li><a href=\"{{baseurl}}inicio/privacidad\"><div>Privacidad</div></a></li>
                            <li><a href=\"{{baseurl}}inicio/canjeo\"><div>Canjeo</div></a></li>
                            <li><a href=\"{{baseurl}}inicio/cookies\" class=\"active\"><div>Cookies</div></a></li> 
                                
                        </ul>

                    </div>
                </div>
            </div>
            
            <!-- Sidebar END
            ============================================= -->
            
        </div>
    
    </div>

</section><!-- #content end -->
", "cookies.php", "/Applications/MAMP/htdocs/Bushido2018/application/views/cookies.php");
    }
}
