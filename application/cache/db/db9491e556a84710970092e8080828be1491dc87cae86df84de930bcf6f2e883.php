<?php

/* mails/pedido_admin.php */
class __TwigTemplate_4a0c13f9170344b3b4d6bd7f5311afd2d84e40cfb68acdb75db223d91c35b009 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\">
    <head>
        <!-- NAME: 1 COLUMN -->
        <!--[if gte mso 15]>
        <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
        <meta charset=\"UTF-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <title>Nuevo Pedido en Yokohama Bushido</title>
        
    <style type=\"text/css\">
\t\tp{
\t\t\tmargin:10px 0;
\t\t\tpadding:0;
\t\t}
\t\ttable{
\t\t\tborder-collapse:collapse;
\t\t}
\t\th1,h2,h3,h4,h5,h6{
\t\t\tdisplay:block;
\t\t\tmargin:0;
\t\t\tpadding:0;
\t\t}
\t\timg,a img{
\t\t\tborder:0;
\t\t\theight:auto;
\t\t\toutline:none;
\t\t\ttext-decoration:none;
\t\t}
\t\tbody,#bodyTable,#bodyCell{
\t\t\theight:100%;
\t\t\tmargin:0;
\t\t\tpadding:0;
\t\t\twidth:100%;
\t\t}
\t\t.mcnPreviewText{
\t\t\tdisplay:none !important;
\t\t}
\t\t#outlook a{
\t\t\tpadding:0;
\t\t}
\t\timg{
\t\t\t-ms-interpolation-mode:bicubic;
\t\t}
\t\ttable{
\t\t\tmso-table-lspace:0pt;
\t\t\tmso-table-rspace:0pt;
\t\t}
\t\t.ReadMsgBody{
\t\t\twidth:100%;
\t\t}
\t\t.ExternalClass{
\t\t\twidth:100%;
\t\t}
\t\tp,a,li,td,blockquote{
\t\t\tmso-line-height-rule:exactly;
\t\t}
\t\ta[href^=tel],a[href^=sms]{
\t\t\tcolor:inherit;
\t\t\tcursor:default;
\t\t\ttext-decoration:none;
\t\t}
\t\tp,a,li,td,body,table,blockquote{
\t\t\t-ms-text-size-adjust:100%;
\t\t\t-webkit-text-size-adjust:100%;
\t\t}
\t\t.ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
\t\t\tline-height:100%;
\t\t}
\t\ta[x-apple-data-detectors]{
\t\t\tcolor:inherit !important;
\t\t\ttext-decoration:none !important;
\t\t\tfont-size:inherit !important;
\t\t\tfont-family:inherit !important;
\t\t\tfont-weight:inherit !important;
\t\t\tline-height:inherit !important;
\t\t}
\t\t#bodyCell{
\t\t\tpadding:10px;
\t\t\tborder-top:5px solid #dc0026;
\t\t}
\t\t.templateContainer{
\t\t\tmax-width:600px !important;
\t\t}
\t\ta.mcnButton{
\t\t\tdisplay:block;
\t\t}
\t\t.mcnImage,.mcnRetinaImage{
\t\t\tvertical-align:bottom;
\t\t}
\t\t.mcnTextContent{
\t\t\tword-break:break-word;
\t\t}
\t\t.mcnTextContent img{
\t\t\theight:auto !important;
\t\t}
\t\t.mcnDividerBlock{
\t\t\ttable-layout:fixed !important;
\t\t}

\t\tbody,#bodyTable{
\t\t\t background-color:#242424;
\t\t}

\t\t#bodyCell{
\t\t\t border-top:5px solid #dc0026;
\t\t}

\t\t.templateContainer{
\t\t\t border:0;
\t\t}

\t\th1{
\t\t\t color:#202020;
\t\t\t font-family:Helvetica;
\t\t\t font-size:26px;
\t\t\t font-style:normal;
\t\t\t font-weight:bold;
\t\t\t line-height:125%;
\t\t\t letter-spacing:normal;
\t\t\t text-align:left;
\t\t}

\t\th2{
\t\t\t color:#202020;
\t\t\t font-family:Helvetica;
\t\t\t font-size:22px;
\t\t\t font-style:normal;
\t\t\t font-weight:bold;
\t\t\t line-height:125%;
\t\t\t letter-spacing:normal;
\t\t\t text-align:left;
\t\t}

\t\th3{
\t\t\t color:#202020;
\t\t\t font-family:Helvetica;
\t\t\t font-size:20px;
\t\t\t font-style:normal;
\t\t\t font-weight:bold;
\t\t\t line-height:125%;
\t\t\t letter-spacing:normal;
\t\t\t text-align:left;
\t\t}

\t\th4{
\t\t\t color:#202020;
\t\t\t font-family:Helvetica;
\t\t\t font-size:18px;
\t\t\t font-style:normal;
\t\t\t font-weight:bold;
\t\t\t line-height:125%;
\t\t\t letter-spacing:normal;
\t\t\t text-align:left;
\t\t}

\t\t#templatePreheader{
\t\t\t background-color:#242424;
\t\t\t background-image:none;
\t\t\t background-repeat:no-repeat;
\t\t\t background-position:center;
\t\t\t background-size:cover;
\t\t\t border-top:0;
\t\t\t border-bottom:0;
\t\t\t padding-top:9px;
\t\t\t padding-bottom:9px;
\t\t}

\t\t#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
\t\t\t color:#656565;
\t\t\t font-family:Helvetica;
\t\t\t font-size:12px;
\t\t\t line-height:150%;
\t\t\t text-align:left;
\t\t}

\t\t#templatePreheader .mcnTextContent a,#templatePreheader .mcnTextContent p a{
\t\t\t color:#656565;
\t\t\t font-weight:normal;
\t\t\t text-decoration:underline;
\t\t}

\t\t#templateHeader{
\t\t\t background-color:#ffffff;
\t\t\t background-image:none;
\t\t\t background-repeat:no-repeat;
\t\t\t background-position:center;
\t\t\t background-size:cover;
\t\t\t border-top:0;
\t\t\t border-bottom:0;
\t\t\t padding-top:0px;
\t\t\t padding-bottom:0;
\t\t}

\t\t#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
\t\t\t color:#202020;
\t\t\t font-family:Helvetica;
\t\t\t font-size:16px;
\t\t\t line-height:150%;
\t\t\t text-align:left;
\t\t}

\t\t#templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{
\t\t\t color:#007C89;
\t\t\t font-weight:normal;
\t\t\t text-decoration:underline;
\t\t}

\t\t#templateBody{
\t\t\t background-color:#ffffff;
\t\t\t background-image:none;
\t\t\t background-repeat:no-repeat;
\t\t\t background-position:center;
\t\t\t background-size:cover;
\t\t\t border-top:0;
\t\t\t border-bottom:2px none #EAEAEA;
\t\t\t padding-top:0;
\t\t\t padding-bottom:0px;
\t\t}

\t\t#templateBody .mcnTextContent,#templateBody .mcnTextContent p{
\t\t\t color:#202020;
\t\t\t font-family:'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif;
\t\t\t font-size:16px;
\t\t\t line-height:150%;
\t\t\t text-align:left;
\t\t}

\t\t#templateBody .mcnTextContent a,#templateBody .mcnTextContent p a{
\t\t\t color:#dc0026;
\t\t\t font-weight:normal;
\t\t\t text-decoration:none;
\t\t}

\t\t#templateFooter{
\t\t\t background-color:#302d2d;
\t\t\t background-image:none;
\t\t\t background-repeat:no-repeat;
\t\t\t background-position:center;
\t\t\t background-size:cover;
\t\t\t border-top:0;
\t\t\t border-bottom:0;
\t\t\t padding-top:0px;
\t\t\t padding-bottom:0px;
\t\t}

\t\t#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
\t\t\t color:#656565;
\t\t\t font-family:Helvetica;
\t\t\t font-size:12px;
\t\t\t line-height:150%;
\t\t\t text-align:center;
\t\t}

\t\t#templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{
\t\t\t color:#656565;
\t\t\t font-weight:normal;
\t\t\t text-decoration:underline;
\t\t}
\t@media only screen and (min-width:768px){
\t\t.templateContainer{
\t\t\twidth:600px !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\tbody,table,td,p,a,li,blockquote{
\t\t\t-webkit-text-size-adjust:none !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\tbody{
\t\t\twidth:100% !important;
\t\t\tmin-width:100% !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t#bodyCell{
\t\t\tpadding-top:10px !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t.mcnRetinaImage{
\t\t\tmax-width:100% !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t.mcnImage{
\t\t\twidth:100% !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t.mcnCartContainer,.mcnCaptionTopContent,.mcnRecContentContainer,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer,.mcnImageCardLeftImageContentContainer,.mcnImageCardRightImageContentContainer{
\t\t\tmax-width:100% !important;
\t\t\twidth:100% !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t.mcnBoxedTextContentContainer{
\t\t\tmin-width:100% !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t.mcnImageGroupContent{
\t\t\tpadding:9px !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
\t\t\tpadding-top:9px !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t.mcnImageCardTopImageContent,.mcnCaptionBottomContent:last-child .mcnCaptionBottomImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
\t\t\tpadding-top:18px !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t.mcnImageCardBottomImageContent{
\t\t\tpadding-bottom:9px !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t.mcnImageGroupBlockInner{
\t\t\tpadding-top:0 !important;
\t\t\tpadding-bottom:0 !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t.mcnImageGroupBlockOuter{
\t\t\tpadding-top:9px !important;
\t\t\tpadding-bottom:9px !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t.mcnTextContent,.mcnBoxedTextContentColumn{
\t\t\tpadding-right:18px !important;
\t\t\tpadding-left:18px !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
\t\t\tpadding-right:18px !important;
\t\t\tpadding-bottom:0 !important;
\t\t\tpadding-left:18px !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t.mcpreview-image-uploader{
\t\t\tdisplay:none !important;
\t\t\twidth:100% !important;
\t\t}

}\t@media only screen and (max-width: 480px){

\t\th1{
\t\t\t font-size:22px !important;
\t\t\t line-height:125% !important;
\t\t}

}\t@media only screen and (max-width: 480px){

\t\th2{
\t\t\t font-size:20px !important;
\t\t\t line-height:125% !important;
\t\t}

}\t@media only screen and (max-width: 480px){

\t\th3{
\t\t\t font-size:18px !important;
\t\t\t line-height:125% !important;
\t\t}

}\t@media only screen and (max-width: 480px){

\t\th4{
\t\t\t font-size:16px !important;
\t\t\t line-height:150% !important;
\t\t}

}\t@media only screen and (max-width: 480px){

\t\t.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
\t\t\t font-size:14px !important;
\t\t\t line-height:150% !important;
\t\t}

}\t@media only screen and (max-width: 480px){

\t\t#templatePreheader{
\t\t\t display:block !important;
\t\t}

}\t@media only screen and (max-width: 480px){

\t\t#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
\t\t\t font-size:14px !important;
\t\t\t line-height:150% !important;
\t\t}

}\t@media only screen and (max-width: 480px){

\t\t#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
\t\t\t font-size:16px !important;
\t\t\t line-height:150% !important;
\t\t}

}\t@media only screen and (max-width: 480px){

\t\t#templateBody .mcnTextContent,#templateBody .mcnTextContent p{
\t\t\t font-size:16px !important;
\t\t\t line-height:150% !important;
\t\t}

}\t@media only screen and (max-width: 480px){

\t\t#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
\t\t\t font-size:14px !important;
\t\t\t line-height:150% !important;
\t\t}

}</style></head>
    <body>
        <!--*|IF:MC_PREVIEW_TEXT|*-->
        <!--[if !gte mso 9]><!----><span class=\"mcnPreviewText\" style=\"display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;\">Nuevo pedido en Yokohama Bushido</span><!--<![endif]-->
        <!--*|END:IF|*-->
        <center>
            <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" width=\"100%\" id=\"bodyTable\">
                <tr>
                    <td align=\"center\" valign=\"top\" id=\"bodyCell\">
                        <!-- BEGIN TEMPLATE // -->
                        <!--[if (gte mso 9)|(IE)]>
                        <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"600\" style=\"width:600px;\">
                        <tr>
                        <td align=\"center\" valign=\"top\" width=\"600\" style=\"width:600px;\">
                        <![endif]-->
                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"templateContainer\">
                            <tr>
                                <td valign=\"top\" id=\"templatePreheader\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnTextBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnTextBlockOuter\">
        <tr>
            <td valign=\"top\" class=\"mcnTextBlockInner\" style=\"padding-top:9px;\">
              \t<!--[if mso]>
\t\t\t\t<table align=\"left\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"width:100%;\">
\t\t\t\t<tr>
\t\t\t\t<![endif]-->
\t\t\t    
\t\t\t\t<!--[if mso]>
\t\t\t\t<td valign=\"top\" width=\"600\" style=\"width:600px;\">
\t\t\t\t<![endif]-->

\t\t\t\t<!--[if mso]>
\t\t\t\t</td>
\t\t\t\t<![endif]-->
                
\t\t\t\t<!--[if mso]>
\t\t\t\t</tr>
\t\t\t\t</table>
\t\t\t\t<![endif]-->
            </td>
        </tr>
    </tbody>
</table></td>
                            </tr>
                            <tr>
                                <td valign=\"top\" id=\"templateHeader\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnImageBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnImageBlockOuter\">
            <tr>
                <td valign=\"top\" style=\"padding:0px\" class=\"mcnImageBlockInner\">
                    <table align=\"left\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnImageContentContainer\" style=\"min-width:100%;\">
                        <tbody><tr>
                            <td class=\"mcnImageContent\" valign=\"top\" style=\"padding-right: 0px; padding-left: 0px; padding-top: 0; padding-bottom: 0; text-align:center;\">
                                
                                    
                                        <img align=\"center\" alt=\"\" src=\"https://gallery.mailchimp.com/cf58ffd76c96f505709247289/images/3348d364-5478-425e-b78a-f75bb4de5f4f.jpg\" width=\"600\" style=\"max-width:700px; padding-bottom: 0; display: inline !important; vertical-align: bottom;\" class=\"mcnImage\">
                                    
                                
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></td>
                            </tr>
                            <tr>
                                <td valign=\"top\" id=\"templateBody\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnDividerBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnDividerBlockOuter\">
        <tr>
            <td class=\"mcnDividerBlockInner\" style=\"min-width:100%; padding:18px;\">
                <table class=\"mcnDividerContent\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-top: 2px dotted #EAEAEA;\">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class=\"mcnDividerBlockInner\" style=\"padding: 18px;\">
                <hr class=\"mcnDividerContent\" style=\"border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;\" />
-->
            </td>
        </tr>
    </tbody>
</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnTextBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnTextBlockOuter\">
        <tr>
            <td valign=\"top\" class=\"mcnTextBlockInner\" style=\"padding-top:9px;\">
              \t<!--[if mso]>
\t\t\t\t<table align=\"left\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"width:100%;\">
\t\t\t\t<tr>
\t\t\t\t<![endif]-->
\t\t\t    
\t\t\t\t<!--[if mso]>
\t\t\t\t<td valign=\"top\" width=\"600\" style=\"width:600px;\">
\t\t\t\t<![endif]-->
                <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width:100%; min-width:100%;\" width=\"100%\" class=\"mcnTextContentContainer\">
                    <tbody><tr>
                        
                        <td valign=\"top\" class=\"mcnTextContent\" style=\"padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;\">
                        
                            <div style=\"text-align: right;\"><strong>Fecha:</strong> ";
        // line 529
        echo twig_escape_filter($this->env, (isset($context["fecha"]) ? $context["fecha"] : null), "html", null, true);
        echo "<br>
<strong>Número de pedido: </strong>2019-";
        // line 530
        echo twig_escape_filter($this->env, (isset($context["idpedido"]) ? $context["idpedido"] : null), "html", null, true);
        echo "</div>

                        </td>
                    </tr>
                </tbody></table>
\t\t\t\t<!--[if mso]>
\t\t\t\t</td>
\t\t\t\t<![endif]-->
                
\t\t\t\t<!--[if mso]>
\t\t\t\t</tr>
\t\t\t\t</table>
\t\t\t\t<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnButtonBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnButtonBlockOuter\">
        <tr>
            <td style=\"padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;\" valign=\"top\" align=\"right\" class=\"mcnButtonBlockInner\">
                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonContentContainer\" style=\"border-collapse: separate !important;border-radius: 0px;background-color: #000000;\">
                    <tbody>
                        <tr>
                            <td align=\"center\" valign=\"middle\" class=\"mcnButtonContent\" style=\"font-family: Arial; font-size: 14px; padding: 15px;\">
                                <a class=\"mcnButton \" title=\"REVISAR PEDIDO\" href=\"\" target=\"_blank\" style=\"font-weight: normal;letter-spacing: 1px;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;\">REVISAR PEDIDO</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnDividerBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnDividerBlockOuter\">
        <tr>
            <td class=\"mcnDividerBlockInner\" style=\"min-width:100%; padding:18px;\">
                <table class=\"mcnDividerContent\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-top: 2px dotted #EAEAEA;\">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class=\"mcnDividerBlockInner\" style=\"padding: 18px;\">
                <hr class=\"mcnDividerContent\" style=\"border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;\" />
-->
            </td>
        </tr>
    </tbody>
</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnTextBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnTextBlockOuter\">
        <tr>
            <td valign=\"top\" class=\"mcnTextBlockInner\" style=\"padding-top:9px;\">
              \t<!--[if mso]>
\t\t\t\t<table align=\"left\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"width:100%;\">
\t\t\t\t<tr>
\t\t\t\t<![endif]-->
\t\t\t    
\t\t\t\t<!--[if mso]>
\t\t\t\t<td valign=\"top\" width=\"600\" style=\"width:600px;\">
\t\t\t\t<![endif]-->
                <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width:100%; min-width:100%;\" width=\"100%\" class=\"mcnTextContentContainer\">
                    <tbody><tr>
                        
                        <td valign=\"top\" class=\"mcnTextContent\" style=\"padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;\">
                        
                             El usuario ";
        // line 597
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "nombre", array()), "html", null, true);
        echo " con código de cliente ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "cod_user", array()), "html", null, true);
        echo ", solicitó un pedido a nombre de:<strong>";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "shippingname", array()), "html", null, true);
        echo "</strong>,<br>Ha realizado el siguiente pedido:
<br>
&nbsp;
                        </td>
                    </tr>
                </tbody></table>
\t\t\t\t<!--[if mso]>
\t\t\t\t</td>
\t\t\t\t<![endif]-->
                
\t\t\t\t<!--[if mso]>
\t\t\t\t</tr>
\t\t\t\t</table>
\t\t\t\t<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnDividerBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnDividerBlockOuter\">
        <tr>
            <td class=\"mcnDividerBlockInner\" style=\"min-width:100%; padding:18px;\">
                <table class=\"mcnDividerContent\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-top: 2px dotted #EAEAEA;\">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class=\"mcnDividerBlockInner\" style=\"padding: 18px;\">
                <hr class=\"mcnDividerContent\" style=\"border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;\" />
-->
            </td>
        </tr>
    </tbody>
</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnTextBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnTextBlockOuter\">
        <tr>
            <td valign=\"top\" class=\"mcnTextBlockInner\" style=\"padding-top:9px;\">
              \t<!--[if mso]>
\t\t\t\t<table align=\"left\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"width:100%;\">
\t\t\t\t<tr>
\t\t\t\t<![endif]-->
\t\t\t    
\t\t\t\t<!--[if mso]>
\t\t\t\t<td valign=\"top\" width=\"300\" style=\"width:300px;\">
\t\t\t\t<![endif]-->
                <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width:300px;\" width=\"100%\" class=\"mcnTextContentContainer\">
                    <tbody><tr>
                        
                        <td valign=\"top\" class=\"mcnTextContent\" style=\"padding-top:0; padding-left:18px; padding-bottom:9px; padding-right:18px;\">
                        
                            <span style=\"font-family:roboto,helvetica neue,helvetica,arial,sans-serif\"><strong>Pedido enviado a:</strong><br>
";
        // line 650
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "shippingaddress", array()), "html", null, true);
        echo "
                                                                                    <br> ";
        // line 651
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "shippingcity", array()), "html", null, true);
        echo "
                                                                                    <br></span>
                        </td>
                    </tr>
                </tbody></table>
\t\t\t\t<!--[if mso]>
\t\t\t\t</td>
\t\t\t\t<![endif]-->
                
\t\t\t\t<!--[if mso]>
\t\t\t\t<td valign=\"top\" width=\"300\" style=\"width:300px;\">
\t\t\t\t<![endif]-->
                <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width:300px;\" width=\"100%\" class=\"mcnTextContentContainer\">
                    <tbody><tr>
                        
                        <td valign=\"top\" class=\"mcnTextContent\" style=\"padding-top:0; padding-left:18px; padding-bottom:9px; padding-right:18px;\">
                        
                            <strong>Dirigiado a:</strong><br>
";
        // line 669
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "shippingname", array()), "html", null, true);
        echo "<br>
";
        // line 670
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "shippingaddress", array()), "html", null, true);
        echo "
                        </td>
                    </tr>
                </tbody></table>
\t\t\t\t<!--[if mso]>
\t\t\t\t</td>
\t\t\t\t<![endif]-->
                
\t\t\t\t<!--[if mso]>
\t\t\t\t</tr>
\t\t\t\t</table>
\t\t\t\t<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnTextBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnTextBlockOuter\">
        <tr>
            <td valign=\"top\" class=\"mcnTextBlockInner\" style=\"padding-top:9px;\">
              \t<!--[if mso]>
\t\t\t\t<table align=\"left\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"width:100%;\">
\t\t\t\t<tr>
\t\t\t\t<![endif]-->
\t\t\t    
\t\t\t\t<!--[if mso]>
\t\t\t\t<td valign=\"top\" width=\"600\" style=\"width:600px;\">
\t\t\t\t<![endif]-->
                <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width:100%; min-width:100%;\" width=\"100%\" class=\"mcnTextContentContainer\">
                    <tbody><tr>
                        
                        <td valign=\"top\" class=\"mcnTextContent\" style=\"padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;\">
                        
                            <span style=\"color:#DC0026\"><strong>INFORMACIÓN DEL PEDIDO</strong></span>
                        </td>
                    </tr>
                </tbody></table>
\t\t\t\t<!--[if mso]>
\t\t\t\t</td>
\t\t\t\t<![endif]-->
                
\t\t\t\t<!--[if mso]>
\t\t\t\t</tr>
\t\t\t\t</table>
\t\t\t\t<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnDividerBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnDividerBlockOuter\">
        <tr>
            <td class=\"mcnDividerBlockInner\" style=\"min-width: 100%; padding: 0px 18px 18px;\">
                <table class=\"mcnDividerContent\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-top: 2px solid #DC0026;\">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class=\"mcnDividerBlockInner\" style=\"padding: 18px;\">
                <hr class=\"mcnDividerContent\" style=\"border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;\" />
-->
            </td>
        </tr>
    </tbody>
</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnCaptionBlock\">
    <tbody class=\"mcnCaptionBlockOuter\">
        <tr>
            <td class=\"mcnCaptionBlockInner\" valign=\"top\" style=\"padding:9px;\">
                


                ";
        // line 742
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["cart"]) ? $context["cart"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["item"]) {
            // line 743
            echo "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnCaptionRightContentOuter\" width=\"100%\">
    <tbody><tr>
        <td valign=\"top\" class=\"mcnCaptionRightContentInner\" style=\"padding:0 9px ;\">
            <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnCaptionRightImageContentContainer\" width=\"264\">
                <tbody><tr>
                    <td class=\"mcnCaptionRightImageContent\" align=\"center\" valign=\"top\">
                    
                        

                        <img alt=\"";
            // line 752
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "name", array()), "html", null, true);
            echo "\" src=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "options", array()), "img", array()), "html", null, true);
            echo "\" width=\"264\" style=\"max-width:1500px;\" class=\"mcnImage\">
                        

                    
                    </td>
                </tr>
            </tbody></table>
            <table class=\"mcnCaptionRightTextContentContainer\" align=\"right\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"264\">
                <tbody><tr>
                    <td valign=\"top\" class=\"mcnTextContent\">
                        <strong>";
            // line 762
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "name", array()), "html", null, true);
            echo "<br>
<span style=\"color:#dc0026\">";
            // line 763
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["item"], "price", array()), 0, ",", "."), "html", null, true);
            echo " km.</span></strong><br>
<br>
Cantidad:&nbsp;";
            // line 765
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "qty", array()), "html", null, true);
            echo "<br>
";
            // line 766
            if ( !(null === $this->getAttribute($this->getAttribute($context["item"], "options", array()), "size", array()))) {
                echo " ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($context["item"], "options", array()), "size", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["talla"]) {
                    // line 767
                    echo "Talla:&nbsp;";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["talla"], 0, array()), "html", null, true);
                    echo " x ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["talla"], 1, array()), "html", null, true);
                    echo " uds<br>";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['talla'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                echo " ";
            }
            // line 768
            echo " ";
            if ( !(null === $this->getAttribute($this->getAttribute($context["item"], "options", array()), "color", array()))) {
                echo "   
Color:";
                // line 769
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "options", array()), "color", array()), "html", null, true);
                echo " ";
            }
            // line 770
            echo "<hr><span style=\"font-size:18px\"><strong>Kilómetros totales: <em>";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ($this->getAttribute($context["item"], "price", array()) * $this->getAttribute($context["item"], "qty", array())), 0, ",", "."), "html", null, true);
            echo " km.</em></strong></span>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 778
        echo "


            </td>
        </tr>
    </tbody>
</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnDividerBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnDividerBlockOuter\">
        <tr>
            <td class=\"mcnDividerBlockInner\" style=\"min-width:100%; padding:18px;\">
                <table class=\"mcnDividerContent\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-top: 2px dotted #818181;\">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class=\"mcnDividerBlockInner\" style=\"padding: 18px;\">
                <hr class=\"mcnDividerContent\" style=\"border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;\" />
-->
            </td>
        </tr>
    </tbody>
</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnTextBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnTextBlockOuter\">
        <tr>
            <td valign=\"top\" class=\"mcnTextBlockInner\" style=\"padding-top:9px;\">
              \t<!--[if mso]>
\t\t\t\t<table align=\"left\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"width:100%;\">
\t\t\t\t<tr>
\t\t\t\t<![endif]-->
\t\t\t    
\t\t\t\t<!--[if mso]>
\t\t\t\t<td valign=\"top\" width=\"600\" style=\"width:600px;\">
\t\t\t\t<![endif]-->
                <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width:100%; min-width:100%;\" width=\"100%\" class=\"mcnTextContentContainer\">
                    <tbody><tr>
                        
                        <td valign=\"top\" class=\"mcnTextContent\" style=\"padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;\">
                        
                            <div style=\"text-align: right;\"><span style=\"font-size:24px\"><strong>Subtotal: ";
        // line 819
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["cart_total"]) ? $context["cart_total"] : null), 0, ",", "."), "html", null, true);
        echo " Km.</strong></span><br>
Gastos de envío: 0,00 €<br>
<span style=\"color:#dc0026\"><strong><span style=\"font-size:24px\">Total Kiómetros: ";
        // line 821
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["cart_total"]) ? $context["cart_total"] : null), 0, ",", "."), "html", null, true);
        echo " km.</span></strong></span></div>

                        </td>
                    </tr>
                </tbody></table>
\t\t\t\t<!--[if mso]>
\t\t\t\t</td>
\t\t\t\t<![endif]-->
                
\t\t\t\t<!--[if mso]>
\t\t\t\t</tr>
\t\t\t\t</table>
\t\t\t\t<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnBoxedTextBlock\" style=\"min-width:100%;\">
    <!--[if gte mso 9]>
\t<table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">
\t<![endif]-->
\t<tbody class=\"mcnBoxedTextBlockOuter\">
        <tr>
            <td valign=\"top\" class=\"mcnBoxedTextBlockInner\">
                
\t\t\t\t<!--[if gte mso 9]>
\t\t\t\t<td align=\"center\" valign=\"top\" \">
\t\t\t\t<![endif]-->
                <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width:100%;\" class=\"mcnBoxedTextContentContainer\">
                    <tbody><tr>
                        
                        <td style=\"padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;\">
                        
                            <table border=\"0\" cellspacing=\"0\" class=\"mcnTextContentContainer\" width=\"100%\" style=\"min-width: 100% !important;border: 2px dotted #818181;\">
                                <tbody><tr>
                                    <td valign=\"top\" class=\"mcnTextContent\" style=\"padding: 18px; font-family: Helvetica; font-size: 14px; font-weight: normal; text-align: center;\">
                                        <div style=\"text-align: left;\"><strong>Observaciones:</strong><br>
<br>
";
        // line 858
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "shippingmessage", array()), "html", null, true);
        echo "</div>

                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
\t\t\t\t<!--[if gte mso 9]>
\t\t\t\t</td>
\t\t\t\t<![endif]-->
                
\t\t\t\t<!--[if gte mso 9]>
                </tr>
                </table>
\t\t\t\t<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnImageBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnImageBlockOuter\">
            <tr>
                <td valign=\"top\" style=\"padding:0px\" class=\"mcnImageBlockInner\">
                    <table align=\"left\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnImageContentContainer\" style=\"min-width:100%;\">
                        <tbody><tr>
                            <td class=\"mcnImageContent\" valign=\"top\" style=\"padding-right: 0px; padding-left: 0px; padding-top: 0; padding-bottom: 0; text-align:center;\">
                                
                                    
                                        <img align=\"center\" alt=\"\" src=\"https://gallery.mailchimp.com/cf58ffd76c96f505709247289/images/c3ff8da1-2132-42ae-afba-474aefb2cc88.jpg\" width=\"600\" style=\"max-width:700px; padding-bottom: 0; display: inline !important; vertical-align: bottom;\" class=\"mcnImage\">
                                    
                                
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></td>
                            </tr>
                            <tr>
                                <td valign=\"top\" id=\"templateFooter\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnImageBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnImageBlockOuter\">
            <tr>
                <td valign=\"top\" style=\"padding:9px\" class=\"mcnImageBlockInner\">
                    <table align=\"left\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnImageContentContainer\" style=\"min-width:100%;\">
                        <tbody><tr>
                            <td class=\"mcnImageContent\" valign=\"top\" style=\"padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;\">
                                
                                    
                                        <img align=\"center\" alt=\"\" src=\"https://gallery.mailchimp.com/cf58ffd76c96f505709247289/images/e12b1576-05d1-4c90-b86d-2ed4fef1d94f.png\" width=\"250\" style=\"max-width:250px; padding-bottom: 0; display: inline !important; vertical-align: bottom;\" class=\"mcnImage\">
                                    
                                
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></td>
                            </tr>
                        </table>
                        <!--[if (gte mso 9)|(IE)]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>
";
    }

    public function getTemplateName()
    {
        return "mails/pedido_admin.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  960 => 858,  920 => 821,  915 => 819,  872 => 778,  857 => 770,  853 => 769,  848 => 768,  836 => 767,  830 => 766,  826 => 765,  821 => 763,  817 => 762,  802 => 752,  791 => 743,  787 => 742,  712 => 670,  708 => 669,  687 => 651,  683 => 650,  623 => 597,  553 => 530,  549 => 529,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!doctype html>
<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\">
    <head>
        <!-- NAME: 1 COLUMN -->
        <!--[if gte mso 15]>
        <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
        <meta charset=\"UTF-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <title>Nuevo Pedido en Yokohama Bushido</title>
        
    <style type=\"text/css\">
\t\tp{
\t\t\tmargin:10px 0;
\t\t\tpadding:0;
\t\t}
\t\ttable{
\t\t\tborder-collapse:collapse;
\t\t}
\t\th1,h2,h3,h4,h5,h6{
\t\t\tdisplay:block;
\t\t\tmargin:0;
\t\t\tpadding:0;
\t\t}
\t\timg,a img{
\t\t\tborder:0;
\t\t\theight:auto;
\t\t\toutline:none;
\t\t\ttext-decoration:none;
\t\t}
\t\tbody,#bodyTable,#bodyCell{
\t\t\theight:100%;
\t\t\tmargin:0;
\t\t\tpadding:0;
\t\t\twidth:100%;
\t\t}
\t\t.mcnPreviewText{
\t\t\tdisplay:none !important;
\t\t}
\t\t#outlook a{
\t\t\tpadding:0;
\t\t}
\t\timg{
\t\t\t-ms-interpolation-mode:bicubic;
\t\t}
\t\ttable{
\t\t\tmso-table-lspace:0pt;
\t\t\tmso-table-rspace:0pt;
\t\t}
\t\t.ReadMsgBody{
\t\t\twidth:100%;
\t\t}
\t\t.ExternalClass{
\t\t\twidth:100%;
\t\t}
\t\tp,a,li,td,blockquote{
\t\t\tmso-line-height-rule:exactly;
\t\t}
\t\ta[href^=tel],a[href^=sms]{
\t\t\tcolor:inherit;
\t\t\tcursor:default;
\t\t\ttext-decoration:none;
\t\t}
\t\tp,a,li,td,body,table,blockquote{
\t\t\t-ms-text-size-adjust:100%;
\t\t\t-webkit-text-size-adjust:100%;
\t\t}
\t\t.ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
\t\t\tline-height:100%;
\t\t}
\t\ta[x-apple-data-detectors]{
\t\t\tcolor:inherit !important;
\t\t\ttext-decoration:none !important;
\t\t\tfont-size:inherit !important;
\t\t\tfont-family:inherit !important;
\t\t\tfont-weight:inherit !important;
\t\t\tline-height:inherit !important;
\t\t}
\t\t#bodyCell{
\t\t\tpadding:10px;
\t\t\tborder-top:5px solid #dc0026;
\t\t}
\t\t.templateContainer{
\t\t\tmax-width:600px !important;
\t\t}
\t\ta.mcnButton{
\t\t\tdisplay:block;
\t\t}
\t\t.mcnImage,.mcnRetinaImage{
\t\t\tvertical-align:bottom;
\t\t}
\t\t.mcnTextContent{
\t\t\tword-break:break-word;
\t\t}
\t\t.mcnTextContent img{
\t\t\theight:auto !important;
\t\t}
\t\t.mcnDividerBlock{
\t\t\ttable-layout:fixed !important;
\t\t}

\t\tbody,#bodyTable{
\t\t\t background-color:#242424;
\t\t}

\t\t#bodyCell{
\t\t\t border-top:5px solid #dc0026;
\t\t}

\t\t.templateContainer{
\t\t\t border:0;
\t\t}

\t\th1{
\t\t\t color:#202020;
\t\t\t font-family:Helvetica;
\t\t\t font-size:26px;
\t\t\t font-style:normal;
\t\t\t font-weight:bold;
\t\t\t line-height:125%;
\t\t\t letter-spacing:normal;
\t\t\t text-align:left;
\t\t}

\t\th2{
\t\t\t color:#202020;
\t\t\t font-family:Helvetica;
\t\t\t font-size:22px;
\t\t\t font-style:normal;
\t\t\t font-weight:bold;
\t\t\t line-height:125%;
\t\t\t letter-spacing:normal;
\t\t\t text-align:left;
\t\t}

\t\th3{
\t\t\t color:#202020;
\t\t\t font-family:Helvetica;
\t\t\t font-size:20px;
\t\t\t font-style:normal;
\t\t\t font-weight:bold;
\t\t\t line-height:125%;
\t\t\t letter-spacing:normal;
\t\t\t text-align:left;
\t\t}

\t\th4{
\t\t\t color:#202020;
\t\t\t font-family:Helvetica;
\t\t\t font-size:18px;
\t\t\t font-style:normal;
\t\t\t font-weight:bold;
\t\t\t line-height:125%;
\t\t\t letter-spacing:normal;
\t\t\t text-align:left;
\t\t}

\t\t#templatePreheader{
\t\t\t background-color:#242424;
\t\t\t background-image:none;
\t\t\t background-repeat:no-repeat;
\t\t\t background-position:center;
\t\t\t background-size:cover;
\t\t\t border-top:0;
\t\t\t border-bottom:0;
\t\t\t padding-top:9px;
\t\t\t padding-bottom:9px;
\t\t}

\t\t#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
\t\t\t color:#656565;
\t\t\t font-family:Helvetica;
\t\t\t font-size:12px;
\t\t\t line-height:150%;
\t\t\t text-align:left;
\t\t}

\t\t#templatePreheader .mcnTextContent a,#templatePreheader .mcnTextContent p a{
\t\t\t color:#656565;
\t\t\t font-weight:normal;
\t\t\t text-decoration:underline;
\t\t}

\t\t#templateHeader{
\t\t\t background-color:#ffffff;
\t\t\t background-image:none;
\t\t\t background-repeat:no-repeat;
\t\t\t background-position:center;
\t\t\t background-size:cover;
\t\t\t border-top:0;
\t\t\t border-bottom:0;
\t\t\t padding-top:0px;
\t\t\t padding-bottom:0;
\t\t}

\t\t#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
\t\t\t color:#202020;
\t\t\t font-family:Helvetica;
\t\t\t font-size:16px;
\t\t\t line-height:150%;
\t\t\t text-align:left;
\t\t}

\t\t#templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{
\t\t\t color:#007C89;
\t\t\t font-weight:normal;
\t\t\t text-decoration:underline;
\t\t}

\t\t#templateBody{
\t\t\t background-color:#ffffff;
\t\t\t background-image:none;
\t\t\t background-repeat:no-repeat;
\t\t\t background-position:center;
\t\t\t background-size:cover;
\t\t\t border-top:0;
\t\t\t border-bottom:2px none #EAEAEA;
\t\t\t padding-top:0;
\t\t\t padding-bottom:0px;
\t\t}

\t\t#templateBody .mcnTextContent,#templateBody .mcnTextContent p{
\t\t\t color:#202020;
\t\t\t font-family:'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif;
\t\t\t font-size:16px;
\t\t\t line-height:150%;
\t\t\t text-align:left;
\t\t}

\t\t#templateBody .mcnTextContent a,#templateBody .mcnTextContent p a{
\t\t\t color:#dc0026;
\t\t\t font-weight:normal;
\t\t\t text-decoration:none;
\t\t}

\t\t#templateFooter{
\t\t\t background-color:#302d2d;
\t\t\t background-image:none;
\t\t\t background-repeat:no-repeat;
\t\t\t background-position:center;
\t\t\t background-size:cover;
\t\t\t border-top:0;
\t\t\t border-bottom:0;
\t\t\t padding-top:0px;
\t\t\t padding-bottom:0px;
\t\t}

\t\t#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
\t\t\t color:#656565;
\t\t\t font-family:Helvetica;
\t\t\t font-size:12px;
\t\t\t line-height:150%;
\t\t\t text-align:center;
\t\t}

\t\t#templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{
\t\t\t color:#656565;
\t\t\t font-weight:normal;
\t\t\t text-decoration:underline;
\t\t}
\t@media only screen and (min-width:768px){
\t\t.templateContainer{
\t\t\twidth:600px !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\tbody,table,td,p,a,li,blockquote{
\t\t\t-webkit-text-size-adjust:none !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\tbody{
\t\t\twidth:100% !important;
\t\t\tmin-width:100% !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t#bodyCell{
\t\t\tpadding-top:10px !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t.mcnRetinaImage{
\t\t\tmax-width:100% !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t.mcnImage{
\t\t\twidth:100% !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t.mcnCartContainer,.mcnCaptionTopContent,.mcnRecContentContainer,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer,.mcnImageCardLeftImageContentContainer,.mcnImageCardRightImageContentContainer{
\t\t\tmax-width:100% !important;
\t\t\twidth:100% !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t.mcnBoxedTextContentContainer{
\t\t\tmin-width:100% !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t.mcnImageGroupContent{
\t\t\tpadding:9px !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
\t\t\tpadding-top:9px !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t.mcnImageCardTopImageContent,.mcnCaptionBottomContent:last-child .mcnCaptionBottomImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
\t\t\tpadding-top:18px !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t.mcnImageCardBottomImageContent{
\t\t\tpadding-bottom:9px !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t.mcnImageGroupBlockInner{
\t\t\tpadding-top:0 !important;
\t\t\tpadding-bottom:0 !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t.mcnImageGroupBlockOuter{
\t\t\tpadding-top:9px !important;
\t\t\tpadding-bottom:9px !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t.mcnTextContent,.mcnBoxedTextContentColumn{
\t\t\tpadding-right:18px !important;
\t\t\tpadding-left:18px !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
\t\t\tpadding-right:18px !important;
\t\t\tpadding-bottom:0 !important;
\t\t\tpadding-left:18px !important;
\t\t}

}\t@media only screen and (max-width: 480px){
\t\t.mcpreview-image-uploader{
\t\t\tdisplay:none !important;
\t\t\twidth:100% !important;
\t\t}

}\t@media only screen and (max-width: 480px){

\t\th1{
\t\t\t font-size:22px !important;
\t\t\t line-height:125% !important;
\t\t}

}\t@media only screen and (max-width: 480px){

\t\th2{
\t\t\t font-size:20px !important;
\t\t\t line-height:125% !important;
\t\t}

}\t@media only screen and (max-width: 480px){

\t\th3{
\t\t\t font-size:18px !important;
\t\t\t line-height:125% !important;
\t\t}

}\t@media only screen and (max-width: 480px){

\t\th4{
\t\t\t font-size:16px !important;
\t\t\t line-height:150% !important;
\t\t}

}\t@media only screen and (max-width: 480px){

\t\t.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
\t\t\t font-size:14px !important;
\t\t\t line-height:150% !important;
\t\t}

}\t@media only screen and (max-width: 480px){

\t\t#templatePreheader{
\t\t\t display:block !important;
\t\t}

}\t@media only screen and (max-width: 480px){

\t\t#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
\t\t\t font-size:14px !important;
\t\t\t line-height:150% !important;
\t\t}

}\t@media only screen and (max-width: 480px){

\t\t#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
\t\t\t font-size:16px !important;
\t\t\t line-height:150% !important;
\t\t}

}\t@media only screen and (max-width: 480px){

\t\t#templateBody .mcnTextContent,#templateBody .mcnTextContent p{
\t\t\t font-size:16px !important;
\t\t\t line-height:150% !important;
\t\t}

}\t@media only screen and (max-width: 480px){

\t\t#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
\t\t\t font-size:14px !important;
\t\t\t line-height:150% !important;
\t\t}

}</style></head>
    <body>
        <!--*|IF:MC_PREVIEW_TEXT|*-->
        <!--[if !gte mso 9]><!----><span class=\"mcnPreviewText\" style=\"display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;\">Nuevo pedido en Yokohama Bushido</span><!--<![endif]-->
        <!--*|END:IF|*-->
        <center>
            <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" width=\"100%\" id=\"bodyTable\">
                <tr>
                    <td align=\"center\" valign=\"top\" id=\"bodyCell\">
                        <!-- BEGIN TEMPLATE // -->
                        <!--[if (gte mso 9)|(IE)]>
                        <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"600\" style=\"width:600px;\">
                        <tr>
                        <td align=\"center\" valign=\"top\" width=\"600\" style=\"width:600px;\">
                        <![endif]-->
                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"templateContainer\">
                            <tr>
                                <td valign=\"top\" id=\"templatePreheader\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnTextBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnTextBlockOuter\">
        <tr>
            <td valign=\"top\" class=\"mcnTextBlockInner\" style=\"padding-top:9px;\">
              \t<!--[if mso]>
\t\t\t\t<table align=\"left\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"width:100%;\">
\t\t\t\t<tr>
\t\t\t\t<![endif]-->
\t\t\t    
\t\t\t\t<!--[if mso]>
\t\t\t\t<td valign=\"top\" width=\"600\" style=\"width:600px;\">
\t\t\t\t<![endif]-->

\t\t\t\t<!--[if mso]>
\t\t\t\t</td>
\t\t\t\t<![endif]-->
                
\t\t\t\t<!--[if mso]>
\t\t\t\t</tr>
\t\t\t\t</table>
\t\t\t\t<![endif]-->
            </td>
        </tr>
    </tbody>
</table></td>
                            </tr>
                            <tr>
                                <td valign=\"top\" id=\"templateHeader\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnImageBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnImageBlockOuter\">
            <tr>
                <td valign=\"top\" style=\"padding:0px\" class=\"mcnImageBlockInner\">
                    <table align=\"left\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnImageContentContainer\" style=\"min-width:100%;\">
                        <tbody><tr>
                            <td class=\"mcnImageContent\" valign=\"top\" style=\"padding-right: 0px; padding-left: 0px; padding-top: 0; padding-bottom: 0; text-align:center;\">
                                
                                    
                                        <img align=\"center\" alt=\"\" src=\"https://gallery.mailchimp.com/cf58ffd76c96f505709247289/images/3348d364-5478-425e-b78a-f75bb4de5f4f.jpg\" width=\"600\" style=\"max-width:700px; padding-bottom: 0; display: inline !important; vertical-align: bottom;\" class=\"mcnImage\">
                                    
                                
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></td>
                            </tr>
                            <tr>
                                <td valign=\"top\" id=\"templateBody\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnDividerBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnDividerBlockOuter\">
        <tr>
            <td class=\"mcnDividerBlockInner\" style=\"min-width:100%; padding:18px;\">
                <table class=\"mcnDividerContent\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-top: 2px dotted #EAEAEA;\">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class=\"mcnDividerBlockInner\" style=\"padding: 18px;\">
                <hr class=\"mcnDividerContent\" style=\"border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;\" />
-->
            </td>
        </tr>
    </tbody>
</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnTextBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnTextBlockOuter\">
        <tr>
            <td valign=\"top\" class=\"mcnTextBlockInner\" style=\"padding-top:9px;\">
              \t<!--[if mso]>
\t\t\t\t<table align=\"left\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"width:100%;\">
\t\t\t\t<tr>
\t\t\t\t<![endif]-->
\t\t\t    
\t\t\t\t<!--[if mso]>
\t\t\t\t<td valign=\"top\" width=\"600\" style=\"width:600px;\">
\t\t\t\t<![endif]-->
                <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width:100%; min-width:100%;\" width=\"100%\" class=\"mcnTextContentContainer\">
                    <tbody><tr>
                        
                        <td valign=\"top\" class=\"mcnTextContent\" style=\"padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;\">
                        
                            <div style=\"text-align: right;\"><strong>Fecha:</strong> {{fecha}}<br>
<strong>Número de pedido: </strong>2019-{{idpedido}}</div>

                        </td>
                    </tr>
                </tbody></table>
\t\t\t\t<!--[if mso]>
\t\t\t\t</td>
\t\t\t\t<![endif]-->
                
\t\t\t\t<!--[if mso]>
\t\t\t\t</tr>
\t\t\t\t</table>
\t\t\t\t<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnButtonBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnButtonBlockOuter\">
        <tr>
            <td style=\"padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;\" valign=\"top\" align=\"right\" class=\"mcnButtonBlockInner\">
                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonContentContainer\" style=\"border-collapse: separate !important;border-radius: 0px;background-color: #000000;\">
                    <tbody>
                        <tr>
                            <td align=\"center\" valign=\"middle\" class=\"mcnButtonContent\" style=\"font-family: Arial; font-size: 14px; padding: 15px;\">
                                <a class=\"mcnButton \" title=\"REVISAR PEDIDO\" href=\"\" target=\"_blank\" style=\"font-weight: normal;letter-spacing: 1px;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;\">REVISAR PEDIDO</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnDividerBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnDividerBlockOuter\">
        <tr>
            <td class=\"mcnDividerBlockInner\" style=\"min-width:100%; padding:18px;\">
                <table class=\"mcnDividerContent\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-top: 2px dotted #EAEAEA;\">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class=\"mcnDividerBlockInner\" style=\"padding: 18px;\">
                <hr class=\"mcnDividerContent\" style=\"border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;\" />
-->
            </td>
        </tr>
    </tbody>
</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnTextBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnTextBlockOuter\">
        <tr>
            <td valign=\"top\" class=\"mcnTextBlockInner\" style=\"padding-top:9px;\">
              \t<!--[if mso]>
\t\t\t\t<table align=\"left\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"width:100%;\">
\t\t\t\t<tr>
\t\t\t\t<![endif]-->
\t\t\t    
\t\t\t\t<!--[if mso]>
\t\t\t\t<td valign=\"top\" width=\"600\" style=\"width:600px;\">
\t\t\t\t<![endif]-->
                <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width:100%; min-width:100%;\" width=\"100%\" class=\"mcnTextContentContainer\">
                    <tbody><tr>
                        
                        <td valign=\"top\" class=\"mcnTextContent\" style=\"padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;\">
                        
                             El usuario {{user.nombre}} con código de cliente {{user.cod_user}}, solicitó un pedido a nombre de:<strong>{{datosEnvio.shippingname}}</strong>,<br>Ha realizado el siguiente pedido:
<br>
&nbsp;
                        </td>
                    </tr>
                </tbody></table>
\t\t\t\t<!--[if mso]>
\t\t\t\t</td>
\t\t\t\t<![endif]-->
                
\t\t\t\t<!--[if mso]>
\t\t\t\t</tr>
\t\t\t\t</table>
\t\t\t\t<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnDividerBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnDividerBlockOuter\">
        <tr>
            <td class=\"mcnDividerBlockInner\" style=\"min-width:100%; padding:18px;\">
                <table class=\"mcnDividerContent\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-top: 2px dotted #EAEAEA;\">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class=\"mcnDividerBlockInner\" style=\"padding: 18px;\">
                <hr class=\"mcnDividerContent\" style=\"border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;\" />
-->
            </td>
        </tr>
    </tbody>
</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnTextBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnTextBlockOuter\">
        <tr>
            <td valign=\"top\" class=\"mcnTextBlockInner\" style=\"padding-top:9px;\">
              \t<!--[if mso]>
\t\t\t\t<table align=\"left\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"width:100%;\">
\t\t\t\t<tr>
\t\t\t\t<![endif]-->
\t\t\t    
\t\t\t\t<!--[if mso]>
\t\t\t\t<td valign=\"top\" width=\"300\" style=\"width:300px;\">
\t\t\t\t<![endif]-->
                <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width:300px;\" width=\"100%\" class=\"mcnTextContentContainer\">
                    <tbody><tr>
                        
                        <td valign=\"top\" class=\"mcnTextContent\" style=\"padding-top:0; padding-left:18px; padding-bottom:9px; padding-right:18px;\">
                        
                            <span style=\"font-family:roboto,helvetica neue,helvetica,arial,sans-serif\"><strong>Pedido enviado a:</strong><br>
{{datosEnvio.shippingaddress}}
                                                                                    <br> {{datosEnvio.shippingcity}}
                                                                                    <br></span>
                        </td>
                    </tr>
                </tbody></table>
\t\t\t\t<!--[if mso]>
\t\t\t\t</td>
\t\t\t\t<![endif]-->
                
\t\t\t\t<!--[if mso]>
\t\t\t\t<td valign=\"top\" width=\"300\" style=\"width:300px;\">
\t\t\t\t<![endif]-->
                <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width:300px;\" width=\"100%\" class=\"mcnTextContentContainer\">
                    <tbody><tr>
                        
                        <td valign=\"top\" class=\"mcnTextContent\" style=\"padding-top:0; padding-left:18px; padding-bottom:9px; padding-right:18px;\">
                        
                            <strong>Dirigiado a:</strong><br>
{{datosEnvio.shippingname}}<br>
{{datosEnvio.shippingaddress}}
                        </td>
                    </tr>
                </tbody></table>
\t\t\t\t<!--[if mso]>
\t\t\t\t</td>
\t\t\t\t<![endif]-->
                
\t\t\t\t<!--[if mso]>
\t\t\t\t</tr>
\t\t\t\t</table>
\t\t\t\t<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnTextBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnTextBlockOuter\">
        <tr>
            <td valign=\"top\" class=\"mcnTextBlockInner\" style=\"padding-top:9px;\">
              \t<!--[if mso]>
\t\t\t\t<table align=\"left\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"width:100%;\">
\t\t\t\t<tr>
\t\t\t\t<![endif]-->
\t\t\t    
\t\t\t\t<!--[if mso]>
\t\t\t\t<td valign=\"top\" width=\"600\" style=\"width:600px;\">
\t\t\t\t<![endif]-->
                <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width:100%; min-width:100%;\" width=\"100%\" class=\"mcnTextContentContainer\">
                    <tbody><tr>
                        
                        <td valign=\"top\" class=\"mcnTextContent\" style=\"padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;\">
                        
                            <span style=\"color:#DC0026\"><strong>INFORMACIÓN DEL PEDIDO</strong></span>
                        </td>
                    </tr>
                </tbody></table>
\t\t\t\t<!--[if mso]>
\t\t\t\t</td>
\t\t\t\t<![endif]-->
                
\t\t\t\t<!--[if mso]>
\t\t\t\t</tr>
\t\t\t\t</table>
\t\t\t\t<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnDividerBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnDividerBlockOuter\">
        <tr>
            <td class=\"mcnDividerBlockInner\" style=\"min-width: 100%; padding: 0px 18px 18px;\">
                <table class=\"mcnDividerContent\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-top: 2px solid #DC0026;\">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class=\"mcnDividerBlockInner\" style=\"padding: 18px;\">
                <hr class=\"mcnDividerContent\" style=\"border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;\" />
-->
            </td>
        </tr>
    </tbody>
</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnCaptionBlock\">
    <tbody class=\"mcnCaptionBlockOuter\">
        <tr>
            <td class=\"mcnCaptionBlockInner\" valign=\"top\" style=\"padding:9px;\">
                


                {%for key, item in cart%}
<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnCaptionRightContentOuter\" width=\"100%\">
    <tbody><tr>
        <td valign=\"top\" class=\"mcnCaptionRightContentInner\" style=\"padding:0 9px ;\">
            <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnCaptionRightImageContentContainer\" width=\"264\">
                <tbody><tr>
                    <td class=\"mcnCaptionRightImageContent\" align=\"center\" valign=\"top\">
                    
                        

                        <img alt=\"{{item.name}}\" src=\"{{item.options.img}}\" width=\"264\" style=\"max-width:1500px;\" class=\"mcnImage\">
                        

                    
                    </td>
                </tr>
            </tbody></table>
            <table class=\"mcnCaptionRightTextContentContainer\" align=\"right\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"264\">
                <tbody><tr>
                    <td valign=\"top\" class=\"mcnTextContent\">
                        <strong>{{item.name}}<br>
<span style=\"color:#dc0026\">{{item.price|number_format(0, ',', '.')}} km.</span></strong><br>
<br>
Cantidad:&nbsp;{{item.qty}}<br>
{% if item.options.size is not null%} {% for talla in item.options.size%}
Talla:&nbsp;{{talla.0}} x {{talla.1}} uds<br>{%endfor%} {%endif%}
 {% if item.options.color is not null%}   
Color:{{item.options.color}} {%endif%}
<hr><span style=\"font-size:18px\"><strong>Kilómetros totales: <em>{{(item.price*item.qty)|number_format(0, ',', '.')}} km.</em></strong></span>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>
{%endfor%}



            </td>
        </tr>
    </tbody>
</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnDividerBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnDividerBlockOuter\">
        <tr>
            <td class=\"mcnDividerBlockInner\" style=\"min-width:100%; padding:18px;\">
                <table class=\"mcnDividerContent\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width: 100%;border-top: 2px dotted #818181;\">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class=\"mcnDividerBlockInner\" style=\"padding: 18px;\">
                <hr class=\"mcnDividerContent\" style=\"border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;\" />
-->
            </td>
        </tr>
    </tbody>
</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnTextBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnTextBlockOuter\">
        <tr>
            <td valign=\"top\" class=\"mcnTextBlockInner\" style=\"padding-top:9px;\">
              \t<!--[if mso]>
\t\t\t\t<table align=\"left\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"width:100%;\">
\t\t\t\t<tr>
\t\t\t\t<![endif]-->
\t\t\t    
\t\t\t\t<!--[if mso]>
\t\t\t\t<td valign=\"top\" width=\"600\" style=\"width:600px;\">
\t\t\t\t<![endif]-->
                <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"max-width:100%; min-width:100%;\" width=\"100%\" class=\"mcnTextContentContainer\">
                    <tbody><tr>
                        
                        <td valign=\"top\" class=\"mcnTextContent\" style=\"padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;\">
                        
                            <div style=\"text-align: right;\"><span style=\"font-size:24px\"><strong>Subtotal: {{cart_total|number_format(0, ',', '.')}} Km.</strong></span><br>
Gastos de envío: 0,00 €<br>
<span style=\"color:#dc0026\"><strong><span style=\"font-size:24px\">Total Kiómetros: {{cart_total|number_format(0, ',', '.')}} km.</span></strong></span></div>

                        </td>
                    </tr>
                </tbody></table>
\t\t\t\t<!--[if mso]>
\t\t\t\t</td>
\t\t\t\t<![endif]-->
                
\t\t\t\t<!--[if mso]>
\t\t\t\t</tr>
\t\t\t\t</table>
\t\t\t\t<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnBoxedTextBlock\" style=\"min-width:100%;\">
    <!--[if gte mso 9]>
\t<table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">
\t<![endif]-->
\t<tbody class=\"mcnBoxedTextBlockOuter\">
        <tr>
            <td valign=\"top\" class=\"mcnBoxedTextBlockInner\">
                
\t\t\t\t<!--[if gte mso 9]>
\t\t\t\t<td align=\"center\" valign=\"top\" \">
\t\t\t\t<![endif]-->
                <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"min-width:100%;\" class=\"mcnBoxedTextContentContainer\">
                    <tbody><tr>
                        
                        <td style=\"padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;\">
                        
                            <table border=\"0\" cellspacing=\"0\" class=\"mcnTextContentContainer\" width=\"100%\" style=\"min-width: 100% !important;border: 2px dotted #818181;\">
                                <tbody><tr>
                                    <td valign=\"top\" class=\"mcnTextContent\" style=\"padding: 18px; font-family: Helvetica; font-size: 14px; font-weight: normal; text-align: center;\">
                                        <div style=\"text-align: left;\"><strong>Observaciones:</strong><br>
<br>
{{datosEnvio.shippingmessage}}</div>

                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
\t\t\t\t<!--[if gte mso 9]>
\t\t\t\t</td>
\t\t\t\t<![endif]-->
                
\t\t\t\t<!--[if gte mso 9]>
                </tr>
                </table>
\t\t\t\t<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnImageBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnImageBlockOuter\">
            <tr>
                <td valign=\"top\" style=\"padding:0px\" class=\"mcnImageBlockInner\">
                    <table align=\"left\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnImageContentContainer\" style=\"min-width:100%;\">
                        <tbody><tr>
                            <td class=\"mcnImageContent\" valign=\"top\" style=\"padding-right: 0px; padding-left: 0px; padding-top: 0; padding-bottom: 0; text-align:center;\">
                                
                                    
                                        <img align=\"center\" alt=\"\" src=\"https://gallery.mailchimp.com/cf58ffd76c96f505709247289/images/c3ff8da1-2132-42ae-afba-474aefb2cc88.jpg\" width=\"600\" style=\"max-width:700px; padding-bottom: 0; display: inline !important; vertical-align: bottom;\" class=\"mcnImage\">
                                    
                                
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></td>
                            </tr>
                            <tr>
                                <td valign=\"top\" id=\"templateFooter\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"mcnImageBlock\" style=\"min-width:100%;\">
    <tbody class=\"mcnImageBlockOuter\">
            <tr>
                <td valign=\"top\" style=\"padding:9px\" class=\"mcnImageBlockInner\">
                    <table align=\"left\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnImageContentContainer\" style=\"min-width:100%;\">
                        <tbody><tr>
                            <td class=\"mcnImageContent\" valign=\"top\" style=\"padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;\">
                                
                                    
                                        <img align=\"center\" alt=\"\" src=\"https://gallery.mailchimp.com/cf58ffd76c96f505709247289/images/e12b1576-05d1-4c90-b86d-2ed4fef1d94f.png\" width=\"250\" style=\"max-width:250px; padding-bottom: 0; display: inline !important; vertical-align: bottom;\" class=\"mcnImage\">
                                    
                                
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></td>
                            </tr>
                        </table>
                        <!--[if (gte mso 9)|(IE)]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>
", "mails/pedido_admin.php", "/Applications/MAMP/htdocs/Bushido2018/application/views/mails/pedido_admin.php");
    }
}
