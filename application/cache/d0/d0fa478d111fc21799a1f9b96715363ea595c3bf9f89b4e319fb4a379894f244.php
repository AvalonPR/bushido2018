<?php

/* viaje.php */
class __TwigTemplate_6b796f9c1fd93f0dc13c6e0e2a97c963792cb2eeda4122094059487492f23200 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "

<!-- Page Title
                ============================================= -->
<section id=\"page-title\" class=\"header viaje_header\">

    <div class=\"container clearfix\">
        <h1 style=\"\">";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "titulo", array()), "html", null, true);
        echo "</h1>
    </div>

</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id=\"content\">


    <div class=\"content-wrap\" style=\"padding-bottom: 0;\">

        <div class=\"container clearfix\">

            <div class=\"row clearfix\">

                <div class=\"col-sm-12\">

                    <div class=\"clear\"></div>

                    <div class=\"row clearfix\">

                        <div class=\"col-md-12 bottommargin-lg\">

                            ";
        // line 31
        if ((null === $this->getAttribute((isset($context["userData"]) ? $context["userData"] : null), "viaje", array()))) {
            // line 32
            echo "                            
                            <h3 style=\"\">";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "subtitulo", array()), "html", null, true);
            echo "</h3>
                            
                            <p>";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "bienvenida", array()), "html", null, true);
            echo "</p>
                            
                            <p>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "condiciones", array()), "html", null, true);
            echo "*</p>
                            <ul>
                                <li>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "dosplazas", array()), "html", null, true);
            echo "</li>
                                <li>";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "unaplaza", array()), "html", null, true);
            echo "</li>
                            </ul>
                            <div class=\"style-msg\" style=\"background-color: #EEE;\">
                                <div class=\"sb-msg\"><b>";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "sinviaje", array()), "html", null, true);
            echo " </b></div>
                            </div>
                            
                            
                            <div class=\"center\" style=\"margin-bottom: 20px;\">
                                <a href=\"#myModal1\" data-lightbox=\"inline\" class=\"button button-large button-rounded\">";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "boton", array()), "html", null, true);
            echo "</a>\t\t\t\t\t\t
                            </div>
                            <p>*";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "corte", array()), "html", null, true);
            echo "</p>
                            <img src=\"";
            // line 51
            echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
            echo "viaje-bushido.jpg\" alt=\"Viaje Yokohama Bushido 2020-2021\">
                        
                            <!-- Modal -->
                            <div class=\"modal1 mfp-hide\" id=\"myModal1\">
                                <div class=\"block divcenter\" style=\"background-color: #dc0026; max-width: 520px;\">
                                    <button title=\"Close (Esc)\" type=\"button\" class=\"mfp-close\">×</button>
                                    <div class=\"row nomargin clearfix\">

                                        <div class=\"col-padding\" data-height-xl=\"350\" data-height-lg=\"400\" data-height-md=\"456\" data-height-sm=\"456\" data-height-xs=\"456\">

                                            <h3>";
            // line 61
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "estimado", array()), "html", null, true);
            echo "</h3>
                                            
                                            <p>";
            // line 63
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "eleccion", array()), "html", null, true);
            echo "</p>
                                            <form class=\"form-legales clearfix\" id=\"aceptar-viaje\" >
                                                <div>
                                                    <input id=\"radio-10\" class=\"radio-style\" name=\"radio-viaje\" type=\"radio\" value=\"1\">
                                                    <label for=\"radio-10\" class=\"radio-style-3-label\">1 ";
            // line 67
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "plaza", array()), "html", null, true);
            echo "</label>
                                                </div>
                                                <div id=\"yearSelect\" style='display: none;'>
                                                    <p>";
            // line 70
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "eleccionyear", array()), "html", null, true);
            echo "</p>
                                                    <div>
                                                        <input id=\"radio-2020\" class=\"radio-style\"name=\"radio-year\" type=\"radio\" value=\"2020\" checked>
                                                        <label for=\"radio-2020\" class=\"radio-style-3-label radio-small\">2020</label>
                                                    </div>
                                                    <div>
                                                        <input id=\"radio-2021\" class=\"radio-style\" name=\"radio-year\" type=\"radio\" value=\"2021\">
                                                        <label for=\"radio-2021\" class=\"radio-style-3-label radio-small\">2021</label>
                                                    </div>
                                                </div>
                                                <div>
                                                    <input id=\"radio-11\" class=\"radio-style\"name=\"radio-viaje\" type=\"radio\" value=\"2\">
                                                    <label for=\"radio-11\" class=\"radio-style-3-label\">2 ";
            // line 82
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "plazas", array()), "html", null, true);
            echo "</label>
                                                </div>
                                                <div>
                                                    <input id=\"radio-12\" class=\"radio-style\" name=\"radio-viaje\" type=\"radio\" value=\"0\">
                                                    <label for=\"radio-12\" class=\"radio-style-3-label\">";
            // line 86
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "participar", array()), "html", null, true);
            echo "</label>
                                                </div>
                                                <button class=\"button button-dark fright\" onclick=\"aceptarViaje()\">";
            // line 88
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "aceptar", array()), "html", null, true);
            echo "</button></p>
                                                <div class=\"clear\"></div>
                                            </form>
                                            

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Fin Modal bases legales-->    
                            ";
        } else {
            // line 99
            echo "
                            
                            <h3>";
            // line 101
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "condiciones", array()), "html", null, true);
            echo "</h3>
                            <ul>
                                <li>";
            // line 103
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "dosplazas", array()), "html", null, true);
            echo "</li>
                                <li>";
            // line 104
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "unaplaza", array()), "html", null, true);
            echo "</li>
                            </ul>
                            <div class=\"style-msg\" style=\"background-color: #EEE;\">
                                <div class=\"sb-msg\"><b>";
            // line 107
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "sinviaje", array()), "html", null, true);
            echo " </b></div>
                            </div>
                            
                            <p>*";
            // line 110
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "corte", array()), "html", null, true);
            echo "</p>
                            <img src=\"";
            // line 111
            echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
            echo "viaje-bushido.jpg\" alt=\"Viaje Yokohama Bushido 2020-2021\">
                            
                            ";
        }
        // line 114
        echo "                            <div class=\"divider\"><i class=\"icon-circle\"></i></div>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!--<div class=\"col_full viaje_call center\">
        <h3 style=\"color:white;\">¿Ya has hecho las maletas?</h3>
        <a href=\"";
        // line 129
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "uploads/agenda_CruceroBushido_2018.pdf\" class=\"button button-xlarge tright button-dark\" id=\"travel_button\">Descarga aquí la agenda del viaje <i class=\"material-icons\">cloud_download</i></a></br>
    <span>&nbsp;</span>
</div>-->
</section><!-- #content end -->
";
    }

    public function getTemplateName()
    {
        return "viaje.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  225 => 129,  208 => 114,  202 => 111,  198 => 110,  192 => 107,  186 => 104,  182 => 103,  177 => 101,  173 => 99,  159 => 88,  154 => 86,  147 => 82,  132 => 70,  126 => 67,  119 => 63,  114 => 61,  101 => 51,  97 => 50,  92 => 48,  84 => 43,  78 => 40,  74 => 39,  69 => 37,  64 => 35,  59 => 33,  56 => 32,  54 => 31,  28 => 8,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("

<!-- Page Title
                ============================================= -->
<section id=\"page-title\" class=\"header viaje_header\">

    <div class=\"container clearfix\">
        <h1 style=\"\">{{textos.titulo}}</h1>
    </div>

</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id=\"content\">


    <div class=\"content-wrap\" style=\"padding-bottom: 0;\">

        <div class=\"container clearfix\">

            <div class=\"row clearfix\">

                <div class=\"col-sm-12\">

                    <div class=\"clear\"></div>

                    <div class=\"row clearfix\">

                        <div class=\"col-md-12 bottommargin-lg\">

                            {% if userData.viaje is null%}
                            
                            <h3 style=\"\">{{textos.subtitulo}}</h3>
                            
                            <p>{{textos.bienvenida}}</p>
                            
                            <p>{{textos.condiciones}}*</p>
                            <ul>
                                <li>{{textos.dosplazas}}</li>
                                <li>{{textos.unaplaza}}</li>
                            </ul>
                            <div class=\"style-msg\" style=\"background-color: #EEE;\">
                                <div class=\"sb-msg\"><b>{{textos.sinviaje}} </b></div>
                            </div>
                            
                            
                            <div class=\"center\" style=\"margin-bottom: 20px;\">
                                <a href=\"#myModal1\" data-lightbox=\"inline\" class=\"button button-large button-rounded\">{{textos.boton}}</a>\t\t\t\t\t\t
                            </div>
                            <p>*{{textos.corte}}</p>
                            <img src=\"{{imgurl}}viaje-bushido.jpg\" alt=\"Viaje Yokohama Bushido 2020-2021\">
                        
                            <!-- Modal -->
                            <div class=\"modal1 mfp-hide\" id=\"myModal1\">
                                <div class=\"block divcenter\" style=\"background-color: #dc0026; max-width: 520px;\">
                                    <button title=\"Close (Esc)\" type=\"button\" class=\"mfp-close\">×</button>
                                    <div class=\"row nomargin clearfix\">

                                        <div class=\"col-padding\" data-height-xl=\"350\" data-height-lg=\"400\" data-height-md=\"456\" data-height-sm=\"456\" data-height-xs=\"456\">

                                            <h3>{{textos.estimado}}</h3>
                                            
                                            <p>{{textos.eleccion}}</p>
                                            <form class=\"form-legales clearfix\" id=\"aceptar-viaje\" >
                                                <div>
                                                    <input id=\"radio-10\" class=\"radio-style\" name=\"radio-viaje\" type=\"radio\" value=\"1\">
                                                    <label for=\"radio-10\" class=\"radio-style-3-label\">1 {{textos.plaza}}</label>
                                                </div>
                                                <div id=\"yearSelect\" style='display: none;'>
                                                    <p>{{textos.eleccionyear}}</p>
                                                    <div>
                                                        <input id=\"radio-2020\" class=\"radio-style\"name=\"radio-year\" type=\"radio\" value=\"2020\" checked>
                                                        <label for=\"radio-2020\" class=\"radio-style-3-label radio-small\">2020</label>
                                                    </div>
                                                    <div>
                                                        <input id=\"radio-2021\" class=\"radio-style\" name=\"radio-year\" type=\"radio\" value=\"2021\">
                                                        <label for=\"radio-2021\" class=\"radio-style-3-label radio-small\">2021</label>
                                                    </div>
                                                </div>
                                                <div>
                                                    <input id=\"radio-11\" class=\"radio-style\"name=\"radio-viaje\" type=\"radio\" value=\"2\">
                                                    <label for=\"radio-11\" class=\"radio-style-3-label\">2 {{textos.plazas}}</label>
                                                </div>
                                                <div>
                                                    <input id=\"radio-12\" class=\"radio-style\" name=\"radio-viaje\" type=\"radio\" value=\"0\">
                                                    <label for=\"radio-12\" class=\"radio-style-3-label\">{{textos.participar}}</label>
                                                </div>
                                                <button class=\"button button-dark fright\" onclick=\"aceptarViaje()\">{{textos.aceptar}}</button></p>
                                                <div class=\"clear\"></div>
                                            </form>
                                            

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Fin Modal bases legales-->    
                            {%else%}

                            
                            <h3>{{textos.condiciones}}</h3>
                            <ul>
                                <li>{{textos.dosplazas}}</li>
                                <li>{{textos.unaplaza}}</li>
                            </ul>
                            <div class=\"style-msg\" style=\"background-color: #EEE;\">
                                <div class=\"sb-msg\"><b>{{textos.sinviaje}} </b></div>
                            </div>
                            
                            <p>*{{textos.corte}}</p>
                            <img src=\"{{imgurl}}viaje-bushido.jpg\" alt=\"Viaje Yokohama Bushido 2020-2021\">
                            
                            {%endif%}
                            <div class=\"divider\"><i class=\"icon-circle\"></i></div>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!--<div class=\"col_full viaje_call center\">
        <h3 style=\"color:white;\">¿Ya has hecho las maletas?</h3>
        <a href=\"{{base_url()}}uploads/agenda_CruceroBushido_2018.pdf\" class=\"button button-xlarge tright button-dark\" id=\"travel_button\">Descarga aquí la agenda del viaje <i class=\"material-icons\">cloud_download</i></a></br>
    <span>&nbsp;</span>
</div>-->
</section><!-- #content end -->
", "viaje.php", "C:\\MAMP\\htdocs\\Bushido2018\\application\\views\\viaje.php");
    }
}
