<?php

/* cart/cart-process.php */
class __TwigTemplate_2dec83ef547793ddea4f6aab09a36c64a4c64aaae6def0723a1f886c61e95a87 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- Page Title
                ============================================= -->
<section id=\"page-title\" class=\"carrito_header\">

    <div class=\"container clearfix\">
        <h1>";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "titulo", array()), "html", null, true);
        echo "</h1>
        <span style=\"color: black\"></span>
    </div>

</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id=\"content\" class=\"content-cart\">
    
    <!--Process Tabs Mobile-->
    <div id=\"processTabsMobile\" class=\"hidden-lg hidden-md hidden-sm\">
        <ul class=\"process-steps bottommargin clearfix\">
            <li>
                <a id=\"carrito\"></a>
                <h5>";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "titulo", array()), "html", null, true);
        echo "</h5>
            </li>
            <li class=\"active\">
                <a id=\"envio\"></a>
                <h5>";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "envio", array()), "html", null, true);
        echo "</h5>
            </li>
            <li>
                <a id=\"pago\"></a>
                <h5>";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "pago", array()), "html", null, true);
        echo "</h5>
            </li>
            <li>
                <a id=\"pedido\"></a>
                <h5>";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "pedido", array()), "html", null, true);
        echo "</h5>
            </li>
        </ul>
    </div>
    <!--Fin Process Tabs Mobile-->

    <div class=\"content-wrap\">

        <div class=\"container clearfix\">
        <div id=\"processTabs\" class=\"hidden-xs\">
                <ul class=\"process-steps bottommargin clearfix\">
                    <li >
                        <a class=\"i-circled i-bordered\" id=\"carrito\"></a>
                   <h5>";
        // line 45
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "pasos", array()), "primero", array()), "html", null, true);
        echo "</h5>
                    </li>
                    <li class=\"active\">
                        <a class=\"i-circled i-bordered\" id=\"envio\"></a>
                        <h5>";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "pasos", array()), "segundo", array()), "html", null, true);
        echo "</h5>
                    </li>
                    <li>
                        <a class=\"i-circled i-bordered\" id=\"pago\"></a>
                        <h5>";
        // line 53
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "pasos", array()), "tercero", array()), "html", null, true);
        echo "</h5>
                    </li>
                    <li>
                        <a class=\"i-circled i-bordered\" id=\"pedido\"></a>
                        <h5>";
        // line 57
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "pasos", array()), "cuarto", array()), "html", null, true);
        echo "</h5>
                    </li>
                </ul>
            </div>

            <div class=\"contact-widget\">
                <div class=\"contact-form-result\"></div>
                    <div class=\"row\">
                <form action=\"";
        // line 65
        echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
        echo "carrito/saveDatos\" method=\"post\" novalidate=\"validate\">
                        <div class=\"col-md-6\">
                            <h3 style=\"margin: 0;\">";
        // line 67
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "process", array()), "tablaFact", array()), "titulo", array()), "html", null, true);
        echo "</h3>
                            <div class=\"line-custom\"></div>
                            <p style=\"line-height: 35px;\">&nbsp;</p>

                            <div class=\"col_full\">
                                <label for=\"billing-form-companyname\">";
        // line 72
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "process", array()), "tablaFact", array()), "nombre", array()), "html", null, true);
        echo ":</label>
                                <input type=\"text\" id=\"billing-form-companyname\" name=\"billing-form-companyname\" value=\"";
        // line 73
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "nombre", array())), "html", null, true);
        echo "\" class=\"sm-form-control\" />
                            </div>

                            <div class=\"col_full\">
                                <label for=\"billing-form-address\">";
        // line 77
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "process", array()), "tablaFact", array()), "dir", array()), "html", null, true);
        echo ":</label>
                                <input type=\"text\" id=\"billing-form-address\" name=\"billing-form-address\" value=\"";
        // line 78
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute((isset($context["direccionesFact"]) ? $context["direccionesFact"] : null), "Calle", array())), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["direccionesFact"]) ? $context["direccionesFact"] : null), "CP", array()), "html", null, true);
        echo "\" class=\"sm-form-control\" />
                            </div>

                            <div class=\"col_full\">
                                <label for=\"billing-form-city\">";
        // line 82
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "process", array()), "tablaFact", array()), "pob", array()), "html", null, true);
        echo ":</label>
                                <input type=\"text\" id=\"billing-form-city\" name=\"billing-form-city\" value=\"";
        // line 83
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute((isset($context["direccionesFact"]) ? $context["direccionesFact"] : null), "Ciudad", array())), "html", null, true);
        echo "\" class=\"sm-form-control\" />
                            </div>

                            <div class=\"col_half\">
                                <label for=\"billing-form-email\">";
        // line 87
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "process", array()), "tablaFact", array()), "email", array()), "html", null, true);
        echo ":</label>
                                <input type=\"email\" id=\"billing-form-email\" name=\"billing-form-email\" value=\"";
        // line 88
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "email", array()), "html", null, true);
        echo "\" class=\"sm-form-control\" readonly />
                            </div>

                            <div class=\"col_half col_last\">
                                <label for=\"billing-form-phone\">";
        // line 92
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "process", array()), "tablaFact", array()), "tfno", array()), "html", null, true);
        echo ":</label>
                                <input type=\"text\" id=\"billing-form-phone\" name=\"billing-form-phone\" value=\"";
        // line 93
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "telefono", array()), "html", null, true);
        echo "\" class=\"sm-form-control\" />
                            </div>

                        </div>
                        <div class=\"col-md-6\">
                            <h3 style=\"margin: 0;\">";
        // line 98
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "process", array()), "tablaEnvio", array()), "titulo", array()), "html", null, true);
        echo "</h3>
                            <div class=\"line-custom\"></div>
                            <p><input type=\"checkbox\" id=\"envioBox\" style=\"margin-right: 15px;\"><label> ";
        // line 100
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "process", array()), "tablaEnvio", array()), "same", array()), "html", null, true);
        echo "</label></p>

                            <div class=\"col_full\">
                                <label for=\"shipping-form-name\">";
        // line 103
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "process", array()), "tablaEnvio", array()), "recev", array()), "html", null, true);
        echo ":</label>
                                <input type=\"text\" id=\"shipping-form-name\" name=\"shipping-form-name\" value=\"\" class=\"sm-form-control\"  required/>
                            </div>

                            <div class=\"clear\"></div>

                            <div class=\"col_full\">
                                <label for=\"shipping-form-companyname\">";
        // line 110
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "process", array()), "tablaEnvio", array()), "nombre", array()), "html", null, true);
        echo ":</label>
                                <input type=\"text\" id=\"shipping-form-companyname\" name=\"shipping-form-companyname\" class=\"sm-form-control\" required/>
                            </div>

                            <div class=\"col_full\">
                                <label for=\"shipping-form-address\">";
        // line 115
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "process", array()), "tablaEnvio", array()), "dir", array()), "html", null, true);
        echo ":</label>
                                <input type=\"text\" id=\"shipping-form-address\" name=\"shipping-form-address\"  class=\"sm-form-control\" required/>
                            </div>

                            <div class=\"col_full\">
                                <label for=\"shipping-form-city\">";
        // line 120
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "process", array()), "tablaEnvio", array()), "pob", array()), "html", null, true);
        echo ":</label>
                                <input type=\"text\" id=\"shipping-form-city\" name=\"shipping-form-city\" class=\"sm-form-control\" required/>
                            </div>

                            <div class=\"col_full\">
                                <label for=\"shipping-form-message\">";
        // line 125
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "process", array()), "tablaEnvio", array()), "obs", array()), "html", null, true);
        echo " <small>*</small></label>
                                <textarea class=\"sm-form-control\" id=\"shipping-form-message\" name=\"shipping-form-message\" rows=\"6\" cols=\"30\"></textarea>
                            </div>

                        </div>

                        <div class=\"col-md-12\">
                            <a href=\"";
        // line 132
        echo twig_escape_filter($this->env, (isset($context["siteurl"]) ? $context["siteurl"] : null), "html", null, true);
        echo "carrito\" class=\"fleft button button-3d button-black\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "process", array()), "backButt", array()), "html", null, true);
        echo "</a>
                            <button class=\"button button-3d fright\">";
        // line 133
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "process", array()), "checkButt", array()), "html", null, true);
        echo "</button>
                        </div>
                </form>
                    </div>
            </div>
        </div>
    </div>
</section><!-- #content end -->
";
    }

    public function getTemplateName()
    {
        return "cart/cart-process.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  244 => 133,  238 => 132,  228 => 125,  220 => 120,  212 => 115,  204 => 110,  194 => 103,  188 => 100,  183 => 98,  175 => 93,  171 => 92,  164 => 88,  160 => 87,  153 => 83,  149 => 82,  140 => 78,  136 => 77,  129 => 73,  125 => 72,  117 => 67,  112 => 65,  101 => 57,  94 => 53,  87 => 49,  80 => 45,  64 => 32,  57 => 28,  50 => 24,  43 => 20,  26 => 6,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- Page Title
                ============================================= -->
<section id=\"page-title\" class=\"carrito_header\">

    <div class=\"container clearfix\">
        <h1>{{textos.index.titulo}}</h1>
        <span style=\"color: black\"></span>
    </div>

</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id=\"content\" class=\"content-cart\">
    
    <!--Process Tabs Mobile-->
    <div id=\"processTabsMobile\" class=\"hidden-lg hidden-md hidden-sm\">
        <ul class=\"process-steps bottommargin clearfix\">
            <li>
                <a id=\"carrito\"></a>
                <h5>{{textos.index.titulo}}</h5>
            </li>
            <li class=\"active\">
                <a id=\"envio\"></a>
                <h5>{{textos.index.envio}}</h5>
            </li>
            <li>
                <a id=\"pago\"></a>
                <h5>{{textos.index.pago}}</h5>
            </li>
            <li>
                <a id=\"pedido\"></a>
                <h5>{{textos.index.pedido}}</h5>
            </li>
        </ul>
    </div>
    <!--Fin Process Tabs Mobile-->

    <div class=\"content-wrap\">

        <div class=\"container clearfix\">
        <div id=\"processTabs\" class=\"hidden-xs\">
                <ul class=\"process-steps bottommargin clearfix\">
                    <li >
                        <a class=\"i-circled i-bordered\" id=\"carrito\"></a>
                   <h5>{{textos.index.pasos.primero}}</h5>
                    </li>
                    <li class=\"active\">
                        <a class=\"i-circled i-bordered\" id=\"envio\"></a>
                        <h5>{{textos.index.pasos.segundo}}</h5>
                    </li>
                    <li>
                        <a class=\"i-circled i-bordered\" id=\"pago\"></a>
                        <h5>{{textos.index.pasos.tercero}}</h5>
                    </li>
                    <li>
                        <a class=\"i-circled i-bordered\" id=\"pedido\"></a>
                        <h5>{{textos.index.pasos.cuarto}}</h5>
                    </li>
                </ul>
            </div>

            <div class=\"contact-widget\">
                <div class=\"contact-form-result\"></div>
                    <div class=\"row\">
                <form action=\"{{baseurl}}carrito/saveDatos\" method=\"post\" novalidate=\"validate\">
                        <div class=\"col-md-6\">
                            <h3 style=\"margin: 0;\">{{textos.process.tablaFact.titulo}}</h3>
                            <div class=\"line-custom\"></div>
                            <p style=\"line-height: 35px;\">&nbsp;</p>

                            <div class=\"col_full\">
                                <label for=\"billing-form-companyname\">{{textos.process.tablaFact.nombre}}:</label>
                                <input type=\"text\" id=\"billing-form-companyname\" name=\"billing-form-companyname\" value=\"{{user.nombre|capitalize}}\" class=\"sm-form-control\" />
                            </div>

                            <div class=\"col_full\">
                                <label for=\"billing-form-address\">{{textos.process.tablaFact.dir}}:</label>
                                <input type=\"text\" id=\"billing-form-address\" name=\"billing-form-address\" value=\"{{direccionesFact.Calle|capitalize}}, {{direccionesFact.CP}}\" class=\"sm-form-control\" />
                            </div>

                            <div class=\"col_full\">
                                <label for=\"billing-form-city\">{{textos.process.tablaFact.pob}}:</label>
                                <input type=\"text\" id=\"billing-form-city\" name=\"billing-form-city\" value=\"{{direccionesFact.Ciudad|capitalize}}\" class=\"sm-form-control\" />
                            </div>

                            <div class=\"col_half\">
                                <label for=\"billing-form-email\">{{textos.process.tablaFact.email}}:</label>
                                <input type=\"email\" id=\"billing-form-email\" name=\"billing-form-email\" value=\"{{user.email}}\" class=\"sm-form-control\" readonly />
                            </div>

                            <div class=\"col_half col_last\">
                                <label for=\"billing-form-phone\">{{textos.process.tablaFact.tfno}}:</label>
                                <input type=\"text\" id=\"billing-form-phone\" name=\"billing-form-phone\" value=\"{{user.telefono}}\" class=\"sm-form-control\" />
                            </div>

                        </div>
                        <div class=\"col-md-6\">
                            <h3 style=\"margin: 0;\">{{textos.process.tablaEnvio.titulo}}</h3>
                            <div class=\"line-custom\"></div>
                            <p><input type=\"checkbox\" id=\"envioBox\" style=\"margin-right: 15px;\"><label> {{textos.process.tablaEnvio.same}}</label></p>

                            <div class=\"col_full\">
                                <label for=\"shipping-form-name\">{{textos.process.tablaEnvio.recev}}:</label>
                                <input type=\"text\" id=\"shipping-form-name\" name=\"shipping-form-name\" value=\"\" class=\"sm-form-control\"  required/>
                            </div>

                            <div class=\"clear\"></div>

                            <div class=\"col_full\">
                                <label for=\"shipping-form-companyname\">{{textos.process.tablaEnvio.nombre}}:</label>
                                <input type=\"text\" id=\"shipping-form-companyname\" name=\"shipping-form-companyname\" class=\"sm-form-control\" required/>
                            </div>

                            <div class=\"col_full\">
                                <label for=\"shipping-form-address\">{{textos.process.tablaEnvio.dir}}:</label>
                                <input type=\"text\" id=\"shipping-form-address\" name=\"shipping-form-address\"  class=\"sm-form-control\" required/>
                            </div>

                            <div class=\"col_full\">
                                <label for=\"shipping-form-city\">{{textos.process.tablaEnvio.pob}}:</label>
                                <input type=\"text\" id=\"shipping-form-city\" name=\"shipping-form-city\" class=\"sm-form-control\" required/>
                            </div>

                            <div class=\"col_full\">
                                <label for=\"shipping-form-message\">{{textos.process.tablaEnvio.obs}} <small>*</small></label>
                                <textarea class=\"sm-form-control\" id=\"shipping-form-message\" name=\"shipping-form-message\" rows=\"6\" cols=\"30\"></textarea>
                            </div>

                        </div>

                        <div class=\"col-md-12\">
                            <a href=\"{{siteurl}}carrito\" class=\"fleft button button-3d button-black\">{{textos.process.backButt}}</a>
                            <button class=\"button button-3d fright\">{{textos.process.checkButt}}</button>
                        </div>
                </form>
                    </div>
            </div>
        </div>
    </div>
</section><!-- #content end -->
", "cart/cart-process.php", "E:\\Programas\\MAMP\\htdocs\\Bushido2018\\application\\views\\cart\\cart-process.php");
    }
}
