<?php

/* canjeo.php */
class __TwigTemplate_58f4a097b3e59f68e5254a25c4d805a5df31b53ed340d3d2bb1c9ad5ad8ea63b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- Page Title
============================================= -->
<section id=\"page-title\" class=\"header programa_header\">

    <div class=\"container clearfix\">
        <h1>Condiciones de canjeo</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
        ============================================= -->
<section id=\"content\">
    <div class=\"content-wrap\">
    
    

    

        <div class=\"container clearfix legal\">
            <!-- Post Content
            ============================================= -->
            <div class=\"postcontent nobottommargin col_last clearfix\">
                
        
                
            </div>
            
            
            <!-- Sidebar
            ============================================= -->
            <div class=\"sidebar nobottommargin clearfix\">
                <div class=\"sidebar-widgets-wrap\">
                    <div class=\"widget widget_links clearfix\">

                        <h3>El programa</h3>
                        <ul>
                            <li><a href=\"";
        // line 38
        echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
        echo "inicio/bases\"><div>Bases legales</div></a></li>
                            <li><a href=\"";
        // line 39
        echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
        echo "inicio/generales\"><div>Condiciones generales</div></a></li>
                            <li><a href=\"";
        // line 40
        echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
        echo "inicio/privacidad\"><div>Privacidad</div></a></li>
                            <li><a href=\"";
        // line 41
        echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
        echo "inicio/canjeo\" class=\"active\"><div>Canjeo</div></a></li>
                            <li><a href=\"";
        // line 42
        echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
        echo "inicio/cookies\"><div>Cookies</div></a></li> 
                                
                        </ul>

                    </div>
                </div>
            </div>
            
            <!-- Sidebar END
            ============================================= -->
            
        </div>
    
    </div>

</section><!-- #content end -->
";
    }

    public function getTemplateName()
    {
        return "canjeo.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 42,  70 => 41,  66 => 40,  62 => 39,  58 => 38,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- Page Title
============================================= -->
<section id=\"page-title\" class=\"header programa_header\">

    <div class=\"container clearfix\">
        <h1>Condiciones de canjeo</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
        ============================================= -->
<section id=\"content\">
    <div class=\"content-wrap\">
    
    

    

        <div class=\"container clearfix legal\">
            <!-- Post Content
            ============================================= -->
            <div class=\"postcontent nobottommargin col_last clearfix\">
                
        
                
            </div>
            
            
            <!-- Sidebar
            ============================================= -->
            <div class=\"sidebar nobottommargin clearfix\">
                <div class=\"sidebar-widgets-wrap\">
                    <div class=\"widget widget_links clearfix\">

                        <h3>El programa</h3>
                        <ul>
                            <li><a href=\"{{baseurl}}inicio/bases\"><div>Bases legales</div></a></li>
                            <li><a href=\"{{baseurl}}inicio/generales\"><div>Condiciones generales</div></a></li>
                            <li><a href=\"{{baseurl}}inicio/privacidad\"><div>Privacidad</div></a></li>
                            <li><a href=\"{{baseurl}}inicio/canjeo\" class=\"active\"><div>Canjeo</div></a></li>
                            <li><a href=\"{{baseurl}}inicio/cookies\"><div>Cookies</div></a></li> 
                                
                        </ul>

                    </div>
                </div>
            </div>
            
            <!-- Sidebar END
            ============================================= -->
            
        </div>
    
    </div>

</section><!-- #content end -->
", "canjeo.php", "/Applications/MAMP/htdocs/Bushido2018/application/views/canjeo.php");
    }
}
