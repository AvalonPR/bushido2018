<?php

/* marketing.php */
class __TwigTemplate_7daedf00d20de7d6d22bafbb8326876e03b14363689964125271396de32f425f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- Page Title
============================================= -->
<section id=\"page-title\" class=\"header marketing_header\">

    <div class=\"container clearfix\">
        <h1>Marketing</h1>
    </div>

</section><!-- #page-title end -->



<!-- Content
============================================= -->
<section id=\"content\">

    <div class=\"content-wrap\">

        <div class=\"container clearfix\">
            
            
            <!-- Portfolio Filter
            ============================================= -->
            <ul class=\"portfolio-filter clearfix\" data-container=\"#portfolio\">

                    <li class=\"activeFilter\"><a href=\"#\" data-filter=\"*\">Mostrar todos</a></li>
                    <li><a href=\"#\" data-filter=\".pf-OTR\">OTR</a></li>
                    <li><a href=\"#\" data-filter=\".pf-TBS\">TBS</a></li>
                    <li><a href=\"#\" data-filter=\".pf-OE\">OE</a></li>
                    <li><a href=\"#\" data-filter=\".pf-Baremo\">Baremos</a></li>
            </ul><!-- #portfolio-filter end -->
            
            <div class=\"clear\"></div>
            
            <!-- Portfolio Items
            ============================================= -->
            
            <div id=\"portfolio\" class=\"portfolio grid-container clearfix\">
                ";
        // line 39
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["catalogos"]) ? $context["catalogos"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["catalogo"]) {
            // line 40
            echo "                <article class=\"portfolio-item pf-";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["catalogo"], "filtros", array()), "Filtros", array()), 0, array()), "html", null, true);
            echo " pf-icons\">
                        <div class=\"portfolio-image\">
                                ";
            // line 42
            if (($this->getAttribute($context["catalogo"], "tipo", array()) == "pdf")) {
                // line 43
                echo "                                <a href=\"#\">
                                        <img src=\"";
                // line 44
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "assets/catalogo/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["catalogo"], "id", array()), "html", null, true);
                echo "/portada/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["catalogo"], "portada", array()), "html", null, true);
                echo "\" alt=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["catalogo"], "descripcion", array()), "html", null, true);
                echo "\">
                                </a>
                                ";
            } else {
                // line 47
                echo "                                <a href=\"#\"><img src=\"";
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "assets/catalogo/icono-excel.png\" alt=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["catalogo"], "descripcion", array()), "html", null, true);
                echo "\"></a>
                                ";
            }
            // line 49
            echo "                                <div class=\"portfolio-overlay\">
                                    <a href=\"";
            // line 50
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            echo "assets/catalogo/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["catalogo"], "id", array()), "html", null, true);
            echo "/pdf/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["catalogo"], "pdf", array()), "html", null, true);
            echo "\" class=\"left-icon\" target=\"_blank\"><i class=\"material-icons\">remove_red_eye</i></a>
                                    <a href=\"";
            // line 51
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            echo "assets/catalogo/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["catalogo"], "id", array()), "html", null, true);
            echo "/pdf/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["catalogo"], "pdf", array()), "html", null, true);
            echo "\" class=\"right-icon\"><i class=\"material-icons\">file_download</i></a>
                                </div>
                        </div>
                        <div class=\"portfolio-desc\">
                                <h3><a href=\"";
            // line 55
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            echo "assets/catalogo/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["catalogo"], "id", array()), "html", null, true);
            echo "/pdf/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["catalogo"], "pdf", array()), "html", null, true);
            echo "\"><h3>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["catalogo"], "titulo", array()), "html", null, true);
            echo "</h3></a></h3>
                                <span><a href=\"#\"> ";
            // line 56
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["catalogo"], "filtros", array()), "Filtros", array()), 0, array()), "html", null, true);
            echo "</a></span>
                        </div>
                </article>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['catalogo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 60
        echo "            </div>
            
            
            
            
              
        </div>
        <!--Container end-->

    </div>
</section>
";
    }

    public function getTemplateName()
    {
        return "marketing.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 60,  126 => 56,  116 => 55,  105 => 51,  97 => 50,  94 => 49,  86 => 47,  74 => 44,  71 => 43,  69 => 42,  63 => 40,  59 => 39,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- Page Title
============================================= -->
<section id=\"page-title\" class=\"header marketing_header\">

    <div class=\"container clearfix\">
        <h1>Marketing</h1>
    </div>

</section><!-- #page-title end -->



<!-- Content
============================================= -->
<section id=\"content\">

    <div class=\"content-wrap\">

        <div class=\"container clearfix\">
            
            
            <!-- Portfolio Filter
            ============================================= -->
            <ul class=\"portfolio-filter clearfix\" data-container=\"#portfolio\">

                    <li class=\"activeFilter\"><a href=\"#\" data-filter=\"*\">Mostrar todos</a></li>
                    <li><a href=\"#\" data-filter=\".pf-OTR\">OTR</a></li>
                    <li><a href=\"#\" data-filter=\".pf-TBS\">TBS</a></li>
                    <li><a href=\"#\" data-filter=\".pf-OE\">OE</a></li>
                    <li><a href=\"#\" data-filter=\".pf-Baremo\">Baremos</a></li>
            </ul><!-- #portfolio-filter end -->
            
            <div class=\"clear\"></div>
            
            <!-- Portfolio Items
            ============================================= -->
            
            <div id=\"portfolio\" class=\"portfolio grid-container clearfix\">
                {%for catalogo in catalogos%}
                <article class=\"portfolio-item pf-{{catalogo.filtros.Filtros.0}} pf-icons\">
                        <div class=\"portfolio-image\">
                                {% if catalogo.tipo == 'pdf'%}
                                <a href=\"#\">
                                        <img src=\"{{base_url()}}assets/catalogo/{{catalogo.id}}/portada/{{catalogo.portada}}\" alt=\"{{catalogo.descripcion}}\">
                                </a>
                                {%else%}
                                <a href=\"#\"><img src=\"{{base_url()}}assets/catalogo/icono-excel.png\" alt=\"{{catalogo.descripcion}}\"></a>
                                {%endif%}
                                <div class=\"portfolio-overlay\">
                                    <a href=\"{{base_url()}}assets/catalogo/{{catalogo.id}}/pdf/{{catalogo.pdf}}\" class=\"left-icon\" target=\"_blank\"><i class=\"material-icons\">remove_red_eye</i></a>
                                    <a href=\"{{base_url()}}assets/catalogo/{{catalogo.id}}/pdf/{{catalogo.pdf}}\" class=\"right-icon\"><i class=\"material-icons\">file_download</i></a>
                                </div>
                        </div>
                        <div class=\"portfolio-desc\">
                                <h3><a href=\"{{base_url()}}assets/catalogo/{{catalogo.id}}/pdf/{{catalogo.pdf}}\"><h3>{{catalogo.titulo}}</h3></a></h3>
                                <span><a href=\"#\"> {{catalogo.filtros.Filtros.0}}</a></span>
                        </div>
                </article>
                {%endfor%}
            </div>
            
            
            
            
              
        </div>
        <!--Container end-->

    </div>
</section>
", "marketing.php", "/Applications/MAMP/htdocs/Bushido2018/application/views/marketing.php");
    }
}
