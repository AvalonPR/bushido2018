<?php

/* cart/cart-resume.php */
class __TwigTemplate_5441f02c98d61018e528022f583e2d41bfbe709070744fc387582704fb59160c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- Page Title
============================================= -->
<section id=\"page-title\" class=\"carrito_header\">

    <div class=\"container clearfix\">
        <h1 style=\"color: #ffa500\">Carrito</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id=\"content\" class=\"content-cart\">
    
    <!--Process Tabs Mobile-->
    <div id=\"processTabsMobile\" class=\"hidden-lg hidden-md hidden-sm\">
        <ul class=\"process-steps bottommargin clearfix\">
            <li>
                <a id=\"carrito\"></a>
                <h5>";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "titulo", array()), "html", null, true);
        echo "</h5>
            </li>
            <li>
                <a id=\"envio\"></a>
                <h5>";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "envio", array()), "html", null, true);
        echo "</h5>
            </li>
            <li class=\"active\">
                <a id=\"pago\"></a>
                <h5>";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "pago", array()), "html", null, true);
        echo "</h5>
            </li>
            <li>
                <a id=\"pedido\"></a>
                <h5>";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "pedido", array()), "html", null, true);
        echo "</h5>
            </li>
        </ul>
    </div>
    <!--Fin Process Tabs Mobile-->

    <div class=\"contact-widget\">
        <div class=\"contact-form-result\"></div>
        <div class=\"content-wrap\">

            <div class=\"container clearfix\">
              <div id=\"processTabs\" class=\"hidden-xs\">
                <ul class=\"process-steps bottommargin clearfix\">
                    <li >
                        <a class=\"i-circled i-bordered\" id=\"carrito\"></a>
                        <h5>";
        // line 47
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "pasos", array()), "primero", array()), "html", null, true);
        echo "</h5>
                    </li>
                    <li>
                        <a class=\"i-circled i-bordered\" id=\"envio\"></a>
                        <h5>";
        // line 51
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "pasos", array()), "segundo", array()), "html", null, true);
        echo "</h5>
                    </li>
                    <li class=\"active\">
                        <a class=\"i-circled i-bordered\" id=\"pago\"></a>
                        <h5>";
        // line 55
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "pasos", array()), "tercero", array()), "html", null, true);
        echo "</h5>
                    </li>
                    <li>
                        <a class=\"i-circled i-bordered\" id=\"pedido\"></a>
                        <h5>";
        // line 59
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "pasos", array()), "cuarto", array()), "html", null, true);
        echo "</h5>
                    </li>
                </ul>
            </div>

                <h3>";
        // line 64
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "resume", array()), "titulo", array()), "html", null, true);
        echo "</h3>
                <div class=\"line-custom\"></div>
                <span class=\"col-md-12 col-xs-12\">";
        // line 66
        echo $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "resume", array()), "entradilla", array());
        echo "</span>

                <div class=\"table-responsive col-md-12 col-xs-12\">

                    <table class=\"table cart hidden-xs\">
                        <thead>
                            <tr>
                                <th class=\"cart-product-thumbnail\">&nbsp;</th>
                                <th class=\"cart-product-name\">";
        // line 74
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "resume", array()), "tabla", array()), "prod", array()), "html", null, true);
        echo "</th>
                                <th class=\"cart-product-price\">";
        // line 75
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "resume", array()), "tabla", array()), "price", array()), "html", null, true);
        echo "</th>
                                <th class=\"cart-product-quantity\">";
        // line 76
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "resume", array()), "tabla", array()), "qty", array()), "html", null, true);
        echo "</th>
                                <th class=\"cart-product-subtotal\">";
        // line 77
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "resume", array()), "tabla", array()), "total", array()), "html", null, true);
        echo "</th>
                            </tr>
                        </thead>
                        <tbody>
                            ";
        // line 81
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["cart"]) ? $context["cart"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["item"]) {
            // line 82
            echo "                            <tr class=\"cart_item\">

                                <td class=\"cart-product-thumbnail\">
                                    <a href=\"#\"><img width=\"64\" height=\"64\" src=\"";
            // line 85
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "options", array()), "img", array()), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "name", array()), "html", null, true);
            echo "\"></a>
                                </td>

                                <td class=\"cart-product-name\">
                                    <a>";
            // line 89
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "name", array()), "html", null, true);
            echo "</a>
                            ";
            // line 90
            if ( !(null === $this->getAttribute($this->getAttribute($context["item"], "options", array()), "size", array()))) {
                // line 91
                echo "                            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($context["item"], "options", array()), "size", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["talla"]) {
                    // line 92
                    echo "                            </br><span>Talla: ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["talla"], 0, array()), "html", null, true);
                    echo " x ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["talla"], 1, array()), "html", null, true);
                    echo "uds</span>
                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['talla'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 94
                echo "                            ";
            }
            // line 95
            echo "                            
                            ";
            // line 96
            if ( !(null === $this->getAttribute($this->getAttribute($context["item"], "options", array()), "color", array()))) {
                // line 97
                echo "                            </br><span>Color: ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "options", array()), "color", array()), "html", null, true);
                echo "</span>
                            ";
            }
            // line 99
            echo "                                </td>

                                <td class=\"cart-product-price\">
                                    <span class=\"amount\">";
            // line 102
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["item"], "price", array()), 0, ",", "."), "html", null, true);
            echo " Km</span>
                                </td>

                                <td class=\"cart-product-quantity\">
                                    <div class=\"quantity clearfix\">
                                        <input type=\"text\" name=\"quantity\" value=\"";
            // line 107
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "qty", array()), "html", null, true);
            echo "\" class=\"qty qty-resume\" disabled/>
                                    </div>
                                </td>

                                <td class=\"cart-product-subtotal\">
                                    <span class=\"amount\">";
            // line 112
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ($this->getAttribute($context["item"], "price", array()) * $this->getAttribute($context["item"], "qty", array())), 0, ",", "."), "html", null, true);
            echo " Km</span>
                                </td>
                            </tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 116
        echo "                        </tbody>

                    </table>
                    
                    
                    <table class=\"table cartMobile hidden-lg hidden-md hidden-sm cart_item\" width=\"300\">
                    ";
        // line 122
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["cart"]) ? $context["cart"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["item"]) {
            // line 123
            echo "                    <tbody>
                        <tr>
                            
                            <td class=\"cart-product-thumbnail\" align=\"center\"><a href=\"";
            // line 126
            echo twig_escape_filter($this->env, (isset($context["siteurl"]) ? $context["siteurl"] : null), "html", null, true);
            echo "productos/";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "options", array()), "url", array()), "html", null, true);
            echo "\"><img width=\"64\" height=\"64\" src=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "options", array()), "img", array()), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "name", array()), "html", null, true);
            echo "\"></a></td>
                            <td class=\"cart-product-name\" colspan=\"2\" align=\"left\" width=\"200\">
                                Producto:<br>
                                <a href=\"";
            // line 129
            echo twig_escape_filter($this->env, (isset($context["siteurl"]) ? $context["siteurl"] : null), "html", null, true);
            echo "productos/";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "options", array()), "url", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "name", array()), "html", null, true);
            echo "</a>
                                ";
            // line 130
            if ( !(null === $this->getAttribute($this->getAttribute($context["item"], "options", array()), "size", array()))) {
                // line 131
                echo "                                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($context["item"], "options", array()), "size", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["talla"]) {
                    // line 132
                    echo "                                </br><span>Talla: ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["talla"], 0, array()), "html", null, true);
                    echo " x ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["talla"], 1, array()), "html", null, true);
                    echo "uds</span>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['talla'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 134
                echo "                                ";
            }
            // line 135
            echo "
                                ";
            // line 136
            if ( !(null === $this->getAttribute($this->getAttribute($context["item"], "options", array()), "color", array()))) {
                // line 137
                echo "                                </br><span>Color: ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "options", array()), "color", array()), "html", null, true);
                echo "</span>
                                ";
            }
            // line 139
            echo "                            </td>
                        </tr>
                        
                        <tr>
                            <td class=\"cart-product-remove\" align=\"center\">
                                <a class=\"remove\" title=\"Eliminar artículo\" onclick=\"removedatatable('";
            // line 144
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "');\"><i class=\"icon-trash2\"></i></a>
                            </td>

                            <td align=\"left\">Precio por unidad</td>
                            <td scope=\"col\"><span class=\"amount\" align=\"center\" id=\"amount";
            // line 148
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["item"], "price", array()), 0, ",", "."), "html", null, true);
            echo " Km</span></td>
                            
                        </tr>
                        
                        <tr>
                            <td>&nbsp;</td>
                            <td align=\"left\">Cantidad:</td>
                            
                            ";
            // line 156
            if ( !(null === $this->getAttribute($this->getAttribute($context["item"], "options", array()), "size", array()))) {
                // line 157
                echo "                            
                            <td class=\"cart-product-quantity\">
                                <div class=\"quantityCart clearfix\">
                                    <input type=\"text\" name=\"quantity\" data-id=\"";
                // line 160
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "qty", array()), "html", null, true);
                echo "\" class=\"qty\" disabled=\"true\"/>
                                </div>
                            </td>
                            ";
            } else {
                // line 164
                echo "                            <td class=\"cart-product-quantity\">
                                <div class=\"quantityCart clearfix\">
                                    <!--<input type=\"button\" value=\"-\" class=\"minus\">-->
                                    <input type=\"text\" name=\"quantity\" data-id=\"";
                // line 167
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "qty", array()), "html", null, true);
                echo "\" class=\"qty\" disabled='true'/>
                                    <!--<input type=\"button\" value=\"+\" class=\"plus\">-->
                                </div>
                            </td>
                            ";
            }
            // line 172
            echo "
                            
                        </tr>
                        
                        <tr>
                            <td>&nbsp;</td>
                            <td align=\"left\">";
            // line 178
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "resume", array()), "tabla", array()), "total", array()), "html", null, true);
            echo ":</td>
                            <td class=\"cart-product-subtotal\"><span class=\"amount\" id=\"totalAmount";
            // line 179
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ($this->getAttribute($context["item"], "price", array()) * $this->getAttribute($context["item"], "qty", array())), 0, ",", "."), "html", null, true);
            echo " Km</span></td>
                        </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 182
        echo "                    </tbody>

                </table>
                    
                    

                </div>

                <div class=\"row clearfix\">
                    <form id=\"resume-form\" name=\"billing-form\" class=\"nobottommargin\" action=\"";
        // line 191
        echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
        echo "carrito/finalizarCompra\" method=\"post\">
                        <div class=\"col-md-6  col-sm-8 col-xs-12 fright\">
                            <div>
                                <h4 class=\"crillee\">";
        // line 194
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "resume", array()), "tabla", array()), "totalCart", array()), "html", null, true);
        echo "</h4>

                                <table class=\"table cart\">
                                    <tbody>
                                        <tr class=\"cart_item\">
                                            <td class=\"cart-product-name\">
                                                <strong>";
        // line 200
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "resume", array()), "tabla", array()), "subtotal", array()), "html", null, true);
        echo "</strong>
                                            </td>

                                            <td class=\"\" style=\"text-align:right;\">
                                                <span class=\"amount\">";
        // line 204
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["cart_total"]) ? $context["cart_total"] : null), 0, ",", "."), "html", null, true);
        echo " Km</span>
                                            </td>
                                        </tr>
                                        <tr class=\"cart_item\">
                                            <td class=\"cart-product-name\">
                                                <strong>";
        // line 209
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "resume", array()), "tabla", array()), "gastos", array()), "html", null, true);
        echo "</strong>
                                            </td>

                                            <td class=\"\" style=\"text-align:right;\">
                                                <span class=\"amount\">";
        // line 213
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "resume", array()), "tabla", array()), "gratis", array()), "html", null, true);
        echo "</span>
                                            </td>
                                        </tr>
                                        <tr class=\"cart_item\">
                                            <td class=\"cart-product-name\">
                                                <strong style=\"font-size: 21px;\" >";
        // line 218
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "resume", array()), "tabla", array()), "total", array()), "html", null, true);
        echo "</strong>
                                            </td>

                                            <td class=\"\" style=\"text-align: right;\">
                                                <span class=\"amount color lead\"><strong>";
        // line 222
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["cart_total"]) ? $context["cart_total"] : null), 0, ",", "."), "html", null, true);
        echo " Km</strong></span>
                                            </td>
                                        </tr>
                                    </tbody>

                                </table>

                            </div>
                        </div>
                        <div class=\"clear\"></div>

                        <div class=\"col-md-12 clearfix\">
                            <div class=\"table\">
                                <h4>";
        // line 235
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "resume", array()), "details", array()), "html", null, true);
        echo "</h4>

                                <table class=\"table cart\">
                                    <tbody>
                                        <tr class=\"cart_item\">
                                            <td class=\"cart-product-name\">
                                                <strong>";
        // line 241
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "resume", array()), "dirFact", array()), "html", null, true);
        echo ":</strong><br>
                                                <span class=\"amount\">";
        // line 242
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "billingname", array()), "html", null, true);
        echo "</span><br/>
                                                <span class=\"amount\">";
        // line 243
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "billingcompanyname", array()), "html", null, true);
        echo "</span><br/>
                                                <span class=\"amount\">";
        // line 244
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "billingaddress", array()), "html", null, true);
        echo "</span><br/>
                                                <span class=\"amount\">";
        // line 245
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "billingcity", array()), "html", null, true);
        echo "</span><br/>
                                                <span class=\"amount\">";
        // line 246
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "billingemail", array()), "html", null, true);
        echo "</span><br/>
                                                <span class=\"amount\">";
        // line 247
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "billingphone", array()), "html", null, true);
        echo "</span>
                                            </td>
                                        </tr>
                                        <tr class=\"cart_item\">
                                            <td class=\"cart-product-name\">
                                                <strong>";
        // line 252
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "resume", array()), "dirEnvio", array()), "html", null, true);
        echo ":</strong><br>
                                                <span class=\"amount\">";
        // line 253
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "shippingname", array()), "html", null, true);
        echo "</span><br/>
                                                <span class=\"amount\">";
        // line 254
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "shippingcompanyname", array()), "html", null, true);
        echo "</span><br/>
                                                <span class=\"amount\">";
        // line 255
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "shippingaddress", array()), "html", null, true);
        echo "</span><br/>
                                                <span class=\"amount\">";
        // line 256
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "shippingcity", array()), "html", null, true);
        echo "</span>
                                            </td>
                                        </tr>
                                        ";
        // line 259
        if (($this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "shippingmessage", array()) != "")) {
            // line 260
            echo "                                        <tr class=\"cart_item\">
                                            <td class=\"cart-product-name\">
                                                <strong>";
            // line 262
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "resume", array()), "obs", array()), "html", null, true);
            echo ":</strong><br>
                                                <span class=\"amount\">";
            // line 263
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datosEnvio"]) ? $context["datosEnvio"] : null), "shippingmessage", array()), "html", null, true);
            echo "</span>
                                            </td>
                                        </tr>
                                        ";
        }
        // line 267
        echo "
                                    </tbody>

                                </table>

                            </div>
                        </div>


                        <div class=\"col-md-12\">
                            <a href=\"";
        // line 277
        echo twig_escape_filter($this->env, (isset($context["siteurl"]) ? $context["siteurl"] : null), "html", null, true);
        echo "carrito/process\" class=\"fleft button button-3d button-black\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "resume", array()), "backButt", array()), "html", null, true);
        echo "</a>
                            <button type=\"submit\" class=\"button button-3d fright\">";
        // line 278
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "resume", array()), "checkButt", array()), "html", null, true);
        echo "</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</section><!-- #content end -->";
    }

    public function getTemplateName()
    {
        return "cart/cart-resume.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  555 => 278,  549 => 277,  537 => 267,  530 => 263,  526 => 262,  522 => 260,  520 => 259,  514 => 256,  510 => 255,  506 => 254,  502 => 253,  498 => 252,  490 => 247,  486 => 246,  482 => 245,  478 => 244,  474 => 243,  470 => 242,  466 => 241,  457 => 235,  441 => 222,  434 => 218,  426 => 213,  419 => 209,  411 => 204,  404 => 200,  395 => 194,  389 => 191,  378 => 182,  367 => 179,  363 => 178,  355 => 172,  345 => 167,  340 => 164,  331 => 160,  326 => 157,  324 => 156,  311 => 148,  304 => 144,  297 => 139,  291 => 137,  289 => 136,  286 => 135,  283 => 134,  272 => 132,  267 => 131,  265 => 130,  257 => 129,  245 => 126,  240 => 123,  236 => 122,  228 => 116,  218 => 112,  210 => 107,  202 => 102,  197 => 99,  191 => 97,  189 => 96,  186 => 95,  183 => 94,  172 => 92,  167 => 91,  165 => 90,  161 => 89,  152 => 85,  147 => 82,  143 => 81,  136 => 77,  132 => 76,  128 => 75,  124 => 74,  113 => 66,  108 => 64,  100 => 59,  93 => 55,  86 => 51,  79 => 47,  61 => 32,  54 => 28,  47 => 24,  40 => 20,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- Page Title
============================================= -->
<section id=\"page-title\" class=\"carrito_header\">

    <div class=\"container clearfix\">
        <h1 style=\"color: #ffa500\">Carrito</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id=\"content\" class=\"content-cart\">
    
    <!--Process Tabs Mobile-->
    <div id=\"processTabsMobile\" class=\"hidden-lg hidden-md hidden-sm\">
        <ul class=\"process-steps bottommargin clearfix\">
            <li>
                <a id=\"carrito\"></a>
                <h5>{{textos.index.titulo}}</h5>
            </li>
            <li>
                <a id=\"envio\"></a>
                <h5>{{textos.index.envio}}</h5>
            </li>
            <li class=\"active\">
                <a id=\"pago\"></a>
                <h5>{{textos.index.pago}}</h5>
            </li>
            <li>
                <a id=\"pedido\"></a>
                <h5>{{textos.index.pedido}}</h5>
            </li>
        </ul>
    </div>
    <!--Fin Process Tabs Mobile-->

    <div class=\"contact-widget\">
        <div class=\"contact-form-result\"></div>
        <div class=\"content-wrap\">

            <div class=\"container clearfix\">
              <div id=\"processTabs\" class=\"hidden-xs\">
                <ul class=\"process-steps bottommargin clearfix\">
                    <li >
                        <a class=\"i-circled i-bordered\" id=\"carrito\"></a>
                        <h5>{{textos.index.pasos.primero}}</h5>
                    </li>
                    <li>
                        <a class=\"i-circled i-bordered\" id=\"envio\"></a>
                        <h5>{{textos.index.pasos.segundo}}</h5>
                    </li>
                    <li class=\"active\">
                        <a class=\"i-circled i-bordered\" id=\"pago\"></a>
                        <h5>{{textos.index.pasos.tercero}}</h5>
                    </li>
                    <li>
                        <a class=\"i-circled i-bordered\" id=\"pedido\"></a>
                        <h5>{{textos.index.pasos.cuarto}}</h5>
                    </li>
                </ul>
            </div>

                <h3>{{textos.resume.titulo}}</h3>
                <div class=\"line-custom\"></div>
                <span class=\"col-md-12 col-xs-12\">{{textos.resume.entradilla|raw}}</span>

                <div class=\"table-responsive col-md-12 col-xs-12\">

                    <table class=\"table cart hidden-xs\">
                        <thead>
                            <tr>
                                <th class=\"cart-product-thumbnail\">&nbsp;</th>
                                <th class=\"cart-product-name\">{{textos.resume.tabla.prod}}</th>
                                <th class=\"cart-product-price\">{{textos.resume.tabla.price}}</th>
                                <th class=\"cart-product-quantity\">{{textos.resume.tabla.qty}}</th>
                                <th class=\"cart-product-subtotal\">{{textos.resume.tabla.total}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            {%for key, item in cart%}
                            <tr class=\"cart_item\">

                                <td class=\"cart-product-thumbnail\">
                                    <a href=\"#\"><img width=\"64\" height=\"64\" src=\"{{item.options.img}}\" alt=\"{{item.name}}\"></a>
                                </td>

                                <td class=\"cart-product-name\">
                                    <a>{{item.name}}</a>
                            {% if item.options.size is not null%}
                            {% for talla in item.options.size%}
                            </br><span>Talla: {{talla.0}} x {{talla.1}}uds</span>
                            {%endfor%}
                            {%endif%}
                            
                            {% if item.options.color is not null%}
                            </br><span>Color: {{item.options.color}}</span>
                            {%endif%}
                                </td>

                                <td class=\"cart-product-price\">
                                    <span class=\"amount\">{{item.price|number_format(0, ',', '.')}} Km</span>
                                </td>

                                <td class=\"cart-product-quantity\">
                                    <div class=\"quantity clearfix\">
                                        <input type=\"text\" name=\"quantity\" value=\"{{item.qty}}\" class=\"qty qty-resume\" disabled/>
                                    </div>
                                </td>

                                <td class=\"cart-product-subtotal\">
                                    <span class=\"amount\">{{(item.price*item.qty)|number_format(0, ',', '.')}} Km</span>
                                </td>
                            </tr>
                            {%endfor%}
                        </tbody>

                    </table>
                    
                    
                    <table class=\"table cartMobile hidden-lg hidden-md hidden-sm cart_item\" width=\"300\">
                    {%for key, item in cart%}
                    <tbody>
                        <tr>
                            
                            <td class=\"cart-product-thumbnail\" align=\"center\"><a href=\"{{siteurl}}productos/{{item.options.url}}\"><img width=\"64\" height=\"64\" src=\"{{item.options.img}}\" alt=\"{{item.name}}\"></a></td>
                            <td class=\"cart-product-name\" colspan=\"2\" align=\"left\" width=\"200\">
                                Producto:<br>
                                <a href=\"{{siteurl}}productos/{{item.options.url}}\">{{item.name}}</a>
                                {% if item.options.size is not null%}
                                {% for talla in item.options.size%}
                                </br><span>Talla: {{talla.0}} x {{talla.1}}uds</span>
                                {%endfor%}
                                {%endif%}

                                {% if item.options.color is not null%}
                                </br><span>Color: {{item.options.color}}</span>
                                {%endif%}
                            </td>
                        </tr>
                        
                        <tr>
                            <td class=\"cart-product-remove\" align=\"center\">
                                <a class=\"remove\" title=\"Eliminar artículo\" onclick=\"removedatatable('{{key}}');\"><i class=\"icon-trash2\"></i></a>
                            </td>

                            <td align=\"left\">Precio por unidad</td>
                            <td scope=\"col\"><span class=\"amount\" align=\"center\" id=\"amount{{key}}\">{{item.price|number_format(0, ',', '.')}} Km</span></td>
                            
                        </tr>
                        
                        <tr>
                            <td>&nbsp;</td>
                            <td align=\"left\">Cantidad:</td>
                            
                            {% if item.options.size is not null%}
                            
                            <td class=\"cart-product-quantity\">
                                <div class=\"quantityCart clearfix\">
                                    <input type=\"text\" name=\"quantity\" data-id=\"{{key}}\" value=\"{{item.qty}}\" class=\"qty\" disabled=\"true\"/>
                                </div>
                            </td>
                            {%else%}
                            <td class=\"cart-product-quantity\">
                                <div class=\"quantityCart clearfix\">
                                    <!--<input type=\"button\" value=\"-\" class=\"minus\">-->
                                    <input type=\"text\" name=\"quantity\" data-id=\"{{key}}\" value=\"{{item.qty}}\" class=\"qty\" disabled='true'/>
                                    <!--<input type=\"button\" value=\"+\" class=\"plus\">-->
                                </div>
                            </td>
                            {%endif%}

                            
                        </tr>
                        
                        <tr>
                            <td>&nbsp;</td>
                            <td align=\"left\">{{textos.resume.tabla.total}}:</td>
                            <td class=\"cart-product-subtotal\"><span class=\"amount\" id=\"totalAmount{{key}}\">{{(item.price*item.qty)|number_format(0, ',', '.')}} Km</span></td>
                        </tr>
                        {%endfor%}
                    </tbody>

                </table>
                    
                    

                </div>

                <div class=\"row clearfix\">
                    <form id=\"resume-form\" name=\"billing-form\" class=\"nobottommargin\" action=\"{{baseurl}}carrito/finalizarCompra\" method=\"post\">
                        <div class=\"col-md-6  col-sm-8 col-xs-12 fright\">
                            <div>
                                <h4 class=\"crillee\">{{textos.resume.tabla.totalCart}}</h4>

                                <table class=\"table cart\">
                                    <tbody>
                                        <tr class=\"cart_item\">
                                            <td class=\"cart-product-name\">
                                                <strong>{{textos.resume.tabla.subtotal}}</strong>
                                            </td>

                                            <td class=\"\" style=\"text-align:right;\">
                                                <span class=\"amount\">{{cart_total|number_format(0, ',', '.')}} Km</span>
                                            </td>
                                        </tr>
                                        <tr class=\"cart_item\">
                                            <td class=\"cart-product-name\">
                                                <strong>{{textos.resume.tabla.gastos}}</strong>
                                            </td>

                                            <td class=\"\" style=\"text-align:right;\">
                                                <span class=\"amount\">{{textos.resume.tabla.gratis}}</span>
                                            </td>
                                        </tr>
                                        <tr class=\"cart_item\">
                                            <td class=\"cart-product-name\">
                                                <strong style=\"font-size: 21px;\" >{{textos.resume.tabla.total}}</strong>
                                            </td>

                                            <td class=\"\" style=\"text-align: right;\">
                                                <span class=\"amount color lead\"><strong>{{cart_total|number_format(0, ',', '.')}} Km</strong></span>
                                            </td>
                                        </tr>
                                    </tbody>

                                </table>

                            </div>
                        </div>
                        <div class=\"clear\"></div>

                        <div class=\"col-md-12 clearfix\">
                            <div class=\"table\">
                                <h4>{{textos.resume.details}}</h4>

                                <table class=\"table cart\">
                                    <tbody>
                                        <tr class=\"cart_item\">
                                            <td class=\"cart-product-name\">
                                                <strong>{{textos.resume.dirFact}}:</strong><br>
                                                <span class=\"amount\">{{datosEnvio.billingname}}</span><br/>
                                                <span class=\"amount\">{{datosEnvio.billingcompanyname}}</span><br/>
                                                <span class=\"amount\">{{datosEnvio.billingaddress}}</span><br/>
                                                <span class=\"amount\">{{datosEnvio.billingcity}}</span><br/>
                                                <span class=\"amount\">{{datosEnvio.billingemail}}</span><br/>
                                                <span class=\"amount\">{{datosEnvio.billingphone}}</span>
                                            </td>
                                        </tr>
                                        <tr class=\"cart_item\">
                                            <td class=\"cart-product-name\">
                                                <strong>{{textos.resume.dirEnvio}}:</strong><br>
                                                <span class=\"amount\">{{datosEnvio.shippingname}}</span><br/>
                                                <span class=\"amount\">{{datosEnvio.shippingcompanyname}}</span><br/>
                                                <span class=\"amount\">{{datosEnvio.shippingaddress}}</span><br/>
                                                <span class=\"amount\">{{datosEnvio.shippingcity}}</span>
                                            </td>
                                        </tr>
                                        {%if datosEnvio.shippingmessage !=''%}
                                        <tr class=\"cart_item\">
                                            <td class=\"cart-product-name\">
                                                <strong>{{textos.resume.obs}}:</strong><br>
                                                <span class=\"amount\">{{datosEnvio.shippingmessage}}</span>
                                            </td>
                                        </tr>
                                        {%endif%}

                                    </tbody>

                                </table>

                            </div>
                        </div>


                        <div class=\"col-md-12\">
                            <a href=\"{{siteurl}}carrito/process\" class=\"fleft button button-3d button-black\">{{textos.resume.backButt}}</a>
                            <button type=\"submit\" class=\"button button-3d fright\">{{textos.resume.checkButt}}</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</section><!-- #content end -->", "cart/cart-resume.php", "/Applications/MAMP/htdocs/Bushido2018/application/views/cart/cart-resume.php");
    }
}
