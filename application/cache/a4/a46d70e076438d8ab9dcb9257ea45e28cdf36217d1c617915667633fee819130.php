<?php

/* cart/cart.php */
class __TwigTemplate_a825e20598e6c55aa98101f414063cc015da424ed999235ad184c98dee2ccc66 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- Page Title
============================================= -->
<section id=\"page-title\" class=\"carrito_header\">

    <div class=\"container clearfix\">
        <h1>";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "titulo", array()), "html", null, true);
        echo "</h1>
        <span style=\"color: black\"></span>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id=\"content\" class=\"content-cart\">
    
    <!--Process Tabs Mobile-->
    <div id=\"processTabsMobile\" class=\"hidden-lg hidden-md hidden-sm\">
        <ul class=\"process-steps bottommargin clearfix\">
            <li class=\"active\">
                <a id=\"carrito\"></a>
                <h5>";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "titulo", array()), "html", null, true);
        echo "</h5>
            </li>
            <li>
                <a id=\"envio\"></a>
                <h5>";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "envio", array()), "html", null, true);
        echo "</h5>
            </li>
            <li>
                <a id=\"pago\"></a>
                <h5>";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "pago", array()), "html", null, true);
        echo "</h5>
            </li>
            <li>
                <a id=\"pedido\"></a>
                <h5>";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "pedido", array()), "html", null, true);
        echo "</h5>
            </li>
        </ul>
    </div>
    <!--Fin Process Tabs Mobile-->
    
    <div class=\"content-wrap\">
        
        
        
        <div class=\"container clearfix\"  id=\"cart-data-context\">
            <div id=\"processTabs\" class=\"hidden-xs\">
                <ul class=\"process-steps bottommargin clearfix\">
                    <li class=\"active\">
                        <a class=\"i-circled i-bordered\" id=\"carrito\"></a>
                        <h5>";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "pasos", array()), "primero", array()), "html", null, true);
        echo "</h5>
                    </li>
                    <li>
                        <a class=\"i-circled i-bordered\" id=\"envio\"></a>
                        <h5>";
        // line 52
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "pasos", array()), "segundo", array()), "html", null, true);
        echo "</h5>
                    </li>
                    <li>
                        <a class=\"i-circled i-bordered\" id=\"pago\"></a>
                        <h5>";
        // line 56
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "pasos", array()), "tercero", array()), "html", null, true);
        echo "</h5>
                    </li>
                    <li>
                        <a class=\"i-circled i-bordered\" id=\"pedido\"></a>
                        <h5>";
        // line 60
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "pasos", array()), "cuarto", array()), "html", null, true);
        echo "</h5>
                    </li>
                </ul>
            </div>

            <h3>Carrito</h3>
            <div class=\"line-custom\"></div>

            <div class=\"table-responsive nobottommargin\">
                ";
        // line 69
        if (twig_test_empty((isset($context["cart"]) ? $context["cart"] : null))) {
            // line 70
            echo "                <div class=\"col-md-5 advice divcenter bottommargin-sm\">
                    <p class=\"center no_items\"> <i class=\"center material-icons\">notification_important</i><br>";
            // line 71
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "empty", array()), "html", null, true);
            echo "</p>
                </div>

                ";
        } else {
            // line 75
            echo "                
                <!-- INICIO TABLA desktop -->
                <table class=\"table cart hidden-xs\">
                    <thead>
                        <tr>
                            <th class=\"cart-product-remove\">&nbsp;</th>
                            <th class=\"cart-product-thumbnail\">&nbsp;</th>
                            <th class=\"cart-product-name\">";
            // line 82
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "tabla", array()), "prod", array()), "html", null, true);
            echo "</th>
                            <th class=\"cart-product-price\">";
            // line 83
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "tabla", array()), "price", array()), "html", null, true);
            echo "</th>
                            <th class=\"cart-product-quantity\">";
            // line 84
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "tabla", array()), "qty", array()), "html", null, true);
            echo "</th>
                            <th class=\"cart-product-subtotal\">";
            // line 85
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "tabla", array()), "total", array()), "html", null, true);
            echo "</th>
                        </tr>
                    </thead>
                    <tbody>
                        ";
            // line 89
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["cart"]) ? $context["cart"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                // line 90
                echo "                        <tr class=\"cart_item\">
                            <td class=\"cart-product-remove\">
                                <a class=\"remove\" title=\"Eliminar artículo\" onclick=\"removedatatable('";
                // line 92
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "');\"><i class=\"icon-trash2\"></i></a>
                            </td>

                            <td class=\"cart-product-thumbnail\">
                                <a href=\"";
                // line 96
                echo twig_escape_filter($this->env, (isset($context["siteurl"]) ? $context["siteurl"] : null), "html", null, true);
                echo "productos/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "options", array()), "url", array()), "html", null, true);
                echo "\"><img width=\"64\" height=\"64\" src=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "options", array()), "img", array()), "html", null, true);
                echo "\" alt=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "name", array()), "html", null, true);
                echo "\"></a>
                            </td>

                            <td class=\"cart-product-name\">
                                <a href=\"";
                // line 100
                echo twig_escape_filter($this->env, (isset($context["siteurl"]) ? $context["siteurl"] : null), "html", null, true);
                echo "productos/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "options", array()), "url", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "name", array()), "html", null, true);
                echo "</a>
                            ";
                // line 101
                if ( !(null === $this->getAttribute($this->getAttribute($context["item"], "options", array()), "size", array()))) {
                    // line 102
                    echo "                            ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($context["item"], "options", array()), "size", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["talla"]) {
                        // line 103
                        echo "                            </br><span>Talla: ";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["talla"], 0, array()), "html", null, true);
                        echo " x ";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["talla"], 1, array()), "html", null, true);
                        echo "uds</span>
                            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['talla'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 105
                    echo "                            ";
                }
                // line 106
                echo "                            
                            ";
                // line 107
                if ( !(null === $this->getAttribute($this->getAttribute($context["item"], "options", array()), "color", array()))) {
                    // line 108
                    echo "                            </br><span>Color: ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "options", array()), "color", array()), "html", null, true);
                    echo "</span>
                            ";
                }
                // line 110
                echo "                            </td>

                            <td class=\"cart-product-price\">
                                <span class=\"amount\"  id=\"amount";
                // line 113
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["item"], "price", array()), 0, ",", "."), "html", null, true);
                echo " Km</span>
                            </td>
                            ";
                // line 115
                if ( !(null === $this->getAttribute($this->getAttribute($context["item"], "options", array()), "size", array()))) {
                    // line 116
                    echo "                            
                            <td class=\"cart-product-quantity\">
                                <div class=\"quantityCart clearfix\">
                                    <input type=\"text\" name=\"quantity\" data-id=\"";
                    // line 119
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo "\" value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "qty", array()), "html", null, true);
                    echo "\" class=\"qty\" disabled=\"true\"/>
                                </div>
                            </td>
                            ";
                } else {
                    // line 123
                    echo "                            <td class=\"cart-product-quantity\">
                                <div class=\"quantityCart clearfix\">
                                    <!--<input type=\"button\" value=\"-\" class=\"minus\">-->
                                    <input type=\"text\" name=\"quantity\" data-id=\"";
                    // line 126
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo "\" value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "qty", array()), "html", null, true);
                    echo "\" class=\"qty\" disabled='true'/>
                                    <!--<input type=\"button\" value=\"+\" class=\"plus\">-->
                                </div>
                            </td>
                            ";
                }
                // line 131
                echo "
                            <td class=\"cart-product-subtotal\">
                                <span class=\"amount\" id=\"totalAmount";
                // line 133
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ($this->getAttribute($context["item"], "price", array()) * $this->getAttribute($context["item"], "qty", array())), 0, ",", "."), "html", null, true);
                echo " Km</span>
                            </td>
                        </tr>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 137
            echo "                    </tbody>

                </table>
                <!-- FIN TABLA desktop -->
                
                <!-- INICIO TABLA MOBILE -->
                
                <table class=\"table cartMobile hidden-lg hidden-md hidden-sm cart_item\" width=\"300\">
                    ";
            // line 145
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["cart"]) ? $context["cart"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                // line 146
                echo "                    <tbody>
                        <tr>
                            
                            <td class=\"cart-product-thumbnail\" align=\"center\"><a href=\"";
                // line 149
                echo twig_escape_filter($this->env, (isset($context["siteurl"]) ? $context["siteurl"] : null), "html", null, true);
                echo "productos/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "options", array()), "url", array()), "html", null, true);
                echo "\"><img width=\"64\" height=\"64\" src=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "options", array()), "img", array()), "html", null, true);
                echo "\" alt=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "name", array()), "html", null, true);
                echo "\"></a></td>
                            <td class=\"cart-product-name\" colspan=\"2\" align=\"left\" width=\"200\">
                                Producto:<br>
                                <a href=\"";
                // line 152
                echo twig_escape_filter($this->env, (isset($context["siteurl"]) ? $context["siteurl"] : null), "html", null, true);
                echo "productos/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "options", array()), "url", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "name", array()), "html", null, true);
                echo "</a>
                                ";
                // line 153
                if ( !(null === $this->getAttribute($this->getAttribute($context["item"], "options", array()), "size", array()))) {
                    // line 154
                    echo "                                ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($context["item"], "options", array()), "size", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["talla"]) {
                        // line 155
                        echo "                                </br><span>Talla: ";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["talla"], 0, array()), "html", null, true);
                        echo " x ";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["talla"], 1, array()), "html", null, true);
                        echo "uds</span>
                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['talla'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 157
                    echo "                                ";
                }
                // line 158
                echo "
                                ";
                // line 159
                if ( !(null === $this->getAttribute($this->getAttribute($context["item"], "options", array()), "color", array()))) {
                    // line 160
                    echo "                                </br><span>Color: ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "options", array()), "color", array()), "html", null, true);
                    echo "</span>
                                ";
                }
                // line 162
                echo "                            </td>
                        </tr>
                        
                        <tr>
                            <td class=\"cart-product-remove\" align=\"center\">
                                <a class=\"remove\" title=\"Eliminar artículo\" onclick=\"removedatatable('";
                // line 167
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "');\"><i class=\"icon-trash2\"></i></a>
                            </td>

                            <td align=\"left\">Precio por unidad</td>
                            <td scope=\"col\"><span class=\"amount\" align=\"center\" id=\"amount";
                // line 171
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["item"], "price", array()), 0, ",", "."), "html", null, true);
                echo " Km</span></td>
                            
                        </tr>
                        
                        <tr>
                            <td>&nbsp;</td>
                            <td align=\"left\">Cantidad:</td>
                            
                            ";
                // line 179
                if ( !(null === $this->getAttribute($this->getAttribute($context["item"], "options", array()), "size", array()))) {
                    // line 180
                    echo "                            
                            <td class=\"cart-product-quantity\">
                                <div class=\"quantityCart clearfix\">
                                    <input type=\"text\" name=\"quantity\" data-id=\"";
                    // line 183
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo "\" value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "qty", array()), "html", null, true);
                    echo "\" class=\"qty\" disabled=\"true\"/>
                                </div>
                            </td>
                            ";
                } else {
                    // line 187
                    echo "                            <td class=\"cart-product-quantity\">
                                <div class=\"quantityCart clearfix\">
                                    <!--<input type=\"button\" value=\"-\" class=\"minus\">-->
                                    <input type=\"text\" name=\"quantity\" data-id=\"";
                    // line 190
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo "\" value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "qty", array()), "html", null, true);
                    echo "\" class=\"qty\" disabled='true'/>
                                    <!--<input type=\"button\" value=\"+\" class=\"plus\">-->
                                </div>
                            </td>
                            ";
                }
                // line 195
                echo "
                            
                        </tr>
                        
                        <tr>
                            <td>&nbsp;</td>
                            <td align=\"left\">Total:</td>
                            <td class=\"cart-product-subtotal\"><span class=\"amount\" id=\"totalAmount";
                // line 202
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ($this->getAttribute($context["item"], "price", array()) * $this->getAttribute($context["item"], "qty", array())), 0, ",", "."), "html", null, true);
                echo " Km</span></td>
                        </tr>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 205
            echo "                    </tbody>

                </table>
                
                <!-- FIN TABLA MOBILE -->

            </div>


            <div class=\"row clearfix\">

                <div class=\"col-md-6  col-sm-8 col-xs-12 clearfix fright\">
                    <div class=\"table-responsive\">
                        <h4>";
            // line 218
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "tabla", array()), "totalCart", array()), "html", null, true);
            echo "</h4>

                        <table class=\"table cart\">
                            <tbody>
                                <tr class=\"cart_item\">
                                    <td class=\"cart-product-name\">
                                        <strong>";
            // line 224
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "tabla", array()), "subtotal", array()), "html", null, true);
            echo "</strong>
                                    </td>

                                    <td style=\"text-align: right;\">
                                        <span class=\"amount\">";
            // line 228
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["cart_total"]) ? $context["cart_total"] : null), 0, ",", "."), "html", null, true);
            echo " Km</span>
                                    </td>
                                </tr>
                                <tr class=\"cart_item\">
                                    <td class=\"cart-product-name\">
                                        <strong>";
            // line 233
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "tabla", array()), "gastos", array()), "html", null, true);
            echo "</strong>
                                    </td>

                                    <td style=\"text-align: right;\">
                                        <span class=\"amount\">";
            // line 237
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "tabla", array()), "gratis", array()), "html", null, true);
            echo "</span>
                                    </td>
                                </tr>
                                <tr class=\"cart_item\">
                                    <td class=\"cart-product-name\">
                                        <strong style=\"font-size: 21px;\">";
            // line 242
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "tabla", array()), "total", array()), "html", null, true);
            echo "</strong>
                                    </td>

                                    <td style=\"text-align: right;\">
                                        <span class=\"amount color lead\"><strong>";
            // line 246
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["cart_total"]) ? $context["cart_total"] : null), 0, ",", "."), "html", null, true);
            echo " Km</strong></span>
                                    </td>
                                </tr>
                            </tbody>

                        </table>

                    </div>
                </div>
            </div>
            ";
        }
        // line 257
        echo "            <div class=\"clearfix tramit_step1\">
                
                    <a href=\"";
        // line 259
        echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
        echo "carrito/process\" class=\"button button-3d  fright ";
        if (twig_test_empty((isset($context["cart"]) ? $context["cart"] : null))) {
            echo "button-black no_item_disabled";
        }
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "checkButt", array()), "html", null, true);
        echo "</a>
                    <a href=\"";
        // line 260
        echo twig_escape_filter($this->env, (isset($context["baseurl"]) ? $context["baseurl"] : null), "html", null, true);
        echo "productos\" class=\"button button-3d button-black  fleft\"><img src=\"";
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "volver-carrito.png\" class=\"hidden-lg hidden-md hidden-sm\"/><span class=\"hidden-xs\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["textos"]) ? $context["textos"] : null), "index", array()), "backButt", array()), "html", null, true);
        echo "</span></a>
                
            </div>

        </div>
    </div>


</section><!-- #content end -->
";
    }

    public function getTemplateName()
    {
        return "cart/cart.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  521 => 260,  511 => 259,  507 => 257,  493 => 246,  486 => 242,  478 => 237,  471 => 233,  463 => 228,  456 => 224,  447 => 218,  432 => 205,  421 => 202,  412 => 195,  402 => 190,  397 => 187,  388 => 183,  383 => 180,  381 => 179,  368 => 171,  361 => 167,  354 => 162,  348 => 160,  346 => 159,  343 => 158,  340 => 157,  329 => 155,  324 => 154,  322 => 153,  314 => 152,  302 => 149,  297 => 146,  293 => 145,  283 => 137,  271 => 133,  267 => 131,  257 => 126,  252 => 123,  243 => 119,  238 => 116,  236 => 115,  229 => 113,  224 => 110,  218 => 108,  216 => 107,  213 => 106,  210 => 105,  199 => 103,  194 => 102,  192 => 101,  184 => 100,  171 => 96,  164 => 92,  160 => 90,  156 => 89,  149 => 85,  145 => 84,  141 => 83,  137 => 82,  128 => 75,  121 => 71,  118 => 70,  116 => 69,  104 => 60,  97 => 56,  90 => 52,  83 => 48,  65 => 33,  58 => 29,  51 => 25,  44 => 21,  26 => 6,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- Page Title
============================================= -->
<section id=\"page-title\" class=\"carrito_header\">

    <div class=\"container clearfix\">
        <h1>{{textos.index.titulo}}</h1>
        <span style=\"color: black\"></span>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id=\"content\" class=\"content-cart\">
    
    <!--Process Tabs Mobile-->
    <div id=\"processTabsMobile\" class=\"hidden-lg hidden-md hidden-sm\">
        <ul class=\"process-steps bottommargin clearfix\">
            <li class=\"active\">
                <a id=\"carrito\"></a>
                <h5>{{textos.index.titulo}}</h5>
            </li>
            <li>
                <a id=\"envio\"></a>
                <h5>{{textos.index.envio}}</h5>
            </li>
            <li>
                <a id=\"pago\"></a>
                <h5>{{textos.index.pago}}</h5>
            </li>
            <li>
                <a id=\"pedido\"></a>
                <h5>{{textos.index.pedido}}</h5>
            </li>
        </ul>
    </div>
    <!--Fin Process Tabs Mobile-->
    
    <div class=\"content-wrap\">
        
        
        
        <div class=\"container clearfix\"  id=\"cart-data-context\">
            <div id=\"processTabs\" class=\"hidden-xs\">
                <ul class=\"process-steps bottommargin clearfix\">
                    <li class=\"active\">
                        <a class=\"i-circled i-bordered\" id=\"carrito\"></a>
                        <h5>{{textos.index.pasos.primero}}</h5>
                    </li>
                    <li>
                        <a class=\"i-circled i-bordered\" id=\"envio\"></a>
                        <h5>{{textos.index.pasos.segundo}}</h5>
                    </li>
                    <li>
                        <a class=\"i-circled i-bordered\" id=\"pago\"></a>
                        <h5>{{textos.index.pasos.tercero}}</h5>
                    </li>
                    <li>
                        <a class=\"i-circled i-bordered\" id=\"pedido\"></a>
                        <h5>{{textos.index.pasos.cuarto}}</h5>
                    </li>
                </ul>
            </div>

            <h3>Carrito</h3>
            <div class=\"line-custom\"></div>

            <div class=\"table-responsive nobottommargin\">
                {%if cart is empty%}
                <div class=\"col-md-5 advice divcenter bottommargin-sm\">
                    <p class=\"center no_items\"> <i class=\"center material-icons\">notification_important</i><br>{{textos.index.empty}}</p>
                </div>

                {%else%}
                
                <!-- INICIO TABLA desktop -->
                <table class=\"table cart hidden-xs\">
                    <thead>
                        <tr>
                            <th class=\"cart-product-remove\">&nbsp;</th>
                            <th class=\"cart-product-thumbnail\">&nbsp;</th>
                            <th class=\"cart-product-name\">{{textos.index.tabla.prod}}</th>
                            <th class=\"cart-product-price\">{{textos.index.tabla.price}}</th>
                            <th class=\"cart-product-quantity\">{{textos.index.tabla.qty}}</th>
                            <th class=\"cart-product-subtotal\">{{textos.index.tabla.total}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {%for key, item in cart%}
                        <tr class=\"cart_item\">
                            <td class=\"cart-product-remove\">
                                <a class=\"remove\" title=\"Eliminar artículo\" onclick=\"removedatatable('{{key}}');\"><i class=\"icon-trash2\"></i></a>
                            </td>

                            <td class=\"cart-product-thumbnail\">
                                <a href=\"{{siteurl}}productos/{{item.options.url}}\"><img width=\"64\" height=\"64\" src=\"{{item.options.img}}\" alt=\"{{item.name}}\"></a>
                            </td>

                            <td class=\"cart-product-name\">
                                <a href=\"{{siteurl}}productos/{{item.options.url}}\">{{item.name}}</a>
                            {% if item.options.size is not null%}
                            {% for talla in item.options.size%}
                            </br><span>Talla: {{talla.0}} x {{talla.1}}uds</span>
                            {%endfor%}
                            {%endif%}
                            
                            {% if item.options.color is not null%}
                            </br><span>Color: {{item.options.color}}</span>
                            {%endif%}
                            </td>

                            <td class=\"cart-product-price\">
                                <span class=\"amount\"  id=\"amount{{key}}\">{{item.price|number_format(0, ',', '.')}} Km</span>
                            </td>
                            {% if item.options.size is not null%}
                            
                            <td class=\"cart-product-quantity\">
                                <div class=\"quantityCart clearfix\">
                                    <input type=\"text\" name=\"quantity\" data-id=\"{{key}}\" value=\"{{item.qty}}\" class=\"qty\" disabled=\"true\"/>
                                </div>
                            </td>
                            {%else%}
                            <td class=\"cart-product-quantity\">
                                <div class=\"quantityCart clearfix\">
                                    <!--<input type=\"button\" value=\"-\" class=\"minus\">-->
                                    <input type=\"text\" name=\"quantity\" data-id=\"{{key}}\" value=\"{{item.qty}}\" class=\"qty\" disabled='true'/>
                                    <!--<input type=\"button\" value=\"+\" class=\"plus\">-->
                                </div>
                            </td>
                            {%endif%}

                            <td class=\"cart-product-subtotal\">
                                <span class=\"amount\" id=\"totalAmount{{key}}\">{{(item.price*item.qty)|number_format(0, ',', '.')}} Km</span>
                            </td>
                        </tr>
                        {%endfor%}
                    </tbody>

                </table>
                <!-- FIN TABLA desktop -->
                
                <!-- INICIO TABLA MOBILE -->
                
                <table class=\"table cartMobile hidden-lg hidden-md hidden-sm cart_item\" width=\"300\">
                    {%for key, item in cart%}
                    <tbody>
                        <tr>
                            
                            <td class=\"cart-product-thumbnail\" align=\"center\"><a href=\"{{siteurl}}productos/{{item.options.url}}\"><img width=\"64\" height=\"64\" src=\"{{item.options.img}}\" alt=\"{{item.name}}\"></a></td>
                            <td class=\"cart-product-name\" colspan=\"2\" align=\"left\" width=\"200\">
                                Producto:<br>
                                <a href=\"{{siteurl}}productos/{{item.options.url}}\">{{item.name}}</a>
                                {% if item.options.size is not null%}
                                {% for talla in item.options.size%}
                                </br><span>Talla: {{talla.0}} x {{talla.1}}uds</span>
                                {%endfor%}
                                {%endif%}

                                {% if item.options.color is not null%}
                                </br><span>Color: {{item.options.color}}</span>
                                {%endif%}
                            </td>
                        </tr>
                        
                        <tr>
                            <td class=\"cart-product-remove\" align=\"center\">
                                <a class=\"remove\" title=\"Eliminar artículo\" onclick=\"removedatatable('{{key}}');\"><i class=\"icon-trash2\"></i></a>
                            </td>

                            <td align=\"left\">Precio por unidad</td>
                            <td scope=\"col\"><span class=\"amount\" align=\"center\" id=\"amount{{key}}\">{{item.price|number_format(0, ',', '.')}} Km</span></td>
                            
                        </tr>
                        
                        <tr>
                            <td>&nbsp;</td>
                            <td align=\"left\">Cantidad:</td>
                            
                            {% if item.options.size is not null%}
                            
                            <td class=\"cart-product-quantity\">
                                <div class=\"quantityCart clearfix\">
                                    <input type=\"text\" name=\"quantity\" data-id=\"{{key}}\" value=\"{{item.qty}}\" class=\"qty\" disabled=\"true\"/>
                                </div>
                            </td>
                            {%else%}
                            <td class=\"cart-product-quantity\">
                                <div class=\"quantityCart clearfix\">
                                    <!--<input type=\"button\" value=\"-\" class=\"minus\">-->
                                    <input type=\"text\" name=\"quantity\" data-id=\"{{key}}\" value=\"{{item.qty}}\" class=\"qty\" disabled='true'/>
                                    <!--<input type=\"button\" value=\"+\" class=\"plus\">-->
                                </div>
                            </td>
                            {%endif%}

                            
                        </tr>
                        
                        <tr>
                            <td>&nbsp;</td>
                            <td align=\"left\">Total:</td>
                            <td class=\"cart-product-subtotal\"><span class=\"amount\" id=\"totalAmount{{key}}\">{{(item.price*item.qty)|number_format(0, ',', '.')}} Km</span></td>
                        </tr>
                        {%endfor%}
                    </tbody>

                </table>
                
                <!-- FIN TABLA MOBILE -->

            </div>


            <div class=\"row clearfix\">

                <div class=\"col-md-6  col-sm-8 col-xs-12 clearfix fright\">
                    <div class=\"table-responsive\">
                        <h4>{{textos.index.tabla.totalCart}}</h4>

                        <table class=\"table cart\">
                            <tbody>
                                <tr class=\"cart_item\">
                                    <td class=\"cart-product-name\">
                                        <strong>{{textos.index.tabla.subtotal}}</strong>
                                    </td>

                                    <td style=\"text-align: right;\">
                                        <span class=\"amount\">{{cart_total|number_format(0, ',', '.')}} Km</span>
                                    </td>
                                </tr>
                                <tr class=\"cart_item\">
                                    <td class=\"cart-product-name\">
                                        <strong>{{textos.index.tabla.gastos}}</strong>
                                    </td>

                                    <td style=\"text-align: right;\">
                                        <span class=\"amount\">{{textos.index.tabla.gratis}}</span>
                                    </td>
                                </tr>
                                <tr class=\"cart_item\">
                                    <td class=\"cart-product-name\">
                                        <strong style=\"font-size: 21px;\">{{textos.index.tabla.total}}</strong>
                                    </td>

                                    <td style=\"text-align: right;\">
                                        <span class=\"amount color lead\"><strong>{{cart_total|number_format(0, ',', '.')}} Km</strong></span>
                                    </td>
                                </tr>
                            </tbody>

                        </table>

                    </div>
                </div>
            </div>
            {%endif%}
            <div class=\"clearfix tramit_step1\">
                
                    <a href=\"{{baseurl}}carrito/process\" class=\"button button-3d  fright {%if cart is empty%}button-black no_item_disabled{%endif%}\">{{textos.index.checkButt}}</a>
                    <a href=\"{{baseurl}}productos\" class=\"button button-3d button-black  fleft\"><img src=\"{{imgurl}}volver-carrito.png\" class=\"hidden-lg hidden-md hidden-sm\"/><span class=\"hidden-xs\">{{textos.index.backButt}}</span></a>
                
            </div>

        </div>
    </div>


</section><!-- #content end -->
", "cart/cart.php", "/Applications/MAMP/htdocs/Bushido2018/application/views/cart/cart.php");
    }
}
