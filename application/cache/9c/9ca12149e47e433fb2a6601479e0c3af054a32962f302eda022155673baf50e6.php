<?php

/* taller/trayectoria.php */
class __TwigTemplate_b3483438a05cff273573411874dfb5f4fd737db5309040962a45a7007d1c1d6b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<!-- Page Title
        ============================================= -->
<section id=\"page-title\" class=\"header trayectoria_header\">

    <div class=\"container clearfix\">
        <h1>Trayectoria</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
        ============================================= -->
<section id=\"content\">

    <div class=\"content-wrap\">

        <div class=\"container clearfix\">
            <h3 style=\"margin: 0;\" >Trayectoria</h3>
            <div style=\"margin: 1% 0 4% 0;\" class=\"line-custom\"></div>
            <div class=\"trayectoria\">
                <div class=\"container clearfix bg-trayectoria nobottommargin\">
                    
                    <div id=\"justicia\" class=\"steps\">
                        <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Justicia\">Gi</span>
                        <span class=\"km\">820 km</span>
                        <img src=\"";
        // line 27
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "trayectoria/steps-trayectoria-01.png\" alt=\"Gi, Justicia\">
                    </div>
                    <div id=\"valor\" class=\"steps\">
                        <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Valor\">Yu</span>
                        <span class=\"km\">1.640 km</span>
                        <img src=\"";
        // line 32
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "trayectoria/steps-trayectoria-02.png\" alt=\"Yu, Valor\">
                    </div>
                    <div id=\"compasion\" class=\"steps\">
                        <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Compasión\">Jin</span>
                        <span class=\"km\">2.460 km</span>
                        <img src=\"";
        // line 37
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "trayectoria/steps-trayectoria-03.png\" alt=\"Gi, Justicia\">
                    </div>
                    <div id=\"cortesia\" class=\"steps\">
                        <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Cortesía\">Rei</span>
                        <span class=\"km\">3.280 km</span>
                        <img src=\"";
        // line 42
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "trayectoria/steps-trayectoria-04.png\" alt=\"Gi, Justicia\">
                    </div>
                    <div id=\"sinceridad\" class=\"steps\">
                        <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Sinceridad\">Makoto</span>
                        <span class=\"km\">4.100 km</span>
                        <img src=\"";
        // line 47
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "trayectoria/steps-trayectoria-05.png\" alt=\"Gi, Justicia\">
                    </div>
                    <div id=\"honor\" class=\"steps\">
                        <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Honor\">Meyo</span>
                        <span class=\"km\">4.920 km</span>
                        <img src=\"";
        // line 52
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "trayectoria/steps-trayectoria-06.png\" alt=\"Gi, Justicia\">
                    </div>
                    <div id=\"lealtad\" class=\"steps\">
                        <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Lealtad\">Chu</span>
                        <span class=\"km\">5.900 km</span>
                        <img src=\"";
        // line 57
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "trayectoria/steps-trayectoria-07.png\" alt=\"Gi, Justicia\">
                    </div>
                    
                    
                </div>     
            </div>
            
        </div>
            <div class=\"promo promo-dark promo-full bottommargin-sm\">
            <div class=\"container clearfix center\">
                <h3>Las 7 virtudes del código Bushido</h3>
            </div>
        </div>
        
        
            
        <div class=\"container clearfix\">
            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
                <div class=\"col-lg-6 col-sm-12\">
                    <img src=\"";
        // line 76
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "trayectoria/bushido/virtudes-samurai-01.png\">
                </div>
                <div class=\"col-lg-6 col-sm-12\">
                    <h4><span class=\"virtudes\">GI</span> / Justicia</h4>
                    <p>Sé honrado en tus tratos con todo el mundo. Cree en la justicia, pero no en la que emana de los demás, sino en la tuya propia.</p>
                </div>
            </div>
            
            
            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
                <div class=\"col-lg-6 col-sm-12\">
                    <img src=\"";
        // line 87
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "trayectoria/bushido/virtudes-samurai-02.png\">
                </div>
                <div class=\"col-lg-6 col-sm-12\">
                    <h4><span class=\"virtudes\">YU</span> / Valor</h4>
                    <p>Es arriesgado, es peligroso, pero sin duda también es vivir la vida de forma plena. Reemplaza el miedo por el respeto y la precaución.</p>
                </div>
            </div>
            
            
            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
                <div class=\"col-lg-6 col-sm-12\">
                    <img src=\"";
        // line 98
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "trayectoria/bushido/virtudes-samurai-03.png\">
                </div>
                <div class=\"col-lg-6 col-sm-12\">
                    <h4><span class=\"virtudes\">JIN</span> / Compasión</h4>
                    <p>Desarollarás un poder tan grande que deberás usarlo solo para el bien de todos. Debes tener compasión.</p>
                </div>
            </div>
            
            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
                <div class=\"col-lg-6 col-sm-12\">
                    <img src=\"";
        // line 108
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "trayectoria/bushido/virtudes-samurai-04.png\">
                </div>
                <div class=\"col-lg-6 col-sm-12\">
                    <h4><span class=\"virtudes\">REI</span> / Cortesía</h4>
                    <p>Ser un guerrero no justifica la crueldad. No necesitas demostrar tu fuerza a nadie salvo a tí mismo. Se especialmente cortés con tus enemigos, sino, no serás mejor que los animales.</p>
                </div>
            </div>
            
            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
                <div class=\"col-lg-6 col-sm-12\">
                    <img src=\"";
        // line 118
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "trayectoria/bushido/virtudes-samurai-05.png\">
                </div>
                <div class=\"col-lg-6 col-sm-12\">
                    <h4><span class=\"virtudes\">MAKOTO</span> / Sinceridad</h4>
                    <p>Cuando un samurai dice que hará algo, es como si ya estuviera hecho. \"Hablar\" y \"hacer\" son, para un samurai, la misma acción.</p>
                </div>
            </div>
            
            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
                <div class=\"col-lg-6 col-sm-12\">
                    <img src=\"";
        // line 128
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "trayectoria/bushido/virtudes-samurai-06.png\">
                </div>
                <div class=\"col-lg-6 col-sm-12\">
                    <h4><span class=\"virtudes\">MEYO</span> / Honor</h4>
                    <p>Solo tienes un juez de tu propio honor, tu mismo. Las decisiones que tomas y cómo las llevas a cabo son un reflejo de quién eres. Nadie puede ocultarse de sí mismo.</p>
                </div>
            </div>
            
            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
                <div class=\"col-lg-6 col-sm-12\">
                    <img src=\"";
        // line 138
        echo twig_escape_filter($this->env, (isset($context["imgurl"]) ? $context["imgurl"] : null), "html", null, true);
        echo "trayectoria/bushido/virtudes-samurai-07.png\">
                </div>
                <div class=\"col-lg-6 col-sm-12\">
                    <h4><span class=\"virtudes\">CHU</span> / Lealtad</h4>
                    <p>Todas tus acciones son como tus huellas: pueden seguirlas donde quiera que vayas, por ello debes tener cuidado con el camino que sigues.</p>
                </div>
            </div>
        </div>
            

            
            
            
            
             
  
    </div>

</section><!-- #content end -->

";
    }

    public function getTemplateName()
    {
        return "taller/trayectoria.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  197 => 138,  184 => 128,  171 => 118,  158 => 108,  145 => 98,  131 => 87,  117 => 76,  95 => 57,  87 => 52,  79 => 47,  71 => 42,  63 => 37,  55 => 32,  47 => 27,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("
<!-- Page Title
        ============================================= -->
<section id=\"page-title\" class=\"header trayectoria_header\">

    <div class=\"container clearfix\">
        <h1>Trayectoria</h1>
    </div>

</section><!-- #page-title end -->

<!-- Content
        ============================================= -->
<section id=\"content\">

    <div class=\"content-wrap\">

        <div class=\"container clearfix\">
            <h3 style=\"margin: 0;\" >Trayectoria</h3>
            <div style=\"margin: 1% 0 4% 0;\" class=\"line-custom\"></div>
            <div class=\"trayectoria\">
                <div class=\"container clearfix bg-trayectoria nobottommargin\">
                    
                    <div id=\"justicia\" class=\"steps\">
                        <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Justicia\">Gi</span>
                        <span class=\"km\">820 km</span>
                        <img src=\"{{imgurl}}trayectoria/steps-trayectoria-01.png\" alt=\"Gi, Justicia\">
                    </div>
                    <div id=\"valor\" class=\"steps\">
                        <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Valor\">Yu</span>
                        <span class=\"km\">1.640 km</span>
                        <img src=\"{{imgurl}}trayectoria/steps-trayectoria-02.png\" alt=\"Yu, Valor\">
                    </div>
                    <div id=\"compasion\" class=\"steps\">
                        <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Compasión\">Jin</span>
                        <span class=\"km\">2.460 km</span>
                        <img src=\"{{imgurl}}trayectoria/steps-trayectoria-03.png\" alt=\"Gi, Justicia\">
                    </div>
                    <div id=\"cortesia\" class=\"steps\">
                        <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Cortesía\">Rei</span>
                        <span class=\"km\">3.280 km</span>
                        <img src=\"{{imgurl}}trayectoria/steps-trayectoria-04.png\" alt=\"Gi, Justicia\">
                    </div>
                    <div id=\"sinceridad\" class=\"steps\">
                        <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Sinceridad\">Makoto</span>
                        <span class=\"km\">4.100 km</span>
                        <img src=\"{{imgurl}}trayectoria/steps-trayectoria-05.png\" alt=\"Gi, Justicia\">
                    </div>
                    <div id=\"honor\" class=\"steps\">
                        <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Honor\">Meyo</span>
                        <span class=\"km\">4.920 km</span>
                        <img src=\"{{imgurl}}trayectoria/steps-trayectoria-06.png\" alt=\"Gi, Justicia\">
                    </div>
                    <div id=\"lealtad\" class=\"steps\">
                        <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Lealtad\">Chu</span>
                        <span class=\"km\">5.900 km</span>
                        <img src=\"{{imgurl}}trayectoria/steps-trayectoria-07.png\" alt=\"Gi, Justicia\">
                    </div>
                    
                    
                </div>     
            </div>
            
        </div>
            <div class=\"promo promo-dark promo-full bottommargin-sm\">
            <div class=\"container clearfix center\">
                <h3>Las 7 virtudes del código Bushido</h3>
            </div>
        </div>
        
        
            
        <div class=\"container clearfix\">
            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
                <div class=\"col-lg-6 col-sm-12\">
                    <img src=\"{{imgurl}}trayectoria/bushido/virtudes-samurai-01.png\">
                </div>
                <div class=\"col-lg-6 col-sm-12\">
                    <h4><span class=\"virtudes\">GI</span> / Justicia</h4>
                    <p>Sé honrado en tus tratos con todo el mundo. Cree en la justicia, pero no en la que emana de los demás, sino en la tuya propia.</p>
                </div>
            </div>
            
            
            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
                <div class=\"col-lg-6 col-sm-12\">
                    <img src=\"{{imgurl}}trayectoria/bushido/virtudes-samurai-02.png\">
                </div>
                <div class=\"col-lg-6 col-sm-12\">
                    <h4><span class=\"virtudes\">YU</span> / Valor</h4>
                    <p>Es arriesgado, es peligroso, pero sin duda también es vivir la vida de forma plena. Reemplaza el miedo por el respeto y la precaución.</p>
                </div>
            </div>
            
            
            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
                <div class=\"col-lg-6 col-sm-12\">
                    <img src=\"{{imgurl}}trayectoria/bushido/virtudes-samurai-03.png\">
                </div>
                <div class=\"col-lg-6 col-sm-12\">
                    <h4><span class=\"virtudes\">JIN</span> / Compasión</h4>
                    <p>Desarollarás un poder tan grande que deberás usarlo solo para el bien de todos. Debes tener compasión.</p>
                </div>
            </div>
            
            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
                <div class=\"col-lg-6 col-sm-12\">
                    <img src=\"{{imgurl}}trayectoria/bushido/virtudes-samurai-04.png\">
                </div>
                <div class=\"col-lg-6 col-sm-12\">
                    <h4><span class=\"virtudes\">REI</span> / Cortesía</h4>
                    <p>Ser un guerrero no justifica la crueldad. No necesitas demostrar tu fuerza a nadie salvo a tí mismo. Se especialmente cortés con tus enemigos, sino, no serás mejor que los animales.</p>
                </div>
            </div>
            
            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
                <div class=\"col-lg-6 col-sm-12\">
                    <img src=\"{{imgurl}}trayectoria/bushido/virtudes-samurai-05.png\">
                </div>
                <div class=\"col-lg-6 col-sm-12\">
                    <h4><span class=\"virtudes\">MAKOTO</span> / Sinceridad</h4>
                    <p>Cuando un samurai dice que hará algo, es como si ya estuviera hecho. \"Hablar\" y \"hacer\" son, para un samurai, la misma acción.</p>
                </div>
            </div>
            
            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
                <div class=\"col-lg-6 col-sm-12\">
                    <img src=\"{{imgurl}}trayectoria/bushido/virtudes-samurai-06.png\">
                </div>
                <div class=\"col-lg-6 col-sm-12\">
                    <h4><span class=\"virtudes\">MEYO</span> / Honor</h4>
                    <p>Solo tienes un juez de tu propio honor, tu mismo. Las decisiones que tomas y cómo las llevas a cabo son un reflejo de quién eres. Nadie puede ocultarse de sí mismo.</p>
                </div>
            </div>
            
            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">
                <div class=\"col-lg-6 col-sm-12\">
                    <img src=\"{{imgurl}}trayectoria/bushido/virtudes-samurai-07.png\">
                </div>
                <div class=\"col-lg-6 col-sm-12\">
                    <h4><span class=\"virtudes\">CHU</span> / Lealtad</h4>
                    <p>Todas tus acciones son como tus huellas: pueden seguirlas donde quiera que vayas, por ello debes tener cuidado con el camino que sigues.</p>
                </div>
            </div>
        </div>
            

            
            
            
            
             
  
    </div>

</section><!-- #content end -->

", "taller/trayectoria.php", "C:\\MAMP\\htdocs\\Bushido2018\\application\\views\\taller\\trayectoria.php");
    }
}
